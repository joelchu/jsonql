// testing the config
const test = require('ava')
const fsx = require('fs-extra')
const { join, resolve } = require('path')

const { configCheck } = require('../src/options')

const processJwtKeysDefault = require('../src/options/process-jwt-keys')
const processJwtKeys = processJwtKeysDefault.default

// const { jwtProcessKey } = require('../src/options/options')

const debug = require('debug')('jsonql-koa:test:config')

const baseDir = join(__dirname, 'fixtures')
const resolverDir = join(baseDir, 'resolvers')
const contractDir = join(baseDir, 'tmp', 'config-test')
const contractDir1 = join(baseDir, 'contracts')
const keysDir = join(baseDir, 'tmp', 'keys')

// mocking a ctx object
let ctx = {
  state: {
    jsonql: {
      setter: () => {},
      getter: () => {}
    }
  }
}

test.after( t => {
  
  fsx.removeSync(contractDir1)
  fsx.removeSync(contractDir)
  fsx.removeSync(keysDir)
  // this trigger by the enableAuth default option test
  fsx.removeSync(join(process.cwd(), 'keys'))
})

test(`It should able to use all the default options when enableAuth=true`, t => {
  const opts = configCheck({
    enableAuth: true,
    contractDir,
    resolverDir
  })

  t.truthy(opts.keysDir)
})


test('It should able to check the in dir', t => {
  const opts = configCheck({
    resolverDir,
    contractDir
  })

  const dir = resolve(resolverDir)
  t.is( dir, opts.resolverDir )
})

test(`It should allow to use the appDir option`, t => {
  const opts = configCheck({
    appDir: 'tests/fixtures',
    contractDir
  })
  // debug(opts)
  t.is(opts.contractDir, contractDir, 'should also allow to pass a custom directory name')
})


test('It should have privateKey and publicKey when set useJwt = true', async t => {
  let opts = configCheck({
    enableAuth: true,
    useJwt: true,
    resolverDir,
    contractDir,
    keysDir
  })

  opts = await processJwtKeys(ctx, opts)

  t.truthy(opts.privateKey && opts.publicKey)
})

