const test = require('ava')

const superkoa = require('superkoa')
const { join } = require('path')
const debug = require('debug')('jsonql-koa:test:auth')
const { createQuery } = require('jsonql-utils')
const fsx = require('fs-extra')
const { merge } = require('lodash')
const { HELLO_FN } = require('jsonql-constants')
// const jsonqlMiddleware = require(join(__dirname, '..', 'index'))
const { type, headers, dirs, bearer, contractKeyName } = require('./fixtures/options')
const createServer = require('./helpers/server')
const myKey = '4670994sdfkl'
const dir = 'auth'
const thisHeader = merge({},  {[contractKeyName]: myKey}, headers)
const keysDir = join(join(dirs.contractDir, 'auth-keys'))

test.before((t) => {
  t.context.app = createServer({
    useJwt: false,
    enableAuth: true,
    contractKey: myKey,
    keysDir
  }, dir)
})

test.after( () => {
  // remove the files after
  fsx.removeSync(join(dirs.contractDir, dir))
  fsx.removeSync(keysDir)
})

// Start running test(s)

test.serial("Should NOT fail this Hello world test even I am not login", async t => {
  let res = await superkoa(t.context.app)
    .post('/jsonql')
    .query({_cb: Date.now()})
    .set(thisHeader)
    .send(
      createQuery(HELLO_FN)
    );
  t.is(200, res.status)
})

test.serial("The public-contract.json file should contain issuer information", async t => {
  let res = await superkoa(t.context.app)
    .get('/jsonql')
    .query({_cb: Date.now()})
    .set(thisHeader);

  t.truthy(res.body.data.auth.login)
})


test.serial('Should able to login with this credential', async t => {
  let res = await superkoa(t.context.app)
    .post('/jsonql')
    .query({_cb: Date.now()})
    .set(thisHeader)
    .send(
      createQuery('login', ['nobody', myKey])
    );
  t.is(200, res.status)
  // we are not using the useJwt anymore, so the token is different from the key
  // t.is(bearer, res.body.data)
})

test.serial('Should cause a JsonqlAuthorisationError if I pass the wrong username or password', async t => {
  let res = await superkoa(t.context.app)
    .post('/jsonql')
    .query({_cb: Date.now()})
    .set(thisHeader)
    .send(
      createQuery('login', ['body-x', myKey])
    );

    debug(res.body.error)

  t.is(200, res.status)
  t.is('JsonqlAuthorisationError', res.body.error.className)
})


test.serial("It should able to call a method that is mark as public without login", async t => {
  // alwaysAvailable
  let res = await superkoa(t.context.app)
    .post('/jsonql')
    .query({_cb: Date.now()})
    .set(headers)
    .send(
      createQuery('alwaysAvailable')
    );
  t.is(200, res.status)
  t.is('Hello there', res.body.data)
})

test.serial('Now I should able to call the api with credential', async t => {
  let res = await superkoa(t.context.app)
    .post('/jsonql')
    .query({_cb: Date.now()})
    .set(merge({} , thisHeader, {Authorization: `Bearer ${bearer}`}))
    .send(
      createQuery('getUser', ['testing', 'userId'])
    )
  t.is(200, res.status)
  // debug(res.body)
  t.is(1, res.body.data.userId)
})

test.serial("Should NOT able to call the logout method without the Bearer", async t => {
  let res = await superkoa(t.context.app)
    .post('/jsonql')
    .query({_cb: Date.now()})
    .set(merge({} , thisHeader))
    .send(
      createQuery('logout')
    );

  t.is(200, res.status)
  // should get an error object
  t.truthy(res.body.error)

})
