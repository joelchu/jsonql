// this will use the enableAuth and useJwt options to test out all the new modules
const test = require('ava')

const superkoa = require('superkoa');
const { join } = require('path');
const clientJwtDecode = require('jwt-decode')
const { jwtDecode } = require('jsonql-jwt')
const { merge } = require('lodash')
const fsx = require('fs-extra')
const { RSA_ALGO } = require('jsonql-constants')

const debug = require('debug')('jsonql-koa:test:jwt');

const { type, headers, dirs, dummy } = require('./fixtures/options');
const createServer = require('./helpers/server')
const dir = 'jwt-auth';
const keysDir = join(__dirname, 'fixtures', 'keys')

const { createTokenValidator, loginResultToJwt } = require('jsonql-jwt')

const name = 'Joel';

// start test
test.before( t => {
  t.context.app = createServer({
    enableAuth: true,
    loginHandlerName: 'customLogin',
    validatorHandlerName: false,
    privateMethodDir: 'private',
    keysDir
  }, dir)

  const privateKey = fsx.readFileSync(join(keysDir, 'privateKey.pem'))
  const options = { exp: 60*60 }
  t.context.encoder = loginResultToJwt(privateKey, options, RSA_ALGO)
  t.context.token = t.context.encoder({ name })
})

test.after(t => {

  fsx.removeSync(join(__dirname, 'fixtures', 'tmp', dir))
  // fsx.removeSync(join(__dirname, 'fixtures', 'tmp', ''))
})


test('It should able to query the private method with token', async t => {

  let res = await superkoa(t.context.app)
    .post('/jsonql')
    .query({_cb: Date.now()})
    .set(merge({}, headers, {Authorization: `Bearer ${t.context.token}`}))
    .send({
      testList: {
        args: [667]
      }
    })

  t.is(200, res.status)
  let payload = res.body.data;

  t.is(666, payload.num)
  t.is('nope', payload.text)
})

test('It should able to query a private method that is inside the private folder', async t => {
  let res = await superkoa(t.context.app)
    .post('/jsonql')
    .query({_cb: Date.now()})
    .set(merge({}, headers, {Authorization: `Bearer ${t.context.token}`}))
    .send({
      getSecretMsg: {
        args: []
      }
    })

  t.is(200, res.status)
  t.truthy( res.body.data.indexOf('secret') )

})
