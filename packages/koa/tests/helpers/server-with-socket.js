// for testing the server with socket
const { join } = require('path')
const server = require('server-io-core')
const { jsonqlWsServer } = require('jsonql-ws-server')

const { jsonqlKoa } = require('../../main')

const debug = require('debug')('jsonql-koa:fixtures:server-with-socket')
const baseDir = join(__dirname, '..', 'fixtures')
// output
module.exports = function serverWithSocket(port = 8001, extra = {}) {
  const config = Object.assign({
    keysDir: join(baseDir, 'keys')
  }, extra)
  debug(`Server ${port}`, config)
  return Promise.resolve(
    server({
      port: port,
      debug: false,
      open: false,
      reload: false,
      socket: false,
      middlewares: [
        jsonqlKoa(config)
      ]
    })
  ).then(({webserver, stop}) => (
    jsonqlWsServer(config, webserver)
      .then(() => ({ stop }))
  ))
}
