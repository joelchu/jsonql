// this will export the server for other test to use
// const test = require('ava');
const http = require('http')



const Koa = require('koa')
const { join } = require('path')
const bodyparser = require('koa-bodyparser')
const { jsonqlKoa } = require('../../main')

const dir = join(__dirname, '..', 'fixtures')
const contractDir = join(dir, 'tmp')

const baseResolverDir = join(dir, 'resolvers')
const msResolverDir = join(dir, 'sub')

const debug = require('debug')('jsonql-koa:helpers:server')

// add a dir to seperate the contract files
module.exports = function createServer(config={}, dir = '') {

  const opts = Object.assign({},{
    resolverDir: baseResolverDir,
    contractDir: join(contractDir, dir)
  }, config)

  debug('createServer raw config', config)

  const app = new Koa()
  app.use(bodyparser())
  app.use(jsonqlKoa(opts))

  return app
}
