// test the default options
const test = require('ava')
const server = require('./helpers/stock-server')
const superkoa = require('superkoa')
const { join } = require('path')
const dir = join(__dirname, 'jsonql')
const fsx = require('fs-extra')
const { headers } = require('./fixtures/options')
const debug = require('debug')('jsonql-koa:test:default')
test.before(t => {
  t.context.app = server()
})

test.after(t => {
  fsx.removeSync(join(dir, 'contract'))
})

test(`We should able to use default option to start up a jsonql koa server`, async t => {
  const res = await superkoa(t.context.app)
    .post('/jsonql')
    .set(headers)
    .send({
      helloWorld: {
        args: []
      }
    })

  debug(res.body)
  t.is(res.body.data, 'Hello world!')
  // t.is(200, res.body.error.statusCode)

})
