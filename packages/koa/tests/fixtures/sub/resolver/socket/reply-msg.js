/**
 * expect a call from someone else then modifiy the msg and reply it
 * @param {string} msg incoming message
 * @return {string} modified message
 */
module.exports = function replyMsg(msg) {
  if (msg === 'hello') {
    return `What's the matter?`
  }
  return `What do you want?`
}
