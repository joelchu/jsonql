
/**
 * This is a custom login method instead of the stock version
 * @param {string} username username
 * @param {string} password password
 * @return {object} userdata payload
 */
module.exports = function customLogin(username, password) {
  if (password === '123456') {
    return {name: username}
  }
  throw new Error('Login failed!')
}
