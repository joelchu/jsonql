// separate test for the contract interface
const test = require('ava')
const { join } = require('path')
const fsx = require('fs-extra')
// it was import from the src but now moved to the module @ 1.3.9
const { contractGenerator } = require('jsonql-contract/extra')
const { isContract } = require('jsonql-utils')

const debug = require('debug')('jsonql-koa:test:gen')

const resolverDir = join(__dirname, 'fixtures', 'resolvers')
const contractDir = join(__dirname, 'fixtures', 'tmp', 'generator')
// start test
test.before(async t => {
  t.context.initContract = await contractGenerator({
    contractDir,
    resolverDir,
    returnAs: 'json'
  })
})

test.after( t => {
  fsx.removeSync(contractDir)
})

test('It should able to generate a contract file', async t => {
  // this way can test if this setup works for the middleware as well
  const contract = await t.context.initContract;
  debug(contract)
  t.truthy(isContract(contract))
})
