// testing the new jsonp methods
const test = require('ava')
const { JSONP_CALLBACK_NAME } = require('jsonql-constants')
const superkoa = require('superkoa')
const { join } = require('path')
const debug = require('debug')('jsonql-koa:test:jsonp')
const { type, headers, dirs, returns } = require('./fixtures/options')
const fsx = require('fs-extra')

const { resolverDir, contractDir } = dirs

const createServer = require('./helpers/server')
const dir = 'standard';

test.before((t) => {
  t.context.app = createServer({enableJsonp: true}, dir)
})

test.after( () => {

})
// @TODO this is not the proper JSONP need to think about it before continue with this feature
test.skip('It should able to tell if this is access using jsonp', async t => {
  let res = await superkoa(t.context.app)
    .post('/jsonql')
    .set(headers)
    .query({
      _cb: Date.now(),
      [JSONP_CALLBACK_NAME]: 'helloWorld'
    })
    .send({
      args: []
    })
  // debug(res.body)
  t.is(200, res.status)
  t.is('Hello world!', res.body.data)

})
