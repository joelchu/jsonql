/**
 * This is the main interface to export the middleware(s)
 * we will take the config here and export array of middleware using koa-compose
 */
import compose  from 'koa-compose'
import {
  coreMiddleware,
  authMiddleware,
  contractMiddleware,
  helloMiddleware,
  consoleMiddleware,
  publicMethodMiddleware,
  initMiddleware
}  from './src/middlewares'
import { configCheck, extendConfigCheck } from './src/options'
import { getDebug }  from './src/utils'
const debug = getDebug('main')
// @2020-03-03 
/*
The configuration check causing no end of problem
I am going to ditch that whole pre-check bs, and take 
apart the config-check and init the actual module 
when we need to put this together with other module
we just take it out like a lego and put it back together again 
*/

/**
 * The actual method that compose all the middlewares together
 * @param {object} opts configuration
 * @return {object} compose together middlewares as one
 */
function composeJsonqlKoa(opts) {
  // export
  let middlewares = [
    initMiddleware(opts),
    helloMiddleware(opts),
    contractMiddleware(opts)
  ]
  // only available when enable it
  if (opts.enableWebConsole) {
    middlewares.push(consoleMiddleware(opts))
  }

  if (opts.enableAuth) {
    middlewares.push(
      publicMethodMiddleware(opts),
      authMiddleware(opts)
    )
  }
  middlewares.push(
    coreMiddleware(opts)
  )
  // finally
  return compose(middlewares)
}

/**
 * Default interface when usig this standalone 
 * @param {object} config user supply configuration 
 * @return {object} composed koa middleware
 */
function jsonqlKoa(config = {}) {
  // first check the config
  const opts = configCheck(config)
  debug('[jsonql-koa] init opts', opts)
  return composeJsonqlKoa(opts)
}

// export 
export { 
  jsonqlKoa, 
  extendConfigCheck, 
  composeJsonqlKoa 
}
