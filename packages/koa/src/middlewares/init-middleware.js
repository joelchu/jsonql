// this will be the first part of the middleware that checkout the
// headers and request and extract the parts that we need for the operation
import { QUERY_NAME, MUTATION_NAME, CONTRACT_NAME, RESOLVER_PARAM_NAME } from 'jsonql-constants'
import { injectToFn } from 'jsonql-utils'
import {
  getDebug,
  isJsonqlRequest,
  ctxErrorHandler,
  processJwtKeys,
  getQueryFromPayload,
  getMutationFromPayload
} from '../utils'

import processContract from '../contracts/process-contract'
import {
  getBaseResolverType,
  isAuthType,
  isJsonpCall
} from '../utils/init-middleware-helpers'

const debug = getDebug('init-middleware')

/**
 * v1.2.0 add setter and getter and store in the ctx for use later
 * @param {object} opts configuration
 * @param {function} setter nodeCache set
 * @param {function} getter nodeCache get
 * @return {function} middleware
 */
export default function initMiddleware(opts) {
  // export
  return async function(ctx, next) {
    ctx.state.jsonql = {}
    // ctx.state.jsonql.setter = setter;
    // ctx.state.jsonql.getter = getter;
    // first check if its calling the jsonql api
    const isJsonql = isJsonqlRequest(ctx, opts)
    // init our own context
    ctx.state.jsonql.isReq = isJsonql;
    if (isJsonql) {
      // its only call once
      ctx.state.jsonql.contract = await processContract(ctx, opts)
      // debug('ctx.state.jsonql.contract', ctx.state.jsonql.contract)
      // @BUG makes no different to define the contract as a property, somewhere still overwrite it
      // @NOTE see if this works or not to make sure the contract prop is not writable
      // ctx.state.jsonql = injectToFn(ctx.state.jsonql, 'contract', __contract__)

      // v1.2.0 grabbing the public / private keys
      opts = await processJwtKeys(ctx, opts)
      // debug('processJwtKeys', opts)
      // get what is calling
      let payload = ctx.request.body;
      // new in v1.3.0 - test for jsonp type
      if (opts.enableJsonp === true) {
        const jsonp = isJsonpCall(ctx)
        if (jsonp !== false) {
          ctx.state.jsonql.jsonp = jsonp;
          // mutate the payload to let the rest to work
          payload = {[jsonp]: payload}
          debug('jsonp', jsonp, payload)
        }
      }
      // start
      let type = getBaseResolverType(ctx)
      if (type) {
        let params;
        switch(type) {
          case CONTRACT_NAME:
            ctx.state.jsonql.resolverType = CONTRACT_NAME;
          break;
          case QUERY_NAME:
            params = getQueryFromPayload(payload)
          break;
          case MUTATION_NAME:
            params = getMutationFromPayload(payload)
          break;
          default: // should never happen!
            throw new JsonqlError(`[init-middleware] ${type} is unknown!`)
        }
        if (type !== CONTRACT_NAME) {
          let name = params[RESOLVER_PARAM_NAME]
          ctx.state.jsonql.resolverName = name;
          ctx.state.jsonql.payload = params;
          ctx.state.jsonql.resolverType = isAuthType(type, name, opts)
        }
      } else {
        return ctxErrorHandler(ctx, 406, {
            message: 'Payload is not the expected object type',
            payload
          }
        )
      }
    }
    await next()
  }
}
