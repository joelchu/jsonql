// new file upload middleware that will get hook into
// our jwt authorization system
import fs from 'fs'
import os from 'os'
import { join } from 'path'

import { API_REQUEST_METHODS } from 'jsonql-constants'
const [ POST ] = API_REQUEST_METHODS;


export default function fileMiddleware(opts) {
  return async function(ctx, next) {
    if (opts.useFileUpload) {
      if (POST != ctx.method) {
        return await next()
      }
      // should expect an array
      const file = ctx.request.files[config.fileUploadName]
      const tmpDir = config.fileUploadDist || join(os.tmpdir(), Math.random().toString())
      const reader = fs.createReadStream(file.path)
      const stream = fs.createWriteStream(tmpDir)

      reader.pipe(stream)

      debug('uploading %s -> %s', file.name, stream.path)
    }
    await next()
  }
}
