// This will be handling the json:ql console
import webConsole from 'jsonql-web-console'
import { isJsonqlConsoleUrl } from '../utils'

// export
export default function consoleMiddleware(opts) {
  return webConsole(opts, isJsonqlConsoleUrl)
}
