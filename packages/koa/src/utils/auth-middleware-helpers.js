// Taking the methods out from the auth-middleware to keep it simple
// jsonql-auth middleware
import {
  AUTH_TYPE,
  ISSUER_NAME,
  VALIDATOR_NAME,
  AUTH_CHECK_HEADER,
  BEARER
} from 'jsonql-constants'
import { createTokenValidator } from 'jsonql-jwt'
import {
  chainFns,
  getDebug,
  forbiddenHandler,
  ctxErrorHandler,
  isObject
} from '../utils'
import {
  JsonqlResolverNotFoundError,
  JsonqlAuthorisationError,
  JsonqlValidationError,
  finalCatch
} from 'jsonql-errors'
// this method just search if the user provide their own validate token method
import { getLocalValidator } from 'jsonql-resolver'
import { trim } from 'lodash'

const debug = getDebug('auth-middleware-helpers')

// this will create a cache version without keep calling the getter
var validatorFn

// declare a global variable to store the userdata with null value
// this way we don't mess up with the resolver have to check
// the last param is the user data

/**
 * @param {object} ctx Koa context
 * @param {string} type to look for
 * @return {mixed} the bearer token on success
 */
export const authHeaderParser = ctx => {
  // const header = headers[AUTH_CHECK_HEADER];
  let header = ctx.request.get(AUTH_CHECK_HEADER)
  // debug(_header, AUTH_CHECK_HEADER);
  // debug('Did we get the token?', header);
  return header ? getToken(header) : false
}

/**
 * just return the token string
 * @param {string} header
 * @return {string} token
 */
export const getToken = header => trim(header.replace(BEARER, ''))


/**
 * when using useJwt we allow the user to provide their own validator
 * and we pass the result to this validator for them to do further processing
 * This is useful because the user can control if they want to invalidate the client side
 * from here based on their need
 * @param {object} config configuration
 * @param {function|boolean} validator false if there is none
 * @return {function} the combine validator
 */
const createJwtValidatorChain = (config, validator = false) => {
  const jwtFn = createTokenValidator(config)
  if (!validator || typeof validator !== 'function') {
    return jwtFn;
  }

  return chainFns(jwtFn, validator)
}

/**
 * wrap this together and try to cache that validator methods
 * @param {object} config configuration
 * @param {*} localValidator could be nothing or a function
 * @return {function} validator
 */
const returnValidator = (config, localValidator) => {
  if (config.useJwt) { // we always use the jwt opiton now
    validatorFn = createJwtValidatorChain(config, localValidator)
  }
  else if (typeof localValidator === 'function') {
    validatorFn = localValidator
  }
  else {
    throw new JsonqlResolverNotFoundError(`[returnValidator] ${config.validatorHandlerName} NOT FOUND!`)
  }
  return validatorFn;
}

/**
 * if useJwt = true then use the jsonql-jwt version
 * @param {object} config configuration
 * @param {object} contract contract.json
 * @return {function} the correct handler
 */
export const getValidator = (config, contract) => {
  let localValidator
  try {
    if (validatorFn && typeof validatorFn === 'function') {
      debug(`return the cache validatorFn`)
      return validatorFn;
    }
    localValidator = getLocalValidator(config, contract)

    return returnValidator(config, localValidator)
  } catch(e) {
    // debug('localValidator throw error', e)
    const checkErr = e.className === 'JsonqlResolverNotFoundError'
                  || e instanceof JsonqlResolverNotFoundError
    if (!checkErr) {
      debug(`Not the JsonqlResolverNotFoundError?`, e)
      return finalCatch(e)
    }
    return returnValidator(config, localValidator)
  }
}
