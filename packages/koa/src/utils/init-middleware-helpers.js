// move all the functions in the init-middleware here to keep it clean
import { inArray, isObject, getDebug } from './index'
import {
  AUTH_TYPE,
  QUERY_NAME,
  MUTATION_NAME,
  CONTRACT_NAME,
  CONTRACT_REQUEST_METHODS,
  API_REQUEST_METHODS,
  JSONP_CALLBACK_NAME
} from 'jsonql-constants'
// import { Jsonql406Error } from 'jsonql-errors'
// import processContract from '../contracts/process-contract'
// should move to the utils as well

const debug = getDebug('init-middleware-helpers')

/**
 * Just figure out what is the calling methods here
 * @param {object} ctx koa context
 * @return {*} false on unknown
 */
const getBaseResolverType = function(ctx) {
  const [ POST, PUT ] = API_REQUEST_METHODS;
  const { method } = ctx;
  switch (true) {
    case inArray(CONTRACT_REQUEST_METHODS, method):
      return CONTRACT_NAME;
    case method === POST:
      return QUERY_NAME;
    case method === PUT:
      return MUTATION_NAME;
  }
}

/**
 * check if it is auth type
 * @param {string} type we need this to be the QUERY_TYPE
 * @param {string} name from ctx.request.body to find the issuer name present or not
 * @param {object} opts config we need that to find the custom names
 */
const isAuthType = function(type, name, opts) {
  const { logoutHandlerName, loginHandlerName } = opts;
  const AUTH_TYPE_METHODS = [loginHandlerName, logoutHandlerName]
  if (type === QUERY_NAME) {
    return inArray(AUTH_TYPE_METHODS, name) ? AUTH_TYPE : type;
  }
  return type;
}

/**
 * new in v1.3.0 jsonp handler
 * @param {object} ctx koa context
 * @return {boolean|string} return resolverName or false when its not
 */
const isJsonpCall = function(ctx) {
  if (ctx.query && ctx.query[JSONP_CALLBACK_NAME]) {
    return ctx.query[JSONP_CALLBACK_NAME]
  }
  return false;
}

export { getBaseResolverType, isAuthType, isJsonpCall }
