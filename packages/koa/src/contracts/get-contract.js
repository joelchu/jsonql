// move all the code into it's folder
// we are going to fork the process to lighten the load when it start
import { splitContractGenerator, contractGenerator } from 'jsonql-contract/extra'

/**
 * getContract main - all the contract related methods move back to contract-cli
 * @param {object} config options
 * @param {boolean} pub public contract or not
 * @return {object} Promise to resolve the contract json
 */
export function getContract(config, pub = false) {
  return splitContractGenerator(config, pub)
  /*
  @TODO not using this for now
  if (config.enableSplitTask) {
    return splitContractGenerator(config, pub)
  }
  return contractGenerator(config, pub)
  */
}
