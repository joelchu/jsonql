// breaking up the methods from inside the index.js
// wrap all the options and method in one instead of all over the places
// @TODO all this should move to the jsonql-utils because when start dev for other
// framework this will get re-use
import { join, resolve } from 'path'
import fsx from 'fs-extra'
import lodash from 'lodash'

import { rsaPemKeys } from 'jsonql-jwt'
import { JsonqlError } from 'jsonql-errors'
import { validateClientConfig, clientsGenerator } from 'jsonql-resolver'
import {
  INIT_CONTRACT_PROP_KEY,
  INIT_CLIENT_PROP_KEY,
  // DEFAULT_RESOLVER_DIR,
  // DEFAULT_CONTRACT_DIR,
  // DEFAULT_KEYS_DIR
} from 'jsonql-constants'
import {
  jwtProcessKey,
  RESOLVER_DIR_KEY,
  REQ_DIR_NAMES,
  REQ_DIR_CHECK
} from './options'

import { isContract, getDebug  } from '../utils'
import { getContract } from '../contracts'

const debug = getDebug('config-check:main')

/**
 * @TODO move to jsonql-utils
 * break out from the applyAuthOptions because it's not suppose to be there
 * @NOTE v1.3.8 change it to a fully functional interface
 * @param {object} config configuration
 * @return {object} with additional properties
 */
const applyGetContract = function(config) {
  return lodash(config)
    .chain()
    .thru(config => {
      const { contract } = config
      if (isContract(contract)) {
        config.contract = contract
        return [true, config]
      }
      return [false, config]
    })
    .thru(result => {
      let [processed, config] = result
      if (!processed) {
        debug(`call ${INIT_CONTRACT_PROP_KEY}`)
        // get resolve later inside the middleware
        config[INIT_CONTRACT_PROP_KEY] = getContract(config)
      }
      return config
    })
    .thru(config => {
      if (config.withPublicContract) {
        debug(`call generate public contract`)
        // we just let it run and don't wait for it
        getContract(config, true)
      }
      return config
    })
    .value()
}

/**
 * @TODO move to jsonql-utils because when we start express will need this
 * we need an extra step to cache some of the auth related configuration data
 * @param {object} config configuration
 * @return {object} config with extra property
 */
const applyAuthOptions = function(config) {
  // @TODO need to get rip of the useJwt option!
  // && config.useJwt && !isString(config.useJwt) OLD options!
  if (config.enableAuth) {
    const { keysDir, publicKeyFileName, privateKeyFileName } = config
    // @BUG some where if there is no config pass then becomes a boolean?
    debug('keysDir', keysDir)
    debug('publicKeyFileName', publicKeyFileName)
    debug('privateKeyFileName', privateKeyFileName)

    const publicKeyPath = join(keysDir, publicKeyFileName)
    const privateKeyPath = join(keysDir, privateKeyFileName)
    if (fsx.existsSync(publicKeyPath) && fsx.existsSync(privateKeyPath)) {
      config.publicKey = fsx.readFileSync(publicKeyPath)
      config.privateKey = fsx.readFileSync(privateKeyPath)
    } else {
      // we only call here then resolve inside the init-middleware
      config[jwtProcessKey] = rsaPemKeys(config.rsaModulusLength, config.keysDir)
    }
  }
  return config
}

/**
 * @1.4.18 pre-config the node client, there is a configuration for it
 * this will only call when using preConfigCheck
 * @param {object} config the main configuration
 * @return {object} the modified configuration if there is need for it
 */
const initNodeClient = function(config) {
  const clientConfigs = validateClientConfig(config)
  if (clientConfigs !== false) {
    debug(`create the clients config at the options level`)
    config[INIT_CLIENT_PROP_KEY] = clientsGenerator(clientConfigs)
  }
  return config
}

/**
 * @1.6.0 we put all the directory under the appDir from now on
 * it doesn't work exactly the same as I wanted it to
 * change tactic, first generate default path
 * then compare the user supplied path, to see if it's the same or different
 * @param {object} config options
 * @return {object} mutated configuration
 */
const applyDirectories = function(config) {
  const { projectRootDir, appDir } = config
  debug('projectRootDir', projectRootDir)
  debug('appDir', appDir) // how come this could become undefined?
  let i = 0
  return REQ_DIR_NAMES.reduce((opt, prop) => {
    let dir = opt[prop]
    if (dir === REQ_DIR_CHECK[i]) { // if it's default name
      debug(`${prop} dir`, dir)
      let propDir = join(projectRootDir, appDir, dir)
      if (prop === RESOLVER_DIR_KEY) {
        if (!fsx.existsSync(propDir)) {
          throw new JsonqlError('options.applyDirectories', `Expect the resolver redirectory to exist! ${propDir}`)
        }
      }
      opt[prop] = propDir
    }
    ++i
    return opt
  }, config)
}

export {
  applyGetContract,
  applyAuthOptions,
  initNodeClient,
  applyDirectories
}