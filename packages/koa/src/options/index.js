import { merge } from 'lodash'
import { chainFns } from 'jsonql-utils'
import { checkConfig } from 'jsonql-params-validator'
import { appProps, constProps } from './options'
import {
  applyGetContract,
  applyAuthOptions,
  initNodeClient,
  applyDirectories
} from './main'

// import { getDebug  } from '../utils'
// const debug = getDebug('config-check:index')

const getConfigFn = () => chainFns(
  checkConfig,
  applyDirectories,
  applyGetContract,
  applyAuthOptions,
  initNodeClient
)

/**
 * The main method that run before everything else to check the configuration
 * @param {object} config configuration supply by developer
 * @return {object} configuration been checked
 * @api public
 */
export function configCheck(config = {}) {
  const fn = getConfigFn()
  return fn(config, appProps, constProps)
}

/**
 * new methods for when using it combine with the jsonql-ws-server
 * @param {object} extraAppProps from the other config
 * @param {object} extraConstProps from the other config
 * @param {object} config supply by the user
 */
export function extendConfigCheck(extraAppProps, extraConstProps, config = {}) {
  const extendAppProps = merge({}, appProps, extraAppProps)
  const extendConstProps = merge({}, constProps, extraConstProps)
  
  const fn = getConfigFn()

  return fn(config, extendAppProps, extendConstProps)
}
