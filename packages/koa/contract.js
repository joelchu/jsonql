// @1.1.4 We move the contract generator to the top level
// so you could do import generator from 'jsonql-koa/contract'
// and pre-generate the contract to supply both the koa / ws setup
const { configCheck } = require('./src')
const contractApi = require('jsonql-contract')

/**
 * Wrap this in a promise
 * @param {object} config could be user supplied
 * @return {object} promise resolve options
 */
const getOptions = config => (
  Promise.resolve(config.__checked__ ? config : configCheck(config))
)

/**
 * Main interface note the public has to pass afterward
 * @param {object} config could be user supplied
 * @param {boolean} pub public contract or not
 * @return {object} promise resolve to the contract JSON
 */
module.exports = function(config, pub = false) {
  return getOptions(config)
    .then(({contractDir, resolverDir, enableAuth, returnAs }) => contractApi({
      public: pub,
      returnAs ,
      contractDir,
      resolverDir,
      enableAuth
    })
  )
}
