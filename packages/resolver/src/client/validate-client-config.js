const { join } = require('path')
const { inArray } = require('jsonql-utils')
const { JsonqlError } = require('jsonql-errors')
const { getDebug } = require('../utils')
const debug = getDebug('validate-client-config')
/**
 * Double check if the client config is correct or not also we could inject some of the properties here
 * @param {object} config configuration
 * @return {object|boolean} false when there is none, with additional properties
 */
const validateClientConfig = function(config) {
  const ctn = config.clientConfig ? config.clientConfig.length : 0
  // take the contractDir from the main
  const contractDir = config.contractDir
  if (ctn) {
    let names = []
    let clients = []
    for (let i = 0; i < ctn; ++i) {
      let client = config.clientConfig[i]
      if (!client.hostname) {
        throw new JsonqlError(`Missing hostname in client config ${i}`)
      }
      debug(`name: ${client.name}`)
      if (!client.name) {
        client.name = `nodeClient${i}`
      }
      if (inArray(names, client.name)) {
        throw new JsonqlError(`[${i}] ${client.name} already existed, can not have duplicated!`)
      }
      names.push(client.name)
      // next we need to create contract dir path using the name
      client.contractDir = join(contractDir, client.name)
      // push it back to the config
      clients.push(client)
    }
    return clients
  }
  return false
}

module.exports = { validateClientConfig }
