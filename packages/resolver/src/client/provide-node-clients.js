// setup and inject the clients into resolver
const { INIT_CLIENT_PROP_KEY } = require('jsonql-constants')
const { injectNodeClient } = require('./inject-node-clients')
const { validateClientConfig } = require('./validate-client-config')
const { clientsGenerator } = require('./clients-generator')
const { getDebug } = require('../utils')
const debug = getDebug('provide-node-clients')
// we need to change this to a class instead otherwise
// we might overwrite each others
let clients = []
let hasClientConfig
/**
 * @TODO delete later
 * The top level methods to inject the clients into the resolver
 * @param {function} resolver the function to get injection
 * @param {object} config configuration
 * @return {function} the resolver with injection if any
 */
function provideNodeClients(resolver, config) {
  return new Promise((resolve, rejecter) => {
    // back to the old code
    if (hasClientConfig === false) {
      debug(`nothing to inject`)
      return resolve(resolver) // nothing to do
    }
    // if there is cache clients
    if (clients.length) {
      debug(`inject client from cache`, clients.length)
      return resolve(injectNodeClient(resolver, clients))
    }
    // new on 0.9.9 check INIT_CLIENT_PROP_KEY from the config
    if (config[INIT_CLIENT_PROP_KEY] && config[INIT_CLIENT_PROP_KEY].then) {
      debug(`using the new INIT_CLIENT_PROP_KEY methods`)
      return config[INIT_CLIENT_PROP_KEY]
        .then(_clients => {
          clients = _clients
          resolve(injectNodeClient(resolver, _clients))
        })
        .catch(rejecter)
    }
    // pass the check result
    hasClientConfig = validateClientConfig(config)
    if (hasClientConfig === false) {
      debug(`check and nothing to inject`, config)
      return resolve(resolver) // nothing to do
    }
    debug(`run init clients`, hasClientConfig)
    // run init client once
    clientsGenerator(hasClientConfig)
      .then(_clients => {
        clients = _clients; // cache it
        resolve(injectNodeClient(resolver, _clients))
      })
      .catch(rejecter)
  })
}

/**
 * @TODO another way is to create a function factory
 * @param {object} config configuration
 * @return {function} to get the node clients if any
 */
function getNodeClientProvider(config) {
  let __clients = []
  let __hasClientConfig
  return (resolver) => {
    return new Promise((resolve, rejecter) => {
      // back to the old code
      if (__hasClientConfig === false) {
        debug(`nothing to inject`)
        return resolve(resolver) // nothing to do
      }
      // if there is cache clients
      if (__clients.length) {
        debug(`inject client from cache`, __clients.length)
        return resolve(injectNodeClient(resolver, __clients))
      }
      // new on 0.9.9 check INIT_CLIENT_PROP_KEY from the config
      if (config[INIT_CLIENT_PROP_KEY] && config[INIT_CLIENT_PROP_KEY].then) {
        debug(`using the new INIT_CLIENT_PROP_KEY methods`)
        return config[INIT_CLIENT_PROP_KEY]
          .then(_clients => {
            __clients = _clients
            resolve(injectNodeClient(resolver, _clients))
          })
          .catch(rejecter)
      }
      // pass the check result
      __hasClientConfig = validateClientConfig(config)
      if (__hasClientConfig === false) {
        debug(`check and nothing to inject`, config)
        return resolve(resolver) // nothing to do
      }
      debug(`run init clients`, __hasClientConfig)
      // run init client once
      clientsGenerator(__hasClientConfig)
        .then(_clients => {
          __clients = _clients; // cache it
          resolve(injectNodeClient(resolver, _clients))
        })
        .catch(rejecter)
    })
  }
}

module.exports = { getNodeClientProvider, provideNodeClients }
