// utils method
// @NOTE 2019-12-25
// some of those utils there are bound too tight with Koa should
// be some where else, and resolver really should be GENERIC node code base

const debug = require('debug')
const MODULE_NAME = 'jsonql-resolver'
const { getDocLen, packError } = require('jsonql-utils')
const { CONTENT_TYPE, SUCCESS_STATUS } = require('jsonql-constants')
const jsonqlErrors = require('jsonql-errors')

/**
 * return the debug method
 * @param {string} name the base name
 * @return {function} the debug function
 */
const getDebug = function(name) {
  return debug(MODULE_NAME).extend(name)
}

// The following are moved back from utils

/**
 * Handle the output
 * @param {object} opts configuration
 * @return {function} with ctx and body as params
 */
const handleOutput = function(opts) {
  return function(ctx, body) {
    ctx.size = getDocLen(body)
    ctx.type = opts.contentType
    ctx.status = SUCCESS_STATUS
    ctx.body = body
  }
}

/**
 * this was in the jsonql-utils then move back to koa
 * but now make it stand alone here
 * use the ctx to generate error output
 * V1.1.0 we render this as a normal output with status 200
 * then on the client side will check against the result object for error
 * @param {object} ctx context
 * @param {number} code 404 / 500 etc
 * @param {object} e actual error
 * @param {string} message if there is one
 * @param {string} name custom error class name
 */
const ctxErrorHandler = function(ctx, code, e, message = '') {
  const render = handleOutput({contentType: CONTENT_TYPE})
  // debug('[ctxErrorHandler]', code, e, message)
  let name;
  if (typeof code === 'string') {
    name = code;
    code = jsonqlErrors[name] ? jsonqlErrors[name].statusCode : -1
  } else {
    // debug(`[ctxErrorHandler] using getErrorByStatus`)
    name = jsonqlErrors.getErrorByStatus(code)
  }
  // debug(`[ctxErrorHandler.name]`, name)
  // preserve the message
  if (!message && e && e.message) {
    message = e.message;
  }
  return render(ctx, packError(e, name, code, message))
}

const isFunc = fun => typeof fun === 'function'


module.exports = {
  isFunc,
  getDebug,
  handleOutput,
  ctxErrorHandler
}
