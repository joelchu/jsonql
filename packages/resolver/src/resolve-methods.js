// this was in the core-middleware now make this standalone for use in
// two middlewares
const {
  JsonqlResolverNotFoundError
} = require('jsonql-errors')
const { 
  provideUserdata 
} = require('@jsonql/security')
const { 
  MODULE_TYPE, 
  CACHE_STORE_PROP_KEY
} = require('jsonql-constants')
const {
  extractArgsFromPayload,
  findFromContract
} = require('jsonql-utils')
// local 
const { 
  getCacheResolver,
  cacheResolver 
} = require('./cache')

const { getDebug } = require('./utils')
const { searchResolvers, requireEsModule } = require('./search-resolvers') // importFromModule, 
const { validateAndCall } = require('./validate-and-call')
const { getNodeClientProvider } = require('./client')

const debug = getDebug('resolve-method')

/**
 * provide the require param then return the resolver function for use
 * @param {string} resolverName name of resolver
 * @param {string} type of resolver
 * @param {object} contract the contract
 * @param {object} opts configuration
 * @return {function} resolver
 */
const getResolverAction = (resolverName, type, contract, opts) => {
  try {
    const { sourceType } = contract
    if (sourceType === MODULE_TYPE) {
      const pathToResolver = findFromContract(type, resolverName, contract)
      debug('call requireEsModule', resolverName, pathToResolver)
      return requireEsModule(pathToResolver)
    }

    return require(searchResolvers(resolverName, type, opts, contract))
  } catch(e) {
    throw new JsonqlResolverNotFoundError(e)
  }
}

/**
 * @TODO cache resolver 
 * @param {string} resolverName name of resolver
 * @param {string} type of resolver
 * @param {object} contract the contract
 * @param {object} opts configuration
 * @return {promise} resolve the resolver
 */
const getResolver = (resolverName, type, contract, opts) => {
  const key = [type, resolverName].join('-')
  const store = opts[CACHE_STORE_PROP_KEY] // setup this up on the otherside
  let resolver, cacheFn
  
  if (store) {
    resolver = getCacheResolver(key, store)
    if (resolver !== false) {
      debug('[getResolver] return from cache')

      return Promise.resolve(resolver)
    }
    cacheFn = cacheResolver(store)
  }
  try {
    const fn = getResolverAction(resolverName, type, contract, opts)
    const provider = getNodeClientProvider(opts)

    return provider(fn)
      .then(_resolver => {
        if (cacheFn) {
          return cacheFn(key, _resolver)
        }
        return _resolver
      })
  } catch(e) {
    return Promise.reject(e)
  }
}

/**
 * A new method breaking out the get resolver and prepare code
 * then make the original resolveMethod as a koa render handler
 * @param {object} opts configuration
 * @param {string} type of resolver
 * @param {string} resolverName name of resolver
 * @param {object} payload to pass to resolver
 * @param {object} contract contract json
 * @param {*} [userdata=false] if there is any
 * @return {*} result - process result from resolver
 */
const executeResolver = (opts, type, resolverName, payload, contract, userdata = false) => {
  
  const args = extractArgsFromPayload(payload, type)
  
  // inject the node client if any
  // @0.9.0 change everything to promise
  return getResolver(resolverName, type, contract, opts)
    .then(fn => validateAndCall(
        provideUserdata(fn, userdata), // always call this one even auth is false
        args,
        contract,
        type,
        resolverName,
        opts)
    )
    .then(result => {
      // @TODO if we need to check returns in the future
      debug('called and now serve up', result)
      return result
    })
}

// named export instead
module.exports = {
  getResolver,
  getResolverAction,
  executeResolver
}
