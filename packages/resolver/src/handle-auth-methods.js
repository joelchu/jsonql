// Auth methods handler
const {
  AUTH_TYPE,
  QUERY_ARG_NAME,
  UNAUTHORIZED_STATUS
} = require('jsonql-constants')
const {
  JsonqlResolverNotFoundError,
  JsonqlValidationError,
  JsonqlAuthorisationError,
  getErrorNameByInstance,
  UNKNOWN_ERROR
} = require('jsonql-errors')
const { packResult } = require('jsonql-utils')
// const { validateAsync } = require('jsonql-params-validator')

const { getDebug, ctxErrorHandler, handleOutput } = require('./utils')
const { searchResolvers } = require('./search-resolvers')
const { validateAndCall } = require('./validate-and-call')

const debug = getDebug('public-method-middleware')

/**
 * if useJwt = true then use the jsonql-jwt version
 * @param {object} config configuration
 * @param {object} contract contract.json
 * @return {function} the correct handler
 */
const getLocalValidator = (config, contract) => {
  const { validatorHandlerName } = config
  // if you explicitly set it to false then it will be ignore
  if (!validatorHandlerName) {
    return false
  }
  // @BUG when this searchResolvers throw error the next call can not use instanceof to check the Error?
  // @TODO there is no need to pass the type here, it will always be the AUTH_TYPE
  let validatorFnPath = searchResolvers(validatorHandlerName, AUTH_TYPE, config, contract)

  return require(validatorFnPath)
}

/**
 * This is move from the auth middleware to handle the ISSUER_NAME and LOGOUT_NAME
 * They both are always publicly available
 * @param {object} ctx koa context
 * @param {object} payload it send
 * @param {object} opts configuration
 * @param {object} contract to match against
 */
const handleAuthMethods = async function(ctx, payload, opts, contract) {
  try {
    const { loginHandlerName } = opts
    // @1.1.0 @TODO the resolverName can overwrite the stock name ?
    // @TODO look at how to deal with the OAuth validation
    // @TODO here we intercept the result and add extra output if the useJwt is enabled
    const issuerFnPath = searchResolvers(loginHandlerName, AUTH_TYPE, opts, contract)
    const issuerFn = require(issuerFnPath)

    const result = await validateAndCall(
      issuerFn,
      payload[QUERY_ARG_NAME],
      contract,
      AUTH_TYPE,
      loginHandlerName,
      opts
    )

    debug(`${loginHandlerName} result: `, result)
    // this might create a problem? What if there is nothing return but that's unlikely
    if (!result) {
      throw new JsonqlAuthorisationError('login failed')
    }
    return handleOutput(opts)(ctx, packResult(result))
  } catch(e) {
    const errorName = getErrorNameByInstance([
      JsonqlResolverNotFoundError,
      JsonqlAuthorisationError,
      JsonqlValidationError
    ], e)

    debug('handleAuthMethods', e, errorName)

    if (errorName === UNKNOWN_ERROR) {
      return ctxErrorHandler(ctx, UNAUTHORIZED_STATUS, e)
    }
    return ctxErrorHandler(ctx, errorName, e)
  }
}


module.exports = {
  getLocalValidator,
  handleAuthMethods
}
