// search for the resolver location
const { join } = require('path')

const { JsonqlResolverNotFoundError, JsonqlError } = require('jsonql-errors')
const { getPathToFn, findFromContract } = require('jsonql-utils')
const { DEFAULT_RESOLVER_IMPORT_FILE_NAME, MODULE_TYPE } = require('jsonql-constants')

const { getDebug } = require('./utils')

const debug = getDebug('search-resolvers')
const prod = process.env.NODE_ENV === 'production'

/**
 * @TODO we might have to change to the contract folder instead
 * New for ES6 module features
 * @param {string} resolverDir resolver directory
 * @param {string} type of resolver
 * @param {string} resolverName name of resolver
 * @return {function} the imported resolver
 */
function importFromModule(resolverDir, type, resolverName) {
  debug('[importFromModule]', resolverDir, type, resolverName)

  const resolvers = require( join(resolverDir, DEFAULT_RESOLVER_IMPORT_FILE_NAME) )
  return resolvers[ [type, resolverName].join('') ]
}

/**
 * Try to use the esm module to require the module directly
 * @param {string} pathToResolver path to resolver from contract
 * @return {*} resolver function on success
 */
function requireEsModule(pathToResolver) {
  let oldRequire
  try {
    oldRequire = require;
    require = require("esm")(module/*, options*/)
    const obj = require(pathToResolver)

    debug('requireEsModule:40', obj)

    if (typeof obj === 'function') {

      return obj
    } else if (obj.default && typeof obj.default === 'function') {

      return obj.default
    }
    throw new JsonqlError(`Unable to import ES module!`)
  } catch (e) {
    throw new JsonqlError(e)
  } finally {
    // reset the require here
    require = oldRequire
  }
}

/**
 * Wrap the two require together to make it easier 
 * @param {string} pathToResolver path to resolver  
 * @param {string} sourceType MODULE or else
 * @return {function} the resolver function itself  
 */
function getFnBySourceType(pathToResolver, sourceType) {
  return sourceType === MODULE_TYPE ? requireEsModule(pathToResolver)
                                    : require(pathToResolver)
}


/**
 * search for the file starting with
 * 1. Is the path in the contract (or do we have a contract file)
 * 2. if not then resolvers/query/name-of-call/index.js (query swap with mutation)
 * 3. then resolvers/query/name-of-call.js
 * @param {string} name of the resolver
 * @param {string} type of the resolver
 * @param {object} opts options
 * @param {object} contract full version
 * @return {string} the path to function
 */
function searchResolvers(name, type, opts, contract) {
  try {
    const json = typeof contract === 'string' ? JSON.parse(contract) : contract
    let pathToResolver
    pathToResolver = findFromContract(type, name, json)
    if (pathToResolver !== false) {
      
      return pathToResolver
    }

    debug(`pathToResolver not found in contract`, type, name, json)
    // search by running
    pathToResolver = getPathToFn(name, type, opts)
    if (pathToResolver) {
      return pathToResolver
    }
    const debugMsg = `${name} not found! [${opts.name}]`
    
    debug('JsonqlResolverNotFoundError', debugMsg)
  // @TODO why the hell when throw from here the instanceof is not working???
    throw new JsonqlResolverNotFoundError(prod ? 'NOT FOUND!' : debugMsg)
  } catch(e) {
    // debug(`throw again?`, JsonqlResolverNotFoundError)
    throw new JsonqlResolverNotFoundError(e)
  }
}

module.exports = {
  getFnBySourceType,
  searchResolvers,
  importFromModule,
  requireEsModule
}
