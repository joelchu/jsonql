// This was in the resolver-methods before 
// we take this out on it's own, becasue this is the top level 
// call by the koa-middleware, later on we will create another one 
// for other framework 
const {
  JsonqlResolverNotFoundError,
  JsonqlResolverAppError,
  JsonqlValidationError,
  JsonqlAuthorisationError,
  getErrorNameByInstance,
  UNKNOWN_ERROR
} = require('jsonql-errors')

const { executeResolver } = require('./resolve-methods')

const { packResult } = require('jsonql-utils')
const { getDebug, handleOutput, ctxErrorHandler } = require('./utils')
const debug = getDebug('resolver-render-handler')

/**
 * This is the only call that the Koa-middleware is using
 * @param {object} ctx Koa context
 * @param {string} type of calls
 * @param {object} opts configuration
 * @param {object} contract to search via the file name info
 * @return {mixed} depends on the contract
 */
async function resolverRenderHandler(ctx, type, opts, contract) {
  const { payload, resolverName, userdata } = ctx.state.jsonql

  debug('resolverRenderHandler', resolverName, payload, type)
  // There must be only one method call
  const renderHandler = handleOutput(opts)
  // first try to catch the resolve error
  try {
    const result = await executeResolver(opts, type, resolverName, payload, contract, userdata)

    return renderHandler(ctx, packResult(result))
  } catch (e) {
    debug('resolveMethod error', e)
    let errorName = getErrorNameByInstance([
      JsonqlResolverNotFoundError,
      JsonqlAuthorisationError,
      JsonqlValidationError,
      JsonqlResolverAppError
    ], e)
    // if this is an unknown error then it will be JsonqlResolverAppError
    if (errorName === UNKNOWN_ERROR) {
      errorName = 'JsonqlResolverAppError'
    }
    // @BUG the JsonqlResolverNotFoundError should have a statusCode but disappeared???
    return ctxErrorHandler(ctx, errorName, e)
  }
}

module.exports = { resolverRenderHandler }