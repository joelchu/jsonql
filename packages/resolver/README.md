# jsonql-resolver

This module is not for direct use. It's part of the sub-module for our jsonql tools.

Please check [jsonql.org](http://jsonql.org) for more information.

---

Joel Chu (c) 2019
