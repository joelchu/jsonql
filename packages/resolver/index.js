// The final export API
const { searchResolvers } = require('./src/search-resolvers')
const { validateAndCall } = require('./src/validate-and-call')
const {
  getResolver,
  executeResolver
} = require('./src/resolve-methods')
const { 
  resolverRenderHandler 
} = require('./src/resolver-render-handler')
const {
  getLocalValidator,
  handleAuthMethods
} = require('./src/handle-auth-methods')
// added in 1.2.0
const { 
  getSocketInterceptor,
  getCacheSocketInterceptor
} = require('./src/get-socket-interceptor')

// for 1.0.0 release (first on 0.9.8)
// we also export the node client generator parts
// when the middleware is use with socket server
// the client(s) will get pre-config and init during the
// check option phrase then it could be re-use between modules
const {
  injectNodeClient,
  validateClientConfig,
  clientsGenerator,
  getNodeClientProvider
} = require('./src/client')
// 1.0.5 re-export this two method to share with jsonql-koa
const {
  handleOutput,
  ctxErrorHandler
} = require('./src/utils')
// new feature in 1.3.0 
const {
  initCacheStore,
  getCacheResolver, 
  cacheResolver, 
  isNodeCache 
} = require('./src/cache')
const {
  getCompleteSocketResolver
} = require('./src/get-complete-socket-resolver')


module.exports = {
  searchResolvers,
  validateAndCall,
  getResolver,

  handleAuthMethods,
  getLocalValidator,

  getSocketInterceptor,
  getCacheSocketInterceptor,
  
  executeResolver,
  resolverRenderHandler,
  // for node clients
  injectNodeClient,
  validateClientConfig,
  clientsGenerator,
  getNodeClientProvider,

  handleOutput,
  ctxErrorHandler,

  initCacheStore,
  getCacheResolver, 
  cacheResolver, 
  isNodeCache,

  getCompleteSocketResolver
}
