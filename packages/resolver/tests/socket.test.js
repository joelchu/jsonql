// testing the get socket auth interceptor
const test = require('ava')
const fsx = require('fs-extra')
const { join } = require('path')
const debug = require('debug')('jsonql-resolver:test:socket')
const { LOGIN_EVENT_NAME, LOGIN_FN_NAME_PROP_KEY } = require('jsonql-constants')
const { getSocketInterceptor } = require('../index')
const baseDir = join(__dirname, 'fixtures', 'contract', 'socket')

test.before(t => {
  const contract = fsx.readJsonSync(join(baseDir, 'contract.json'))
  t.context.config = {
    resolverDir: join(__dirname, 'fixtures', 'resolvers'),
    contract,
    [LOGIN_FN_NAME_PROP_KEY]: 'login'
  }
})

test(`Just stub it here for the time being`, t => {

  t.true(typeof getSocketInterceptor === 'function')
})

test(`It should able to find socket auth interceptors`, t => {

  const fn = getSocketInterceptor(LOGIN_EVENT_NAME, t.context.config)

  debug('auth interceptor', fn)

  t.true(typeof fn === 'function')

})
