// create config file to generate the auth contract
const { join } = require('path')
const baseDir = join(__dirname)

module.exports = {
  inDir: join(baseDir, 'resolvers'),
  outDir: join(baseDir, 'contract', 'socket'),
  enableAuth: true,
  public: process.env.IS_PUBLIC === 'true'
}
