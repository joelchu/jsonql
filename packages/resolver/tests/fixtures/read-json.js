const fs = require('fs')

const readJson = file => {
  return new Promise((resolver, rejecter) => {
    fs.readFile(file, 'utf8', function (err, data) {
      if (err) {
        return rejecter(err)
      }
      resolver(JSON.parse(data))
    })
  })
}

module.exports = readJson
