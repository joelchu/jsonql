/**
 * Just a dummy socket interface
 * @param {string} msg
 * @return {string} msg
 */
module.exports = function(msg) {
  return 'whatever you said is: ' + msg 
}
