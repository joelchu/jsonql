/**
 * @param {string} name pass a name
 * @return {string} name with prefix
 */
module.exports = function getSomething(name) {
  return `Hello ${name}!`
}
