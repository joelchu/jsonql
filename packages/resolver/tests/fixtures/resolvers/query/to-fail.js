/**
 * This resolver will fail and throw an error
 * @return {*} a variable that didn't exist
 */
module.exports = function toFail() {
  return varNotExisted
}
