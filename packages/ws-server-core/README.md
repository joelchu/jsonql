# jsonql-ws-server-core

> This is the core module for Jsonql WS Web Socket server

This module is not intend to use directly.

Check [jsonql.org](http://jsonql.org) for more information.

## Note

The main functionality of this module is: 

- Setup all the default configuration options 
- Setup the resolver handlers, and prepare the resolver to act
- Setting up the public and private NSP(s) to handle authentication / security 

## Export modules 

This is for our own reference use **ONLY**:

- addProperty
- getContract
- createWsReply
- getDebug
- getNamespace
- extractWsPayload
- nil
- getUserdata
- resolveMethod
- wsServerDefaultOptions
- wsServerConstProps
- jsonqlWsCoreConstants
- jsonqlWsServerCore
- jsonqlWsServerCoreAction

## TODO (toward V.1 release)

- origin / ip checking 
- ~~CSRF~~
- TS check 
- standalone mode features 
- ~~Intercom event handling~~ 

---

[NEWBRAN LTD UK](https://newbran.ch)

[TO1SOURCE CHINA](https://to1source.cn)
