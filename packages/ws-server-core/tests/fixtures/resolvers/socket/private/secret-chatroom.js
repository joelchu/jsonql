const { debug } = require('../../../../../src/utils')
/**
 * @param {string} room room name
 * @param {*} msg message to that room
 * @return {*} depends
 */
module.exports = function secretChatroom(room, msg) {

  debug('secretChatroom.userdata', secretChatroom.userdata)

  let userdata = secretChatroom.userdata
  // @TODO
  return `send ${msg+''} to ${room} room from ${userdata.name}`
}
