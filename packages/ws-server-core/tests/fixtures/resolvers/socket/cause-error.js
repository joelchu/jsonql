// this method will throw an error

/**
 * @param {string} msg a message
 * @return {string} a message but here we throw an error
 */
module.exports = function causeError(msg) {
  causeError.send('something else')
  throw new Error(msg)
}
