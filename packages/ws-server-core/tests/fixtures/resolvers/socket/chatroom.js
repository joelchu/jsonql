// a private method

/**
 *
 * @param {string} msg message
 * @param {number} timestamp for checking the time
 * @return {string} reply
 */
module.exports = function(msg, timestamp) {
  const d = Date.now() - (timestamp || 0)
  return msg + ` took ${d} ms`
}
