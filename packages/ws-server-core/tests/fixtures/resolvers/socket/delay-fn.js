// test this with a Promise return result
/**
 * @param {string} msg a message
 * @param {number} timestamp a timestamp
 */
module.exports = function delayFn(msg, timestamp) {
  // also test the global socket instance
  delayFn.send('I am calling from delayFn')

  return new Promise(resolver => {
    setTimeout(() => {
      resolver(msg + (Date.now() - timestamp))
    }, 1000)
  })
}
