// will test this one with the send property

/**
 * method just for testing the ws
 * @param {string} msg message
 * @param {number} timestamp timestamp
 * @return {string} msg + time lapsed
 */
module.exports = function wsHandler(msg, timestamp) {
  const ts = Date.now()
  wsHandler.send('I am sending a message back from ws', ts)

  return new Promise(resolver => {
    setTimeout(() => {
      resolver(msg + ' - ' +(ts - timestamp))
    }, 1000)
  })
}
