const debug = require('debug')('jsonql-ws-server:test:object');
/**
 * @return {string} a msg
 *
 *
 */
module.exports = function myFn() {
  // debug(myFn.ctx);
  return myFn.ctx() // 'This is a fn';
}
