// Take the code out from the run-resolver to share between other method
// const { JsonqlValidationError } = require('jsonql-errors')
const { packError } = require('jsonql-utils')
const { isNotEmpty, validateAsync } = require('jsonql-params-validator')
const { getRainbowDebug, createWsReply } = require('../share/helpers')
const {
  INTERCOM_RESOLVER_NAME,
  ACKNOWLEDGE_REPLY_TYPE,
  ERROR_KEY
} = require('jsonql-constants')
const debug = getRainbowDebug('share:resolver-methods')


/**
 * Take this out for the send method to use
 * @param {*} args the input
 * @param {object} params map for checking
 * @return {promise} resolve or reject after validation
 */
function validateInput(args, params) {
  return validateAsync(args, params, true)
  /* It should be fix by now that it will throw the correct error
    .catch(err => {
      throw new JsonqlValidationError(`validateInput`, err)
    })
  */
}


/**
 * the final step to delivery the result to the client
 * @param {function} deliverFn framework specific method to delivery
 * @param {string} resolverName that perform this method
 * @param {*} result from the resolver process
 * @param {string} type of the message, it could chanage depends on where it send
 * @return {void}
 */
function handleResult(deliverFn, resolverName, result, type) {
  debug('resolver return result', result)
  // decide if we need to call the cb or not here
  if (isNotEmpty(result)) {
    deliverFn(createWsReply(type, resolverName, result))
  }
  return result // just forward it
}


/**
 * the final step to delivery the error to the client
 * @param {function} deliverFn framework specific method to delivery
 * @param {string} resolverName that perform this method
 * @param {*} err the error 
 * @return {void}
 */
function handleError(deliverFn, resolverName, err) {
  const payload = createWsReply(ERROR_KEY, resolverName, packError(err))
  
  debug('--> CATCH RESOLVER ERROR <--')
  debug(ERROR_KEY, resolverName, payload)
  
  deliverFn(payload)
}


/**
 * The final step to delivery the message
 * @param {function} deliverFn framework specific method to delivery 
 * @param {string} resolverName that perform this method
 * @param {*} result from the resolver process
 * @param {string} type what type of message
 * @return {promise} resolve the final result
 */
function deliverMsg(deliverFn, resolverName, result, type = ACKNOWLEDGE_REPLY_TYPE) {
  
  return Promise
    .resolve(result)
    .then(result => handleResult(deliverFn, resolverName, result, type))  
}

/**
 * Wrapper method to send back a intercom reply 
 * @param {function} deliverFn framework specific send method  
 * @param {*} payload things we want to send back 
 * @param {string} type type of intercom event 
 * @return {void} 
 */
function deliverIntercomMsg(deliverFn, payload, type) {
  deliverFn(createWsReply(type, INTERCOM_RESOLVER_NAME, payload))
}


module.exports = {
  validateInput,
  deliverMsg,
  handleError,
  deliverIntercomMsg
}
