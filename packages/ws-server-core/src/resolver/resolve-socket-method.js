// search for the resolver location
const { CACHE_STORE_PROP_KEY } = require('jsonql-constants')
const { JsonqlResolverAppError } = require('jsonql-errors')
const { getCompleteSocketResolver } = require('jsonql-resolver')
const { getInjectors } = require('./get-injectors')
const { getRainbowDebug } = require('../share/helpers')

const debug = getRainbowDebug('share:resolve-method')
// it's the same all the time, so we just call it here once 
const injectorFns = getInjectors()

/**
 * similiar to the one in Koa-middleware without the ctx
 * @param {string} namespace connected namespace
 * @param {string} resolverName name to call
 * @param {function} deliverFn framework specific method
 * @param {array} args arguments
 * @param {object} opts for search later
 * @param {object} ws the WebSocket instance
 * @param {object} [userdata=false] userdata
 * @return {promise} depends on the contract
 */
const resolveSocketMethod = function(
  namespace,
  resolverName,
  deliverFn,
  args, // this is the clean value from validateAsync
  opts,
  ws,
  userdata) {
  // resolverName, opts, key = null, store = null
  const _params = [resolverName, opts]

  if (opts.enableCacheResolver === true) {
    const key = [namespace, resolverName].join('-')
    const store = opts[CACHE_STORE_PROP_KEY]
    _params.push(key, store)
  }

  const constructorFn = Reflect.apply(getCompleteSocketResolver, null, _params) 

  const params = [ deliverFn, namespace, resolverName, ws, userdata, opts ]
  // here although we always passs the params but inside will return the cache version 
  // if there is one
  const resolverFn = constructorFn(injectorFns, params)
  
  debug('resolveSocketMethod resolver', typeof resolverFn, resolverFn, args)
  
  return new Promise((resolver, rejecter) => {
    try {
      resolver( Reflect.apply(resolverFn, null, args) )
    } catch(err) {
      debug('Error happen here in side the resolveSocketMethod', err)
      // it's not nice, but we catch error here then we won't affect 
      // other error like validation error might get throw earlier in the process
      rejecter(new JsonqlResolverAppError(resolverName, err))
    }
  })
}

// we only need to export one method
module.exports = { resolveSocketMethod }
