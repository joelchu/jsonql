// all these methods were in the share folder before
// now all group under the resolver because they are all related to resolver 
const {
  validateInput,
  deliverMsg,
  handleError,
  deliverIntercomMsg
} = require('./resolver-methods')
const { 
  resolveSocketMethod 
} = require('./resolve-socket-method')


module.exports = {
  validateInput,
  deliverMsg,
  handleError,
  deliverIntercomMsg,
  resolveSocketMethod
}