// re-export here
const {
  SOCKET_STATE_KEY,
  NO_TOKEN_ERR_MSG
} = require('./options/constants')
const {
  initWsServerOption,
  checkSocketServerType,
  wsServerCheckConfiguration,
  wsDefaultOptions,
  wsConstProps
} = require('./options')
const {
  jsonqlWsServerCoreAction,
  jsonqlWsServerCore
} = require('./core')
// rename it before export
const wsServerDefaultOptions = wsDefaultOptions
const wsServerConstProps = wsConstProps
// re-export
module.exports = {
  SOCKET_STATE_KEY,
  NO_TOKEN_ERR_MSG,
  // rename them
  wsServerDefaultOptions,
  wsServerConstProps,
  // rest of the exports
  checkSocketServerType,
  initWsServerOption,
  wsServerCheckConfiguration,
  // the two top level core methods to create the websocket server
  jsonqlWsServerCoreAction,
  jsonqlWsServerCore
}
