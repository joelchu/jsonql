// create a hello world method to test the installation
const { deliverMsg } = require('../resolver/resolver-methods')
const { HELLO_FN, HELLO } = require('jsonql-constants')

function handleHello(deliverFn, connectedNamespace, userdata) {
  
  // change the return message 
  const world = userdata && userdata.name ? HELLO.replace('world', userdata.name) : HELLO
  const msg = `From: ${connectedNamespace}, ${world}`

  deliverMsg(deliverFn, HELLO_FN, msg)
}

module.exports = { handleHello }