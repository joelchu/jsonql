// The top level socket handler and redistribute task
const {
  INTERCOM_RESOLVER_NAME,
  NSP_GROUP,
  HELLO_FN
} = require('jsonql-constants')
const { getResolverFromPayload } = require('jsonql-utils')

const { handleHello } = require('./handle-hello')
const { handleInterCom } = require('./handle-intercom')
const { handleNspResolvers } = require('./handle-nsp-resolvers')
const { handleUnknownPayload } = require('./handle-unknown-payload')

const { getRainbowDebug, matchResolverByNamespace } = require('../share/helpers')

const debug = getRainbowDebug('handles:get-socket-handler', 'whatever')

/**
 * This is the generic methods that will be wrap inside the on.message (for ws)
 * all it takes it how to delivery the message back
 * @param {object} config full configuration
 * @param {object} ws The socket server instance for injection
 * @param {function} deliverFn a wrapper method to deliver the message
 * @param {object} req the request object
 * @param {string} connectedNamespace which namespace is this calling from
 * @param {object} payload from the message call
 * @param {object|boolean} userdata if any
 * @return {void} unless we throw an error
 */
function getSocketHandler(config, ws, deliverFn, req, connectedNamespace, payload, userdata) {
  const nspGroup = config.nspInfo[NSP_GROUP]
  const resolverName = getResolverFromPayload(payload)
  const args = payload[resolverName]
  // break up from previous setup so we get a cache version
  const nspHandlerFn = handleNspResolvers(deliverFn, ws, config)

  // it might be the special internal event?
  switch (true) {
    case resolverName === INTERCOM_RESOLVER_NAME:
      // debug('getSocketHandler:intercom')
      // We don't need to send anything back // @TBC do we need the config
      return handleInterCom(config, ws, deliverFn, req, connectedNamespace, args, userdata)
    case resolverName === HELLO_FN: // basically we hijack it here and no need to care about the contract setup
      
      return handleHello(deliverFn, connectedNamespace, userdata)
    default:
      const contractParam = matchResolverByNamespace(resolverName, connectedNamespace, nspGroup)
      debug('getSocketHandler:default', contractParam)

      if (contractParam === false) {
        // just return an error
        return handleUnknownPayload(deliverFn, resolverName, args)
      }
      // we change the interface a bit 
      return nspHandlerFn(
        connectedNamespace, 
        resolverName, 
        args.args, 
        contractParam.params, 
        userdata
      )
  }
}

module.exports = { getSocketHandler }
