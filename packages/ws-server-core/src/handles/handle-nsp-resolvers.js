// make this more generic and share it to create resolver for each nsp
// @TODO this property still not create correctly
// const { TIMESTAMP_PARAM_NAME } = require('jsonql-constants')
// get the resolver to handle the request
const { 
  validateInput, 
  deliverMsg, 
  handleError, 
  resolveSocketMethod 
} = require('../resolver')

const { getRainbowDebug } = require('../share/helpers')
const debug = getRainbowDebug('handles:handle-nsp-resolver', 'x')
// for caching
let nspHandlerFn

/**
 * We pass those params that is not change once it's set to this method 
 * Then we get back the actual handler that will return the resolver to take the call
 * 
 * @param {function} deliverFn the final delivery message method
 * @param {object} ws the socket instance get add to the resolver as property
 * @param {object} opts configuration
 * @return {function} 
 */
function handleNspResolvers(deliverFn, ws, opts) {
  // check the cache method first
  /*
  @BUG when we use this cache method, the reply broken halfway 
  if (typeof nspHandlerFn === 'function') {
    debug('return the cached nspHandlerFn')
    return nspHandlerFn
  }
  */
  /** 
   * The actual method to action on the execution of the resolver
   * @param {string} namespace connected namespace
   * @param {string} resolverName name of resolver
   * @param {*} args from payload
   * @param {object} params from contract.json
   * @param {object} userdata userdata
   * @return {promise} resolve the result
   */
  return (namespace, resolverName, args, params, userdata) => (
    validateInput(args, params)
      .then(_args => resolveSocketMethod(
          namespace,
          resolverName,
          deliverFn,
          _args, // this is the clean value from validateAsync
          opts,
          ws,
          userdata
        )
      )
      .then(result => {
        debug(`${resolverName} return result`, result)

        return deliverMsg(deliverFn, resolverName, result)
      })
      .catch(err => {
        debug('error happen inside the nspHandlerAction', err)
        // By the time the error gets here already become JsonqlError???
        // rdebug('in the catch', err)
        handleError(deliverFn, resolverName, err)
      })
    )
}

module.exports = { handleNspResolvers }
