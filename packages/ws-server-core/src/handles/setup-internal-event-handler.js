// this is the inter communication event setup 
// what we need to do is take the namespace / contract 
// then create event caller here 
// then on the framework part they will listen to these events 
// in fact, both can be completely do here 
// 1) also we should implement the suspend feature as well
// 2) also we will only deal with the socket part here 
//  where the http part will deal with by other part of the system 
const debug = require('debug')('jsonql-ws-server:handles:setup-internal')
const { createInternalEvtName } = require('../share/helpers')

/**
 * This is the same in both cases therefore we just pass callback to handle it 
 * @param {array} namespaces from nspInfo 
 * @param {object} socket from contract  
 * @param {function} callback
 * @return {void} 
 */
function generateEvtNames(namespaces, socket, callback) {
  namespaces.forEach(namespace => {
    for (let resolverName in socket) {
      let evtName = createInternalEvtName(namespace, resolverName)
      callback(evtName, resolverName)
    }
  })
}

/**
 * Create the internal call via the Event system 
 * This should get call in the handle-nsp-resolvers
 * @param {*} opts configuration 
 * @return {void}
 */
function setupInternalEventCallers(opts) {
  const { nspInfo, eventEmitter, contract, log } = opts
  const { namespaces } = nspInfo
  const { socket } = contract
  // @TODO might need a few more props here
}

/**
 * Create the internal listener via the Event system 
 * @param {*} opts configuration 
 * @return {void}
 */
function setupInternalEventListeners(opts) {

  return debug('setupInternalEventListeners')

  // debug('initWsServerOption', opts)
  const { nspInfo, eventEmitter, contract, log } = opts
  const { namespaces } = nspInfo
  const { socket } = contract
  // run the create event name for each 
  generateEvtNames(namespaces, socket, (evtName, resolverName) => {
    // we might need to create a new type of delivery struture ? 
    eventEmitter.$only(evtName, function(args) {
      log(`resolverName`, resolverName, args)
    })
  })
}

module.exports = {  
  createInternalEvtName,
  setupInternalEventListeners,
  setupInternalEventCallers
}