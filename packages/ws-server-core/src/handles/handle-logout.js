const { getCacheSocketInterceptor } = require('jsonql-resolver')
const { 
  INTERCOM_RESOLVER_NAME,
  LOGOUT_EVENT_NAME,
  CACHE_STORE_PROP_KEY
} = require('jsonql-constants')
const { getMethodParams } = require('../share/helpers')
const { getDebug } = require('../share/helpers')
const debug = getDebug('handle-intercome:logout')

// const WS_EXIT_ID = 1

/**
 * This will change based on the WS spec
 * @TODO handle the client logout event, it will send back one last message before exit
 * @param {object} config configuraton 
 * @param {object} ws the socket instance 
 * @param {function} deliverFn framework specific to send out message
 * @param {object} req the request object
 * @param {string} connectedNamespace the namesapce its connected to 
 * @param {array} args extract from the payload, the first is the event name
 * @return {void} nothing return just send back a reply
 */
function handleLogout(config, ws, deliverFn, req, connectedNamespace, args) {
  debug(`handleLogout called`, args)
  const { contract } = config
  const resolverName = config[LOGOUT_FN_NAME_PROP_KEY]
  const params = getMethodParams(contract, resolverName)

  const key = [INTERCOM_RESOLVER_NAME, LOGOUT_EVENT_NAME].join('-')
  const store = config[CACHE_STORE_PROP_KEY]

  const interceptorFn = getCacheSocketInterceptor(LOGOUT_EVENT_NAME, config, key, store)

  if (isFunc(interceptorFn) && params) {
    // @TODO need to figure out how to extract the params here
    return validateInput(args, params)
      .then(_args => {
        return Reflect.apply(interceptorFn, null, _args)
      })
      .catch(err => {
        debug('[handleLogout] error', err)
      })
      // what if the argument not pass the validation? 
  } else {
    debug(`[handleLogout] resolver not found`, interceptorFn, `or params not found`, params)
  }
}

module.exports = { handleLogout }
