const { getDebug } = require('../share/helpers')
const debug = getDebug('handle-intercome')
const { handleError } = require('../resolver/resolver-methods')
const { ERROR_KEY } = require('jsonql-constants')

/**
 * @TODO not sure what to do with this one, perhaps just log it
 * @param {object} config configuraton 
 * @param {object} ws the socket instance 
 * @param {function} deliverFn framework specific to send out message
 * @param {object} req the request object
 * @param {string} connectedNamespace the namesapce its connected to 
 * @param {array} args extract from the payload, the first is the event name
 * @param {*} userdata it could be empty most of the time
 * @return {void} nothing return just send back a reply
 */
function handleUnknownPayload(deliverFn, resolverName, args) {
  debug(`Handle unknown payload called with argument`, args)

  handleError(deliverFn, resolverName, 'UNKNOWN')
}

module.exports = { handleUnknownPayload }
