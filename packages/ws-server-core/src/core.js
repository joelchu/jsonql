// These methods were inside the index.js
// which make it hard to understand why it's there
// these are the core functionality for the other module
// to create the actual Web Socket Server
const { JsonqlError } = require('jsonql-errors')
const {
  initWsServerOption,
  wsServerCheckConfiguration
} = require('./options')


/**
 * @0.4.0 we breaking up the config and init server in two fn
 * Then when we need to combine with other modules, we only use the init
 * Also there might be a chance that we need to merge the contract?
 * @param {object} obj combine several properties in one
 * @param {object} obj.opts checked configuration options
 * @param {object} obj.server the http server instance
 * @param {object} obj.setupSocketServer reverse passing the wsCreateServer fn from outter module
 * @return {promise} checked config
 */
function jsonqlWsServerCoreAction({ opts, server, setupSocketServer }) {

  return initWsServerOption(opts)
    .then(config => (
      Reflect.apply(setupSocketServer, null, [config, server])
    ))
    .catch(err => {
      console.error('Init jsonql Web Socket server error', err)
      throw new JsonqlError('jsonqlWsServer', err)
    })
}


/**
 * This will take the two methods and return a method to generate the web socket server
 * This is for standalone server type which unlikely to get use often
 * @param {function} setupSocketServer generate the base server
 * @param {object} checkMaps because each framework has it's own property we should pass back the map here
 * @return {function} the method that accept the config and server instance to run
 */
function jsonqlWsServerCore(setupSocketServer, checkMaps = {}) {

  return (config, server) => (
    wsServerCheckConfiguration(config, checkMaps)
      .then(opts => ({ opts, server, setupSocketServer }))
      .then(jsonqlWsServerCoreAction)
  )
}

module.exports = {
  jsonqlWsServerCoreAction,
  jsonqlWsServerCore
}
