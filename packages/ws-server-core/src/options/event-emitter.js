// we are going extend this class and have it's own name and stuff

const EventEmitter = require('@to1source/event')

class WsServerEventEmitter extends EventEmitter {

  constructor(logger) {
    super({ logger })
  }

  get $name() {
    return 'ws-server-core-event-emitter'
  }
}


module.exports = { WsServerEventEmitter }