// break out from the index.js to keep property in one place
// and methods in another
const { join } = require('path')
const { createConfig } = require('jsonql-params-validator')
const {
  HSA_ALGO,

  ALIAS_KEY,
  PUBLIC_KEY,
  PRIVATE_KEY,

  STRING_TYPE,
  BOOLEAN_TYPE,
  NUMBER_TYPE,
  OBJECT_TYPE,
  ARRAY_TYPE,

  DEFAULT_RESOLVER_DIR,
  DEFAULT_CONTRACT_DIR,
  DEFAULT_KEYS_DIR,

  SOCKET_TYPE_PROP_KEY,
  SOCKET_TYPE_SERVER_ALIAS,

  DISCONNECT_FN_NAME,
  SWITCH_USER_FN_NAME,
  LOGIN_FN_NAME,
  LOGOUT_FN_NAME,

  JS_WS_NAME,
  CSRF_HEADER_KEY,

  APP_DIR_PROP_KEY,
  ALGORITHM_PROP_KEY,
  AUTH_TO_PROP_KEY,
  ENABLE_AUTH_PROP_KEY,
  USE_JWT_PROP_KEY,
  RESOLVER_DIR_PROP_KEY,
  CONTRACT_DIR_PROP_KEY,
  KEYS_DIR_PROP_KEY,
  INIT_CONNECTION_FN_NAME_PROP_KEY,
  LOGIN_FN_NAME_PROP_KEY,
  LOGOUT_FN_NAME_PROP_KEY,
  DISCONNECT_FN_NAME_PROP_KEY,
  SWITCH_USER_FN_NAME_PROP_KEY,
  PUBLIC_FN_DIR_PROP_KEY,
  PRIVATE_FN_DIR_DROP_KEY,
  ALLOW_ORIGIN_PROP_KEY,
  SERVER_INIT_OPT_PROP_KEY,
  CSRF_PROP_KEY,
  ENABLE_CACHE_RESOLVER_PROP_KEY,
  PUBLIC_KEY_NAME_PROP_KEY,
  PRIVATE_KEY_NAME_PROP_KEY,
  CONTRACT_PROP_KEY,
  SECRET_PROP_KEY,
  PUBLIC_NAMESPACE_PROP_KEY,
  PRIVATE_NAMESPACE_PROP_KEY,
  STANDALONE_PROP_KEY,
  PUBLIC_KEY_NAME,
  PRIVATE_KEY_NAME
} = require('jsonql-constants')
// default root folder
const dirname = process.cwd()

// base options
const wsBaseOptions = {
  [APP_DIR_PROP_KEY]: createConfig('', [STRING_TYPE]), // just matching the Koa but not in use at the moment
  // @TODO this will be moving out shortly after the test done
  // RS256 this will need to figure out how to distribute the key
  [ALGORITHM_PROP_KEY]: createConfig(HSA_ALGO, [STRING_TYPE]),
  [AUTH_TO_PROP_KEY]: createConfig(15000, [NUMBER_TYPE]),
  // we require the contract already generated and pass here
  // contract: createConfig({}, [OBJECT_TYPE]), this been causing no end of problem, we don't need it!
  [ENABLE_AUTH_PROP_KEY]: createConfig(false, [BOOLEAN_TYPE]),
  // this option now is only for passing the key
  // this cause a bug because this option is always BOOLEAN and STRING TYPE!
  
  // update this options to match the jsonql-koa 1.4.10
  [RESOLVER_DIR_PROP_KEY]: createConfig(join(dirname, DEFAULT_RESOLVER_DIR), [STRING_TYPE]),
  [CONTRACT_DIR_PROP_KEY]: createConfig(join(dirname, DEFAULT_CONTRACT_DIR), [STRING_TYPE]),
  // we only want the keys directory then we read it back
  [KEYS_DIR_PROP_KEY]: createConfig(join(dirname, DEFAULT_KEYS_DIR), [STRING_TYPE]),
  // @0.6.0 expect this to be the path to the interComEventHandler to handle the INTER_COM_EVENT_NAMES
  [INIT_CONNECTION_FN_NAME_PROP_KEY]: createConfig('', [STRING_TYPE]),
  [LOGIN_FN_NAME_PROP_KEY]: createConfig(LOGIN_FN_NAME, [STRING_TYPE]),
  [LOGOUT_FN_NAME_PROP_KEY]: createConfig(LOGOUT_FN_NAME, [STRING_TYPE]),
  [DISCONNECT_FN_NAME_PROP_KEY]: createConfig(DISCONNECT_FN_NAME, [STRING_TYPE]),
  [SWITCH_USER_FN_NAME_PROP_KEY]: createConfig(SWITCH_USER_FN_NAME, [STRING_TYPE]),
  // this is for construct the namespace
  // this should be following what is the contract-cli using
  [PUBLIC_FN_DIR_PROP_KEY]: createConfig(PUBLIC_KEY, [STRING_TYPE]),
  [PRIVATE_FN_DIR_DROP_KEY]: createConfig(PRIVATE_KEY, [STRING_TYPE, BOOLEAN_TYPE]),
  [STANDALONE_PROP_KEY]: createConfig(false, [BOOLEAN_TYPE]),
  // check the origin if we can
  [ALLOW_ORIGIN_PROP_KEY]: createConfig(['*'], [ARRAY_TYPE]),
  [SERVER_INIT_OPT_PROP_KEY]: createConfig({}, [OBJECT_TYPE]),
  // placeholder for now
  [CSRF_PROP_KEY]: createConfig(CSRF_HEADER_KEY, [STRING_TYPE]),
  // if we want to cache the resolver or not 
  [ENABLE_CACHE_RESOLVER_PROP_KEY]: createConfig(true, [BOOLEAN_TYPE])
}

// we have to create a standlone prop to check if the user pass the socket server config first
const socketAppProps = {
  [SOCKET_TYPE_PROP_KEY]: createConfig(JS_WS_NAME, [STRING_TYPE], {
    [ALIAS_KEY]: SOCKET_TYPE_SERVER_ALIAS
  })
}

// merge this together for use
const wsDefaultOptions = Object.assign(wsBaseOptions, socketAppProps)

const wsConstProps = {
  [USE_JWT_PROP_KEY]: true, // this is always set to true for now
  [PUBLIC_KEY_NAME_PROP_KEY]: false,
  [PRIVATE_KEY_NAME_PROP_KEY]: false,
  [CONTRACT_PROP_KEY]: false,
  [SECRET_PROP_KEY]: false,
  [PUBLIC_NAMESPACE_PROP_KEY]: PUBLIC_KEY,
  [PRIVATE_NAMESPACE_PROP_KEY]: PRIVATE_KEY,
  // @TODO they might get replace with the name above
  [PUBLIC_KEY_NAME]: false, 
  [PRIVATE_KEY_NAME]: false
}

module.exports = {
  wsDefaultOptions,
  wsConstProps,
  socketAppProps
}
