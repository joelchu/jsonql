# jsonql-ws-client-core

This was call `jsonql-ws-client` and now rename to `jsonql-ws-client-core`. The old module has been deprecated and no longer under development.

This module is **NOT** for direct use within your project. This is the foundation of all the
framework specific web socket (like) modules.

Please check the following

- [@jsonql/ws](https://npmjs.com/package/@jsonql/ws) for [WebSocket](https://npmjs.com/package/ws) client / server
- [@jsonql/socketio](https://npmjs.com/package/@jsonql/socketio) for [Socket.io](https://npmjs.com/package/socketio) client / server
- ~~@jsonql/primus for Primus client / server~~ (planning stage)  

## Architecture

The entire sequence of create a Web Socket client:

check configuration --> capture contract --> generate resolvers (event callers) --> connect to socket server --> create event listeners 

Therefore the upper level part of the client, for example `WebSocket` only provide the framework sepecific 
interface. Then this package will do the rest. 


## Create combine client 

2020-03-13 

After a long period of struggle with the architect problem. Comes to a conclusion - it was shit.

Instead of think of it from a host / slave structure. Should take a higher approach to look at it like 
onion skins. 

At the moment, the ws client module export back the `configCheckMap` and `constProps` then back in here
which is very messy. Instead, I am going to change it to something like this 

```js
import { httpClient, configCheckMap, constProps } from 'jsonql-http-client/module'
import { createCombineClient } from '@jsonql/ws/combine'

const clientConstructor = createCombineClient(httpClient, configCheckMap, constProps)

clientConstructor(config)
  .then(client => {
    // now you will get http-client as well as socket client as one 
  })

```

This will be a much cleaner approach, although you won't see it. But we write it here as a note to somebody else might be facing the same issue (modular, architect). Well, it will be in the book. 

Please check [jsonql.org](https://jsonql.org) for further information

---

MIT

[Joel Chu](https://joelchu.com) (c) 2019

Co-develop by [NEWBRAN LTD](https://newbran.ch) and [to1source CN](https://to1source.cn)
