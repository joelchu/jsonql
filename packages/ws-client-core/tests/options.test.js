// just testing if the options are export correctly
const test = require('ava')
const debug = require('debug')('jsonql-ws-client:test:options')
const { injectToFn, chainFns } = require('jsonql-utils')
const constants = require('../src/options/constants')
const { triggerNamespacesOnError } = require('../src/listener/trigger-namespaces-on-error')
const { checkConfiguration } = require('../src/options')
const { setupStatePropKeys } = require('../src/utils')
const { IS_LOGIN_PROP_KEY, IS_READY_PROP_KEY } = require('jsonql-constants')

test(`Should have a debug logger pass from here`, async t => {
  const opts = { hostname:'http://localhost:3456' }
  const result = await checkConfiguration(opts, {}, { eventEmitter: {name: 'test'}, log: debug })
  result.log(`you should see me here`)
  t.truthy(typeof result.log === 'function')
  t.truthy(result.eventEmitter)
})

test(`Should have the constants in options`, t => {
  t.truthy(constants.MY_NAMESPACE)
  t.is(typeof triggerNamespacesOnError, 'function')
})

test(`test the objDefineProps if it return a new prop`, t => {
  const prop0 = 'prop zero'
  const prop1 = 'prop from fn1'
  const fn1 = (fn, name, prop) => {
    return [
      injectToFn(fn, name, prop),
      'prop1',
      prop1
    ]
  }
  const fn2 = (fn, name, prop) => {
    debug(name, prop, fn)
    return injectToFn(fn, name, prop)
  }
  const exe = chainFns(fn1, fn2)
  const obj = {}

  const fnFinal = exe(obj, 'prop0', prop0)

  t.is(obj.prop0, prop0)
  t.is(obj.prop1, prop1)

})


test(`test the inject prop_keys method`, t => {
  const client = setupStatePropKeys({})

  debug(client)

  t.is(client[IS_LOGIN_PROP_KEY], false)
  t.is(client[IS_READY_PROP_KEY], false)

})