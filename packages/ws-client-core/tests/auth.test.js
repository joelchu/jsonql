// mock a enableAuth client 

// mocking the client generator 
// and only test the internal 
// espcially debug the event system 
const test = require('ava')
const { mockClient, log } = require('./fixtures/lib/mock-client')
const {
  ON_ERROR_FN_NAME,
  ON_LOGIN_FN_NAME
} = require('jsonql-constants')

test.before(async t => {
  t.context.client = await mockClient(true)
})

test.cb(`We should able to get a list of event register via the eventEmitter`, t => {
  
  const client = t.context.client 
  t.plan(2)

  //log('show client', client)
  log(client.verifyEventEmitter())

  t.truthy(client)

  // note it's one name onError that will listen to all the nsp errors
  client[ON_ERROR_FN_NAME] = function(err) {
    log(`OnError callback added`, err)
  }

  // there is only one onReady call now
  
  client[ON_LOGIN_FN_NAME] = function(msg) {
    log(`${ON_LOGIN_FN_NAME} callback added`, msg)
    t.pass()
    t.end()
  }

})




