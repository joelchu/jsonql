// there is a problem with the configuration how create correctly 
const test = require('ava')
const {
  checkSocketClientType,

  createCombineConfigCheck,
  postCheckInjectOpts,
  createRequiredParams
} = require('../index')
const debug = require('debug')('jsonql-ws-client:test:config')

const isF = f => typeof f === 'function'

test(`These functions from options should be exported`, t => {

  t.true(isF(checkSocketClientType))
  t.true(isF(createCombineConfigCheck))
  t.true(isF(postCheckInjectOpts))
  t.true(isF(createRequiredParams))

})

test(`It should able to pass a log method via the checkCombineConfigCheck`, async t => {

  const msg = 'this is a log function'

  const checkFn = createCombineConfigCheck({}, {log: function() {
    return msg 
  }}, true)

  const config = await checkFn({hostname: 'http://localhost', debugOn: true, enableAuth: true})
  
  debug('config result', config)

  t.true(isF(config.log))

  t.is(msg, config.log())

})