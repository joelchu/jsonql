// this will create an isolated environment to test the login 
// and stimulate several different scenario such the following 
// 1. First login to the public interface, then call a private method 
// 2. Then trigger the login call, and ensure the related hook are call correctly
// 3. Create a false connection event and check the error handling 
// 4. figure out how to salvage the fail connection situation 

const test = require('ava')


test.todo(`(1) Login to the public interface, then call a private method`)

test.todo(`(2) Rerun the connection first then the login call`)

test.todo(`(3) create a false connection and check the error handling`)

test.todo(`(4) try the reconnection on interval`)