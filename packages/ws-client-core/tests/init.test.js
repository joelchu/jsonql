// testing the init connection
const test = require('ava')
const { basicClient } = require('jsonql-ws-server/client')
const wsServer = require('./fixtures/server-setup')
const { JSONQL_PATH } = require('jsonql-constants')
const debug = require('debug')('jsonql-ws-server:test:ws')
const port = 9081

const { createInitPing, extractPingResult } = require('../src/callers/intercom-methods')

test.before(async t => {
  const { app } = await wsServer()

  t.context.server = app
  t.context.server.listen(port)
  
  t.context.client = basicClient(`ws://localhost:${port}/${JSONQL_PATH}`)
})

test.after(t => {
  t.context.server.close()
})

test.cb(`Testing the init ping call`, t => {
  t.plan(1)
  let client = t.context.client

  client.on('open', () => {
    const payload = createInitPing()
    debug(payload)
    client.send(payload)
  })

  client.on('message', data => {
    // that prove that on the server side and client side the data is different structure
    debug('raw data', typeof data, data)
    const header = extractPingResult(data)

    t.truthy(header)
    
    client.terminate()

    debug('header', header)
    // t.end()
    // reinit the client with an options
    const newClient = basicClient(`ws://localhost:${port}/${JSONQL_PATH}`, false, header)

    newClient.on('open', () => {
      t.end()
    })
  })

})