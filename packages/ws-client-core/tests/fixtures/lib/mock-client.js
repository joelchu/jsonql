// create a headless mock client
import { wsClientCoreAction } from '../../../index'
import { join } from 'path'
import { fakeWsClient, log } from './fake-ws-client'
import { checkConfiguration } from '../../../src/options/index'

import fsx from 'fs-extra'

const contract = fsx.readJsonSync(join(__dirname, '..', 'contract','public-contract.json'))

/**
 * generate a mock client to test this side of the code
 * @param {boolean} enableAuth 
 */
function mockClient(enableAuth = false) {
  // get a default options first 
  return checkConfiguration({ 
    enableAuth,
    contract, 
    debugOn: true,
    hostname: 'http://localhost:8888'
  })
  .then(wsClientCoreAction(fakeWsClient))
}

export { mockClient, log }





