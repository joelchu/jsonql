/**
 * Standalone mode to allow using the socket server to login
 * @param {string} username 
 * @param {string} password
 * @return {object} a user object then package as jwt string (internal process)
 */
module.exports = function login(username, password) {
  const users = [{name: 'joel', pass: '1234'} , {name: 'davided', pass: '5678'}]
  const user = users.filter(user => username === user.name && password === user.pass)
                    .reduce((base, user) => {
                      return user
                    }, false)
  if (user !== false) {
    return user
  }
  throw new Error(`User not found!`)
}