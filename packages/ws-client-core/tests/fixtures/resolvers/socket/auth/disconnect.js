const debug = require('debug')('ws-client-core:fixtures:auth:disconnect')
/**
 * Intercept the disconnect event 
 * @param {array} args
 * @return {void}
 */
module.exports = function disconnect(...args) {
  // @TODO 
  debug('intercept the disconnect event', args)
}