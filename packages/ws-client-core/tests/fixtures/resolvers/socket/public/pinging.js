// this is a public method always avaialble
const debug = require('debug')('jsonql-ws-client:resolver:pinging')

let ctn = 0
/**
 * @param {string} msg message
 * @return {string} reply message based on your message
 */
module.exports = function pinging(msg) {
  debug(`--> got call with --> ${msg}`)
  if (ctn > 0) {
    switch (msg) {
      case 'ping':
        debug(`got a ping`)
        pinging.send = 'pong';
      case 'pong':
        debug(`got a pong`)
        pinging.send = 'ping';
      default:
        return `Got your message ${msg}`
        //pinging.send = 'You lose!';
    }
    return true;
  }
  ++ctn;
  debug(`Did return here`)
  return 'connection established'
}
