const { join } = require('path')
const { jsonqlWsStandaloneServer } = require('jsonql-ws-server')

const resolverDir = join(__dirname, 'resolvers')

module.exports = function(extra = {}) {

  const contractDir = extra.enableAuth ? join(__dirname, 'contract', 'auth') 
                                       : join(__dirname, 'contract')

  return new Promise(resolver => {
    jsonqlWsStandaloneServer(
      Object.assign({
        resolverDir,
        contractDir,
        serverType: 'ws'
      }, extra),
      false 
    )
    .then(({ws, server}) => {
      resolver({
        io: ws,
        app: server
      })
    })
  })
}
