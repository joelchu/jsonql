// when this is running standalone mode then we need to 
// provide the extra methods for it to store and retrieve the userdata 

import { setupAuthMethods } from './setup-auth-methods'

export { setupAuthMethods }