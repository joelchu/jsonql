// take out from the resolver-methods
import {
  LOGIN_EVENT_NAME,
  LOGOUT_EVENT_NAME,
  ON_LOGIN_FN_NAME,
  IS_LOGIN_PROP_KEY,
  STANDALONE_PROP_KEY
  // SOCKET_AUTH_NAME
} from 'jsonql-constants'
import { JsonqlValidationError } from 'jsonql-errors'
import { injectToFn, chainFns, isString, objDefineProps, isFunc } from '../utils'
// import { validateAsync } from 'jsonql-params-validator'

/**
 * @UPDATE it might be better if we decoup the two http-client only emit a login event
 * Here should catch it and reload the ws client @TBC
 * break out from createAuthMethods to allow chaining call
 * @TODO when this is using the standalone mode then this will have to be disable
 * or delegated to the login method from the contract
 * @param {object} obj the main client object
 * @param {object} opts configuration
 * @param {object} ee event emitter
 * @return {array} [ obj, opts, ee ] what comes in what goes out
 */
const setupLoginHandler = (obj, opts, ee) => [
  injectToFn(
    obj, 
    opts.loginHandlerName, 
    function loginHandler(token) {
      if (token && isString(token)) {
        opts.log(`Received ${LOGIN_EVENT_NAME} with ${token}`)
        // @TODO add the interceptor hook
        return ee.$trigger(LOGIN_EVENT_NAME, [token])
      }
      // should trigger a global error instead @TODO
      throw new JsonqlValidationError(opts.loginHandlerName, `Unexpected token ${token}`)
    }
  ),
  opts,
  ee
]

/**
 * Switch to this one when standalone mode is enable 
 * @NOTE we don't actually have this because the public contract not included it
 * so we need to figure out a different way to include the auth method 
 * that get expose to the public before we can continue with this standalone mode
 * const { contract, loginHandlerName } = opts 
 * const params = contract[SOCKET_AUTH_NAME][loginHandlerName]
 * @param {*} obj 
 * @param {*} opts 
 * @param {*} ee 
 */
const setupStandaloneLoginHandler = (obj, opts, ee) => [
  injectToFn(
    obj, 
    loginHandlerName,
    function standaloneLoginHandler(...args) {
      // we only need to pass the argument 
      // let the listener to handle the rest
      ee.$trigger(LOGIN_EVENT_NAME, args)
    }
  ),
  opts,
  ee 
]

/**
 * break out from createAuthMethods to allow chaining call - final in chain
 * @param {object} obj the main client object
 * @param {object} opts configuration
 * @param {object} ee event emitter
 * @return {array} [ obj, opts, ee ] what comes in what goes out
 */
const setupLogoutHandler = (obj, opts, ee) => [
  injectToFn(
    obj, 
    opts.logoutHandlerName, 
    function logoutHandler(...args) {
      opts[IS_LOGIN_PROP_KEY] = true
      ee.$trigger(LOGOUT_EVENT_NAME, args)
    }
  ),
  opts,
  ee
]

/**
 * This event will fire when the socket.io.on('connection') and ws.onopen
 * Plus this will check if it's the private namespace that fired the event
 * @param {object} obj the client itself
 * @param {object} ee Event Emitter
 * @return {array} [ obj, opts, ee] what comes in what goes out
 */
const setupOnLoginListener = (obj, opts, ee) => [
  objDefineProps(
    obj, 
    ON_LOGIN_FN_NAME, 
    function onLoginCallbackHandler(onLoginCallback) {
      if (isFunc(onLoginCallback)) {
        // only one callback can registered with it, TBC
        // Should this be a $onlyOnce listener after the logout 
        // we add it back? 
        ee.$only(ON_LOGIN_FN_NAME, onLoginCallback)
      }
    }
  ),
  opts,
  ee
]

/**
 * Main Create auth related methods
 * @param {object} obj the client itself
 * @param {object} opts configuration
 * @param {object} ee Event Emitter
 * @return {array} [ obj, opts, ee ] what comes in what goes out
 */
export function setupAuthMethods(obj, opts, ee) {
  return chainFns(
    opts[STANDALONE_PROP_KEY] === true ? setupStandaloneLoginHandler : setupLoginHandler, 
    setupLogoutHandler,
    setupOnLoginListener
  )(obj, opts, ee)
}
