/** 
 * After a long period of struggle with the architect problem. Comes to a conclusion - it was shit.
 *
 * Instead of think of it from a host / slave structure. Should take a higher approach to look at it like 
 * onion skins. 
 *
 * At the moment, the ws client module export back the `configCheckMap` and `constProps` then back in here
 * which is very messy.
 */
import { createCombineConfigCheck } from './options'
import { wsClientCoreAction } from './api'
import { SOCKET_NAME } from 'jsonql-constants'

/**
 * Create a combine client
 * @param {function} httpClientConstructor the method generate the http client  
 * @param {function} wsClientEngine the core socket engine  
 * @param {object} configCheckMap the configuration check map  
 * @param {object} constProps props that add to the config post check
 * @return {function} (config) => combine client 
 */
function createCombineClient(httpClientConstructor, wsClientEngine, configCheckMap, constProps) {
  const checkConfigFn = createCombineConfigCheck(configCheckMap, constProps)
  /**
   * This will become the final interace facing the app to use
   */
  return (config = {}) => checkConfigFn(config)
                            .then(opts => {
                              // this constructor should not need to check the config again
                              return httpClientConstructor(opts)
                            })
                            .then(httpClient => {
                              const socketClient = wsClientCoreAction(wsClientEngine)(opts)
                              httpClient[SOCKET_NAME] = socketClient 
                              return httpClient
                            })
}

export { createCombineClient }
