// This is share between different clients so we export it
// @TODO port what is in the ws-main-handler
// because all the client side call are via the ee
// and that makes it re-usable between different client setup

import { getPrivateNamespace } from 'jsonql-utils/src/namespace'
import { logoutEvtListener, notLoginListener } from './event-listeners'

/**
 * Instead of running it in a loop we run this connection one by one 
 * And the private namespace will trigger via the event callback 
 * @param {*} bindSocketEventListener 
 * @param {string} namespace the nsp the namespace belongs to 
 * @param {*} [nsp=false] the nsp object it could be false during init when there is not login 
 */
export function namespaceEventListener(bindSocketEventListener, namespace, nsp = false) {
  /**
   * BREAKING CHANGE instead of one flat structure
   * we return a function to accept the required parameter to bind the event to socket
   * @param {object} opts configuration
   * @param {object} nspMap this is not in the opts
   * @param {object} ee Event Emitter instance
   * @return {array} although we return something but this is the last step and nothing to do further
   */
  return function listenerFn(opts, nspMap, ee) {
    const { log } = opts
    const { namespaces } = nspMap
    const privateNamespace = getPrivateNamespace(namespaces)
    let isPrivate = privateNamespace === namespace

    if (nsp) {
      log('[call bindWsHandler]', isPrivate, namespace)
      // we need to add one more property here to tell the bindSocketEventListener 
      // how many times it should call the onReady 
      let args = [namespace, nsp, ee, isPrivate, opts]
      // Finally we binding everything together
      Reflect.apply(bindSocketEventListener, null, args)
      
    } else {
      log(`binding notLoginWsHandler to ${namespace}`)
      // a dummy placeholder
      // @TODO but it should be a not connect handler
      // when it's not login (or fail) this should be handle differently
      notLoginListener(namespace, ee, opts)
    }

    if (isPrivate) {
      log(`Has private and add logoutEvtHandler`)
      // @TODO this method need to change as well 
      logoutEvtListener(nsp, namespace, ee, opts)
    }
  }

}


/**
 * centralize all the comm in one place
 * @param {function} bindSocketEventHandler binding the ee to ws --> this is the core bit
 * @param {object} nsps namespaced nsp
 * @return {void} nothing
 */
export function OLDnamespaceEventListener(bindSocketEventListener, nsps) {
  /**
   * BREAKING CHANGE instead of one flat structure
   * we return a function to accept the two
   * @param {object} opts configuration
   * @param {object} nspMap this is not in the opts
   * @param {object} ee Event Emitter instance
   * @return {array} although we return something but this is the last step and nothing to do further
   */
  return (opts, nspMap, ee) => {
    // since all these params already in the opts
    const { log } = opts
    const { namespaces } = nspMap
    // @1.1.3 add isPrivate prop to id which namespace is the private nsp
    // then we can use this prop to determine if we need to fire the ON_LOGIN_PROP_NAME event
    const privateNamespace = getPrivateNamespace(namespaces)
    // The total number of namespaces (which is 2 at the moment) minus the private namespace number
    let ctn = namespaces.length - 1   
    // @NOTE we need to somehow id if this namespace already been connected 
    // and the event been released before 
    return namespaces.map(namespace => {
      let isPrivate = privateNamespace === namespace
      log(namespace, ` --> ${isPrivate ? 'private': 'public'} nsp --> `, nsps[namespace] !== false)
      if (nsps[namespace]) {
        log('[call bindWsHandler]', isPrivate, namespace)
        // we need to add one more property here to tell the bindSocketEventListener 
        // how many times it should call the onReady 
        let args = [namespace, nsps[namespace], ee, isPrivate, opts, --ctn]
        // Finally we binding everything together
        Reflect.apply(bindSocketEventListener, null, args)
        
      } else {
        log(`binding notLoginWsHandler to ${namespace}`)
        // a dummy placeholder
        // @TODO but it should be a not connect handler
        // when it's not login (or fail) this should be handle differently
        notLoginListener(namespace, ee, opts)
      }
      if (isPrivate) {
        log(`Has private and add logoutEvtHandler`)
        logoutEvtListener(nsps, namespaces, ee, opts)
      }
      // just return something its not going to get use anywhere
      return isPrivate
    })
  }
}
