// this one bind the framework engine and our event engine together
import { 
  namespaceEventListener
} from './namespace-event-listener'
// error handler
import {
  triggerNamespacesOnError, 
  handleNamespaceOnError
} from './trigger-namespaces-on-error'


// group all the import export in one place 
export {
  
  namespaceEventListener,
  triggerNamespacesOnError, 
  handleNamespaceOnError
}