// this use by client-event-handler
import { ON_ERROR_FN_NAME } from 'jsonql-constants'
import { createEvt } from '../utils'

/**
 * trigger errors on all the namespace onError handler
 * @param {object} ee Event Emitter
 * @param {array} namespaces nsps string
 * @param {string} message optional
 * @return {void}
 */
export function triggerNamespacesOnError(ee, namespaces, message) {
  namespaces.forEach( namespace => {
    ee.$trigger(
      createEvt(namespace, ON_ERROR_FN_NAME), 
      [{ message, namespace }]
    )
  })
}

/**
 * Handle the onerror callback 
 * @param {object} ee event emitter  
 * @param {string} namespace which namespace has error 
 * @param {*} err error object
 * @return {void} 
 */
export const handleNamespaceOnError = (ee, namespace, err) => {
  ee.$trigger(createEvt(namespace, ON_ERROR_FN_NAME), [err])
}