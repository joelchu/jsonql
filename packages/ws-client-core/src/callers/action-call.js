// the actual trigger call method
import { ON_RESULT_FN_NAME, EMIT_REPLY_TYPE } from 'jsonql-constants'
import { createEvt, toArray } from '../utils'
import { respondHandler } from './respond-handler'

/**
 * just wrapper
 * @param {object} ee EventEmitter
 * @param {string} namespace where this belongs
 * @param {string} resolverName resolver
 * @param {array} args arguments
 * @param {function} log function 
 * @return {void} nothing
 */
export function actionCall(ee, namespace, resolverName, args = [], log) {
  // reply event 
  const outEventName = createEvt(namespace, EMIT_REPLY_TYPE)

  log(`actionCall: ${outEventName} --> ${resolverName}`, args)
  // This is the out going call 
  ee.$trigger(outEventName, [resolverName, toArray(args)])
  
  // then we need to listen to the event callback here as well
  return new Promise((resolver, rejecter) => {
    const inEventName = createEvt(namespace, resolverName, ON_RESULT_FN_NAME)
    // this cause the onResult got the result back first 
    // and it should be the promise resolve first
    // @TODO we need to rewrote the respondHandler to change the problem stated above 
    ee.$on(
      inEventName,
      function actionCallResultHandler(result) {
        log(`got the first result`, result)
        respondHandler(result, resolver, rejecter)
      }
    )
  })
}
