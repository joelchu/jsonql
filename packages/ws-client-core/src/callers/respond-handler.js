// breaking it up further to share between methods
import { DATA_KEY, ERROR_KEY } from 'jsonql-constants'
import { UNKNOWN_RESULT } from '../options/constants'
import { isObjectHasKey } from '../utils'

/**
 * break out to use in different places to handle the return from server
 * @param {object} data from server
 * @param {function} resolver NOT from promise
 * @param {function} rejecter NOT from promise
 * @return {void} nothing
 */
export function respondHandler(data, resolver, rejecter) {
  if (isObjectHasKey(data, ERROR_KEY)) {
    // debugFn('-- rejecter called --', data[ERROR_KEY])
    rejecter(data[ERROR_KEY])
  } else if (isObjectHasKey(data, DATA_KEY)) {
    // debugFn('-- resolver called --', data[DATA_KEY])
    // @NOTE we change from calling it directly to use reflect 
    // this could have another problem later when the return data is no in an array structure
    Reflect.apply(resolver, null, [...data[DATA_KEY]])
  } else {
    // debugFn('-- UNKNOWN_RESULT --', data)
    rejecter({message: UNKNOWN_RESULT, error: data})
  }
}
