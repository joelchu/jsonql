// just doing the import export thing here
// we move the following methods from listener to callers
// because they are actually sending things out not react to anything
import {
  createInitPing,
  extractPingResult,
  createIntercomPayload,
  extractSrvPayload
} from './intercom-methods'
import { helloWorld, getHelloMsg } from './hello'
import { callersGenerator } from './callers-generator'


// just rename it
export {
  createInitPing,
  extractPingResult,
  createIntercomPayload,
  extractSrvPayload,

  helloWorld,
  getHelloMsg,

  callersGenerator
}
