// @TODO using the obj.on syntax to do the same thing
/*
The new callback style `useCallbackStyle` set to true then use this one
client.resolverName.on(EVENT_NAME, cb)
*/
import { JsonqlValidationError, JsonqlError, finalCatch } from 'jsonql-errors'
import {
  ERROR_TYPE,
  DATA_KEY,
  ERROR_KEY,
  ERROR_PROP_NAME,
  MESSAGE_PROP_NAME,
  RESULT_PROP_NAME,
  READY_PROP_NAME,
  ON_ERROR_PROP_NAME,
  ON_RESULT_PROP_NAME,
  ON_MESSAGE_PROP_NAME
} from 'jsonql-constants'
import { CB_FN_NAME } from '../../options/constants'

import { respondHandler } from '../respond-handler'
import { chainFns, injectToFn, createEvt, toArray, isFunc, isString } from '../../utils'

/**
 * Add extra property to the resolver via the getter
 * @param {function} fn the resolver itself
 * @param {object} ee event emitter
 * @param {string} namespace the namespace this belongs to
 * @param {string} resolverName resolver namee
 * @param {object} params from the contract
 * @return {array} same as what goes in
 */
export function setupCallbackApi(fn, ee, namespace, resolverName, params) {
  return [
    injectToFn(fn, CB_FN_NAME, function(evtName, callback) {
      if (isString(evtName) && isFunc(callback)) {
        switch(evtName) {
          case RESULT_PROP_NAME:
            ee.$on(
              createEvt(namespace, resolverName, ON_RESULT_PROP_NAME),
              function resultHandler(result) {
                respondHandler(result, callback, (error) => {
                  ee.$trigger(createEvt(namespace, resolverName, ON_ERROR_PROP_NAME), error)
                })
              }
            )
            break;
          // register the handler for this message event
          case MESSAGE_PROP_NAME:
            ee.$only(
              createEvt(namespace, resolverName, ON_MESSAGE_PROP_NAME),
              function onMessageCallback(args) {
                respondHandler(args, callback, (error) => {
                  ee.$trigger(createEvt(namespace, resolverName, ON_ERROR_PROP_NAME), error)
                })
              }
            )
          break;
          case READY_PROP_NAME:
            ee.$only(
              createEvt(namespace, resolverName, ON_ERROR_PROP_NAME),
              callback
            )
          break;
          default:
            ee.$trigger(
              createEvt(namespace, resolverName, ON_ERROR_PROP_NAME),
              new JsonqlError(resolverName, `Unknown event name ${evtName}!`)
            )
        }
      }
    }),
    ee,
    namespace,
    resolverName,
    params
  ]
}
