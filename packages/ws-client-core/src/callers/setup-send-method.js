// setting up the send method 
import { JsonqlValidationError } from 'jsonql-errors'
import {
  ON_ERROR_FN_NAME,
  SEND_MSG_FN_NAME
} from 'jsonql-constants'
import { validateAsync } from 'jsonql-params-validator'
import { objDefineProps, createEvt, toArray, nil } from '../utils'
import { actionCall } from './action-call'

/** 
 * pairing with the server vesrion SEND_MSG_FN_NAME
 * last of the chain so only return the resolver (fn)
 * This is now change to a getter / setter method 
 * and call like this: resolver.send(...args)
 * @param {function} fn the resolver function 
 * @param {object} ee event emitter instance 
 * @param {string} namespace the namespace it belongs to 
 * @param {string} resolverName name of the resolver 
 * @param {object} params from contract 
 * @param {function} log a logger function
 * @return {function} return the resolver itself 
 */ 
export const setupSendMethod = (fn, ee, namespace, resolverName, params, log) => (
  objDefineProps(
    fn, 
    SEND_MSG_FN_NAME, 
    nil, 
    function sendHandler() {
      log(`running call getter method`)
      // let _log = (...args) => Reflect.apply(console.info, console, ['[SEND]'].concat(args))
      /** 
       * This will follow the same pattern like the resolver 
       * @param {array} args list of unknown argument follow the resolver 
       * @return {promise} resolve the result 
       */
      return function sendCallback(...args) {
        return validateAsync(args, params.params, true)
          .then(_args => {
            // @TODO check the result 
            // because the validation could failed with the list of fail properties 
            log('execute send', namespace, resolverName, _args)
            return actionCall(ee, namespace, resolverName, _args, log)
          })
          .catch(err => {
            // @TODO it shouldn't be just a validation error 
            // it could be server return error, so we need to check 
            // what error we got back here first 
            log('send error', err)
            // @TODO it might not an validation error need the finalCatch here
            ee.$call(
              createEvt(namespace, resolverName, ON_ERROR_FN_NAME),
              [new JsonqlValidationError(resolverName, err)]
            )
          })
      }    
    })
)
