// group all the small functions here
import { 
  EMIT_REPLY_TYPE,
  IS_LOGIN_PROP_KEY,
  IS_READY_PROP_KEY 
} from 'jsonql-constants'
import { toArray, createEvt } from 'jsonql-utils/src/generic'
import { injectToFn } from 'jsonql-utils/src/obj-define-props'
import JsonqlValidationError from 'jsonql-errors/src/validation-error'

/**
 * WebSocket is strict about the path, therefore we need to make sure before it goes in
 * @param {string} url input url
 * @return {string} url with correct path name
 */
export const fixWss = url => {
  const pattern = new RegExp('^(http|https)\:\/\/', 'i')
  const result = url.match(pattern)
  if (result && result.length) {
    const target = result[1].toLowerCase()
    const replacement = target === 'https' ? 'wss' : 'ws'
    
    return url.replace(pattern, replacement + '://')
  }

  return url 
}


/**
 * get a stock host name from browser
 */
export const getHostName = () => {
  try {
    return [window.location.protocol, window.location.host].join('//')
  } catch(e) {
    throw new JsonqlValidationError(e)
  }
}

/**
 * Unbind the event, this should only get call when it's disconnected
 * @param {object} ee EventEmitter
 * @param {string} namespace
 * @return {void}
 */
export const clearMainEmitEvt = (ee, namespace) => {
  let nsps = toArray(namespace)
  nsps.forEach(n => {
    ee.$off(createEvt(n, EMIT_REPLY_TYPE))
  })
}

/**
 * Setup the IS_LOGIN_PROP_KEY IS_READY_PROP_KEY 
 * @param {*} client 
 * @return {*} client 
 */
export function setupStatePropKeys(client) {
  return [IS_READY_PROP_KEY, IS_LOGIN_PROP_KEY]
    // .map(key => injectToFn(client, key, false, true))
    .reduce((c, key) => {
      return injectToFn(c, key, false, true)
    }, client)
}


