// export the util methods
import { isArray, isString } from 'jsonql-params-validator'
import { getLogFn } from './get-log-fn'
import { getEventEmitter, EventEmitterClass } from './get-event-emitter'
// moved to jsonql-utils
// some of these are unnecessary export should get rip of them
import {
  isContract,
  toArray,
  createEvt,
  injectToFn,
  objDefineProps,
  isFunc,
  isObjectHasKey,
  chainFns,
  chainPromises,
  objHasProp,
  nil,
  getNspInfoByConfig
} from 'jsonql-utils/module'

import {
  fixWss,
  getHostName,
  clearMainEmitEvt,
  setupStatePropKeys
} from './helpers'

// export
export {
  getLogFn,
  getEventEmitter,
  EventEmitterClass,
  
  isArray,
  isString,

  isContract,
  toArray,
  createEvt,
  injectToFn,
  objDefineProps,
  isFunc,
  isObjectHasKey,
  chainFns,
  chainPromises,
  objHasProp,
  nil,
  getNspInfoByConfig,
  

  fixWss,
  getHostName,
  clearMainEmitEvt,
  setupStatePropKeys
}
