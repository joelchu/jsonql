// just checking if the new opt export works or not
const test = require('ava')
const { getPreConfigCheck } = require('../module')
const { CHECKED_KEY } = require('jsonql-constants')
const { objHasProp } = require('jsonql-utils')
const debug = require('debug')('jsonql-client:test:opt')


test(`It should able to create a function to check the config`, t => {

  const checkFn = getPreConfigCheck()

  const result = checkFn({hostname: 'http://localhost:3456'})

  t.truthy(objHasProp(result, CHECKED_KEY))

})
