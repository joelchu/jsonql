

QUnit.test('jsonqlClientStatic should able to connect to server', function(assert) {
  var done1 = assert.async()
  var done2 = assert.async()
  var done3 = assert.async()
  // init client
  var client = jsonqlStaticClient({
    contract: publicJson,
    hostname: 'http://localhost:10081',
    showContractDesc: true,
    keepContract: false,
    debugOn: true
  })

  console.info('static client', client)

  client.getSomething('whatever', 'shit')
    .then(function(result) {
      console.log('[Qunit]', 'getSomething result', result)
      assert.equal(true, typeof result.whatever === 'string', 'just passing this one')
      done2()
    })
    .catch(function(error) {
      console.error('[Qunit]', 'some error happend?', error)
      done2()
    })

  client.helloWorld()
    .then(function(result) {
      console.info(result)
      assert.equal('Hello world!', result, "Hello world test done")
      done1()
    })
    .catch(function(error) {
      console.error('[Qunit]', 'catch', error)
      done1()
    })

  client.getArray()
    .then(function(result) {
      try {
        console.log('getArray result', result)
        assert.equal(4, result.length, 'Check the length of the array')
        done3()
      } catch(e) {
        throw new Error(e)
      }
    })
    .catch(function(error) {
      console.error(`[Qunit]`, 'get array error', error)
      done3()
    })
  // @TODO test the auth

})
