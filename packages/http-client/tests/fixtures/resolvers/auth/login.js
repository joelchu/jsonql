

/**
 * preparing the enable auth test
 * @param {string} username user name
 * @return {object} userdata
 */
module.exports = function issuer(username) {
  return {name: username, timestamp: Date.now()}
}
