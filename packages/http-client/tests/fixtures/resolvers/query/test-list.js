/**
 * Just return a Hello world object
 * @return {object} key hello prop world
 */
module.exports = async function testList() {
  return {
    hello: 'World!'
  };
}
