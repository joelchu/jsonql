/**
 * Test to see why the return result only get the first item in the array
 * @return {array} an array result
 */
module.exports = function getArray() {
  return [
    {id: 1, name: 'Tomato'},
    {id: 2, name: 'Fortune'},
    {id: 3, name: 'Little feather'},
    {id: 4, name: 'Fa fa'}
  ]
}
