# jsonql-client

> This is jsonql browser HTTP client for jsonql services

This is **NOT** just a HTTP client, it comes with full user client side management (using JWT)

Instead of using this directly, we recommend you to use [@jsonql/client](https://npmjs.com/package/@jsonql/client).

If you need to use in node environment, then you should use [jsonql-node-client](https://npmjs.com/package/jsonql-node-client) instead.

## How to use it

First, this module **DOES NOT** distribute with the [Flyio](https://github.com/wendux/fly) module. You have to explicitly import it yourself.
The reason is - we intend to support as many platform as possible, and Fly allow you to do just that, check their
[documentation](https://github.com/wendux/fly) for more information.

```js
import Fly from 'flyio'
import jsonqlClient from '@jsonql/http-client'

const client = jsonqlClient(Fly, config)

client.then(c => {
  c.query.helloWorld()
    .then(msg => {
      // should be a Hello World message
    })
})

```

### Pass contract via config gives you the client directly

If you pass the `contract` via the config, then you don't need to call the end

```js
import contract from 'some/where/contract.json'
import Fly from 'flyio'
import jsonqlClient from 'jsonql-client'

let config = { contract }
const client = jsonqlClient(Fly, config)

client.query.helloWorld()
  .then(msg => {
    // Hello world
  })
```

### Using a static client

There is a different style client that you can use:

```js
import Fly from 'flyio/dist/npm/fly' // using the Fly.js browser client
import jsonqlClientStatic from 'jsonql-client/static'

const client = jsonqlClientStatic(Fly) // optional pass a config

client.query('helloWorld')
  .then(msg => {

  })

```

As you can see from the above example, it also returned a promise, but the call signature
is different; the first parameter is the name of the resolver,
and the subsequence parameters are their expected parameters like so `client.query(resolverName, ...args)`

### Include from the dist to use

If you are not using build tools to build your js project and import via the HTML.
There are two files to use

- jsonql-client/dist/jsonql-client.umd.js (global name `jsonqlClient`)
- jsonql-client/dist/jsonql-client.static.js (global name `jsonqlClientStatic`)

**You must include the fly.js in the html document before any one of both of them**

See the following example (pretty close to our own test version as well)

```js
(function() {
  // jsonql-client/dist/jsonql-client.umd.js
  // you don't need to pass the Fly but you DO need to include in the html document
  jsonqlClient()
    .then(function(client) {
      // now use the client to do your thing
      client.query.helloWorld()
        .then(function(msg) {
          // hello world
        })
    })

  // jsonql-client/dist/jsonql-client.static.js
  jsonqlClientStatic()
    .then(function(staticClient) {
      // now use your static client to do your thing
      staticClient.query('helloWorld')
        .then(function(msg) {
          // hello world
        })
      // example of mutation fn(resolverName, payload, condition)  
      staticClient.mutation('updateSomething', {data: 'something'}, {id: 1})
        .then(function(result) {
          // use the result to do stuff
        })
    })

})()

```

## Configuration options

| Name        | Description           | Expected Type | Default value  |
| ----------- |:----------------------| :------------:| :--------------|
| hostname    | The hostname of the jsonql server | `String` | capture via the `window.location` object |
| loginHandlerName | custom login function name must match the server side resolver name | `String` | `login` |
| logoutHandlerName | custom logout function name must match the server side resolver name | `String` | `logout` |
| enableAuth  | Whether to use authorisation or not | `Boolean` | `false` |
| storageKey  | client side local storage key name | `String` | `CLIENT_STORAGE_KEY` |
| debugOn     | When enable, you will see debug message in your browser console | `Boolean` | `false` |


---

Please consult [jsonql.org](https:jsonql.js.org) for more information.

---

NB + T1S
