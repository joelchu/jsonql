// take only the module part which is what we use here
// and export it again to use through out the client
// this way we avoid those that we don't want node.js module got build into the code
import { isContract } from 'jsonql-utils/src/contract'
import { hashCode2Str } from 'nb-event-service/src/hash-code'

/**
 * @param {object} jsonqlInstance the init instance of jsonql client
 * @param {object} contract the static contract
 * @return {object} contract may be from server
 */
const getContractFromConfig = function(jsonqlInstance, contract = {}) {
  if (isContract(contract)) {
    return Promise.resolve(contract)
  }
  return jsonqlInstance.getContract()
}
// wrapper method to make sure it's a string
// just alias now
const hashCode = str => hashCode2Str(str)

// simple util to check if an object has any properties
// const hasProp = obj => isObject(obj) && Object.keys(obj).length

// export some constants as well
// since it's only use here there is no point of adding it to the constants module
// or may be we add it back later
const ENDPOINT_TABLE = 'endpoint' // not in use anymore delete later @TODO
const USERDATA_TABLE = 'userdata'
const CLS_LOCAL_STORE_NAME = 'localStore'
const CLS_SESS_STORE_NAME = 'sessionStore'
const CLS_CONTRACT_NAME = 'contract'
const CLS_PROFILE_IDX = 'prof_idx'
const LOG_ERROR_SWITCH = '__error__'
const ZERO_IDX = 0
// export
export {
  isContract,
  hashCode,
  getContractFromConfig,
  // constants
  ENDPOINT_TABLE,
  USERDATA_TABLE,
  CLS_LOCAL_STORE_NAME,
  CLS_SESS_STORE_NAME,
  CLS_CONTRACT_NAME,
  CLS_PROFILE_IDX,
  LOG_ERROR_SWITCH,
  ZERO_IDX
}
