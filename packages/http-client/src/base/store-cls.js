// new 1.5.0
// create a class method to handle all the saving and retriving data
// using the instanceKey to id the data hence allow to use multiple instance
import merge from 'lodash-es/merge'
import { localStore, sessionStore } from '../stores'
import { CLS_SESS_STORE_NAME, CLS_LOCAL_STORE_NAME, hashCode } from '../utils'

// this becomes the base class instead of the HttpCls
export default class StoreClass {

  constructor(opts) {
    this.opts = opts
    // make it a string
    this.instanceKey = hashCode(this.opts.hostname)
    // pass this store for use later
    this.localStore = localStore
    this.sessionStore = sessionStore
    /*
    if (this.opts.debugOn) { // reuse this to clear out the data
      this.log('clear all stores')
      localStore.clearAll()
      sessionStore.clearAll()

      localStore.set('TEST', Date.now())
      sessionStore.set('TEST', Date.now())
    }
    */
  }
  // store in local storage id by the instanceKey
  // values should be an object so with key so we just merge
  // into the existing store without going through the keys
  __setMethod(storeType, values) {
    let store = this[storeType]
    let data = this.__getMethod(storeType)
    const skey = this.opts.storageKey
    const ikey = this.instanceKey
    store.set(skey, {
      [ikey]: data ? merge({}, data, values) : values
    })
  }
  // return the data id by the instaceKey
  __getMethod(storeType) {
    let store = this[storeType]
    let data = store.get(this.opts.storageKey)
    return data ? data[this.instanceKey] : false
  }
  // remove from local store id by instanceKey
  __delMethod(storeType, key) {
    let data = this.__getMethod(storeType)
    if (data) {
      let store = {}
      for (let k in data) {
        if (k !== key) {
          store[k] = data[k]
        }
      }
      this.__setMethod(storeType, store)
    }
  }
  // clear everything by this instanceKey
  __clearMethod(storeKey) {
    const skey = this.opts.storageKey
    const store = this[storeKey]
    let data = store.get(skey)
    if (data) {
      let _store = {}
      for (let k in data) {
        if (k !== this.instanceKey) {
          _store[k] = data[k]
        }
      }
      store.set(skey, _store)
    }
  }
  // Alias for different store
  set lset(values) {
    return this.__setMethod(CLS_LOCAL_STORE_NAME, values)
  }

  get lget() {
    return this.__getMethod(CLS_LOCAL_STORE_NAME)
  }

  ldel(key) {
    return this.__delMethod(CLS_LOCAL_STORE_NAME, key)
  }

  lclear() {
    return this.__clearMethod(CLS_LOCAL_STORE_NAME)
  }

  // store in session store id by the instanceKey
  set sset(values) {
    // this.log('--- sset ---', values)
    return this.__setMethod(CLS_SESS_STORE_NAME, values)
  }

  get sget() {
    return this.__getMethod(CLS_SESS_STORE_NAME)
  }

  sdel(key) {
    return this.__delMethod(CLS_SESS_STORE_NAME, key)
  }

  sclear() {
    return this.__clearMethod(CLS_SESS_STORE_NAME)
  }


}
