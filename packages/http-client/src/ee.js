// this will generate a event emitter and will be use everywhere
import NBEventService from 'nb-event-service'

class JsonqlEventEmitter extends NBEventService {
  constructor(prop) {
    super(prop)
  }

  get name() {
    return 'jsonql-event-emitter'
  }
}

// export
export function getEventEmitter(debugOn) {
  let logger = debugOn ? (...args) => {
    args.unshift('[BUILTIN]') // rename here to id where this come from
    console.log.apply(null, args)
  }: undefined;
  return new JsonqlEventEmitter({ logger })
}
