// sort of persist on the user side
import engine from 'store/src/store-engine'

import localStorage from 'store/storages/localStorage'
import cookieStorage from 'store/storages/cookieStorage'

import defaultPlugin from 'store/plugins/defaults'
// @1.5.0 stop using the expired plugin, and deal with it ourself
// import expiredPlugin from 'store/plugins/expire'
import eventsPlugin from 'store/plugins/events'
import compressionPlugin from 'store/plugins/compression'

const storages = [localStorage, cookieStorage]
const plugins = [defaultPlugin, eventsPlugin, compressionPlugin]

const localStore = engine.createStore(storages, plugins)

export default localStore
