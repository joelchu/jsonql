// session store with watch
import engine from 'store/src/store-engine'

import sessionStorage from 'store/storages/sessionStorage'
import cookieStorage from 'store/storages/cookieStorage'

import defaultPlugin from 'store/plugins/defaults'
// start using compression in 1.5.0 
import compressionPlugin from 'store/plugins/compression'
// @1.5.0 stop using the expired plugin and deal it ourself
// import expiredPlugin from 'store/plugins/expire'

const storages = [sessionStorage, cookieStorage]
const plugins = [defaultPlugin, compressionPlugin]

const sessionStore = engine.createStore(storages, plugins)

export default sessionStore
