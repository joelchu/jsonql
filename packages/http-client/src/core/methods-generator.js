// breaking out the inner methods generator in here
import {
  // JsonqlValidationError,
  // JsonqlError,
  // clientErrorsHandler,
  finalCatch
} from 'jsonql-errors'
import { validateAsync } from 'jsonql-params-validator'
import { LOGOUT_NAME, LOGIN_NAME, KEY_WORD } from 'jsonql-constants'
import { chainFns } from 'jsonql-utils/src/chain-fns'
import { injectToFn } from 'jsonql-utils/src/obj-define-props'
import merge from 'lodash-es/merge'

/**
 * generate authorisation specific methods
 * @param {object} jsonqlInstance instance of this
 * @param {string} name of method
 * @param {object} opts configuration
 * @param {object} contract to match
 * @return {function} for use
 */
const authMethodGenerator = (jsonqlInstance, name, opts, contract) => {
  return (...args) => {
    const params = contract.auth[name].params;
    const values = params.map((p, i) => args[i])
    const header = args[params.length] || {};
    return validateAsync(args, params)
      .then(() => jsonqlInstance
          .query
          .apply(jsonqlInstance, [name, values, header])
      )
      .catch(finalCatch)
  }
}

/**
 * construct the final obj namespaced or not @1.6.0
 * @param {object} config --> namespaced
 * @param {string} type of resolver
 * @param {object} obj original object
 * @param {object} _obj the local obj
 * @return {object} the mutated object
 */
const getFinalObj = ({namespaced}, type, obj, _obj) => {
  let finalObj
  if (namespaced === true) {
    finalObj = obj
    finalObj[type] = _obj
  } else {
    finalObj = _obj
  }
  return finalObj
}

/**
 * Break up the different type each - create query methods
 * @param {object} obj to hold all the objects
 * @param {object} jsonqlInstance jsonql class instance
 * @param {object} ee eventEmitter
 * @param {object} config configuration
 * @param {object} contract json
 * @return {object} modified output for next op
 */
const createQueryMethods = (obj, jsonqlInstance, ee, config, contract) => {
  let _obj = config.namespaced === false ? obj : {}
  for (let queryFn in contract.query) {
    _obj = injectToFn(_obj, queryFn, function queryFnHandler(...args) {
      const params = contract.query[queryFn].params;
      const _args = params.map((param, i) => args[i])
      // debug('query', queryFn, _params);
      // @TODO this need to change to a different way to add an extra header
      const header = {}
      // @TODO validate against the type
      return validateAsync(_args, params)
        .then(() => jsonqlInstance
            .query
            .apply(jsonqlInstance, [queryFn, _args, header])
        )
        .catch(finalCatch)
    })
  }

  return [ getFinalObj(config, 'query', obj, _obj), jsonqlInstance, ee, config, contract ]
}

/**
 * create mutation methods
 * @param {object} obj to hold all the objects
 * @param {object} jsonqlInstance jsonql class instance
 * @param {object} ee eventEmitter
 * @param {object} config configuration
 * @param {object} contract json
 * @return {object} modified output for next op
 */
const createMutationMethods = (obj, jsonqlInstance, ee, config, contract) => {
  let _obj = config.namespaced === false ? obj : {}
  // process the mutation, the reason the mutation has a fixed number of parameters
  // there is only the payload, and conditions parameters
  // plus a header at the end
  for (let mutationFn in contract.mutation) {
    _obj = injectToFn(_obj, mutationFn, function mutationFnHandler(payload, conditions, header = {}) {
      const args = [payload, conditions]
      const params = contract.mutation[mutationFn].params
      return validateAsync(args, params)
        .then(() => jsonqlInstance
            .mutation
            .apply(jsonqlInstance, [mutationFn, payload, conditions, header])
        )
        .catch(finalCatch)
    })
  }

  return [ getFinalObj(config, 'mutation', obj, _obj), jsonqlInstance, ee, config, contract ]
}

/**
 * create auth methods
 * @param {object} obj to hold all the objects
 * @param {object} jsonqlInstance jsonql class instance
 * @param {object} ee eventEmitter
 * @param {object} config configuration
 * @param {object} contract json
 * @return {object} modified output for next op
 */
const createAuthMethods = (obj, jsonqlInstance, ee, config, contract) => {
  if (config.enableAuth && contract.auth) {
    let auth = config.namespaced === false ? obj : {}
    const { loginHandlerName, logoutHandlerName } = config;
    if (contract.auth[loginHandlerName]) {
      // changing to the name the config specify
      auth[loginHandlerName] = function loginHandlerFn(...args) {
        const fn = authMethodGenerator(jsonqlInstance, loginHandlerName, config, contract)
        return fn.apply(null, args)
          .then(jsonqlInstance.postLoginAction.bind(jsonqlInstance))
          .then(({token, userdata}) => {
            ee.$trigger(LOGIN_NAME, token)
            //  1.5.6 return the decoded userdata instead
            return userdata
          })
      }
    }
    // @TODO allow to logout one particular profile or all of them
    if (contract.auth[logoutHandlerName]) { // this one has a server side logout
      auth[logoutHandlerName] = function logoutHandlerFn(...args) {
        const fn = authMethodGenerator(jsonqlInstance, logoutHandlerName, config, contract)
        return fn.apply(null, args)
          .then(jsonqlInstance.postLogoutAction.bind(jsonqlInstance))
          .then(reason => {
            ee.$trigger(LOGOUT_NAME, reason)
            return reason
          })
      }
    } else { // this is only for client side logout
      // @TODO should allow to login particular profile
      auth[logoutHandlerName] = function logoutHandlerFn(profileId = null) {
        jsonqlInstance.postLogoutAction(KEY_WORD, profileId)
        ee.$trigger(LOGOUT_NAME, KEY_WORD)
      }
    }
    // @1.6.0
    return getFinalObj(config, 'auth', obj, auth)
  }

  return obj
}

/**
 * We want the same event emitter that get injected return to the client
 * Therefore we need to take the one been used and return it
 */
export function addPropsToClient(obj, jsonqlInstance, ee, config, contract) {
  obj.eventEmitter = ee // this might have to enable by config
  obj.contract = contract // do we need this?
  obj.version = '__VERSION__'
  // use this method then we can hook into the debugOn at the same time
  // 1.5.2 change it to a getter to return a method, pass a name to id which one is which
  obj.getLogger = (name) => (...args) => Reflect.apply(jsonqlInstance.log, jsonqlInstance, [name, ...args])
  // auth
  // create the rest of the methods
  if (config.enableAuth) {
    /**
     * new method to allow retrieve the current login user data
     * @TODO allow to pass an id to switch to different userdata
     * @return {*} userdata
     */
    obj.getUserdata = () => jsonqlInstance.jsonqlUserdata
    // allow getting the token for valdiate agains the socket
    // if it's not require auth there is no point of calling getToken
    obj.getToken = (idx = false) => jsonqlInstance.rawAuthToken(idx)
    // switch profile or read back what is the currenct index
    obj.profileIndex = (idx = false) => {
      if (idx === false) {
        return jsonqlInstance.profileIndex
      }
      jsonqlInstance.profileIndex = idx
    }
    // new in 1.5.1 to return different profiles
    obj.getProfiles = (idx = false) => jsonqlInstance.getProfiles(idx)
  }
  // @1.6.0 @TODO expose the store?

  return obj
}

/**
 * Here just generate the methods calls
 * @param {object} jsonqlInstance what it said
 * @param {object} ee event emitter
 * @param {object} config configuration
 * @param {object} contract the map
 * @return {object} with mapped methods
 */
export function methodsGenerator(jsonqlInstance, ee, config, contract) {
  let obj = {}
  const fns = [createQueryMethods, createMutationMethods, createAuthMethods]
  const executor = Reflect.apply(chainFns, null, fns)
  return executor(obj, jsonqlInstance, ee, config, contract)
}
