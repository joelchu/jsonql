// re-export here
const { 
  setupSocketServer 
} = require('./core/setup-socket-server')
// move back from the root index.js
const {
  jsonqlWsServerCore,
  jsonqlWsServerCoreAction
} = require('./modules')
const {
  DEFAULT_PORT,
  configCheckMap
} = require('./core/options')
const { 
  createHttpServer 
} = require('./http-server')
const { 
  STANDALONE_PROP_KEY 
} = require('jsonql-constants')

/**
 * This is the method that is the actual create server without the config check
 * @param {object} opts the already checked configuration
 * @param {object} server the http server instance
 * @return {object} the jsonql ws server instance
 */
function jsonqlWsServerAction(opts, server) {
  const params = { opts, server, setupSocketServer }

  return jsonqlWsServerCoreAction(params)
}

/**
 * The main method
 * @param {object} config this is now diverse from the middleware setup
 * @param {string} config.serverType socket.io or ws in the background
 * @param {object} config.options the actual options to pass to the underlying setups
 * @param {object} config.server this is the http/s server that required to bind to the socket to run on the same connection
 * @return {promise} return the undelying socket server instance for further use
 */
function jsonqlWsServer(config, server) {  
  const setupFn = jsonqlWsServerCore(setupSocketServer, configCheckMap)
  
  return setupFn(config, server)
    .then(ws => {
      return { ws, server }
    })
}

/**
 * quick way to create a standalone server
 * @param {*} config 
 * @param {*} port pass FALSE then no auto start
 * @return {promise} jsonqlWsServer, server 
 */
function jsonqlWsStandaloneServer(config, port = DEFAULT_PORT) {
  const server = createHttpServer()
  const opts = Object.assign(config, {[STANDALONE_PROP_KEY]: true})
  
  return jsonqlWsServer(opts, server)
    .then(({ws,server}) => {
      if (port !== false) {
        server.listen(port)
      }
      return { ws, server }
    })
}

module.exports = {
  jsonqlWsServerAction,
  jsonqlWsServer,
  jsonqlWsStandaloneServer
}
