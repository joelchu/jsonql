// @TBC each different server might have different way to verify the client?
// export the verifyClient at init time for a handshake authentication

const { 
  SOCKET_STATE_KEY, 
  jwtDecode, 
  getWsAuthToken 
} = require('../../modules')

const { debug } = require('../../utils')

/**
 * We need to check the origin from the header to make sure it's match
 * @param {object} opts configuration options
 * @param {object} req request object
 * @return {boolean} false when fail check
 */
function checkHeaders(opts, req) {
  debug('headers', req.headers)
  return true
}

/*
{
  'sec-websocket-version': '13',
  'sec-websocket-key': 'JStndaPScjQej8+aMN4H4w==',
  connection: 'Upgrade',
  upgrade: 'websocket',
  'sec-websocket-extensions': 'permessage-deflate; client_max_window_bits',
  host: 'localhost:3003'
}
*/
/**
 * Just output the key of this object and see what is inside
 *
 */
function peek(info) {
  for (let key in info) {
    debug('info key', key)
    if (key !== 'req') {
      debug(key, info[key])
    }
  }
}

/**
 * Nicer way to provide a default value to the cb param below
 * @param {string} msg to show
 * @return {void}
 */
const localCb = msg => {
  console.error('verifyClient error:', msg)
}

/**
 * provide the necessary param to create the verifyClient function
 * @param {string} namespace the namespace to generate verifyClient for
 * @param {object} opts need to add this because from now on we will always verifyClient
 * @param {object} [jwtOptions={}] optional pass to the jwtDecode
 * @param {function|boolean} [cb=false] if the token is invalid
 * @return {function} verifyClient method
 */
function createVerifyClient(namespace, opts, jwtOptions = {}, cb = localCb) {
  const { enableAuth, nspInfo, publicKey } = opts
  // This property now generate inside the ws-server-core
  const { publicNamespace } = nspInfo

  /**
   * pass this as part of the option to the WebSocket at init time
   * @param {object} info we use the info.req.url to extract the token
   * @param {function} done a callback to pass or fail the login
   */
  return function verifyClient(info, done) {
    const { req } = info
    if (!checkHeaders(opts, req)) {
      // @TODO replace with share constants from ws-server-core
      cb('Header check failed')
      return done(false)
    }

    if (!enableAuth) {
     
      return done(true) // job done here
    }
    const token = getWsAuthToken(opts, req)

    // we should also check if there is a token here 
    // if there is then we need to decode it also
    if (namespace === publicNamespace && token === false) { 
      // connect to the public namespace without a token still pass
      return done(true)  
    }
    // @TODO here we need to take the interceptor.validate method that created by 
    // develop to run their own checks
    
    // decoding the token and pass to the resolver(s)
    if (token) {
      debug(`Got a token`, token)
      try {
        const payload = jwtDecode(token, publicKey, jwtOptions)

        debug(`verifyClient decoded with result:`, payload)

        if (!info.req[SOCKET_STATE_KEY]) {
          info.req[SOCKET_STATE_KEY] = {}
        }
        // passing this along to the next
        info.req[SOCKET_STATE_KEY].userdata = payload

        return done(payload)
      } catch(e) {
        debug('verifyClient', e, token)

        cb(e) // just show the error
        done(false)
      }
    }
    // @TODO replace with share constants from ws-server-core
    cb('================= no token! =================')
    done(false)
  }
}

module.exports = { createVerifyClient }
