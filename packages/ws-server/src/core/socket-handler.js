// we wrap the get-socket-handler and our interceptor here 

const { getSocketHandler } = require('../modules')
const { interceptIntercomCall } = require('../intercom-methods')

/**
 * This will combine the part where we need to handle here then pass to 
 * the generic part to handle the socket events
 * @param {*} opts 
 * @param {*} ws 
 * @param {*} deliverFn 
 * @param {*} req 
 * @param {*} namespace 
 * @param {*} payload 
 * @param {*} userdata 
 * @return {void}
 */
function socketHandler(opts, ws, deliverFn, req, namespace, payload, userdata) {

  interceptIntercomCall(opts, ws, namespace, payload, userdata)

  getSocketHandler(opts, ws, deliverFn, req, namespace, payload, userdata)
}




module.exports = { socketHandler }