// this will run via ava but not included in the main test 
const test = require('ava')
const { join } = require('path')
const fsx = require('fs-extra')
// const colors = require('colors/safe')
// this will get the umd version of the client module
const { 
  JSONQL_PATH, 
  INTERCOM_RESOLVER_NAME,
  DISCONNECT_EVENT_NAME
} = require('jsonql-constants')
// const { decodeToken } = require('jsonql-jwt')
const serverSetup = require('./fixtures/server')
const { basicClient } = require('../client')
const { 
  createQueryStr, 
  extractWsPayload,
  timestamp
} = require('jsonql-utils')
// const { extractWsPayload } = require('jsonql-ws-server-core')
// const createToken = require('./fixtures/token')
const createPayload = require('./fixtures/create-payload')

const debug = require('debug')('jsonql-ws-server:test:heartbeat')

const contractDir = join(__dirname, 'fixtures', 'contract', 'socket')
const contract = fsx.readJsonSync(join(contractDir, 'contract.json'))
const port = 3003
const baseUrl = `ws://localhost:${port}/${JSONQL_PATH}`

/**
 * Create a generic intercom method
 * @param {string} type the event type 
 * @param {array} args if any 
 * @return {string} formatted payload to send
 */
function createIntercomPayload(type, ...args) {
  const ts = timestamp()
  let payload = [type].concat(args)
  payload.push(ts)
  return createQueryStr(INTERCOM_RESOLVER_NAME, payload)
}

test.before(async t => {
  const { app } = await serverSetup({
    contract,
    contractDir,
    resolverDir: join(__dirname, 'fixtures', 'resolvers'),
    // privateMethodDir: 'private',
    // enableAuth: true,
    keysDir: join(__dirname, 'fixtures', 'keys')
  })

  t.context.server = app 
  t.context.server.listen(port)
  // @1.3.0 there is no different between the two only need the token param now
  t.context.client_public = basicClient(baseUrl)
})

test.after(t => {
  t.context.server.close()
})


test.cb(`testing the disconnect method`, t => {
  let ctn = 0
  let interval
  const client = t.context.client_public
  const plan = 3
  t.plan(plan)

  client.on('open', () => {
    const payload = createPayload('availableToEveryone')
    // debug('open payload', payload)
    interval = setInterval(() => {
      client.send(payload)
    }, 1000)

  })

  client.on('message', data => {
    // debug('received raw data', data)
    let json = extractWsPayload(data)
    // debug('reply', json)
    t.truthy(json)
    ++ctn
    if (ctn >= plan) {
      client.send(createIntercomPayload(DISCONNECT_EVENT_NAME))
      setTimeout(() => {
        client.terminate()
      // next should test the onBeforeClose call
      }, 500)
    }
  })

  client.on('close', () => {
    debug(`socket connection closed`)
    clearInterval(interval)
    t.end()
  })


})