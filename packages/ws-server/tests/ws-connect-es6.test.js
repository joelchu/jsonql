// test the basic connection and such
const test = require('ava')

const { join } = require('path')
const fsx = require('fs-extra')

const { basicClient } = require('../client')

const wsServer = require('./fixtures/server')
const { JSONQL_PATH, ERROR_KEY } = require('jsonql-constants')
const debug = require('debug')('jsonql-ws-server:test:ws')

const contractDir = join(__dirname, 'fixtures', 'contract', 'es6')
const resolverDir = join(__dirname, 'fixtures', 'es6resolvers')
const contractFile = join(contractDir, 'contract.json')
const contract = fsx.readJsonSync(contractFile)

const { extractWsPayload } = require('jsonql-ws-server-core')
const createPayload = require('./fixtures/create-payload')
const port = 3007
const msg = 'Something'


test.before(async t => {
  const { app, io } = await wsServer({
    contractDir,
    resolverDir,
    contract
  })
  t.context.io = io;
  t.context.server = app;
  t.context.server.listen(port)

  t.context.client = basicClient(`ws://localhost:${port}/${JSONQL_PATH}`)
})

test.after(t => {
  t.context.server.close()
})

test.cb('Grouping all ES6 test together as one because the way ws reponse to on.message', t => {
  t.plan(9)
  // connect to the server
  let client = t.context.client
  client.on('open', () => {
    client.send( createPayload('wsHandler', msg + ' 1st', Date.now()) )
    client.send( createPayload('chatroom', msg + ' 2nd', Date.now()) )
    setTimeout(() => {
      client.send( createPayload('causeError', msg + ' 3nd') )
    }, 500)
  })
  let i = 0
  const shouldEnd = (ctn, t) => {
    if (ctn > 2) {
      t.end()
    }
  }
  // wait for reply
  client.on('message', (data) => {
    // (1)
    t.is(true , t.context.io[JSONQL_PATH] !== undefined)
    let json
    try {
      json = extractWsPayload(data)
      debug('on message received: ', json)
      switch (json.resolverName) {
        case 'wsHandler':
          // (2)
          t.truthy(json.data, 'wsHandler should reply with a message')
          shouldEnd(++i, t)
          break
        case 'chatroom':
          // (3)
          t.truthy(json.data, 'chatroom should reply with a message')
          shouldEnd(++i, t)
          break
        default:
          debug('catch error here %O', json)
          let { data } = json

          t.truthy(data.error, 'causeError should have error field')
          t.is(json.type, ERROR_KEY, 'with a ERROR_KEY field')
          t.is(data.error.className, 'JsonqlResolverAppError', 'Expect to received resolver app error')
          t.is(data.error.message, 'causeError', 'Expect to get the resolver name via message field')

          shouldEnd(++i, t)
      }
    } catch(e) {
      debug('error', e)
      t.end()
    }
  })
})
