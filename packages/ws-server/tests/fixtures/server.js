const debug = require('debug')('jsonql-ws-server:fixtures:server')
const { join } = require('path')

const resolverDir = join(__dirname, 'resolvers')
const contractDir = join(__dirname, 'contract')

const { jsonqlWsStandaloneServer } = require('../../index')

module.exports = function(extra = {}) {
  // extra.contract = extra.contract || contract;
  return Promise.resolve(
    Object.assign({
      resolverDir,
      contractDir,
      serverType: 'ws'
    }, extra)
  )
    .then(config => {
      debug('server setup config', config)
      
      return jsonqlWsStandaloneServer(config, false)
        .then(({ws, server}) => {
          
          //  just keep it consistent 
          return {
            io: ws ,
            app: server
          }
        })
      })
    
}
