// This method for testing the public call

/**
 * There is no parameter require for this call
 * @return {string} a message
 */
module.exports = function availableToEveryone() {
  const ts = Date.now()
  // testing the userdata injection 
  if (availableToEveryone.userdata) {
    const { name } = availableToEveryone.userdata
    availableToEveryone.send(`${name} is logged in @ ${ts}`)
  }

  return `You get a public message @ ${ts}`
}
