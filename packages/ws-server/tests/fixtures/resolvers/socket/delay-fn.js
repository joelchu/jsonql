// test this with a Promise return result
// const debug = require('debug')('jsonql-ws-server:socket:delayFn')
/**
 * @param {string} msg a message
 * @param {number} timestamp a timestamp
 */
module.exports = function delayFn(msg, timestamp) {
  // also test the global socket instance
  delayFn.send('I am calling from delayFn @ ' + Date.now())

  console.info('delayFn', delayFn)

  return new Promise(resolver => {
    setTimeout(() => {
      resolver(msg + (Date.now() - timestamp))
    }, 1000)
  })
}
