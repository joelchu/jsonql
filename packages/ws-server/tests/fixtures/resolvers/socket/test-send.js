// this is a copy of the delay-fn to debug why the resolver not able to received update code 
const debug = require('debug')('jsonql-ws-server:fixtures:resolvers:test-send')
let sendCtn = 0
/**
 * Delay the first respond and using send with a reply first
 * @param {string} msg a message
 * @param {number} timestamp a timestamp
 * @return {string} msg + time lapsed
 */
module.exports = function testSend(msg, timestamp) {
  debug(`testSend getting call`)
  
  let timer = null 
  
  if (msg === 'terminate') {
    debug(`got another msg`, msg)
    clearInterval(timer)
  }

  timer = setInterval(() => {
    testSend.send(`>>>>>>>>>>>>>>>I keep sending in new message [${++sendCtn}] >>>>>>>>>>>>>>>>>>> ${Date.now()}`)
  }, 300)

  // also test the global socket instance
  testSend.send('I am calling from testSend before the promise resolve @ ' + Date.now())

  console.info('testSend', testSend)

  return new Promise(resolver => {
    setTimeout(() => {
      resolver(msg + ', it took ' + (Date.now() - timestamp) + 'ms')
    }, 500)
  })
}