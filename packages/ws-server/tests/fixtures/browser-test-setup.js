const Koa = require('koa')
const IO = require('koa-socket-2')
const socketioJwt = require('socketio-jwt')
const { join } = require('path');

const default_io = new IO({namespace: 'jsonql'})
const app = new Koa()

const root = join(__dirname, 'demo')
const port = process.env.PORT || 3001;
let timer;
app.use(require('koa-static')(root))

default_io.attach(app)

// should able to show the methods
console.log(app['jsonql'].onConnection)

default_io.on('chat message', function(msg){
  console.info('got a message', msg);
  if (msg === 'terminate') {
    clearTimeout(timer)
  }
  default_io.emit('chat message', msg)
})

// hijack the connnection for all channels
/*
app['jsonql'].on('connection', socketioJwt.authorize({
    secret: 'your secret or public key',
    timeout: 15000 // 15 seconds to send the authentication message
  })).on('authenticated', function(socket) {
    //this socket is authenticated, we are good to handle more events from it.
    console.log('hello! ' + socket.decoded_token.name);
  }));
*/

app.listen( port , () => {
  console.log('start @ ' + port)
  open('http://localhost:' + port)
})
