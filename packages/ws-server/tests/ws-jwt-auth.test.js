const test = require('ava')
const { join } = require('path')
const fsx = require('fs-extra')
const colors = require('colors/safe')
// this will get the umd version of the client module
const { JSONQL_PATH } = require('jsonql-constants')

const { basicClient } = require('../client')
const { extractWsPayload } = require('jsonql-ws-server-core')
const serverSetup = require('./fixtures/server')
const createToken = require('./fixtures/token')
const createPayload = require('./fixtures/create-payload')

const debug = require('debug')('jsonql-ws-server:test:jwt-auth')

const contractDir = join(__dirname, 'fixtures', 'contract', 'auth')
const contract = fsx.readJsonSync(join(contractDir, 'contract.json'))

const payload = {name: 'Joel', Location: 'Zhuhai'}
const payload1 = {name: 'Davide', location: 'London'}
const port = 3009
const baseUrl = `ws://localhost:${port}/${JSONQL_PATH}/`
const { rainbow } = colors

const EventEmitter = require('@to1source/event')


test.before(async t => {
  const { app, io } = await serverSetup({
    contract,
    contractDir,
    privateMethodDir: 'private',
    enableAuth: true,
    keysDir: join(__dirname, 'fixtures', 'keys')
  })

  t.context.token = createToken(payload)
  t.context.token1 = createToken(payload1)

  t.context.io = io
  t.context.app = app

  t.context.app.listen(port)
  
  t.context.client_private = basicClient(baseUrl + 'private', {}, t.context.token)
  t.context.client_public = basicClient(baseUrl + 'public')

  t.context.eventEmitter = new EventEmitter()

})

test.after(t => {
  t.context.app.close()
})


test.cb('It should able to connect to public namespace without a token', t => {
  // connect to the private channel
  t.plan(2)

  let client = t.context.client_public

  t.truthy(t.context.io[ [JSONQL_PATH, 'public'].join('/') ])

  client.on('open', () => {
    client.send( createPayload('availableToEveryone') )
  })

  client.on('message', data => {
    let json = extractWsPayload(data)
    debug('public namespace reply', json)

    t.truthy(json.data)
    t.end()
  })
})

test.cb('It should able to connect to the private namespace', t => {
  t.plan(2)
  let client = t.context.client_private

  t.truthy(t.context.io[[JSONQL_PATH, 'private'].join('/')])

  client.on('open', () => {
    client.send( createPayload('secretChatroom', 'back', 'stuff') )
  })

  client.on('message', data => {
    let json = extractWsPayload(data)
    t.truthy(json.data)

    client.close()
    debug('second payload from public namespace', json)
    t.end()
  })
})

// @NOTE when we run this in serial, it could hang up.
// Also if a 401 happens, does it mean the server die? or the client die (of course)
// what about the other clients? Should we reconnect them? so many question at the moment
test.cb(`first try to connect to the private without login and see what happens`, t => {
  const evtEmitter = t.context.eventEmitter
  const evtName = 'count'
  const add = () => evtEmitter.$trigger(evtName)
  const plan = 3
  t.plan(plan)

  let ctn = 0

  evtEmitter.$on(evtName, () => {
    ++ctn 
    debug(rainbow('============================= count added ==========================='), ctn)
    if (ctn >= plan) {
      t.end()
    }
  })

  // crash out the client
  let authClient = basicClient(baseUrl + 'private')
  // this is useful we could add this to the client to handle the error
  authClient.on('error', err => {
    debug(rainbow(`Got error`), err)

    t.truthy(err, `Expect to get an error`) // (1)
    add()
  })


    // next try to connect to the public again and see what happen
    let pClient = basicClient(baseUrl + 'public')


    pClient.on('open', () => {
      debug(rainbow(`onopen`))
      pClient.send( createPayload('availableToEveryone') )
    })

    pClient.on('message', data => {
      let json = extractWsPayload(data)
      debug(rainbow('PUBLIC reply'), json)

      t.truthy(json.data, `Expect to get respond from the public nsp`)
      add()
    })
    
    pClient.on('error', err => {
      debug(rainbow('public client error'), err)
    })

  setTimeout(() => {
    authClient = basicClient(baseUrl + 'private', {}, t.context.token1)
    
    authClient.on('message', data => {
      ++ctn
      let json = extractWsPayload(data)

      debug(rainbow(ctn+''), json)
      
      t.truthy(json.data) // 3
      
      authClient.close()
      add()
    })

    authClient.on('open', () => {
      authClient.send( 
        createPayload('secretChatroom', 'la', 'na') 
      )
    })
  }, 500)

})
