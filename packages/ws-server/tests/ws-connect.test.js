// test the basic connection and such
const test = require('ava')
// const WebSocket = require('ws')
const { join } = require('path')
const fsx = require('fs-extra')

const { basicClient } = require('../client')

const wsServer = require('./fixtures/server')
const { JSONQL_PATH } = require('jsonql-constants')
const debug = require('debug')('jsonql-ws-server:test:ws')

const contractDir = join(__dirname, 'fixtures', 'contract')
const contractFile = join(contractDir, 'contract.json')
const contract = fsx.readJsonSync(contractFile)

const { extractWsPayload } = require('jsonql-ws-server-core')
const createPayload = require('./fixtures/create-payload')
const port = 3008
const msg = 'Something'

test.before(async t => {
  const { app, io } = await wsServer({
    contractDir,
    contract
  })
  t.context.io = io
  t.context.server = app
  t.context.server.listen(port)
  t.context.client = basicClient(`ws://localhost:${port}/${JSONQL_PATH}`)
})

test.after(t => {
  t.context.server.close()
})

test.cb('Grouping all CJS test together as one because the way WS reponse to on.message', t => {
  t.plan(3)
  let tested = 0
  // connect to the server
  let client = t.context.client
  t.is(true , t.context.io[JSONQL_PATH] !== undefined)

  client.on('open', () => {
    client.send( createPayload('wsHandler', msg + ' 1st', Date.now()) )
    client.send( createPayload('chatroom', msg + ' 2nd', Date.now()) )
  })

  // wait for reply
  client.on('message', (data) => {
    let json
    try {
      json = extractWsPayload(data)

      debug('on message received: ', json)

      switch (json.resolverName) {
        case 'wsHandler':
          ++tested
          t.truthy(json.data, 'wsHandler should reply with a message')
          if (tested > 1) {
            t.end()
          }
          break
        case 'chatroom':
          t.truthy(json.data, 'chatroom should reply with a message')
          ++tested
          if (tested > 1) {
            t.end()
          }
          break
        default:
      }
    } catch(e) {
      debug('error happens in the catch phrase and end the test', e)
      t.end()
    }
  })
})
