// testing the Error handling
const test = require('ava')

const { join } = require('path')
const fsx = require('fs-extra')

const { basicClient } = require('../client')

const wsServer = require('./fixtures/server')
const { JSONQL_PATH, ERROR_KEY } = require('jsonql-constants')
const debug = require('debug')('jsonql-ws-server:test:ws')
const colors = require('colors/safe')

const localDebug = (str, ...args) => Reflect.apply(debug, null, [colors.black.bgBrightGreen(str), ...args])

const contractDir = join(__dirname, 'fixtures', 'contract')
const contractFile = join(contractDir, 'contract.json')
const contract = fsx.readJsonSync(contractFile);

const { extractWsPayload } = require('jsonql-utils')
const createPayload = require('./fixtures/create-payload')
const port = 3006
const msg = 'Something'
let ctn = 0

test.before(async t => {
  const { app, io } = await wsServer({
    contractDir,
    contract
  })
  t.context.io = io
  t.context.server = app
  t.context.server.listen(port)

  t.context.client = basicClient(`ws://localhost:${port}/${JSONQL_PATH}`)
})

test.after(t => {
  t.context.server.close()
})

test.cb(`It should able to extract the error object when error throw from the server side`, t => {
  t.plan(4)
  // connect to the server
  let client = t.context.client

  client.on('open', () => {
    client.send( createPayload('causeError', msg + ' 3nd') )
  })

  // wait for reply
  client.on('message', msg => {
    let json
    try {
      localDebug('on message received:', msg)

      json = extractWsPayload(msg) // @TODO the cb become a distraction and not helping

      localDebug('typeof json', typeof json)

      let { data } = json

      localDebug('json decoded from msg:', data)

      if (data.error) {

        t.truthy(data.error, 'causeError should have error field')
        // data.error = typeof data.error === 'string' ? JSON.parse(data.error) : data.error;
        t.is(data.error.className, 'JsonqlResolverAppError')
        t.is(json.type, ERROR_KEY)
        t.is(json.resolverName, 'causeError')
        t.end()
      } else {
        // this seems weird but in fact, 
        // before the resolver throw error, it send a message out 
        // therefore here should able to capture a message 

        debug('Got a send message:', data)
      }

    } catch(e) {
      localDebug('error happens in the catch phrase and end the test', e)
      t.end()
    }
  })
})
