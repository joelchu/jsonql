// only testing the send method 
const test = require('ava')
const { join } = require('path')
const fsx = require('fs-extra')
const { basicClient } = require('../client')
const wsServer = require('./fixtures/server')
const { JSONQL_PATH, HELLO_FN } = require('jsonql-constants')
const { extractWsPayload } = require('jsonql-ws-server-core')
const createPayload = require('./fixtures/create-payload')
const contractDir = join(__dirname, 'fixtures', 'contract', 'send')
const contractFile = join(contractDir, 'contract.json')
const contract = fsx.readJsonSync(contractFile)
const port = 3005

const debug = require('debug')('jsonql-ws-server:test:send')


test.before(async t => {
  const { app, io } = await wsServer({
    contractDir,
    contract
  })
  t.context.io = io
  t.context.server = app
  t.context.server.listen(port)
  t.context.client = basicClient(`ws://localhost:${port}/${JSONQL_PATH}`)
})

test.after(t => {
  t.context.server.close()
})

test.cb(`Testing the send method from within the resolver`, t => {

  t.plan(7)
  let ts
  let tested = 0 
  let client = t.context.client 

  client.on('message', data => {
    // debug(`===================== take a look at the raw data =======================\n`, data)
    ++tested
    
    let json = extractWsPayload(data)

    debug(JSON.stringify(json))

    t.pass()
    if (tested > 6) {

      client.send(createPayload('testSend', 'terminate', Date.now()))

      t.end()
    }
  })

  client.on('open', () => {
    ts = Date.now()
    // test with real resolver 
    client.send( createPayload('testSend', 'Knock knock', ts))
    // test hello world method 
    client.send( createPayload(HELLO_FN, 'Just say hi'))
    // test the unknown handler 
    client.send( createPayload('whatever', 'na na na') )

  })

})