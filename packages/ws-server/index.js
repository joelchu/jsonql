// Not going to use the koa-socket-2 due to it's lack of support namespace
// which is completely useless for us if there is no namespace
const {
  checkSocketServerType,
  // props export
  wsServerDefaultOptions,
  wsServerConstProps
} = require('./src/modules')
const {
  jsonqlWsServer,
  jsonqlWsServerAction,
  jsonqlWsStandaloneServer
} = require('./src')
const { 
  createHttpServer 
} = require('./src/http-server')

// breaking change @1.6.0 we export it as a name module
module.exports = {
  jsonqlWsServer,
  jsonqlWsServerAction,
  jsonqlWsStandaloneServer,
  // util export
  checkSocketServerType,
  // props export
  wsServerDefaultOptions,
  wsServerConstProps,
  createHttpServer
}
