// try to create a self reference split-task file
const { contractGenerator, readContract } = require('./extra')

/**
 * For split task to import this method
 * @param {object} config configuration
 * @param {boolean} pub public or not
 * @return {promise} resolve the contract
 */
module.exports = function splitGetContract(config, pub) {
  return new Promise((resolver, rejecter) => {
    if (config) {
      let contract = readContract(config.contractDir, pub)
      if (contract !== false) {
        return resolver(contract)
      }
      contractGenerator(config, pub)
        .then(contract => {
          resolver(contract)
        })
        .catch(error => {
          rejecter(error)
        })
    }
  })
}
