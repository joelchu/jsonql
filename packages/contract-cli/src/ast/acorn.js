// put all the acorn related methods here
// later on we can switch between acorn or typescript
const acorn = require('acorn')
const { DEFAULT_TYPE } = require('jsonql-constants')

/**
 * First filter we only interested in ExpressionStatement
 * @param {object} tree to walk
 * @return {boolean} true found
 */
function isExpression(tree) {
  return tree.type === 'ExpressionStatement'
}

/**
 * filter function to filter out the non export methods
 * @param {object} tree the tree to process
 * @return {mixed} array on success, false on failure
 */
function extractParams(tree) {
  if (tree.expression.type === 'AssignmentExpression'
  && tree.expression.left.type === 'MemberExpression'
  ) {
    const obj = tree.expression.left.object;
    const prop = tree.expression.left.property;
    const fnObj = tree.expression.right;
    // check
    if (obj.type === 'Identifier' && obj.name === 'module'
    && prop.type === 'Identifier' && prop.name === 'exports'
    && fnObj.type === 'FunctionExpression'
    ) {
      // && fnObj.async === true - we don't need to care if it's async or not
      return fnObj.params.map(param => {
        return {
          type: DEFAULT_TYPE, // @TODO <-- wait for the jsdoc api
          name: param.name
        }
      });
    }
  }
  return false
}

/**
 * always given an any type
 * @param {object} args the final return object
 * @return {object} cleaned object
 */
function processArgOutput(args) {
  return args.type || DEFAULT_TYPE
}

/**
 * try to extract the return
 * @TODO at the moment the first pass is tree.expression but what if
 * the coding style is different like declare function then export
 * this might affect the outcome, so we need to have multiple ways to read the
 * resolver files
 * @params {object} tree
 */
function extractReturns(tree) {
  // console.log(inspect(tree.right.body.body, false, null));
  try {
    return takeDownArray(tree.right.body.body
        .filter(arg => arg.type === 'ReturnStatement')
        .map(arg => processArgOutput(arg.argument)))
  } catch (e) {
    return false
  }
}

/**
 * when we we start the ts port we switch inside the astParser
 * @param {string} source to parse
 * @param {object} [options={}] optional options require when parsing module files
 * @return {object} parsed tree
 */
function acornParser(source, options = {}) {
  return acorn.parse(source, options)
}


module.exports = {
  acornParser,
  extractReturns,
  extractParams,
  isExpression
}
