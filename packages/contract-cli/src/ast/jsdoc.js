// using jsdoc-cli to read the file and get additional properties
// @TODO we still need to handle the @callback tag for function parameter
// http://usejsdoc.org/tags-param.html
// const { join } = require('path')
const { inspect } = require('util')
// const fs = require('fs-extra')
const jsdoc = require('jsdoc-api')
const debug = require('debug')('jsonql-contract:jsdoc-api')
const {
  DEFAULT_TYPE,
  SUPPORTED_TYPES,
  NUMBER_TYPES,
  NUMBER_TYPE,
  OBJECT_TYPE,
  ARRAY_TYPE_LFT,
  ARRAY_TYPE_RGT
} = require('jsonql-constants')
const { JsonqlError } = require('jsonql-errors')
const { some, groupBy, indexOf } = require('lodash')

/**
 * small helper to output all the debug code in details
 */
const detailOut = code => inspect(code, false,  null)

/**
 * normalize the type to the one we support therefore mini the risk of error
 * @param {string} type from jsdoc
 * @return {string} our supported type
 */
const normalizeType = function(type) {
  const t = type.toLowerCase();
  // is wildcard
  if (t === '*') {
    return DEFAULT_TYPE
  }
  // normal check
  if (indexOf(SUPPORTED_TYPES, t) > -1) {
    return t
  }
  // @TODO if they pass something like number[] string[] then we need special care
  // if its number?
  if (indexOf(NUMBER_TYPES, t) > -1) {
    return NUMBER_TYPE
  }
  // this is the Array type and we keep it
  if (t.indexOf('array.') > -1) {
    // debug('Array type here', t);
    // process the type within the array
    const _type = t.replace(ARRAY_TYPE_LFT,'').replace(ARRAY_TYPE_RGT,'')
    // call myself again
    return ARRAY_TYPE_LFT + normalizeType(_type) + ARRAY_TYPE_RGT
  }
  // and finally if we couldn't figure it out then it will all be any type!
  debug(`Raised a warning here, the ${type} / ${t} is unknown to use!`)
  return DEFAULT_TYPE
}

/**
 * break down the name into parent and name
 * @param {array} params
 * @return {array} transformed params
 */
const transformName = function(params) {
  return params.map(p => {
    if (p.name.indexOf('.') > -1) {
      // only support one level at the moment
      // actually that is the same with Typescript Interface
      const names = p.name.split('.')
      p.name = names[1]
      p.parent = names[0]
    }
    return p
  })
}

/**
 * Get all the children
 * @param {array} params
 * @return {object} children with parent as key
 */
const getChildren = function(params) {
  return groupBy(params.filter(p => p.parent ? p : false), 'parent')
}

/**
 * when using the object type, they SHOULD provide the keys in name.key
 * style, when we encounter this style of jsdoc, we fold them under the name
 * with an additional `keys` property, then the validator will able to validate
 * correctly, especially this is important for mutation
 * @param {object} params from jsdoc
 * @return {object} with key folded
 */
const foldParams = function(params) {
  if (some(params, {type: [OBJECT_TYPE]})) {
    const _params = transformName(params)
    const children = getChildren(_params)
    // capture to validate if there is any children left
    // const len = size(children)
    let ctn = 0
    // @TODO here if we just reduce the array and it's done
    // but the problem is if there is an wrong comment with orphan
    // then this will cause a bug gone unnotice, so we need to check once again
    return params.filter(p => {
      return !p.parent
    }).map(p => {
      if (children[p.name]) {
        ++ctn;
        p.keys = children[p.name]
      }
      return p
    })
  }
  return params
}

/**
 * Just hook into the jsdoc.explain API
 * @param {string} source stringify function
 * @return {object} result from jsdoc
 */
const explainSync = function(source) {
  return jsdoc.explainSync({ source })
}

/**
 * flatten the pararms for contract to use
 * @param {object} params parsed result
 * @return {object} clean result for contract
 */
const processParams = function(params) {
  if (params && Array.isArray(params)) {
    return params.map(param => {
      // always return array from now on
      // @TODO if the jsdoc is wrong (the problem was cause by without a type)
      // then there is no names field, how do we notify the user about the correct
      // error?
      if (!param.type || !param.type.names) {
        throw new Error(`Please check your documentation is correct or not!`)
      }
      param.type = param.type.names.map(normalizeType)
      // pass through
      return param
    })
  }
  return false
}

/**
 * Taken out from the code below
 * @param {object} res from jsdoc
 * @return {*} false on nothing
 */
const getParams = res => (
  !!res.params ? ( foldParams( processParams(res.params) ) || false )
             : ( res.undocumented ? false : [] )
)

/**
 * Take the output then search for the comment we need
 * @param {object} output parsed source
 * @param {string} [name = ''] function name for the next search
 * @return {array|null} null on not found
 */
const search = function(output, name = '') {
  return output.filter(res => {
    // if first search didn't find anything then search by name
    if (name !== '') {
      // debug('search by name', name);
      return res.meta && res.meta.code.name === name
    }
    // @2019-05-04 we might have another problem, discover that sometime this is not actually true
    // @TODO find out what the hells going on here
    return res.longname === 'module.exports'
  }).map(res => {
    let resolverName = res.meta.code.value || res.meta.code.name

    // debug(`----------------${resolverName}---------------------`)
    // debug('res.meta', detailOut(res.meta))
    // debug('res.params', detailOut(res.params))
    // debug('res.returns', detailOut(res.returns))

    return {
      name: resolverName,
      description: res.description || false,
      params: getParams(res),
      // @BUG The strange bug happens here, it should not call the processed but still calling it???
      returns: processParams(res.returns)
    }
  }).reduce((first, next) => {
    return next || false
  }, false)
}

/**
 * wrapping the output and check if we found what we are looking for
 * @param {object} output parse source
 * @return {promise} resolved with params or throw
 */
const searchForParams = function(output) {
  const result = search(output)
  if (result.name && !result.params) {
    debug(`params is no defined?`, detailOut(result))
    debug(`output`, detailOut(output))
    return search(output, result.name)
  }
  return result
}

/**
 * @param {object} result from jsdoc.explain
 * @param {string} [name] optional params to have the name show for error output
 * @return {object} json clear for output
 */
const clearForOutput = function(result, name) {
  if (Array.isArray(result)) {
    const res = searchForParams(result)
    if (!res.params) {
      // keep having problem with particular resolver but what is the problem?
      console.error(`res.params it not defined?`, detailOut(res))
      throw new Error(`Could not parse the jsdoc for ${name}! res.params ${res.params}`)
    }
    return res
  } else {
    console.error('jsdoc return result', detailOut(result))
    throw new JsonqlError(`${name} jsdoc parsing result is unexpected, did the programmer wrote comment correctly?`, result)
  }
}

/**
 * Final export
 * @param {string} filePath path to file
 * @param {string} [name = ''] optional name for error output
 * @return {object} destructed jsdoc json
 */
const getJsdoc = function(source, name = '') {
  return clearForOutput( explainSync( source ), name )
}

module.exports = {
//  explainSync, NOT use outside of here
  getJsdoc
}
