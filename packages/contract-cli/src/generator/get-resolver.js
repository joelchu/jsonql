// this is the HEART of this module
const fsx = require('fs-extra')
const { basename } = require('path') //  join,
const { compact } = require('lodash')
const { 
  INDEX_KEY, 
  QUERY_NAME,
  MUTATION_NAME,
  AUTH_NAME
} = require('jsonql-constants')
const {
  inTypesArray,
  isAuthType,
  checkIfIsPublic,
  checkIfIsPrivate,
  packOutput
} = require('./helpers')

const { getDebug, getTimestamp } = require('../utils')
const { getSourceType } = require('./get-source-type')
const { parseFileToAst } = require('./parse-file-to-ast')

// const debug = getDebug('generator:get-resolvers')

/**
 * Breaking out from the getResolver because we need to hook another method into it
 * @param {string} baseFile the path to the found file
 * @param {string} inDir the base path
 * @param {string} fileType ext
 * @param {object} config options to use
 * @return {object} with {ok:true} to id that is a resolver
 */
const checkResolver = (indexFile, inDir, fileType, config) => (baseFile) => {
  const failed = {ok: false}
  const fileParts = compact(baseFile.replace(inDir, '').toLowerCase().split('/'))
  const ctn = fileParts.length
  const type = fileParts[0]
  // we ignore all the fileParts on the root level
  if (fileParts.length === 1) {
    return failed;
  }
  const ext = '.' + fileType
  // process fileParts within the folder of query, mutation, auth
  const fileName = basename(fileParts[1], ext)
  // const evt = basename(fileParts[1]);
  switch (ctn) {
    case 4: // this will be inside the public folder
      if (inTypesArray(type)) {
        // we need to shift down one level to get the filename
        const fileName4 = basename(fileParts[2], ext)
        let ok = (fileParts[ctn - 1] === indexFile)
        if (checkIfIsPublic(fileParts, config)) {
          // debug(4, _fileName, fileParts);
          return packOutput(type, fileName4, ok, baseFile, true, config)
        } else if (checkIfIsPrivate(fileParts, config)) {
          return packOutput(type, fileName4, ok, baseFile, false, config)
        }
      }
      // make sure it always terminate here
      return failed
    case 3: // this could be inside the public folder
      if (inTypesArray(type) || isAuthType(type, fileName, config)) {
        let isPublic = checkIfIsPublic(fileParts, config)
        let isPrivate = checkIfIsPrivate(fileParts, config)
        let lastFile = fileParts[ctn - 1]
        let ok = lastFile === indexFile
        let fileName3 = fileName;
        if (isPublic || isPrivate) {
          ok = true;
          fileName3 = lastFile.replace(ext, '')
        }
        return packOutput(type, fileName3, ok, baseFile, isPublic, config)
      }
    case 2:
      if (inTypesArray(type) || isAuthType(type, fileName, config)) {
        // debug(2, type, fileName, fileParts);
        return packOutput(type, fileName, true, baseFile, false, config)
      }
    default:
      return failed
  }
}

/**
 * filter to get which is resolver or not
 * @param {object} config options to use
 * @param {string} fileType ext
 * @return {function} work out if it's or not
 */
function getResolver(config, fileType) {
  const indexFile = [INDEX_KEY, fileType].join('.')
  // return a function here
  const fn = checkResolver(indexFile, config.resolverDir, fileType, config)
  // debug(fn.toString())
  return fn
}

/**
 * move from the process-files, the final step to process the resolver files to contract
 * The parameter were pre-processed by the getResolver method
 * @param {string} type of resolver
 * @param {string} name of resolver
 * @param {string} file resolver
 * @param {boolean} public or private method
 * @param {string} namespace if this is a socket
 * @param {string} sourceType module or script
 * @return {object} group together for merge
 */
function processResolverToContract(type, name, file, public, namespace, sourceType) {
  // how do I change this to non-block instead?
  const source = fsx.readFileSync(file, 'utf8').toString()
  const { params, description, returns } = parseFileToAst(source, name, sourceType)
  // return
  return {
    [type]: {
      [name]: {
        namespace,
        public, // mark if this is a public accessible method or not
        file,
        description,
        params, // merge array NOT OBJECT
        returns
      }
    }
  }
}

/**
 * just return the base object for constructing the contract
 * @param {string} sourceType module or script
 * @return {object} the contract base
 */
function getContractBase(sourceType) {
  return {
    [QUERY_NAME]: {},
    [MUTATION_NAME]: {},
    [AUTH_NAME]: {},
    timestamp: getTimestamp(),
    sourceType: sourceType
  }
}

// export
module.exports = {
  getResolver,
  getSourceType,
  getContractBase,
  processResolverToContract
}
