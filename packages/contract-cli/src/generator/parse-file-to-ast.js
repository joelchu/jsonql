// take the core parse to ast function on its own for reuse 
const { merge } = require('lodash')
const {
  // extractReturns,
  astParser,
  extractParams,
  isExpression,
  getJsdoc
} = require('../ast')
/**
 * Break out the core function for resuse 
 * @param {string} source read the resolver into file 
 * @param {string} name of the resovler 
 * @param {string} sourceType ES6 or other
 */
function parseFileToAst(source, name, sourceType) {
  // parsing the file
  const result = astParser(source, { sourceType })
  // logToFile(config.logDirectory, [type, name].join('-'), result)
  // get params @BUG the error happens here?
  const baseParams = result.body.filter(isExpression).map(extractParams).filter(p => p)
  const { description, params, returns } = getJsdoc(source, name)

  return { 
    params: merge([], baseParams[0], params),
    description,
    returns 
  }
}

module.exports = { parseFileToAst }