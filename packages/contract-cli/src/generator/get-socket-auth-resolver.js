// This will be add to the post generate main contract.json file process
// these functions should not appear in the public-contract.json
// its mainly for internal use
const fsx = require('fs-extra')
const { join, sep, dirname, basename } = require('path')
const { merge } = require('lodash')

const { chainPromises } = require('jsonql-utils')
const { inArray, isObjectHasKey } = require('jsonql-params-validator')
const {
  SOCKET_NAME,
  AUTH_NAME,
  EXT,
  INDEX_KEY,
  SOCKET_AUTH_NAME,
  STANDALONE_PROP_KEY
} = require('jsonql-constants')

const { removeSrvAuthFromContract } = require('../utils')

const { getResolverFiles } = require('./read-files-output-contract')
const { getSourceType } = require('./get-source-type')
const { parseFileToAst } = require('./parse-file-to-ast')

/**
 * Return the list of files from the folder first
 * @param {string} resolverDir where the base resolver directory
 * @param {string} [fileExt=EXT] what type of files
 * @return {promise} resolve the list of files
 */
function getSocketAuthResolverFiles(resolverDir, fileExt = EXT) {
  const dir = join(resolverDir, SOCKET_NAME, AUTH_NAME)
  if (fsx.existsSync(dir)) {
    return getResolverFiles(dir, fileExt)
  }
  console.error(`${dir} does not existed!`)
  // we don't throw error here just return an empty result
  return Promise.resolve([])
}

/**
 * Take the list of files from the folder, then filter out those that are not
 * registered socket auth files
 * @param {object} config configuration options
 * @param {array} authFiles the list of auth files
 * @param {string} fileExt the file extension
 * @return {object} the list of registered socket auth files using name as key
 */
function filterAuthFiles(config, authFiles, fileExt = EXT) {
  // setup the targets
  const {
    loginHandlerName,
    logoutHandlerName,
    validatorHandlerName,
    disconnectHandlerName
  } = config
  const targets = [
    loginHandlerName,
    logoutHandlerName,
    validatorHandlerName,
    disconnectHandlerName
  ]

  return authFiles
    .map(file => {
      const name = basename(file).replace('.' + fileExt, '')
      // need to take the folder name in if it's an index file
      if (name === INDEX_KEY) {
        const dir = dirname(file).split(sep)

        return {[ dir[dir.length - 1] ]: file}
      }

      return {[name]: file}
    })
    .filter(fileObj => {
      const name = Object.keys(fileObj)[0]

      return inArray(targets, name)
    })
    .reduce((a,b) => Object.assign(a, b), {})
}

/**
 * This is similiar to the processResolverToContract in get-resolver
 * but we don't need that many options, so this is a cut down version
 * @param {string} file path to the file
 * @param {string} sourceType what type are they
 * @return {promise} resolve the partial contract for that particular resolver
 */
function processFileToContract(name, file, sourceType) {

  return Promise
    .resolve(fsx.readFileSync(file, 'utf8').toString() )
    .then(source => parseFileToAst(source, name, sourceType))
    .then(({ description, params, returns }) => (
      {
        [name]: {
          file,
          description,
          returns,
          params
        }
      }
    ))
}

/**
 * The main method to generate the partial contract of the socket auth methods
 * @param {object} config configuration
 * @param {string|boolean} [sourceType=null] what type of files are they ES6 or CS
 * @param {string} [fileExt=EXT] the file extension
 * @return {promise} resolve the socket auth partial contract
 */
function getSocketAuthResolver(config, sourceType = null, fileExt = EXT) {
  const { resolverDir } = config

  return getSocketAuthResolverFiles(resolverDir, fileExt)
    .then(files => filterAuthFiles(config, files, fileExt))
    .then(fileObj => {
      if (sourceType === null) {
        // need to match the expected object structure
        const _files = Object.values(fileObj).map(f => ({file: f}))

        return {
          sourceType: getSourceType(_files),
          fileObj
        }
      }
      return { sourceType, fileObj }
    })
    .then(({ sourceType, fileObj }) => {
      const names = Object.keys(fileObj)
      const files = Object.values(fileObj)

      return chainPromises(
        names.map((n, i) => (
          Promise.resolve(processFileToContract(n, files[i], sourceType))
        )),
        true
      )
    })
    .then(contract => ({
      [SOCKET_AUTH_NAME]: contract
    }))
}

/**
 * This will be the main interface that insert into the generateOutput
 * @param {object} config configuration
 * @param {object} contract the already working contract
 * @param {boolean} public true then need to filter out some of the things
 * @return {object} contract with the extra bit
 */
function processSocketAuth(config, contract, public) {
  if (isObjectHasKey(contract, SOCKET_NAME) && config.enableAuth === true) {
    const { sourceType } = contract

    return getSocketAuthResolver(config, sourceType)
      .then(partialContract => {
        if (public) {
          // check if this is standalone mode
          if (config[STANDALONE_PROP_KEY] !== true) {
            // just remove the whole thing
            return {}
          }
          // console.info(`is public and call removeSrvAuthFromContract`)
          // @TODO when socket is not in standaone mode then we should not expose
          // any auth method to the public contract
          return removeSrvAuthFromContract(partialContract, SOCKET_AUTH_NAME, config)
        }
        return partialContract
      })
      .then(partialContract => (
        merge({}, contract, partialContract)
      ))
  }

  return contract
}



module.exports = {
  getSocketAuthResolver,
  filterAuthFiles,
  getSocketAuthResolverFiles,
  processSocketAuth
}
