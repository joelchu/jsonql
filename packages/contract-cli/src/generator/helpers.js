// a bunch of small methods
const fsx = require('fs-extra')
const { merge, extend, camelCase } = require('lodash')
const { isObject } = require('jsonql-params-validator')
const { isContract, inArray } = require('jsonql-utils') // isObjectHasKey, 
const {
  RESOLVER_TYPES,
  AUTH_TYPE,
  SOCKET_NAME,
  JSONQL_PATH,
  PRIVATE_KEY
} = require('jsonql-constants')
// const { getJsdoc } = require('../ast')
const { getDebug } = require('../utils')

// Not using the stock one from jsonql-constants
let AUTH_TYPE_METHODS

const debug = getDebug('generator:helpers')

///////////////////////////////
//    get-resolvers helpers  //
///////////////////////////////

/**
 * it should only be ONE
 * @param {string} item to check
 * @return {boolean} found losey true otherwise false
 */
const inTypesArray = (item) => (
  RESOLVER_TYPES.filter(type => item === type).length === 1
)

/**
 * @param {string} type to compare
 * @param {string} file name to check --> need to change to camelCase!
 * @param {object} config options
 * @return {boolean} true on success
 */
const isAuthType = function(type, file, config) {
  let resolverName = camelCase(file)
  
  if (!AUTH_TYPE_METHODS) {
    const { loginHandlerName, logoutHandlerName, validatorHandlerName } = config
    AUTH_TYPE_METHODS = [loginHandlerName, logoutHandlerName, validatorHandlerName]
  }

  return type === AUTH_TYPE && inArray(AUTH_TYPE_METHODS, resolverName)
}

/**
 * use the filename as the event name and add to the contract object
 * @2019-06-03 also re-use this to add the namespace using the public private information
 * @param {string} type of the resolver
 * @param {object} config the original configuration
 * @param {object} obj the contract params object
 * @return {object} if it's socket then add a `event` filed to it
 */
const addSocketProps = function(type, config, obj) {
  if (type === SOCKET_NAME && config.enableAuth) {
    let nsp = `${JSONQL_PATH}/`
    let { publicMethodDir, privateMethodDir } = config
    let namespace;
    if (obj.public) {
      namespace = nsp + publicMethodDir
    } else {
      namespace = nsp + (privateMethodDir ? privateMethodDir : PRIVATE_KEY)
    }

    return extend(obj, { namespace })
  }
  return obj
}

/**
 * Check if it's a public folder
 * 2020-02-21 we remove this line `config.enableAuth === true && `
 * to allow public folder get include into the contract regardless enableAuth 
 * @param {array} files file path split
 * @param {object} config publicKey replace the PUBLIC_KEY
 * @return {boolean} true on success or undefined on fail
 */
const checkIfIsPublic = (files, config) => (
  files[1] === config.publicMethodDir
)

/**
 * Check if it's a private folder, name provide by user
 * @param {array} files file path split
 * @param {object} config privateKey could be false if the user didn't set it
 * @return {boolean} true on success or undefined on fail
 */
const checkIfIsPrivate = (files, config) => (
  config.enableAuth === true &&
  config.privateMethodDir !== false &&
  files[1] === config.privateMethodDir
)

/**
 * add the public flag to package object
 * @param {object} baseObj package object
 * @param {boolean} isPublic flag
 * @return {object} flag up or not
 */
const addPublicKey = (baseObj, isPublic) => (
  extend( baseObj, (isPublic ? {public: isPublic} : {}) )
)

/**
 * wrap them together for the output
 * @param {string} type of the call
 * @param {string} fileName name of file
 * @param {boolean} ok is this pass or not
 * @param {string} baseFile the full path to file
 * @param {boolean} isPublic or not
 * @param {object} config the original configuration object
 * @return {object} package object
 */
const packOutput = (type, fileName, ok, baseFile, isPublic, config) => (
  addSocketProps(type, config, addPublicKey({
      ok: ok,
      type: type,
      name: camelCase(fileName),
      file: baseFile
    }, isPublic)
  )
)

///////////////////////////////
//    PROCESSING METHODs     //
///////////////////////////////

/**
 * The return result shouldn't be array of array
 * it should be just one level array
 * @param {array} arr unknown level of array
 * @return {mixed} depends
 */
const takeDownArray = function(arr) {
  if (arr.length && arr.length === 1) {
    return takeDownArray(arr[0])
  }
  return arr
}

/**
 * V1.7.1 always add the sourceType for reference
 * inject extra to the contract.json file using the config
 * @param {object} config options passed by developer
 * @param {object} contract the contract object
 * @return {object} mutation contract
 */
function mutateContract(config, contract) {
  let extraContractProps = {}
  if (config.extraContractProps && isObject(config.extraContractProps)) {
    debug('merge with config.extraContractProps')
    extraContractProps = config.extraContractProps
  }
  return merge({}, contract, extraContractProps)
}

/**
 * The debug output is too much and can not be use, therefore log it a file for checking
 * @param {string} dir where to store it
 * @param {string} file filename to write to
 * @param {object} params the params to log
 * @return {boolean} true on success or nothing
 */
function logToFile(dir, file, params) {
  if (dir) {
    const filename = file.indexOf('.json') > -1 ? file : [file, 'json'].join('.')
    return fsx.outputJsonSync(join(dir, filename), params, {spaces: 2})
  }
}

// export
module.exports = {
  inTypesArray,
  isAuthType,
  checkIfIsPublic,
  checkIfIsPrivate,
  addPublicKey,
  packOutput,

  takeDownArray,

  mutateContract,
  logToFile,

  isContract
}
