// this will be responsible to split the task and run parallel computing to speed the task
const { join } = require('path')
const nbSplitTasks = require('nb-split-tasks')

const { getDebug } = require('../utils')
const debug = getDebug('split-task')

const basePath = join(__dirname, 'sub')

/**
 * The top level export method to get the task to do and array of payload
 * @param {string} taskName the name of the task
 * @param {array} payload array of payload
 * @return {promise} resolve the final result
 */
function splitTask(taskName, payload, returnType = 'array') {
  let scriptName, postProcessor;
  let payloads = [] // prepare payload
  switch (taskName) {
    case 'process':
      scriptName = join(basePath, 'explain.js')
      let { sourceType } = payload;
      payloads = payload.files.map(
        ({ type, name, file, public, namespace }) => ({ type, name, file, public, namespace, sourceType })
      )
      postProcessor = result => result;
    break;
    case 'pre':
      // 'prep.js'
      scriptName = join(basePath, 'prep.js')
      // here we need to get a preprocessor, then we don't need to keep calling it
      // can not send a function as arument to the fork fn
      let { fileType, files } = payload;
      // debug(preprocessor, files)
      payloads = files.map((file, idx) => ({ idx, file, fileType, config: payload.config }))
      // run the filter at this point
      postProcessor = result => result.filter(obj => obj.ok)
    break;
    default:
      throw new Error(`Unknown task ${taskName}`)
  }
  // debug(scriptName, payloads, returnType)
  // finally return
  return nbSplitTasks(scriptName, payloads, returnType)
    .then(postProcessor)
}

// export
module.exports = { splitTask }
