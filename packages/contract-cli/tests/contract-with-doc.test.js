// this will test the contract with useDoc option
const test = require('ava')
const { join } = require('path')
// const { inspect } = require('util')
const generator = require('../index')
const resolverDir = join(__dirname, 'fixtures', 'resolvers')
const contractDir = join(__dirname, 'fixtures', 'tmp', 'doc')
const debug = require('debug')('jsonql-contract:test:generator')
const fsx = require('fs-extra')
const baseContractFile = join(contractDir, 'contract.json')
// const publicContractFile = join(contractDir, 'public-contract.json')
const { DEFAULT_TYPE } = require('jsonql-constants')
const checkConfig = require('../src/options')

test.before(async t => {
  const options = {
    resolverDir,
    contractDir,
    enableAuth: true
  }
  const result = await generator(options)
  t.context.contract = fsx.readJsonSync( baseContractFile )
})

test.after(async t => {
  fsx.removeSync(contractDir)
})

test('The contract.json should contain jsdoc information', async t => {

  t.truthy(fsx.existsSync( baseContractFile ))
  const json = t.context.contract
  // check the properties
  t.true(typeof json.query.getOthers === 'object', 'Test if we have getOthers')
  t.true(json.query.getOthers.params[0].type[0] === DEFAULT_TYPE , 'Test if there is a param of DEFAULT_TYPE')
  t.true(json.query.getOthers.returns[0].type[0] === 'object', 'Test to see if have an object type')
})

test('The mutation should able to fold the key under the same name', async t => {
  const json = t.context.contract
  const baseFn = json.mutation.setDetailObj.params

  t.true(baseFn[0].name === 'payload', 'Test to see if we have payload')
  t.true(Array.isArray(baseFn[0].keys), 'Test to see if we capture the keys of the object')
  t.true(baseFn[1].name === 'condition' && baseFn[1].type[0] === 'object', 'Test to see if we have condition')
  t.true(baseFn[1].keys[0].name === 'id', 'Test to see if the keys has name id')

})

test('The contract should contain socket information', async t => {
  const json = t.context.contract;
  const socket = json.socket;
  t.true(typeof socket === 'object')
  t.truthy(socket.chatroom.namespace)
})

test('There should be a logout method', t => {
  const json = t.context.contract
  debug(json)
  t.deepEqual([], json.auth.logout.params)
})

test('There should be a method from the private folder', t => {
  const json = t.context.contract
  // private-fn-in-folder
  t.truthy( json.query.privateFnInFolder )
  t.truthy( json.query.privateFn )
})
