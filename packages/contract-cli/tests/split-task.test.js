// TDD to build the split task with cpu load caculcation and multiple
// array of promises
const test = require('ava')
const { join } = require('path')
const debug = require('debug')('jsonql-contract:test:split')
const { EXT } = require('jsonql-constants')

const fsx = require('fs-extra')
// import method frr testing
const { getResolverFiles } = require('../src/generator/read-files-output-contract')
const { splitTask } = require('../src/generator/split-task')
const { splitContractGenerator } = require('../extra')

const { applyDefaultOptions } = require('../src')
/*
const nbSplitTasks = require('nb-split-tasks')
const {
  getResolver,
  getSourceType,
  getContractBase
} = require('../src/generator/get-resolver')
*/
const resolverDir = join(__dirname, 'fixtures', 'koa-resolvers')
const contractDir = join(__dirname, 'tmp', 'split-test')

test.before(t => {
  fsx.removeSync(contractDir)
})

test.after(t=> {
  fsx.removeSync(contractDir)
})

test.cb(`It should able to split the task and return an array`, t => {
  t.plan(2)

  applyDefaultOptions({ resolverDir })
    .then(config => {
      return getResolverFiles(resolverDir, EXT)
        .then(files => {
          // prepare the payloads
          t.truthy(files.length)
          // create payload for task
          const payload = {
            files,
            config,
            resolverDir, // we should take this option out later
            fileType: EXT // this also should be taken out
          }
          return splitTask('pre', payload)
        })
        .then(result => {
          debug(result)
          t.pass()
          t.end()
        })
      })
})

test.only(`It should able to call a self reference splitContractGenerator`, async t => {
  const contract = await splitContractGenerator({
    resolverDir,
    contractDir,
    returnAs: 'json'
  })

  // debug(contract)
  t.pass()
  // if return as is NOT json then it will be the path to the json
  // t.true(fsx.existsSync(join(contractDir, 'contract.json')))

})
