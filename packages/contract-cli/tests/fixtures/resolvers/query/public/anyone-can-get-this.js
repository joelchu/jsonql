// This is the new feature for marking a method that is available publicly
/**
 * @return {string} a message
 */
module.exports = function() {
  return 'This is for anyone to see';
}
