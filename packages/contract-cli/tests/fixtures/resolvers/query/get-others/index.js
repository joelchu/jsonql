const lib = require('./lib');
/**
 * @param {*} param1 anything 1
 * @param {*} param2 anything 2
 * @param {*} param3 anything 3
 * @return {object} param as key with value
 */
module.exports = async function(param1, params2, params3) {
  return {
    param1,
    param2,
    param3
  };
}
