/**
 * A custom login method to determine why its not generate by the contract
 * @param {string} username username
 * @param {string} password password
 * @return {object} userdata payload
 */
module.exports = function customLogin(username, password) {
  if (password === 'password') {
    return {name: username}
  }
  throw new Error('Login failed!')
}
