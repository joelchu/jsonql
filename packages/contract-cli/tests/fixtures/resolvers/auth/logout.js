// this is the logout method

/**
 * The logout method require no params
 * @return {void} nothing to return
 */
module.exports = function logout() {
  console.log('You are now logout');
}
