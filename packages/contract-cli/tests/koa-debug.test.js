// this resolvers cause an error with the updateMsService resolver when call from within jsonql-koa
const test = require('ava')
const { join } = require('path')
const { inspect } = require('util')
const fsx = require('fs-extra')
const debug = require('debug')('jsonql-contract:koa-debugging')

const contractApi = require('../index')
const { contractGenerator } = require('../extra')

const resolverDir = join(__dirname, 'fixtures', 'koa-resolvers')
const contractDir = join(__dirname, 'tmp', 'koa-debug')

test.after(t => {
  fsx.removeSync(contractDir)
})

test(`First test the contract api directly`, async t => {
  const result = await contractApi({
    resolverDir,
    contractDir,
    returnAs: 'json'
  })

  debug(inspect(result, false, null))

  t.truthy(result.query && result.mutation, 'It should have the two basic required fields')
})

test(`It should able to generate a contract without error`, async t => {
  const result = await contractGenerator({
    resolverDir,
    contractDir,
    returnAs: 'json'
  })

  debug(inspect(result, false, null))

  t.truthy(result.mutation && result.mutation.updateMsService, 'It should have mutation field and associate method property')

  t.falsy(result.socket, 'It should not have a socket field')

})
