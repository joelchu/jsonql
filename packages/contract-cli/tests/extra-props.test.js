// test out the extra props will present on the contract.json or not
const test = require('ava')
const { join } = require('path')
// const { inspect } = require('util')
const generator = require('../index')
const resolverDir = join(__dirname, 'fixtures', 'resolvers')
const contractDir = join(__dirname, 'fixtures', 'tmp', 'with-public-methods')
const debug = require('debug')('jsonql-contract:test:extra-props')
const fsx = require('fs-extra')
const { RETURN_AS_JSON } = require('jsonql-constants')
const baseContractFile = join(contractDir, 'contract.json')
// const publicContractFile = join(contractDir, 'public-contract.json')

test.before(async t => {
  const result = await generator({
    resolverDir,
    contractDir,
    extraContractProps: {socketServerType: 'socket.io'},
    // publicMethodDir: 'public',
    // privateMethodDir: 'private',
    enableAuth: true
  })
  t.context.json = fsx.readJsonSync(baseContractFile)
})

test.after(async t => {
  fsx.removeSync(contractDir)
})

test('Should able to pass the extraContractProps option and read it back', async t => {
  t.is(true, t.context.json.socketServerType === 'socket.io')
})

test('It should able to find the public methods', async t => {

  t.is(true, t.context.json.query.anyoneCanGetThis.public, 'check query.anyoneCanGetThis')
  t.is(true, t.context.json.mutation.publicFn.public, 'check mutation.publicFn')

})

test("Now we call the generator again to check if it's using the same contract.json", async t => {
  const result = await generator({
    resolverDir,
    contractDir,
    public: true,
    enableAuth: true,
    returnAs: RETURN_AS_JSON
  });
  debug(result)
  // const contractJson = fsx.readJsonSync(publicContractFile);
  // then I can check the returnAs is correct or not
  t.is(true, t.context.json.timestamp === result.timestamp )

})

test('It should able to use a custom path to find the private methods', t => {
  t.truthy(t.context.json.query.privateFn)
})
