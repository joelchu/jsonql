// test with the custom login name method

const test = require('ava')
const { join } = require('path')

const generator = require('../index')
const resolverDir = join(__dirname, 'fixtures', 'resolvers')
const contractDir = join(__dirname, 'fixtures', 'tmp', 'custom-login')
// const debug = require('debug')('jsonql-contract:test:extra-props')
const fsx = require('fs-extra')

const baseContractFile = join(contractDir, 'contract.json')
// const publicContractFile = join(contractDir, 'public-contract.json')

test.before(async t => {
  const result = await generator({
    resolverDir,
    contractDir,
    extraContractProps: {socketServerType: 'socket.io'},
    // publicMethodDir: 'public',
    // privateMethodDir: 'private',
    loginHandlerName: 'customLogin',
    enableAuth: true
  })
  t.context.json = fsx.readJsonSync(baseContractFile)
})

test.after(async t => {
  fsx.removeSync(contractDir)
})

test('It should able to pick up the customLogin method instead of login', t => {

  // debug(t.context.json)

  t.truthy(t.context.json.auth.customLogin)

})
