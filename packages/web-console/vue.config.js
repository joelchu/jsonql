// add a copy contract here
const fsx = require('fs-extra')
const { join } = require('path')
fsx.copy(
  join(__dirname, 'tests', 'fixtures', 'contract', 'public-contract.json'),
  join(__dirname, 'src', 'plugin', 'public-contract.json')
)

/*
fsx.copy(
  join(__dirname, 'node_modules', 'flyio', 'dist', 'npm', 'fly.js'),
  join(__dirname, 'public', 'fly.js')
)
*/

module.exports = {
  lintOnSave: false,
  devServer: {
    port: 8081,
    open: true,
    proxy: {
      '^/jsonql': {
        target: "http://localhost:8082",
        ws: true
      }
    }
  }
}
