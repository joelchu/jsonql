// just run the template out and see what return
const _ = require('lodash')
const fsx = require('fs-extra')
const { join } = require('path')
const debug = require('debug')('jsonql-web-console:test:tpl')
const dir = join(__dirname, '..', '..', 'koa')
const content = fsx.readFileSync(join(dir, 'public', 'index.html'))
const json = fsx.readJsonSync(join(__dirname, 'contract', 'public-contract.json'))
const { renderTpl } = require('../../koa/utils')

const out = renderTpl(content, json) 

debug(out)
