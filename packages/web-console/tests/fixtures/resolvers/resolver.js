import querydefaultCall from './query/default-call.js' 
import mutationdefaultSave from './mutation/default-save.js' 

export {
	querydefaultCall,
mutationdefaultSave
}