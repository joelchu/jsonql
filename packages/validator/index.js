// export
import {
  checkIsObject,
  notEmpty,
  checkIsAny,
  checkIsBoolean,
  checkIsArray
} from './src'
import * as validator from './src/validator'
// configuration checking
import * as jsonqlOptions from './src/options'
// the two extra functions
import isInArray from './src/is-in-array'
// re-export from jsonql-utils 
import { isNumber, isString, isObjectHasKey } from 'jsonql-utils/src/generic'

// rename 
const isObject = checkIsObject
const isAny = checkIsAny
const isBoolean = checkIsBoolean
const isArray = checkIsArray
const isNotEmpty = notEmpty

const normalizeArgs = validator.normalizeArgs
const validateSync = validator.validateSync
const validateAsync = validator.validateAsync

const JSONQL_PARAMS_VALIDATOR_INFO = jsonqlOptions.JSONQL_PARAMS_VALIDATOR_INFO

const createConfig = jsonqlOptions.createConfig
const constructConfig = jsonqlOptions.constructConfigFn
// construct the final output 1.5.2
const checkConfigAsync = jsonqlOptions.getCheckConfigAsync(validator.validateSync)
const checkConfig = jsonqlOptions.getCheckConfig(validator.validateSync)

const inArray = isInArray

// check returns methods 
import { 
  checkReturns, 
  checkReturnsAsync, 
  checkResolverReturns, 
  checkResolverReturnsAsync 
} from './src/returns'

// group the in one 
export {
  JSONQL_PARAMS_VALIDATOR_INFO,
  
  isObject,
  isAny,
  isString,
  isBoolean,
  isNumber,
  isArray,
  isNotEmpty,
  
  inArray,
  isObjectHasKey,

  normalizeArgs,
  validateSync,
  validateAsync,

  createConfig,
  constructConfig,
  checkConfig,
  checkConfigAsync,

  checkReturns, 
  checkReturnsAsync, 
  checkResolverReturns, 
  checkResolverReturnsAsync
}





