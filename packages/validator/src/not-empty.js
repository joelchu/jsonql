
import { isArray, isString, trim } from 'jsonql-utils/src/generic'
import { isPlainObject, objIsEqual } from 'jsonql-utils/src/object'


/**
 * Check several parameter that there is something in the param
 * @param {*} param input
 * @param {boolean} [e=false] extended check
 * @return {boolean}
 */
 const isNotEmpty = (a, e = false) => {
  if (a === undefined || a === null) {
    return false
  }
  if (isString(a)) {
    return trim(a) !== ''
  }
  if (e === true) {
    if (isArray(a)) {
      return !!a.length
    }
    if (isPlainObject(a)) {
      return !objIsEqual(a, {})
    }
  }

  return true
}

export default isNotEmpty
