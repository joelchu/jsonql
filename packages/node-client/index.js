// this is the top level of app
const {
  JsonqlRequestClient,
  getContract,
  generator,
  checkConfiguration,
  getDebug
} = require('./src')
const {
  getSocketClient,
  createSocketClient,
} = require('./src/create-socket-client')

const debug = getDebug('main')

// finally
/**
 * The config takes two things
 * hostname, contractDir
 * if there is security feature on then the contractKey
 * @2020-03-04 We have to completely re-architect how to deal with the socket client
 * @param {object} config configuration the only require field is hostname
 * @return {object} promise to resolve the jsonql node client
 */
module.exports = function jsonqlNodeClient(config) {
  const { 
    socketDefaultOptions, 
    socketConstProps, 
    createSocketClientFn
  } = getSocketClient(config)
  // start the generator
  return checkConfiguration(config, socketDefaultOptions, socketConstProps)
    // @TODO remove once the socket client checked
    .then(opts => {
      debug('[jsonql-node-client] init opts', opts)
      return opts
    })
    // start the init sequence
    .then(opts => (
      {
        jsonqlInstance: new JsonqlRequestClient(opts),
        opts: opts
      }
    ))
    // create the client
    .then( ({jsonqlInstance, opts}) => (
      getContract(jsonqlInstance, opts.contract)
        .then(contract => {
            opts.contract = contract
            return {
              opts,
              contract,
              client: generator(jsonqlInstance, opts, contract)
            }
          }
        )
      )
    )
    // create the node socket client if any
    .then(({opts, contract, client}) => (
      createSocketClient(createSocketClientFn, client, opts, contract)
    ))
}
