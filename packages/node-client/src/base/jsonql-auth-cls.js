// this part is focus on the login / logout / validate metthod only
// const nbEventService = require('nb-event-service')
const { LOGIN_EVENT_NAME, LOGOUT_EVENT_NAME, AUTH_HEADER , BEARER } = require('jsonql-constants')
// const { isFunc } = require('jsonql-utils')
const { JsonqlNodeClientEventService } = require('./event-service')
const { getDebug } = require('../utils')
const JsonqlRequestClient = require('./request-client')

const debug = getDebug('jsonql-auth-cls')


class JsonqlAuthClient extends JsonqlRequestClient {

  constructor(config) {
    super(config)
    this.eventEmitter = new JsonqlNodeClientEventService()
  }

  /**
   * setter for the issuer method to use
   * @param {string} token to store
   * @return {string} on success
   */
  set token(token) {
    return this.__storeAuthToken(token)
  }

  /**
   * We first call the user / built in method then use this
   * callback method to trigger the success login (token)
   * @param {string} token the valid token
   * @return {string} the token
   */
  loginCallback(token) {
    debug(`loginCallback called`)
    this.token = token;
    this.eventEmitter.$trigger(LOGIN_EVENT_NAME, token)
    return token;
  }

  /**
   * this is the same as above also trigger the logout event
   * @param {*} args when using the custom logout method might have this
   * @return {array} return what comes in as an array
   */
  logoutCallback(...args) {
    this.token = false; // calling the setter
    this.eventEmitter.$trigger(LOGOUT_EVENT_NAME, args)
    return args;
  }


  /**
   * Store the auth token into session storage
   * @return {string} token
   */
  __storeAuthToken(token) {
    // the setter doesn't produce a return!
    this.setter('token', token)
    debug('store token --> ', token)
    return token
  }

  /**
   * Get the authorization header
   * @return {object}  Auth header
   */
  __getAuthHeader() {
    let t = this.__getAuthToken()
    debug(`[getAuthHeader] ${t}`)
    return t ? { [AUTH_HEADER]: `${BEARER} ${t}` } : {}
  }

  /**
   * Get the stored auth token from session storage
   * @return {string} auth token
   */
  __getAuthToken() {
    let v = this.getter('token')
    debug('get token from node-cache', v)
    return v;
  }

  /**
   * When we have the contractKey setup then we need to include this into heading
   * @return {object} combine object
   */
  __getAuth() {
    const extraParams = this.__cacheBurst()
    if (this.opts.contractKey === false) {
      return extraParams;
    }
    const contractAuth = {
      [this.opts.contractKeyName]: this.opts.contractKey
    }
    // debug('let see the contract key value pair', contractAuth);
    return merge({}, contractAuth, extraParams);
  }
}

module.exports = JsonqlAuthClient
