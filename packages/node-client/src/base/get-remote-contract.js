// getting contract from server
const fsx = require('fs-extra')
const request = require('@to1source/request/jsonql')
const { join, resolve } = require('path')
const { PUBLIC_CONTRACT_FILE_NAME, KEY_WORD } = require('jsonql-constants')
const { JsonqlError } = require('jsonql-errors')
const { getDebug } = require('../utils')

const debug = getDebug('get-remote-contract')

/**
 * fetching the remote contract and save locally
 * jsonql-contract --remote=http://hostname/jsonql
 * @param {object} args argument options
 * @param {function} cb callback
 * @return {promise}
 */
function getRemoteContract(args, cb) {
  return new Promise((resolver, rejecter) => {
    // if we have the remote or not here
    if (!args.remote) {
      return resolver(KEY_WORD) // allow the next chain to do stuff
    }
    const url = args.remote
    // options
    const options = {
      method: 'GET',
      headers: args.authHeader
    }
    request.jsonql(url, [],  options)
      .then(({result}) => {
        // @v1.4.3 the server now standardize the return in the same format
        if (!result.data) {
          return rejecter(result.error || 'Server return wrong contract format')
        }
        const contract = result.data;
        // v1.1.4 add raw output
        if (args.raw) {
          return resolver(contract)
        }
        // cache it
        cb(contract)
        // write to file
        const outDir = args.out ? resolve(args.out) : process.cwd()
        const outFile = join(outDir, PUBLIC_CONTRACT_FILE_NAME)
        debug(`outFile to: ${outFile}`)
        fsx.outputJson(outFile, contract, {spaces: 2}, err => {
          if (err) {
            return rejecter(err)
          }
          const result = args.returnAs && args.returnAs === 'file' ? outFile : contract;
          resolver(result)
        })
      })
      .catch(err => {
        rejecter(new JsonqlError('getRemoteContract', err))
      })
  })
}

module.exports = { getRemoteContract }