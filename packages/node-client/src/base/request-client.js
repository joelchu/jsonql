/**
 * @1.3.0 replace request with flyio because request is deprecated
 */
const request = require('@to1source/request/jsonql')
const querystring = require('querystring')
const fsx = require('fs-extra')
const { createQuery, createMutation } = require('jsonql-utils')
const { clientErrorsHandler } = require('jsonql-errors')

const { API_REQUEST_METHODS } = require('jsonql-constants')
const [ POST, PUT ] = API_REQUEST_METHODS

const { display, getDebug, resultHandler } = require('../utils')
const JsonqlClient = require('./jsonql-base-cls')

const debug = getDebug('request-client')

// main
class JsonqlRequestClient extends JsonqlClient {

  constructor(config = {}) {
    super(config)
  }

  /**
   * just a wrapper
   */
  getContract() {
    return this.__readContract().then(result => {
      if (typeof result === 'string') {
        return fsx.readJsonSync(result)
      }
      return result
    })
  }

  /**
   * wrapper method for the request
   * @param {string} method POST PUT
   * @param {object} payload what to send
   * @param {object} headers extra headers
   */
  __requestWrapper(method, payload, headers) {
    debug('sending payload', display(payload,1))

    const url = [this.__url__, querystring.stringify(this.__cacheBurst())].join('?')
    const config = { method, headers: this.__createHeaders(headers) }

    return request.jsonql(url, payload, config)
      .then(({result}) => {
        debug('got result from request', result)
        return this.__interceptor(payload, result)
      })
      .catch(err => {
        debug('Error happens here in request', err)
        throw new Error(err)
      })
  }

  /**
   * main query interface
   * @param {string} name fn to call
   * @param {object} args to get around the name paramter problem
   * @param {object} headers additional headers to add
   * @return {object} promise to resolve the result data
   * @api public
   */
  query(name, args = [], headers = {}) {
    const payload = createQuery(name, args)
    return this.__requestWrapper(POST, payload, headers)
      .then(clientErrorsHandler)
      .then(resultHandler)
  }

  /**
   * main mutation interface
   * @param {string} name fn to call
   * @param {object} payload to save
   * @param {object} conditions to use for backend
   * @param {object} headers additional headers
   * @return {object} promise
   * @api public
   */
  mutation(name, payload={}, conditions={}, headers={}) {
    let _payload = createMutation(name, payload, conditions)
    return this.__requestWrapper(PUT, _payload, headers)
      .then(clientErrorsHandler)
      .then(resultHandler)
  }
}

module.exports = JsonqlRequestClient
