// new method to include the socket client when pass the serverType config
const { 
  JS_WS_NAME, 
  JS_WS_SOCKET_IO_NAME, 
  JS_PRIMUS_NAME, 
  SOCKET_NAME, 
  SOCKET_TYPE_KEY
} = require('jsonql-constants')
const { getDebug } = require('./utils')
const debug = getDebug('create-socket-client')

/**
 * get the socket client node version using serverType
 * @param {object} config take the raw config and check this first 
 * config.serverType the socket server (client) type
 * @return {object} the module
 */
function getSocketClient(config) {
  const serverType = config[SOCKET_TYPE_KEY]
  let obj = {
    socketDefaultOptions: null, 
    socketConstProps: null, 
    createSocketClientFn: null
  }
  try {
    switch (serverType) {
      case JS_WS_NAME:
        // @2020-03-04 from now on take only the module version out
        const {
          jsonqlWsClientAppProps,
          jsonqlWsConstProps,
          createWsClient
        } = require('@jsonql/ws/node') // from now on we will always use the same naming
        
        obj.socketDefaultOptions = jsonqlWsClientAppProps,
        obj.socketConstProps = jsonqlWsConstProps,
        obj.createSocketClientFn = createWsClient
                
        break 
      case JS_WS_SOCKET_IO_NAME:
      case JS_PRIMUS_NAME:
      default:
        if (serverType) {
          console.error(`getSocketClient`, `Not support ${serverType} at the moment!`) 
        }
      }
    return obj 
  } catch(e) {
    console.error(e)
    // throw new Error(e)
    return obj 
  }
}

/**
 * This will hook into the last of the chain during init
 * @param {*} createSocketClientFn from the require module if any
 * @param {object} client the init jsonql-node-client
 * @param {object} config the checked configuration
 * @param {object} contract the contract json
 * @return {object} the modified client object if socketClientType presented
 */
function createSocketClient(createSocketClientFn, client, config, contract) {
  if (createSocketClientFn) {
    const opts = Object.assign(config, {
      // also need to pass the contract
      contract,
      // need to pass the eventEmitter here to the config
      eventEmitter: client.eventEmitter(),
      log: debug
    })
    // @1.3.1 using the require module directly
    return createSocketClientFn(opts)
      .then(socketClient => {
        client[SOCKET_NAME] = socketClient
        return client
      })
  }
  // nothing then just pass it onward
  return client 
}

module.exports = { createSocketClient, getSocketClient }
