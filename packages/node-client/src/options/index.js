// configuration options and check method
const { join } = require('path')
const {
  constructConfig,
  createConfig,
  checkConfig,
  checkConfigAsync
} = require('jsonql-params-validator')
const { getDebug, injectToFn } = require('../utils')
const debug = getDebug('options')
const {
  JSONQL_PATH,
  CONTENT_TYPE,
  CLIENT_STORAGE_KEY,
  CLIENT_AUTH_KEY,
  CONTRACT_KEY_NAME,
  PUBLIC_CONTRACT_FILE_NAME,
  DEFAULT_HEADER,
  DEFAULT_CONTRACT_DIR,
  BOOLEAN_TYPE,
  STRING_TYPE,
  ALIAS_KEY,
  ISSUER_NAME,
  LOGOUT_NAME,
  VALIDATOR_NAME,
  SOCKET_TYPE_CLIENT_ALIAS,
  SOCKET_TYPE_KEY
} = require('jsonql-constants')
const BASE_DIR = process.cwd()
// constants properties
const constProps = {
  contentType: CONTENT_TYPE,
  contract: {},
  useDoc: true,
  returnAs: 'json'
}
// node client configuration options
const ncAppProps = {

  hostname: constructConfig('', STRING_TYPE), // required the hostname
  jsonqlPath: constructConfig(JSONQL_PATH, STRING_TYPE), // The path on the server
  // matching the browser client version 
  namespaced: createConfig(false, [BOOLEAN_TYPE]),
  useJwt: createConfig(true, [BOOLEAN_TYPE]),
  appDir: createConfig('', [STRING_TYPE]), // matching the koa setup
  loginHandlerName: createConfig(ISSUER_NAME, [STRING_TYPE]),
  logoutHandlerName: createConfig(LOGOUT_NAME, [STRING_TYPE]),
  validatorHandlerName: createConfig(VALIDATOR_NAME, [STRING_TYPE]),

  enableAuth: createConfig(false, [BOOLEAN_TYPE]),

  // useLocalstorage: constructConfig(true, BOOLEAN_TYPE), // should we store the contract into localStorage
  storageKey: constructConfig(CLIENT_STORAGE_KEY, STRING_TYPE),// the key to use when store into localStorage
  authKey: constructConfig(CLIENT_AUTH_KEY, STRING_TYPE),// the key to use when store into the sessionStorage

  contractKey: constructConfig(false,  [BOOLEAN_TYPE, STRING_TYPE]), // if the server side is lock by the key you need this
  contractKeyName: constructConfig(CONTRACT_KEY_NAME, STRING_TYPE),// same as above they go in pairs
  // we don't really need to check this, it will get generated
  contractDir: constructConfig( join(BASE_DIR, DEFAULT_CONTRACT_DIR), STRING_TYPE),
  contractFileName: constructConfig(PUBLIC_CONTRACT_FILE_NAME, STRING_TYPE),
  // functions
  storeAuthToken: constructConfig(false, BOOLEAN_TYPE),
  getAuthToken: constructConfig(false, BOOLEAN_TYPE),
  defaultHeader: constructConfig(DEFAULT_HEADER, STRING_TYPE)
}
// socket client
const socketAppProps = {
  // new prop for socket client
  [SOCKET_TYPE_KEY]: createConfig(null, [STRING_TYPE], {[ALIAS_KEY]: SOCKET_TYPE_CLIENT_ALIAS})
    // [ENUM_KEY]: [JS_WS_NAME, JS_WS_SOCKET_IO_NAME, JS_PRIMUS_NAME],
}
// combine the two
const appProps = Object.assign(ncAppProps, socketAppProps)

/**
 * We need this to find the socket server type 
 * @param {*} config 
 * @return {string} the name of the socket server if any
 */
function checkSocketClientType(config) {
  return checkConfig(config, socketAppProps)
}

/**
 * check the node-client config(s) 
 * Also add the socket server config map and const map if any
 * @param {object} config the user supplied configuration
 * @param {*} [socketDefaultOptions = null] the socket server default app props
 * @param {*} [socketConstProps = null] the socket server const props
 * @return {object} the checked configuration  
 */
function checkConfiguration(config, socketDefaultOptions = null, socketConstProps = null) {
  let { contract } = config
  debug(`jsonql-node-client raw config`, config)

  const localAppProps = socketDefaultOptions ? Object.assign({}, appProps, socketDefaultOptions) : appProps
  const localConstProps = socketConstProps ? Object.assign({}, constProps, socketConstProps) : constProps

  return checkConfigAsync(config, localAppProps, localConstProps)
    .then(opts => {
      opts.contract = contract
      return opts
    })
}

// we should name all of them using the same name checkConfiguration
module.exports = { checkConfiguration, checkSocketClientType }