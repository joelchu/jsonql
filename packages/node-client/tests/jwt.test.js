const test = require('ava')
const fsx = require('fs-extra')
const { join } = require('path')
const debug = require('debug')('jsonql-node-client:test:jwt')

const server = require('./fixtures/server-with-auth')
const nodeClient = require('../index')

const { contractKey, users } = require('./fixtures/options')
const contractDir = join(__dirname, 'fixtures', 'jwt')
const keysDir = join(__dirname, 'fixtures', 'keys')
const port = 8890;
const username = 'joel'

test.before(async t => {
  // make sure the folder is clean
  // fsx.removeSync(contractDir)
  const contract = fsx.readJsonSync(join(contractDir, 'contract.json'))
  const { stop } = await server(port, {
    contract,
    contractDir,
    keysDir,
    // enableAuth: true,
    loginHandlerName: 'customLogin',
    validatorHandlerName: 'customValidator'
  })

  t.context.stop = stop;

  const client = await nodeClient({
    namespaced: true,
    hostname:`http://localhost:${port}`,
    enableAuth: true,
    loginHandlerName: 'customLogin',
    contractDir,
    contractKey
  })

  t.context.client = client;
})

test.after(() => {
  // fsx.removeSync(contractDir)
})

test.serial("It should able to login and received a token", async t => {
  let client = t.context.client;
  let result = await client.customLogin(username, '1234')

  debug(result)

  t.truthy(result)

  let userdata = client.userdata()
  debug('userdata', userdata)
  t.truthy(userdata.name === username)

})

test.serial("It should able to use the token to login and query private methods", async t => {
  let client = t.context.client;
  let user = await client.query.getUser(1)
  t.is(user, users[1])
})

test.serial.cb('It should not be able to call the private method after we call logout', t => {
  t.plan(1)
  let client = t.context.client;

  client.logout()

  client.query.getUser(2)
    .catch(err => {
      debug('catch error', err)
      t.truthy(err)
      t.end()
    })

})
