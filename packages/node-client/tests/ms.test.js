// a new test to check if we have two setup on two different port
// what happen with the middleware
const test = require('ava')
const fsx = require('fs-extra')
const client = require('../index')
const server = require('./fixtures/two/server-setup')
const { join } = require('path')
const baseDir = join(__dirname, 'fixtures', 'two')
const chain = fn => Promise.resolve(fn)
const portA = 7007;
const portB = 8008;
const host = 'http://localhost:'
const contract_a = join(baseDir, 'tmp', 'a')
const contract_b = join(baseDir, 'tmp', 'b')

const debug = require('debug')('jsonql-node-client:test:contract')

test.before(async t => {
  t.context.client_A = await chain(
    server(portA, join(baseDir, 'resolvera'), contract_a)
  ).then(a => {
    let { name } = a;
    let app = a[name].app
    t.context.app_A = app
    t.context.stop_A = a[name].stop
    return app;
  }).then(app => {
    return client({
      namespaced: true,
      hostname: `${host}${portA}`,
      contractDir: join(baseDir, 'tmp', 'a', 'client')
    })
  })

  t.context.client_B = await chain(
    server(portB, join(baseDir, 'resolverb'), contract_b)
  ).then(b => {
    let { name } = b;
    let app = b[name].app
    t.context.app_B = app
    t.context.stop_B = b[name].stop
    return app
  }).then(app => {
    return client({
      namespaced: true,
      hostname: `${host}${portB}`,
      contractDir: join(baseDir, 'tmp', 'b', 'client')
    })
  })
})

test.after(t => {

  t.context.stop_A()
  t.context.stop_B()

  fsx.removeSync(contract_a)
  fsx.removeSync(contract_b)
})

test(`It should able to say hello to server B`, async t => {
  const c = t.context.client_B;
  const result1 = await c.query.helloWorld()
  t.truthy(result1.indexOf('world'))
  const result2 = await c.query.getterB()
  t.is(result2, 'B')
})

test(`It should able to say hello to server A`, async t => {
  const c = t.context.client_A;
  const result1 = await c.query.helloWorld()
  t.truthy(result1.indexOf('world'))
  const result2 = await c.query.getterA()
  t.is(result2, 'A')
})

test(`It should able to call server A mutation`, async t => {
  const c = t.context.client_A
  const result1 = await c.mutation.saverA('A')
  t.is(result1, true)
})

test(`It should able to call server B mutation`, async t => {
  const c = t.context.client_B
  const result1 = await c.mutation.saverB('B')
  t.is(result1, true)
})
