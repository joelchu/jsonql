const server = require('server-io-core')
const jsonqlKoa = require('./jsonql-koa')
const { contractKey } = require('./options')
const { join } = require('path')

module.exports = function serverWithAuth(port = 8889, extra = {}) {
  return server({
    port: port,
    debug: false,
    open: false,
    reload: false,
    middlewares: [
      jsonqlKoa(Object.assign({
        contractKey,
        resolverDir: join(__dirname,'resolvers'),
        contractDir: join(__dirname,'contract','tmp','server-with-auth'),
        keysDir: join(__dirname, 'keys'),
        enableAuth: true
      }, extra))
    ]
  })
}
