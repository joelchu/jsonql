/**
 * simeple save B
 * @param {string} payload
 * @return {boolean} when match
 */
module.exports = function saverB(payload) {
  return payload === 'B'
}
