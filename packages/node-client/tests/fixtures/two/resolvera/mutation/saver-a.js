/**
 * simeple save A
 * @param {string} payload
 * @return {boolean} when match
 */
module.exports = function saverB(payload) {
  return payload === 'A'
}
