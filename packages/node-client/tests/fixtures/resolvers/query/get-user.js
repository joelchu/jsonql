// const debug = require('debug')('jsonql-node-client:test:get-user');
const { users, msg } = require('../../options')
const { JsonqlResolverAppError } = require('jsonql-errors')

/**
 * get user call
 * @param {number} id user id
 * @return {object|string} user object on ok
 */
module.exports = function getUser(id) {
  const index = parseInt(id, 10)
  const ctn = users.length;
  if (index > ctn) {
    // @TODO we should try throw different kind of error and see what happen
    // by default if the resolver throw an error that is no specify
    // it should always be JsonqlResolverAppError
    throw new JsonqlResolverAppError('Index exceeds the size of array')
  }
  return users[ index ]
}
