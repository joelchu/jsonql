const debug = require('debug')('jsonql-node-client:test:send-user');
const merge = require('lodash.merge');
/**
 * @param {object} payload
 * @param {object} condition
 * @return {object} two merge together
 */
module.exports = function sendUser(payload, condition) {
  return merge({timestamp: Date.now()}, payload, condition)
}
