// for testing the server with socket
const { join } = require('path')
const server = require('server-io-core')
const jsonqlKoa = require('./jsonql-koa')

const { jsonqlWsServer } = require('jsonql-ws-server')

// output
module.exports = function serverWithSocket(extra = {}) {
  const config = Object.assign({
    resolverDir: join(__dirname,'resolvers'),
    // resuse this contract if there is one
    contractDir: join(__dirname,'contract','server-with-auth'),
    keysDir: join(__dirname, 'keys'),
    enableAuth: true
  }, extra)
  const port = 9001;
  return Promise.resolve(
    server({
      port: port,
      debug: false,
      open: false,
      reload: false,
      socket: false,
      middlewares: [
        jsonqlKoa(config)
      ]
    })
  ).then(({webserver, stop}) => (
    jsonqlWsServer(config, webserver)
      .then(() => ({stop, config, port}))
  ))
}
