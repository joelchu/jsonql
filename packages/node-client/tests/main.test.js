const test = require('ava')
const { join } = require('path')
const server = require('./fixtures/server')
// const options = require('./fixtures/options')
const nodeClient = require('../index')
const { getRemoteContract } = require('../src/base/get-remote-contract')


const debug = require('debug')('jsonql-node-client:test:main')

const hostname = 'http://localhost:8888'
const contractDir = join(__dirname,'fixtures','contract','tmp', 'client')

test.before(async t => {
  // create server
  const { stop } = await server()
  t.context.stop = stop
  // create client
  t.context.client = await nodeClient({
    hostname,
    contractDir 
  })

})

test.after(async t => {
  t.context.stop()
})

/*
test(`Just to cause the jsonql-koa to run`, t => {
  t.pass()
})
*/

test('Should able to say Hello world using the shorthand version!' , async t => {
  // debug(t.context.client);
  const result = await t.context.client.helloWorld()
  t.is('Hello world!', result)
})

test(`It should able to access the mutation call using the new non-namspaced option`, async t => {

  const result = await t.context.client.sendUser({name: 'Joel'}, {id: 1})

  t.truthy(result.timestamp)
})

test(`Test the getRemoteContract method`, async t => {

  const result = await getRemoteContract({
    remote: [hostname, 'jsonql'].join('/'),
    out: join(contractDir, 'remote')
  }, (...args) => {
    debug('cb', args)
  })

  // debug('getRemoteContractResult', result)
  t.truthy(result)
})
