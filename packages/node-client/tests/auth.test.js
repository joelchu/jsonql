const test = require('ava')
const { join } = require('path')
const fs = require('fs')
const fsx = require('fs-extra')
const debug = require('debug')('jsonql-node-client:test:auth')
const { PUBLIC_CONTRACT_FILE_NAME } = require('jsonql-constants')
const server = require('./fixtures/server-with-auth')
const nodeClient = require('../index')
const contractDir =join(__dirname, 'fixtures','contract','tmp' ,'client-with-auth')
const { contractKey, loginToken, token } = require('./fixtures/options')

const { JsonqlAuthorisationError } = require('jsonql-errors')

test.before(async (t) => {
  const { stop } = await server()
  t.context.stop = stop;

  const client = await nodeClient({
    namespaced: true,
    hostname:'http://localhost:8889',
    enableAuth: true,
    contractDir,
    contractKey
  })
  // we need to login here otherwise its hard to test the next call in sequence
  let { login } = client;
  // t.is(true, typeof login === 'function', 'check if the auth method generate correctly')
  let result = await login(loginToken)
  t.context.client = client;
})

test.after(t => {
  t.context.stop()
  fsx.removeSync(contractDir)
})

test.cb("Try a wrong password and cause the server to throw error", t => {
  t.plan(1)
  let client = t.context.client;
  client.login('wrong-password')
    .catch(err => {
      t.is(err.className, 'JsonqlAuthorisationError')
      t.end()
    })
})

test('Testing the login function', async (t) => {
  // let client = t.context.client;
  t.is(true, fs.existsSync(join(contractDir, PUBLIC_CONTRACT_FILE_NAME)), 'verify the public contract file is generated')
  // remove the compare because the token is not the same anymore
  // t.truthy(result, 'verify the return token is what we expected')
})

test.only(`Test if the login client token is persist`, async t => {
  let client = t.context.client;
  let { getUser } = client.query;

  let user = await getUser(1)

  t.is(user, 'Davide')
})
