// testing if the rsa generate key pair are working or not
const test = require('ava')
const { join } = require('path')
const fsx = require('fs-extra')
const debug = require('debug')('jsonql-jwt:test:rsa-keys')
const { jwtRsaToken, jwtDecode, rsaPemKeys } = require('../main')
const payload = {name: 'Joel', msg: 'Hello world!'};
const baseDir = join(__dirname, 'fixtures', 'keys')
const tmp = join(__dirname, 'fixtures', 'tmp')
// start test
test.before( t => {
  t.context.publicKey = fsx.readFileSync(join(baseDir, 'publicKey.pem'))
  t.context.privateKey = fsx.readFileSync(join(baseDir, 'privateKey.pem'))
})

test.after( t => {
  fsx.removeSync(join(tmp, 'publicKey.pem'))
  fsx.removeSync(join(tmp, 'privateKey.pem'))
})

test("It should able to use the privateKey encrypt a message and decrypt with the publicKey", t => {
  const token = jwtRsaToken(payload, t.context.privateKey)
  debug('token', token)

  const decodedPayload = jwtDecode(token, t.context.publicKey)
  debug('decodedPayload', decodedPayload)

  t.true(decodedPayload.name === payload.name)
})

test("It should able to generate rsa keys pair and output to folder", async t => {
  const files = await rsaPemKeys(1024, tmp)
  debug('files', files)
  t.truthy(files.publicKey && files.privateKey)
})
