// testing the browser jwt-decode

const test = require('ava')
const { join } = require('path')
const fsx = require('fs-extra')
// const jwtDecode = require('jwt-decode')
const debug = require('debug')('jsonql-jwt:test:jwt-decode')
// const { RSA_ALGO } = require('jsonql-constants')
const {
  loginResultToJwt,
  createTokenValidator,
  decodeToken,
  tokenValidator
} = require('../main')

const payload = {name: 'Joel', msg: 'Hello world!'}
const baseDir = join(__dirname, 'fixtures', 'keys')
// start test
test.before( t => {
  let publicKey = fsx.readFileSync(join(baseDir, 'publicKey.pem'))
  t.context.privateKey = fsx.readFileSync(join(baseDir, 'privateKey.pem'))

  t.context.validator = createTokenValidator({
    useJwt: true,
    publicKey
  })

  t.context.publicKey = publicKey
  const options = {exp: 5} // expired in 5 seconds
  const token = loginResultToJwt(t.context.privateKey, options)(payload)
  t.context.token = token
});

test('try to see if its able to decode the token encrypted by rsa key', t => {

  let decodedPayload1 = t.context.validator(t.context.token)
  t.true(decodedPayload1.name === payload.name)

})

test('Check if the tokenValidator able to return the correct configuration', t => {
  let time = Math.floor(Date.now() / 1000) + (60 * 60)
  let config = { exp: time }
  let opts = tokenValidator(config)

  debug(opts)

  t.is(time, opts.expiresIn)
})

test('Check if the validation works inside the decodeToken when token is expired' , t => {
  // t.plan(1)
  const fn = () => decodeToken(t.context.token)
  setTimeout(() => {
    t.throws(() => {
      fn()
    }, null, 'Expect the decodeToken throw error because the token has expired')
  }, 6000)

})
