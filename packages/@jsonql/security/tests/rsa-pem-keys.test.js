// test generate key pairs
const test = require('ava')
const { join, resolve } = require('path')
const fsx = require('fs-extra')
const { spawn } = require('child_process')
const debug = require('debug')('jsonql-jwt:test:rsa-pem-keys')

const outputDir = join(__dirname, 'fixtures', 'tmp', 'pem-test')

const pubKey = join(outputDir, 'publicKey.pem')
const prvKey = join(outputDir, 'privateKey.pem')

test.after( t => {
  // fsx.removeSync(outputDir)
})

test.cb('It should able to generate pair of public and private keys', t => {
  t.plan(2)

  t.context.ps = spawn('node', [
    resolve(join(__dirname, '..' , 'cmd.js')),
    'rsa-pem',
    '--outputDir',
    outputDir
  ])

  t.context.ps.stdout.on('data', data => {
    debug('stdout: ', data.toString())
  })

  t.context.ps.stderr.on('data', data => {
    debug('stderr: ', data.toString())
  })

  t.context.ps.on('close', code => {
    debug(`(1) Exited with ${code}`)

    t.truthy(fsx.existsSync(pubKey))
    t.truthy(fsx.existsSync(prvKey))

    t.end()
  })


})
