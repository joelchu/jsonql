// testing the cache with the associate methods 
const test = require('ava')
const { 
  getCache,
  isCacheObj,
  createCSRFToken,
  isCSRFTokenExist
} = require('../src/cache')

const debug = require('debug')('jsonql-security:cache')

test.before(t => {
  t.context.store = getCache()
})


test(`Test the CSRF token get and store`, t => {
  // test it without the cache object
  const token = createCSRFToken(t.context.store)

  t.truthy(token, 'Should able to generate token')

  const existed = isCSRFTokenExist(t.context.store, token)

  t.true(existed, 'The token should existed')

})

test(`Test the new JsonqlStore features`, t => {
  const store = getCache()

  const check = isCacheObj(store)

  t.true(check)

  const key = 'some-fn'
  function callme(msg) {
    return `Hello yes, I got your: ${msg}`
  }

  const result1 = store.set(key, callme)
  debug('take a look at what return', result1)

  const fn1 = store.get(key)
  const msg1 = fn1('moron')

  t.truthy(msg1.indexOf('moron'))

  t.true(typeof fn1 === 'function')

  const fn2 = store.get(key)
  const msg2 = fn2('indiot!')
  
  t.truthy(msg2.indexOf('indiot'))

})
