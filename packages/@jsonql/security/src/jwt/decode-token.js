'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var jsonqlParamsValidator = require('jsonql-params-validator');

// same with the invalid-token-error 

/*
function InvalidCharacterError(message) {
  this.message = message;
}

InvalidCharacterError.prototype = new Error();
InvalidCharacterError.prototype.name = 'InvalidCharacterError';

*/

var InvalidCharacterError = /*@__PURE__*/(function (Error) {
  function InvalidCharacterError(message) {
    this.message = message; 
  }

  if ( Error ) InvalidCharacterError.__proto__ = Error;
  InvalidCharacterError.prototype = Object.create( Error && Error.prototype );
  InvalidCharacterError.prototype.constructor = InvalidCharacterError;

  var prototypeAccessors = { name: { configurable: true } };

  prototypeAccessors.name.get = function () {
    return 'InvalidCharacterError'
  };

  Object.defineProperties( InvalidCharacterError.prototype, prototypeAccessors );

  return InvalidCharacterError;
}(Error));

/**
 * The code was extracted from:
 * https://github.com/davidchambers/Base64.js
 */
var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

/**
 * Polyfill the non ASCII code 
 * @param {*} input
 * @return {*} usable output 
 */
function atob(input) {
  var str = String(input).replace(/=+$/, '');
  if (str.length % 4 == 1) {
    throw new InvalidCharacterError("'atob' failed: The string to be decoded is not correctly encoded.")
  }
  for (
    // initialize result and counters
    var bc = 0, bs = (void 0), buffer = (void 0), idx = 0, output$1 = '';
    // get next character
    buffer = str.charAt(idx++);
    // character found in table? initialize bit storage and add its ascii value;
    ~buffer && (bs = bc % 4 ? bs * 64 + buffer : buffer,
      // and if not first of each 4 characters,
      // convert the first 8 bits to one ascii character
      bc++ % 4) ? output$1 += String.fromCharCode(255 & bs >> (-2 * bc & 6)) : 0
  ) {
    // try to find character in table (0-63, not found => -1)
    buffer = chars.indexOf(buffer);
  }
  return output
}

// polyfill the window object
try {
  typeof window !== 'undefined' && window.atob && window.atob.bind(window) || atob;
} catch(e) {}

// handle the actual base64 decode 

/**
 * Use this when encounter non ASCII payload 
 * @param {string} str the token
 * @return {*} decoded payload  
 */
function b64DecodeUnicode(str) {
  return decodeURIComponent(atob(str).replace(/(.)/g, function (m, p) {
    var code = p.charCodeAt(0).toString(16).toUpperCase();
    if (code.length < 2) {
      code = '0' + code;
    }
    return '%' + code
  }))
}

/**
 * the main interface to handle the base64 url decode 
 * @param {string} str the token 
 * @return {*} decoded base64 payload  
 */
function base64UrlDecode(str) {
  var output = str.replace(/-/g, "+").replace(/_/g, "/");
  switch (output.length % 4) {
    case 0:
      break;
    case 2:
      output += "==";
      break;
    case 3:
      output += "=";
      break;
    default:
      throw new Error("Illegal base64url string!")
  }

  try{
    return b64DecodeUnicode(output)
  } catch (err) {
    return atob(output)
  }
}

// rewrote this in class syntax instead of the function prototype 

/*
function InvalidTokenError(message) {
  this.message = message
}

InvalidTokenError.prototype = new Error()
InvalidTokenError.prototype.name = 'InvalidTokenError'
*/

var InvalidTokenError = /*@__PURE__*/(function (Error) {
  function InvalidTokenError(message) {
    this.message = message; 
  }

  if ( Error ) InvalidTokenError.__proto__ = Error;
  InvalidTokenError.prototype = Object.create( Error && Error.prototype );
  InvalidTokenError.prototype.constructor = InvalidTokenError;

  var prototypeAccessors = { name: { configurable: true } };

  prototypeAccessors.name.get = function () {
    return 'InvalidTokenError'
  };

  Object.defineProperties( InvalidTokenError.prototype, prototypeAccessors );

  return InvalidTokenError;
}(Error));

// JWT library entry point 


/**
 * the main interface to decode a jwt token 
 * @param {string} token 
 * @param {object} options
 * @return {*} decoded token payload  
 */
function jwtDecode(token,options) {
  if (typeof token !== 'string') {
    throw new InvalidTokenError('Invalid token specified')
  }

  options = options || {};
  var pos = options.header === true ? 0 : 1;
  try {
    return JSON.parse(base64UrlDecode(token.split('.')[pos]))
  } catch (e) {
    throw new InvalidTokenError('Invalid token specified: ' + e.message)
  }
}

// we are going to replace the dependencey of lodash-es 

// ensure this method is available in the Object
Object.getPrototypeOf || (Object.getPrototypeOf=function(obj){
  return obj.__proto__ || obj.prototype || (obj.constructor&&obj.constructor.prototype) || Object.prototype
});

// bunch of generic helpers

var isString = function (s) { return typeof s === 'string'; };

/**
 * @param {boolean} sec return in second or not
 * @return {number} timestamp
 */
var timestamp = function (sec) {
  if ( sec === void 0 ) sec = false;

  var time = Date.now();
  return sec ? Math.floor( time / 1000 ) : time
};

var NO_STATUS_CODE = -1;

/**
 * This is a custom error to throw whenever a error happen inside the jsonql
 * This help us to capture the right error, due to the call happens in sequence
 * @param {string} message to tell what happen
 * @param {mixed} extra things we want to add, 500?
 */
var JsonqlError = /*@__PURE__*/(function (Error) {
  function JsonqlError() {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

    Error.apply(this, args);

    this.message = args[0];
    this.detail = args[1];

    this.className = JsonqlError.name;

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, JsonqlError);
      // this.detail = this.stack;
    }
  }

  if ( Error ) JsonqlError.__proto__ = Error;
  JsonqlError.prototype = Object.create( Error && Error.prototype );
  JsonqlError.prototype.constructor = JsonqlError;

  var staticAccessors = { name: { configurable: true },statusCode: { configurable: true } };

  staticAccessors.name.get = function () {
    return 'JsonqlError'
  };

  staticAccessors.statusCode.get = function () {
    return NO_STATUS_CODE
  };

  Object.defineProperties( JsonqlError, staticAccessors );

  return JsonqlError;
}(Error));

// when the user is login with the jwt

/**
 * We only check the nbf and exp
 * @param {object} token for checking
 * @return {object} token on success
 */
function validate(token) {
  var start = token.iat || timestamp(true);
  // we only check the exp for the time being
  if (token.exp) {
    if (start >= token.exp) {
      var expired = new Date(token.exp).toISOString();
      throw new JsonqlError(("Token has expired on " + expired), token)
    }
  }
  return token
}

/**
 * The browser client version it has far fewer options and it doesn't verify it
 * because it couldn't this is the job for the server
 * @TODO we need to add some extra proessing here to check for the exp field
 * @param {string} token to decrypted
 * @return {object} decrypted object
 */
function jwtDecode$1(token) {
  if (isString(token)) {
    var t = jwtDecode(token);
    return validate(t)
  }
  throw new JsonqlError('Token must be a string!')
}

/* base.js */
var OPTIONAL_KEY = 'optional';
var ALIAS_KEY = 'alias';
var HSA_ALGO = 'HS256';
var STRING_TYPE = 'string';
var BOOLEAN_TYPE = 'boolean';

var NUMBER_TYPE = 'number';

var obj, obj$1, obj$2, obj$3, obj$4, obj$5, obj$6, obj$7, obj$8;

var appProps = {
  algorithm: jsonqlParamsValidator.createConfig(HSA_ALGO, [STRING_TYPE]),
  expiresIn: jsonqlParamsValidator.createConfig(false, [BOOLEAN_TYPE, NUMBER_TYPE, STRING_TYPE], ( obj = {}, obj[ALIAS_KEY] = 'exp', obj[OPTIONAL_KEY] = true, obj )),
  notBefore: jsonqlParamsValidator.createConfig(false, [BOOLEAN_TYPE, NUMBER_TYPE, STRING_TYPE], ( obj$1 = {}, obj$1[ALIAS_KEY] = 'nbf', obj$1[OPTIONAL_KEY] = true, obj$1 )),
  audience: jsonqlParamsValidator.createConfig(false, [BOOLEAN_TYPE, STRING_TYPE], ( obj$2 = {}, obj$2[ALIAS_KEY] = 'iss', obj$2[OPTIONAL_KEY] = true, obj$2 )),
  subject: jsonqlParamsValidator.createConfig(false, [BOOLEAN_TYPE, STRING_TYPE], ( obj$3 = {}, obj$3[ALIAS_KEY] = 'sub', obj$3[OPTIONAL_KEY] = true, obj$3 )),
  issuer: jsonqlParamsValidator.createConfig(false, [BOOLEAN_TYPE, STRING_TYPE], ( obj$4 = {}, obj$4[ALIAS_KEY] = 'iss', obj$4[OPTIONAL_KEY] = true, obj$4 )),
  noTimestamp: jsonqlParamsValidator.createConfig(false, [BOOLEAN_TYPE], ( obj$5 = {}, obj$5[OPTIONAL_KEY] = true, obj$5 )),
  header: jsonqlParamsValidator.createConfig(false, [BOOLEAN_TYPE, STRING_TYPE], ( obj$6 = {}, obj$6[OPTIONAL_KEY] = true, obj$6 )),
  keyid: jsonqlParamsValidator.createConfig(false, [BOOLEAN_TYPE, STRING_TYPE], ( obj$7 = {}, obj$7[OPTIONAL_KEY] = true, obj$7 )),
  mutatePayload: jsonqlParamsValidator.createConfig(false, [BOOLEAN_TYPE], ( obj$8 = {}, obj$8[OPTIONAL_KEY] = true, obj$8 ))
};

/**
 * validate the token type 
 * @param {object} config options
 * @return {object} tested result  
 */
function tokenValidator(config) {
  if (!jsonqlParamsValidator.isObject(config)) {
    return {} // @TODO we just ignore it all together?
  }
  var result = {};
  var opts = jsonqlParamsValidator.checkConfig(config, appProps);
  // need to remove options that is false
  for (var key in opts) {
    if (opts[key]) {
      result[key] = opts[key];
    }
  }
  return result
}

exports.decodeToken = jwtDecode$1;
exports.tokenValidator = tokenValidator;
