const crypto = require('crypto')
const path = require('path')
const fs = require('fs-extra')
const { BASE64_FORMAT, UTF8_FORMAT } = require('jsonql-constants')

/**
 * Encrypt data with a public key in pem file format
 * @param {*} toEncrypt data to encypt
 * @param {string} relativeOrAbsolutePathToPublicKey path to pem public key file
 */
function encryptWithPublicPem(toEncrypt, relativeOrAbsolutePathToPublicKey) {
  const absolutePath = path.resolve(relativeOrAbsolutePathToPublicKey)
  const publicKey = fs.readFileSync(absolutePath, UTF8_FORMAT)
  const buffer = Buffer.from(toEncrypt, UTF8_FORMAT)
  const encrypted = crypto.publicEncrypt(publicKey, buffer)
  return encrypted.toString(BASE64_FORMAT)
}

/**
 * Decrypt the data with the public key in pem file format
 * @param {*} toDecrypt data to decrypt
 * @param {string} relativeOrAbsolutePathtoPrivateKey path to pem private key file
 */
function decryptWithPrivatePem(toDecrypt, relativeOrAbsolutePathtoPrivateKey) {
  const absolutePath = path.resolve(relativeOrAbsolutePathtoPrivateKey)
  const privateKey = fs.readFileSync(absolutePath, UTF8_FORMAT)
  const buffer = Buffer.from(toDecrypt, BASE64_FORMAT)
  const decrypted = crypto.privateDecrypt(
    {
      key: privateKey.toString(),
      passphrase: '',
    },
    buffer,
  )
  return decrypted.toString(UTF8_FORMAT)
}


/**
 * not sure how to use it at the moment, but just put it here 
 * @param {*} str 
 * @param {*} publicKey 
 */
function signObject(str, publicKey) {
  const SIGN_KEY_NAME = 'RSA-SHA256'
  
  const signerObject = crypto.createSign(SIGN_KEY_NAME)
  
  signerObject.update(str)
  
  const signature = signerObject.sign({
    key: privateKey,
    padding: crypto.constants.RSA_PKCS1_PSS_PADDING
  }, BASE64_FORMAT)

  console.info("signature: %s", signature)
  
  //verify String
  const verifierObject = crypto.createVerify(SIGN_KEY_NAME)
  
  verifierObject.update(str)

  const verified = verifierObject.verify({
    key: publicKey,
    padding: crypto.constants.RSA_PKCS1_PSS_PADDING
  }, signature, BASE64_FORMAT)
  
  console.info("is signature ok?: %s", verified)

  return verified
}


module.exports = {
  signObject,
  encryptWithPublicPem,
  decryptWithPrivatePem
}
