# JWT library 

This is the client side jwt-decode, using [jwt-decode](https://npmjs.com/package/jwt-decode)

Rewritten using ES6.

The main change is using named export (rollup keep complaining about missing default export)

```js

import { jwtDecode } from './jwt'

// do your thing
const payload = jwtDecode(token, {})

```