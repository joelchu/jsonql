// same with the invalid-token-error 

/*
function InvalidCharacterError(message) {
  this.message = message;
}

InvalidCharacterError.prototype = new Error();
InvalidCharacterError.prototype.name = 'InvalidCharacterError';

*/

class InvalidCharacterError extends Error {

  constructor(message) {
    this.message = message 
  }

  get name() {
    return 'InvalidCharacterError'
  }
}

export { InvalidCharacterError }