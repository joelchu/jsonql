// new node side cache module using node-cache and re-use everywhere
const { getCache, isCacheObj } = require('./get-cache')
const { getToken, createCSRFToken, isCSRFTokenExist } = require('./csrf-methods')

module.exports = {
  getCache,
  isCacheObj,
  getToken,
  createCSRFToken,
  isCSRFTokenExist
}