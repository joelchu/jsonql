// here is a problem 
// because we need to use global property to store the node-cache instance 
// how do we generate a dynamic key wihtout losing it? 

/*
stdTTL: (default: 0) the standard ttl as number in seconds for every generated cache element. 0 = unlimited
checkperiod: (default: 600) The period in seconds, as a number, used for the automatic delete check interval. 0 = no periodic check.
useClones: (default: true) en/disable cloning of variables. If true you'll get a copy of the cached variable. If false you'll save and get just the reference.
Note:
true is recommended if you want simplicity, because it'll behave like a server-based cache (it caches copies of plain data).
false is recommended if you want to achieve performance or save mutable objects or other complex types with mutability involved and wanted, because it'll only store references of your data.
Here's a simple code example showing the different behavior
deleteOnExpire: (default: true) whether variables will be deleted automatically when they expire. If true the variable will be deleted. If false the variable will remain. You are encouraged to handle the variable upon the event expired by yourself.
enableLegacyCallbacks: (default: false) re-enables the usage of callbacks instead of sync functions. Adds an additional cb argument to each function which resolves to (err, result). will be removed in node-cache v6.x.
maxKeys: (default: -1) specifies a maximum amount of keys that can be stored in the cache. If a new item is set and the cache is full, an error is thrown and the key will not be saved in the cache. -1 disables the key limit.
*/

// using node-cache to store can be throw away data
// here we implement a simple store to store the CSRF token
// in this structure {CSRF-TOKEN: TIMESTAMP}
// so you can check if this key actually exist or not
// const debug = require('debug')('jsonql-ws-server-core:share:get-node-cache')
// const NodeCache = require('node-cache')
// need to rethink should we do it like this
// global.jsonqlLocalNodeCacheStore = {} 
const { JsonqlStore } = require('@jsonql/store')
/**
 * We dont' use the global property like before anymore
 * Because that could be a security risk
 * @param {objec} opts configuration
 * @return {object} the nodeCache instance
 */
function getCache(opts = {}) {
  return new JsonqlStore(opts)
} 

/**
 * check if the store is DiyCache
 * @param {*} store 
 */
function isCacheObj(store) {
  return store instanceof JsonqlStore
} 

module.exports = { 
  getCache,
  isCacheObj
}