// collection of WebSocket extra token for server side 
// ported from ws-server
const { 
  TOKEN_PARAM_NAME,
  TOKEN_DELIVER_LOCATION_PROP_KEY, 
  TOKEN_IN_URL,
  TOKEN_IN_HEADER,
  AUTH_CHECK_HEADER,
  BEARER
} = require('jsonql-constants')
const url = require('url')
const { parse } = require('querystring')
const { extractHeaderValue } = require('./index.cjs')
/**
 * What the name said
 * @param {string} uri to decode
 * @return {string|boolean} token or false means didn't find it
 */
function getTokenFromUrl(uri) {
  const { query } = url.parse(uri)
  if (query) {
    const params = parse(query)
    
    return params[TOKEN_PARAM_NAME] || false
  }
  return false
}

/**
 * Check if there is the Bearer in the header string as well 
 * @param {string} header extracted Authorisation header 
 * @return {string} token 
 */
function processAuthTokenHeader(header) {

  return header && typeof header === 'string' ? header.replace(BEARER, '').trim() : false
}

/**
 * new method to get the token from the header 
 * @param {*} req request object
 * @return {string|boolean} token, false when failed to find it
 */
function getTokenFromHeader(headers) {
  const result = extractHeaderValue(headers, AUTH_CHECK_HEADER)
  
  return processAuthTokenHeader(result) 
}

/**
 * New combine get token method using the options to switch
 * @param {object} opts configuration  
 * @param {object} req request object
 * @return {string|boolean} false on failed
 */
function getWsAuthToken(opts, req) {
  switch (opts[TOKEN_DELIVER_LOCATION_PROP_KEY]) {
    case TOKEN_IN_URL:
      if (req.url) {
        return getTokenFromUrl(req.url)
      }
    case TOKEN_IN_HEADER:
      if (req.headers) {
        console.log(req.headers, opts)
        return getTokenFromHeader(req.headers)
      }
    default:

      return false 
  }
}


module.exports = {
  getTokenFromUrl,
  processAuthTokenHeader,
  getTokenFromHeader,
  getWsAuthToken
}

