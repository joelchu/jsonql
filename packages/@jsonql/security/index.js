// main export interface for client side modules
// main export interface for client side modules
// rename this to module and make sure we import just this 
import {
  decodeToken,
  tokenValidator
} from './src/client'
import {
  extractConfig,
  prepareConnectConfig
} from './src/socket'
/* @TODO do we need this on the client side? 
import {
  isCacheObj,
  getCache   
} from './src/cache'
*/

export {
  extractConfig,
  prepareConnectConfig,
  decodeToken,
  tokenValidator
}
