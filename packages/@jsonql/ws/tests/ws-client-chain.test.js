// testing the chain methods
const test = require('ava')
const { join } = require('path')
const fsx = require('fs-extra')
const { chainPromises } = require('jsonql-utils')
const { setupWebsocketClientFn } = require('../src/core/setup-connect-client/setup-websocket-client-fn')
const createConfig = require('./fixtures/create-config')

const WebSocket = require('ws')

const wsNodeAuthClient = setupWebsocketClientFn(WebSocket, 'node', true)
const wsNodeClient = setupWebsocketClientFn(WebSocket, 'node')

const serverSetup = require('./fixtures/server-setup')
const genToken = require('./fixtures/token')

const contractDir = join(__dirname, 'fixtures', 'contract', 'auth')
const contract = fsx.readJsonSync(join(contractDir, 'contract.json'))

const payload = {name: 'Joel'}
const token = genToken(payload)

const rainbow = require('./fixtures/rainbow')
const debug = require('debug')('jsonql-ws-client:test:ws-client-chain')

const port = 8005

test.before(async t => {
  const { app } = await serverSetup({
    contract,
    contractDir,
    resolverDir: join(__dirname, 'fixtures', 'resolvers'),
    serverType: 'ws',
    enableAuth: true,
    keysDir: join(__dirname, 'fixtures', 'keys')
  })

  t.context.server = app.listen(port)

  let baseUrl = `ws://localhost:${port}`
  t.context.nsp1url = [baseUrl, 'jsonql/private'].join('/')
  t.context.nsp2url = [baseUrl, 'jsonql/public'].join('/')
  // TBC
  t.context.config = await createConfig({
    enableAuth: true, 
    debugOn: true,
    hostname: baseUrl
  })
})

test.serial.cb('First test the connection individually', t => {
  t.plan(2)
  
  const opts = t.context.config 

  wsNodeAuthClient(t.context.nsp1url, opts, token)
    .then(ws1 => {
      ws1.onopen = function() {
        debug('ws1 onopen')
        t.pass()
        ws1.terminate()
      }
    })
  
  wsNodeClient(t.context.nsp2url, opts)
    .then(ws2 => {
      debug(ws2)
      ws2.onopen = function() {

        debug('ws2 onopen')

        t.pass()
        ws2.terminate()
        t.end()
      }
    })
})

// @BUG whenever I wrap this code in the promise the ws just hang up
// @TODO The bug still here after we port it over

test.serial.cb('Try to create a promise based ws client and using the chainPromises method to login', t => {
  t.plan(1)

  const opts = t.context.config 

  const { nsp1url, nsp2url } = t.context

  let p1 = () => (
    wsNodeAuthClient(nsp1url, opts, token)
  )
  let p2 = () => (
    wsNodeClient(nsp2url, opts)
  )

  chainPromises([p1(), p2()])
    .then(nsps => {
      t.is(nsps.length, 2)
      t.end()
    })
    .then(nsps => {
      nsps.map(nsp => {
        rainbow(nsp.url)
        nsp.terminate()
      })
    })
    .catch(err => {
      debug(err)
      t.end()
    })

})
