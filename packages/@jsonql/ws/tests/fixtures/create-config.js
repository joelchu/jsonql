// we need to create a configuration to do a break down test 

const { 
  createCombineConfigCheck, 
  // postCheckInjectOpts,
  // createRequiredParams
} = require('../../src/core/modules')

const {
  wsClientCheckMap, 
  wsClientConstProps 
} = require('../../src/options')

const debug = require('debug')('jsonql-ws-client:fixtures/create-config')

module.exports = function(config) {
  
  const constProps = Object.assign(wsClientConstProps, {log: debug})

  const fn = createCombineConfigCheck(
    wsClientCheckMap, 
    constProps,
    true
  )

  return fn(config)
    .then(opts => {
      // debug('fixtures:createConfig', opts)

      return opts 
    })
    .catch(err => {
      debug('init config error', err)
    })
}