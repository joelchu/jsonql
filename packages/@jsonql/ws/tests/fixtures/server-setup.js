
const { join } = require('path')

const resolverDir = join(__dirname, 'resolvers')
const contractDir = join(__dirname, 'contract')

const { jsonqlWsStandaloneServer } = require('../../../../ws-server')
// require('jsonql-ws-server')

module.exports = function(extra = {}) {
  const config = Object.assign({
    resolverDir,
    contractDir,
    serverType: 'ws'
  }, extra)

  return jsonqlWsStandaloneServer(config, false)
      .then(({ws, server}) => {
        return {
          io: ws,
          app: server
        }
      })
}
