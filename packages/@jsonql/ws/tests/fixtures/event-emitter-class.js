const NBEventService = require('nb-event-service')
// create our own for testing integration 
class LocalEventEmitter extends NBEventService {
  constructor(logger) {
    super({ logger })
  }

  get name() {
    return 'test-local-event-service'
  }
}

module.exports = { LocalEventEmitter }