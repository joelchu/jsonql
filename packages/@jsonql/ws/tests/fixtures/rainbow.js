// create a rainbow console.log 
const colors = require('colors/safe')
module.exports = function rainbow(...args) {
  let newArgs = [
    colors.rainbow(args[0])
  ]
  args.splice(0, 1)
  Reflect.apply(console.log, console, newArgs.concat(args))
}
