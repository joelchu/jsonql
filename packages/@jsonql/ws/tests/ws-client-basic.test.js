// standard ws client test without auth
const test = require('ava')
const { join } = require('path')
const fsx = require('fs-extra')

const wsClient = require('../node-ws-client')
const serverSetup = require('./fixtures/server-setup')

const contractDir = join(__dirname, 'fixtures', 'contract')
const publicContract = fsx.readJsonSync(join(contractDir, 'public-contract.json'))
const contract = fsx.readJsonSync(join(contractDir, 'contract.json'))

const colors = require('colors/safe')
const _debug = require('debug')('jsonql-ws-client:test:basic')

const debug = (str, ...args) => {
  Reflect.apply(_debug, null, [colors.black.bgBrightGreen(str)].concat(args))
}

const port = 8004
// prepare
test.before(async t => {
  const { app } = await serverSetup({
    contract
  })
  t.context.server = app.listen(port)
  t.context.client = await wsClient({
    hostname: `ws://localhost:${port}`,
    contract: publicContract
  }, {log: debug}) 
})
// finish
test.after(t => {
  t.context.server.close()
})
// start test

test.cb('It should able to connect to the ws server', t => {
  t.plan(2)
  let client = t.context.client
  // this will get respond last
  client.simple(100)
    .then(result => {
      debug(`Got the first result`.rainbow, result)
      t.is(101, result)
      t.end()
    })

  client.simple.onResult = (result) => {
    debug(`Catch the onResult event`.rainbow, result)
    t.is(101, result)
  }
})

test.cb('It should able to handle error', t => {
  t.plan(1)
  let client = t.context.client

  client.throwError()

  client.throwError.onError = (error) => {
    debug(error)
    t.truthy(error)
    t.end()
  }
})


test.cb('It should able to send message back while its talking to the server', t => {
  let testCtn = 3
  t.plan(testCtn)

  let client = t.context.client

  const evt = t.context.client.eventEmitter
  const evtName = `add-test-count` 
  evt.$on(evtName, () => {
    --testCtn 
    if (testCtn === 0) {
      t.end()
    }
    return testCtn
  })

  // debug(client)

  client.continuous('Jumping')
    .then(result => {
      t.pass()
      debug('<then.result> continous got result', result)
      evt.$trigger(evtName)
    })

  client.continuous.onResult = (msg) => {
    t.pass()
    debug('<onResult>', testCtn , msg)
    evt.$trigger(evtName) 
  }
  // add a event handler - @BUG never fired
  client.continuous.onMessage = function(msg) {
    
    
    debug('<onMessage>', testCtn , msg)

    client.continuous.send('terminate')

    t.pass()
    evt.$trigger(evtName)
    
  }
})
