// this will grab the list of test files
// inject into the html and run the server
const glob = require('glob')
const { join, resolve } = require('path')
const { jsonqlWsServer } = require('jsonql-ws-server')
const serverIoCore = require('server-io-core')

const debug = require('debug')('jsonql-ws-client:test:browser')

const baseDir = join(__dirname, 'files')
const appBaseDir = resolve(join(__dirname, '..', 'fixtures'))

const contractBaseDir = join(appBaseDir, 'contract')
const contractAuthDir = join(contractBaseDir, 'auth')

const resolverDir = join(appBaseDir, 'resolvers')
const keysDir = join(appBaseDir, 'keys')

const nodeEnv = process.env.NODE_ENV
let port = process.env.PORT 
if (!port) {
  port = nodeEnv === 'ws-auth' ? 8102 : 8101
}

const contractDir = nodeEnv === 'ws-auth' ? contractAuthDir : contractBaseDir

debug('port', process.env.PORT, port)

const getConfig = (env) => {
  let opts = {
    contractDir,
    resolverDir,
    keysDir,
    hostname: `ws://localhost:${port}`,
  }
  switch (env) {
    case 'ws-auth':
      opts.enableAuth = true
      opts.serverType = 'ws'
    break

    case 'ws':
    default:
      opts.serverType = 'ws'
    break
  }
  return opts
}

const wsServerSetup = (server) => {
  const wss = jsonqlWsServer(getConfig(nodeEnv), server)
  return wss // not in use
}

// just hardcode the file
const getInjectFile = (e) => {
  let file = process.env.FILE
  if (!file) {
    file = e === 'ws-auth' ?  'auth-test.js' : 'simple-test.js'
  }
  debug('file', file)
  return file
}

const runQunit = (open = true) => {
  return new Promise((resolver, rejecter) => {
    glob(join(baseDir, '*-test.js'), function(err, files) {
      if (err) {
        console.log('FAILED!', err)

        return rejecter(err)
      }
      // now start the server
      const { webserver, app, start } = serverIoCore({
        autoStart: false,
        port: port,
        webroot: [
          join(__dirname),
          join(__dirname, 'webroot'),
          join(__dirname, '..', '..', 'node_modules'),
          join(__dirname, '..', '..', 'dist')
        ],
        // open: open,
        // this will interface with jsonql-ws-server
        socket: false,
        reload: false,
        debugger: false,
        inject: {
          insertBefore: false,
          target: {
            body: [
              join('files', getInjectFile(nodeEnv))
            ]
          }
        }
      })
      wsServerSetup(webserver)
      // now we have a webserver then we could setup the jsonql-ws-server
      resolver({ webserver, app, start })
    })
  })
}

runQunit()
  .then(({ start }) => {
    start()
  })
