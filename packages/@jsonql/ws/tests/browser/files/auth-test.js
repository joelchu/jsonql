QUnit.test('It should able to connect to the auth enabled server', function(assert) {
  var done1 = assert.async()
  var token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiSm9lbCIsImlhdCI6MTU3MTkwODg5N30.Cp53a29nuFCKjEqUT8MmO2pAKsUVOHogBLuiGg-WOYetqgW48HvEudpO1KdBnZMzJPQcbdqVHVQlhv720GaYfE33ZOgC6o37LTTrPPKzXgynJ9kTfWI-Mct28teKN7SMKl2rACHn5OBogFkUE1zDB4wADoF77ClgnhehXkv-XlM"
  var jclient = jsonqlWsClient({
    hostname: 'http://localhost:8102',
    enableAuth: true,
    token: token,
    contract: authContract
  })

  jclient.then(function(client) {

    client.simple(1)

    client.simple.onResult = function(result) {
      console.log('result', 2)
      assert.equal(2, result, "Hello world test done")
      done1()
    }

    client.onLogin = function(namespace) {

      // console.info('myNamespace', client.simple.myNamespace)
      // console.info('namespace', namespace)

      // if (client.simple.myNamespace === namespace) {
        // console.info('Logged in')

        assert.equal('jsonql/private', namespace, "Expect the private namespace")
        done1()
      // }
    }
  })
  .catch(function(error) {
    console.error('init error', error)
  })

})
