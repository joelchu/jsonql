// standard ws client test without auth
const test = require('ava')
const { join } = require('path')
const fsx = require('fs-extra')
const rainbow = require('./fixtures/rainbow')
const wsClient = require('../node-ws-client')
const serverSetup = require('./fixtures/server-setup')
const genToken = require('./fixtures/token')

const contractDir = join(__dirname, 'fixtures', 'contract', 'auth')
const contract = fsx.readJsonSync(join(contractDir, 'contract.json'))
const publicContract = fsx.readJsonSync(join(contractDir, 'public-contract.json'))

const { NOT_LOGIN_ERR_MSG } = require('jsonql-constants')
const payload = {name: 'Joel'}

const _debug = require('debug')
const colors = require('colors/safe')

const debug = (str, ...args) => {
  const fn = _debug('jsonql-ws-client:test:login')

  Reflect.apply(fn , null, [colors.blue.bgBrightGreen(str), ...args])
}

const port = 8002

test.before(async t => {

  const { app } = await serverSetup({
    contract,
    contractDir,
    resolverDir: join(__dirname, 'fixtures', 'resolvers'),
    enableAuth: true,
    keysDir: join(__dirname, 'fixtures', 'keys')
  })
  t.context.server = app.listen(port)
  // without the token
  t.context.client = await wsClient({
    hostname: `ws://localhost:${port}`,
    contract: publicContract,
    enableAuth: true
  }, { 
    log: () => {} //debug 
  })
})

test.after(t => {
  t.context.server.close()
})
// access the public namespace

// @BUG the login is not quite 100% yet, but we need to test this more
// in the browser, the problem is this is bi-directional and
// when we add a client to connect to the server, it could create some
// strange effects. We might have to issue an LOGOUT before we attempt to
// LOGIN to clear out all the existing connections

test.serial.cb('It should able to connect to the ws server public namespace', t => {
  // let ctn = 0
  t.plan(3)
  let client = t.context.client

  rainbow('client.login', typeof client.login)

  client.pinging('Hello')
    .then(promiseResult => {
      debug(`==================== then result ======================`, promiseResult)
      t.is(promiseResult, 'connection established')
    })

  client.pinging.onResult = function testOneOnResultCallback(res) {
    debug('========================== res ==============================\n', res)
    t.is(res, 'connection established')
    client.pinging.send('ping')  
  }

  // the send is happen after the result return on the server side
  client.pinging.onMessage = function testOneOnMessageCallback(msg) {
    debug(`==============================  onMessage =========================== \n`, msg)

    if (msg==='pong') {
      client.pinging.send('pong')
    } else {
      client.pinging.send('giving up')
      debug('TEST ONE SHOULD HALT HERE')
      t.pass()
      t.end()
    }
  }
})


