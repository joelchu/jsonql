# @jsonql/ws

The socket client for jsonql, [ws](https://www.npmjs.com/package/ws) on node, and [WebSocket](https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API) on browser

You don't usually use this directly. This is part of the optional dependencies for [@jsonql/client](https://www.npmjs.com/package/@jsonql/client)

## Internal (note to ourself)

The core provide one method: `ws-client-resolver`, if we think of the `jsonql-ws-client-core` as the **client** 
then this will provide the **server**. Each different framework (i.e. WebSocket, Socket.io etc) can create their own implementation within this method. Then binding all the events back to the **client** to accept connection call. It's a bit around about, but effectively solve many of the problem, such as when client is not login, then 
login, how do we reload all the connections. 

**STEPS**:

- `createClientResolver(WebSocket)` [create the framework specific client] return a function 
- `createNsp(opts, nspMap, ee)` takes these three parameter provided by `jsonql-ws-client-core` then determine if this module require authentication or not (id by `enableAuth`)
- `socketEventHandler` this is the heart of the framework specific socket event handle that bind all the event to our client interface 
- finally, passing back to `jsonql-ws-client-core` `clientEventHandler` to complete the client creation

---

MIT

NB and T1S

https://jsonql.org
