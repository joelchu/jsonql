(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global = global || self, global.jsonqlWsClient = factory());
}(this, (function () { 'use strict';

  /* base.js */
  // the core stuff to id if it's calling with jsonql
  var DATA_KEY = 'data';
  var ERROR_KEY = 'error';
  var HEADERS_KEY = 'headers';

  var JSONQL_PATH = 'jsonql';

  // export const INDEX = 'index' use INDEX_KEY instead
  var DEFAULT_TYPE = 'any';

  // @TODO remove this is not in use
  // export const CLIENT_CONFIG_FILE = '.clients.json'
  // export const CONTRACT_CONFIG_FILE = 'jsonql-contract-config.js'
  // type of resolvers
  var QUERY_NAME = 'query';
  var MUTATION_NAME = 'mutation';
  var SOCKET_NAME = 'socket';
  var QUERY_ARG_NAME = 'args';
  var TIMESTAMP_PARAM_NAME = 'TS';
  // for  contract-cli
  var KEY_WORD = 'continue';
  var PUBLIC_KEY = 'public';
  var PRIVATE_KEY = 'private';
  var LOGIN_FN_NAME = 'login';
  // export const ISSUER_NAME = LOGIN_NAME // legacy issue need to replace them later
  var LOGOUT_FN_NAME = 'logout';
  var DISCONNECT_FN_NAME = 'disconnect';
  var SWITCH_USER_FN_NAME = 'switch-user';

  var AUTH_HEADER = 'Authorization';
  var BEARER = 'Bearer';
  // headers
  var CSRF_HEADER_KEY = 'X-CSRF-Token';

  /* prop.js */

  // this is all the key name for the config check map
  // all subfix with prop_key

  var TYPE_KEY = 'type';
  var OPTIONAL_KEY = 'optional';
  var ENUM_KEY = 'enumv';  // need to change this because enum is a reserved word
  var ARGS_KEY = 'args';
  var CHECKER_KEY = 'checker';
  var ALIAS_KEY = 'alias';
  var ENABLE_AUTH_PROP_KEY = 'enableAuth';
  var USE_JWT_PROP_KEY = 'useJwt';
  var LOGIN_FN_NAME_PROP_KEY = 'loginHandlerName';
  var LOGOUT_FN_NAME_PROP_KEY = 'logoutHandlerName';
  var DISCONNECT_FN_NAME_PROP_KEY = 'disconnectHandlerName';
  var SWITCH_USER_FN_NAME_PROP_KEY = 'switchUserHandlerName';
  // type name and Alias
  var SOCKET_TYPE_PROP_KEY = 'serverType'; //1.9.1
  var SOCKET_TYPE_CLIENT_ALIAS = 'socketClientType'; // 1.9.0

  var CSRF_PROP_KEY = 'csrf';

  var STANDALONE_PROP_KEY = 'standalone';
  var DEBUG_ON_PROP_KEY = 'debugOn';

  var HOSTNAME_PROP_KEY = 'hostname';
  var NAMESAPCE_PROP_KEY = 'namespace';

  var WS_OPT_PROP_KEY = 'wsOptions';

  var CONTRACT_PROP_KEY = 'contract';
  var TOKEN_PROP_KEY = 'token';

  var CONNECTED_PROP_KEY = 'connected';
  // track this key if we want to suspend event on start
  var SUSPEND_EVENT_PROP_KEY = 'suspendOnStart';
  // we could pass the token in the header instead when init the WebSocket 
  var TOKEN_DELIVER_LOCATION_PROP_KEY = 'tokenDeliverLocation';
  // for tracking the login and connect state for socket client 
  var IS_READY_PROP_KEY = 'isReady';
  var IS_LOGIN_PROP_KEY = 'isLogin';/* socket.js */

  // the constants file is gettig too large
  // we need to split up and group the related constant in one file
  // also it makes the other module easiler to id what they are importing
  // use throughout the clients
  var SOCKET_PING_EVENT_NAME = '__ping__'; // when init connection do a ping
  var LOGIN_EVENT_NAME = '__login__';
  var LOGOUT_EVENT_NAME$1 = '__logout__';
  // at the moment we only have __logout__ regardless enableAuth is enable
  // this is incorrect, because logout suppose to come after login
  // and it should only logout from auth nsp, instead of clear out the
  // connection, the following new event @1.9.2 will correct this edge case
  // although it should never happens, but in some edge case might want to
  // disconnect from the current server, then re-establish connection later
  var CONNECT_EVENT_NAME = '__connect__';
  // we still need the connected event because after the connection establish 
  // we need to change a state within the client to let the front end know that 
  // it's current hook up to the server but we don't want to loop back the client 
  // inside the setup phrase, intead just trigger a connected event and the listener 
  // setup this property 
  var CONNECTED_EVENT_NAME = '__connected__';
  var DISCONNECT_EVENT_NAME = '__disconnect__';
  // instead of using an event name in place of resolverName in the param
  // we use this internal resolverName instead, and in type using the event names
  var INTERCOM_RESOLVER_NAME = '__intercom__';
  // for ws servers
  var WS_REPLY_TYPE = '__reply__';
  var WS_EVT_NAME = '__event__';
  var WS_DATA_NAME = '__data__';

  // for ws client, 1.9.3 breaking change to name them as FN instead of PROP
  var ON_MESSAGE_FN_NAME = 'onMessage';
  var ON_RESULT_FN_NAME = 'onResult'; // this will need to be internal from now on
  var ON_ERROR_FN_NAME = 'onError';
  var ON_READY_FN_NAME = 'onReady';
  var ON_LOGIN_FN_NAME = 'onLogin'; // new @1.8.6
  // the actual method name client.resolverName.send
  var SEND_MSG_FN_NAME = 'send';

  // this is somewhat vague about what is suppose to do
  var EMIT_REPLY_TYPE = 'emit_reply';

  var NSP_GROUP = 'nspGroup';
  var PUBLIC_NAMESPACE = 'publicNamespace';
  var NOT_LOGIN_ERR_MSG = 'NOT LOGIN';
  var HSA_ALGO = 'HS256';
  var TOKEN_PARAM_NAME = 'token';

  // this is the value for TOKEN_DELIVER_LOCATION_PROP_KEY
  var TOKEN_IN_HEADER = 'header';
  var TOKEN_IN_URL = 'url';

  /* validation.js */

  // validation related constants


  var OR_SEPERATOR = '|';
  var STRING_TYPE = 'string';
  var BOOLEAN_TYPE = 'boolean';
  var ARRAY_TYPE = 'array';
  var OBJECT_TYPE = 'object';

  var NUMBER_TYPE = 'number';
  var ARRAY_TYPE_LFT = 'array.<';
  var ARRAY_TYPE_RGT = '>';

  /**
   * Checks if `value` is classified as an `Array` object.
   *
   * @static
   * @memberOf _
   * @since 0.1.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is an array, else `false`.
   * @example
   *
   * _.isArray([1, 2, 3]);
   * // => true
   *
   * _.isArray(document.body.children);
   * // => false
   *
   * _.isArray('abc');
   * // => false
   *
   * _.isArray(_.noop);
   * // => false
   */
  var isArray = Array.isArray;

  /** Detect free variable `global` from Node.js. */
  var freeGlobal = typeof global == 'object' && global && global.Object === Object && global;

  /** Detect free variable `self`. */
  var freeSelf = typeof self == 'object' && self && self.Object === Object && self;

  /** Used as a reference to the global object. */
  var root = freeGlobal || freeSelf || Function('return this')();

  /** Built-in value references. */
  var Symbol = root.Symbol;

  /** Used for built-in method references. */
  var objectProto = Object.prototype;

  /** Used to check objects for own properties. */
  var hasOwnProperty = objectProto.hasOwnProperty;

  /**
   * Used to resolve the
   * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
   * of values.
   */
  var nativeObjectToString = objectProto.toString;

  /** Built-in value references. */
  var symToStringTag = Symbol ? Symbol.toStringTag : undefined;

  /**
   * A specialized version of `baseGetTag` which ignores `Symbol.toStringTag` values.
   *
   * @private
   * @param {*} value The value to query.
   * @returns {string} Returns the raw `toStringTag`.
   */
  function getRawTag(value) {
    var isOwn = hasOwnProperty.call(value, symToStringTag),
        tag = value[symToStringTag];

    try {
      value[symToStringTag] = undefined;
      var unmasked = true;
    } catch (e) {}

    var result = nativeObjectToString.call(value);
    if (unmasked) {
      if (isOwn) {
        value[symToStringTag] = tag;
      } else {
        delete value[symToStringTag];
      }
    }
    return result;
  }

  /** Used for built-in method references. */
  var objectProto$1 = Object.prototype;

  /**
   * Used to resolve the
   * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
   * of values.
   */
  var nativeObjectToString$1 = objectProto$1.toString;

  /**
   * Converts `value` to a string using `Object.prototype.toString`.
   *
   * @private
   * @param {*} value The value to convert.
   * @returns {string} Returns the converted string.
   */
  function objectToString(value) {
    return nativeObjectToString$1.call(value);
  }

  /** `Object#toString` result references. */
  var nullTag = '[object Null]',
      undefinedTag = '[object Undefined]';

  /** Built-in value references. */
  var symToStringTag$1 = Symbol ? Symbol.toStringTag : undefined;

  /**
   * The base implementation of `getTag` without fallbacks for buggy environments.
   *
   * @private
   * @param {*} value The value to query.
   * @returns {string} Returns the `toStringTag`.
   */
  function baseGetTag(value) {
    if (value == null) {
      return value === undefined ? undefinedTag : nullTag;
    }
    return (symToStringTag$1 && symToStringTag$1 in Object(value))
      ? getRawTag(value)
      : objectToString(value);
  }

  /**
   * Creates a unary function that invokes `func` with its argument transformed.
   *
   * @private
   * @param {Function} func The function to wrap.
   * @param {Function} transform The argument transform.
   * @returns {Function} Returns the new function.
   */
  function overArg(func, transform) {
    return function(arg) {
      return func(transform(arg));
    };
  }

  /** Built-in value references. */
  var getPrototype = overArg(Object.getPrototypeOf, Object);

  /**
   * Checks if `value` is object-like. A value is object-like if it's not `null`
   * and has a `typeof` result of "object".
   *
   * @static
   * @memberOf _
   * @since 4.0.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
   * @example
   *
   * _.isObjectLike({});
   * // => true
   *
   * _.isObjectLike([1, 2, 3]);
   * // => true
   *
   * _.isObjectLike(_.noop);
   * // => false
   *
   * _.isObjectLike(null);
   * // => false
   */
  function isObjectLike(value) {
    return value != null && typeof value == 'object';
  }

  /** `Object#toString` result references. */
  var objectTag = '[object Object]';

  /** Used for built-in method references. */
  var funcProto = Function.prototype,
      objectProto$2 = Object.prototype;

  /** Used to resolve the decompiled source of functions. */
  var funcToString = funcProto.toString;

  /** Used to check objects for own properties. */
  var hasOwnProperty$1 = objectProto$2.hasOwnProperty;

  /** Used to infer the `Object` constructor. */
  var objectCtorString = funcToString.call(Object);

  /**
   * Checks if `value` is a plain object, that is, an object created by the
   * `Object` constructor or one with a `[[Prototype]]` of `null`.
   *
   * @static
   * @memberOf _
   * @since 0.8.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a plain object, else `false`.
   * @example
   *
   * function Foo() {
   *   this.a = 1;
   * }
   *
   * _.isPlainObject(new Foo);
   * // => false
   *
   * _.isPlainObject([1, 2, 3]);
   * // => false
   *
   * _.isPlainObject({ 'x': 0, 'y': 0 });
   * // => true
   *
   * _.isPlainObject(Object.create(null));
   * // => true
   */
  function isPlainObject(value) {
    if (!isObjectLike(value) || baseGetTag(value) != objectTag) {
      return false;
    }
    var proto = getPrototype(value);
    if (proto === null) {
      return true;
    }
    var Ctor = hasOwnProperty$1.call(proto, 'constructor') && proto.constructor;
    return typeof Ctor == 'function' && Ctor instanceof Ctor &&
      funcToString.call(Ctor) == objectCtorString;
  }

  /**
   * A specialized version of `_.map` for arrays without support for iteratee
   * shorthands.
   *
   * @private
   * @param {Array} [array] The array to iterate over.
   * @param {Function} iteratee The function invoked per iteration.
   * @returns {Array} Returns the new mapped array.
   */
  function arrayMap(array, iteratee) {
    var index = -1,
        length = array == null ? 0 : array.length,
        result = Array(length);

    while (++index < length) {
      result[index] = iteratee(array[index], index, array);
    }
    return result;
  }

  /** `Object#toString` result references. */
  var symbolTag = '[object Symbol]';

  /**
   * Checks if `value` is classified as a `Symbol` primitive or object.
   *
   * @static
   * @memberOf _
   * @since 4.0.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a symbol, else `false`.
   * @example
   *
   * _.isSymbol(Symbol.iterator);
   * // => true
   *
   * _.isSymbol('abc');
   * // => false
   */
  function isSymbol(value) {
    return typeof value == 'symbol' ||
      (isObjectLike(value) && baseGetTag(value) == symbolTag);
  }

  /** Used as references for various `Number` constants. */
  var INFINITY = 1 / 0;

  /** Used to convert symbols to primitives and strings. */
  var symbolProto = Symbol ? Symbol.prototype : undefined,
      symbolToString = symbolProto ? symbolProto.toString : undefined;

  /**
   * The base implementation of `_.toString` which doesn't convert nullish
   * values to empty strings.
   *
   * @private
   * @param {*} value The value to process.
   * @returns {string} Returns the string.
   */
  function baseToString(value) {
    // Exit early for strings to avoid a performance hit in some environments.
    if (typeof value == 'string') {
      return value;
    }
    if (isArray(value)) {
      // Recursively convert values (susceptible to call stack limits).
      return arrayMap(value, baseToString) + '';
    }
    if (isSymbol(value)) {
      return symbolToString ? symbolToString.call(value) : '';
    }
    var result = (value + '');
    return (result == '0' && (1 / value) == -INFINITY) ? '-0' : result;
  }

  /**
   * The base implementation of `_.slice` without an iteratee call guard.
   *
   * @private
   * @param {Array} array The array to slice.
   * @param {number} [start=0] The start position.
   * @param {number} [end=array.length] The end position.
   * @returns {Array} Returns the slice of `array`.
   */
  function baseSlice(array, start, end) {
    var index = -1,
        length = array.length;

    if (start < 0) {
      start = -start > length ? 0 : (length + start);
    }
    end = end > length ? length : end;
    if (end < 0) {
      end += length;
    }
    length = start > end ? 0 : ((end - start) >>> 0);
    start >>>= 0;

    var result = Array(length);
    while (++index < length) {
      result[index] = array[index + start];
    }
    return result;
  }

  /**
   * Casts `array` to a slice if it's needed.
   *
   * @private
   * @param {Array} array The array to inspect.
   * @param {number} start The start position.
   * @param {number} [end=array.length] The end position.
   * @returns {Array} Returns the cast slice.
   */
  function castSlice(array, start, end) {
    var length = array.length;
    end = end === undefined ? length : end;
    return (!start && end >= length) ? array : baseSlice(array, start, end);
  }

  /**
   * The base implementation of `_.findIndex` and `_.findLastIndex` without
   * support for iteratee shorthands.
   *
   * @private
   * @param {Array} array The array to inspect.
   * @param {Function} predicate The function invoked per iteration.
   * @param {number} fromIndex The index to search from.
   * @param {boolean} [fromRight] Specify iterating from right to left.
   * @returns {number} Returns the index of the matched value, else `-1`.
   */
  function baseFindIndex(array, predicate, fromIndex, fromRight) {
    var length = array.length,
        index = fromIndex + (fromRight ? 1 : -1);

    while ((fromRight ? index-- : ++index < length)) {
      if (predicate(array[index], index, array)) {
        return index;
      }
    }
    return -1;
  }

  /**
   * The base implementation of `_.isNaN` without support for number objects.
   *
   * @private
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is `NaN`, else `false`.
   */
  function baseIsNaN(value) {
    return value !== value;
  }

  /**
   * A specialized version of `_.indexOf` which performs strict equality
   * comparisons of values, i.e. `===`.
   *
   * @private
   * @param {Array} array The array to inspect.
   * @param {*} value The value to search for.
   * @param {number} fromIndex The index to search from.
   * @returns {number} Returns the index of the matched value, else `-1`.
   */
  function strictIndexOf(array, value, fromIndex) {
    var index = fromIndex - 1,
        length = array.length;

    while (++index < length) {
      if (array[index] === value) {
        return index;
      }
    }
    return -1;
  }

  /**
   * The base implementation of `_.indexOf` without `fromIndex` bounds checks.
   *
   * @private
   * @param {Array} array The array to inspect.
   * @param {*} value The value to search for.
   * @param {number} fromIndex The index to search from.
   * @returns {number} Returns the index of the matched value, else `-1`.
   */
  function baseIndexOf(array, value, fromIndex) {
    return value === value
      ? strictIndexOf(array, value, fromIndex)
      : baseFindIndex(array, baseIsNaN, fromIndex);
  }

  /**
   * Used by `_.trim` and `_.trimEnd` to get the index of the last string symbol
   * that is not found in the character symbols.
   *
   * @private
   * @param {Array} strSymbols The string symbols to inspect.
   * @param {Array} chrSymbols The character symbols to find.
   * @returns {number} Returns the index of the last unmatched string symbol.
   */
  function charsEndIndex(strSymbols, chrSymbols) {
    var index = strSymbols.length;

    while (index-- && baseIndexOf(chrSymbols, strSymbols[index], 0) > -1) {}
    return index;
  }

  /**
   * Used by `_.trim` and `_.trimStart` to get the index of the first string symbol
   * that is not found in the character symbols.
   *
   * @private
   * @param {Array} strSymbols The string symbols to inspect.
   * @param {Array} chrSymbols The character symbols to find.
   * @returns {number} Returns the index of the first unmatched string symbol.
   */
  function charsStartIndex(strSymbols, chrSymbols) {
    var index = -1,
        length = strSymbols.length;

    while (++index < length && baseIndexOf(chrSymbols, strSymbols[index], 0) > -1) {}
    return index;
  }

  /**
   * Converts an ASCII `string` to an array.
   *
   * @private
   * @param {string} string The string to convert.
   * @returns {Array} Returns the converted array.
   */
  function asciiToArray(string) {
    return string.split('');
  }

  /** Used to compose unicode character classes. */
  var rsAstralRange = '\\ud800-\\udfff',
      rsComboMarksRange = '\\u0300-\\u036f',
      reComboHalfMarksRange = '\\ufe20-\\ufe2f',
      rsComboSymbolsRange = '\\u20d0-\\u20ff',
      rsComboRange = rsComboMarksRange + reComboHalfMarksRange + rsComboSymbolsRange,
      rsVarRange = '\\ufe0e\\ufe0f';

  /** Used to compose unicode capture groups. */
  var rsZWJ = '\\u200d';

  /** Used to detect strings with [zero-width joiners or code points from the astral planes](http://eev.ee/blog/2015/09/12/dark-corners-of-unicode/). */
  var reHasUnicode = RegExp('[' + rsZWJ + rsAstralRange  + rsComboRange + rsVarRange + ']');

  /**
   * Checks if `string` contains Unicode symbols.
   *
   * @private
   * @param {string} string The string to inspect.
   * @returns {boolean} Returns `true` if a symbol is found, else `false`.
   */
  function hasUnicode(string) {
    return reHasUnicode.test(string);
  }

  /** Used to compose unicode character classes. */
  var rsAstralRange$1 = '\\ud800-\\udfff',
      rsComboMarksRange$1 = '\\u0300-\\u036f',
      reComboHalfMarksRange$1 = '\\ufe20-\\ufe2f',
      rsComboSymbolsRange$1 = '\\u20d0-\\u20ff',
      rsComboRange$1 = rsComboMarksRange$1 + reComboHalfMarksRange$1 + rsComboSymbolsRange$1,
      rsVarRange$1 = '\\ufe0e\\ufe0f';

  /** Used to compose unicode capture groups. */
  var rsAstral = '[' + rsAstralRange$1 + ']',
      rsCombo = '[' + rsComboRange$1 + ']',
      rsFitz = '\\ud83c[\\udffb-\\udfff]',
      rsModifier = '(?:' + rsCombo + '|' + rsFitz + ')',
      rsNonAstral = '[^' + rsAstralRange$1 + ']',
      rsRegional = '(?:\\ud83c[\\udde6-\\uddff]){2}',
      rsSurrPair = '[\\ud800-\\udbff][\\udc00-\\udfff]',
      rsZWJ$1 = '\\u200d';

  /** Used to compose unicode regexes. */
  var reOptMod = rsModifier + '?',
      rsOptVar = '[' + rsVarRange$1 + ']?',
      rsOptJoin = '(?:' + rsZWJ$1 + '(?:' + [rsNonAstral, rsRegional, rsSurrPair].join('|') + ')' + rsOptVar + reOptMod + ')*',
      rsSeq = rsOptVar + reOptMod + rsOptJoin,
      rsSymbol = '(?:' + [rsNonAstral + rsCombo + '?', rsCombo, rsRegional, rsSurrPair, rsAstral].join('|') + ')';

  /** Used to match [string symbols](https://mathiasbynens.be/notes/javascript-unicode). */
  var reUnicode = RegExp(rsFitz + '(?=' + rsFitz + ')|' + rsSymbol + rsSeq, 'g');

  /**
   * Converts a Unicode `string` to an array.
   *
   * @private
   * @param {string} string The string to convert.
   * @returns {Array} Returns the converted array.
   */
  function unicodeToArray(string) {
    return string.match(reUnicode) || [];
  }

  /**
   * Converts `string` to an array.
   *
   * @private
   * @param {string} string The string to convert.
   * @returns {Array} Returns the converted array.
   */
  function stringToArray(string) {
    return hasUnicode(string)
      ? unicodeToArray(string)
      : asciiToArray(string);
  }

  /**
   * Converts `value` to a string. An empty string is returned for `null`
   * and `undefined` values. The sign of `-0` is preserved.
   *
   * @static
   * @memberOf _
   * @since 4.0.0
   * @category Lang
   * @param {*} value The value to convert.
   * @returns {string} Returns the converted string.
   * @example
   *
   * _.toString(null);
   * // => ''
   *
   * _.toString(-0);
   * // => '-0'
   *
   * _.toString([1, 2, 3]);
   * // => '1,2,3'
   */
  function toString(value) {
    return value == null ? '' : baseToString(value);
  }

  /** Used to match leading and trailing whitespace. */
  var reTrim = /^\s+|\s+$/g;

  /**
   * Removes leading and trailing whitespace or specified characters from `string`.
   *
   * @static
   * @memberOf _
   * @since 3.0.0
   * @category String
   * @param {string} [string=''] The string to trim.
   * @param {string} [chars=whitespace] The characters to trim.
   * @param- {Object} [guard] Enables use as an iteratee for methods like `_.map`.
   * @returns {string} Returns the trimmed string.
   * @example
   *
   * _.trim('  abc  ');
   * // => 'abc'
   *
   * _.trim('-_-abc-_-', '_-');
   * // => 'abc'
   *
   * _.map(['  foo  ', '  bar  '], _.trim);
   * // => ['foo', 'bar']
   */
  function trim(string, chars, guard) {
    string = toString(string);
    if (string && (guard || chars === undefined)) {
      return string.replace(reTrim, '');
    }
    if (!string || !(chars = baseToString(chars))) {
      return string;
    }
    var strSymbols = stringToArray(string),
        chrSymbols = stringToArray(chars),
        start = charsStartIndex(strSymbols, chrSymbols),
        end = charsEndIndex(strSymbols, chrSymbols) + 1;

    return castSlice(strSymbols, start, end).join('');
  }

  // bunch of generic helpers

  /**
   * DIY in Array
   * @param {array} arr to check from
   * @param {*} value to check against
   * @return {boolean} true on found
   */
  var inArray = function (arr, value) { return !!arr.filter(function (a) { return a === value; }).length; };

  // quick and dirty to turn non array to array
  var toArray = function (arg) { return isArray(arg) ? arg : [arg]; };

  /**
   * parse string to json or just return the original value if error happened
   * @param {*} n input
   * @param {boolean} [t=true] or throw
   * @return {*} json object on success
   */
  var parseJson = function(n, t) {
    if ( t === void 0 ) t=true;

    try {
      return JSON.parse(n)
    } catch(e) {
      if (t) {
        return n
      }
      throw new Error(e)
    }
  };

  /**
   * @param {object} obj for search
   * @param {string} key target
   * @return {boolean} true on success
   */
  var isObjectHasKey = function(obj, key) {
    try {
      var keys = Object.keys(obj);
      return inArray(keys, key)
    } catch(e) {
      // @BUG when the obj is not an OBJECT we got some weird output
      return false
    }
  };

  /**
   * create a event name
   * @param {string[]} args
   * @return {string} event name for use
   */
  var createEvt = function () {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

    return args.join('_');
  };

  /**
   * small util to make sure the return value is valid JSON object
   * @param {*} n input
   * @return {object} correct JSON object
   */
  var toJson = function (n) {
    if (typeof n === 'string') {
      return parseJson(n)
    }
    return parseJson(JSON.stringify(n))
  };

  /**
   * Simple check if the prop is function
   * @param {*} prop input
   * @return {boolean} true on success
   */
  var isFunc = function (prop) {
    if (typeof prop === 'function') {
      return true;
    }
    console.error(("Expect to be Function type! Got " + (typeof prop)));
  };
    
  /** 
   * generic placeholder function
   * @return {boolean} false 
   */
  var nil = function () { return false; };

  /**
   * using just the map reduce to chain multiple functions together
   * @param {function} mainFn the init function
   * @param {array} moreFns as many as you want to take the last value and return a new one
   * @return {function} accept value for the mainFn
   */
  var chainFns = function (mainFn) {
    var moreFns = [], len = arguments.length - 1;
    while ( len-- > 0 ) moreFns[ len ] = arguments[ len + 1 ];

    return (
    function () {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      return (
      moreFns.reduce(function (value, nextFn) { return (
        // change here to check if the return value is array then we spread it 
        Reflect.apply(nextFn, null, toArray(value))
      ); }, Reflect.apply(mainFn, null, args))
    );
    }
  );
  };

  /**
   * Removes all key-value entries from the list cache.
   *
   * @private
   * @name clear
   * @memberOf ListCache
   */
  function listCacheClear() {
    this.__data__ = [];
    this.size = 0;
  }

  /**
   * Performs a
   * [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
   * comparison between two values to determine if they are equivalent.
   *
   * @static
   * @memberOf _
   * @since 4.0.0
   * @category Lang
   * @param {*} value The value to compare.
   * @param {*} other The other value to compare.
   * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
   * @example
   *
   * var object = { 'a': 1 };
   * var other = { 'a': 1 };
   *
   * _.eq(object, object);
   * // => true
   *
   * _.eq(object, other);
   * // => false
   *
   * _.eq('a', 'a');
   * // => true
   *
   * _.eq('a', Object('a'));
   * // => false
   *
   * _.eq(NaN, NaN);
   * // => true
   */
  function eq(value, other) {
    return value === other || (value !== value && other !== other);
  }

  /**
   * Gets the index at which the `key` is found in `array` of key-value pairs.
   *
   * @private
   * @param {Array} array The array to inspect.
   * @param {*} key The key to search for.
   * @returns {number} Returns the index of the matched value, else `-1`.
   */
  function assocIndexOf(array, key) {
    var length = array.length;
    while (length--) {
      if (eq(array[length][0], key)) {
        return length;
      }
    }
    return -1;
  }

  /** Used for built-in method references. */
  var arrayProto = Array.prototype;

  /** Built-in value references. */
  var splice = arrayProto.splice;

  /**
   * Removes `key` and its value from the list cache.
   *
   * @private
   * @name delete
   * @memberOf ListCache
   * @param {string} key The key of the value to remove.
   * @returns {boolean} Returns `true` if the entry was removed, else `false`.
   */
  function listCacheDelete(key) {
    var data = this.__data__,
        index = assocIndexOf(data, key);

    if (index < 0) {
      return false;
    }
    var lastIndex = data.length - 1;
    if (index == lastIndex) {
      data.pop();
    } else {
      splice.call(data, index, 1);
    }
    --this.size;
    return true;
  }

  /**
   * Gets the list cache value for `key`.
   *
   * @private
   * @name get
   * @memberOf ListCache
   * @param {string} key The key of the value to get.
   * @returns {*} Returns the entry value.
   */
  function listCacheGet(key) {
    var data = this.__data__,
        index = assocIndexOf(data, key);

    return index < 0 ? undefined : data[index][1];
  }

  /**
   * Checks if a list cache value for `key` exists.
   *
   * @private
   * @name has
   * @memberOf ListCache
   * @param {string} key The key of the entry to check.
   * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
   */
  function listCacheHas(key) {
    return assocIndexOf(this.__data__, key) > -1;
  }

  /**
   * Sets the list cache `key` to `value`.
   *
   * @private
   * @name set
   * @memberOf ListCache
   * @param {string} key The key of the value to set.
   * @param {*} value The value to set.
   * @returns {Object} Returns the list cache instance.
   */
  function listCacheSet(key, value) {
    var data = this.__data__,
        index = assocIndexOf(data, key);

    if (index < 0) {
      ++this.size;
      data.push([key, value]);
    } else {
      data[index][1] = value;
    }
    return this;
  }

  /**
   * Creates an list cache object.
   *
   * @private
   * @constructor
   * @param {Array} [entries] The key-value pairs to cache.
   */
  function ListCache(entries) {
    var index = -1,
        length = entries == null ? 0 : entries.length;

    this.clear();
    while (++index < length) {
      var entry = entries[index];
      this.set(entry[0], entry[1]);
    }
  }

  // Add methods to `ListCache`.
  ListCache.prototype.clear = listCacheClear;
  ListCache.prototype['delete'] = listCacheDelete;
  ListCache.prototype.get = listCacheGet;
  ListCache.prototype.has = listCacheHas;
  ListCache.prototype.set = listCacheSet;

  /**
   * Removes all key-value entries from the stack.
   *
   * @private
   * @name clear
   * @memberOf Stack
   */
  function stackClear() {
    this.__data__ = new ListCache;
    this.size = 0;
  }

  /**
   * Removes `key` and its value from the stack.
   *
   * @private
   * @name delete
   * @memberOf Stack
   * @param {string} key The key of the value to remove.
   * @returns {boolean} Returns `true` if the entry was removed, else `false`.
   */
  function stackDelete(key) {
    var data = this.__data__,
        result = data['delete'](key);

    this.size = data.size;
    return result;
  }

  /**
   * Gets the stack value for `key`.
   *
   * @private
   * @name get
   * @memberOf Stack
   * @param {string} key The key of the value to get.
   * @returns {*} Returns the entry value.
   */
  function stackGet(key) {
    return this.__data__.get(key);
  }

  /**
   * Checks if a stack value for `key` exists.
   *
   * @private
   * @name has
   * @memberOf Stack
   * @param {string} key The key of the entry to check.
   * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
   */
  function stackHas(key) {
    return this.__data__.has(key);
  }

  /**
   * Checks if `value` is the
   * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
   * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
   *
   * @static
   * @memberOf _
   * @since 0.1.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is an object, else `false`.
   * @example
   *
   * _.isObject({});
   * // => true
   *
   * _.isObject([1, 2, 3]);
   * // => true
   *
   * _.isObject(_.noop);
   * // => true
   *
   * _.isObject(null);
   * // => false
   */
  function isObject(value) {
    var type = typeof value;
    return value != null && (type == 'object' || type == 'function');
  }

  /** `Object#toString` result references. */
  var asyncTag = '[object AsyncFunction]',
      funcTag = '[object Function]',
      genTag = '[object GeneratorFunction]',
      proxyTag = '[object Proxy]';

  /**
   * Checks if `value` is classified as a `Function` object.
   *
   * @static
   * @memberOf _
   * @since 0.1.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a function, else `false`.
   * @example
   *
   * _.isFunction(_);
   * // => true
   *
   * _.isFunction(/abc/);
   * // => false
   */
  function isFunction(value) {
    if (!isObject(value)) {
      return false;
    }
    // The use of `Object#toString` avoids issues with the `typeof` operator
    // in Safari 9 which returns 'object' for typed arrays and other constructors.
    var tag = baseGetTag(value);
    return tag == funcTag || tag == genTag || tag == asyncTag || tag == proxyTag;
  }

  /** Used to detect overreaching core-js shims. */
  var coreJsData = root['__core-js_shared__'];

  /** Used to detect methods masquerading as native. */
  var maskSrcKey = (function() {
    var uid = /[^.]+$/.exec(coreJsData && coreJsData.keys && coreJsData.keys.IE_PROTO || '');
    return uid ? ('Symbol(src)_1.' + uid) : '';
  }());

  /**
   * Checks if `func` has its source masked.
   *
   * @private
   * @param {Function} func The function to check.
   * @returns {boolean} Returns `true` if `func` is masked, else `false`.
   */
  function isMasked(func) {
    return !!maskSrcKey && (maskSrcKey in func);
  }

  /** Used for built-in method references. */
  var funcProto$1 = Function.prototype;

  /** Used to resolve the decompiled source of functions. */
  var funcToString$1 = funcProto$1.toString;

  /**
   * Converts `func` to its source code.
   *
   * @private
   * @param {Function} func The function to convert.
   * @returns {string} Returns the source code.
   */
  function toSource(func) {
    if (func != null) {
      try {
        return funcToString$1.call(func);
      } catch (e) {}
      try {
        return (func + '');
      } catch (e) {}
    }
    return '';
  }

  /**
   * Used to match `RegExp`
   * [syntax characters](http://ecma-international.org/ecma-262/7.0/#sec-patterns).
   */
  var reRegExpChar = /[\\^$.*+?()[\]{}|]/g;

  /** Used to detect host constructors (Safari). */
  var reIsHostCtor = /^\[object .+?Constructor\]$/;

  /** Used for built-in method references. */
  var funcProto$2 = Function.prototype,
      objectProto$3 = Object.prototype;

  /** Used to resolve the decompiled source of functions. */
  var funcToString$2 = funcProto$2.toString;

  /** Used to check objects for own properties. */
  var hasOwnProperty$2 = objectProto$3.hasOwnProperty;

  /** Used to detect if a method is native. */
  var reIsNative = RegExp('^' +
    funcToString$2.call(hasOwnProperty$2).replace(reRegExpChar, '\\$&')
    .replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, '$1.*?') + '$'
  );

  /**
   * The base implementation of `_.isNative` without bad shim checks.
   *
   * @private
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a native function,
   *  else `false`.
   */
  function baseIsNative(value) {
    if (!isObject(value) || isMasked(value)) {
      return false;
    }
    var pattern = isFunction(value) ? reIsNative : reIsHostCtor;
    return pattern.test(toSource(value));
  }

  /**
   * Gets the value at `key` of `object`.
   *
   * @private
   * @param {Object} [object] The object to query.
   * @param {string} key The key of the property to get.
   * @returns {*} Returns the property value.
   */
  function getValue(object, key) {
    return object == null ? undefined : object[key];
  }

  /**
   * Gets the native function at `key` of `object`.
   *
   * @private
   * @param {Object} object The object to query.
   * @param {string} key The key of the method to get.
   * @returns {*} Returns the function if it's native, else `undefined`.
   */
  function getNative(object, key) {
    var value = getValue(object, key);
    return baseIsNative(value) ? value : undefined;
  }

  /* Built-in method references that are verified to be native. */
  var Map$1 = getNative(root, 'Map');

  /* Built-in method references that are verified to be native. */
  var nativeCreate = getNative(Object, 'create');

  /**
   * Removes all key-value entries from the hash.
   *
   * @private
   * @name clear
   * @memberOf Hash
   */
  function hashClear() {
    this.__data__ = nativeCreate ? nativeCreate(null) : {};
    this.size = 0;
  }

  /**
   * Removes `key` and its value from the hash.
   *
   * @private
   * @name delete
   * @memberOf Hash
   * @param {Object} hash The hash to modify.
   * @param {string} key The key of the value to remove.
   * @returns {boolean} Returns `true` if the entry was removed, else `false`.
   */
  function hashDelete(key) {
    var result = this.has(key) && delete this.__data__[key];
    this.size -= result ? 1 : 0;
    return result;
  }

  /** Used to stand-in for `undefined` hash values. */
  var HASH_UNDEFINED = '__lodash_hash_undefined__';

  /** Used for built-in method references. */
  var objectProto$4 = Object.prototype;

  /** Used to check objects for own properties. */
  var hasOwnProperty$3 = objectProto$4.hasOwnProperty;

  /**
   * Gets the hash value for `key`.
   *
   * @private
   * @name get
   * @memberOf Hash
   * @param {string} key The key of the value to get.
   * @returns {*} Returns the entry value.
   */
  function hashGet(key) {
    var data = this.__data__;
    if (nativeCreate) {
      var result = data[key];
      return result === HASH_UNDEFINED ? undefined : result;
    }
    return hasOwnProperty$3.call(data, key) ? data[key] : undefined;
  }

  /** Used for built-in method references. */
  var objectProto$5 = Object.prototype;

  /** Used to check objects for own properties. */
  var hasOwnProperty$4 = objectProto$5.hasOwnProperty;

  /**
   * Checks if a hash value for `key` exists.
   *
   * @private
   * @name has
   * @memberOf Hash
   * @param {string} key The key of the entry to check.
   * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
   */
  function hashHas(key) {
    var data = this.__data__;
    return nativeCreate ? (data[key] !== undefined) : hasOwnProperty$4.call(data, key);
  }

  /** Used to stand-in for `undefined` hash values. */
  var HASH_UNDEFINED$1 = '__lodash_hash_undefined__';

  /**
   * Sets the hash `key` to `value`.
   *
   * @private
   * @name set
   * @memberOf Hash
   * @param {string} key The key of the value to set.
   * @param {*} value The value to set.
   * @returns {Object} Returns the hash instance.
   */
  function hashSet(key, value) {
    var data = this.__data__;
    this.size += this.has(key) ? 0 : 1;
    data[key] = (nativeCreate && value === undefined) ? HASH_UNDEFINED$1 : value;
    return this;
  }

  /**
   * Creates a hash object.
   *
   * @private
   * @constructor
   * @param {Array} [entries] The key-value pairs to cache.
   */
  function Hash(entries) {
    var index = -1,
        length = entries == null ? 0 : entries.length;

    this.clear();
    while (++index < length) {
      var entry = entries[index];
      this.set(entry[0], entry[1]);
    }
  }

  // Add methods to `Hash`.
  Hash.prototype.clear = hashClear;
  Hash.prototype['delete'] = hashDelete;
  Hash.prototype.get = hashGet;
  Hash.prototype.has = hashHas;
  Hash.prototype.set = hashSet;

  /**
   * Removes all key-value entries from the map.
   *
   * @private
   * @name clear
   * @memberOf MapCache
   */
  function mapCacheClear() {
    this.size = 0;
    this.__data__ = {
      'hash': new Hash,
      'map': new (Map$1 || ListCache),
      'string': new Hash
    };
  }

  /**
   * Checks if `value` is suitable for use as unique object key.
   *
   * @private
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is suitable, else `false`.
   */
  function isKeyable(value) {
    var type = typeof value;
    return (type == 'string' || type == 'number' || type == 'symbol' || type == 'boolean')
      ? (value !== '__proto__')
      : (value === null);
  }

  /**
   * Gets the data for `map`.
   *
   * @private
   * @param {Object} map The map to query.
   * @param {string} key The reference key.
   * @returns {*} Returns the map data.
   */
  function getMapData(map, key) {
    var data = map.__data__;
    return isKeyable(key)
      ? data[typeof key == 'string' ? 'string' : 'hash']
      : data.map;
  }

  /**
   * Removes `key` and its value from the map.
   *
   * @private
   * @name delete
   * @memberOf MapCache
   * @param {string} key The key of the value to remove.
   * @returns {boolean} Returns `true` if the entry was removed, else `false`.
   */
  function mapCacheDelete(key) {
    var result = getMapData(this, key)['delete'](key);
    this.size -= result ? 1 : 0;
    return result;
  }

  /**
   * Gets the map value for `key`.
   *
   * @private
   * @name get
   * @memberOf MapCache
   * @param {string} key The key of the value to get.
   * @returns {*} Returns the entry value.
   */
  function mapCacheGet(key) {
    return getMapData(this, key).get(key);
  }

  /**
   * Checks if a map value for `key` exists.
   *
   * @private
   * @name has
   * @memberOf MapCache
   * @param {string} key The key of the entry to check.
   * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
   */
  function mapCacheHas(key) {
    return getMapData(this, key).has(key);
  }

  /**
   * Sets the map `key` to `value`.
   *
   * @private
   * @name set
   * @memberOf MapCache
   * @param {string} key The key of the value to set.
   * @param {*} value The value to set.
   * @returns {Object} Returns the map cache instance.
   */
  function mapCacheSet(key, value) {
    var data = getMapData(this, key),
        size = data.size;

    data.set(key, value);
    this.size += data.size == size ? 0 : 1;
    return this;
  }

  /**
   * Creates a map cache object to store key-value pairs.
   *
   * @private
   * @constructor
   * @param {Array} [entries] The key-value pairs to cache.
   */
  function MapCache(entries) {
    var index = -1,
        length = entries == null ? 0 : entries.length;

    this.clear();
    while (++index < length) {
      var entry = entries[index];
      this.set(entry[0], entry[1]);
    }
  }

  // Add methods to `MapCache`.
  MapCache.prototype.clear = mapCacheClear;
  MapCache.prototype['delete'] = mapCacheDelete;
  MapCache.prototype.get = mapCacheGet;
  MapCache.prototype.has = mapCacheHas;
  MapCache.prototype.set = mapCacheSet;

  /** Used as the size to enable large array optimizations. */
  var LARGE_ARRAY_SIZE = 200;

  /**
   * Sets the stack `key` to `value`.
   *
   * @private
   * @name set
   * @memberOf Stack
   * @param {string} key The key of the value to set.
   * @param {*} value The value to set.
   * @returns {Object} Returns the stack cache instance.
   */
  function stackSet(key, value) {
    var data = this.__data__;
    if (data instanceof ListCache) {
      var pairs = data.__data__;
      if (!Map$1 || (pairs.length < LARGE_ARRAY_SIZE - 1)) {
        pairs.push([key, value]);
        this.size = ++data.size;
        return this;
      }
      data = this.__data__ = new MapCache(pairs);
    }
    data.set(key, value);
    this.size = data.size;
    return this;
  }

  /**
   * Creates a stack cache object to store key-value pairs.
   *
   * @private
   * @constructor
   * @param {Array} [entries] The key-value pairs to cache.
   */
  function Stack(entries) {
    var data = this.__data__ = new ListCache(entries);
    this.size = data.size;
  }

  // Add methods to `Stack`.
  Stack.prototype.clear = stackClear;
  Stack.prototype['delete'] = stackDelete;
  Stack.prototype.get = stackGet;
  Stack.prototype.has = stackHas;
  Stack.prototype.set = stackSet;

  var defineProperty = (function() {
    try {
      var func = getNative(Object, 'defineProperty');
      func({}, '', {});
      return func;
    } catch (e) {}
  }());

  /**
   * The base implementation of `assignValue` and `assignMergeValue` without
   * value checks.
   *
   * @private
   * @param {Object} object The object to modify.
   * @param {string} key The key of the property to assign.
   * @param {*} value The value to assign.
   */
  function baseAssignValue(object, key, value) {
    if (key == '__proto__' && defineProperty) {
      defineProperty(object, key, {
        'configurable': true,
        'enumerable': true,
        'value': value,
        'writable': true
      });
    } else {
      object[key] = value;
    }
  }

  /**
   * This function is like `assignValue` except that it doesn't assign
   * `undefined` values.
   *
   * @private
   * @param {Object} object The object to modify.
   * @param {string} key The key of the property to assign.
   * @param {*} value The value to assign.
   */
  function assignMergeValue(object, key, value) {
    if ((value !== undefined && !eq(object[key], value)) ||
        (value === undefined && !(key in object))) {
      baseAssignValue(object, key, value);
    }
  }

  /**
   * Creates a base function for methods like `_.forIn` and `_.forOwn`.
   *
   * @private
   * @param {boolean} [fromRight] Specify iterating from right to left.
   * @returns {Function} Returns the new base function.
   */
  function createBaseFor(fromRight) {
    return function(object, iteratee, keysFunc) {
      var index = -1,
          iterable = Object(object),
          props = keysFunc(object),
          length = props.length;

      while (length--) {
        var key = props[fromRight ? length : ++index];
        if (iteratee(iterable[key], key, iterable) === false) {
          break;
        }
      }
      return object;
    };
  }

  /**
   * The base implementation of `baseForOwn` which iterates over `object`
   * properties returned by `keysFunc` and invokes `iteratee` for each property.
   * Iteratee functions may exit iteration early by explicitly returning `false`.
   *
   * @private
   * @param {Object} object The object to iterate over.
   * @param {Function} iteratee The function invoked per iteration.
   * @param {Function} keysFunc The function to get the keys of `object`.
   * @returns {Object} Returns `object`.
   */
  var baseFor = createBaseFor();

  /** Detect free variable `exports`. */
  var freeExports = typeof exports == 'object' && exports && !exports.nodeType && exports;

  /** Detect free variable `module`. */
  var freeModule = freeExports && typeof module == 'object' && module && !module.nodeType && module;

  /** Detect the popular CommonJS extension `module.exports`. */
  var moduleExports = freeModule && freeModule.exports === freeExports;

  /** Built-in value references. */
  var Buffer = moduleExports ? root.Buffer : undefined,
      allocUnsafe = Buffer ? Buffer.allocUnsafe : undefined;

  /**
   * Creates a clone of  `buffer`.
   *
   * @private
   * @param {Buffer} buffer The buffer to clone.
   * @param {boolean} [isDeep] Specify a deep clone.
   * @returns {Buffer} Returns the cloned buffer.
   */
  function cloneBuffer(buffer, isDeep) {
    if (isDeep) {
      return buffer.slice();
    }
    var length = buffer.length,
        result = allocUnsafe ? allocUnsafe(length) : new buffer.constructor(length);

    buffer.copy(result);
    return result;
  }

  /** Built-in value references. */
  var Uint8Array = root.Uint8Array;

  /**
   * Creates a clone of `arrayBuffer`.
   *
   * @private
   * @param {ArrayBuffer} arrayBuffer The array buffer to clone.
   * @returns {ArrayBuffer} Returns the cloned array buffer.
   */
  function cloneArrayBuffer(arrayBuffer) {
    var result = new arrayBuffer.constructor(arrayBuffer.byteLength);
    new Uint8Array(result).set(new Uint8Array(arrayBuffer));
    return result;
  }

  /**
   * Creates a clone of `typedArray`.
   *
   * @private
   * @param {Object} typedArray The typed array to clone.
   * @param {boolean} [isDeep] Specify a deep clone.
   * @returns {Object} Returns the cloned typed array.
   */
  function cloneTypedArray(typedArray, isDeep) {
    var buffer = isDeep ? cloneArrayBuffer(typedArray.buffer) : typedArray.buffer;
    return new typedArray.constructor(buffer, typedArray.byteOffset, typedArray.length);
  }

  /**
   * Copies the values of `source` to `array`.
   *
   * @private
   * @param {Array} source The array to copy values from.
   * @param {Array} [array=[]] The array to copy values to.
   * @returns {Array} Returns `array`.
   */
  function copyArray(source, array) {
    var index = -1,
        length = source.length;

    array || (array = Array(length));
    while (++index < length) {
      array[index] = source[index];
    }
    return array;
  }

  /** Built-in value references. */
  var objectCreate = Object.create;

  /**
   * The base implementation of `_.create` without support for assigning
   * properties to the created object.
   *
   * @private
   * @param {Object} proto The object to inherit from.
   * @returns {Object} Returns the new object.
   */
  var baseCreate = (function() {
    function object() {}
    return function(proto) {
      if (!isObject(proto)) {
        return {};
      }
      if (objectCreate) {
        return objectCreate(proto);
      }
      object.prototype = proto;
      var result = new object;
      object.prototype = undefined;
      return result;
    };
  }());

  /** Used for built-in method references. */
  var objectProto$6 = Object.prototype;

  /**
   * Checks if `value` is likely a prototype object.
   *
   * @private
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a prototype, else `false`.
   */
  function isPrototype(value) {
    var Ctor = value && value.constructor,
        proto = (typeof Ctor == 'function' && Ctor.prototype) || objectProto$6;

    return value === proto;
  }

  /**
   * Initializes an object clone.
   *
   * @private
   * @param {Object} object The object to clone.
   * @returns {Object} Returns the initialized clone.
   */
  function initCloneObject(object) {
    return (typeof object.constructor == 'function' && !isPrototype(object))
      ? baseCreate(getPrototype(object))
      : {};
  }

  /** `Object#toString` result references. */
  var argsTag = '[object Arguments]';

  /**
   * The base implementation of `_.isArguments`.
   *
   * @private
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is an `arguments` object,
   */
  function baseIsArguments(value) {
    return isObjectLike(value) && baseGetTag(value) == argsTag;
  }

  /** Used for built-in method references. */
  var objectProto$7 = Object.prototype;

  /** Used to check objects for own properties. */
  var hasOwnProperty$5 = objectProto$7.hasOwnProperty;

  /** Built-in value references. */
  var propertyIsEnumerable = objectProto$7.propertyIsEnumerable;

  /**
   * Checks if `value` is likely an `arguments` object.
   *
   * @static
   * @memberOf _
   * @since 0.1.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is an `arguments` object,
   *  else `false`.
   * @example
   *
   * _.isArguments(function() { return arguments; }());
   * // => true
   *
   * _.isArguments([1, 2, 3]);
   * // => false
   */
  var isArguments = baseIsArguments(function() { return arguments; }()) ? baseIsArguments : function(value) {
    return isObjectLike(value) && hasOwnProperty$5.call(value, 'callee') &&
      !propertyIsEnumerable.call(value, 'callee');
  };

  /** Used as references for various `Number` constants. */
  var MAX_SAFE_INTEGER = 9007199254740991;

  /**
   * Checks if `value` is a valid array-like length.
   *
   * **Note:** This method is loosely based on
   * [`ToLength`](http://ecma-international.org/ecma-262/7.0/#sec-tolength).
   *
   * @static
   * @memberOf _
   * @since 4.0.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a valid length, else `false`.
   * @example
   *
   * _.isLength(3);
   * // => true
   *
   * _.isLength(Number.MIN_VALUE);
   * // => false
   *
   * _.isLength(Infinity);
   * // => false
   *
   * _.isLength('3');
   * // => false
   */
  function isLength(value) {
    return typeof value == 'number' &&
      value > -1 && value % 1 == 0 && value <= MAX_SAFE_INTEGER;
  }

  /**
   * Checks if `value` is array-like. A value is considered array-like if it's
   * not a function and has a `value.length` that's an integer greater than or
   * equal to `0` and less than or equal to `Number.MAX_SAFE_INTEGER`.
   *
   * @static
   * @memberOf _
   * @since 4.0.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is array-like, else `false`.
   * @example
   *
   * _.isArrayLike([1, 2, 3]);
   * // => true
   *
   * _.isArrayLike(document.body.children);
   * // => true
   *
   * _.isArrayLike('abc');
   * // => true
   *
   * _.isArrayLike(_.noop);
   * // => false
   */
  function isArrayLike(value) {
    return value != null && isLength(value.length) && !isFunction(value);
  }

  /**
   * This method is like `_.isArrayLike` except that it also checks if `value`
   * is an object.
   *
   * @static
   * @memberOf _
   * @since 4.0.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is an array-like object,
   *  else `false`.
   * @example
   *
   * _.isArrayLikeObject([1, 2, 3]);
   * // => true
   *
   * _.isArrayLikeObject(document.body.children);
   * // => true
   *
   * _.isArrayLikeObject('abc');
   * // => false
   *
   * _.isArrayLikeObject(_.noop);
   * // => false
   */
  function isArrayLikeObject(value) {
    return isObjectLike(value) && isArrayLike(value);
  }

  /**
   * This method returns `false`.
   *
   * @static
   * @memberOf _
   * @since 4.13.0
   * @category Util
   * @returns {boolean} Returns `false`.
   * @example
   *
   * _.times(2, _.stubFalse);
   * // => [false, false]
   */
  function stubFalse() {
    return false;
  }

  /** Detect free variable `exports`. */
  var freeExports$1 = typeof exports == 'object' && exports && !exports.nodeType && exports;

  /** Detect free variable `module`. */
  var freeModule$1 = freeExports$1 && typeof module == 'object' && module && !module.nodeType && module;

  /** Detect the popular CommonJS extension `module.exports`. */
  var moduleExports$1 = freeModule$1 && freeModule$1.exports === freeExports$1;

  /** Built-in value references. */
  var Buffer$1 = moduleExports$1 ? root.Buffer : undefined;

  /* Built-in method references for those with the same name as other `lodash` methods. */
  var nativeIsBuffer = Buffer$1 ? Buffer$1.isBuffer : undefined;

  /**
   * Checks if `value` is a buffer.
   *
   * @static
   * @memberOf _
   * @since 4.3.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a buffer, else `false`.
   * @example
   *
   * _.isBuffer(new Buffer(2));
   * // => true
   *
   * _.isBuffer(new Uint8Array(2));
   * // => false
   */
  var isBuffer = nativeIsBuffer || stubFalse;

  /** `Object#toString` result references. */
  var argsTag$1 = '[object Arguments]',
      arrayTag = '[object Array]',
      boolTag = '[object Boolean]',
      dateTag = '[object Date]',
      errorTag = '[object Error]',
      funcTag$1 = '[object Function]',
      mapTag = '[object Map]',
      numberTag = '[object Number]',
      objectTag$1 = '[object Object]',
      regexpTag = '[object RegExp]',
      setTag = '[object Set]',
      stringTag = '[object String]',
      weakMapTag = '[object WeakMap]';

  var arrayBufferTag = '[object ArrayBuffer]',
      dataViewTag = '[object DataView]',
      float32Tag = '[object Float32Array]',
      float64Tag = '[object Float64Array]',
      int8Tag = '[object Int8Array]',
      int16Tag = '[object Int16Array]',
      int32Tag = '[object Int32Array]',
      uint8Tag = '[object Uint8Array]',
      uint8ClampedTag = '[object Uint8ClampedArray]',
      uint16Tag = '[object Uint16Array]',
      uint32Tag = '[object Uint32Array]';

  /** Used to identify `toStringTag` values of typed arrays. */
  var typedArrayTags = {};
  typedArrayTags[float32Tag] = typedArrayTags[float64Tag] =
  typedArrayTags[int8Tag] = typedArrayTags[int16Tag] =
  typedArrayTags[int32Tag] = typedArrayTags[uint8Tag] =
  typedArrayTags[uint8ClampedTag] = typedArrayTags[uint16Tag] =
  typedArrayTags[uint32Tag] = true;
  typedArrayTags[argsTag$1] = typedArrayTags[arrayTag] =
  typedArrayTags[arrayBufferTag] = typedArrayTags[boolTag] =
  typedArrayTags[dataViewTag] = typedArrayTags[dateTag] =
  typedArrayTags[errorTag] = typedArrayTags[funcTag$1] =
  typedArrayTags[mapTag] = typedArrayTags[numberTag] =
  typedArrayTags[objectTag$1] = typedArrayTags[regexpTag] =
  typedArrayTags[setTag] = typedArrayTags[stringTag] =
  typedArrayTags[weakMapTag] = false;

  /**
   * The base implementation of `_.isTypedArray` without Node.js optimizations.
   *
   * @private
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
   */
  function baseIsTypedArray(value) {
    return isObjectLike(value) &&
      isLength(value.length) && !!typedArrayTags[baseGetTag(value)];
  }

  /**
   * The base implementation of `_.unary` without support for storing metadata.
   *
   * @private
   * @param {Function} func The function to cap arguments for.
   * @returns {Function} Returns the new capped function.
   */
  function baseUnary(func) {
    return function(value) {
      return func(value);
    };
  }

  /** Detect free variable `exports`. */
  var freeExports$2 = typeof exports == 'object' && exports && !exports.nodeType && exports;

  /** Detect free variable `module`. */
  var freeModule$2 = freeExports$2 && typeof module == 'object' && module && !module.nodeType && module;

  /** Detect the popular CommonJS extension `module.exports`. */
  var moduleExports$2 = freeModule$2 && freeModule$2.exports === freeExports$2;

  /** Detect free variable `process` from Node.js. */
  var freeProcess = moduleExports$2 && freeGlobal.process;

  /** Used to access faster Node.js helpers. */
  var nodeUtil = (function() {
    try {
      // Use `util.types` for Node.js 10+.
      var types = freeModule$2 && freeModule$2.require && freeModule$2.require('util').types;

      if (types) {
        return types;
      }

      // Legacy `process.binding('util')` for Node.js < 10.
      return freeProcess && freeProcess.binding && freeProcess.binding('util');
    } catch (e) {}
  }());

  /* Node.js helper references. */
  var nodeIsTypedArray = nodeUtil && nodeUtil.isTypedArray;

  /**
   * Checks if `value` is classified as a typed array.
   *
   * @static
   * @memberOf _
   * @since 3.0.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
   * @example
   *
   * _.isTypedArray(new Uint8Array);
   * // => true
   *
   * _.isTypedArray([]);
   * // => false
   */
  var isTypedArray = nodeIsTypedArray ? baseUnary(nodeIsTypedArray) : baseIsTypedArray;

  /**
   * Gets the value at `key`, unless `key` is "__proto__" or "constructor".
   *
   * @private
   * @param {Object} object The object to query.
   * @param {string} key The key of the property to get.
   * @returns {*} Returns the property value.
   */
  function safeGet(object, key) {
    if (key === 'constructor' && typeof object[key] === 'function') {
      return;
    }

    if (key == '__proto__') {
      return;
    }

    return object[key];
  }

  /** Used for built-in method references. */
  var objectProto$8 = Object.prototype;

  /** Used to check objects for own properties. */
  var hasOwnProperty$6 = objectProto$8.hasOwnProperty;

  /**
   * Assigns `value` to `key` of `object` if the existing value is not equivalent
   * using [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
   * for equality comparisons.
   *
   * @private
   * @param {Object} object The object to modify.
   * @param {string} key The key of the property to assign.
   * @param {*} value The value to assign.
   */
  function assignValue(object, key, value) {
    var objValue = object[key];
    if (!(hasOwnProperty$6.call(object, key) && eq(objValue, value)) ||
        (value === undefined && !(key in object))) {
      baseAssignValue(object, key, value);
    }
  }

  /**
   * Copies properties of `source` to `object`.
   *
   * @private
   * @param {Object} source The object to copy properties from.
   * @param {Array} props The property identifiers to copy.
   * @param {Object} [object={}] The object to copy properties to.
   * @param {Function} [customizer] The function to customize copied values.
   * @returns {Object} Returns `object`.
   */
  function copyObject(source, props, object, customizer) {
    var isNew = !object;
    object || (object = {});

    var index = -1,
        length = props.length;

    while (++index < length) {
      var key = props[index];

      var newValue = customizer
        ? customizer(object[key], source[key], key, object, source)
        : undefined;

      if (newValue === undefined) {
        newValue = source[key];
      }
      if (isNew) {
        baseAssignValue(object, key, newValue);
      } else {
        assignValue(object, key, newValue);
      }
    }
    return object;
  }

  /**
   * The base implementation of `_.times` without support for iteratee shorthands
   * or max array length checks.
   *
   * @private
   * @param {number} n The number of times to invoke `iteratee`.
   * @param {Function} iteratee The function invoked per iteration.
   * @returns {Array} Returns the array of results.
   */
  function baseTimes(n, iteratee) {
    var index = -1,
        result = Array(n);

    while (++index < n) {
      result[index] = iteratee(index);
    }
    return result;
  }

  /** Used as references for various `Number` constants. */
  var MAX_SAFE_INTEGER$1 = 9007199254740991;

  /** Used to detect unsigned integer values. */
  var reIsUint = /^(?:0|[1-9]\d*)$/;

  /**
   * Checks if `value` is a valid array-like index.
   *
   * @private
   * @param {*} value The value to check.
   * @param {number} [length=MAX_SAFE_INTEGER] The upper bounds of a valid index.
   * @returns {boolean} Returns `true` if `value` is a valid index, else `false`.
   */
  function isIndex(value, length) {
    var type = typeof value;
    length = length == null ? MAX_SAFE_INTEGER$1 : length;

    return !!length &&
      (type == 'number' ||
        (type != 'symbol' && reIsUint.test(value))) &&
          (value > -1 && value % 1 == 0 && value < length);
  }

  /** Used for built-in method references. */
  var objectProto$9 = Object.prototype;

  /** Used to check objects for own properties. */
  var hasOwnProperty$7 = objectProto$9.hasOwnProperty;

  /**
   * Creates an array of the enumerable property names of the array-like `value`.
   *
   * @private
   * @param {*} value The value to query.
   * @param {boolean} inherited Specify returning inherited property names.
   * @returns {Array} Returns the array of property names.
   */
  function arrayLikeKeys(value, inherited) {
    var isArr = isArray(value),
        isArg = !isArr && isArguments(value),
        isBuff = !isArr && !isArg && isBuffer(value),
        isType = !isArr && !isArg && !isBuff && isTypedArray(value),
        skipIndexes = isArr || isArg || isBuff || isType,
        result = skipIndexes ? baseTimes(value.length, String) : [],
        length = result.length;

    for (var key in value) {
      if ((inherited || hasOwnProperty$7.call(value, key)) &&
          !(skipIndexes && (
             // Safari 9 has enumerable `arguments.length` in strict mode.
             key == 'length' ||
             // Node.js 0.10 has enumerable non-index properties on buffers.
             (isBuff && (key == 'offset' || key == 'parent')) ||
             // PhantomJS 2 has enumerable non-index properties on typed arrays.
             (isType && (key == 'buffer' || key == 'byteLength' || key == 'byteOffset')) ||
             // Skip index properties.
             isIndex(key, length)
          ))) {
        result.push(key);
      }
    }
    return result;
  }

  /**
   * This function is like
   * [`Object.keys`](http://ecma-international.org/ecma-262/7.0/#sec-object.keys)
   * except that it includes inherited enumerable properties.
   *
   * @private
   * @param {Object} object The object to query.
   * @returns {Array} Returns the array of property names.
   */
  function nativeKeysIn(object) {
    var result = [];
    if (object != null) {
      for (var key in Object(object)) {
        result.push(key);
      }
    }
    return result;
  }

  /** Used for built-in method references. */
  var objectProto$a = Object.prototype;

  /** Used to check objects for own properties. */
  var hasOwnProperty$8 = objectProto$a.hasOwnProperty;

  /**
   * The base implementation of `_.keysIn` which doesn't treat sparse arrays as dense.
   *
   * @private
   * @param {Object} object The object to query.
   * @returns {Array} Returns the array of property names.
   */
  function baseKeysIn(object) {
    if (!isObject(object)) {
      return nativeKeysIn(object);
    }
    var isProto = isPrototype(object),
        result = [];

    for (var key in object) {
      if (!(key == 'constructor' && (isProto || !hasOwnProperty$8.call(object, key)))) {
        result.push(key);
      }
    }
    return result;
  }

  /**
   * Creates an array of the own and inherited enumerable property names of `object`.
   *
   * **Note:** Non-object values are coerced to objects.
   *
   * @static
   * @memberOf _
   * @since 3.0.0
   * @category Object
   * @param {Object} object The object to query.
   * @returns {Array} Returns the array of property names.
   * @example
   *
   * function Foo() {
   *   this.a = 1;
   *   this.b = 2;
   * }
   *
   * Foo.prototype.c = 3;
   *
   * _.keysIn(new Foo);
   * // => ['a', 'b', 'c'] (iteration order is not guaranteed)
   */
  function keysIn(object) {
    return isArrayLike(object) ? arrayLikeKeys(object, true) : baseKeysIn(object);
  }

  /**
   * Converts `value` to a plain object flattening inherited enumerable string
   * keyed properties of `value` to own properties of the plain object.
   *
   * @static
   * @memberOf _
   * @since 3.0.0
   * @category Lang
   * @param {*} value The value to convert.
   * @returns {Object} Returns the converted plain object.
   * @example
   *
   * function Foo() {
   *   this.b = 2;
   * }
   *
   * Foo.prototype.c = 3;
   *
   * _.assign({ 'a': 1 }, new Foo);
   * // => { 'a': 1, 'b': 2 }
   *
   * _.assign({ 'a': 1 }, _.toPlainObject(new Foo));
   * // => { 'a': 1, 'b': 2, 'c': 3 }
   */
  function toPlainObject(value) {
    return copyObject(value, keysIn(value));
  }

  /**
   * A specialized version of `baseMerge` for arrays and objects which performs
   * deep merges and tracks traversed objects enabling objects with circular
   * references to be merged.
   *
   * @private
   * @param {Object} object The destination object.
   * @param {Object} source The source object.
   * @param {string} key The key of the value to merge.
   * @param {number} srcIndex The index of `source`.
   * @param {Function} mergeFunc The function to merge values.
   * @param {Function} [customizer] The function to customize assigned values.
   * @param {Object} [stack] Tracks traversed source values and their merged
   *  counterparts.
   */
  function baseMergeDeep(object, source, key, srcIndex, mergeFunc, customizer, stack) {
    var objValue = safeGet(object, key),
        srcValue = safeGet(source, key),
        stacked = stack.get(srcValue);

    if (stacked) {
      assignMergeValue(object, key, stacked);
      return;
    }
    var newValue = customizer
      ? customizer(objValue, srcValue, (key + ''), object, source, stack)
      : undefined;

    var isCommon = newValue === undefined;

    if (isCommon) {
      var isArr = isArray(srcValue),
          isBuff = !isArr && isBuffer(srcValue),
          isTyped = !isArr && !isBuff && isTypedArray(srcValue);

      newValue = srcValue;
      if (isArr || isBuff || isTyped) {
        if (isArray(objValue)) {
          newValue = objValue;
        }
        else if (isArrayLikeObject(objValue)) {
          newValue = copyArray(objValue);
        }
        else if (isBuff) {
          isCommon = false;
          newValue = cloneBuffer(srcValue, true);
        }
        else if (isTyped) {
          isCommon = false;
          newValue = cloneTypedArray(srcValue, true);
        }
        else {
          newValue = [];
        }
      }
      else if (isPlainObject(srcValue) || isArguments(srcValue)) {
        newValue = objValue;
        if (isArguments(objValue)) {
          newValue = toPlainObject(objValue);
        }
        else if (!isObject(objValue) || isFunction(objValue)) {
          newValue = initCloneObject(srcValue);
        }
      }
      else {
        isCommon = false;
      }
    }
    if (isCommon) {
      // Recursively merge objects and arrays (susceptible to call stack limits).
      stack.set(srcValue, newValue);
      mergeFunc(newValue, srcValue, srcIndex, customizer, stack);
      stack['delete'](srcValue);
    }
    assignMergeValue(object, key, newValue);
  }

  /**
   * The base implementation of `_.merge` without support for multiple sources.
   *
   * @private
   * @param {Object} object The destination object.
   * @param {Object} source The source object.
   * @param {number} srcIndex The index of `source`.
   * @param {Function} [customizer] The function to customize merged values.
   * @param {Object} [stack] Tracks traversed source values and their merged
   *  counterparts.
   */
  function baseMerge(object, source, srcIndex, customizer, stack) {
    if (object === source) {
      return;
    }
    baseFor(source, function(srcValue, key) {
      stack || (stack = new Stack);
      if (isObject(srcValue)) {
        baseMergeDeep(object, source, key, srcIndex, baseMerge, customizer, stack);
      }
      else {
        var newValue = customizer
          ? customizer(safeGet(object, key), srcValue, (key + ''), object, source, stack)
          : undefined;

        if (newValue === undefined) {
          newValue = srcValue;
        }
        assignMergeValue(object, key, newValue);
      }
    }, keysIn);
  }

  /**
   * This method returns the first argument it receives.
   *
   * @static
   * @since 0.1.0
   * @memberOf _
   * @category Util
   * @param {*} value Any value.
   * @returns {*} Returns `value`.
   * @example
   *
   * var object = { 'a': 1 };
   *
   * console.log(_.identity(object) === object);
   * // => true
   */
  function identity(value) {
    return value;
  }

  /**
   * A faster alternative to `Function#apply`, this function invokes `func`
   * with the `this` binding of `thisArg` and the arguments of `args`.
   *
   * @private
   * @param {Function} func The function to invoke.
   * @param {*} thisArg The `this` binding of `func`.
   * @param {Array} args The arguments to invoke `func` with.
   * @returns {*} Returns the result of `func`.
   */
  function apply(func, thisArg, args) {
    switch (args.length) {
      case 0: return func.call(thisArg);
      case 1: return func.call(thisArg, args[0]);
      case 2: return func.call(thisArg, args[0], args[1]);
      case 3: return func.call(thisArg, args[0], args[1], args[2]);
    }
    return func.apply(thisArg, args);
  }

  /* Built-in method references for those with the same name as other `lodash` methods. */
  var nativeMax = Math.max;

  /**
   * A specialized version of `baseRest` which transforms the rest array.
   *
   * @private
   * @param {Function} func The function to apply a rest parameter to.
   * @param {number} [start=func.length-1] The start position of the rest parameter.
   * @param {Function} transform The rest array transform.
   * @returns {Function} Returns the new function.
   */
  function overRest(func, start, transform) {
    start = nativeMax(start === undefined ? (func.length - 1) : start, 0);
    return function() {
      var args = arguments,
          index = -1,
          length = nativeMax(args.length - start, 0),
          array = Array(length);

      while (++index < length) {
        array[index] = args[start + index];
      }
      index = -1;
      var otherArgs = Array(start + 1);
      while (++index < start) {
        otherArgs[index] = args[index];
      }
      otherArgs[start] = transform(array);
      return apply(func, this, otherArgs);
    };
  }

  /**
   * Creates a function that returns `value`.
   *
   * @static
   * @memberOf _
   * @since 2.4.0
   * @category Util
   * @param {*} value The value to return from the new function.
   * @returns {Function} Returns the new constant function.
   * @example
   *
   * var objects = _.times(2, _.constant({ 'a': 1 }));
   *
   * console.log(objects);
   * // => [{ 'a': 1 }, { 'a': 1 }]
   *
   * console.log(objects[0] === objects[1]);
   * // => true
   */
  function constant(value) {
    return function() {
      return value;
    };
  }

  /**
   * The base implementation of `setToString` without support for hot loop shorting.
   *
   * @private
   * @param {Function} func The function to modify.
   * @param {Function} string The `toString` result.
   * @returns {Function} Returns `func`.
   */
  var baseSetToString = !defineProperty ? identity : function(func, string) {
    return defineProperty(func, 'toString', {
      'configurable': true,
      'enumerable': false,
      'value': constant(string),
      'writable': true
    });
  };

  /** Used to detect hot functions by number of calls within a span of milliseconds. */
  var HOT_COUNT = 800,
      HOT_SPAN = 16;

  /* Built-in method references for those with the same name as other `lodash` methods. */
  var nativeNow = Date.now;

  /**
   * Creates a function that'll short out and invoke `identity` instead
   * of `func` when it's called `HOT_COUNT` or more times in `HOT_SPAN`
   * milliseconds.
   *
   * @private
   * @param {Function} func The function to restrict.
   * @returns {Function} Returns the new shortable function.
   */
  function shortOut(func) {
    var count = 0,
        lastCalled = 0;

    return function() {
      var stamp = nativeNow(),
          remaining = HOT_SPAN - (stamp - lastCalled);

      lastCalled = stamp;
      if (remaining > 0) {
        if (++count >= HOT_COUNT) {
          return arguments[0];
        }
      } else {
        count = 0;
      }
      return func.apply(undefined, arguments);
    };
  }

  /**
   * Sets the `toString` method of `func` to return `string`.
   *
   * @private
   * @param {Function} func The function to modify.
   * @param {Function} string The `toString` result.
   * @returns {Function} Returns `func`.
   */
  var setToString = shortOut(baseSetToString);

  /**
   * The base implementation of `_.rest` which doesn't validate or coerce arguments.
   *
   * @private
   * @param {Function} func The function to apply a rest parameter to.
   * @param {number} [start=func.length-1] The start position of the rest parameter.
   * @returns {Function} Returns the new function.
   */
  function baseRest(func, start) {
    return setToString(overRest(func, start, identity), func + '');
  }

  /**
   * Checks if the given arguments are from an iteratee call.
   *
   * @private
   * @param {*} value The potential iteratee value argument.
   * @param {*} index The potential iteratee index or key argument.
   * @param {*} object The potential iteratee object argument.
   * @returns {boolean} Returns `true` if the arguments are from an iteratee call,
   *  else `false`.
   */
  function isIterateeCall(value, index, object) {
    if (!isObject(object)) {
      return false;
    }
    var type = typeof index;
    if (type == 'number'
          ? (isArrayLike(object) && isIndex(index, object.length))
          : (type == 'string' && index in object)
        ) {
      return eq(object[index], value);
    }
    return false;
  }

  /**
   * Creates a function like `_.assign`.
   *
   * @private
   * @param {Function} assigner The function to assign values.
   * @returns {Function} Returns the new assigner function.
   */
  function createAssigner(assigner) {
    return baseRest(function(object, sources) {
      var index = -1,
          length = sources.length,
          customizer = length > 1 ? sources[length - 1] : undefined,
          guard = length > 2 ? sources[2] : undefined;

      customizer = (assigner.length > 3 && typeof customizer == 'function')
        ? (length--, customizer)
        : undefined;

      if (guard && isIterateeCall(sources[0], sources[1], guard)) {
        customizer = length < 3 ? undefined : customizer;
        length = 1;
      }
      object = Object(object);
      while (++index < length) {
        var source = sources[index];
        if (source) {
          assigner(object, source, index, customizer);
        }
      }
      return object;
    });
  }

  /**
   * This method is like `_.assign` except that it recursively merges own and
   * inherited enumerable string keyed properties of source objects into the
   * destination object. Source properties that resolve to `undefined` are
   * skipped if a destination value exists. Array and plain object properties
   * are merged recursively. Other objects and value types are overridden by
   * assignment. Source objects are applied from left to right. Subsequent
   * sources overwrite property assignments of previous sources.
   *
   * **Note:** This method mutates `object`.
   *
   * @static
   * @memberOf _
   * @since 0.5.0
   * @category Object
   * @param {Object} object The destination object.
   * @param {...Object} [sources] The source objects.
   * @returns {Object} Returns `object`.
   * @example
   *
   * var object = {
   *   'a': [{ 'b': 2 }, { 'd': 4 }]
   * };
   *
   * var other = {
   *   'a': [{ 'c': 3 }, { 'e': 5 }]
   * };
   *
   * _.merge(object, other);
   * // => { 'a': [{ 'b': 2, 'c': 3 }, { 'd': 4, 'e': 5 }] }
   */
  var merge = createAssigner(function(object, source, srcIndex) {
    baseMerge(object, source, srcIndex);
  });

  /**
   * this is essentially the same as the injectToFn
   * but this will not allow overwrite and set the setter and getter
   * @param {object} obj to get injected
   * @param {string} name of the property
   * @param {function} setter for set
   * @param {function} [getter=null] for get default return null fn
   * @return {object} the injected obj
   */
  function objDefineProps(obj, name, setter, getter) {
    if ( getter === void 0 ) getter = null;

    if (Object.getOwnPropertyDescriptor(obj, name) === undefined) {
      Object.defineProperty(obj, name, {
        set: setter,
        get: getter === null ? function() { return null; } : getter
      });
    }
    return obj
  }

  /**
   * check if the object has name property
   * @param {object} obj the object to check
   * @param {string} name the prop name
   * @return {*} the value or undefined
   */
  function objHasProp(obj, name) {
    var prop = Object.getOwnPropertyDescriptor(obj, name);
    return prop !== undefined && prop.value ? prop.value : prop
  }

  /**
   * After the user login we will use this Object.define add a new property
   * to the resolver with the decoded user data
   * @param {function} resolver target resolver
   * @param {string} name the name of the object to get inject also for checking
   * @param {object} data to inject into the function static interface
   * @param {boolean} [overwrite=false] if we want to overwrite the existing data
   * @return {function} added property resolver
   */
  function injectToFn(resolver, name, data, overwrite) {
    if ( overwrite === void 0 ) overwrite = false;

    var check = objHasProp(resolver, name);
    if (overwrite === false && check !== undefined) {
      // console.info(`NOT INJECTED`)
      return resolver
    }
    /* this will throw error! @TODO how to remove props? 
    if (overwrite === true && check !== undefined) {
      delete resolver[name] // delete this property
    }
    */
    // console.info(`INJECTED`)
    Object.defineProperty(resolver, name, {
      value: data,
      writable: overwrite // if its set to true then we should able to overwrite it
    });

    return resolver
  }

  var NO_ERROR_MSG = 'No message';
  var NO_STATUS_CODE = -1;
  var UNAUTHORIZED_STATUS = 401;
  var FORBIDDEN_STATUS = 403;
  var NOT_FOUND_STATUS = 404;
  var NOT_ACCEPTABLE_STATUS = 406;
  var SERVER_INTERNAL_STATUS = 500;

  /**
   * This is a custom error to throw when server throw a 406
   * This help us to capture the right error, due to the call happens in sequence
   * @param {string} message to tell what happen
   * @param {mixed} extra things we want to add, 500?
   */
  var Jsonql406Error = /*@__PURE__*/(function (Error) {
    function Jsonql406Error() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      Error.apply(this, args);
      this.message = args[0];
      this.detail = args[1];
      // We can't access the static name from an instance
      // but we can do it like this
      this.className = Jsonql406Error.name;

      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, Jsonql406Error);
      }
    }

    if ( Error ) Jsonql406Error.__proto__ = Error;
    Jsonql406Error.prototype = Object.create( Error && Error.prototype );
    Jsonql406Error.prototype.constructor = Jsonql406Error;

    var staticAccessors = { statusCode: { configurable: true },name: { configurable: true } };

    staticAccessors.statusCode.get = function () {
      return NOT_ACCEPTABLE_STATUS
    };

    staticAccessors.name.get = function () {
      return 'Jsonql406Error'
    };

    Object.defineProperties( Jsonql406Error, staticAccessors );

    return Jsonql406Error;
  }(Error));

  /**
   * This is a custom error to throw when server throw a 500
   * This help us to capture the right error, due to the call happens in sequence
   * @param {string} message to tell what happen
   * @param {mixed} extra things we want to add, 500?
   */
  var Jsonql500Error = /*@__PURE__*/(function (Error) {
    function Jsonql500Error() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      Error.apply(this, args);

      this.message = args[0];
      this.detail = args[1];

      this.className = Jsonql500Error.name;

      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, Jsonql500Error);
      }
    }

    if ( Error ) Jsonql500Error.__proto__ = Error;
    Jsonql500Error.prototype = Object.create( Error && Error.prototype );
    Jsonql500Error.prototype.constructor = Jsonql500Error;

    var staticAccessors = { statusCode: { configurable: true },name: { configurable: true } };

    staticAccessors.statusCode.get = function () {
      return SERVER_INTERNAL_STATUS
    };

    staticAccessors.name.get = function () {
      return 'Jsonql500Error'
    };

    Object.defineProperties( Jsonql500Error, staticAccessors );

    return Jsonql500Error;
  }(Error));

  /**
   * this is the 403 Forbidden error
   * that means this user is not login
   * use the 401 for try to login and failed
   * @param {string} message to tell what happen
   * @param {mixed} extra things we want to add, 500?
   */
  var JsonqlForbiddenError = /*@__PURE__*/(function (Error) {
    function JsonqlForbiddenError() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      Error.apply(this, args);
      this.message = args[0];
      this.detail = args[1];

      this.className = JsonqlForbiddenError.name;

      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, JsonqlForbiddenError);
      }
    }

    if ( Error ) JsonqlForbiddenError.__proto__ = Error;
    JsonqlForbiddenError.prototype = Object.create( Error && Error.prototype );
    JsonqlForbiddenError.prototype.constructor = JsonqlForbiddenError;

    var staticAccessors = { statusCode: { configurable: true },name: { configurable: true } };

    staticAccessors.statusCode.get = function () {
      return FORBIDDEN_STATUS
    };

    staticAccessors.name.get = function () {
      return 'JsonqlForbiddenError';
    };

    Object.defineProperties( JsonqlForbiddenError, staticAccessors );

    return JsonqlForbiddenError;
  }(Error));

  /**
   * This is a custom error to throw when pass credential but fail
   * This help us to capture the right error, due to the call happens in sequence
   * @param {string} message to tell what happen
   * @param {mixed} extra things we want to add, 500?
   */
  var JsonqlAuthorisationError = /*@__PURE__*/(function (Error) {
    function JsonqlAuthorisationError() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      Error.apply(this, args);
      this.message = args[0];
      this.detail = args[1];

      this.className = JsonqlAuthorisationError.name;

      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, JsonqlAuthorisationError);
      }
    }

    if ( Error ) JsonqlAuthorisationError.__proto__ = Error;
    JsonqlAuthorisationError.prototype = Object.create( Error && Error.prototype );
    JsonqlAuthorisationError.prototype.constructor = JsonqlAuthorisationError;

    var staticAccessors = { statusCode: { configurable: true },name: { configurable: true } };

    staticAccessors.statusCode.get = function () {
      return UNAUTHORIZED_STATUS
    };

    staticAccessors.name.get = function () {
      return 'JsonqlAuthorisationError';
    };

    Object.defineProperties( JsonqlAuthorisationError, staticAccessors );

    return JsonqlAuthorisationError;
  }(Error));

  /**
   * This is a custom error when not supply the credential and try to get contract
   * This help us to capture the right error, due to the call happens in sequence
   * @param {string} message to tell what happen
   * @param {mixed} extra things we want to add, 500?
   */
  var JsonqlContractAuthError = /*@__PURE__*/(function (Error) {
    function JsonqlContractAuthError() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      Error.apply(this, args);
      this.message = args[0];
      this.detail = args[1];

      this.className = JsonqlContractAuthError.name;

      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, JsonqlContractAuthError);
      }
    }

    if ( Error ) JsonqlContractAuthError.__proto__ = Error;
    JsonqlContractAuthError.prototype = Object.create( Error && Error.prototype );
    JsonqlContractAuthError.prototype.constructor = JsonqlContractAuthError;

    var staticAccessors = { statusCode: { configurable: true },name: { configurable: true } };

    staticAccessors.statusCode.get = function () {
      return UNAUTHORIZED_STATUS
    };

    staticAccessors.name.get = function () {
      return 'JsonqlContractAuthError'
    };

    Object.defineProperties( JsonqlContractAuthError, staticAccessors );

    return JsonqlContractAuthError;
  }(Error));

  /**
   * This is a custom error to throw when the resolver throw error and capture inside the middleware
   * This help us to capture the right error, due to the call happens in sequence
   * @param {string} message to tell what happen
   * @param {mixed} extra things we want to add, 500?
   */
  var JsonqlResolverAppError = /*@__PURE__*/(function (Error) {
    function JsonqlResolverAppError() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      Error.apply(this, args);

      this.message = args[0];
      this.detail = args[1];

      this.className = JsonqlResolverAppError.name;

      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, JsonqlResolverAppError);
      }
    }

    if ( Error ) JsonqlResolverAppError.__proto__ = Error;
    JsonqlResolverAppError.prototype = Object.create( Error && Error.prototype );
    JsonqlResolverAppError.prototype.constructor = JsonqlResolverAppError;

    var staticAccessors = { statusCode: { configurable: true },name: { configurable: true } };

    staticAccessors.statusCode.get = function () {
      return SERVER_INTERNAL_STATUS
    };

    staticAccessors.name.get = function () {
      return 'JsonqlResolverAppError'
    };

    Object.defineProperties( JsonqlResolverAppError, staticAccessors );

    return JsonqlResolverAppError;
  }(Error));

  /**
   * This is a custom error to throw when could not find the resolver
   * This help us to capture the right error, due to the call happens in sequence
   * @param {string} message to tell what happen
   * @param {mixed} extra things we want to add, 500?
   */
  var JsonqlResolverNotFoundError = /*@__PURE__*/(function (Error) {
    function JsonqlResolverNotFoundError() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      Error.apply(this, args);

      this.message = args[0];
      this.detail = args[1];

      this.className = JsonqlResolverNotFoundError.name;

      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, JsonqlResolverNotFoundError);
      }
    }

    if ( Error ) JsonqlResolverNotFoundError.__proto__ = Error;
    JsonqlResolverNotFoundError.prototype = Object.create( Error && Error.prototype );
    JsonqlResolverNotFoundError.prototype.constructor = JsonqlResolverNotFoundError;

    var staticAccessors = { statusCode: { configurable: true },name: { configurable: true } };

    staticAccessors.statusCode.get = function () {
      return NOT_FOUND_STATUS
    };

    staticAccessors.name.get = function () {
      return 'JsonqlResolverNotFoundError';
    };

    Object.defineProperties( JsonqlResolverNotFoundError, staticAccessors );

    return JsonqlResolverNotFoundError;
  }(Error));

  // this get throw from within the checkOptions when run through the enum failed
  var JsonqlEnumError = /*@__PURE__*/(function (Error) {
    function JsonqlEnumError() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      Error.apply(this, args);

      this.message = args[0];
      this.detail = args[1];

      this.className = JsonqlEnumError.name;

      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, JsonqlEnumError);
      }
    }

    if ( Error ) JsonqlEnumError.__proto__ = Error;
    JsonqlEnumError.prototype = Object.create( Error && Error.prototype );
    JsonqlEnumError.prototype.constructor = JsonqlEnumError;

    var staticAccessors = { name: { configurable: true } };

    staticAccessors.name.get = function () {
      return 'JsonqlEnumError'
    };

    Object.defineProperties( JsonqlEnumError, staticAccessors );

    return JsonqlEnumError;
  }(Error));

  // this will throw from inside the checkOptions
  var JsonqlTypeError = /*@__PURE__*/(function (Error) {
    function JsonqlTypeError() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      Error.apply(this, args);

      this.message = args[0];
      this.detail = args[1];

      this.className = JsonqlTypeError.name;

      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, JsonqlTypeError);
      }
    }

    if ( Error ) JsonqlTypeError.__proto__ = Error;
    JsonqlTypeError.prototype = Object.create( Error && Error.prototype );
    JsonqlTypeError.prototype.constructor = JsonqlTypeError;

    var staticAccessors = { name: { configurable: true } };

    staticAccessors.name.get = function () {
      return 'JsonqlTypeError'
    };

    Object.defineProperties( JsonqlTypeError, staticAccessors );

    return JsonqlTypeError;
  }(Error));

  // allow supply a custom checker function
  // if that failed then we throw this error
  var JsonqlCheckerError = /*@__PURE__*/(function (Error) {
    function JsonqlCheckerError() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      Error.apply(this, args);
      this.message = args[0];
      this.detail = args[1];

      this.className = JsonqlCheckerError.name;

      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, JsonqlCheckerError);
      }
    }

    if ( Error ) JsonqlCheckerError.__proto__ = Error;
    JsonqlCheckerError.prototype = Object.create( Error && Error.prototype );
    JsonqlCheckerError.prototype.constructor = JsonqlCheckerError;

    var staticAccessors = { name: { configurable: true } };

    staticAccessors.name.get = function () {
      return 'JsonqlCheckerError'
    };

    Object.defineProperties( JsonqlCheckerError, staticAccessors );

    return JsonqlCheckerError;
  }(Error));

  // custom validation error class
  // when validaton failed
  var JsonqlValidationError = /*@__PURE__*/(function (Error) {
    function JsonqlValidationError() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      Error.apply(this, args);

      this.message = args[0];
      this.detail = args[1];

      this.className = JsonqlValidationError.name;

      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, JsonqlValidationError);
      }
    }

    if ( Error ) JsonqlValidationError.__proto__ = Error;
    JsonqlValidationError.prototype = Object.create( Error && Error.prototype );
    JsonqlValidationError.prototype.constructor = JsonqlValidationError;

    var staticAccessors = { name: { configurable: true } };

    staticAccessors.name.get = function () {
      return 'JsonqlValidationError'
    };

    Object.defineProperties( JsonqlValidationError, staticAccessors );

    return JsonqlValidationError;
  }(Error));

  /**
   * This is a custom error to throw whenever a error happen inside the jsonql
   * This help us to capture the right error, due to the call happens in sequence
   * @param {string} message to tell what happen
   * @param {mixed} extra things we want to add, 500?
   */
  var JsonqlError$1 = /*@__PURE__*/(function (Error) {
    function JsonqlError() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      Error.apply(this, args);

      this.message = args[0];
      this.detail = args[1];

      this.className = JsonqlError.name;

      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, JsonqlError);
        // this.detail = this.stack;
      }
    }

    if ( Error ) JsonqlError.__proto__ = Error;
    JsonqlError.prototype = Object.create( Error && Error.prototype );
    JsonqlError.prototype.constructor = JsonqlError;

    var staticAccessors = { name: { configurable: true },statusCode: { configurable: true } };

    staticAccessors.name.get = function () {
      return 'JsonqlError'
    };

    staticAccessors.statusCode.get = function () {
      return NO_STATUS_CODE
    };

    Object.defineProperties( JsonqlError, staticAccessors );

    return JsonqlError;
  }(Error));

  // this is from an example from Koa team to use for internal middleware ctx.throw
  // but after the test the res.body part is unable to extract the required data
  // I keep this one here for future reference

  var JsonqlServerError = /*@__PURE__*/(function (Error) {
    function JsonqlServerError(statusCode, message) {
      Error.call(this, message);
      this.statusCode = statusCode;
      this.className = JsonqlServerError.name;
    }

    if ( Error ) JsonqlServerError.__proto__ = Error;
    JsonqlServerError.prototype = Object.create( Error && Error.prototype );
    JsonqlServerError.prototype.constructor = JsonqlServerError;

    var staticAccessors = { statusCode: { configurable: true },name: { configurable: true } };

    staticAccessors.statusCode.get = function () {
      return SERVER_INTERNAL_STATUS
    };

    staticAccessors.name.get = function () {
      return 'JsonqlServerError'
    };

    Object.defineProperties( JsonqlServerError, staticAccessors );

    return JsonqlServerError;
  }(Error));

  /**
   * this will put into generator call at the very end and catch
   * the error throw from inside then throw again
   * this is necessary because we split calls inside and the throw
   * will not reach the actual client unless we do it this way
   * @param {object} e Error
   * @return {void} just throw
   */
  function finalCatch(e) {
    // this is a hack to get around the validateAsync not actually throw error
    // instead it just rejected it with the array of failed parameters
    if (Array.isArray(e)) {
      // if we want the message then I will have to create yet another function
      // to wrap this function to provide the name prop
      throw new JsonqlValidationError('', e)
    }
    var msg = e.message || NO_ERROR_MSG;
    var detail = e.detail || e;
    // @BUG the instance of not always work for some reason!
    // need to figure out a better way to find out the type of the error
    switch (true) {
      case e instanceof Jsonql406Error:
        throw new Jsonql406Error(msg, detail)
      case e instanceof Jsonql500Error:
        throw new Jsonql500Error(msg, detail)
      case e instanceof JsonqlForbiddenError:
        throw new JsonqlForbiddenError(msg, detail)
      case e instanceof JsonqlAuthorisationError:
        throw new JsonqlAuthorisationError(msg, detail)
      case e instanceof JsonqlContractAuthError:
        throw new JsonqlContractAuthError(msg, detail)
      case e instanceof JsonqlResolverAppError:
        throw new JsonqlResolverAppError(msg, detail)
      case e instanceof JsonqlResolverNotFoundError:
        throw new JsonqlResolverNotFoundError(msg, detail)
      case e instanceof JsonqlEnumError:
        throw new JsonqlEnumError(msg, detail)
      case e instanceof JsonqlTypeError:
        throw new JsonqlTypeError(msg, detail)
      case e instanceof JsonqlCheckerError:
        throw new JsonqlCheckerError(msg, detail)
      case e instanceof JsonqlValidationError:
        throw new JsonqlValidationError(msg, detail)
      case e instanceof JsonqlServerError:
        throw new JsonqlServerError(msg, detail)
      default:
        throw new JsonqlError$1(msg, detail)
    }
  }

  // split the contract into the node side and the generic side
  /**
   * Check if the json is a contract file or not
   * @param {object} contract json object
   * @return {boolean} true
   */
  function checkIsContract(contract) {
    return isPlainObject(contract)
    && (
      isObjectHasKey(contract, QUERY_NAME)
   || isObjectHasKey(contract, MUTATION_NAME)
   || isObjectHasKey(contract, SOCKET_NAME)
    )
  }

  /**
   * Wrapper method that check if it's contract then return the contract or false
   * @param {object} contract the object to check
   * @return {boolean | object} false when it's not
   */
  function isContract(contract) {
    return checkIsContract(contract) ? contract : false
  }

  /**
   * Ported from jsonql-params-validator but different
   * if we don't find the socket part then return false
   * @param {object} contract the contract object
   * @return {object|boolean} false on failed
   */
  function extractSocketPart(contract) {
    if (isObjectHasKey(contract, SOCKET_NAME)) {
      return contract[SOCKET_NAME]
    }
    return false
  }

  /**
   * @param {boolean} sec return in second or not
   * @return {number} timestamp
   */
  var timestamp = function (sec) {
    if ( sec === void 0 ) sec = false;

    var time = Date.now();
    return sec ? Math.floor( time / 1000 ) : time
  };

  /** `Object#toString` result references. */
  var stringTag$1 = '[object String]';

  /**
   * Checks if `value` is classified as a `String` primitive or object.
   *
   * @static
   * @since 0.1.0
   * @memberOf _
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a string, else `false`.
   * @example
   *
   * _.isString('abc');
   * // => true
   *
   * _.isString(1);
   * // => false
   */
  function isString(value) {
    return typeof value == 'string' ||
      (!isArray(value) && isObjectLike(value) && baseGetTag(value) == stringTag$1);
  }

  // ported from jsonql-params-validator

  /**
   * @param {*} args arguments to send
   *@return {object} formatted payload
   */
  var formatPayload = function (args) {
    var obj;

    return (
    ( obj = {}, obj[QUERY_ARG_NAME] = args, obj )
  );
  };

  /**
   * wrapper method to add the timestamp as well
   * @param {string} resolverName name of the resolver
   * @param {*} payload what is sending 
   * @param {object} extra additonal property we want to merge into the deliverable
   * @return {object} delierable
   */
  function createDeliverable(resolverName, payload, extra) {
    var obj;

    if ( extra === void 0 ) extra = {};
    return Object.assign(( obj = {}, obj[resolverName] = payload, obj[TIMESTAMP_PARAM_NAME] = [ timestamp() ], obj ), extra)
  }

  /**
   * @param {string} resolverName name of function
   * @param {array} [args=[]] from the ...args
   * @param {boolean} [jsonp = false] add v1.3.0 to koa
   * @return {object} formatted argument
   */
  function createQuery(resolverName, args, jsonp) {
    if ( args === void 0 ) args = [];
    if ( jsonp === void 0 ) jsonp = false;

    if (isString(resolverName) && isArray(args)) {
      var payload = formatPayload(args);
      if (jsonp === true) {
        return payload
      }
      return createDeliverable(resolverName, payload)
    }
    throw new JsonqlValidationError('utils:params-api:createQuery', { 
      message: "expect resolverName to be string and args to be array!",
      resolverName: resolverName, 
      args: args 
    })
  }

  /**
   * string version of the createQuery
   * @return {string}
   */
  function createQueryStr(resolverName, args, jsonp) {
    if ( args === void 0 ) args = [];
    if ( jsonp === void 0 ) jsonp = false;

    return JSON.stringify(createQuery(resolverName, args, jsonp))
  }

  // take out all the namespace related methods in one place for easy to find
  var SOCKET_NOT_FOUND_ERR = "socket not found in contract!";
  var SIZE = 'size';

  /**
   * create the group using publicNamespace when there is only public
   * @param {object} socket from contract
   * @param {string} publicNamespace
   */
  function groupPublicNamespace(socket, publicNamespace) {
    var obj;

    var g = {};
    for (var resolverName in socket) {
      var params = socket[resolverName];
      g[resolverName] = params;
    }
    return { size: 1, nspGroup: ( obj = {}, obj[publicNamespace] = g, obj ), publicNamespace: publicNamespace}
  }


  /**
   * @BUG we should check the socket part instead of expect the downstream to read the menu!
   * We only need this when the enableAuth is true otherwise there is only one namespace
   * @param {object} contract the socket part of the contract file
   * @return {object} 1. remap the contract using the namespace --> resolvers
   * 2. the size of the object (1 all private, 2 mixed public with private)
   * 3. which namespace is public
   */
  function groupByNamespace(contract) {
    var socket = extractSocketPart(contract);
    if (socket === false) {
      throw new JsonqlError('groupByNamespace', SOCKET_NOT_FOUND_ERR)
    }
    var prop = {};
    prop[NSP_GROUP] = {};
    prop[PUBLIC_NAMESPACE] = null;
    prop[SIZE] = 0;

    for (var resolverName in socket) {
      var params = socket[resolverName];
      var namespace = params.namespace;
      if (namespace) {
        if (!prop[NSP_GROUP][namespace]) {
          ++prop[SIZE];
          prop[NSP_GROUP][namespace] = {};
        }
        prop[NSP_GROUP][namespace][resolverName] = params;
        // get the public namespace
        if (!prop[PUBLIC_NAMESPACE] && params[PUBLIC_KEY]) {
          prop[PUBLIC_NAMESPACE] = namespace;
        }
      }
    }
    
    return prop 
  }

  /**
   * @TODO this might change, what if we want to do room with ws
   * 1. there will only be max two namespace
   * 2. when it's normal we will have the stock path as namespace
   * 3. when enableAuth then we will have two, one is jsonql/public + private
   * @param {object} config options
   * @return {array} of namespace(s)
   */
  function getNamespace(config) {
    var base = JSONQL_PATH;
    if (config.enableAuth) {
      // the public come first @1.0.1 we use the constants instead of the user supplied value
      // @1.0.4 we use the config value again, because we could control this via the post init
      return [
        [ base , config.privateNamespace ].join('/'),
        [ base , config.publicNamespace ].join('/')
      ]
    }
    return [ base ]
  }

  /**
   * get the private namespace
   * @param {array} namespaces array
   * @return {*} string on success
   */
  function getPrivateNamespace$1(namespaces) {
    return namespaces.length > 1 ? namespaces[0] : false
  }

  /**
   * Got a problem with a contract that is public only the groupByNamespace is wrong
   * which is actually not a problem when using a fallback, but to be sure things in order
   * we could combine with the config to group it
   * @param {object} config configuration
   * @return {object} nspInfo object
   */
  function getNspInfoByConfig(config) {
    var contract = config.contract;
    var enableAuth = config.enableAuth;
    var namespaces = getNamespace(config);
    var nspInfo = enableAuth ? groupByNamespace(contract)
                             : groupPublicNamespace(contract.socket, namespaces[0]);
    // add the namespaces into it as well
    return Object.assign(nspInfo, { namespaces: namespaces })
  }

  // There are the socket related methods ported back from 

  var PAYLOAD_NOT_DECODED_ERR = 'payload can not decoded';
  var WS_KEYS = [
    WS_REPLY_TYPE,
    WS_EVT_NAME,
    WS_DATA_NAME
  ];

  /**
   * @param {string|object} payload should be string when reply but could be transformed
   * @return {boolean} true is OK
   */
  var isWsReply = function (payload) {
    var json = isString(payload) ? toJson(payload) : payload;
    var data = json.data;
    if (data) {
      var result = WS_KEYS.filter(function (key) { return isObjectHasKey(data, key); });
      return (result.length === WS_KEYS.length) ? data : false
    }
    return false
  };

  /**
   * @param {string|object} data received data
   * @param {function} [cb=nil] this is for extracting the TS field or when it's error
   * @return {object} false on failed
   */
  var extractWsPayload = function (payload, cb) {
    if ( cb === void 0 ) cb = nil;

    try {
      var json = toJson(payload);
      // now handle the data
      var _data;
      if ((_data = isWsReply(json)) !== false) {
        // note the ts property is on its own 
        cb('_data', _data);
        
        return {
          data: toJson(_data[WS_DATA_NAME]),
          resolverName: _data[WS_EVT_NAME],
          type: _data[WS_REPLY_TYPE]
        }
      }
      throw new JsonqlError$1(PAYLOAD_NOT_DECODED_ERR, payload)
    } catch(e) {
      return cb(ERROR_KEY, e)
    }
  };

  // this will be part of the init client sequence
  var CSRF_HEADER_NOT_EXIST_ERR = 'CSRF header is not in the received payload';

  /**
   * Util method 
   * @param {string} payload return from server
   * @return {object} the useful bit 
   */
  function extractSrvPayload(payload) {
    var json = toJson(payload);
    
    if (json && typeof json === 'object') {
      // note this method expect the json.data inside
      return extractWsPayload(json)
    }
    
    throw new JsonqlError$1('extractSrvPayload', json)
  }

  /**
   * call the server to get a csrf token 
   * @return {string} formatted payload to send to the server 
   */
  function createInitPing() {
    var ts = timestamp();

    return createQueryStr(INTERCOM_RESOLVER_NAME, [SOCKET_PING_EVENT_NAME, ts])
  }

  /**
   * Take the raw on.message result back then decoded it 
   * @param {*} payload the raw result from server
   * @return {object} the csrf payload
   */
  function extractPingResult(payload) {
    var obj;

    var result = extractSrvPayload(payload);
    
    if (result && result[DATA_KEY] && result[DATA_KEY][CSRF_HEADER_KEY]) {
      return ( obj = {}, obj[HEADERS_KEY] = result[DATA_KEY], obj )
    }

    throw new JsonqlError$1('extractPingResult', CSRF_HEADER_NOT_EXIST_ERR)
  }


  /**
   * Create a generic intercom method
   * @param {string} type the event type 
   * @param {array} args if any 
   * @return {string} formatted payload to send
   */
  function createIntercomPayload(type) {
    var args = [], len = arguments.length - 1;
    while ( len-- > 0 ) args[ len ] = arguments[ len + 1 ];

    var ts = timestamp();
    var payload = [type].concat(args);
    payload.push(ts);
    return createQueryStr(INTERCOM_RESOLVER_NAME, payload)
  }

  /**
   * Check several parameter that there is something in the param
   * @param {*} param input
   * @return {boolean}
   */
   var isNotEmpty = function (a) {
    if (isArray(a)) {
      return true;
    }
    return a !== undefined && a !== null && trim(a) !== ''
  };

  /** `Object#toString` result references. */
  var numberTag$1 = '[object Number]';

  /**
   * Checks if `value` is classified as a `Number` primitive or object.
   *
   * **Note:** To exclude `Infinity`, `-Infinity`, and `NaN`, which are
   * classified as numbers, use the `_.isFinite` method.
   *
   * @static
   * @memberOf _
   * @since 0.1.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a number, else `false`.
   * @example
   *
   * _.isNumber(3);
   * // => true
   *
   * _.isNumber(Number.MIN_VALUE);
   * // => true
   *
   * _.isNumber(Infinity);
   * // => true
   *
   * _.isNumber('3');
   * // => false
   */
  function isNumber(value) {
    return typeof value == 'number' ||
      (isObjectLike(value) && baseGetTag(value) == numberTag$1);
  }

  /**
   * Checks if `value` is `NaN`.
   *
   * **Note:** This method is based on
   * [`Number.isNaN`](https://mdn.io/Number/isNaN) and is not the same as
   * global [`isNaN`](https://mdn.io/isNaN) which returns `true` for
   * `undefined` and other non-number values.
   *
   * @static
   * @memberOf _
   * @since 0.1.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is `NaN`, else `false`.
   * @example
   *
   * _.isNaN(NaN);
   * // => true
   *
   * _.isNaN(new Number(NaN));
   * // => true
   *
   * isNaN(undefined);
   * // => true
   *
   * _.isNaN(undefined);
   * // => false
   */
  function isNaN$1(value) {
    // An `NaN` primitive is the only value that is not equal to itself.
    // Perform the `toStringTag` check first to avoid errors with some
    // ActiveX objects in IE.
    return isNumber(value) && value != +value;
  }

  // validator numbers
  /**
   * @2015-05-04 found a problem if the value is a number like string
   * it will pass, so add a chck if it's string before we pass to next
   * @param {number} value expected value
   * @return {boolean} true if OK
   */
  var checkIsNumber = function(value) {
    return isString(value) ? false : !isNaN$1( parseFloat(value) )
  };

  // validate string type
  /**
   * @param {string} value expected value
   * @return {boolean} true if OK
   */
  var checkIsString = function(value) {
    return (trim(value) !== '') ? isString(value) : false
  };

  // check for boolean

  /**
   * @param {boolean} value expected
   * @return {boolean} true if OK
   */
  var checkIsBoolean = function(value) {
    return value !== null && value !== undefined && typeof value === 'boolean'
  };

  // validate any thing only check if there is something

  /**
   * @param {*} value the value
   * @param {boolean} [checkNull=true] strict check if there is null value
   * @return {boolean} true is OK
   */
  var checkIsAny = function(value, checkNull) {
    if ( checkNull === void 0 ) checkNull = true;

    if (value !== undefined && value !== '' && trim(value) !== '') {
      if (checkNull === false || (checkNull === true && value !== null)) {
        return true;
      }
    }
    return false;
  };

  // Good practice rule - No magic number

  var ARGS_NOT_ARRAY_ERR = "args is not an array! You might want to do: ES6 Array.from(arguments) or ES5 Array.prototype.slice.call(arguments)";
  var PARAMS_NOT_ARRAY_ERR = "params is not an array! Did something gone wrong when you generate the contract.json?";
  var EXCEPTION_CASE_ERR = 'Could not understand your arguments and parameter structure!';

  // primitive types

  /**
   * this is a wrapper method to call different one based on their type
   * @param {string} type to check
   * @return {function} a function to handle the type
   */
  var combineFn = function(type) {
    switch (type) {
      case NUMBER_TYPE:
        return checkIsNumber
      case STRING_TYPE:
        return checkIsString
      case BOOLEAN_TYPE:
        return checkIsBoolean
      default:
        return checkIsAny
    }
  };

  // validate array type

  /**
   * @param {array} value expected
   * @param {string} [type=''] pass the type if we encounter array.<T> then we need to check the value as well
   * @return {boolean} true if OK
   */
  var checkIsArray = function(value, type) {
    if ( type === void 0 ) type='';

    if (isArray(value)) {
      if (type === '' || trim(type)==='') {
        return true;
      }
      // we test it in reverse
      // @TODO if the type is an array (OR) then what?
      // we need to take into account this could be an array
      var c = value.filter(function (v) { return !combineFn(type)(v); });
      return !(c.length > 0)
    }
    return false
  };

  /**
   * check if it matches the array.<T> pattern
   * @param {string} type
   * @return {boolean|array} false means NO, always return array
   */
  var isArrayLike$1 = function(type) {
    // @TODO could that have something like array<> instead of array.<>? missing the dot?
    // because type script is Array<T> without the dot
    if (type.indexOf(ARRAY_TYPE_LFT) > -1 && type.indexOf(ARRAY_TYPE_RGT) > -1) {
      var _type = type.replace(ARRAY_TYPE_LFT, '').replace(ARRAY_TYPE_RGT, '');
      if (_type.indexOf(OR_SEPERATOR)) {
        return _type.split(OR_SEPERATOR)
      }
      return [_type]
    }
    return false
  };

  /**
   * we might encounter something like array.<T> then we need to take it apart
   * @param {object} p the prepared object for processing
   * @param {string|array} type the type came from <T>
   * @return {boolean} for the filter to operate on
   */
  var arrayTypeHandler = function(p, type) {
    var arg = p.arg;
    // need a special case to handle the OR type
    // we need to test the args instead of the type(s)
    if (type.length > 1) {
      return !arg.filter(function (v) { return (
        !(type.length > type.filter(function (t) { return !combineFn(t)(v); }).length)
      ); }).length
    }
    // type is array so this will be or!
    return type.length > type.filter(function (t) { return !checkIsArray(arg, t); }).length
  };

  // validate object type
  /**
   * @TODO if provide with the keys then we need to check if the key:value type as well
   * @param {object} value expected
   * @param {array} [keys=null] if it has the keys array to compare as well
   * @return {boolean} true if OK
   */
  var checkIsObject = function(value, keys) {
    if ( keys === void 0 ) keys=null;

    if (isPlainObject(value)) {
      if (!keys) {
        return true
      }
      if (checkIsArray(keys)) {
        // please note we DON'T care if some is optional
        // plese refer to the contract.json for the keys
        return !keys.filter(function (key) {
          var _value = value[key.name];
          return !(key.type.length > key.type.filter(function (type) {
            var tmp;
            if (_value !== undefined) {
              if ((tmp = isArrayLike$1(type)) !== false) {
                return !arrayTypeHandler({arg: _value}, tmp)
                // return tmp.filter(t => !checkIsArray(_value, t)).length;
                // @TODO there might be an object within an object with keys as well :S
              }
              return !combineFn(type)(_value)
            }
            return true
          }).length)
        }).length;
      }
    }
    return false
  };

  /**
   * fold this into it's own function to handler different object type
   * @param {object} p the prepared object for process
   * @return {boolean}
   */
  var objectTypeHandler = function(p) {
    var arg = p.arg;
    var param = p.param;
    var _args = [arg];
    if (Array.isArray(param.keys) && param.keys.length) {
      _args.push(param.keys);
    }
    // just simple check
    return Reflect.apply(checkIsObject, null, _args)
  };

  // move the index.js code here that make more sense to find where things are
  // import debug from 'debug'
  // const debugFn = debug('jsonql-params-validator:validator')
  // also export this for use in other places

  /**
   * We need to handle those optional parameter without a default value
   * @param {object} params from contract.json
   * @return {boolean} for filter operation false is actually OK
   */
  var optionalHandler = function( params ) {
    var arg = params.arg;
    var param = params.param;
    if (isNotEmpty(arg)) {
      // debug('call optional handler', arg, params);
      // loop through the type in param
      return !(param.type.length > param.type.filter(function (type) { return validateHandler(type, params); }
      ).length)
    }
    return false
  };

  /**
   * actually picking the validator
   * @param {*} type for checking
   * @param {*} value for checking
   * @return {boolean} true on OK
   */
  var validateHandler = function(type, value) {
    var tmp;
    switch (true) {
      case type === OBJECT_TYPE:
        // debugFn('call OBJECT_TYPE')
        return !objectTypeHandler(value)
      case type === ARRAY_TYPE:
        // debugFn('call ARRAY_TYPE')
        return !checkIsArray(value.arg)
      // @TODO when the type is not present, it always fall through here
      // so we need to find a way to actually pre-check the type first
      // AKA check the contract.json map before running here
      case (tmp = isArrayLike$1(type)) !== false:
        // debugFn('call ARRAY_LIKE: %O', value)
        return !arrayTypeHandler(value, tmp)
      default:
        return !combineFn(type)(value.arg)
    }
  };

  /**
   * it get too longer to fit in one line so break it out from the fn below
   * @param {*} arg value
   * @param {object} param config
   * @return {*} value or apply default value
   */
  var getOptionalValue = function(arg, param) {
    if (arg !== undefined) {
      return arg
    }
    return (param.optional === true && param.defaultvalue !== undefined ? param.defaultvalue : null)
  };

  /**
   * padding the arguments with defaultValue if the arguments did not provide the value
   * this will be the name export
   * @param {array} args normalized arguments
   * @param {array} params from contract.json
   * @return {array} merge the two together
   */
  var normalizeArgs = function(args, params) {
    // first we should check if this call require a validation at all
    // there will be situation where the function doesn't need args and params
    if (!checkIsArray(params)) {
      // debugFn('params value', params)
      throw new JsonqlValidationError(PARAMS_NOT_ARRAY_ERR)
    }
    if (params.length === 0) {
      return []
    }
    if (!checkIsArray(args)) {
      console.info(args);
      throw new JsonqlValidationError(ARGS_NOT_ARRAY_ERR)
    }
    // debugFn(args, params);
    // fall through switch
    switch(true) {
      case args.length == params.length: // standard
        return args.map(function (arg, i) { return (
          {
            arg: arg,
            index: i,
            param: params[i]
          }
        ); })
      case params[0].variable === true: // using spread syntax
        var type = params[0].type;
        return args.map(function (arg, i) { return (
          {
            arg: arg,
            index: i, // keep the index for reference
            param: params[i] || { type: type, name: '_' }
          }
        ); })
      // with optional defaultValue parameters
      case args.length < params.length:
        return params.map(function (param, i) { return (
          {
            param: param,
            index: i,
            arg: getOptionalValue(args[i], param),
            optional: param.optional || false
          }
        ); })
      // this one pass more than it should have anything after the args.length will be cast as any type
      case args.length > params.length:
        var ctn = params.length;
        // this happens when we have those array.<number> type
        var _type = [ DEFAULT_TYPE ];
        // we only looking at the first one, this might be a @BUG
        /*
        if ((tmp = isArrayLike(params[0].type[0])) !== false) {
          _type = tmp;
        } */
        // if we use the params as guide then the rest will get throw out
        // which is not what we want, instead, anything without the param
        // will get a any type and optional flag
        return args.map(function (arg, i) {
          var optional = i >= ctn ? true : !!params[i].optional;
          var param = params[i] || { type: _type, name: ("_" + i) };
          return {
            arg: optional ? getOptionalValue(arg, param) : arg,
            index: i,
            param: param,
            optional: optional
          }
        })
      // @TODO find out if there is more cases not cover
      default: // this should never happen
        // debugFn('args', args)
        // debugFn('params', params)
        // this is unknown therefore we just throw it!
        throw new JsonqlError$1(EXCEPTION_CASE_ERR, { args: args, params: params })
    }
  };

  // what we want is after the validaton we also get the normalized result
  // which is with the optional property if the argument didn't provide it
  /**
   * process the array of params back to their arguments
   * @param {array} result the params result
   * @return {array} arguments
   */
  var processReturn = function (result) { return result.map(function (r) { return r.arg; }); };

  /**
   * validator main interface
   * @param {array} args the arguments pass to the method call
   * @param {array} params from the contract for that method
   * @param {boolean} [withResul=false] if true then this will return the normalize result as well
   * @return {array} empty array on success, or failed parameter and reasons
   */
  var validateSync = function(args, params, withResult) {
    var obj;

    if ( withResult === void 0 ) withResult = false;
    var cleanArgs = normalizeArgs(args, params);
    var checkResult = cleanArgs.filter(function (p) {
      // v1.4.4 this fixed the problem, the root level optional is from the last fn
      if (p.optional === true || p.param.optional === true) {
        return optionalHandler(p)
      }
      // because array of types means OR so if one pass means pass
      return !(p.param.type.length > p.param.type.filter(
        function (type) { return validateHandler(type, p); }
      ).length)
    });
    // using the same convention we been using all this time
    return !withResult ? checkResult : ( obj = {}, obj[ERROR_KEY] = checkResult, obj[DATA_KEY] = processReturn(cleanArgs), obj )
  };

  /**
   * A wrapper method that return promise
   * @param {array} args arguments
   * @param {array} params from contract.json
   * @param {boolean} [withResul=false] if true then this will return the normalize result as well
   * @return {object} promise.then or catch
   */
  var validateAsync = function(args, params, withResult) {
    if ( withResult === void 0 ) withResult = false;

    return new Promise(function (resolver, rejecter) {
      var result = validateSync(args, params, withResult);
      if (withResult) {
        return result[ERROR_KEY].length ? rejecter(result[ERROR_KEY])
                                        : resolver(result[DATA_KEY])
      }
      // the different is just in the then or catch phrase
      return result.length ? rejecter(result) : resolver([])
    })
  };

  /* Built-in method references for those with the same name as other `lodash` methods. */
  var nativeKeys = overArg(Object.keys, Object);

  /** Used for built-in method references. */
  var objectProto$b = Object.prototype;

  /** Used to check objects for own properties. */
  var hasOwnProperty$9 = objectProto$b.hasOwnProperty;

  /**
   * The base implementation of `_.keys` which doesn't treat sparse arrays as dense.
   *
   * @private
   * @param {Object} object The object to query.
   * @returns {Array} Returns the array of property names.
   */
  function baseKeys(object) {
    if (!isPrototype(object)) {
      return nativeKeys(object);
    }
    var result = [];
    for (var key in Object(object)) {
      if (hasOwnProperty$9.call(object, key) && key != 'constructor') {
        result.push(key);
      }
    }
    return result;
  }

  /**
   * Creates an array of the own enumerable property names of `object`.
   *
   * **Note:** Non-object values are coerced to objects. See the
   * [ES spec](http://ecma-international.org/ecma-262/7.0/#sec-object.keys)
   * for more details.
   *
   * @static
   * @since 0.1.0
   * @memberOf _
   * @category Object
   * @param {Object} object The object to query.
   * @returns {Array} Returns the array of property names.
   * @example
   *
   * function Foo() {
   *   this.a = 1;
   *   this.b = 2;
   * }
   *
   * Foo.prototype.c = 3;
   *
   * _.keys(new Foo);
   * // => ['a', 'b'] (iteration order is not guaranteed)
   *
   * _.keys('hi');
   * // => ['0', '1']
   */
  function keys(object) {
    return isArrayLike(object) ? arrayLikeKeys(object) : baseKeys(object);
  }

  /**
   * The base implementation of `_.forOwn` without support for iteratee shorthands.
   *
   * @private
   * @param {Object} object The object to iterate over.
   * @param {Function} iteratee The function invoked per iteration.
   * @returns {Object} Returns `object`.
   */
  function baseForOwn(object, iteratee) {
    return object && baseFor(object, iteratee, keys);
  }

  /** Used to stand-in for `undefined` hash values. */
  var HASH_UNDEFINED$2 = '__lodash_hash_undefined__';

  /**
   * Adds `value` to the array cache.
   *
   * @private
   * @name add
   * @memberOf SetCache
   * @alias push
   * @param {*} value The value to cache.
   * @returns {Object} Returns the cache instance.
   */
  function setCacheAdd(value) {
    this.__data__.set(value, HASH_UNDEFINED$2);
    return this;
  }

  /**
   * Checks if `value` is in the array cache.
   *
   * @private
   * @name has
   * @memberOf SetCache
   * @param {*} value The value to search for.
   * @returns {number} Returns `true` if `value` is found, else `false`.
   */
  function setCacheHas(value) {
    return this.__data__.has(value);
  }

  /**
   *
   * Creates an array cache object to store unique values.
   *
   * @private
   * @constructor
   * @param {Array} [values] The values to cache.
   */
  function SetCache(values) {
    var index = -1,
        length = values == null ? 0 : values.length;

    this.__data__ = new MapCache;
    while (++index < length) {
      this.add(values[index]);
    }
  }

  // Add methods to `SetCache`.
  SetCache.prototype.add = SetCache.prototype.push = setCacheAdd;
  SetCache.prototype.has = setCacheHas;

  /**
   * A specialized version of `_.some` for arrays without support for iteratee
   * shorthands.
   *
   * @private
   * @param {Array} [array] The array to iterate over.
   * @param {Function} predicate The function invoked per iteration.
   * @returns {boolean} Returns `true` if any element passes the predicate check,
   *  else `false`.
   */
  function arraySome(array, predicate) {
    var index = -1,
        length = array == null ? 0 : array.length;

    while (++index < length) {
      if (predicate(array[index], index, array)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Checks if a `cache` value for `key` exists.
   *
   * @private
   * @param {Object} cache The cache to query.
   * @param {string} key The key of the entry to check.
   * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
   */
  function cacheHas(cache, key) {
    return cache.has(key);
  }

  /** Used to compose bitmasks for value comparisons. */
  var COMPARE_PARTIAL_FLAG = 1,
      COMPARE_UNORDERED_FLAG = 2;

  /**
   * A specialized version of `baseIsEqualDeep` for arrays with support for
   * partial deep comparisons.
   *
   * @private
   * @param {Array} array The array to compare.
   * @param {Array} other The other array to compare.
   * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
   * @param {Function} customizer The function to customize comparisons.
   * @param {Function} equalFunc The function to determine equivalents of values.
   * @param {Object} stack Tracks traversed `array` and `other` objects.
   * @returns {boolean} Returns `true` if the arrays are equivalent, else `false`.
   */
  function equalArrays(array, other, bitmask, customizer, equalFunc, stack) {
    var isPartial = bitmask & COMPARE_PARTIAL_FLAG,
        arrLength = array.length,
        othLength = other.length;

    if (arrLength != othLength && !(isPartial && othLength > arrLength)) {
      return false;
    }
    // Assume cyclic values are equal.
    var stacked = stack.get(array);
    if (stacked && stack.get(other)) {
      return stacked == other;
    }
    var index = -1,
        result = true,
        seen = (bitmask & COMPARE_UNORDERED_FLAG) ? new SetCache : undefined;

    stack.set(array, other);
    stack.set(other, array);

    // Ignore non-index properties.
    while (++index < arrLength) {
      var arrValue = array[index],
          othValue = other[index];

      if (customizer) {
        var compared = isPartial
          ? customizer(othValue, arrValue, index, other, array, stack)
          : customizer(arrValue, othValue, index, array, other, stack);
      }
      if (compared !== undefined) {
        if (compared) {
          continue;
        }
        result = false;
        break;
      }
      // Recursively compare arrays (susceptible to call stack limits).
      if (seen) {
        if (!arraySome(other, function(othValue, othIndex) {
              if (!cacheHas(seen, othIndex) &&
                  (arrValue === othValue || equalFunc(arrValue, othValue, bitmask, customizer, stack))) {
                return seen.push(othIndex);
              }
            })) {
          result = false;
          break;
        }
      } else if (!(
            arrValue === othValue ||
              equalFunc(arrValue, othValue, bitmask, customizer, stack)
          )) {
        result = false;
        break;
      }
    }
    stack['delete'](array);
    stack['delete'](other);
    return result;
  }

  /**
   * Converts `map` to its key-value pairs.
   *
   * @private
   * @param {Object} map The map to convert.
   * @returns {Array} Returns the key-value pairs.
   */
  function mapToArray(map) {
    var index = -1,
        result = Array(map.size);

    map.forEach(function(value, key) {
      result[++index] = [key, value];
    });
    return result;
  }

  /**
   * Converts `set` to an array of its values.
   *
   * @private
   * @param {Object} set The set to convert.
   * @returns {Array} Returns the values.
   */
  function setToArray(set) {
    var index = -1,
        result = Array(set.size);

    set.forEach(function(value) {
      result[++index] = value;
    });
    return result;
  }

  /** Used to compose bitmasks for value comparisons. */
  var COMPARE_PARTIAL_FLAG$1 = 1,
      COMPARE_UNORDERED_FLAG$1 = 2;

  /** `Object#toString` result references. */
  var boolTag$1 = '[object Boolean]',
      dateTag$1 = '[object Date]',
      errorTag$1 = '[object Error]',
      mapTag$1 = '[object Map]',
      numberTag$2 = '[object Number]',
      regexpTag$1 = '[object RegExp]',
      setTag$1 = '[object Set]',
      stringTag$2 = '[object String]',
      symbolTag$1 = '[object Symbol]';

  var arrayBufferTag$1 = '[object ArrayBuffer]',
      dataViewTag$1 = '[object DataView]';

  /** Used to convert symbols to primitives and strings. */
  var symbolProto$1 = Symbol ? Symbol.prototype : undefined,
      symbolValueOf = symbolProto$1 ? symbolProto$1.valueOf : undefined;

  /**
   * A specialized version of `baseIsEqualDeep` for comparing objects of
   * the same `toStringTag`.
   *
   * **Note:** This function only supports comparing values with tags of
   * `Boolean`, `Date`, `Error`, `Number`, `RegExp`, or `String`.
   *
   * @private
   * @param {Object} object The object to compare.
   * @param {Object} other The other object to compare.
   * @param {string} tag The `toStringTag` of the objects to compare.
   * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
   * @param {Function} customizer The function to customize comparisons.
   * @param {Function} equalFunc The function to determine equivalents of values.
   * @param {Object} stack Tracks traversed `object` and `other` objects.
   * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
   */
  function equalByTag(object, other, tag, bitmask, customizer, equalFunc, stack) {
    switch (tag) {
      case dataViewTag$1:
        if ((object.byteLength != other.byteLength) ||
            (object.byteOffset != other.byteOffset)) {
          return false;
        }
        object = object.buffer;
        other = other.buffer;

      case arrayBufferTag$1:
        if ((object.byteLength != other.byteLength) ||
            !equalFunc(new Uint8Array(object), new Uint8Array(other))) {
          return false;
        }
        return true;

      case boolTag$1:
      case dateTag$1:
      case numberTag$2:
        // Coerce booleans to `1` or `0` and dates to milliseconds.
        // Invalid dates are coerced to `NaN`.
        return eq(+object, +other);

      case errorTag$1:
        return object.name == other.name && object.message == other.message;

      case regexpTag$1:
      case stringTag$2:
        // Coerce regexes to strings and treat strings, primitives and objects,
        // as equal. See http://www.ecma-international.org/ecma-262/7.0/#sec-regexp.prototype.tostring
        // for more details.
        return object == (other + '');

      case mapTag$1:
        var convert = mapToArray;

      case setTag$1:
        var isPartial = bitmask & COMPARE_PARTIAL_FLAG$1;
        convert || (convert = setToArray);

        if (object.size != other.size && !isPartial) {
          return false;
        }
        // Assume cyclic values are equal.
        var stacked = stack.get(object);
        if (stacked) {
          return stacked == other;
        }
        bitmask |= COMPARE_UNORDERED_FLAG$1;

        // Recursively compare objects (susceptible to call stack limits).
        stack.set(object, other);
        var result = equalArrays(convert(object), convert(other), bitmask, customizer, equalFunc, stack);
        stack['delete'](object);
        return result;

      case symbolTag$1:
        if (symbolValueOf) {
          return symbolValueOf.call(object) == symbolValueOf.call(other);
        }
    }
    return false;
  }

  /**
   * Appends the elements of `values` to `array`.
   *
   * @private
   * @param {Array} array The array to modify.
   * @param {Array} values The values to append.
   * @returns {Array} Returns `array`.
   */
  function arrayPush(array, values) {
    var index = -1,
        length = values.length,
        offset = array.length;

    while (++index < length) {
      array[offset + index] = values[index];
    }
    return array;
  }

  /**
   * The base implementation of `getAllKeys` and `getAllKeysIn` which uses
   * `keysFunc` and `symbolsFunc` to get the enumerable property names and
   * symbols of `object`.
   *
   * @private
   * @param {Object} object The object to query.
   * @param {Function} keysFunc The function to get the keys of `object`.
   * @param {Function} symbolsFunc The function to get the symbols of `object`.
   * @returns {Array} Returns the array of property names and symbols.
   */
  function baseGetAllKeys(object, keysFunc, symbolsFunc) {
    var result = keysFunc(object);
    return isArray(object) ? result : arrayPush(result, symbolsFunc(object));
  }

  /**
   * A specialized version of `_.filter` for arrays without support for
   * iteratee shorthands.
   *
   * @private
   * @param {Array} [array] The array to iterate over.
   * @param {Function} predicate The function invoked per iteration.
   * @returns {Array} Returns the new filtered array.
   */
  function arrayFilter(array, predicate) {
    var index = -1,
        length = array == null ? 0 : array.length,
        resIndex = 0,
        result = [];

    while (++index < length) {
      var value = array[index];
      if (predicate(value, index, array)) {
        result[resIndex++] = value;
      }
    }
    return result;
  }

  /**
   * This method returns a new empty array.
   *
   * @static
   * @memberOf _
   * @since 4.13.0
   * @category Util
   * @returns {Array} Returns the new empty array.
   * @example
   *
   * var arrays = _.times(2, _.stubArray);
   *
   * console.log(arrays);
   * // => [[], []]
   *
   * console.log(arrays[0] === arrays[1]);
   * // => false
   */
  function stubArray() {
    return [];
  }

  /** Used for built-in method references. */
  var objectProto$c = Object.prototype;

  /** Built-in value references. */
  var propertyIsEnumerable$1 = objectProto$c.propertyIsEnumerable;

  /* Built-in method references for those with the same name as other `lodash` methods. */
  var nativeGetSymbols = Object.getOwnPropertySymbols;

  /**
   * Creates an array of the own enumerable symbols of `object`.
   *
   * @private
   * @param {Object} object The object to query.
   * @returns {Array} Returns the array of symbols.
   */
  var getSymbols = !nativeGetSymbols ? stubArray : function(object) {
    if (object == null) {
      return [];
    }
    object = Object(object);
    return arrayFilter(nativeGetSymbols(object), function(symbol) {
      return propertyIsEnumerable$1.call(object, symbol);
    });
  };

  /**
   * Creates an array of own enumerable property names and symbols of `object`.
   *
   * @private
   * @param {Object} object The object to query.
   * @returns {Array} Returns the array of property names and symbols.
   */
  function getAllKeys(object) {
    return baseGetAllKeys(object, keys, getSymbols);
  }

  /** Used to compose bitmasks for value comparisons. */
  var COMPARE_PARTIAL_FLAG$2 = 1;

  /** Used for built-in method references. */
  var objectProto$d = Object.prototype;

  /** Used to check objects for own properties. */
  var hasOwnProperty$a = objectProto$d.hasOwnProperty;

  /**
   * A specialized version of `baseIsEqualDeep` for objects with support for
   * partial deep comparisons.
   *
   * @private
   * @param {Object} object The object to compare.
   * @param {Object} other The other object to compare.
   * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
   * @param {Function} customizer The function to customize comparisons.
   * @param {Function} equalFunc The function to determine equivalents of values.
   * @param {Object} stack Tracks traversed `object` and `other` objects.
   * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
   */
  function equalObjects(object, other, bitmask, customizer, equalFunc, stack) {
    var isPartial = bitmask & COMPARE_PARTIAL_FLAG$2,
        objProps = getAllKeys(object),
        objLength = objProps.length,
        othProps = getAllKeys(other),
        othLength = othProps.length;

    if (objLength != othLength && !isPartial) {
      return false;
    }
    var index = objLength;
    while (index--) {
      var key = objProps[index];
      if (!(isPartial ? key in other : hasOwnProperty$a.call(other, key))) {
        return false;
      }
    }
    // Assume cyclic values are equal.
    var stacked = stack.get(object);
    if (stacked && stack.get(other)) {
      return stacked == other;
    }
    var result = true;
    stack.set(object, other);
    stack.set(other, object);

    var skipCtor = isPartial;
    while (++index < objLength) {
      key = objProps[index];
      var objValue = object[key],
          othValue = other[key];

      if (customizer) {
        var compared = isPartial
          ? customizer(othValue, objValue, key, other, object, stack)
          : customizer(objValue, othValue, key, object, other, stack);
      }
      // Recursively compare objects (susceptible to call stack limits).
      if (!(compared === undefined
            ? (objValue === othValue || equalFunc(objValue, othValue, bitmask, customizer, stack))
            : compared
          )) {
        result = false;
        break;
      }
      skipCtor || (skipCtor = key == 'constructor');
    }
    if (result && !skipCtor) {
      var objCtor = object.constructor,
          othCtor = other.constructor;

      // Non `Object` object instances with different constructors are not equal.
      if (objCtor != othCtor &&
          ('constructor' in object && 'constructor' in other) &&
          !(typeof objCtor == 'function' && objCtor instanceof objCtor &&
            typeof othCtor == 'function' && othCtor instanceof othCtor)) {
        result = false;
      }
    }
    stack['delete'](object);
    stack['delete'](other);
    return result;
  }

  /* Built-in method references that are verified to be native. */
  var DataView = getNative(root, 'DataView');

  /* Built-in method references that are verified to be native. */
  var Promise$1 = getNative(root, 'Promise');

  /* Built-in method references that are verified to be native. */
  var Set$1 = getNative(root, 'Set');

  /* Built-in method references that are verified to be native. */
  var WeakMap$1 = getNative(root, 'WeakMap');

  /** `Object#toString` result references. */
  var mapTag$2 = '[object Map]',
      objectTag$2 = '[object Object]',
      promiseTag = '[object Promise]',
      setTag$2 = '[object Set]',
      weakMapTag$1 = '[object WeakMap]';

  var dataViewTag$2 = '[object DataView]';

  /** Used to detect maps, sets, and weakmaps. */
  var dataViewCtorString = toSource(DataView),
      mapCtorString = toSource(Map$1),
      promiseCtorString = toSource(Promise$1),
      setCtorString = toSource(Set$1),
      weakMapCtorString = toSource(WeakMap$1);

  /**
   * Gets the `toStringTag` of `value`.
   *
   * @private
   * @param {*} value The value to query.
   * @returns {string} Returns the `toStringTag`.
   */
  var getTag = baseGetTag;

  // Fallback for data views, maps, sets, and weak maps in IE 11 and promises in Node.js < 6.
  if ((DataView && getTag(new DataView(new ArrayBuffer(1))) != dataViewTag$2) ||
      (Map$1 && getTag(new Map$1) != mapTag$2) ||
      (Promise$1 && getTag(Promise$1.resolve()) != promiseTag) ||
      (Set$1 && getTag(new Set$1) != setTag$2) ||
      (WeakMap$1 && getTag(new WeakMap$1) != weakMapTag$1)) {
    getTag = function(value) {
      var result = baseGetTag(value),
          Ctor = result == objectTag$2 ? value.constructor : undefined,
          ctorString = Ctor ? toSource(Ctor) : '';

      if (ctorString) {
        switch (ctorString) {
          case dataViewCtorString: return dataViewTag$2;
          case mapCtorString: return mapTag$2;
          case promiseCtorString: return promiseTag;
          case setCtorString: return setTag$2;
          case weakMapCtorString: return weakMapTag$1;
        }
      }
      return result;
    };
  }

  var getTag$1 = getTag;

  /** Used to compose bitmasks for value comparisons. */
  var COMPARE_PARTIAL_FLAG$3 = 1;

  /** `Object#toString` result references. */
  var argsTag$2 = '[object Arguments]',
      arrayTag$1 = '[object Array]',
      objectTag$3 = '[object Object]';

  /** Used for built-in method references. */
  var objectProto$e = Object.prototype;

  /** Used to check objects for own properties. */
  var hasOwnProperty$b = objectProto$e.hasOwnProperty;

  /**
   * A specialized version of `baseIsEqual` for arrays and objects which performs
   * deep comparisons and tracks traversed objects enabling objects with circular
   * references to be compared.
   *
   * @private
   * @param {Object} object The object to compare.
   * @param {Object} other The other object to compare.
   * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
   * @param {Function} customizer The function to customize comparisons.
   * @param {Function} equalFunc The function to determine equivalents of values.
   * @param {Object} [stack] Tracks traversed `object` and `other` objects.
   * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
   */
  function baseIsEqualDeep(object, other, bitmask, customizer, equalFunc, stack) {
    var objIsArr = isArray(object),
        othIsArr = isArray(other),
        objTag = objIsArr ? arrayTag$1 : getTag$1(object),
        othTag = othIsArr ? arrayTag$1 : getTag$1(other);

    objTag = objTag == argsTag$2 ? objectTag$3 : objTag;
    othTag = othTag == argsTag$2 ? objectTag$3 : othTag;

    var objIsObj = objTag == objectTag$3,
        othIsObj = othTag == objectTag$3,
        isSameTag = objTag == othTag;

    if (isSameTag && isBuffer(object)) {
      if (!isBuffer(other)) {
        return false;
      }
      objIsArr = true;
      objIsObj = false;
    }
    if (isSameTag && !objIsObj) {
      stack || (stack = new Stack);
      return (objIsArr || isTypedArray(object))
        ? equalArrays(object, other, bitmask, customizer, equalFunc, stack)
        : equalByTag(object, other, objTag, bitmask, customizer, equalFunc, stack);
    }
    if (!(bitmask & COMPARE_PARTIAL_FLAG$3)) {
      var objIsWrapped = objIsObj && hasOwnProperty$b.call(object, '__wrapped__'),
          othIsWrapped = othIsObj && hasOwnProperty$b.call(other, '__wrapped__');

      if (objIsWrapped || othIsWrapped) {
        var objUnwrapped = objIsWrapped ? object.value() : object,
            othUnwrapped = othIsWrapped ? other.value() : other;

        stack || (stack = new Stack);
        return equalFunc(objUnwrapped, othUnwrapped, bitmask, customizer, stack);
      }
    }
    if (!isSameTag) {
      return false;
    }
    stack || (stack = new Stack);
    return equalObjects(object, other, bitmask, customizer, equalFunc, stack);
  }

  /**
   * The base implementation of `_.isEqual` which supports partial comparisons
   * and tracks traversed objects.
   *
   * @private
   * @param {*} value The value to compare.
   * @param {*} other The other value to compare.
   * @param {boolean} bitmask The bitmask flags.
   *  1 - Unordered comparison
   *  2 - Partial comparison
   * @param {Function} [customizer] The function to customize comparisons.
   * @param {Object} [stack] Tracks traversed `value` and `other` objects.
   * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
   */
  function baseIsEqual(value, other, bitmask, customizer, stack) {
    if (value === other) {
      return true;
    }
    if (value == null || other == null || (!isObjectLike(value) && !isObjectLike(other))) {
      return value !== value && other !== other;
    }
    return baseIsEqualDeep(value, other, bitmask, customizer, baseIsEqual, stack);
  }

  /** Used to compose bitmasks for value comparisons. */
  var COMPARE_PARTIAL_FLAG$4 = 1,
      COMPARE_UNORDERED_FLAG$2 = 2;

  /**
   * The base implementation of `_.isMatch` without support for iteratee shorthands.
   *
   * @private
   * @param {Object} object The object to inspect.
   * @param {Object} source The object of property values to match.
   * @param {Array} matchData The property names, values, and compare flags to match.
   * @param {Function} [customizer] The function to customize comparisons.
   * @returns {boolean} Returns `true` if `object` is a match, else `false`.
   */
  function baseIsMatch(object, source, matchData, customizer) {
    var index = matchData.length,
        length = index,
        noCustomizer = !customizer;

    if (object == null) {
      return !length;
    }
    object = Object(object);
    while (index--) {
      var data = matchData[index];
      if ((noCustomizer && data[2])
            ? data[1] !== object[data[0]]
            : !(data[0] in object)
          ) {
        return false;
      }
    }
    while (++index < length) {
      data = matchData[index];
      var key = data[0],
          objValue = object[key],
          srcValue = data[1];

      if (noCustomizer && data[2]) {
        if (objValue === undefined && !(key in object)) {
          return false;
        }
      } else {
        var stack = new Stack;
        if (customizer) {
          var result = customizer(objValue, srcValue, key, object, source, stack);
        }
        if (!(result === undefined
              ? baseIsEqual(srcValue, objValue, COMPARE_PARTIAL_FLAG$4 | COMPARE_UNORDERED_FLAG$2, customizer, stack)
              : result
            )) {
          return false;
        }
      }
    }
    return true;
  }

  /**
   * Checks if `value` is suitable for strict equality comparisons, i.e. `===`.
   *
   * @private
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` if suitable for strict
   *  equality comparisons, else `false`.
   */
  function isStrictComparable(value) {
    return value === value && !isObject(value);
  }

  /**
   * Gets the property names, values, and compare flags of `object`.
   *
   * @private
   * @param {Object} object The object to query.
   * @returns {Array} Returns the match data of `object`.
   */
  function getMatchData(object) {
    var result = keys(object),
        length = result.length;

    while (length--) {
      var key = result[length],
          value = object[key];

      result[length] = [key, value, isStrictComparable(value)];
    }
    return result;
  }

  /**
   * A specialized version of `matchesProperty` for source values suitable
   * for strict equality comparisons, i.e. `===`.
   *
   * @private
   * @param {string} key The key of the property to get.
   * @param {*} srcValue The value to match.
   * @returns {Function} Returns the new spec function.
   */
  function matchesStrictComparable(key, srcValue) {
    return function(object) {
      if (object == null) {
        return false;
      }
      return object[key] === srcValue &&
        (srcValue !== undefined || (key in Object(object)));
    };
  }

  /**
   * The base implementation of `_.matches` which doesn't clone `source`.
   *
   * @private
   * @param {Object} source The object of property values to match.
   * @returns {Function} Returns the new spec function.
   */
  function baseMatches(source) {
    var matchData = getMatchData(source);
    if (matchData.length == 1 && matchData[0][2]) {
      return matchesStrictComparable(matchData[0][0], matchData[0][1]);
    }
    return function(object) {
      return object === source || baseIsMatch(object, source, matchData);
    };
  }

  /** Used to match property names within property paths. */
  var reIsDeepProp = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/,
      reIsPlainProp = /^\w*$/;

  /**
   * Checks if `value` is a property name and not a property path.
   *
   * @private
   * @param {*} value The value to check.
   * @param {Object} [object] The object to query keys on.
   * @returns {boolean} Returns `true` if `value` is a property name, else `false`.
   */
  function isKey(value, object) {
    if (isArray(value)) {
      return false;
    }
    var type = typeof value;
    if (type == 'number' || type == 'symbol' || type == 'boolean' ||
        value == null || isSymbol(value)) {
      return true;
    }
    return reIsPlainProp.test(value) || !reIsDeepProp.test(value) ||
      (object != null && value in Object(object));
  }

  /** Error message constants. */
  var FUNC_ERROR_TEXT = 'Expected a function';

  /**
   * Creates a function that memoizes the result of `func`. If `resolver` is
   * provided, it determines the cache key for storing the result based on the
   * arguments provided to the memoized function. By default, the first argument
   * provided to the memoized function is used as the map cache key. The `func`
   * is invoked with the `this` binding of the memoized function.
   *
   * **Note:** The cache is exposed as the `cache` property on the memoized
   * function. Its creation may be customized by replacing the `_.memoize.Cache`
   * constructor with one whose instances implement the
   * [`Map`](http://ecma-international.org/ecma-262/7.0/#sec-properties-of-the-map-prototype-object)
   * method interface of `clear`, `delete`, `get`, `has`, and `set`.
   *
   * @static
   * @memberOf _
   * @since 0.1.0
   * @category Function
   * @param {Function} func The function to have its output memoized.
   * @param {Function} [resolver] The function to resolve the cache key.
   * @returns {Function} Returns the new memoized function.
   * @example
   *
   * var object = { 'a': 1, 'b': 2 };
   * var other = { 'c': 3, 'd': 4 };
   *
   * var values = _.memoize(_.values);
   * values(object);
   * // => [1, 2]
   *
   * values(other);
   * // => [3, 4]
   *
   * object.a = 2;
   * values(object);
   * // => [1, 2]
   *
   * // Modify the result cache.
   * values.cache.set(object, ['a', 'b']);
   * values(object);
   * // => ['a', 'b']
   *
   * // Replace `_.memoize.Cache`.
   * _.memoize.Cache = WeakMap;
   */
  function memoize(func, resolver) {
    if (typeof func != 'function' || (resolver != null && typeof resolver != 'function')) {
      throw new TypeError(FUNC_ERROR_TEXT);
    }
    var memoized = function() {
      var args = arguments,
          key = resolver ? resolver.apply(this, args) : args[0],
          cache = memoized.cache;

      if (cache.has(key)) {
        return cache.get(key);
      }
      var result = func.apply(this, args);
      memoized.cache = cache.set(key, result) || cache;
      return result;
    };
    memoized.cache = new (memoize.Cache || MapCache);
    return memoized;
  }

  // Expose `MapCache`.
  memoize.Cache = MapCache;

  /** Used as the maximum memoize cache size. */
  var MAX_MEMOIZE_SIZE = 500;

  /**
   * A specialized version of `_.memoize` which clears the memoized function's
   * cache when it exceeds `MAX_MEMOIZE_SIZE`.
   *
   * @private
   * @param {Function} func The function to have its output memoized.
   * @returns {Function} Returns the new memoized function.
   */
  function memoizeCapped(func) {
    var result = memoize(func, function(key) {
      if (cache.size === MAX_MEMOIZE_SIZE) {
        cache.clear();
      }
      return key;
    });

    var cache = result.cache;
    return result;
  }

  /** Used to match property names within property paths. */
  var rePropName = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|$))/g;

  /** Used to match backslashes in property paths. */
  var reEscapeChar = /\\(\\)?/g;

  /**
   * Converts `string` to a property path array.
   *
   * @private
   * @param {string} string The string to convert.
   * @returns {Array} Returns the property path array.
   */
  var stringToPath = memoizeCapped(function(string) {
    var result = [];
    if (string.charCodeAt(0) === 46 /* . */) {
      result.push('');
    }
    string.replace(rePropName, function(match, number, quote, subString) {
      result.push(quote ? subString.replace(reEscapeChar, '$1') : (number || match));
    });
    return result;
  });

  /**
   * Casts `value` to a path array if it's not one.
   *
   * @private
   * @param {*} value The value to inspect.
   * @param {Object} [object] The object to query keys on.
   * @returns {Array} Returns the cast property path array.
   */
  function castPath(value, object) {
    if (isArray(value)) {
      return value;
    }
    return isKey(value, object) ? [value] : stringToPath(toString(value));
  }

  /** Used as references for various `Number` constants. */
  var INFINITY$1 = 1 / 0;

  /**
   * Converts `value` to a string key if it's not a string or symbol.
   *
   * @private
   * @param {*} value The value to inspect.
   * @returns {string|symbol} Returns the key.
   */
  function toKey(value) {
    if (typeof value == 'string' || isSymbol(value)) {
      return value;
    }
    var result = (value + '');
    return (result == '0' && (1 / value) == -INFINITY$1) ? '-0' : result;
  }

  /**
   * The base implementation of `_.get` without support for default values.
   *
   * @private
   * @param {Object} object The object to query.
   * @param {Array|string} path The path of the property to get.
   * @returns {*} Returns the resolved value.
   */
  function baseGet(object, path) {
    path = castPath(path, object);

    var index = 0,
        length = path.length;

    while (object != null && index < length) {
      object = object[toKey(path[index++])];
    }
    return (index && index == length) ? object : undefined;
  }

  /**
   * Gets the value at `path` of `object`. If the resolved value is
   * `undefined`, the `defaultValue` is returned in its place.
   *
   * @static
   * @memberOf _
   * @since 3.7.0
   * @category Object
   * @param {Object} object The object to query.
   * @param {Array|string} path The path of the property to get.
   * @param {*} [defaultValue] The value returned for `undefined` resolved values.
   * @returns {*} Returns the resolved value.
   * @example
   *
   * var object = { 'a': [{ 'b': { 'c': 3 } }] };
   *
   * _.get(object, 'a[0].b.c');
   * // => 3
   *
   * _.get(object, ['a', '0', 'b', 'c']);
   * // => 3
   *
   * _.get(object, 'a.b.c', 'default');
   * // => 'default'
   */
  function get(object, path, defaultValue) {
    var result = object == null ? undefined : baseGet(object, path);
    return result === undefined ? defaultValue : result;
  }

  /**
   * The base implementation of `_.hasIn` without support for deep paths.
   *
   * @private
   * @param {Object} [object] The object to query.
   * @param {Array|string} key The key to check.
   * @returns {boolean} Returns `true` if `key` exists, else `false`.
   */
  function baseHasIn(object, key) {
    return object != null && key in Object(object);
  }

  /**
   * Checks if `path` exists on `object`.
   *
   * @private
   * @param {Object} object The object to query.
   * @param {Array|string} path The path to check.
   * @param {Function} hasFunc The function to check properties.
   * @returns {boolean} Returns `true` if `path` exists, else `false`.
   */
  function hasPath(object, path, hasFunc) {
    path = castPath(path, object);

    var index = -1,
        length = path.length,
        result = false;

    while (++index < length) {
      var key = toKey(path[index]);
      if (!(result = object != null && hasFunc(object, key))) {
        break;
      }
      object = object[key];
    }
    if (result || ++index != length) {
      return result;
    }
    length = object == null ? 0 : object.length;
    return !!length && isLength(length) && isIndex(key, length) &&
      (isArray(object) || isArguments(object));
  }

  /**
   * Checks if `path` is a direct or inherited property of `object`.
   *
   * @static
   * @memberOf _
   * @since 4.0.0
   * @category Object
   * @param {Object} object The object to query.
   * @param {Array|string} path The path to check.
   * @returns {boolean} Returns `true` if `path` exists, else `false`.
   * @example
   *
   * var object = _.create({ 'a': _.create({ 'b': 2 }) });
   *
   * _.hasIn(object, 'a');
   * // => true
   *
   * _.hasIn(object, 'a.b');
   * // => true
   *
   * _.hasIn(object, ['a', 'b']);
   * // => true
   *
   * _.hasIn(object, 'b');
   * // => false
   */
  function hasIn(object, path) {
    return object != null && hasPath(object, path, baseHasIn);
  }

  /** Used to compose bitmasks for value comparisons. */
  var COMPARE_PARTIAL_FLAG$5 = 1,
      COMPARE_UNORDERED_FLAG$3 = 2;

  /**
   * The base implementation of `_.matchesProperty` which doesn't clone `srcValue`.
   *
   * @private
   * @param {string} path The path of the property to get.
   * @param {*} srcValue The value to match.
   * @returns {Function} Returns the new spec function.
   */
  function baseMatchesProperty(path, srcValue) {
    if (isKey(path) && isStrictComparable(srcValue)) {
      return matchesStrictComparable(toKey(path), srcValue);
    }
    return function(object) {
      var objValue = get(object, path);
      return (objValue === undefined && objValue === srcValue)
        ? hasIn(object, path)
        : baseIsEqual(srcValue, objValue, COMPARE_PARTIAL_FLAG$5 | COMPARE_UNORDERED_FLAG$3);
    };
  }

  /**
   * The base implementation of `_.property` without support for deep paths.
   *
   * @private
   * @param {string} key The key of the property to get.
   * @returns {Function} Returns the new accessor function.
   */
  function baseProperty(key) {
    return function(object) {
      return object == null ? undefined : object[key];
    };
  }

  /**
   * A specialized version of `baseProperty` which supports deep paths.
   *
   * @private
   * @param {Array|string} path The path of the property to get.
   * @returns {Function} Returns the new accessor function.
   */
  function basePropertyDeep(path) {
    return function(object) {
      return baseGet(object, path);
    };
  }

  /**
   * Creates a function that returns the value at `path` of a given object.
   *
   * @static
   * @memberOf _
   * @since 2.4.0
   * @category Util
   * @param {Array|string} path The path of the property to get.
   * @returns {Function} Returns the new accessor function.
   * @example
   *
   * var objects = [
   *   { 'a': { 'b': 2 } },
   *   { 'a': { 'b': 1 } }
   * ];
   *
   * _.map(objects, _.property('a.b'));
   * // => [2, 1]
   *
   * _.map(_.sortBy(objects, _.property(['a', 'b'])), 'a.b');
   * // => [1, 2]
   */
  function property(path) {
    return isKey(path) ? baseProperty(toKey(path)) : basePropertyDeep(path);
  }

  /**
   * The base implementation of `_.iteratee`.
   *
   * @private
   * @param {*} [value=_.identity] The value to convert to an iteratee.
   * @returns {Function} Returns the iteratee.
   */
  function baseIteratee(value) {
    // Don't store the `typeof` result in a variable to avoid a JIT bug in Safari 9.
    // See https://bugs.webkit.org/show_bug.cgi?id=156034 for more details.
    if (typeof value == 'function') {
      return value;
    }
    if (value == null) {
      return identity;
    }
    if (typeof value == 'object') {
      return isArray(value)
        ? baseMatchesProperty(value[0], value[1])
        : baseMatches(value);
    }
    return property(value);
  }

  /**
   * Creates an object with the same keys as `object` and values generated
   * by running each own enumerable string keyed property of `object` thru
   * `iteratee`. The iteratee is invoked with three arguments:
   * (value, key, object).
   *
   * @static
   * @memberOf _
   * @since 2.4.0
   * @category Object
   * @param {Object} object The object to iterate over.
   * @param {Function} [iteratee=_.identity] The function invoked per iteration.
   * @returns {Object} Returns the new mapped object.
   * @see _.mapKeys
   * @example
   *
   * var users = {
   *   'fred':    { 'user': 'fred',    'age': 40 },
   *   'pebbles': { 'user': 'pebbles', 'age': 1 }
   * };
   *
   * _.mapValues(users, function(o) { return o.age; });
   * // => { 'fred': 40, 'pebbles': 1 } (iteration order is not guaranteed)
   *
   * // The `_.property` iteratee shorthand.
   * _.mapValues(users, 'age');
   * // => { 'fred': 40, 'pebbles': 1 } (iteration order is not guaranteed)
   */
  function mapValues(object, iteratee) {
    var result = {};
    iteratee = baseIteratee(iteratee);

    baseForOwn(object, function(value, key, object) {
      baseAssignValue(result, key, iteratee(value, key, object));
    });
    return result;
  }

  /**
   * The opposite of `_.mapValues`; this method creates an object with the
   * same values as `object` and keys generated by running each own enumerable
   * string keyed property of `object` thru `iteratee`. The iteratee is invoked
   * with three arguments: (value, key, object).
   *
   * @static
   * @memberOf _
   * @since 3.8.0
   * @category Object
   * @param {Object} object The object to iterate over.
   * @param {Function} [iteratee=_.identity] The function invoked per iteration.
   * @returns {Object} Returns the new mapped object.
   * @see _.mapValues
   * @example
   *
   * _.mapKeys({ 'a': 1, 'b': 2 }, function(value, key) {
   *   return key + value;
   * });
   * // => { 'a1': 1, 'b2': 2 }
   */
  function mapKeys(object, iteratee) {
    var result = {};
    iteratee = baseIteratee(iteratee);

    baseForOwn(object, function(value, key, object) {
      baseAssignValue(result, iteratee(value, key, object), value);
    });
    return result;
  }

  /** Error message constants. */
  var FUNC_ERROR_TEXT$1 = 'Expected a function';

  /**
   * Creates a function that negates the result of the predicate `func`. The
   * `func` predicate is invoked with the `this` binding and arguments of the
   * created function.
   *
   * @static
   * @memberOf _
   * @since 3.0.0
   * @category Function
   * @param {Function} predicate The predicate to negate.
   * @returns {Function} Returns the new negated function.
   * @example
   *
   * function isEven(n) {
   *   return n % 2 == 0;
   * }
   *
   * _.filter([1, 2, 3, 4, 5, 6], _.negate(isEven));
   * // => [1, 3, 5]
   */
  function negate(predicate) {
    if (typeof predicate != 'function') {
      throw new TypeError(FUNC_ERROR_TEXT$1);
    }
    return function() {
      var args = arguments;
      switch (args.length) {
        case 0: return !predicate.call(this);
        case 1: return !predicate.call(this, args[0]);
        case 2: return !predicate.call(this, args[0], args[1]);
        case 3: return !predicate.call(this, args[0], args[1], args[2]);
      }
      return !predicate.apply(this, args);
    };
  }

  /**
   * The base implementation of `_.set`.
   *
   * @private
   * @param {Object} object The object to modify.
   * @param {Array|string} path The path of the property to set.
   * @param {*} value The value to set.
   * @param {Function} [customizer] The function to customize path creation.
   * @returns {Object} Returns `object`.
   */
  function baseSet(object, path, value, customizer) {
    if (!isObject(object)) {
      return object;
    }
    path = castPath(path, object);

    var index = -1,
        length = path.length,
        lastIndex = length - 1,
        nested = object;

    while (nested != null && ++index < length) {
      var key = toKey(path[index]),
          newValue = value;

      if (index != lastIndex) {
        var objValue = nested[key];
        newValue = customizer ? customizer(objValue, key, nested) : undefined;
        if (newValue === undefined) {
          newValue = isObject(objValue)
            ? objValue
            : (isIndex(path[index + 1]) ? [] : {});
        }
      }
      assignValue(nested, key, newValue);
      nested = nested[key];
    }
    return object;
  }

  /**
   * The base implementation of  `_.pickBy` without support for iteratee shorthands.
   *
   * @private
   * @param {Object} object The source object.
   * @param {string[]} paths The property paths to pick.
   * @param {Function} predicate The function invoked per property.
   * @returns {Object} Returns the new object.
   */
  function basePickBy(object, paths, predicate) {
    var index = -1,
        length = paths.length,
        result = {};

    while (++index < length) {
      var path = paths[index],
          value = baseGet(object, path);

      if (predicate(value, path)) {
        baseSet(result, castPath(path, object), value);
      }
    }
    return result;
  }

  /* Built-in method references for those with the same name as other `lodash` methods. */
  var nativeGetSymbols$1 = Object.getOwnPropertySymbols;

  /**
   * Creates an array of the own and inherited enumerable symbols of `object`.
   *
   * @private
   * @param {Object} object The object to query.
   * @returns {Array} Returns the array of symbols.
   */
  var getSymbolsIn = !nativeGetSymbols$1 ? stubArray : function(object) {
    var result = [];
    while (object) {
      arrayPush(result, getSymbols(object));
      object = getPrototype(object);
    }
    return result;
  };

  /**
   * Creates an array of own and inherited enumerable property names and
   * symbols of `object`.
   *
   * @private
   * @param {Object} object The object to query.
   * @returns {Array} Returns the array of property names and symbols.
   */
  function getAllKeysIn(object) {
    return baseGetAllKeys(object, keysIn, getSymbolsIn);
  }

  /**
   * Creates an object composed of the `object` properties `predicate` returns
   * truthy for. The predicate is invoked with two arguments: (value, key).
   *
   * @static
   * @memberOf _
   * @since 4.0.0
   * @category Object
   * @param {Object} object The source object.
   * @param {Function} [predicate=_.identity] The function invoked per property.
   * @returns {Object} Returns the new object.
   * @example
   *
   * var object = { 'a': 1, 'b': '2', 'c': 3 };
   *
   * _.pickBy(object, _.isNumber);
   * // => { 'a': 1, 'c': 3 }
   */
  function pickBy(object, predicate) {
    if (object == null) {
      return {};
    }
    var props = arrayMap(getAllKeysIn(object), function(prop) {
      return [prop];
    });
    predicate = baseIteratee(predicate);
    return basePickBy(object, props, function(value, path) {
      return predicate(value, path[0]);
    });
  }

  /**
   * The opposite of `_.pickBy`; this method creates an object composed of
   * the own and inherited enumerable string keyed properties of `object` that
   * `predicate` doesn't return truthy for. The predicate is invoked with two
   * arguments: (value, key).
   *
   * @static
   * @memberOf _
   * @since 4.0.0
   * @category Object
   * @param {Object} object The source object.
   * @param {Function} [predicate=_.identity] The function invoked per property.
   * @returns {Object} Returns the new object.
   * @example
   *
   * var object = { 'a': 1, 'b': '2', 'c': 3 };
   *
   * _.omitBy(object, _.isNumber);
   * // => { 'b': '2' }
   */
  function omitBy(object, predicate) {
    return pickBy(object, negate(baseIteratee(predicate)));
  }

  /**
   * Performs a deep comparison between two values to determine if they are
   * equivalent.
   *
   * **Note:** This method supports comparing arrays, array buffers, booleans,
   * date objects, error objects, maps, numbers, `Object` objects, regexes,
   * sets, strings, symbols, and typed arrays. `Object` objects are compared
   * by their own, not inherited, enumerable properties. Functions and DOM
   * nodes are compared by strict equality, i.e. `===`.
   *
   * @static
   * @memberOf _
   * @since 0.1.0
   * @category Lang
   * @param {*} value The value to compare.
   * @param {*} other The other value to compare.
   * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
   * @example
   *
   * var object = { 'a': 1 };
   * var other = { 'a': 1 };
   *
   * _.isEqual(object, other);
   * // => true
   *
   * object === other;
   * // => false
   */
  function isEqual(value, other) {
    return baseIsEqual(value, other);
  }

  /**
   * The base implementation of methods like `_.findKey` and `_.findLastKey`,
   * without support for iteratee shorthands, which iterates over `collection`
   * using `eachFunc`.
   *
   * @private
   * @param {Array|Object} collection The collection to inspect.
   * @param {Function} predicate The function invoked per iteration.
   * @param {Function} eachFunc The function to iterate over `collection`.
   * @returns {*} Returns the found element or its key, else `undefined`.
   */
  function baseFindKey(collection, predicate, eachFunc) {
    var result;
    eachFunc(collection, function(value, key, collection) {
      if (predicate(value, key, collection)) {
        result = key;
        return false;
      }
    });
    return result;
  }

  /**
   * This method is like `_.find` except that it returns the key of the first
   * element `predicate` returns truthy for instead of the element itself.
   *
   * @static
   * @memberOf _
   * @since 1.1.0
   * @category Object
   * @param {Object} object The object to inspect.
   * @param {Function} [predicate=_.identity] The function invoked per iteration.
   * @returns {string|undefined} Returns the key of the matched element,
   *  else `undefined`.
   * @example
   *
   * var users = {
   *   'barney':  { 'age': 36, 'active': true },
   *   'fred':    { 'age': 40, 'active': false },
   *   'pebbles': { 'age': 1,  'active': true }
   * };
   *
   * _.findKey(users, function(o) { return o.age < 40; });
   * // => 'barney' (iteration order is not guaranteed)
   *
   * // The `_.matches` iteratee shorthand.
   * _.findKey(users, { 'age': 1, 'active': true });
   * // => 'pebbles'
   *
   * // The `_.matchesProperty` iteratee shorthand.
   * _.findKey(users, ['active', false]);
   * // => 'fred'
   *
   * // The `_.property` iteratee shorthand.
   * _.findKey(users, 'active');
   * // => 'barney'
   */
  function findKey(object, predicate) {
    return baseFindKey(object, baseIteratee(predicate), baseForOwn);
  }

  /**
   * @param {array} arr Array for check
   * @param {*} value target
   * @return {boolean} true on successs
   */
  var isInArray = function(arr, value) {
    return !!arr.filter(function (a) { return a === value; }).length
  };

  var isObjectHasKey$1 = function(obj, key) {
    var keys = Object.keys(obj);
    return isInArray(keys, key)
  };

  // just not to make my head hurt
  var isEmpty = function (value) { return !isNotEmpty(value); };

  /**
   * Map the alias to their key then grab their value over
   * @param {object} config the user supplied config
   * @param {object} appProps the default option map
   * @return {object} the config keys replaced with the appProps key by the ALIAS
   */
  function mapAliasConfigKeys(config, appProps) {
    // need to do two steps
    // 1. take key with alias key
    var aliasMap = omitBy(appProps, function (value, k) { return !value[ALIAS_KEY]; } );
    if (isEqual(aliasMap, {})) {
      return config;
    }
    return mapKeys(config, function (v, key) { return findKey(aliasMap, function (o) { return o.alias === key; }) || key; })
  }

  /**
   * We only want to run the valdiation against the config (user supplied) value
   * but keep the defaultOptions untouch
   * @param {object} config configuraton supplied by user
   * @param {object} appProps the default options map
   * @return {object} the pristine values that will add back to the final output
   */
  function preservePristineValues(config, appProps) {
    // @BUG this will filter out those that is alias key
    // we need to first map the alias keys back to their full key
    var _config = mapAliasConfigKeys(config, appProps);
    // take the default value out
    var pristineValues = mapValues(
      omitBy(appProps, function (value, key) { return isObjectHasKey$1(_config, key); }),
      function (value) { return value.args; }
    );
    // for testing the value
    var checkAgainstAppProps = omitBy(appProps, function (value, key) { return !isObjectHasKey$1(_config, key); });
    // output
    return {
      pristineValues: pristineValues,
      checkAgainstAppProps: checkAgainstAppProps,
      config: _config // passing this correct values back
    }
  }

  /**
   * This will take the value that is ONLY need to check
   * @param {object} config that one
   * @param {object} props map for creating checking
   * @return {object} put that arg into the args
   */
  function processConfigAction(config, props) {
    // debugFn('processConfigAction', props)
    // v.1.2.0 add checking if its mark optional and the value is empty then pass
    return mapValues(props, function (value, key) {
      var obj, obj$1;

      return (
      config[key] === undefined || (value[OPTIONAL_KEY] === true && isEmpty(config[key]))
        ? merge({}, value, ( obj = {}, obj[KEY_WORD] = true, obj ))
        : ( obj$1 = {}, obj$1[ARGS_KEY] = config[key], obj$1[TYPE_KEY] = value[TYPE_KEY], obj$1[OPTIONAL_KEY] = value[OPTIONAL_KEY] || false, obj$1[ENUM_KEY] = value[ENUM_KEY] || false, obj$1[CHECKER_KEY] = value[CHECKER_KEY] || false, obj$1 )
      );
    }
    )
  }

  /**
   * Quick transform
   * @TODO we should only validate those that is pass from the config
   * and pass through those values that is from the defaultOptions
   * @param {object} opts that one
   * @param {object} appProps mutation configuration options
   * @return {object} put that arg into the args
   */
  function prepareArgsForValidation(opts, appProps) {
    var ref = preservePristineValues(opts, appProps);
    var config = ref.config;
    var pristineValues = ref.pristineValues;
    var checkAgainstAppProps = ref.checkAgainstAppProps;
    // output
    return [
      processConfigAction(config, checkAgainstAppProps),
      pristineValues
    ]
  }

  // breaking the whole thing up to see what cause the multiple calls issue

  // import debug from 'debug';
  // const debugFn = debug('jsonql-params-validator:options:validation')

  /**
   * just make sure it returns an array to use
   * @param {*} arg input
   * @return {array} output
   */
  var toArray$1 = function (arg) { return checkIsArray(arg) ? arg : [arg]; };

  /**
   * DIY in array
   * @param {array} arr to check against
   * @param {*} value to check
   * @return {boolean} true on OK
   */
  var inArray$1 = function (arr, value) { return (
    !!arr.filter(function (v) { return v === value; }).length
  ); };

  /**
   * break out to make the code easier to read
   * @param {object} value to process
   * @param {function} cb the validateSync
   * @return {array} empty on success
   */
  function validateHandler$1(value, cb) {
    var obj;

    // cb is the validateSync methods
    var args = [
      [ value[ARGS_KEY] ],
      [( obj = {}, obj[TYPE_KEY] = toArray$1(value[TYPE_KEY]), obj[OPTIONAL_KEY] = value[OPTIONAL_KEY], obj )]
    ];
    // debugFn('validateHandler', args)
    return Reflect.apply(cb, null, args)
  }

  /**
   * Check against the enum value if it's provided
   * @param {*} value to check
   * @param {*} enumv to check against if it's not false
   * @return {boolean} true on OK
   */
  var enumHandler = function (value, enumv) {
    if (checkIsArray(enumv)) {
      return inArray$1(enumv, value)
    }
    return true
  };

  /**
   * Allow passing a function to check the value
   * There might be a problem here if the function is incorrect
   * and that will makes it hard to debug what is going on inside
   * @TODO there could be a few feature add to this one under different circumstance
   * @param {*} value to check
   * @param {function} checker for checking
   */
  var checkerHandler = function (value, checker) {
    try {
      return isFunction(checker) ? checker.apply(null, [value]) : false
    } catch (e) {
      return false
    }
  };

  /**
   * Taken out from the runValidaton this only validate the required values
   * @param {array} args from the config2argsAction
   * @param {function} cb validateSync
   * @return {array} of configuration values
   */
  function runValidationAction(cb) {
    return function (value, key) {
      // debugFn('runValidationAction', key, value)
      if (value[KEY_WORD]) {
        return value[ARGS_KEY]
      }
      var check = validateHandler$1(value, cb);
      if (check.length) {
        // log('runValidationAction', key, value)
        throw new JsonqlTypeError(key, check)
      }
      if (value[ENUM_KEY] !== false && !enumHandler(value[ARGS_KEY], value[ENUM_KEY])) {
        // log(ENUM_KEY, value[ENUM_KEY])
        throw new JsonqlEnumError(key)
      }
      if (value[CHECKER_KEY] !== false && !checkerHandler(value[ARGS_KEY], value[CHECKER_KEY])) {
        // log(CHECKER_KEY, value[CHECKER_KEY])
        throw new JsonqlCheckerError(key)
      }
      return value[ARGS_KEY]
    }
  }

  /**
   * @param {object} args from the config2argsAction
   * @param {function} cb validateSync
   * @return {object} of configuration values
   */
  function runValidation(args, cb) {
    var argsForValidate = args[0];
    var pristineValues = args[1];
    // turn the thing into an array and see what happen here
    // debugFn('_args', argsForValidate)
    var result = mapValues(argsForValidate, runValidationAction(cb));
    return merge(result, pristineValues)
  }

  /// this is port back from the client to share across all projects

  // import debug from 'debug'
  // const debugFn = debug('jsonql-params-validator:check-options-async')

  /**
   * Quick transform
   * @param {object} config that one
   * @param {object} appProps mutation configuration options
   * @return {object} put that arg into the args
   */
  var configToArgs = function (config, appProps) {
    return Promise.resolve(
      prepareArgsForValidation(config, appProps)
    )
  };

  /**
   * @param {object} config user provide configuration option
   * @param {object} appProps mutation configuration options
   * @param {object} constProps the immutable configuration options
   * @param {function} cb the validateSync method
   * @return {object} Promise resolve merge config object
   */
  function checkOptionsAsync(config, appProps, constProps, cb) {
    if ( config === void 0 ) config = {};

    return configToArgs(config, appProps)
      .then(function (args1) { return runValidation(args1, cb); })
      // next if every thing good then pass to final merging
      .then(function (args2) { return merge({}, args2, constProps); })
  }

  // create function to construct the config entry so we don't need to keep building object
  // import checkIsBoolean from '../boolean'
  // import debug from 'debug';
  // const debugFn = debug('jsonql-params-validator:construct-config');
  /**
   * @param {*} args value
   * @param {string} type for value
   * @param {boolean} [optional=false]
   * @param {boolean|array} [enumv=false]
   * @param {boolean|function} [checker=false]
   * @return {object} config entry
   */
  function constructConfig(args, type, optional, enumv, checker, alias) {
    if ( optional === void 0 ) optional=false;
    if ( enumv === void 0 ) enumv=false;
    if ( checker === void 0 ) checker=false;
    if ( alias === void 0 ) alias=false;

    var base = {};
    base[ARGS_KEY] = args;
    base[TYPE_KEY] = type;
    if (optional === true) {
      base[OPTIONAL_KEY] = true;
    }
    if (checkIsArray(enumv)) {
      base[ENUM_KEY] = enumv;
    }
    if (isFunction(checker)) {
      base[CHECKER_KEY] = checker;
    }
    if (isString(alias)) {
      base[ALIAS_KEY] = alias;
    }
    return base
  }

  // export also create wrapper methods

  /**
   * This has a different interface
   * @param {*} value to supply
   * @param {string|array} type for checking
   * @param {object} params to map against the config check
   * @param {array} params.enumv NOT enum
   * @param {boolean} params.optional false then nothing
   * @param {function} params.checker need more work on this one later
   * @param {string} params.alias mostly for cmd
   */
  var createConfig = function (value, type, params) {
    if ( params === void 0 ) params = {};

    // Note the enumv not ENUM
    // const { enumv, optional, checker, alias } = params;
    // let args = [value, type, optional, enumv, checker, alias];
    var o = params[OPTIONAL_KEY];
    var e = params[ENUM_KEY];
    var c = params[CHECKER_KEY];
    var a = params[ALIAS_KEY];
    return constructConfig.apply(null,  [value, type, o, e, c, a])
  };

  /**
   * construct the actual end user method, rename with prefix get since 1.5.2
   * @param {function} validateSync validation method
   * @return {function} for performaning the actual valdiation
   */
  var getCheckConfigAsync = function(validateSync) {
    /**
     * We recreate the method here to avoid the circlar import
     * @param {object} config user supply configuration
     * @param {object} appProps mutation options
     * @param {object} [constantProps={}] optional: immutation options
     * @return {object} all checked configuration
     */
    return function(config, appProps, constantProps) {
      if ( constantProps === void 0 ) constantProps= {};

      return checkOptionsAsync(config, appProps, constantProps, validateSync)
    }
  };

  // export
  var isString$1 = checkIsString;
  var validateAsync$1 = validateAsync;

  var createConfig$1 = createConfig;
  // construct the final output 1.5.2
  var checkConfigAsync = getCheckConfigAsync(validateSync);

  // move the get logger stuff here

  // it does nothing
  var dummyLogger = function () {};

  /**
   * re-use the debugOn prop to control this log method
   * @param {object} opts configuration
   * @return {function} the log function
   */
  var getLogger = function (opts) {
    var debugOn = opts.debugOn;  
    if (debugOn) {
      return function () {
        var args = [], len = arguments.length;
        while ( len-- ) args[ len ] = arguments[ len ];

        Reflect.apply(console.info, console, ['[jsonql-ws-client-core]' ].concat( args));
      }
    }
    return dummyLogger
  };

  /**
   * Make sure there is a log method
   * @param {object} opts configuration
   * @return {object} opts
   */
  var getLogFn = function (opts) {
    var log = opts.log; // 1.3.9 if we pass a log method here then we use this
    if (!log || typeof log !== 'function') {
      return getLogger(opts)
    }
    opts.log('---> getLogFn user supplied log function <---', opts);
    return log
  };

  // group all the repetitive message here

  var TAKEN_BY_OTHER_TYPE_ERR = 'You are trying to register an event already been taken by other type:';

  // use constants for type
  var ON_TYPE = 'on';
  var ONLY_TYPE = 'only';
  var ONCE_TYPE = 'once';
  var ONLY_ONCE_TYPE = 'onlyOnce';
  var NEG_RETURN = -1;

  var AVAILABLE_TYPES = [
    ON_TYPE,
    ONLY_TYPE,
    ONCE_TYPE,
    ONLY_ONCE_TYPE
  ];
  // the type which the callMax can execute on
  var ON_MAX_TYPES = [
    ON_TYPE,
    ONLY_TYPE
  ];

  /**
   * generate a 32bit hash based on the function.toString()
   * _from http://stackoverflow.com/questions/7616461/generate-a-hash-_from-string-in-javascript-jquery
   * @param {string} s the converted to string function
   * @return {string} the hashed function string
   */
  function hashCode(s) {
  	return s.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0)
  }

  /**
   * wrapper to make sure it string
   * @param {*} input whatever
   * @return {string} output
   */
  function hashCode2Str(s) {
    return hashCode(s) + ''
  }

  /**
   * Just check if a pattern is an RegExp object
   * @param {*} pat whatever
   * @return {boolean} false when its not
   */
  function isRegExp(pat) {
    return pat instanceof RegExp
  }

  /**
   * check if its string
   * @param {*} arg whatever
   * @return {boolean} false when it's not
   */
  function isString$2(arg) {
    return typeof arg === 'string'
  }

  /**
   * check if it's an integer
   * @param {*} num input number
   * @return {boolean}
   */
  function isInt(num) {
    if (isString$2(num)) {
      throw new Error("Wrong type, we want number!")
    }
    return !isNaN(parseInt(num))
  }

  /**
   * Find from the array by matching the pattern
   * @param {*} pattern a string or RegExp object
   * @return {object} regex object or false when we can not id the input
   */
  function getRegex(pattern) {
    switch (true) {
      case isRegExp(pattern) === true:
        return pattern
      case isString$2(pattern) === true:
        return new RegExp(pattern)
      default:
        return false
    }
  }


  /**
   * in array
   * @param {array} arr to search
   * @param {*} prop to search
   */
   var inArray$2 = function (arr, prop) { return !!arr.filter(function (v) { return prop === v; }).length; };

  // Create two WeakMap store as a private keys
  var NB_EVENT_SERVICE_PRIVATE_STORE = new WeakMap();
  var NB_EVENT_SERVICE_PRIVATE_LAZY = new WeakMap();

  // setup a base class to put all the don't know where to put methods 

  var BaseClass = function BaseClass() {};

  var prototypeAccessors = { $name: { configurable: true },is: { configurable: true } };

  /**
   * logger function for overwrite
   */
  BaseClass.prototype.logger = function logger () {};

  // for id if the instance is this class
  prototypeAccessors.$name.get = function () {
    return 'to1source-event'
  };

  // take this down in the next release
  prototypeAccessors.is.get = function () {
    return this.$name
  };

  /**
   * validate the event name(s)
   * @param {string[]} evt event name
   * @return {boolean} true when OK
   */
  BaseClass.prototype.validateEvt = function validateEvt () {
      var this$1 = this;
      var evt = [], len = arguments.length;
      while ( len-- ) evt[ len ] = arguments[ len ];

    evt.forEach(function (e) {
      if (!isString$2(e)) {
        this$1.logger('(validateEvt)', e);

        throw new Error(("Event name must be string type! we got " + (typeof e)))
      }
    });

    return true
  };

  /**
   * Simple quick check on the two main parameters
   * @param {string} evt event name
   * @param {function} callback function to call
   * @return {boolean} true when OK
   */
  BaseClass.prototype.validate = function validate (evt, callback) {
    if (this.validateEvt(evt)) {
      if (typeof callback === 'function') {

        return true
      }
    }
    throw new Error(("callback required to be function type! we got " + (typeof callback)))
  };

  /**
   * Check if this type is correct or not added in V1.5.0
   * @param {string} type for checking
   * @return {boolean} true on OK
   */
  BaseClass.prototype.validateType = function validateType (type) {
    this.validateEvt(type);
      
    return !!AVAILABLE_TYPES.filter(function (t) { return type === t; }).length
  };

  /**
   * Run the callback
   * @param {function} callback function to execute
   * @param {array} payload for callback
   * @param {object} ctx context or null
   * @return {void} the result store in $done
   */
  BaseClass.prototype.run = function run (callback, payload, ctx) {
    this.logger('(run) callback:', callback, 'payload:', payload, 'context:', ctx);
    this.$done = Reflect.apply(callback, ctx, this.toArray(payload));

    return this.$done // return it here first 
  };

  /**
   * generate a hashKey to identify the function call
   * The build-in store some how could store the same values!
   * @param {function} fn the converted to string function
   * @return {string} hashKey
   */
  BaseClass.prototype.hashFnToKey = function hashFnToKey (fn) {

    return hashCode2Str(fn.toString())
  };

  Object.defineProperties( BaseClass.prototype, prototypeAccessors );

  // making all the functionality on it's own

  var SuspendClass = /*@__PURE__*/(function (BaseClass) {
    function SuspendClass() {
      BaseClass.call(this);
      // suspend, release and queue
      this.__suspend_state__ = null;
      // to do this proper we don't use a new prop to hold the event name pattern
      // @1.2.2 this become an array so we can hold different events
      this.__pattern__ = [];
      // key value pair store to store the queued calls
      this.queueStore = new Set();
    }

    if ( BaseClass ) SuspendClass.__proto__ = BaseClass;
    SuspendClass.prototype = Object.create( BaseClass && BaseClass.prototype );
    SuspendClass.prototype.constructor = SuspendClass;

    var prototypeAccessors = { $queues: { configurable: true } };

    /**
     * start suspend
     * @return {void}
     */
    SuspendClass.prototype.$suspend = function $suspend () {
      this.logger("---> SUSPEND ALL OPS <---");
      this.__suspend__(true);
    };

    /**
     * release the queue
     * @return {void}
     */
    SuspendClass.prototype.$release = function $release () {
      this.logger("---> RELEASE ALL SUSPENDED QUEUE <---");
      this.__suspend__(false);
    };

    /**
     * suspend event by pattern
     * @param {array.<string>} patterns the pattern search matches the event name
     * @return {array} if the pattern return is registered or not
     */
    SuspendClass.prototype.$suspendEvent = function $suspendEvent () {
      var this$1 = this;
      var patterns = [], len = arguments.length;
      while ( len-- ) patterns[ len ] = arguments[ len ];

      return patterns.map(function (pattern) {
        var regex = getRegex(pattern);
        if (isRegExp(regex)) {
          // check if it's already added 
          if (this$1.__isPatternRegisterd(regex) === false) {
            this$1.__pattern__.push(regex);

            return this$1.__pattern__.length
          }
          return false
        }
        throw new Error(("We expect a pattern variable to be string or RegExp, but we got \"" + (typeof regex) + "\" instead"))
      })
    };

    /**
     * This is pair with $suspnedEvent to release part of the event queue by the pattern (eventName)
     * @param {array.<*>} patterns a eventName of partial eventName to create a RegExp
     * @return {number} should be the number of queue got released
     */
    SuspendClass.prototype.$releaseEvent = function $releaseEvent () {
      var this$1 = this;
      var patterns = [], len = arguments.length;
      while ( len-- ) patterns[ len ] = arguments[ len ];

      return patterns.map(function (pattern) {
        this$1.logger("($releaseEvent)", pattern);
        var regex = getRegex(pattern);
        if (isRegExp(regex) && this$1.__isPatternRegisterd(regex)) {
          var self = this$1;

          return this$1.__getToReleaseQueue(regex)
            .map(function (args, i) {
              
              Reflect.apply(self.$trigger, self, args);

              return i 
            }).reduce(function (a, b) { return ++b; }, 0)
        }

        this$1.logger('$releaseEvent throw error ==========================>', this$1.__pattern__, regex);
        throw new Error(("We expect a pattern variable to be string or RegExp, but we got \"" + (typeof regex) + "\" instead"))
      })
      .reduce(function (x, y) { return x + y; }, 0)
    };

    /**
     * queuing call up when it's in suspend mode,
     * it's currently suspending then add to store then the $trigger will do nothing
     * @param {string} evt the event name
     * @param {*} args unknown number of arguments
     * @return {boolean} true when added or false when it's not
     */
    SuspendClass.prototype.$queue = function $queue (evt) {
      var args = [], len = arguments.length - 1;
      while ( len-- > 0 ) args[ len ] = arguments[ len + 1 ];

      switch (true) {
        case this.__suspend_state__ === true: // this will take priority over the pattern
          
          return this.__addToQueueStore(evt, args)
        case !!this.__pattern__.length === true: 
          // check the pattern and decide if we want to suspend it or not
          if (!!this.__pattern__.filter(function (p) { return p.test(evt); }).length) {
            
            return this.__addToQueueStore(evt, args)
          }
          this.logger(("($queue) " + evt + " NOT added to $queueStore"), this.__pattern__);
            
          return false 
        default:
          this.logger('($queue) get called NOTHING added');
          return false
      } 
    };

    /**
     * a getter to get all the store queue
     * @return {array} Set turn into Array before return
     */
    prototypeAccessors.$queues.get = function () {
      var size = this.queueStore.size;
      this.logger('($queues)', ("size: " + size));
      if (size > 0) {
        return Array.from(this.queueStore)
      }
      return []
    };


    /**
     * The reason is before we call $trigger we need to remove the pattern from queue
     * otherwise, it will never get release
     * @param {*} pattern to find the queue 
     * @return {array} queue to get execute 
     */
    SuspendClass.prototype.__getToReleaseQueue = function __getToReleaseQueue (regex) {
      var this$1 = this;

      // first get the list of events in the queue store that match this pattern
      var list = this.$queues
        // first index is the eventName
        .filter(function (content) { return regex.test(content[0]); })
        .map(function (content) {
          this$1.logger(("[release] execute " + (content[0]) + " matches " + regex), content);
              // we just remove it
          this$1.queueStore.delete(content);
          
          return content
        });
      if (list.length > 0) {
        // we need to remove this event from the pattern queue array 
        this.__pattern__ = this.__pattern__.filter(function (p) { return p.toString() !== regex.toString(); }); 
      }

      return list 
    };

    /**
     * Wrapper method with a logger 
     * @param {*} evt 
     * @param {*} args 
     * @return {boolean}
     */
    SuspendClass.prototype.__addToQueueStore = function __addToQueueStore (evt, args) {
      this.logger(("($queue) " + evt + " added to $queueStore"), args);

      // @TODO should we check if this already added? 
      // what if that is a multiple call like $on
      this.queueStore.add([evt].concat(args));

      return true
    };

    /**
     * check if certain pattern already registered in the queue
     * @param {*} pattern
     * @return {boolean} 
     */
    SuspendClass.prototype.__isPatternRegisterd = function __isPatternRegisterd (pattern) {
      // this is a bit of a hack to compare two regex Object  
      return !!this.__pattern__.filter(function (p) { return (
        p.toString() === pattern.toString()
      ); }).length 
    };

    /**
     * to set the suspend and check if it's boolean value
     * @param {boolean} value to trigger
     */
    SuspendClass.prototype.__suspend__ = function __suspend__ (value) {
      if (typeof value === 'boolean') {
        var lastValue = this.__suspend_state__;
        this.__suspend_state__ = value;
        this.logger(("($suspend) Change from \"" + lastValue + "\" --> \"" + value + "\""));
        if (lastValue === true && value === false) {
          this.__release__();
        }
      } else {
        throw new Error(("$suspend only accept Boolean value! we got " + (typeof value)))
      }
    };

    /**
     * Release the queue, this is a wholesale release ALL
     * @return {int} size if any
     */
    SuspendClass.prototype.__release__ = function __release__ () {
      var this$1 = this;

      var size = this.queueStore.size;
      var pattern = this.__pattern__;
      this.__pattern__ = [];
      
      this.logger(("(release) was called with " + size + (pattern.length ? ' for "' + pattern.join(',') + '"': '') + " item" + (size > 1 ? 's' : '')));
      
      if (size > 0) {
        var queue = Array.from(this.queueStore);
        this.logger('(release queue)', queue);

        queue.forEach(function (args) {
          this$1.logger(("[release] execute " + (args[0])), args);

          Reflect.apply(this$1.$trigger, this$1, args);
        });

        this.queueStore.clear();
        this.logger(("Release size " + (this.queueStore.size)));
      }

      return size
    };

    Object.defineProperties( SuspendClass.prototype, prototypeAccessors );

    return SuspendClass;
  }(BaseClass));

  // break up the main file because its getting way too long

  var StoreService = /*@__PURE__*/(function (SuspendClass) {
    function StoreService(config) {
      if ( config === void 0 ) config = {};

      SuspendClass.call(this);
      if (config.logger && typeof config.logger === 'function') {
        this.logger = config.logger;
      }
      this.keep = config.keep;
      // for the $done setter
      this.result = config.keep ? [] : null;
      // we need to init the store first otherwise it could be a lot of checking later
      this.normalStore = new Map();
      this.lazyStore = new Map();
      // this is the new throw away map
      this.maxCountStore = new Map();
    }

    if ( SuspendClass ) StoreService.__proto__ = SuspendClass;
    StoreService.prototype = Object.create( SuspendClass && SuspendClass.prototype );
    StoreService.prototype.constructor = StoreService;

    var prototypeAccessors = { normalStore: { configurable: true },lazyStore: { configurable: true } };

    /**
     * We need this to pre-check the store, otherwise
     * the execution will be unable to determine the number of calls
     * @param {string} evtName event name
     * @return {number} the count of this store
     */
    StoreService.prototype.getMaxStore = function getMaxStore (evtName) {
      return this.maxCountStore.get(evtName) || NEG_RETURN
    };

    /**
     * This is one stop shop to check and munipulate the maxStore
     * @param {*} evtName
     * @param {*} [max=null]
     * @return {number} when return -1 means removed
     */
    StoreService.prototype.checkMaxStore = function checkMaxStore (evtName, max) {
      if ( max === void 0 ) max = null;

      this.logger("===========================================");
      this.logger('checkMaxStore start', evtName, max);
      // init the store
      if (max !== null && isInt(max)) {
        // because this is the setup phrase we just return the max value
        this.maxCountStore.set(evtName, max);
        this.logger(("Setup max store for " + evtName + " with " + max));
        return max
      }
      if (max === null) {
        // first check if this exist in the maxStore
        var value = this.getMaxStore(evtName);

        this.logger('getMaxStore value', value);

        if (value !== NEG_RETURN) {
          if (value > 0) {
            --value;
          }
          if (value > 0) {
            this.maxCountStore.set(evtName, value); // just update the value
          } else {
            this.maxCountStore.delete(evtName); // just remove it
            this.logger(("remove " + evtName + " from maxStore"));
            return NEG_RETURN
          }
        }
        return value
      }
      throw new Error(("Expect max to be an integer, but we got " + (typeof max) + " " + max))
    };

    /**
     * Wrap several get filter ops together to return the callback we are looking for
     * @param {string} evtName to search for
     * @return {array} empty array when not found
     */
    StoreService.prototype.searchMapEvt = function searchMapEvt (evtName) {
      var evts = this.$get(evtName, true); // return in full
      var search = evts.filter(function (result) {
        var type = result[3];

        return inArray$2(ON_MAX_TYPES, type)
      });

      return search.length ? search : []
    };

    /**
     * Take the content out and remove it from store id by the name
     * @param {string} evt event name
     * @param {string} [storeName = lazyStore] name of store
     * @return {object|boolean} content or false on not found
     */
    StoreService.prototype.takeFromStore = function takeFromStore (evt, storeName) {
      if ( storeName === void 0 ) storeName = 'lazyStore';

      var store = this[storeName]; // it could be empty at this point
      if (store) {
        this.logger('(takeFromStore)', storeName, store);

        if (store.has(evt)) {
          var content = store.get(evt);
          this.logger(("(takeFromStore) has \"" + evt + "\""), content);
          store.delete(evt);

          return content
        }

        return false
      }
      throw new Error(("\"" + storeName + "\" is not supported!"))
    };

    /**
     * This was part of the $get. We take it out
     * so we could use a regex to remove more than one event
     * @param {object} store the store to return from
     * @param {string} evt event name
     * @param {boolean} full return just the callback or everything
     * @return {array|boolean} false when not found
     */
    StoreService.prototype.findFromStore = function findFromStore (evt, store, full) {
      if ( full === void 0 ) full = false;

      if (store.has(evt)) {
        return Array
          .from(store.get(evt))
          .map( function (l) {
            if (full) {
              return l
            }
            var callback = l[1];

            return callback
          })
      }
      return false
    };

    /**
     * Similar to the findFromStore, but remove
     * @param {string} evt event name
     * @param {object} store the store to remove from
     * @return {boolean} false when not found
     */
    StoreService.prototype.removeFromStore = function removeFromStore (evt, store) {
      if (store.has(evt)) {
        this.logger('($off)', evt);

        store.delete(evt);

        return true
      }
      return false
    };

    /**
     * Take out from addToStore for reuse
     * @param {object} store the store to use
     * @param {string} evt event name
     * @return {object} the set within the store
     */
    StoreService.prototype.getStoreSet = function getStoreSet (store, evt) {
      var fnSet;
      if (store.has(evt)) {
        this.logger(("(addToStore) \"" + evt + "\" existed"));
        fnSet = store.get(evt);
      } else {
        this.logger(("(addToStore) create new Set for \"" + evt + "\""));
        // this is new
        fnSet = new Set();
      }
      return fnSet
    };

    /**
     * The add to store step is similar so make it generic for resuse
     * @param {object} store which store to use
     * @param {string} evt event name
     * @param {spread} args because the lazy store and normal store store different things
     * @return {array} store and the size of the store
     */
    StoreService.prototype.addToStore = function addToStore (store, evt) {
      var args = [], len = arguments.length - 2;
      while ( len-- > 0 ) args[ len ] = arguments[ len + 2 ];

      var fnSet = this.getStoreSet(store, evt);
      // lazy only store 2 items - this is not the case in V1.6.0 anymore
      // we need to check the first parameter is string or not
      if (args.length > 2) {
        if (Array.isArray(args[0])) { // lazy store
          // check if this type of this event already register in the lazy store
          var t = args[2];
          if (!this.checkTypeInLazyStore(evt, t)) {
            fnSet.add(args);
          }
        } else {
          if (!this.checkContentExist(args, fnSet)) {
            this.logger("(addToStore) insert new", args);
            fnSet.add(args);
          }
        }
      } else { // add straight to lazy store
        fnSet.add(args);
      }
      store.set(evt, fnSet);

      return [store, fnSet.size]
    };

    /**
     * @param {array} args for compare
     * @param {object} fnSet A Set to search from
     * @return {boolean} true on exist
     */
    StoreService.prototype.checkContentExist = function checkContentExist (args, fnSet) {
      var list = Array.from(fnSet);
      return !!list.filter(function (li) {
        var hash = li[0];
        return hash === args[0]
      }).length
    };

    /**
     * get the existing type to make sure no mix type add to the same store
     * @param {string} evtName event name
     * @param {string} type the type to check
     * @return {boolean} true you can add, false then you can't add this type
     */
    StoreService.prototype.checkTypeInStore = function checkTypeInStore (evtName, type) {
      this.validateEvt(evtName, type);
      var all = this.$get(evtName, true);
      if (all === false) {
        // pristine it means you can add
        return true
      }
      // it should only have ONE type in ONE event store
      return !all.filter(function (list) {
        var t = list[3];
        return type !== t
      }).length
    };

    /**
     * This is checking just the lazy store because the structure is different
     * therefore we need to use a new method to check it
     */
    StoreService.prototype.checkTypeInLazyStore = function checkTypeInLazyStore (evtName, type) {
      this.validateEvt(evtName, type);
      var store = this.lazyStore.get(evtName);

      this.logger('(checkTypeInLazyStore)', store);

      if (store) {

        return !!Array
          .from(store)
          .filter(function (li) {
            var t = li[2];
            return t !== type
          }).length
      }

      return false
    };

    /**
     * wrapper to re-use the addToStore,
     * V1.3.0 add extra check to see if this type can add to this evt
     * @param {string} evt event name
     * @param {string} type on or once
     * @param {function} callback function
     * @param {object} context the context the function execute in or null
     * @return {number} size of the store
     */
    StoreService.prototype.addToNormalStore = function addToNormalStore (evt, type, callback, context) {
      if ( context === void 0 ) context = null;

      this.logger(("(addToNormalStore) try to add \"" + type + "\" --> \"" + evt + "\" to normal store"));
      // @TODO we need to check the existing store for the type first!
      if (this.checkTypeInStore(evt, type)) {

        this.logger('(addToNormalStore)', ("\"" + type + "\" --> \"" + evt + "\" can add to normal store"));

        var key = this.hashFnToKey(callback);
        var args = [this.normalStore, evt, key, callback, context, type];
        var ref = Reflect.apply(this.addToStore, this, args);
        var _store = ref[0];
        var size = ref[1];
        this.normalStore = _store;

        return size
      }

      return false
    };

    /**
     * Add to lazy store this get calls when the callback is not register yet
     * so we only get a payload object or even nothing
     * @param {string} evt event name
     * @param {array} payload of arguments or empty if there is none
     * @param {object} [context=null] the context the callback execute in
     * @param {string} [type=false] register a type so no other type can add to this evt
     * @return {number} size of the store
     */
    StoreService.prototype.addToLazyStore = function addToLazyStore (evt, payload, context, type) {
      if ( payload === void 0 ) payload = [];
      if ( context === void 0 ) context = null;
      if ( type === void 0 ) type = false;

      // this is add in V1.6.0
      // when there is type then we will need to check if this already added in lazy store
      // and no other type can add to this lazy store
      var args = [this.lazyStore, evt, this.toArray(payload), context];
      if (type) {
        args.push(type);
      }
      var ref = Reflect.apply(this.addToStore, this, args);
      var _store = ref[0];
      var size = ref[1];
      this.lazyStore = _store;
      this.logger(("(addToLazyStore) size: " + size));

      return size
    };

    /**
     * make sure we store the argument correctly
     * @param {*} arg could be array
     * @return {array} make sured
     */
    StoreService.prototype.toArray = function toArray (arg) {
      return Array.isArray(arg) ? arg : [arg]
    };

    /**
     * setter to store the Set in private
     * @param {object} obj a Set
     */
    prototypeAccessors.normalStore.set = function (obj) {
      NB_EVENT_SERVICE_PRIVATE_STORE.set(this, obj);
    };

    /**
     * @return {object} Set object
     */
    prototypeAccessors.normalStore.get = function () {
      return NB_EVENT_SERVICE_PRIVATE_STORE.get(this)
    };

    /**
     * setter to store the Set in lazy store
     * @param {object} obj a Set
     */
    prototypeAccessors.lazyStore.set = function (obj) {
      NB_EVENT_SERVICE_PRIVATE_LAZY.set(this , obj);
    };

    /**
     * @return {object} the lazy store Set
     */
    prototypeAccessors.lazyStore.get = function () {
      return NB_EVENT_SERVICE_PRIVATE_LAZY.get(this)
    };

    Object.defineProperties( StoreService.prototype, prototypeAccessors );

    return StoreService;
  }(SuspendClass));

  // The top level
  // export
  var EventService = /*@__PURE__*/(function (StoreService) {
    function EventService(config) {
      if ( config === void 0 ) config = {};

      StoreService.call(this, config);
    }

    if ( StoreService ) EventService.__proto__ = StoreService;
    EventService.prototype = Object.create( StoreService && StoreService.prototype );
    EventService.prototype.constructor = EventService;

    var prototypeAccessors = { $done: { configurable: true } };

    //////////////////////////
    //    PUBLIC METHODS    //
    //////////////////////////

    /**
     * Register your evt handler, note we don't check the type here,
     * we expect you to be sensible and know what you are doing.
     * @param {string} evt name of event
     * @param {function} callback bind method --> if it's array or not
     * @param {object} [context=null] to execute this call in
     * @return {number} the size of the store
     */
    EventService.prototype.$on = function $on (evt , callback , context) {
      var this$1 = this;
      if ( context === void 0 ) context = null;

      var type = 'on';
      this.validate(evt, callback);
      // first need to check if this evt is in lazy store
      var lazyStoreContent = this.takeFromStore(evt);
      // this is normal register first then call later
      if (lazyStoreContent === false) {
        this.logger(("($on) \"" + evt + "\" is not in lazy store"));
        // @TODO we need to check if there was other listener to this
        // event and are they the same type then we could solve that
        // register the different type to the same event name
        return this.addToNormalStore(evt, type, callback, context)
      }
      this.logger(("($on) " + evt + " found in lazy store"));
      // this is when they call $trigger before register this callback
      var size = 0;
      lazyStoreContent.forEach(function (content) {
        var payload = content[0];
        var ctx = content[1];
        var t = content[2];
        if (t && t !== type) {
          throw new Error((TAKEN_BY_OTHER_TYPE_ERR + " " + t))
        }
        this$1.logger("($on)", ("call run \"" + evt + "\""));
        this$1.run(callback, payload, context || ctx);
        size += this$1.addToNormalStore(evt, type, callback, context || ctx);
      });

      this.logger(("($on) return size " + size));
      return size
    };

    /**
     * once only registered it once, there is no overwrite option here
     * @NOTE change in v1.3.0 $once can add multiple listeners
     *       but once the event fired, it will remove this event (see $only)
     * @param {string} evt name
     * @param {function} callback to execute
     * @param {object} [context=null] the handler execute in
     * @return {boolean} result
     */
    EventService.prototype.$once = function $once (evt , callback , context) {
      if ( context === void 0 ) context = null;

      this.validate(evt, callback);

      var lazyStoreContent = this.takeFromStore(evt);
      // this is normal register before call $trigger
      // let nStore = this.normalStore
      if (lazyStoreContent === false) {
        this.logger(("($once) \"" + evt + "\" is not in the lazy store"));
        // v1.3.0 $once now allow to add multiple listeners
        return this.addToNormalStore(evt, ONCE_TYPE, callback, context)
      } else {
        // now this is the tricky bit
        // there is a potential bug here that cause by the developer
        // if they call $trigger first, the lazy won't know it's a once call
        // so if in the middle they register any call with the same evt name
        // then this $once call will be fucked - add this to the documentation
        this.logger('($once)', lazyStoreContent);
        var list = Array.from(lazyStoreContent);
        // should never have more than 1
        var ref = list[0];
        var payload = ref[0];
        var ctx = ref[1];
        var t = ref[2];
        if (t && t !== ONCE_TYPE) {
          throw new Error((TAKEN_BY_OTHER_TYPE_ERR + " " + t))
        }
        this.logger('($once)', ("call run \"" + evt + "\""));
        this.run(callback, payload, context || ctx);
        // remove this evt from store
        this.$off(evt);
      }
    };

    /**
     * This one event can only bind one callbackback
     * @param {string} evt event name
     * @param {function} callback event handler
     * @param {object} [context=null] the context the event handler execute in
     * @return {boolean} true bind for first time, false already existed
     */
    EventService.prototype.$only = function $only (evt, callback, context) {
      var this$1 = this;
      if ( context === void 0 ) context = null;

      this.validate(evt, callback);

      var added = false;
      var lazyStoreContent = this.takeFromStore(evt);
      // this is normal register before call $trigger
      var nStore = this.normalStore;

      if (!nStore.has(evt)) {
        this.logger(("($only) \"" + evt + "\" add to normalStore"));

        added = this.addToNormalStore(evt, ONLY_TYPE, callback, context);
      }

      if (lazyStoreContent !== false) {
        // there are data store in lazy store
        this.logger(("($only) \"" + evt + "\" found data in lazy store to execute"));
        var list = Array.from(lazyStoreContent);
        // $only allow to trigger this multiple time on the single handler
        list.forEach( function (li) {
          var payload = li[0];
          var ctx = li[1];
          var t = li[2];
          if (t && t !== ONLY_TYPE) {
            throw new Error((TAKEN_BY_OTHER_TYPE_ERR + " " + t))
          }
          this$1.logger(("($only) call run \"" + evt + "\""));
          this$1.run(callback, payload, context || ctx);
        });
      }

      return added
    };

    /**
     * $only + $once this is because I found a very subtile bug when we pass a
     * resolver, rejecter - and it never fire because that's OLD added in v1.4.0
     * @param {string} evt event name
     * @param {function} callback to call later
     * @param {object} [context=null] exeucte context
     * @return {void}
     */
    EventService.prototype.$onlyOnce = function $onlyOnce (evt, callback, context) {
      if ( context === void 0 ) context = null;

      this.validate(evt, callback);

      var added = false;
      var lazyStoreContent = this.takeFromStore(evt);
      // this is normal register before call $trigger
      var nStore = this.normalStore;
      if (!nStore.has(evt)) {
        this.logger(("($onlyOnce) \"" + evt + "\" add to normalStore"));

        added = this.addToNormalStore(evt, ONLY_ONCE_TYPE, callback, context);
      }

      if (lazyStoreContent !== false) {
        // there are data store in lazy store
        this.logger('($onlyOnce)', lazyStoreContent);
        var list = Array.from(lazyStoreContent);
        // should never have more than 1
        var ref = list[0];
        var payload = ref[0];
        var ctx = ref[1];
        var t = ref[2];
        if (t && t !== ONLY_ONCE_TYPE) {
          throw new Error((TAKEN_BY_OTHER_TYPE_ERR + " " + t))
        }
        this.logger(("($onlyOnce) call run \"" + evt + "\""));

        this.run(callback, payload, context || ctx);
        // remove this evt from store
        this.$off(evt);
      }
      return added
    };

    /**
     * change the way how it suppose to work, instead of create another new store
     * We perform this check on the trigger end, so we set the number max
     * whenever we call the callback, we increment a value in the store
     * once it reaches that number we remove that event from the store,
     * also this will not get add to the lazy store,
     * which means the event must register before we can fire it
     * therefore we don't have to deal with the backward check
     * @param {string} evtName the event to get pre-registered
     * @param {number} max pass the max amount of callback can add to this event
     * @param {*} [ctx=null] the context the callback execute in
     * @return {function} the event handler
     */
    EventService.prototype.$max = function $max (evtName, max, ctx) {
      if ( ctx === void 0 ) ctx = null;

      this.validateEvt(evtName);
      if (isInt(max) && max > 0) {
        // find this in the normalStore
        var fnSet = this.$get(evtName, true);
        if (fnSet !== false) {
          var evts = this.searchMapEvt(evtName);
          if (evts.length) {
            // should only have one anyway
            var ref = evts[0];
            var type = ref[3];
            // now init the max store
            var value = this.checkMaxStore(evtName, max);
            var _self = this;
            /**
             * construct the callback
             * @param {array<*>} args
             * @return {number} 
             */
            return function executeMaxCall() {
              var args = [], len = arguments.length;
              while ( len-- ) args[ len ] = arguments[ len ];

              var ctn = _self.getMaxStore(evtName);
              var value = NEG_RETURN;
              if (ctn > 0) {
                var fn = _self.$call(evtName, type, ctx);
                Reflect.apply(fn, _self, args);

                value = _self.checkMaxStore(evtName);
                if (value === NEG_RETURN) {
                  _self.$off(evtName);
                  return NEG_RETURN
                }
              }
              return value
            }
          }
        }
        // change in 1.1.1 because we might just call it without knowing if it's register or not
        this.logger(("The " + evtName + " is not registered, can not execute non-existing event at the moment"));
        return NEG_RETURN
      }
      throw new Error(("Expect max to be an integer and greater than zero! But we got [" + (typeof max) + "]" + max + " instead"))
    };

    /**
     * This is a shorthand of $off + $on added in V1.5.0
     * @param {string} evt event name
     * @param {function} callback to exeucte
     * @param {object} [context = null] or pass a string as type
     * @param {string} [type=on] what type of method to replace
     * @return {*}
     */
    EventService.prototype.$replace = function $replace (evt, callback, context, type) {
      if ( context === void 0 ) context = null;
      if ( type === void 0 ) type = ON_TYPE;

      if (this.validateType(type)) {
        this.$off(evt);
        var method = this['$' + type];

        this.logger("($replace)", evt, callback);

        return Reflect.apply(method, this, [evt, callback, context])
      }
      throw new Error((type + " is not supported!"))
    };

    /**
     * trigger the event
     * @param {string} evt name NOT allow array anymore!
     * @param {mixed} [payload = []] pass to fn
     * @param {object|string} [context = null] overwrite what stored
     * @param {string} [type=false] if pass this then we need to add type to store too
     * @return {number} if it has been execute how many times
     */
    EventService.prototype.$trigger = function $trigger (evt , payload , context, type) {
      if ( payload === void 0 ) payload = [];
      if ( context === void 0 ) context = null;
      if ( type === void 0 ) type = false;

      this.validateEvt(evt);
      var found = 0;
      // first check the normal store
      var nStore = this.normalStore;
      this.logger('($trigger) normalStore', nStore);
      if (nStore.has(evt)) {
        this.logger(("($trigger) \"" + evt + "\" found"));
        // @1.8.0 to add the suspend queue
        var added = this.$queue(evt, payload, context, type);
        if (added) {
          this.logger(("($trigger) Currently suspended \"" + evt + "\" added to queue, nothing executed. Exit now."));
          return false // not executed
        }
        var nSet = Array.from(nStore.get(evt));
        var ctn = nSet.length;
        var hasOnce = false;
        // let hasOnly = false
        for (var i=0; i < ctn; ++i) {
          ++found;
          // this.logger('found', found)
          var ref = nSet[i];
          var _ = ref[0];
          var callback = ref[1];
          var ctx = ref[2];
          var _type = ref[3];
          this.logger(("($trigger) call run for " + type + ":" + evt));

          this.run(callback, payload, context || ctx);

          if (_type === 'once' || _type === 'onlyOnce') {
            hasOnce = true;
          }
        }
        if (hasOnce) {
          nStore.delete(evt);
        }
        return found
      }
      // now this is not register yet
      this.addToLazyStore(evt, payload, context, type);
      return found
    };

    /**
     * this is an alias to the $trigger
     * @NOTE breaking change in V1.6.0 we swap the parameter aroun
     * @NOTE breaking change: v1.9.1 it return an function to accept the params as spread
     * @param {string} evt event name
     * @param {string} type of call
     * @param {object} context what context callback execute in
     * @return {*} from $trigger
     */
    EventService.prototype.$call = function $call (evt, type, context) {
      if ( type === void 0 ) type = false;
      if ( context === void 0 ) context = null;

      var ctx = this;

      return function executeCall() {
        var args = [], len = arguments.length;
        while ( len-- ) args[ len ] = arguments[ len ];

        var _args = [evt, args, context, type];

        return Reflect.apply(ctx.$trigger, ctx, _args)
      }
    };

    /**
     * remove the evt from all the stores
     * @param {string} evt name
     * @return {boolean} true actually delete something
     */
    EventService.prototype.$off = function $off (evt) {
      var this$1 = this;

      // @TODO we will allow a regex pattern to mass remove event
      this.validateEvt(evt);
      var stores = [ this.lazyStore, this.normalStore ];

      return !!stores
            .filter(function (store) { return store.has(evt); })
            .map(function (store) { return this$1.removeFromStore(evt, store); })
            .length
    };

    /**
     * return all the listener bind to that event name
     * @param {string} evtName event name
     * @param {boolean} [full=false] if true then return the entire content
     * @return {array|boolean} listerner(s) or false when not found
     */
    EventService.prototype.$get = function $get (evt, full) {
      if ( full === void 0 ) full = false;

      // @TODO should we allow the same Regex to search for all?
      this.validateEvt(evt);
      var store = this.normalStore;
      return this.findFromStore(evt, store, full)
    };

    /**
     * store the return result from the run
     * @param {*} value whatever return from callback
     */
    prototypeAccessors.$done.set = function (value) {
      this.logger('($done) set value: ', value);
      if (this.keep) {
        this.result.push(value);
      } else {
        this.result = value;
      }
    };

    /**
     * @TODO is there any real use with the keep prop?
     * getter for $done
     * @return {*} whatever last store result
     */
    prototypeAccessors.$done.get = function () {
      this.logger('($done) get result:', this.result);
      if (this.keep) {
        return this.result[this.result.length - 1]
      }
      return this.result
    };

    /**
     * Take a look inside the stores
     * @param {number|null} idx of the store, null means all
     * @return {void}
     */
    EventService.prototype.$debug = function $debug (idx) {
      var this$1 = this;
      if ( idx === void 0 ) idx = null;

      var names = ['lazyStore', 'normalStore'];
      var stores = [this.lazyStore, this.normalStore];
      if (stores[idx]) {
        this.logger(names[idx], stores[idx]);
      } else {
        stores.map(function (store, i) {
          this$1.logger(names[i], store);
        });
      }
    };

    Object.defineProperties( EventService.prototype, prototypeAccessors );

    return EventService;
  }(StoreService));

  // default

  // this will generate a event emitter and will be use everywhere
  // create a clone version so we know which one we actually is using
  var JsonqlWsEvt = /*@__PURE__*/(function (EventEmitterClass) {
    function JsonqlWsEvt(logger) {
      if (typeof logger !== 'function') {
        throw new Error("Just die here the logger is not a function!")
      }
      logger("---> Create a new EventEmitter <---");
      // this ee will always come with the logger
      // because we should take the ee from the configuration
      EventEmitterClass.call(this, { logger: logger });
    }

    if ( EventEmitterClass ) JsonqlWsEvt.__proto__ = EventEmitterClass;
    JsonqlWsEvt.prototype = Object.create( EventEmitterClass && EventEmitterClass.prototype );
    JsonqlWsEvt.prototype.constructor = JsonqlWsEvt;

    var prototypeAccessors = { name: { configurable: true } };

    prototypeAccessors.name.get = function () {
      return 'jsonql-ws-client-core'
    };

    Object.defineProperties( JsonqlWsEvt.prototype, prototypeAccessors );

    return JsonqlWsEvt;
  }(EventService));

  /**
   * getting the event emitter
   * @param {object} opts configuration
   * @return {object} the event emitter instance
   */
  var getEventEmitter = function (opts) {
    var log = opts.log;
    var eventEmitter = opts.eventEmitter;
    
    if (eventEmitter) {
      log("eventEmitter is:", eventEmitter.name);
      return eventEmitter
    }
    
    return new JsonqlWsEvt( opts.log )
  };

  // group all the small functions here

  /**
   * WebSocket is strict about the path, therefore we need to make sure before it goes in
   * @param {string} url input url
   * @return {string} url with correct path name
   */
  var fixWss = function (url) {
    var pattern = new RegExp('^(http|https)\:\/\/', 'i');
    var result = url.match(pattern);
    if (result && result.length) {
      var target = result[1].toLowerCase();
      var replacement = target === 'https' ? 'wss' : 'ws';
      
      return url.replace(pattern, replacement + '://')
    }

    return url 
  };


  /**
   * get a stock host name from browser
   */
  var getHostName = function () {
    try {
      return [window.location.protocol, window.location.host].join('//')
    } catch(e) {
      throw new JsonqlValidationError(e)
    }
  };

  /**
   * Unbind the event, this should only get call when it's disconnected
   * @param {object} ee EventEmitter
   * @param {string} namespace
   * @return {void}
   */
  var clearMainEmitEvt = function (ee, namespace) {
    var nsps = toArray(namespace);
    nsps.forEach(function (n) {
      ee.$off(createEvt(n, EMIT_REPLY_TYPE));
    });
  };

  /**
   * Setup the IS_LOGIN_PROP_KEY IS_READY_PROP_KEY 
   * @param {*} client 
   * @return {*} client 
   */
  function setupStatePropKeys(client) {
    return [IS_READY_PROP_KEY, IS_LOGIN_PROP_KEY]
      // .map(key => injectToFn(client, key, false, true))
      .reduce(function (c, key) {
        return injectToFn(c, key, false, true)
      }, client)
  }

  // constants

  var EMIT_EVT = EMIT_REPLY_TYPE;

  var UNKNOWN_RESULT = 'UKNNOWN RESULT!';

  var MY_NAMESPACE = 'myNamespace';

  // breaking it up further to share between methods

  /**
   * break out to use in different places to handle the return from server
   * @param {object} data from server
   * @param {function} resolver NOT from promise
   * @param {function} rejecter NOT from promise
   * @return {void} nothing
   */
  function respondHandler(data, resolver, rejecter) {
    if (isObjectHasKey(data, ERROR_KEY)) {
      // debugFn('-- rejecter called --', data[ERROR_KEY])
      rejecter(data[ERROR_KEY]);
    } else if (isObjectHasKey(data, DATA_KEY)) {
      // debugFn('-- resolver called --', data[DATA_KEY])
      // @NOTE we change from calling it directly to use reflect 
      // this could have another problem later when the return data is no in an array structure
      Reflect.apply(resolver, null, [].concat( data[DATA_KEY] ));
    } else {
      // debugFn('-- UNKNOWN_RESULT --', data)
      rejecter({message: UNKNOWN_RESULT, error: data});
    }
  }

  // the actual trigger call method

  /**
   * just wrapper
   * @param {object} ee EventEmitter
   * @param {string} namespace where this belongs
   * @param {string} resolverName resolver
   * @param {array} args arguments
   * @param {function} log function 
   * @return {void} nothing
   */
  function actionCall(ee, namespace, resolverName, args, log) {
    if ( args === void 0 ) args = [];

    // reply event 
    var outEventName = createEvt(namespace, EMIT_REPLY_TYPE);

    log(("actionCall: " + outEventName + " --> " + resolverName), args);
    // This is the out going call 
    ee.$trigger(outEventName, [resolverName, toArray(args)]);
    
    // then we need to listen to the event callback here as well
    return new Promise(function (resolver, rejecter) {
      var inEventName = createEvt(namespace, resolverName, ON_RESULT_FN_NAME);
      // this cause the onResult got the result back first 
      // and it should be the promise resolve first
      // @TODO we need to rewrote the respondHandler to change the problem stated above 
      ee.$on(
        inEventName,
        function actionCallResultHandler(result) {
          log("got the first result", result);
          respondHandler(result, resolver, rejecter);
        }
      );
    })
  }

  // setting up the send method 

  /** 
   * pairing with the server vesrion SEND_MSG_FN_NAME
   * last of the chain so only return the resolver (fn)
   * This is now change to a getter / setter method 
   * and call like this: resolver.send(...args)
   * @param {function} fn the resolver function 
   * @param {object} ee event emitter instance 
   * @param {string} namespace the namespace it belongs to 
   * @param {string} resolverName name of the resolver 
   * @param {object} params from contract 
   * @param {function} log a logger function
   * @return {function} return the resolver itself 
   */ 
  var setupSendMethod = function (fn, ee, namespace, resolverName, params, log) { return (
    objDefineProps(
      fn, 
      SEND_MSG_FN_NAME, 
      nil, 
      function sendHandler() {
        log("running call getter method");
        // let _log = (...args) => Reflect.apply(console.info, console, ['[SEND]'].concat(args))
        /** 
         * This will follow the same pattern like the resolver 
         * @param {array} args list of unknown argument follow the resolver 
         * @return {promise} resolve the result 
         */
        return function sendCallback() {
          var args = [], len = arguments.length;
          while ( len-- ) args[ len ] = arguments[ len ];

          return validateAsync$1(args, params.params, true)
            .then(function (_args) {
              // @TODO check the result 
              // because the validation could failed with the list of fail properties 
              log('execute send', namespace, resolverName, _args);
              return actionCall(ee, namespace, resolverName, _args, log)
            })
            .catch(function (err) {
              // @TODO it shouldn't be just a validation error 
              // it could be server return error, so we need to check 
              // what error we got back here first 
              log('send error', err);
              // @TODO it might not an validation error need the finalCatch here
              ee.$call(
                createEvt(namespace, resolverName, ON_ERROR_FN_NAME),
                [new JsonqlValidationError(resolverName, err)]
              );
            })
        }    
      })
  ); };

  // break up the original setup resolver method here


  /**
   * moved back from generator-methods  
   * create the actual function to send message to server
   * @param {object} ee EventEmitter instance
   * @param {string} namespace this resolver end point
   * @param {string} resolverName name of resolver as event name
   * @param {object} params from contract
   * @param {function} log pass the log function
   * @return {function} resolver
   */
  function createResolver(ee, namespace, resolverName, params, log) {
    // note we pass the new withResult=true option
    return function resolver() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      return validateAsync$1(args, params.params, true)
        .then(function (_args) { return actionCall(ee, namespace, resolverName, _args, log); })
        .catch(finalCatch)
    }
  }

  /**
   * The first one in the chain, just setup a namespace prop
   * the rest are passing through
   * @param {function} fn the resolver function
   * @param {object} ee the event emitter
   * @param {string} resolverName what it said
   * @param {object} params for resolver from contract
   * @param {function} log the logger function
   * @return {array}
   */
  var setupNamespace = function (fn, ee, namespace, resolverName, params, log) { return [
    injectToFn(fn, MY_NAMESPACE, namespace),
    ee,
    namespace,
    resolverName,
    params,
    log
  ]; };

  /**
   * onResult handler
   */
  var setupOnResult = function (fn, ee, namespace, resolverName, params, log) { return [
    objDefineProps(fn, ON_RESULT_FN_NAME, function(resultCallback) {
      if (isFunc(resultCallback)) {
        ee.$on(
          createEvt(namespace, resolverName, ON_RESULT_FN_NAME),
          function resultHandler(result) {
            respondHandler(result, resultCallback, function (error) {
              log(("Catch error: \"" + resolverName + "\""), error);
              ee.$trigger(
                createEvt(namespace, resolverName, ON_ERROR_FN_NAME),
                error
              );
            });
          }
        );
      }
    }),
    ee,
    namespace,
    resolverName,
    params,
    log
  ]; };

  /**
   * we do need to add the send prop back because it's the only way to deal with
   * bi-directional data stream
   */
  var setupOnMessage = function (fn, ee, namespace, resolverName, params, log) { return [
    objDefineProps(fn, ON_MESSAGE_FN_NAME, function(messageCallback) {
      // we expect this to be a function
      if (isFunc(messageCallback)) {
        // did that add to the callback
        var onMessageCallback = function (args) {
          log("onMessageCallback", args);
          respondHandler(
            args, 
            messageCallback, 
            function (error) {
              log(("Catch error: \"" + resolverName + "\""), error);
              ee.$trigger(
                createEvt(namespace, resolverName, ON_ERROR_FN_NAME),
                error
              );
            });
          };
        // register the handler for this message event
        ee.$only(
          createEvt(namespace, resolverName, ON_MESSAGE_FN_NAME),
          onMessageCallback
        );
      }
    }),
    ee,
    namespace,
    resolverName,
    params,
    log
  ]; };

  /**
   * ON_ERROR_FN_NAME handler
   */
  var setupOnError = function (fn, ee, namespace, resolverName, params, log) { return [
    objDefineProps(fn, ON_ERROR_FN_NAME, function(resolverErrorHandler) {
      if (isFunc(resolverErrorHandler)) {
        // please note ON_ERROR_FN_NAME can add multiple listners
        ee.$only(
          createEvt(namespace, resolverName, ON_ERROR_FN_NAME),
          resolverErrorHandler
        );
      }
    }),
    ee,
    namespace,
    resolverName,
    params,
    log
  ]; };

  /**
   * Add extra property / listeners to the resolver
   * @param {string} namespace where this belongs
   * @param {string} resolverName name as event name
   * @param {object} params from contract
   * @param {function} fn resolver function
   * @param {object} ee EventEmitter
   * @param {function} log function
   * @return {function} resolver
   */  
  function setupResolver(namespace, resolverName, params, fn, ee, log) {
    var fns = [
      setupNamespace,
      setupOnResult,
      setupOnMessage,
      setupOnError,
      setupSendMethod
    ];
    var executor = Reflect.apply(chainFns, null, fns);
    // get the executor
    return executor(fn, ee, namespace, resolverName, params, log)
  }

  // put all the resolver related methods here to make it more clear

  /**
   * step one get the clientmap with the namespace
   * @param {object} opts configuration
   * @param {object} ee EventEmitter
   * @param {object} nspGroup resolvers index by their namespace
   * @return {promise} resolve the clientmapped, and start the chain
   */
  function generateResolvers(opts, ee, nspGroup) {
    var log = opts.log;
    var client= {};
    
    for (var namespace in nspGroup) {
      var list = nspGroup[namespace];
      for (var resolverName in list) {
        // resolverNames.push(resolverName)
        var params = list[resolverName];
        var fn = createResolver(ee, namespace, resolverName, params, log);
        // this should set as a getter therefore can not be overwrite by accident
        client = injectToFn(
          client,
          resolverName,
          setupResolver(namespace, resolverName, params, fn, ee, log)
        );
      }
    }
    // resolve the clientto start the chain
    // chain the result to allow the chain processing
    return [ client, opts, ee, nspGroup ]
  }

  // move from generator-methods 

  /**
   * This event will fire when the socket.io.on('connection') and ws.onopen
   * @param {object} client  client itself
   * @param {object} opts configuration
   * @param {object} ee Event Emitter
   * @return {array} [ obj, opts, ee ]
   */
  function setupOnReadyListener(client, opts, ee) {
    return [
      objDefineProps(
        client,
        ON_READY_FN_NAME,
        function onReadyCallbackHandler(onReadyCallback) {
          if (isFunc(onReadyCallback)) {
            // reduce it down to just one flat level
            // @2020-03-19 only allow ONE onReady callback otherwise
            // it will get fire multiple times which is not what we want
            ee.$only(ON_READY_FN_NAME, onReadyCallback);
          }
        }
      ),
      opts,
      ee
    ]
  }

  /**
   * The problem is the namespace can have more than one
   * and we only have on onError message
   * @param {object} clientthe client itself
   * @param {object} opts configuration
   * @param {object} ee Event Emitter
   * @param {object} nspGroup namespace keys
   * @return {array} [obj, opts, ee]
   */
  function setupNamespaceErrorListener(client, opts, ee, nspGroup) {
    return [
      objDefineProps(
        client,
        ON_ERROR_FN_NAME,
        function namespaceErrorCallbackHandler(namespaceErrorHandler) {
          if (isFunc(namespaceErrorHandler)) {
            // please note ON_ERROR_FN_NAME can add multiple listners
            for (var namespace in nspGroup) {
              // this one is very tricky, we need to make sure the trigger is calling
              // with the namespace as well as the error
              ee.$on(createEvt(namespace, ON_ERROR_FN_NAME), namespaceErrorHandler);
            }
          }
        }
      ),
      opts,
      ee
    ]
  }

  // take out from the resolver-methods
  // import { validateAsync } from 'jsonql-params-validator'

  /**
   * @UPDATE it might be better if we decoup the two http-client only emit a login event
   * Here should catch it and reload the ws client @TBC
   * break out from createAuthMethods to allow chaining call
   * @TODO when this is using the standalone mode then this will have to be disable
   * or delegated to the login method from the contract
   * @param {object} obj the main client object
   * @param {object} opts configuration
   * @param {object} ee event emitter
   * @return {array} [ obj, opts, ee ] what comes in what goes out
   */
  var setupLoginHandler = function (obj, opts, ee) { return [
    injectToFn(
      obj, 
      opts.loginHandlerName, 
      function loginHandler(token) {
        if (token && isString$1(token)) {
          opts.log(("Received " + LOGIN_EVENT_NAME + " with " + token));
          // @TODO add the interceptor hook
          return ee.$trigger(LOGIN_EVENT_NAME, [token])
        }
        // should trigger a global error instead @TODO
        throw new JsonqlValidationError(opts.loginHandlerName, ("Unexpected token " + token))
      }
    ),
    opts,
    ee
  ]; };

  /**
   * Switch to this one when standalone mode is enable 
   * @NOTE we don't actually have this because the public contract not included it
   * so we need to figure out a different way to include the auth method 
   * that get expose to the public before we can continue with this standalone mode
   * const { contract, loginHandlerName } = opts 
   * const params = contract[SOCKET_AUTH_NAME][loginHandlerName]
   * @param {*} obj 
   * @param {*} opts 
   * @param {*} ee 
   */
  var setupStandaloneLoginHandler = function (obj, opts, ee) { return [
    injectToFn(
      obj, 
      loginHandlerName,
      function standaloneLoginHandler() {
        var args = [], len = arguments.length;
        while ( len-- ) args[ len ] = arguments[ len ];

        // we only need to pass the argument 
        // let the listener to handle the rest
        ee.$trigger(LOGIN_EVENT_NAME, args);
      }
    ),
    opts,
    ee 
  ]; };

  /**
   * break out from createAuthMethods to allow chaining call - final in chain
   * @param {object} obj the main client object
   * @param {object} opts configuration
   * @param {object} ee event emitter
   * @return {array} [ obj, opts, ee ] what comes in what goes out
   */
  var setupLogoutHandler = function (obj, opts, ee) { return [
    injectToFn(
      obj, 
      opts.logoutHandlerName, 
      function logoutHandler() {
        var args = [], len = arguments.length;
        while ( len-- ) args[ len ] = arguments[ len ];

        opts[IS_LOGIN_PROP_KEY] = true;
        ee.$trigger(LOGOUT_EVENT_NAME$1, args);
      }
    ),
    opts,
    ee
  ]; };

  /**
   * This event will fire when the socket.io.on('connection') and ws.onopen
   * Plus this will check if it's the private namespace that fired the event
   * @param {object} obj the client itself
   * @param {object} ee Event Emitter
   * @return {array} [ obj, opts, ee] what comes in what goes out
   */
  var setupOnLoginListener = function (obj, opts, ee) { return [
    objDefineProps(
      obj, 
      ON_LOGIN_FN_NAME, 
      function onLoginCallbackHandler(onLoginCallback) {
        if (isFunc(onLoginCallback)) {
          // only one callback can registered with it, TBC
          // Should this be a $onlyOnce listener after the logout 
          // we add it back? 
          ee.$only(ON_LOGIN_FN_NAME, onLoginCallback);
        }
      }
    ),
    opts,
    ee
  ]; };

  /**
   * Main Create auth related methods
   * @param {object} obj the client itself
   * @param {object} opts configuration
   * @param {object} ee Event Emitter
   * @return {array} [ obj, opts, ee ] what comes in what goes out
   */
  function setupAuthMethods(obj, opts, ee) {
    return chainFns(
      opts[STANDALONE_PROP_KEY] === true ? setupStandaloneLoginHandler : setupLoginHandler, 
      setupLogoutHandler,
      setupOnLoginListener
    )(obj, opts, ee)
  }

  // this is a new method that will create several

  /**
   * Set up the CONNECTED_PROP_KEY to the client
   * @param {*} client 
   * @param {*} opts 
   * @param {*} ee 
   */
  function setupConnectPropKey(client, opts, ee) {
    var log = opts.log; 
    log('[1] setupConnectPropKey');
      // we just inject a helloWorld method here
    // set up the init state of the connect
    client = injectToFn(client, CONNECTED_PROP_KEY , false, true);
    return [ client, opts, ee ]
  }


  /**
   * setup listener to the connect event 
   * @param {*} client 
   * @param {*} opts 
   * @param {*} ee 
   */
  function setupConnectEvtListener(client, opts, ee) {
    // @TODO do what at this point?
    var log = opts.log; 

    log("[2] setupConnectEvtListener");

    ee.$on(CONNECT_EVENT_NAME, function() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      log("setupConnectEvtListener pass and do nothing at the moment", args);
    });
    
    return [client, opts, ee]
  }

  /**
   * setup listener to the connected event 
   * @param {*} client 
   * @param {*} opts 
   * @param {*} ee 
   */
  function setupConnectedEvtListener(client, opts, ee) {
    var log = opts.log; 

    log("[3] setupConnectedEvtListener");

    ee.$on(CONNECTED_EVENT_NAME, function() {
      var obj;

      client[CONNECTED_PROP_KEY] = true;
      // new action to take release the holded event queue 
      var ctn = ee.$release();

      log("CONNECTED_EVENT_NAME", true, 'queue count', ctn);

    return ( obj = {}, obj[CONNECTED_PROP_KEY] = true, obj )
    });

    return [client, opts, ee]
  }

  /**
   * Listen to the disconnect event and set the property to the client 
   * @param {*} client 
   * @param {*} opts 
   * @param {*} ee 
   */
  function setupDisconnectListener(client, opts, ee) {
    var log = opts.log; 

    log("[4] setupDisconnectListener");

    ee.$on(DISCONNECT_EVENT_NAME, function() {
      var obj;

      client[CONNECTED_PROP_KEY] = false;
      log("CONNECTED_EVENT_NAME", false);

    return ( obj = {}, obj[CONNECTED_PROP_KEY] = false, obj )
    });

    return [client, opts, ee]
  }

  /**
   * disconnect action
   * @param {*} client 
   * @param {*} opts 
   * @param {*} ee 
   * @return {object} this is the final step to return the client
   */
  function setupDisconectAction(client, opts, ee) {
    var disconnectHandlerName = opts.disconnectHandlerName;
    var log = opts.log;
    log("[5] setupDisconectAction");

    return injectToFn(
      client,
      disconnectHandlerName,
      function disconnectHandler() {
        var args = [], len = arguments.length;
        while ( len-- ) args[ len ] = arguments[ len ];

        ee.$trigger(DISCONNECT_EVENT_NAME, args);
      }
    )
  }

  /**
   * this is the new method that setup the intercom handler
   * also this serve as the final call in the then chain to
   * output the client
   * @param {object} client the client
   * @param {object} opts configuration
   * @param {object} ee the event emitter
   * @return {object} client
   */
  function setupInterCom(client, opts, ee) {
    var fns = [
      setupConnectPropKey,
      setupConnectEvtListener,
      setupConnectedEvtListener,
      setupDisconnectListener,
      setupDisconectAction
    ];

    var executor = Reflect.apply(chainFns, null, fns);
    return executor(client, opts, ee)
  }

  // The final step of the setup before it returns the client


  /**
   * The final step to return the client
   * @param {object} obj the client
   * @param {object} opts configuration
   * @param {object} ee the event emitter
   * @return {object} client
   */
  function setupFinalStep(obj, opts, ee) {
    
    var client = setupInterCom(obj, opts, ee);
    // opts.log(`---> The final step to return the ws-client <---`)
    // add some debug functions
    client.verifyEventEmitter = function () { return ee.is; };
    // we add back the two things into the client
    // then when we do integration, we run it in reverse,
    // create the ws client first then the host client
    client.eventEmitter = opts.eventEmitter;
    client.log = opts.log;

    // now at this point, we are going to call the connect event
    ee.$trigger(CONNECT_EVENT_NAME, [opts, ee]); // just passing back the entire opts object
    // also we can release the queue here 
    if (opts[SUSPEND_EVENT_PROP_KEY] === true) {
      opts.$releaseNamespace();
    }

    // finally inject some prop keys to the client 
    return setupStatePropKeys(obj)
  }

  // resolvers generator

  /**
   * This is the starting point of binding callable method to the 
   * ws, and we have to determine which is actually active 
   * if enableAuth but we don't have a token, when the connection 
   * to ws establish we only release the public related event 
   * and the private one remain hold until the LOGIN_EVENT get trigger
   * @param {object} opts configuration
   * @param {object} nspMap resolvers index by their namespace
   * @param {object} ee EventEmitter
   * @return {object} of resolvers
   * @public
   */
  function callersGenerator(opts, nspMap, ee) {
    
    var fns = [
      generateResolvers,
      setupOnReadyListener,
      setupNamespaceErrorListener
    ];
    if (opts.enableAuth) {
      // there is a problem here, when this is a public namespace
      // it should not have a login logout event attach to it
      fns.push(setupAuthMethods);
    }
    // we will always get back the [ obj, opts, ee ]
    // then we only return the obj (wsClient)
    // This has move outside of here, into the main method
    // the reason is we could switch around the sequence much easier
    fns.push(setupFinalStep);
    // stupid reaon!!!
    var executer = Reflect.apply(chainFns, null, fns);
    // run it
    return executer(opts, ee, nspMap[NSP_GROUP])
  }

  var obj, obj$1;



  var configCheckMap = {};
  configCheckMap[STANDALONE_PROP_KEY] = createConfig$1(false, [BOOLEAN_TYPE]);
  configCheckMap[DEBUG_ON_PROP_KEY] = createConfig$1(false, [BOOLEAN_TYPE]);
  configCheckMap[LOGIN_FN_NAME_PROP_KEY] = createConfig$1(LOGIN_FN_NAME, [STRING_TYPE]);
  configCheckMap[LOGOUT_FN_NAME_PROP_KEY] = createConfig$1(LOGOUT_FN_NAME, [STRING_TYPE]);
  configCheckMap[DISCONNECT_FN_NAME_PROP_KEY] = createConfig$1(DISCONNECT_FN_NAME, [STRING_TYPE]);
  configCheckMap[SWITCH_USER_FN_NAME_PROP_KEY] = createConfig$1(SWITCH_USER_FN_NAME, [STRING_TYPE]);
  configCheckMap[HOSTNAME_PROP_KEY] = createConfig$1(false, [STRING_TYPE]);
  configCheckMap[NAMESAPCE_PROP_KEY] = createConfig$1(JSONQL_PATH, [STRING_TYPE]);
  configCheckMap[WS_OPT_PROP_KEY] = createConfig$1({}, [OBJECT_TYPE]);
  configCheckMap[CONTRACT_PROP_KEY] = createConfig$1({}, [OBJECT_TYPE], ( obj = {}, obj[CHECKER_KEY] = isContract, obj ));
  configCheckMap[ENABLE_AUTH_PROP_KEY] = createConfig$1(false, [BOOLEAN_TYPE]);
  configCheckMap[TOKEN_PROP_KEY] = createConfig$1(false, [STRING_TYPE]);
  configCheckMap[CSRF_PROP_KEY] = createConfig$1(CSRF_HEADER_KEY, [STRING_TYPE]);
  configCheckMap[SUSPEND_EVENT_PROP_KEY] = createConfig$1(false, [BOOLEAN_TYPE]);

  // socket client
  var socketCheckMap = {};
  socketCheckMap[SOCKET_TYPE_PROP_KEY] = createConfig$1(null, [STRING_TYPE], ( obj$1 = {}, obj$1[ALIAS_KEY] = SOCKET_TYPE_CLIENT_ALIAS, obj$1 ));

  var wsCoreCheckMap = Object.assign(configCheckMap, socketCheckMap);

  // constant props
  var wsCoreConstProps = {};
  wsCoreConstProps[USE_JWT_PROP_KEY] = true;
  wsCoreConstProps.log = null;
  wsCoreConstProps.eventEmitter = null;
  wsCoreConstProps.nspClient = null;
  wsCoreConstProps.nspAuthClient = null;
  wsCoreConstProps.wssPath = '';
  wsCoreConstProps.publicNamespace = PUBLIC_KEY;
  wsCoreConstProps.privateNamespace = PRIVATE_KEY;

  // create options


  /**
   * wrapper method to check this already did the pre check
   * @param {object} config user supply config
   * @param {object} defaultOptions for checking
   * @param {object} constProps user supply const props
   * @return {promise} resolve to the checked opitons
   */
  function checkConfiguration(config, defaultOptions, constProps) {
    var defaultCheckMap= Object.assign(wsCoreCheckMap, defaultOptions);
    var wsConstProps = Object.assign(wsCoreConstProps, constProps);

    return checkConfigAsync(config, defaultCheckMap, wsConstProps)
  }

  /**
   * Taking the `then` part from the method below
   * @param {object} opts
   * @return {promise} opts all done
   */
  function postCheckInjectOpts(opts) {
    
    return Promise.resolve(opts)
      .then(function (opts) {
        if (!opts.hostname) {
          opts.hostname = getHostName();
        }
        // @TODO the contract now will supply the namespace information
        // and we need to use that to group the namespace call
        
        opts.wssPath = fixWss([opts.hostname, opts.namespace].join('/'), opts.serverType);
        // get the log function here
        opts.log = getLogFn(opts);

        opts.eventEmitter = getEventEmitter(opts);
      
        return opts
      })
  }

  /**
   * Don't want to make things confusing
   * Breaking up the opts process in one place
   * then generate the necessary parameter in another step
   * @2020-3-20 here we suspend operation by it's namespace first
   * Then in the framework part, after the connection establish we release
   * the queue
   * @param {object} opts checked --> merge --> injected
   * @return {object} {opts, nspMap, ee}
   */
  function createRequiredParams(opts) {
    var nspMap = getNspInfoByConfig(opts);
    var ee = opts.eventEmitter;
    // @TODO here we are going to add suspend event to the namespace related methods
    var log = opts.log; 
    var namespaces = nspMap.namespaces;
    var publicNamespace = nspMap.publicNamespace;
    log("namespaces", namespaces);
    // next we loop the namespace and suspend all the events prefix with namespace 
    if (opts[SUSPEND_EVENT_PROP_KEY] === true) {
      // ON_READY_FN_NAME
      // ON_LOGIN_FN_NAME

      // we create this as a function then we can call it again 
      // @TODO we need to add the onReady and the onLogin in the suspend list as well
      opts.$suspendNamepsace = function () {
        var eventNames = [ON_RESULT_FN_NAME ].concat( namespaces);
        if (opts.enableAuth) {
          eventNames.push(ON_LOGIN_FN_NAME);
        }
        // [ON_READY_FN_NAME, ON_LOGIN_FN_NAME, ...namespaces]
        Reflect.apply(ee.$suspendEvent, ee, eventNames);
      };
      // then we create a new method to releas the queue 
      // we prefix it with the $ to notify this is not a jsonql part methods
      opts.$releaseNamespace = function () {
        ee.$releaseEvent(ON_RESULT_FN_NAME, publicNamespace);
      };

      if (opts.enableAuth) {
        opts.$releasePrivateNamespace = function () {
          ee.$releaseEvent(ON_LOGIN_FN_NAME, namespaces[1]);
        };
      }

      // now run it 
      opts.$suspendNamepsace();
    }
    
    return { opts: opts, nspMap: nspMap, ee: ee }
  }

  // the top level API


  /**
   * 0.5.0 we break up the wsClientCore in two parts one without the config check
   * @param {function} setupSocketClientListener just make sure what it said it does
   * @return {function} to actually generate the client
   */
  function wsClientCoreAction(setupSocketClientListener) {
    /**
     * This is a breaking change, to continue the onion skin design
     * @param {object} config the already checked config
     * @return {promise} resolve the client
     */
    return function createClientAction(config) {
      if ( config === void 0 ) config = {};


      return postCheckInjectOpts(config)
        .then(createRequiredParams)
        .then(
          function (ref) {
            var opts = ref.opts;
            var nspMap = ref.nspMap;
            var ee = ref.ee;

            return setupSocketClientListener(opts, nspMap, ee);
      }
        )
        .then(
          function (ref) {
            var opts = ref.opts;
            var nspMap = ref.nspMap;
            var ee = ref.ee;

            return callersGenerator(opts, nspMap, ee);
      }
        )
        .catch(function (err) {
          console.error("[jsonql-ws-core-client init error]", err);
        })
    }
  }

  /**
   * The main interface which will generate the socket clients and map all events
   * @param {object} socketClientListerner this is the one method export by various clients
   * @param {object} [configCheckMap={}] we should do all the checking in the core instead of the client
   * @param {object} [constProps={}] add this to supply the constProps from the downstream client
   * @return {function} accept a config then return the wsClient instance with all the available API
   */
  function wsClientCore(socketClientListener, configCheckMap, constProps) {
    if ( configCheckMap === void 0 ) configCheckMap = {};
    if ( constProps === void 0 ) constProps = {};

    // we need to inject property to this client later
    return function (config) {
      if ( config === void 0 ) config = {};

      return checkConfiguration(config, configCheckMap, constProps)
                              .then(
                                wsClientCoreAction(socketClientListener)
                              );
    }
  }

  // this use by client-event-handler

  /**
   * trigger errors on all the namespace onError handler
   * @param {object} ee Event Emitter
   * @param {array} namespaces nsps string
   * @param {string} message optional
   * @return {void}
   */
  function triggerNamespacesOnError(ee, namespaces, message) {
    namespaces.forEach( function (namespace) {
      ee.$trigger(
        createEvt(namespace, ON_ERROR_FN_NAME), 
        [{ message: message, namespace: namespace }]
      );
    });
  }

  /**
   * Handle the onerror callback 
   * @param {object} ee event emitter  
   * @param {string} namespace which namespace has error 
   * @param {*} err error object
   * @return {void} 
   */
  var handleNamespaceOnError = function (ee, namespace, err) {
    ee.$trigger(createEvt(namespace, ON_ERROR_FN_NAME), [err]);
  };

  // NOT IN USE AT THE MOMENT JUST KEEP IT HERE FOR THE TIME BEING 

  /**
   * A Event Listerner placeholder when it's not connect to the private nsp
   * @param {string} namespace nsp
   * @param {object} ee EventEmitter
   * @param {object} opts configuration
   * @return {void}
   */
  var notLoginListener = function (namespace, ee, opts) {
    var log = opts.log; 

    ee.$only(
      createEvt(namespace, EMIT_EVT),
      function notLoginListernerCallback(resolverName, args) {
        log('[notLoginListerner] hijack the ws call', namespace, resolverName, args);
        var error = { message: NOT_LOGIN_ERR_MSG };
        // It should just throw error here and should not call the result
        // because that's channel for handling normal event not the fake one
        ee.$call(createEvt(namespace, resolverName, ON_ERROR_FN_NAME), [ error ]);
        // also trigger the result Listerner, but wrap inside the error key
        ee.$call(createEvt(namespace, resolverName, ON_RESULT_FN_NAME), [{ error: error }]);
      }
    );
  };

  /**
   * Only when there is a private namespace then we bind to this event
   * @param {object} nsps the available nsp(s)
   * @param {array} namespaces available namespace 
   * @param {object} ee eventEmitter 
   * @param {object} opts configuration
   * @return {void}
   */
  var logoutEvtListener = function (nsps, namespaces, ee, opts) {
    var log = opts.log; 
    // this will be available regardless enableAuth
    // because the server can log the client out
    ee.$on(
      LOGOUT_EVENT_NAME$1, 
      function logoutEvtCallback() {
        var privateNamespace = getPrivateNamespace(namespaces);
        log((LOGOUT_EVENT_NAME$1 + " event triggered"));
        // disconnect(nsps, opts.serverType)
        // we need to issue error to all the namespace onError Listerner
        triggerNamespacesOnError(ee, [privateNamespace], LOGOUT_EVENT_NAME$1);
        // rebind all of the Listerner to the fake one
        log(("logout from " + privateNamespace));

        clearMainEmitEvt(ee, privateNamespace);
        // we need to issue one more call to the server before we disconnect
        // now this is a catch 22, here we are not suppose to do anything platform specific
        // so that should fire before trigger this event
        // clear out the nsp
        nsps[privateNamespace] = null; 
        // add a NOT LOGIN error if call
        notLoginWsListerner(privateNamespace, ee, opts);
      }
    );
  };

  // This is share between different clients so we export it

  /**
   * centralize all the comm in one place
   * @param {function} bindSocketEventHandler binding the ee to ws --> this is the core bit
   * @param {object} nsps namespaced nsp
   * @return {void} nothing
   */
  function namespaceEventListener(bindSocketEventListener, nsps) {
    /**
     * BREAKING CHANGE instead of one flat structure
     * we return a function to accept the two
     * @param {object} opts configuration
     * @param {object} nspMap this is not in the opts
     * @param {object} ee Event Emitter instance
     * @return {array} although we return something but this is the last step and nothing to do further
     */
    return function (opts, nspMap, ee) {
      // since all these params already in the opts
      var log = opts.log;
      var namespaces = nspMap.namespaces;
      // @1.1.3 add isPrivate prop to id which namespace is the private nsp
      // then we can use this prop to determine if we need to fire the ON_LOGIN_PROP_NAME event
      var privateNamespace = getPrivateNamespace$1(namespaces);
      // The total number of namespaces (which is 2 at the moment) minus the private namespace number
      var ctn = namespaces.length - 1;   
      // @NOTE we need to somehow id if this namespace already been connected 
      // and the event been released before 
      return namespaces.map(function (namespace) {
        var isPrivate = privateNamespace === namespace;
        log(namespace, (" --> " + (isPrivate ? 'private': 'public') + " nsp --> "), nsps[namespace] !== false);
        if (nsps[namespace]) {
          log('[call bindWsHandler]', isPrivate, namespace);
          // we need to add one more property here to tell the bindSocketEventListener 
          // how many times it should call the onReady 
          var args = [namespace, nsps[namespace], ee, isPrivate, opts, --ctn];
          // Finally we binding everything together
          Reflect.apply(bindSocketEventListener, null, args);
          
        } else {
          log(("binding notLoginWsHandler to " + namespace));
          // a dummy placeholder
          // @TODO but it should be a not connect handler
          // when it's not login (or fail) this should be handle differently
          notLoginListener(namespace, ee, opts);
        }
        if (isPrivate) {
          log("Has private and add logoutEvtHandler");
          logoutEvtListener(nsps, namespaces, ee, opts);
        }
        // just return something its not going to get use anywhere
        return isPrivate
      })
    }
  }

  /*
  This two client is the final one that gets call 
  all it does is to create the url that connect to 
  and actually trigger the connection and return the socket 
  therefore they are as generic as it can be
  */

  /**
   * wrapper method to create a nsp without login
   * @param {string|boolean} namespace namespace url could be false
   * @param {object} opts configuration
   * @return {object} ws client instance
   */
  function createNspClient(namespace, opts) {
    var hostname = opts.hostname;
    var wssPath = opts.wssPath;
    var nspClient = opts.nspClient;
    var log = opts.log;
    var url = namespace ? [hostname, namespace].join('/') : wssPath;
    log("createNspClient with URL --> ", url);

    return nspClient(url, opts)
  }

  /**
   * wrapper method to create a nsp with token auth
   * @param {string} namespace namespace url
   * @param {object} opts configuration
   * @return {object} ws client instance
   */
  function createNspAuthClient(namespace, opts) {
    var hostname = opts.hostname;
    var wssPath = opts.wssPath;
    var token = opts.token;
    var nspAuthClient = opts.nspAuthClient;
    var log = opts.log;
    var url = namespace ? [hostname, namespace].join('/') : wssPath;
    
    log("createNspAuthClient with URL -->", url);

    if (token && typeof token !== 'string') {
      throw new Error(("Expect token to be string, but got " + token))
    }
    // now we need to get an extra options for framework specific method, which is not great
    // instead we just pass the entrie opts to the authClient 

    return nspAuthClient(url, opts, token)
  }

  // same with the invalid-token-error 

  /*
  function InvalidCharacterError(message) {
    this.message = message;
  }

  InvalidCharacterError.prototype = new Error();
  InvalidCharacterError.prototype.name = 'InvalidCharacterError';

  */

  var InvalidCharacterError = /*@__PURE__*/(function (Error) {
    function InvalidCharacterError(message) {
      this.message = message; 
    }

    if ( Error ) InvalidCharacterError.__proto__ = Error;
    InvalidCharacterError.prototype = Object.create( Error && Error.prototype );
    InvalidCharacterError.prototype.constructor = InvalidCharacterError;

    var prototypeAccessors = { name: { configurable: true } };

    prototypeAccessors.name.get = function () {
      return 'InvalidCharacterError'
    };

    Object.defineProperties( InvalidCharacterError.prototype, prototypeAccessors );

    return InvalidCharacterError;
  }(Error));

  /**
   * The code was extracted from:
   * https://github.com/davidchambers/Base64.js
   */
  var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

  /**
   * Polyfill the non ASCII code 
   * @param {*} input
   * @return {*} usable output 
   */
  function atob(input) {
    var str = String(input).replace(/=+$/, '');
    if (str.length % 4 == 1) {
      throw new InvalidCharacterError("'atob' failed: The string to be decoded is not correctly encoded.")
    }
    for (
      // initialize result and counters
      var bc = 0, bs = (void 0), buffer = (void 0), idx = 0, output$1 = '';
      // get next character
      buffer = str.charAt(idx++);
      // character found in table? initialize bit storage and add its ascii value;
      ~buffer && (bs = bc % 4 ? bs * 64 + buffer : buffer,
        // and if not first of each 4 characters,
        // convert the first 8 bits to one ascii character
        bc++ % 4) ? output$1 += String.fromCharCode(255 & bs >> (-2 * bc & 6)) : 0
    ) {
      // try to find character in table (0-63, not found => -1)
      buffer = chars.indexOf(buffer);
    }
    return output
  }

  // polyfill the window object
  try {
    typeof window !== 'undefined' && window.atob && window.atob.bind(window) || atob;
  } catch(e) {}

  var obj$2, obj$1$1, obj$2$1, obj$3, obj$4, obj$5, obj$6, obj$7, obj$8;

  var appProps = {
    algorithm: createConfig$1(HSA_ALGO, [STRING_TYPE]),
    expiresIn: createConfig$1(false, [BOOLEAN_TYPE, NUMBER_TYPE, STRING_TYPE], ( obj$2 = {}, obj$2[ALIAS_KEY] = 'exp', obj$2[OPTIONAL_KEY] = true, obj$2 )),
    notBefore: createConfig$1(false, [BOOLEAN_TYPE, NUMBER_TYPE, STRING_TYPE], ( obj$1$1 = {}, obj$1$1[ALIAS_KEY] = 'nbf', obj$1$1[OPTIONAL_KEY] = true, obj$1$1 )),
    audience: createConfig$1(false, [BOOLEAN_TYPE, STRING_TYPE], ( obj$2$1 = {}, obj$2$1[ALIAS_KEY] = 'iss', obj$2$1[OPTIONAL_KEY] = true, obj$2$1 )),
    subject: createConfig$1(false, [BOOLEAN_TYPE, STRING_TYPE], ( obj$3 = {}, obj$3[ALIAS_KEY] = 'sub', obj$3[OPTIONAL_KEY] = true, obj$3 )),
    issuer: createConfig$1(false, [BOOLEAN_TYPE, STRING_TYPE], ( obj$4 = {}, obj$4[ALIAS_KEY] = 'iss', obj$4[OPTIONAL_KEY] = true, obj$4 )),
    noTimestamp: createConfig$1(false, [BOOLEAN_TYPE], ( obj$5 = {}, obj$5[OPTIONAL_KEY] = true, obj$5 )),
    header: createConfig$1(false, [BOOLEAN_TYPE, STRING_TYPE], ( obj$6 = {}, obj$6[OPTIONAL_KEY] = true, obj$6 )),
    keyid: createConfig$1(false, [BOOLEAN_TYPE, STRING_TYPE], ( obj$7 = {}, obj$7[OPTIONAL_KEY] = true, obj$7 )),
    mutatePayload: createConfig$1(false, [BOOLEAN_TYPE], ( obj$8 = {}, obj$8[OPTIONAL_KEY] = true, obj$8 ))
  };

  // this method is re-use in several clients 
  // move back to jsonql-constants later

  /**
   * extract the new options for authorization
   * @param {*} opts configuration
   * @return {string} the header option
   */
  function extractConfig(opts) {
    // we don't really need to do any validation here 
    // because the opts should be clean before calling here
    return opts[TOKEN_DELIVER_LOCATION_PROP_KEY] || TOKEN_IN_URL
  }

  /**
   * When running the CSRF token, and have a Auth token 
   * the csrf is missing so we need to take that into account as well
   * @param {object} config configuration
   * @param {string} token auth token 
   * @return {object} constructed the full options to pass to the WS object 
   */
  function prepareHeaderOpts(config, token) {
    var obj, obj$1;

    if ( token === void 0 ) token = false;
    var wsOptions = config[WS_OPT_PROP_KEY] || {};
    var headers = config[HEADERS_KEY] || {};
    if (token) {
      headers = Object.assign(headers, ( obj = {}, obj[AUTH_HEADER] = (BEARER + " " + token), obj ));
    }
    // we might have to use the merge here
    return Object.assign({}, wsOptions, ( obj$1 = {}, obj$1[HEADERS_KEY] = headers, obj$1 ))
  }

  /**
   * prepare the url and options to the WebSocket
   * @param {*} url 
   * @param {*} config 
   * @param {*} [token = false] 
   * @return {object} with url and opts key  
   */
  function prepareConnectConfig(url, config, token) {
    if ( config === void 0 ) config = {};
    if ( token === void 0 ) token = false;

    var tokenOpt = extractConfig(config);

    switch (tokenOpt) {
      case TOKEN_IN_URL:
        return {
          url: token ? (url + "?" + TOKEN_PARAM_NAME + "=" + token) : url,
          opts: prepareHeaderOpts(config, false)
        }
      case TOKEN_IN_HEADER:
      default: 
        return {
          url: url,
          opts: prepareHeaderOpts(config, token)
        }
    }
  }

  /* base.js */
  var ERROR_KEY$1 = 'error';
  var QUERY_ARG_NAME$1 = 'args';
  var TIMESTAMP_PARAM_NAME$1 = 'TS';

  /* prop.js */

  // this is all the key name for the config check map
  // all subfix with prop_key

  var TYPE_KEY$1 = 'type';
  var OPTIONAL_KEY$1 = 'optional';
  var ENUM_KEY$1 = 'enumv';  // need to change this because enum is a reserved word
  var ARGS_KEY$1 = 'args';
  var CHECKER_KEY$1 = 'checker';
  var ALIAS_KEY$1 = 'alias';
  var ENABLE_AUTH_PROP_KEY$1 = 'enableAuth';
  // we could pass the token in the header instead when init the WebSocket 
  var TOKEN_DELIVER_LOCATION_PROP_KEY$1 = 'tokenDeliverLocation';
  var LOGIN_EVENT_NAME$1 = '__login__';
  // at the moment we only have __logout__ regardless enableAuth is enable
  // this is incorrect, because logout suppose to come after login
  // and it should only logout from auth nsp, instead of clear out the
  // connection, the following new event @1.9.2 will correct this edge case
  // although it should never happens, but in some edge case might want to
  // disconnect from the current server, then re-establish connection later
  var CONNECT_EVENT_NAME$1 = '__connect__';
  var DISCONNECT_EVENT_NAME$1 = '__disconnect__';
  // for ws servers
  var WS_REPLY_TYPE$1 = '__reply__';
  var WS_EVT_NAME$1 = '__event__';
  var WS_DATA_NAME$1 = '__data__';

  // for ws client, 1.9.3 breaking change to name them as FN instead of PROP
  var ON_MESSAGE_FN_NAME$1 = 'onMessage';
  var ON_RESULT_FN_NAME$1 = 'onResult'; // this will need to be internal from now on
  var ON_ERROR_FN_NAME$1 = 'onError';
  var ON_READY_FN_NAME$1 = 'onReady';
  var ON_LOGIN_FN_NAME$1 = 'onLogin'; // new @1.8.6

  // this is somewhat vague about what is suppose to do
  var EMIT_REPLY_TYPE$1 = 'emit_reply';
  var ACKNOWLEDGE_REPLY_TYPE = 'emit_acknowledge';
  var JS_WS_NAME = 'ws';

  var NSP_AUTH_CLIENT = 'nspAuthClient';
  var NSP_CLIENT = 'nspClient';

  // this is the value for TOKEN_DELIVER_LOCATION_PROP_KEY
  var TOKEN_IN_HEADER$1 = 'header';
  var TOKEN_IN_URL$1 = 'url';
  var STRING_TYPE$1 = 'string';
  var BOOLEAN_TYPE$1 = 'boolean';

  var NUMBER_TYPE$1 = 'number';

  /** Detect free variable `global` from Node.js. */
  var freeGlobal$1 = typeof global == 'object' && global && global.Object === Object && global;

  /** Detect free variable `self`. */
  var freeSelf$1 = typeof self == 'object' && self && self.Object === Object && self;

  /** Used as a reference to the global object. */
  var root$1 = freeGlobal$1 || freeSelf$1 || Function('return this')();

  /** Built-in value references. */
  var Symbol$1 = root$1.Symbol;

  /**
   * A specialized version of `_.map` for arrays without support for iteratee
   * shorthands.
   *
   * @private
   * @param {Array} [array] The array to iterate over.
   * @param {Function} iteratee The function invoked per iteration.
   * @returns {Array} Returns the new mapped array.
   */
  function arrayMap$1(array, iteratee) {
    var index = -1,
        length = array == null ? 0 : array.length,
        result = Array(length);

    while (++index < length) {
      result[index] = iteratee(array[index], index, array);
    }
    return result;
  }

  /**
   * Checks if `value` is classified as an `Array` object.
   *
   * @static
   * @memberOf _
   * @since 0.1.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is an array, else `false`.
   * @example
   *
   * _.isArray([1, 2, 3]);
   * // => true
   *
   * _.isArray(document.body.children);
   * // => false
   *
   * _.isArray('abc');
   * // => false
   *
   * _.isArray(_.noop);
   * // => false
   */
  var isArray$1 = Array.isArray;

  /** Used for built-in method references. */
  var objectProto$f = Object.prototype;

  /** Used to check objects for own properties. */
  var hasOwnProperty$c = objectProto$f.hasOwnProperty;

  /**
   * Used to resolve the
   * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
   * of values.
   */
  var nativeObjectToString$2 = objectProto$f.toString;

  /** Built-in value references. */
  var symToStringTag$2 = Symbol$1 ? Symbol$1.toStringTag : undefined;

  /**
   * A specialized version of `baseGetTag` which ignores `Symbol.toStringTag` values.
   *
   * @private
   * @param {*} value The value to query.
   * @returns {string} Returns the raw `toStringTag`.
   */
  function getRawTag$1(value) {
    var isOwn = hasOwnProperty$c.call(value, symToStringTag$2),
        tag = value[symToStringTag$2];

    try {
      value[symToStringTag$2] = undefined;
      var unmasked = true;
    } catch (e) {}

    var result = nativeObjectToString$2.call(value);
    if (unmasked) {
      if (isOwn) {
        value[symToStringTag$2] = tag;
      } else {
        delete value[symToStringTag$2];
      }
    }
    return result;
  }

  /** Used for built-in method references. */
  var objectProto$g = Object.prototype;

  /**
   * Used to resolve the
   * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
   * of values.
   */
  var nativeObjectToString$3 = objectProto$g.toString;

  /**
   * Converts `value` to a string using `Object.prototype.toString`.
   *
   * @private
   * @param {*} value The value to convert.
   * @returns {string} Returns the converted string.
   */
  function objectToString$1(value) {
    return nativeObjectToString$3.call(value);
  }

  /** `Object#toString` result references. */
  var nullTag$1 = '[object Null]',
      undefinedTag$1 = '[object Undefined]';

  /** Built-in value references. */
  var symToStringTag$3 = Symbol$1 ? Symbol$1.toStringTag : undefined;

  /**
   * The base implementation of `getTag` without fallbacks for buggy environments.
   *
   * @private
   * @param {*} value The value to query.
   * @returns {string} Returns the `toStringTag`.
   */
  function baseGetTag$1(value) {
    if (value == null) {
      return value === undefined ? undefinedTag$1 : nullTag$1;
    }
    return (symToStringTag$3 && symToStringTag$3 in Object(value))
      ? getRawTag$1(value)
      : objectToString$1(value);
  }

  /**
   * Checks if `value` is object-like. A value is object-like if it's not `null`
   * and has a `typeof` result of "object".
   *
   * @static
   * @memberOf _
   * @since 4.0.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
   * @example
   *
   * _.isObjectLike({});
   * // => true
   *
   * _.isObjectLike([1, 2, 3]);
   * // => true
   *
   * _.isObjectLike(_.noop);
   * // => false
   *
   * _.isObjectLike(null);
   * // => false
   */
  function isObjectLike$1(value) {
    return value != null && typeof value == 'object';
  }

  /** `Object#toString` result references. */
  var symbolTag$2 = '[object Symbol]';

  /**
   * Checks if `value` is classified as a `Symbol` primitive or object.
   *
   * @static
   * @memberOf _
   * @since 4.0.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a symbol, else `false`.
   * @example
   *
   * _.isSymbol(Symbol.iterator);
   * // => true
   *
   * _.isSymbol('abc');
   * // => false
   */
  function isSymbol$1(value) {
    return typeof value == 'symbol' ||
      (isObjectLike$1(value) && baseGetTag$1(value) == symbolTag$2);
  }

  /** Used as references for various `Number` constants. */
  var INFINITY$2 = 1 / 0;

  /** Used to convert symbols to primitives and strings. */
  var symbolProto$2 = Symbol$1 ? Symbol$1.prototype : undefined,
      symbolToString$1 = symbolProto$2 ? symbolProto$2.toString : undefined;

  /**
   * The base implementation of `_.toString` which doesn't convert nullish
   * values to empty strings.
   *
   * @private
   * @param {*} value The value to process.
   * @returns {string} Returns the string.
   */
  function baseToString$1(value) {
    // Exit early for strings to avoid a performance hit in some environments.
    if (typeof value == 'string') {
      return value;
    }
    if (isArray$1(value)) {
      // Recursively convert values (susceptible to call stack limits).
      return arrayMap$1(value, baseToString$1) + '';
    }
    if (isSymbol$1(value)) {
      return symbolToString$1 ? symbolToString$1.call(value) : '';
    }
    var result = (value + '');
    return (result == '0' && (1 / value) == -INFINITY$2) ? '-0' : result;
  }

  /**
   * The base implementation of `_.slice` without an iteratee call guard.
   *
   * @private
   * @param {Array} array The array to slice.
   * @param {number} [start=0] The start position.
   * @param {number} [end=array.length] The end position.
   * @returns {Array} Returns the slice of `array`.
   */
  function baseSlice$1(array, start, end) {
    var index = -1,
        length = array.length;

    if (start < 0) {
      start = -start > length ? 0 : (length + start);
    }
    end = end > length ? length : end;
    if (end < 0) {
      end += length;
    }
    length = start > end ? 0 : ((end - start) >>> 0);
    start >>>= 0;

    var result = Array(length);
    while (++index < length) {
      result[index] = array[index + start];
    }
    return result;
  }

  /**
   * Casts `array` to a slice if it's needed.
   *
   * @private
   * @param {Array} array The array to inspect.
   * @param {number} start The start position.
   * @param {number} [end=array.length] The end position.
   * @returns {Array} Returns the cast slice.
   */
  function castSlice$1(array, start, end) {
    var length = array.length;
    end = end === undefined ? length : end;
    return (!start && end >= length) ? array : baseSlice$1(array, start, end);
  }

  /**
   * The base implementation of `_.findIndex` and `_.findLastIndex` without
   * support for iteratee shorthands.
   *
   * @private
   * @param {Array} array The array to inspect.
   * @param {Function} predicate The function invoked per iteration.
   * @param {number} fromIndex The index to search from.
   * @param {boolean} [fromRight] Specify iterating from right to left.
   * @returns {number} Returns the index of the matched value, else `-1`.
   */
  function baseFindIndex$1(array, predicate, fromIndex, fromRight) {
    var length = array.length,
        index = fromIndex + (fromRight ? 1 : -1);

    while ((fromRight ? index-- : ++index < length)) {
      if (predicate(array[index], index, array)) {
        return index;
      }
    }
    return -1;
  }

  /**
   * The base implementation of `_.isNaN` without support for number objects.
   *
   * @private
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is `NaN`, else `false`.
   */
  function baseIsNaN$1(value) {
    return value !== value;
  }

  /**
   * A specialized version of `_.indexOf` which performs strict equality
   * comparisons of values, i.e. `===`.
   *
   * @private
   * @param {Array} array The array to inspect.
   * @param {*} value The value to search for.
   * @param {number} fromIndex The index to search from.
   * @returns {number} Returns the index of the matched value, else `-1`.
   */
  function strictIndexOf$1(array, value, fromIndex) {
    var index = fromIndex - 1,
        length = array.length;

    while (++index < length) {
      if (array[index] === value) {
        return index;
      }
    }
    return -1;
  }

  /**
   * The base implementation of `_.indexOf` without `fromIndex` bounds checks.
   *
   * @private
   * @param {Array} array The array to inspect.
   * @param {*} value The value to search for.
   * @param {number} fromIndex The index to search from.
   * @returns {number} Returns the index of the matched value, else `-1`.
   */
  function baseIndexOf$1(array, value, fromIndex) {
    return value === value
      ? strictIndexOf$1(array, value, fromIndex)
      : baseFindIndex$1(array, baseIsNaN$1, fromIndex);
  }

  /**
   * Used by `_.trim` and `_.trimEnd` to get the index of the last string symbol
   * that is not found in the character symbols.
   *
   * @private
   * @param {Array} strSymbols The string symbols to inspect.
   * @param {Array} chrSymbols The character symbols to find.
   * @returns {number} Returns the index of the last unmatched string symbol.
   */
  function charsEndIndex$1(strSymbols, chrSymbols) {
    var index = strSymbols.length;

    while (index-- && baseIndexOf$1(chrSymbols, strSymbols[index], 0) > -1) {}
    return index;
  }

  /**
   * Used by `_.trim` and `_.trimStart` to get the index of the first string symbol
   * that is not found in the character symbols.
   *
   * @private
   * @param {Array} strSymbols The string symbols to inspect.
   * @param {Array} chrSymbols The character symbols to find.
   * @returns {number} Returns the index of the first unmatched string symbol.
   */
  function charsStartIndex$1(strSymbols, chrSymbols) {
    var index = -1,
        length = strSymbols.length;

    while (++index < length && baseIndexOf$1(chrSymbols, strSymbols[index], 0) > -1) {}
    return index;
  }

  /**
   * Converts an ASCII `string` to an array.
   *
   * @private
   * @param {string} string The string to convert.
   * @returns {Array} Returns the converted array.
   */
  function asciiToArray$1(string) {
    return string.split('');
  }

  /** Used to compose unicode character classes. */
  var rsAstralRange$2 = '\\ud800-\\udfff',
      rsComboMarksRange$2 = '\\u0300-\\u036f',
      reComboHalfMarksRange$2 = '\\ufe20-\\ufe2f',
      rsComboSymbolsRange$2 = '\\u20d0-\\u20ff',
      rsComboRange$2 = rsComboMarksRange$2 + reComboHalfMarksRange$2 + rsComboSymbolsRange$2,
      rsVarRange$2 = '\\ufe0e\\ufe0f';

  /** Used to compose unicode capture groups. */
  var rsZWJ$2 = '\\u200d';

  /** Used to detect strings with [zero-width joiners or code points from the astral planes](http://eev.ee/blog/2015/09/12/dark-corners-of-unicode/). */
  var reHasUnicode$1 = RegExp('[' + rsZWJ$2 + rsAstralRange$2  + rsComboRange$2 + rsVarRange$2 + ']');

  /**
   * Checks if `string` contains Unicode symbols.
   *
   * @private
   * @param {string} string The string to inspect.
   * @returns {boolean} Returns `true` if a symbol is found, else `false`.
   */
  function hasUnicode$1(string) {
    return reHasUnicode$1.test(string);
  }

  /** Used to compose unicode character classes. */
  var rsAstralRange$3 = '\\ud800-\\udfff',
      rsComboMarksRange$3 = '\\u0300-\\u036f',
      reComboHalfMarksRange$3 = '\\ufe20-\\ufe2f',
      rsComboSymbolsRange$3 = '\\u20d0-\\u20ff',
      rsComboRange$3 = rsComboMarksRange$3 + reComboHalfMarksRange$3 + rsComboSymbolsRange$3,
      rsVarRange$3 = '\\ufe0e\\ufe0f';

  /** Used to compose unicode capture groups. */
  var rsAstral$1 = '[' + rsAstralRange$3 + ']',
      rsCombo$1 = '[' + rsComboRange$3 + ']',
      rsFitz$1 = '\\ud83c[\\udffb-\\udfff]',
      rsModifier$1 = '(?:' + rsCombo$1 + '|' + rsFitz$1 + ')',
      rsNonAstral$1 = '[^' + rsAstralRange$3 + ']',
      rsRegional$1 = '(?:\\ud83c[\\udde6-\\uddff]){2}',
      rsSurrPair$1 = '[\\ud800-\\udbff][\\udc00-\\udfff]',
      rsZWJ$3 = '\\u200d';

  /** Used to compose unicode regexes. */
  var reOptMod$1 = rsModifier$1 + '?',
      rsOptVar$1 = '[' + rsVarRange$3 + ']?',
      rsOptJoin$1 = '(?:' + rsZWJ$3 + '(?:' + [rsNonAstral$1, rsRegional$1, rsSurrPair$1].join('|') + ')' + rsOptVar$1 + reOptMod$1 + ')*',
      rsSeq$1 = rsOptVar$1 + reOptMod$1 + rsOptJoin$1,
      rsSymbol$1 = '(?:' + [rsNonAstral$1 + rsCombo$1 + '?', rsCombo$1, rsRegional$1, rsSurrPair$1, rsAstral$1].join('|') + ')';

  /** Used to match [string symbols](https://mathiasbynens.be/notes/javascript-unicode). */
  var reUnicode$1 = RegExp(rsFitz$1 + '(?=' + rsFitz$1 + ')|' + rsSymbol$1 + rsSeq$1, 'g');

  /**
   * Converts a Unicode `string` to an array.
   *
   * @private
   * @param {string} string The string to convert.
   * @returns {Array} Returns the converted array.
   */
  function unicodeToArray$1(string) {
    return string.match(reUnicode$1) || [];
  }

  /**
   * Converts `string` to an array.
   *
   * @private
   * @param {string} string The string to convert.
   * @returns {Array} Returns the converted array.
   */
  function stringToArray$1(string) {
    return hasUnicode$1(string)
      ? unicodeToArray$1(string)
      : asciiToArray$1(string);
  }

  /**
   * Converts `value` to a string. An empty string is returned for `null`
   * and `undefined` values. The sign of `-0` is preserved.
   *
   * @static
   * @memberOf _
   * @since 4.0.0
   * @category Lang
   * @param {*} value The value to convert.
   * @returns {string} Returns the converted string.
   * @example
   *
   * _.toString(null);
   * // => ''
   *
   * _.toString(-0);
   * // => '-0'
   *
   * _.toString([1, 2, 3]);
   * // => '1,2,3'
   */
  function toString$1(value) {
    return value == null ? '' : baseToString$1(value);
  }

  /** Used to match leading and trailing whitespace. */
  var reTrim$1 = /^\s+|\s+$/g;

  /**
   * Removes leading and trailing whitespace or specified characters from `string`.
   *
   * @static
   * @memberOf _
   * @since 3.0.0
   * @category String
   * @param {string} [string=''] The string to trim.
   * @param {string} [chars=whitespace] The characters to trim.
   * @param- {Object} [guard] Enables use as an iteratee for methods like `_.map`.
   * @returns {string} Returns the trimmed string.
   * @example
   *
   * _.trim('  abc  ');
   * // => 'abc'
   *
   * _.trim('-_-abc-_-', '_-');
   * // => 'abc'
   *
   * _.map(['  foo  ', '  bar  '], _.trim);
   * // => ['foo', 'bar']
   */
  function trim$1(string, chars, guard) {
    string = toString$1(string);
    if (string && (guard || chars === undefined)) {
      return string.replace(reTrim$1, '');
    }
    if (!string || !(chars = baseToString$1(chars))) {
      return string;
    }
    var strSymbols = stringToArray$1(string),
        chrSymbols = stringToArray$1(chars),
        start = charsStartIndex$1(strSymbols, chrSymbols),
        end = charsEndIndex$1(strSymbols, chrSymbols) + 1;

    return castSlice$1(strSymbols, start, end).join('');
  }

  /** `Object#toString` result references. */
  var numberTag$3 = '[object Number]';

  /**
   * Checks if `value` is classified as a `Number` primitive or object.
   *
   * **Note:** To exclude `Infinity`, `-Infinity`, and `NaN`, which are
   * classified as numbers, use the `_.isFinite` method.
   *
   * @static
   * @memberOf _
   * @since 0.1.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a number, else `false`.
   * @example
   *
   * _.isNumber(3);
   * // => true
   *
   * _.isNumber(Number.MIN_VALUE);
   * // => true
   *
   * _.isNumber(Infinity);
   * // => true
   *
   * _.isNumber('3');
   * // => false
   */
  function isNumber$1(value) {
    return typeof value == 'number' ||
      (isObjectLike$1(value) && baseGetTag$1(value) == numberTag$3);
  }

  /**
   * Checks if `value` is `NaN`.
   *
   * **Note:** This method is based on
   * [`Number.isNaN`](https://mdn.io/Number/isNaN) and is not the same as
   * global [`isNaN`](https://mdn.io/isNaN) which returns `true` for
   * `undefined` and other non-number values.
   *
   * @static
   * @memberOf _
   * @since 0.1.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is `NaN`, else `false`.
   * @example
   *
   * _.isNaN(NaN);
   * // => true
   *
   * _.isNaN(new Number(NaN));
   * // => true
   *
   * isNaN(undefined);
   * // => true
   *
   * _.isNaN(undefined);
   * // => false
   */
  function isNaN$2(value) {
    // An `NaN` primitive is the only value that is not equal to itself.
    // Perform the `toStringTag` check first to avoid errors with some
    // ActiveX objects in IE.
    return isNumber$1(value) && value != +value;
  }

  /** `Object#toString` result references. */
  var stringTag$3 = '[object String]';

  /**
   * Checks if `value` is classified as a `String` primitive or object.
   *
   * @static
   * @since 0.1.0
   * @memberOf _
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a string, else `false`.
   * @example
   *
   * _.isString('abc');
   * // => true
   *
   * _.isString(1);
   * // => false
   */
  function isString$3(value) {
    return typeof value == 'string' ||
      (!isArray$1(value) && isObjectLike$1(value) && baseGetTag$1(value) == stringTag$3);
  }

  // validator numbers
  /**
   * @2015-05-04 found a problem if the value is a number like string
   * it will pass, so add a chck if it's string before we pass to next
   * @param {number} value expected value
   * @return {boolean} true if OK
   */
  var checkIsNumber$1 = function(value) {
    return isString$3(value) ? false : !isNaN$2( parseFloat(value) )
  };

  // validate string type
  /**
   * @param {string} value expected value
   * @return {boolean} true if OK
   */
  var checkIsString$1 = function(value) {
    return (trim$1(value) !== '') ? isString$3(value) : false
  };

  // check for boolean

  /**
   * @param {boolean} value expected
   * @return {boolean} true if OK
   */
  var checkIsBoolean$1 = function(value) {
    return value !== null && value !== undefined && typeof value === 'boolean'
  };

  // validate any thing only check if there is something

  /**
   * @param {*} value the value
   * @param {boolean} [checkNull=true] strict check if there is null value
   * @return {boolean} true is OK
   */
  var checkIsAny$1 = function(value, checkNull) {
    if ( checkNull === void 0 ) checkNull = true;

    if (value !== undefined && value !== '' && trim$1(value) !== '') {
      if (checkNull === false || (checkNull === true && value !== null)) {
        return true;
      }
    }
    return false;
  };

  // primitive types

  /**
   * this is a wrapper method to call different one based on their type
   * @param {string} type to check
   * @return {function} a function to handle the type
   */
  var combineFn$1 = function(type) {
    switch (type) {
      case NUMBER_TYPE$1:
        return checkIsNumber$1
      case STRING_TYPE$1:
        return checkIsString$1
      case BOOLEAN_TYPE$1:
        return checkIsBoolean$1
      default:
        return checkIsAny$1
    }
  };

  // validate array type

  /**
   * @param {array} value expected
   * @param {string} [type=''] pass the type if we encounter array.<T> then we need to check the value as well
   * @return {boolean} true if OK
   */
  var checkIsArray$1 = function(value, type) {
    if ( type === void 0 ) type='';

    if (isArray$1(value)) {
      if (type === '' || trim$1(type)==='') {
        return true;
      }
      // we test it in reverse
      // @TODO if the type is an array (OR) then what?
      // we need to take into account this could be an array
      var c = value.filter(function (v) { return !combineFn$1(type)(v); });
      return !(c.length > 0)
    }
    return false
  };

  /**
   * Creates a unary function that invokes `func` with its argument transformed.
   *
   * @private
   * @param {Function} func The function to wrap.
   * @param {Function} transform The argument transform.
   * @returns {Function} Returns the new function.
   */
  function overArg$1(func, transform) {
    return function(arg) {
      return func(transform(arg));
    };
  }

  /** Built-in value references. */
  var getPrototype$1 = overArg$1(Object.getPrototypeOf, Object);

  /** `Object#toString` result references. */
  var objectTag$4 = '[object Object]';

  /** Used for built-in method references. */
  var funcProto$3 = Function.prototype,
      objectProto$h = Object.prototype;

  /** Used to resolve the decompiled source of functions. */
  var funcToString$3 = funcProto$3.toString;

  /** Used to check objects for own properties. */
  var hasOwnProperty$d = objectProto$h.hasOwnProperty;

  /** Used to infer the `Object` constructor. */
  var objectCtorString$1 = funcToString$3.call(Object);

  /**
   * Checks if `value` is a plain object, that is, an object created by the
   * `Object` constructor or one with a `[[Prototype]]` of `null`.
   *
   * @static
   * @memberOf _
   * @since 0.8.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a plain object, else `false`.
   * @example
   *
   * function Foo() {
   *   this.a = 1;
   * }
   *
   * _.isPlainObject(new Foo);
   * // => false
   *
   * _.isPlainObject([1, 2, 3]);
   * // => false
   *
   * _.isPlainObject({ 'x': 0, 'y': 0 });
   * // => true
   *
   * _.isPlainObject(Object.create(null));
   * // => true
   */
  function isPlainObject$1(value) {
    if (!isObjectLike$1(value) || baseGetTag$1(value) != objectTag$4) {
      return false;
    }
    var proto = getPrototype$1(value);
    if (proto === null) {
      return true;
    }
    var Ctor = hasOwnProperty$d.call(proto, 'constructor') && proto.constructor;
    return typeof Ctor == 'function' && Ctor instanceof Ctor &&
      funcToString$3.call(Ctor) == objectCtorString$1;
  }

  // custom validation error class
  // when validaton failed
  var JsonqlValidationError$1 = /*@__PURE__*/(function (Error) {
    function JsonqlValidationError() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      Error.apply(this, args);

      this.message = args[0];
      this.detail = args[1];

      this.className = JsonqlValidationError.name;

      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, JsonqlValidationError);
      }
    }

    if ( Error ) JsonqlValidationError.__proto__ = Error;
    JsonqlValidationError.prototype = Object.create( Error && Error.prototype );
    JsonqlValidationError.prototype.constructor = JsonqlValidationError;

    var staticAccessors = { name: { configurable: true } };

    staticAccessors.name.get = function () {
      return 'JsonqlValidationError'
    };

    Object.defineProperties( JsonqlValidationError, staticAccessors );

    return JsonqlValidationError;
  }(Error));

  var NO_STATUS_CODE$1 = -1;

  /**
   * This is a custom error to throw whenever a error happen inside the jsonql
   * This help us to capture the right error, due to the call happens in sequence
   * @param {string} message to tell what happen
   * @param {mixed} extra things we want to add, 500?
   */
  var JsonqlError$2 = /*@__PURE__*/(function (Error) {
    function JsonqlError() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      Error.apply(this, args);

      this.message = args[0];
      this.detail = args[1];

      this.className = JsonqlError.name;

      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, JsonqlError);
        // this.detail = this.stack;
      }
    }

    if ( Error ) JsonqlError.__proto__ = Error;
    JsonqlError.prototype = Object.create( Error && Error.prototype );
    JsonqlError.prototype.constructor = JsonqlError;

    var staticAccessors = { name: { configurable: true },statusCode: { configurable: true } };

    staticAccessors.name.get = function () {
      return 'JsonqlError'
    };

    staticAccessors.statusCode.get = function () {
      return NO_STATUS_CODE$1
    };

    Object.defineProperties( JsonqlError, staticAccessors );

    return JsonqlError;
  }(Error));

  /**
   * Removes all key-value entries from the list cache.
   *
   * @private
   * @name clear
   * @memberOf ListCache
   */
  function listCacheClear$1() {
    this.__data__ = [];
    this.size = 0;
  }

  /**
   * Performs a
   * [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
   * comparison between two values to determine if they are equivalent.
   *
   * @static
   * @memberOf _
   * @since 4.0.0
   * @category Lang
   * @param {*} value The value to compare.
   * @param {*} other The other value to compare.
   * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
   * @example
   *
   * var object = { 'a': 1 };
   * var other = { 'a': 1 };
   *
   * _.eq(object, object);
   * // => true
   *
   * _.eq(object, other);
   * // => false
   *
   * _.eq('a', 'a');
   * // => true
   *
   * _.eq('a', Object('a'));
   * // => false
   *
   * _.eq(NaN, NaN);
   * // => true
   */
  function eq$1(value, other) {
    return value === other || (value !== value && other !== other);
  }

  /**
   * Gets the index at which the `key` is found in `array` of key-value pairs.
   *
   * @private
   * @param {Array} array The array to inspect.
   * @param {*} key The key to search for.
   * @returns {number} Returns the index of the matched value, else `-1`.
   */
  function assocIndexOf$1(array, key) {
    var length = array.length;
    while (length--) {
      if (eq$1(array[length][0], key)) {
        return length;
      }
    }
    return -1;
  }

  /** Used for built-in method references. */
  var arrayProto$1 = Array.prototype;

  /** Built-in value references. */
  var splice$1 = arrayProto$1.splice;

  /**
   * Removes `key` and its value from the list cache.
   *
   * @private
   * @name delete
   * @memberOf ListCache
   * @param {string} key The key of the value to remove.
   * @returns {boolean} Returns `true` if the entry was removed, else `false`.
   */
  function listCacheDelete$1(key) {
    var data = this.__data__,
        index = assocIndexOf$1(data, key);

    if (index < 0) {
      return false;
    }
    var lastIndex = data.length - 1;
    if (index == lastIndex) {
      data.pop();
    } else {
      splice$1.call(data, index, 1);
    }
    --this.size;
    return true;
  }

  /**
   * Gets the list cache value for `key`.
   *
   * @private
   * @name get
   * @memberOf ListCache
   * @param {string} key The key of the value to get.
   * @returns {*} Returns the entry value.
   */
  function listCacheGet$1(key) {
    var data = this.__data__,
        index = assocIndexOf$1(data, key);

    return index < 0 ? undefined : data[index][1];
  }

  /**
   * Checks if a list cache value for `key` exists.
   *
   * @private
   * @name has
   * @memberOf ListCache
   * @param {string} key The key of the entry to check.
   * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
   */
  function listCacheHas$1(key) {
    return assocIndexOf$1(this.__data__, key) > -1;
  }

  /**
   * Sets the list cache `key` to `value`.
   *
   * @private
   * @name set
   * @memberOf ListCache
   * @param {string} key The key of the value to set.
   * @param {*} value The value to set.
   * @returns {Object} Returns the list cache instance.
   */
  function listCacheSet$1(key, value) {
    var data = this.__data__,
        index = assocIndexOf$1(data, key);

    if (index < 0) {
      ++this.size;
      data.push([key, value]);
    } else {
      data[index][1] = value;
    }
    return this;
  }

  /**
   * Creates an list cache object.
   *
   * @private
   * @constructor
   * @param {Array} [entries] The key-value pairs to cache.
   */
  function ListCache$1(entries) {
    var index = -1,
        length = entries == null ? 0 : entries.length;

    this.clear();
    while (++index < length) {
      var entry = entries[index];
      this.set(entry[0], entry[1]);
    }
  }

  // Add methods to `ListCache`.
  ListCache$1.prototype.clear = listCacheClear$1;
  ListCache$1.prototype['delete'] = listCacheDelete$1;
  ListCache$1.prototype.get = listCacheGet$1;
  ListCache$1.prototype.has = listCacheHas$1;
  ListCache$1.prototype.set = listCacheSet$1;

  /**
   * Removes all key-value entries from the stack.
   *
   * @private
   * @name clear
   * @memberOf Stack
   */
  function stackClear$1() {
    this.__data__ = new ListCache$1;
    this.size = 0;
  }

  /**
   * Removes `key` and its value from the stack.
   *
   * @private
   * @name delete
   * @memberOf Stack
   * @param {string} key The key of the value to remove.
   * @returns {boolean} Returns `true` if the entry was removed, else `false`.
   */
  function stackDelete$1(key) {
    var data = this.__data__,
        result = data['delete'](key);

    this.size = data.size;
    return result;
  }

  /**
   * Gets the stack value for `key`.
   *
   * @private
   * @name get
   * @memberOf Stack
   * @param {string} key The key of the value to get.
   * @returns {*} Returns the entry value.
   */
  function stackGet$1(key) {
    return this.__data__.get(key);
  }

  /**
   * Checks if a stack value for `key` exists.
   *
   * @private
   * @name has
   * @memberOf Stack
   * @param {string} key The key of the entry to check.
   * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
   */
  function stackHas$1(key) {
    return this.__data__.has(key);
  }

  /**
   * Checks if `value` is the
   * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
   * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
   *
   * @static
   * @memberOf _
   * @since 0.1.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is an object, else `false`.
   * @example
   *
   * _.isObject({});
   * // => true
   *
   * _.isObject([1, 2, 3]);
   * // => true
   *
   * _.isObject(_.noop);
   * // => true
   *
   * _.isObject(null);
   * // => false
   */
  function isObject$1(value) {
    var type = typeof value;
    return value != null && (type == 'object' || type == 'function');
  }

  /** `Object#toString` result references. */
  var asyncTag$1 = '[object AsyncFunction]',
      funcTag$2 = '[object Function]',
      genTag$1 = '[object GeneratorFunction]',
      proxyTag$1 = '[object Proxy]';

  /**
   * Checks if `value` is classified as a `Function` object.
   *
   * @static
   * @memberOf _
   * @since 0.1.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a function, else `false`.
   * @example
   *
   * _.isFunction(_);
   * // => true
   *
   * _.isFunction(/abc/);
   * // => false
   */
  function isFunction$1(value) {
    if (!isObject$1(value)) {
      return false;
    }
    // The use of `Object#toString` avoids issues with the `typeof` operator
    // in Safari 9 which returns 'object' for typed arrays and other constructors.
    var tag = baseGetTag$1(value);
    return tag == funcTag$2 || tag == genTag$1 || tag == asyncTag$1 || tag == proxyTag$1;
  }

  /** Used to detect overreaching core-js shims. */
  var coreJsData$1 = root$1['__core-js_shared__'];

  /** Used to detect methods masquerading as native. */
  var maskSrcKey$1 = (function() {
    var uid = /[^.]+$/.exec(coreJsData$1 && coreJsData$1.keys && coreJsData$1.keys.IE_PROTO || '');
    return uid ? ('Symbol(src)_1.' + uid) : '';
  }());

  /**
   * Checks if `func` has its source masked.
   *
   * @private
   * @param {Function} func The function to check.
   * @returns {boolean} Returns `true` if `func` is masked, else `false`.
   */
  function isMasked$1(func) {
    return !!maskSrcKey$1 && (maskSrcKey$1 in func);
  }

  /** Used for built-in method references. */
  var funcProto$4 = Function.prototype;

  /** Used to resolve the decompiled source of functions. */
  var funcToString$4 = funcProto$4.toString;

  /**
   * Converts `func` to its source code.
   *
   * @private
   * @param {Function} func The function to convert.
   * @returns {string} Returns the source code.
   */
  function toSource$1(func) {
    if (func != null) {
      try {
        return funcToString$4.call(func);
      } catch (e) {}
      try {
        return (func + '');
      } catch (e) {}
    }
    return '';
  }

  /**
   * Used to match `RegExp`
   * [syntax characters](http://ecma-international.org/ecma-262/7.0/#sec-patterns).
   */
  var reRegExpChar$1 = /[\\^$.*+?()[\]{}|]/g;

  /** Used to detect host constructors (Safari). */
  var reIsHostCtor$1 = /^\[object .+?Constructor\]$/;

  /** Used for built-in method references. */
  var funcProto$5 = Function.prototype,
      objectProto$i = Object.prototype;

  /** Used to resolve the decompiled source of functions. */
  var funcToString$5 = funcProto$5.toString;

  /** Used to check objects for own properties. */
  var hasOwnProperty$e = objectProto$i.hasOwnProperty;

  /** Used to detect if a method is native. */
  var reIsNative$1 = RegExp('^' +
    funcToString$5.call(hasOwnProperty$e).replace(reRegExpChar$1, '\\$&')
    .replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, '$1.*?') + '$'
  );

  /**
   * The base implementation of `_.isNative` without bad shim checks.
   *
   * @private
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a native function,
   *  else `false`.
   */
  function baseIsNative$1(value) {
    if (!isObject$1(value) || isMasked$1(value)) {
      return false;
    }
    var pattern = isFunction$1(value) ? reIsNative$1 : reIsHostCtor$1;
    return pattern.test(toSource$1(value));
  }

  /**
   * Gets the value at `key` of `object`.
   *
   * @private
   * @param {Object} [object] The object to query.
   * @param {string} key The key of the property to get.
   * @returns {*} Returns the property value.
   */
  function getValue$1(object, key) {
    return object == null ? undefined : object[key];
  }

  /**
   * Gets the native function at `key` of `object`.
   *
   * @private
   * @param {Object} object The object to query.
   * @param {string} key The key of the method to get.
   * @returns {*} Returns the function if it's native, else `undefined`.
   */
  function getNative$1(object, key) {
    var value = getValue$1(object, key);
    return baseIsNative$1(value) ? value : undefined;
  }

  /* Built-in method references that are verified to be native. */
  var Map$2 = getNative$1(root$1, 'Map');

  /* Built-in method references that are verified to be native. */
  var nativeCreate$1 = getNative$1(Object, 'create');

  /**
   * Removes all key-value entries from the hash.
   *
   * @private
   * @name clear
   * @memberOf Hash
   */
  function hashClear$1() {
    this.__data__ = nativeCreate$1 ? nativeCreate$1(null) : {};
    this.size = 0;
  }

  /**
   * Removes `key` and its value from the hash.
   *
   * @private
   * @name delete
   * @memberOf Hash
   * @param {Object} hash The hash to modify.
   * @param {string} key The key of the value to remove.
   * @returns {boolean} Returns `true` if the entry was removed, else `false`.
   */
  function hashDelete$1(key) {
    var result = this.has(key) && delete this.__data__[key];
    this.size -= result ? 1 : 0;
    return result;
  }

  /** Used to stand-in for `undefined` hash values. */
  var HASH_UNDEFINED$3 = '__lodash_hash_undefined__';

  /** Used for built-in method references. */
  var objectProto$j = Object.prototype;

  /** Used to check objects for own properties. */
  var hasOwnProperty$f = objectProto$j.hasOwnProperty;

  /**
   * Gets the hash value for `key`.
   *
   * @private
   * @name get
   * @memberOf Hash
   * @param {string} key The key of the value to get.
   * @returns {*} Returns the entry value.
   */
  function hashGet$1(key) {
    var data = this.__data__;
    if (nativeCreate$1) {
      var result = data[key];
      return result === HASH_UNDEFINED$3 ? undefined : result;
    }
    return hasOwnProperty$f.call(data, key) ? data[key] : undefined;
  }

  /** Used for built-in method references. */
  var objectProto$k = Object.prototype;

  /** Used to check objects for own properties. */
  var hasOwnProperty$g = objectProto$k.hasOwnProperty;

  /**
   * Checks if a hash value for `key` exists.
   *
   * @private
   * @name has
   * @memberOf Hash
   * @param {string} key The key of the entry to check.
   * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
   */
  function hashHas$1(key) {
    var data = this.__data__;
    return nativeCreate$1 ? (data[key] !== undefined) : hasOwnProperty$g.call(data, key);
  }

  /** Used to stand-in for `undefined` hash values. */
  var HASH_UNDEFINED$4 = '__lodash_hash_undefined__';

  /**
   * Sets the hash `key` to `value`.
   *
   * @private
   * @name set
   * @memberOf Hash
   * @param {string} key The key of the value to set.
   * @param {*} value The value to set.
   * @returns {Object} Returns the hash instance.
   */
  function hashSet$1(key, value) {
    var data = this.__data__;
    this.size += this.has(key) ? 0 : 1;
    data[key] = (nativeCreate$1 && value === undefined) ? HASH_UNDEFINED$4 : value;
    return this;
  }

  /**
   * Creates a hash object.
   *
   * @private
   * @constructor
   * @param {Array} [entries] The key-value pairs to cache.
   */
  function Hash$1(entries) {
    var index = -1,
        length = entries == null ? 0 : entries.length;

    this.clear();
    while (++index < length) {
      var entry = entries[index];
      this.set(entry[0], entry[1]);
    }
  }

  // Add methods to `Hash`.
  Hash$1.prototype.clear = hashClear$1;
  Hash$1.prototype['delete'] = hashDelete$1;
  Hash$1.prototype.get = hashGet$1;
  Hash$1.prototype.has = hashHas$1;
  Hash$1.prototype.set = hashSet$1;

  /**
   * Removes all key-value entries from the map.
   *
   * @private
   * @name clear
   * @memberOf MapCache
   */
  function mapCacheClear$1() {
    this.size = 0;
    this.__data__ = {
      'hash': new Hash$1,
      'map': new (Map$2 || ListCache$1),
      'string': new Hash$1
    };
  }

  /**
   * Checks if `value` is suitable for use as unique object key.
   *
   * @private
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is suitable, else `false`.
   */
  function isKeyable$1(value) {
    var type = typeof value;
    return (type == 'string' || type == 'number' || type == 'symbol' || type == 'boolean')
      ? (value !== '__proto__')
      : (value === null);
  }

  /**
   * Gets the data for `map`.
   *
   * @private
   * @param {Object} map The map to query.
   * @param {string} key The reference key.
   * @returns {*} Returns the map data.
   */
  function getMapData$1(map, key) {
    var data = map.__data__;
    return isKeyable$1(key)
      ? data[typeof key == 'string' ? 'string' : 'hash']
      : data.map;
  }

  /**
   * Removes `key` and its value from the map.
   *
   * @private
   * @name delete
   * @memberOf MapCache
   * @param {string} key The key of the value to remove.
   * @returns {boolean} Returns `true` if the entry was removed, else `false`.
   */
  function mapCacheDelete$1(key) {
    var result = getMapData$1(this, key)['delete'](key);
    this.size -= result ? 1 : 0;
    return result;
  }

  /**
   * Gets the map value for `key`.
   *
   * @private
   * @name get
   * @memberOf MapCache
   * @param {string} key The key of the value to get.
   * @returns {*} Returns the entry value.
   */
  function mapCacheGet$1(key) {
    return getMapData$1(this, key).get(key);
  }

  /**
   * Checks if a map value for `key` exists.
   *
   * @private
   * @name has
   * @memberOf MapCache
   * @param {string} key The key of the entry to check.
   * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
   */
  function mapCacheHas$1(key) {
    return getMapData$1(this, key).has(key);
  }

  /**
   * Sets the map `key` to `value`.
   *
   * @private
   * @name set
   * @memberOf MapCache
   * @param {string} key The key of the value to set.
   * @param {*} value The value to set.
   * @returns {Object} Returns the map cache instance.
   */
  function mapCacheSet$1(key, value) {
    var data = getMapData$1(this, key),
        size = data.size;

    data.set(key, value);
    this.size += data.size == size ? 0 : 1;
    return this;
  }

  /**
   * Creates a map cache object to store key-value pairs.
   *
   * @private
   * @constructor
   * @param {Array} [entries] The key-value pairs to cache.
   */
  function MapCache$1(entries) {
    var index = -1,
        length = entries == null ? 0 : entries.length;

    this.clear();
    while (++index < length) {
      var entry = entries[index];
      this.set(entry[0], entry[1]);
    }
  }

  // Add methods to `MapCache`.
  MapCache$1.prototype.clear = mapCacheClear$1;
  MapCache$1.prototype['delete'] = mapCacheDelete$1;
  MapCache$1.prototype.get = mapCacheGet$1;
  MapCache$1.prototype.has = mapCacheHas$1;
  MapCache$1.prototype.set = mapCacheSet$1;

  /** Used as the size to enable large array optimizations. */
  var LARGE_ARRAY_SIZE$1 = 200;

  /**
   * Sets the stack `key` to `value`.
   *
   * @private
   * @name set
   * @memberOf Stack
   * @param {string} key The key of the value to set.
   * @param {*} value The value to set.
   * @returns {Object} Returns the stack cache instance.
   */
  function stackSet$1(key, value) {
    var data = this.__data__;
    if (data instanceof ListCache$1) {
      var pairs = data.__data__;
      if (!Map$2 || (pairs.length < LARGE_ARRAY_SIZE$1 - 1)) {
        pairs.push([key, value]);
        this.size = ++data.size;
        return this;
      }
      data = this.__data__ = new MapCache$1(pairs);
    }
    data.set(key, value);
    this.size = data.size;
    return this;
  }

  /**
   * Creates a stack cache object to store key-value pairs.
   *
   * @private
   * @constructor
   * @param {Array} [entries] The key-value pairs to cache.
   */
  function Stack$1(entries) {
    var data = this.__data__ = new ListCache$1(entries);
    this.size = data.size;
  }

  // Add methods to `Stack`.
  Stack$1.prototype.clear = stackClear$1;
  Stack$1.prototype['delete'] = stackDelete$1;
  Stack$1.prototype.get = stackGet$1;
  Stack$1.prototype.has = stackHas$1;
  Stack$1.prototype.set = stackSet$1;

  var defineProperty$1 = (function() {
    try {
      var func = getNative$1(Object, 'defineProperty');
      func({}, '', {});
      return func;
    } catch (e) {}
  }());

  /**
   * The base implementation of `assignValue` and `assignMergeValue` without
   * value checks.
   *
   * @private
   * @param {Object} object The object to modify.
   * @param {string} key The key of the property to assign.
   * @param {*} value The value to assign.
   */
  function baseAssignValue$1(object, key, value) {
    if (key == '__proto__' && defineProperty$1) {
      defineProperty$1(object, key, {
        'configurable': true,
        'enumerable': true,
        'value': value,
        'writable': true
      });
    } else {
      object[key] = value;
    }
  }

  /**
   * This function is like `assignValue` except that it doesn't assign
   * `undefined` values.
   *
   * @private
   * @param {Object} object The object to modify.
   * @param {string} key The key of the property to assign.
   * @param {*} value The value to assign.
   */
  function assignMergeValue$1(object, key, value) {
    if ((value !== undefined && !eq$1(object[key], value)) ||
        (value === undefined && !(key in object))) {
      baseAssignValue$1(object, key, value);
    }
  }

  /**
   * Creates a base function for methods like `_.forIn` and `_.forOwn`.
   *
   * @private
   * @param {boolean} [fromRight] Specify iterating from right to left.
   * @returns {Function} Returns the new base function.
   */
  function createBaseFor$1(fromRight) {
    return function(object, iteratee, keysFunc) {
      var index = -1,
          iterable = Object(object),
          props = keysFunc(object),
          length = props.length;

      while (length--) {
        var key = props[fromRight ? length : ++index];
        if (iteratee(iterable[key], key, iterable) === false) {
          break;
        }
      }
      return object;
    };
  }

  /**
   * The base implementation of `baseForOwn` which iterates over `object`
   * properties returned by `keysFunc` and invokes `iteratee` for each property.
   * Iteratee functions may exit iteration early by explicitly returning `false`.
   *
   * @private
   * @param {Object} object The object to iterate over.
   * @param {Function} iteratee The function invoked per iteration.
   * @param {Function} keysFunc The function to get the keys of `object`.
   * @returns {Object} Returns `object`.
   */
  var baseFor$1 = createBaseFor$1();

  /** Detect free variable `exports`. */
  var freeExports$3 = typeof exports == 'object' && exports && !exports.nodeType && exports;

  /** Detect free variable `module`. */
  var freeModule$3 = freeExports$3 && typeof module == 'object' && module && !module.nodeType && module;

  /** Detect the popular CommonJS extension `module.exports`. */
  var moduleExports$3 = freeModule$3 && freeModule$3.exports === freeExports$3;

  /** Built-in value references. */
  var Buffer$2 = moduleExports$3 ? root$1.Buffer : undefined,
      allocUnsafe$1 = Buffer$2 ? Buffer$2.allocUnsafe : undefined;

  /**
   * Creates a clone of  `buffer`.
   *
   * @private
   * @param {Buffer} buffer The buffer to clone.
   * @param {boolean} [isDeep] Specify a deep clone.
   * @returns {Buffer} Returns the cloned buffer.
   */
  function cloneBuffer$1(buffer, isDeep) {
    if (isDeep) {
      return buffer.slice();
    }
    var length = buffer.length,
        result = allocUnsafe$1 ? allocUnsafe$1(length) : new buffer.constructor(length);

    buffer.copy(result);
    return result;
  }

  /** Built-in value references. */
  var Uint8Array$1 = root$1.Uint8Array;

  /**
   * Creates a clone of `arrayBuffer`.
   *
   * @private
   * @param {ArrayBuffer} arrayBuffer The array buffer to clone.
   * @returns {ArrayBuffer} Returns the cloned array buffer.
   */
  function cloneArrayBuffer$1(arrayBuffer) {
    var result = new arrayBuffer.constructor(arrayBuffer.byteLength);
    new Uint8Array$1(result).set(new Uint8Array$1(arrayBuffer));
    return result;
  }

  /**
   * Creates a clone of `typedArray`.
   *
   * @private
   * @param {Object} typedArray The typed array to clone.
   * @param {boolean} [isDeep] Specify a deep clone.
   * @returns {Object} Returns the cloned typed array.
   */
  function cloneTypedArray$1(typedArray, isDeep) {
    var buffer = isDeep ? cloneArrayBuffer$1(typedArray.buffer) : typedArray.buffer;
    return new typedArray.constructor(buffer, typedArray.byteOffset, typedArray.length);
  }

  /**
   * Copies the values of `source` to `array`.
   *
   * @private
   * @param {Array} source The array to copy values from.
   * @param {Array} [array=[]] The array to copy values to.
   * @returns {Array} Returns `array`.
   */
  function copyArray$1(source, array) {
    var index = -1,
        length = source.length;

    array || (array = Array(length));
    while (++index < length) {
      array[index] = source[index];
    }
    return array;
  }

  /** Built-in value references. */
  var objectCreate$1 = Object.create;

  /**
   * The base implementation of `_.create` without support for assigning
   * properties to the created object.
   *
   * @private
   * @param {Object} proto The object to inherit from.
   * @returns {Object} Returns the new object.
   */
  var baseCreate$1 = (function() {
    function object() {}
    return function(proto) {
      if (!isObject$1(proto)) {
        return {};
      }
      if (objectCreate$1) {
        return objectCreate$1(proto);
      }
      object.prototype = proto;
      var result = new object;
      object.prototype = undefined;
      return result;
    };
  }());

  /** Used for built-in method references. */
  var objectProto$l = Object.prototype;

  /**
   * Checks if `value` is likely a prototype object.
   *
   * @private
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a prototype, else `false`.
   */
  function isPrototype$1(value) {
    var Ctor = value && value.constructor,
        proto = (typeof Ctor == 'function' && Ctor.prototype) || objectProto$l;

    return value === proto;
  }

  /**
   * Initializes an object clone.
   *
   * @private
   * @param {Object} object The object to clone.
   * @returns {Object} Returns the initialized clone.
   */
  function initCloneObject$1(object) {
    return (typeof object.constructor == 'function' && !isPrototype$1(object))
      ? baseCreate$1(getPrototype$1(object))
      : {};
  }

  /** `Object#toString` result references. */
  var argsTag$3 = '[object Arguments]';

  /**
   * The base implementation of `_.isArguments`.
   *
   * @private
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is an `arguments` object,
   */
  function baseIsArguments$1(value) {
    return isObjectLike$1(value) && baseGetTag$1(value) == argsTag$3;
  }

  /** Used for built-in method references. */
  var objectProto$m = Object.prototype;

  /** Used to check objects for own properties. */
  var hasOwnProperty$h = objectProto$m.hasOwnProperty;

  /** Built-in value references. */
  var propertyIsEnumerable$2 = objectProto$m.propertyIsEnumerable;

  /**
   * Checks if `value` is likely an `arguments` object.
   *
   * @static
   * @memberOf _
   * @since 0.1.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is an `arguments` object,
   *  else `false`.
   * @example
   *
   * _.isArguments(function() { return arguments; }());
   * // => true
   *
   * _.isArguments([1, 2, 3]);
   * // => false
   */
  var isArguments$1 = baseIsArguments$1(function() { return arguments; }()) ? baseIsArguments$1 : function(value) {
    return isObjectLike$1(value) && hasOwnProperty$h.call(value, 'callee') &&
      !propertyIsEnumerable$2.call(value, 'callee');
  };

  /** Used as references for various `Number` constants. */
  var MAX_SAFE_INTEGER$2 = 9007199254740991;

  /**
   * Checks if `value` is a valid array-like length.
   *
   * **Note:** This method is loosely based on
   * [`ToLength`](http://ecma-international.org/ecma-262/7.0/#sec-tolength).
   *
   * @static
   * @memberOf _
   * @since 4.0.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a valid length, else `false`.
   * @example
   *
   * _.isLength(3);
   * // => true
   *
   * _.isLength(Number.MIN_VALUE);
   * // => false
   *
   * _.isLength(Infinity);
   * // => false
   *
   * _.isLength('3');
   * // => false
   */
  function isLength$1(value) {
    return typeof value == 'number' &&
      value > -1 && value % 1 == 0 && value <= MAX_SAFE_INTEGER$2;
  }

  /**
   * Checks if `value` is array-like. A value is considered array-like if it's
   * not a function and has a `value.length` that's an integer greater than or
   * equal to `0` and less than or equal to `Number.MAX_SAFE_INTEGER`.
   *
   * @static
   * @memberOf _
   * @since 4.0.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is array-like, else `false`.
   * @example
   *
   * _.isArrayLike([1, 2, 3]);
   * // => true
   *
   * _.isArrayLike(document.body.children);
   * // => true
   *
   * _.isArrayLike('abc');
   * // => true
   *
   * _.isArrayLike(_.noop);
   * // => false
   */
  function isArrayLike$2(value) {
    return value != null && isLength$1(value.length) && !isFunction$1(value);
  }

  /**
   * This method is like `_.isArrayLike` except that it also checks if `value`
   * is an object.
   *
   * @static
   * @memberOf _
   * @since 4.0.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is an array-like object,
   *  else `false`.
   * @example
   *
   * _.isArrayLikeObject([1, 2, 3]);
   * // => true
   *
   * _.isArrayLikeObject(document.body.children);
   * // => true
   *
   * _.isArrayLikeObject('abc');
   * // => false
   *
   * _.isArrayLikeObject(_.noop);
   * // => false
   */
  function isArrayLikeObject$1(value) {
    return isObjectLike$1(value) && isArrayLike$2(value);
  }

  /**
   * This method returns `false`.
   *
   * @static
   * @memberOf _
   * @since 4.13.0
   * @category Util
   * @returns {boolean} Returns `false`.
   * @example
   *
   * _.times(2, _.stubFalse);
   * // => [false, false]
   */
  function stubFalse$1() {
    return false;
  }

  /** Detect free variable `exports`. */
  var freeExports$4 = typeof exports == 'object' && exports && !exports.nodeType && exports;

  /** Detect free variable `module`. */
  var freeModule$4 = freeExports$4 && typeof module == 'object' && module && !module.nodeType && module;

  /** Detect the popular CommonJS extension `module.exports`. */
  var moduleExports$4 = freeModule$4 && freeModule$4.exports === freeExports$4;

  /** Built-in value references. */
  var Buffer$3 = moduleExports$4 ? root$1.Buffer : undefined;

  /* Built-in method references for those with the same name as other `lodash` methods. */
  var nativeIsBuffer$1 = Buffer$3 ? Buffer$3.isBuffer : undefined;

  /**
   * Checks if `value` is a buffer.
   *
   * @static
   * @memberOf _
   * @since 4.3.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a buffer, else `false`.
   * @example
   *
   * _.isBuffer(new Buffer(2));
   * // => true
   *
   * _.isBuffer(new Uint8Array(2));
   * // => false
   */
  var isBuffer$1 = nativeIsBuffer$1 || stubFalse$1;

  /** `Object#toString` result references. */
  var argsTag$4 = '[object Arguments]',
      arrayTag$2 = '[object Array]',
      boolTag$2 = '[object Boolean]',
      dateTag$2 = '[object Date]',
      errorTag$2 = '[object Error]',
      funcTag$3 = '[object Function]',
      mapTag$3 = '[object Map]',
      numberTag$4 = '[object Number]',
      objectTag$5 = '[object Object]',
      regexpTag$2 = '[object RegExp]',
      setTag$3 = '[object Set]',
      stringTag$4 = '[object String]',
      weakMapTag$2 = '[object WeakMap]';

  var arrayBufferTag$2 = '[object ArrayBuffer]',
      dataViewTag$3 = '[object DataView]',
      float32Tag$1 = '[object Float32Array]',
      float64Tag$1 = '[object Float64Array]',
      int8Tag$1 = '[object Int8Array]',
      int16Tag$1 = '[object Int16Array]',
      int32Tag$1 = '[object Int32Array]',
      uint8Tag$1 = '[object Uint8Array]',
      uint8ClampedTag$1 = '[object Uint8ClampedArray]',
      uint16Tag$1 = '[object Uint16Array]',
      uint32Tag$1 = '[object Uint32Array]';

  /** Used to identify `toStringTag` values of typed arrays. */
  var typedArrayTags$1 = {};
  typedArrayTags$1[float32Tag$1] = typedArrayTags$1[float64Tag$1] =
  typedArrayTags$1[int8Tag$1] = typedArrayTags$1[int16Tag$1] =
  typedArrayTags$1[int32Tag$1] = typedArrayTags$1[uint8Tag$1] =
  typedArrayTags$1[uint8ClampedTag$1] = typedArrayTags$1[uint16Tag$1] =
  typedArrayTags$1[uint32Tag$1] = true;
  typedArrayTags$1[argsTag$4] = typedArrayTags$1[arrayTag$2] =
  typedArrayTags$1[arrayBufferTag$2] = typedArrayTags$1[boolTag$2] =
  typedArrayTags$1[dataViewTag$3] = typedArrayTags$1[dateTag$2] =
  typedArrayTags$1[errorTag$2] = typedArrayTags$1[funcTag$3] =
  typedArrayTags$1[mapTag$3] = typedArrayTags$1[numberTag$4] =
  typedArrayTags$1[objectTag$5] = typedArrayTags$1[regexpTag$2] =
  typedArrayTags$1[setTag$3] = typedArrayTags$1[stringTag$4] =
  typedArrayTags$1[weakMapTag$2] = false;

  /**
   * The base implementation of `_.isTypedArray` without Node.js optimizations.
   *
   * @private
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
   */
  function baseIsTypedArray$1(value) {
    return isObjectLike$1(value) &&
      isLength$1(value.length) && !!typedArrayTags$1[baseGetTag$1(value)];
  }

  /**
   * The base implementation of `_.unary` without support for storing metadata.
   *
   * @private
   * @param {Function} func The function to cap arguments for.
   * @returns {Function} Returns the new capped function.
   */
  function baseUnary$1(func) {
    return function(value) {
      return func(value);
    };
  }

  /** Detect free variable `exports`. */
  var freeExports$5 = typeof exports == 'object' && exports && !exports.nodeType && exports;

  /** Detect free variable `module`. */
  var freeModule$5 = freeExports$5 && typeof module == 'object' && module && !module.nodeType && module;

  /** Detect the popular CommonJS extension `module.exports`. */
  var moduleExports$5 = freeModule$5 && freeModule$5.exports === freeExports$5;

  /** Detect free variable `process` from Node.js. */
  var freeProcess$1 = moduleExports$5 && freeGlobal$1.process;

  /** Used to access faster Node.js helpers. */
  var nodeUtil$1 = (function() {
    try {
      // Use `util.types` for Node.js 10+.
      var types = freeModule$5 && freeModule$5.require && freeModule$5.require('util').types;

      if (types) {
        return types;
      }

      // Legacy `process.binding('util')` for Node.js < 10.
      return freeProcess$1 && freeProcess$1.binding && freeProcess$1.binding('util');
    } catch (e) {}
  }());

  /* Node.js helper references. */
  var nodeIsTypedArray$1 = nodeUtil$1 && nodeUtil$1.isTypedArray;

  /**
   * Checks if `value` is classified as a typed array.
   *
   * @static
   * @memberOf _
   * @since 3.0.0
   * @category Lang
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
   * @example
   *
   * _.isTypedArray(new Uint8Array);
   * // => true
   *
   * _.isTypedArray([]);
   * // => false
   */
  var isTypedArray$1 = nodeIsTypedArray$1 ? baseUnary$1(nodeIsTypedArray$1) : baseIsTypedArray$1;

  /**
   * Gets the value at `key`, unless `key` is "__proto__" or "constructor".
   *
   * @private
   * @param {Object} object The object to query.
   * @param {string} key The key of the property to get.
   * @returns {*} Returns the property value.
   */
  function safeGet$1(object, key) {
    if (key === 'constructor' && typeof object[key] === 'function') {
      return;
    }

    if (key == '__proto__') {
      return;
    }

    return object[key];
  }

  /** Used for built-in method references. */
  var objectProto$n = Object.prototype;

  /** Used to check objects for own properties. */
  var hasOwnProperty$i = objectProto$n.hasOwnProperty;

  /**
   * Assigns `value` to `key` of `object` if the existing value is not equivalent
   * using [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
   * for equality comparisons.
   *
   * @private
   * @param {Object} object The object to modify.
   * @param {string} key The key of the property to assign.
   * @param {*} value The value to assign.
   */
  function assignValue$1(object, key, value) {
    var objValue = object[key];
    if (!(hasOwnProperty$i.call(object, key) && eq$1(objValue, value)) ||
        (value === undefined && !(key in object))) {
      baseAssignValue$1(object, key, value);
    }
  }

  /**
   * Copies properties of `source` to `object`.
   *
   * @private
   * @param {Object} source The object to copy properties from.
   * @param {Array} props The property identifiers to copy.
   * @param {Object} [object={}] The object to copy properties to.
   * @param {Function} [customizer] The function to customize copied values.
   * @returns {Object} Returns `object`.
   */
  function copyObject$1(source, props, object, customizer) {
    var isNew = !object;
    object || (object = {});

    var index = -1,
        length = props.length;

    while (++index < length) {
      var key = props[index];

      var newValue = customizer
        ? customizer(object[key], source[key], key, object, source)
        : undefined;

      if (newValue === undefined) {
        newValue = source[key];
      }
      if (isNew) {
        baseAssignValue$1(object, key, newValue);
      } else {
        assignValue$1(object, key, newValue);
      }
    }
    return object;
  }

  /**
   * The base implementation of `_.times` without support for iteratee shorthands
   * or max array length checks.
   *
   * @private
   * @param {number} n The number of times to invoke `iteratee`.
   * @param {Function} iteratee The function invoked per iteration.
   * @returns {Array} Returns the array of results.
   */
  function baseTimes$1(n, iteratee) {
    var index = -1,
        result = Array(n);

    while (++index < n) {
      result[index] = iteratee(index);
    }
    return result;
  }

  /** Used as references for various `Number` constants. */
  var MAX_SAFE_INTEGER$3 = 9007199254740991;

  /** Used to detect unsigned integer values. */
  var reIsUint$1 = /^(?:0|[1-9]\d*)$/;

  /**
   * Checks if `value` is a valid array-like index.
   *
   * @private
   * @param {*} value The value to check.
   * @param {number} [length=MAX_SAFE_INTEGER] The upper bounds of a valid index.
   * @returns {boolean} Returns `true` if `value` is a valid index, else `false`.
   */
  function isIndex$1(value, length) {
    var type = typeof value;
    length = length == null ? MAX_SAFE_INTEGER$3 : length;

    return !!length &&
      (type == 'number' ||
        (type != 'symbol' && reIsUint$1.test(value))) &&
          (value > -1 && value % 1 == 0 && value < length);
  }

  /** Used for built-in method references. */
  var objectProto$o = Object.prototype;

  /** Used to check objects for own properties. */
  var hasOwnProperty$j = objectProto$o.hasOwnProperty;

  /**
   * Creates an array of the enumerable property names of the array-like `value`.
   *
   * @private
   * @param {*} value The value to query.
   * @param {boolean} inherited Specify returning inherited property names.
   * @returns {Array} Returns the array of property names.
   */
  function arrayLikeKeys$1(value, inherited) {
    var isArr = isArray$1(value),
        isArg = !isArr && isArguments$1(value),
        isBuff = !isArr && !isArg && isBuffer$1(value),
        isType = !isArr && !isArg && !isBuff && isTypedArray$1(value),
        skipIndexes = isArr || isArg || isBuff || isType,
        result = skipIndexes ? baseTimes$1(value.length, String) : [],
        length = result.length;

    for (var key in value) {
      if ((inherited || hasOwnProperty$j.call(value, key)) &&
          !(skipIndexes && (
             // Safari 9 has enumerable `arguments.length` in strict mode.
             key == 'length' ||
             // Node.js 0.10 has enumerable non-index properties on buffers.
             (isBuff && (key == 'offset' || key == 'parent')) ||
             // PhantomJS 2 has enumerable non-index properties on typed arrays.
             (isType && (key == 'buffer' || key == 'byteLength' || key == 'byteOffset')) ||
             // Skip index properties.
             isIndex$1(key, length)
          ))) {
        result.push(key);
      }
    }
    return result;
  }

  /**
   * This function is like
   * [`Object.keys`](http://ecma-international.org/ecma-262/7.0/#sec-object.keys)
   * except that it includes inherited enumerable properties.
   *
   * @private
   * @param {Object} object The object to query.
   * @returns {Array} Returns the array of property names.
   */
  function nativeKeysIn$1(object) {
    var result = [];
    if (object != null) {
      for (var key in Object(object)) {
        result.push(key);
      }
    }
    return result;
  }

  /** Used for built-in method references. */
  var objectProto$p = Object.prototype;

  /** Used to check objects for own properties. */
  var hasOwnProperty$k = objectProto$p.hasOwnProperty;

  /**
   * The base implementation of `_.keysIn` which doesn't treat sparse arrays as dense.
   *
   * @private
   * @param {Object} object The object to query.
   * @returns {Array} Returns the array of property names.
   */
  function baseKeysIn$1(object) {
    if (!isObject$1(object)) {
      return nativeKeysIn$1(object);
    }
    var isProto = isPrototype$1(object),
        result = [];

    for (var key in object) {
      if (!(key == 'constructor' && (isProto || !hasOwnProperty$k.call(object, key)))) {
        result.push(key);
      }
    }
    return result;
  }

  /**
   * Creates an array of the own and inherited enumerable property names of `object`.
   *
   * **Note:** Non-object values are coerced to objects.
   *
   * @static
   * @memberOf _
   * @since 3.0.0
   * @category Object
   * @param {Object} object The object to query.
   * @returns {Array} Returns the array of property names.
   * @example
   *
   * function Foo() {
   *   this.a = 1;
   *   this.b = 2;
   * }
   *
   * Foo.prototype.c = 3;
   *
   * _.keysIn(new Foo);
   * // => ['a', 'b', 'c'] (iteration order is not guaranteed)
   */
  function keysIn$1(object) {
    return isArrayLike$2(object) ? arrayLikeKeys$1(object, true) : baseKeysIn$1(object);
  }

  /**
   * Converts `value` to a plain object flattening inherited enumerable string
   * keyed properties of `value` to own properties of the plain object.
   *
   * @static
   * @memberOf _
   * @since 3.0.0
   * @category Lang
   * @param {*} value The value to convert.
   * @returns {Object} Returns the converted plain object.
   * @example
   *
   * function Foo() {
   *   this.b = 2;
   * }
   *
   * Foo.prototype.c = 3;
   *
   * _.assign({ 'a': 1 }, new Foo);
   * // => { 'a': 1, 'b': 2 }
   *
   * _.assign({ 'a': 1 }, _.toPlainObject(new Foo));
   * // => { 'a': 1, 'b': 2, 'c': 3 }
   */
  function toPlainObject$1(value) {
    return copyObject$1(value, keysIn$1(value));
  }

  /**
   * A specialized version of `baseMerge` for arrays and objects which performs
   * deep merges and tracks traversed objects enabling objects with circular
   * references to be merged.
   *
   * @private
   * @param {Object} object The destination object.
   * @param {Object} source The source object.
   * @param {string} key The key of the value to merge.
   * @param {number} srcIndex The index of `source`.
   * @param {Function} mergeFunc The function to merge values.
   * @param {Function} [customizer] The function to customize assigned values.
   * @param {Object} [stack] Tracks traversed source values and their merged
   *  counterparts.
   */
  function baseMergeDeep$1(object, source, key, srcIndex, mergeFunc, customizer, stack) {
    var objValue = safeGet$1(object, key),
        srcValue = safeGet$1(source, key),
        stacked = stack.get(srcValue);

    if (stacked) {
      assignMergeValue$1(object, key, stacked);
      return;
    }
    var newValue = customizer
      ? customizer(objValue, srcValue, (key + ''), object, source, stack)
      : undefined;

    var isCommon = newValue === undefined;

    if (isCommon) {
      var isArr = isArray$1(srcValue),
          isBuff = !isArr && isBuffer$1(srcValue),
          isTyped = !isArr && !isBuff && isTypedArray$1(srcValue);

      newValue = srcValue;
      if (isArr || isBuff || isTyped) {
        if (isArray$1(objValue)) {
          newValue = objValue;
        }
        else if (isArrayLikeObject$1(objValue)) {
          newValue = copyArray$1(objValue);
        }
        else if (isBuff) {
          isCommon = false;
          newValue = cloneBuffer$1(srcValue, true);
        }
        else if (isTyped) {
          isCommon = false;
          newValue = cloneTypedArray$1(srcValue, true);
        }
        else {
          newValue = [];
        }
      }
      else if (isPlainObject$1(srcValue) || isArguments$1(srcValue)) {
        newValue = objValue;
        if (isArguments$1(objValue)) {
          newValue = toPlainObject$1(objValue);
        }
        else if (!isObject$1(objValue) || isFunction$1(objValue)) {
          newValue = initCloneObject$1(srcValue);
        }
      }
      else {
        isCommon = false;
      }
    }
    if (isCommon) {
      // Recursively merge objects and arrays (susceptible to call stack limits).
      stack.set(srcValue, newValue);
      mergeFunc(newValue, srcValue, srcIndex, customizer, stack);
      stack['delete'](srcValue);
    }
    assignMergeValue$1(object, key, newValue);
  }

  /**
   * The base implementation of `_.merge` without support for multiple sources.
   *
   * @private
   * @param {Object} object The destination object.
   * @param {Object} source The source object.
   * @param {number} srcIndex The index of `source`.
   * @param {Function} [customizer] The function to customize merged values.
   * @param {Object} [stack] Tracks traversed source values and their merged
   *  counterparts.
   */
  function baseMerge$1(object, source, srcIndex, customizer, stack) {
    if (object === source) {
      return;
    }
    baseFor$1(source, function(srcValue, key) {
      stack || (stack = new Stack$1);
      if (isObject$1(srcValue)) {
        baseMergeDeep$1(object, source, key, srcIndex, baseMerge$1, customizer, stack);
      }
      else {
        var newValue = customizer
          ? customizer(safeGet$1(object, key), srcValue, (key + ''), object, source, stack)
          : undefined;

        if (newValue === undefined) {
          newValue = srcValue;
        }
        assignMergeValue$1(object, key, newValue);
      }
    }, keysIn$1);
  }

  /**
   * This method returns the first argument it receives.
   *
   * @static
   * @since 0.1.0
   * @memberOf _
   * @category Util
   * @param {*} value Any value.
   * @returns {*} Returns `value`.
   * @example
   *
   * var object = { 'a': 1 };
   *
   * console.log(_.identity(object) === object);
   * // => true
   */
  function identity$1(value) {
    return value;
  }

  /**
   * A faster alternative to `Function#apply`, this function invokes `func`
   * with the `this` binding of `thisArg` and the arguments of `args`.
   *
   * @private
   * @param {Function} func The function to invoke.
   * @param {*} thisArg The `this` binding of `func`.
   * @param {Array} args The arguments to invoke `func` with.
   * @returns {*} Returns the result of `func`.
   */
  function apply$1(func, thisArg, args) {
    switch (args.length) {
      case 0: return func.call(thisArg);
      case 1: return func.call(thisArg, args[0]);
      case 2: return func.call(thisArg, args[0], args[1]);
      case 3: return func.call(thisArg, args[0], args[1], args[2]);
    }
    return func.apply(thisArg, args);
  }

  /* Built-in method references for those with the same name as other `lodash` methods. */
  var nativeMax$1 = Math.max;

  /**
   * A specialized version of `baseRest` which transforms the rest array.
   *
   * @private
   * @param {Function} func The function to apply a rest parameter to.
   * @param {number} [start=func.length-1] The start position of the rest parameter.
   * @param {Function} transform The rest array transform.
   * @returns {Function} Returns the new function.
   */
  function overRest$1(func, start, transform) {
    start = nativeMax$1(start === undefined ? (func.length - 1) : start, 0);
    return function() {
      var args = arguments,
          index = -1,
          length = nativeMax$1(args.length - start, 0),
          array = Array(length);

      while (++index < length) {
        array[index] = args[start + index];
      }
      index = -1;
      var otherArgs = Array(start + 1);
      while (++index < start) {
        otherArgs[index] = args[index];
      }
      otherArgs[start] = transform(array);
      return apply$1(func, this, otherArgs);
    };
  }

  /**
   * Creates a function that returns `value`.
   *
   * @static
   * @memberOf _
   * @since 2.4.0
   * @category Util
   * @param {*} value The value to return from the new function.
   * @returns {Function} Returns the new constant function.
   * @example
   *
   * var objects = _.times(2, _.constant({ 'a': 1 }));
   *
   * console.log(objects);
   * // => [{ 'a': 1 }, { 'a': 1 }]
   *
   * console.log(objects[0] === objects[1]);
   * // => true
   */
  function constant$1(value) {
    return function() {
      return value;
    };
  }

  /**
   * The base implementation of `setToString` without support for hot loop shorting.
   *
   * @private
   * @param {Function} func The function to modify.
   * @param {Function} string The `toString` result.
   * @returns {Function} Returns `func`.
   */
  var baseSetToString$1 = !defineProperty$1 ? identity$1 : function(func, string) {
    return defineProperty$1(func, 'toString', {
      'configurable': true,
      'enumerable': false,
      'value': constant$1(string),
      'writable': true
    });
  };

  /** Used to detect hot functions by number of calls within a span of milliseconds. */
  var HOT_COUNT$1 = 800,
      HOT_SPAN$1 = 16;

  /* Built-in method references for those with the same name as other `lodash` methods. */
  var nativeNow$1 = Date.now;

  /**
   * Creates a function that'll short out and invoke `identity` instead
   * of `func` when it's called `HOT_COUNT` or more times in `HOT_SPAN`
   * milliseconds.
   *
   * @private
   * @param {Function} func The function to restrict.
   * @returns {Function} Returns the new shortable function.
   */
  function shortOut$1(func) {
    var count = 0,
        lastCalled = 0;

    return function() {
      var stamp = nativeNow$1(),
          remaining = HOT_SPAN$1 - (stamp - lastCalled);

      lastCalled = stamp;
      if (remaining > 0) {
        if (++count >= HOT_COUNT$1) {
          return arguments[0];
        }
      } else {
        count = 0;
      }
      return func.apply(undefined, arguments);
    };
  }

  /**
   * Sets the `toString` method of `func` to return `string`.
   *
   * @private
   * @param {Function} func The function to modify.
   * @param {Function} string The `toString` result.
   * @returns {Function} Returns `func`.
   */
  var setToString$1 = shortOut$1(baseSetToString$1);

  /**
   * The base implementation of `_.rest` which doesn't validate or coerce arguments.
   *
   * @private
   * @param {Function} func The function to apply a rest parameter to.
   * @param {number} [start=func.length-1] The start position of the rest parameter.
   * @returns {Function} Returns the new function.
   */
  function baseRest$1(func, start) {
    return setToString$1(overRest$1(func, start, identity$1), func + '');
  }

  /**
   * Checks if the given arguments are from an iteratee call.
   *
   * @private
   * @param {*} value The potential iteratee value argument.
   * @param {*} index The potential iteratee index or key argument.
   * @param {*} object The potential iteratee object argument.
   * @returns {boolean} Returns `true` if the arguments are from an iteratee call,
   *  else `false`.
   */
  function isIterateeCall$1(value, index, object) {
    if (!isObject$1(object)) {
      return false;
    }
    var type = typeof index;
    if (type == 'number'
          ? (isArrayLike$2(object) && isIndex$1(index, object.length))
          : (type == 'string' && index in object)
        ) {
      return eq$1(object[index], value);
    }
    return false;
  }

  /**
   * Creates a function like `_.assign`.
   *
   * @private
   * @param {Function} assigner The function to assign values.
   * @returns {Function} Returns the new assigner function.
   */
  function createAssigner$1(assigner) {
    return baseRest$1(function(object, sources) {
      var index = -1,
          length = sources.length,
          customizer = length > 1 ? sources[length - 1] : undefined,
          guard = length > 2 ? sources[2] : undefined;

      customizer = (assigner.length > 3 && typeof customizer == 'function')
        ? (length--, customizer)
        : undefined;

      if (guard && isIterateeCall$1(sources[0], sources[1], guard)) {
        customizer = length < 3 ? undefined : customizer;
        length = 1;
      }
      object = Object(object);
      while (++index < length) {
        var source = sources[index];
        if (source) {
          assigner(object, source, index, customizer);
        }
      }
      return object;
    });
  }

  /**
   * This method is like `_.assign` except that it recursively merges own and
   * inherited enumerable string keyed properties of source objects into the
   * destination object. Source properties that resolve to `undefined` are
   * skipped if a destination value exists. Array and plain object properties
   * are merged recursively. Other objects and value types are overridden by
   * assignment. Source objects are applied from left to right. Subsequent
   * sources overwrite property assignments of previous sources.
   *
   * **Note:** This method mutates `object`.
   *
   * @static
   * @memberOf _
   * @since 0.5.0
   * @category Object
   * @param {Object} object The destination object.
   * @param {...Object} [sources] The source objects.
   * @returns {Object} Returns `object`.
   * @example
   *
   * var object = {
   *   'a': [{ 'b': 2 }, { 'd': 4 }]
   * };
   *
   * var other = {
   *   'a': [{ 'c': 3 }, { 'e': 5 }]
   * };
   *
   * _.merge(object, other);
   * // => { 'a': [{ 'b': 2, 'c': 3 }, { 'd': 4, 'e': 5 }] }
   */
  var merge$1 = createAssigner$1(function(object, source, srcIndex) {
    baseMerge$1(object, source, srcIndex);
  });

  // create function to construct the config entry so we don't need to keep building object
  // import checkIsBoolean from '../boolean'
  // import debug from 'debug';
  // const debugFn = debug('jsonql-params-validator:construct-config');
  /**
   * @param {*} args value
   * @param {string} type for value
   * @param {boolean} [optional=false]
   * @param {boolean|array} [enumv=false]
   * @param {boolean|function} [checker=false]
   * @return {object} config entry
   */
  function constructConfig$1(args, type, optional, enumv, checker, alias) {
    if ( optional === void 0 ) optional=false;
    if ( enumv === void 0 ) enumv=false;
    if ( checker === void 0 ) checker=false;
    if ( alias === void 0 ) alias=false;

    var base = {};
    base[ARGS_KEY$1] = args;
    base[TYPE_KEY$1] = type;
    if (optional === true) {
      base[OPTIONAL_KEY$1] = true;
    }
    if (checkIsArray$1(enumv)) {
      base[ENUM_KEY$1] = enumv;
    }
    if (isFunction$1(checker)) {
      base[CHECKER_KEY$1] = checker;
    }
    if (isString$3(alias)) {
      base[ALIAS_KEY$1] = alias;
    }
    return base
  }

  // export also create wrapper methods

  /**
   * This has a different interface
   * @param {*} value to supply
   * @param {string|array} type for checking
   * @param {object} params to map against the config check
   * @param {array} params.enumv NOT enum
   * @param {boolean} params.optional false then nothing
   * @param {function} params.checker need more work on this one later
   * @param {string} params.alias mostly for cmd
   */
  var createConfig$2 = function (value, type, params) {
    if ( params === void 0 ) params = {};

    // Note the enumv not ENUM
    // const { enumv, optional, checker, alias } = params;
    // let args = [value, type, optional, enumv, checker, alias];
    var o = params[OPTIONAL_KEY$1];
    var e = params[ENUM_KEY$1];
    var c = params[CHECKER_KEY$1];
    var a = params[ALIAS_KEY$1];
    return constructConfig$1.apply(null,  [value, type, o, e, c, a])
  };

  // export

  var createConfig$3 = createConfig$2;

  var obj$9;

  var AVAILABLE_PLACES = [
    TOKEN_IN_URL$1,
    TOKEN_IN_HEADER$1
  ];

  // constant props
  var wsClientConstProps = {
    version: 'version: 1.2.0 module: umd', // will get replace
    serverType: JS_WS_NAME
  };

  var wsClientCheckMap = {};
  wsClientCheckMap[TOKEN_DELIVER_LOCATION_PROP_KEY$1] = createConfig$3(TOKEN_IN_URL$1, [STRING_TYPE$1], ( obj$9 = {}, obj$9[ENUM_KEY$1] = AVAILABLE_PLACES, obj$9 ));

  // this is all the isormophic-ws is
  var ws = null;

  if (typeof WebSocket !== 'undefined') {
    ws = WebSocket;
  } else if (typeof MozWebSocket !== 'undefined') {
    ws = MozWebSocket;
  } else if (typeof global !== 'undefined') {
    ws = global.WebSocket || global.MozWebSocket;
  } else if (typeof window !== 'undefined') {
    ws = window.WebSocket || window.MozWebSocket;
  } else if (typeof self !== 'undefined') {
    ws = self.WebSocket || self.MozWebSocket;
  }

  var ws$1 = ws;

  function createCommonjsModule(fn, module) {
  	return module = { exports: {} }, fn(module, module.exports), module.exports;
  }

  var js_cookie = createCommonjsModule(function (module, exports) {
  (function (factory) {
  	var registeredInModuleLoader;
  	{
  		module.exports = factory();
  		registeredInModuleLoader = true;
  	}
  	if (!registeredInModuleLoader) {
  		var OldCookies = window.Cookies;
  		var api = window.Cookies = factory();
  		api.noConflict = function () {
  			window.Cookies = OldCookies;
  			return api;
  		};
  	}
  }(function () {
  	function extend () {
  		var arguments$1 = arguments;

  		var i = 0;
  		var result = {};
  		for (; i < arguments.length; i++) {
  			var attributes = arguments$1[ i ];
  			for (var key in attributes) {
  				result[key] = attributes[key];
  			}
  		}
  		return result;
  	}

  	function decode (s) {
  		return s.replace(/(%[0-9A-Z]{2})+/g, decodeURIComponent);
  	}

  	function init (converter) {
  		function api() {}

  		function set (key, value, attributes) {
  			if (typeof document === 'undefined') {
  				return;
  			}

  			attributes = extend({
  				path: '/'
  			}, api.defaults, attributes);

  			if (typeof attributes.expires === 'number') {
  				attributes.expires = new Date(new Date() * 1 + attributes.expires * 864e+5);
  			}

  			// We're using "expires" because "max-age" is not supported by IE
  			attributes.expires = attributes.expires ? attributes.expires.toUTCString() : '';

  			try {
  				var result = JSON.stringify(value);
  				if (/^[\{\[]/.test(result)) {
  					value = result;
  				}
  			} catch (e) {}

  			value = converter.write ?
  				converter.write(value, key) :
  				encodeURIComponent(String(value))
  					.replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);

  			key = encodeURIComponent(String(key))
  				.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent)
  				.replace(/[\(\)]/g, escape);

  			var stringifiedAttributes = '';
  			for (var attributeName in attributes) {
  				if (!attributes[attributeName]) {
  					continue;
  				}
  				stringifiedAttributes += '; ' + attributeName;
  				if (attributes[attributeName] === true) {
  					continue;
  				}

  				// Considers RFC 6265 section 5.2:
  				// ...
  				// 3.  If the remaining unparsed-attributes contains a %x3B (";")
  				//     character:
  				// Consume the characters of the unparsed-attributes up to,
  				// not including, the first %x3B (";") character.
  				// ...
  				stringifiedAttributes += '=' + attributes[attributeName].split(';')[0];
  			}

  			return (document.cookie = key + '=' + value + stringifiedAttributes);
  		}

  		function get (key, json) {
  			if (typeof document === 'undefined') {
  				return;
  			}

  			var jar = {};
  			// To prevent the for loop in the first place assign an empty array
  			// in case there are no cookies at all.
  			var cookies = document.cookie ? document.cookie.split('; ') : [];
  			var i = 0;

  			for (; i < cookies.length; i++) {
  				var parts = cookies[i].split('=');
  				var cookie = parts.slice(1).join('=');

  				if (!json && cookie.charAt(0) === '"') {
  					cookie = cookie.slice(1, -1);
  				}

  				try {
  					var name = decode(parts[0]);
  					cookie = (converter.read || converter)(cookie, name) ||
  						decode(cookie);

  					if (json) {
  						try {
  							cookie = JSON.parse(cookie);
  						} catch (e) {}
  					}

  					jar[name] = cookie;

  					if (key === name) {
  						break;
  					}
  				} catch (e) {}
  			}

  			return key ? jar[key] : jar;
  		}

  		api.set = set;
  		api.get = function (key) {
  			return get(key, false /* read as raw */);
  		};
  		api.getJSON = function (key) {
  			return get(key, true /* read as json */);
  		};
  		api.remove = function (key, attributes) {
  			set(key, '', extend(attributes, {
  				expires: -1
  			}));
  		};

  		api.defaults = {};

  		api.withConverter = init;

  		return api;
  	}

  	return init(function () {});
  }));
  });

  // bunch of generic helpers

  /**
   * DIY in Array
   * @param {array} arr to check from
   * @param {*} value to check against
   * @return {boolean} true on found
   */
  var inArray$3 = function (arr, value) { return !!arr.filter(function (a) { return a === value; }).length; };

  /**
   * parse string to json or just return the original value if error happened
   * @param {*} n input
   * @param {boolean} [t=true] or throw
   * @return {*} json object on success
   */
  var parseJson$1 = function(n, t) {
    if ( t === void 0 ) t=true;

    try {
      return JSON.parse(n)
    } catch(e) {
      if (t) {
        return n
      }
      throw new Error(e)
    }
  };

  /**
   * @param {object} obj for search
   * @param {string} key target
   * @return {boolean} true on success
   */
  var isObjectHasKey$2 = function(obj, key) {
    try {
      var keys = Object.keys(obj);
      return inArray$3(keys, key)
    } catch(e) {
      // @BUG when the obj is not an OBJECT we got some weird output
      return false
    }
  };

  /**
   * create a event name
   * @param {string[]} args
   * @return {string} event name for use
   */
  var createEvt$1 = function () {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

    return args.join('_');
  };

  /**
   * small util to make sure the return value is valid JSON object
   * @param {*} n input
   * @return {object} correct JSON object
   */
  var toJson$1 = function (n) {
    if (typeof n === 'string') {
      return parseJson$1(n)
    }
    return parseJson$1(JSON.stringify(n))
  };

  /**
   * Simple check if the prop is function
   * @param {*} prop input
   * @return {boolean} true on success
   */
  var isFunc$1 = function (prop) {
    if (typeof prop === 'function') {
      return true;
    }
    console.error(("Expect to be Function type! Got " + (typeof prop)));
  };
    
  /** 
   * generic placeholder function
   * @return {boolean} false 
   */
  var nil$1 = function () { return false; };

  // break it out on its own because

  /**
   * previously we already make sure the order of the namespaces
   * and attach the auth client to it
   * @param {array} promises array of unresolved promises
   * @param {boolean} asObject if true then merge the result object
   * @return {object} promise resolved with the array of promises resolved results
   */
  function chainPromises(promises, asObject) {
    if ( asObject === void 0 ) asObject = false;

    return promises.reduce(function (promiseChain, currentTask) { return (
      promiseChain.then(function (chainResults) { return (
        currentTask.then(function (currentResult) { return (
          asObject === false ? chainResults.concat( [currentResult]) : merge$1(chainResults, currentResult)
        ); })
      ); })
    ); }, Promise.resolve(
      asObject === false ? [] : (isPlainObject$1(asObject) ? asObject : {})
    ))
  }

  /**
   * @param {boolean} sec return in second or not
   * @return {number} timestamp
   */
  var timestamp$1 = function (sec) {
    if ( sec === void 0 ) sec = false;

    var time = Date.now();
    return sec ? Math.floor( time / 1000 ) : time
  };

  // ported from jsonql-params-validator

  /**
   * @param {*} args arguments to send
   *@return {object} formatted payload
   */
  var formatPayload$1 = function (args) {
    var obj;

    return (
    ( obj = {}, obj[QUERY_ARG_NAME$1] = args, obj )
  );
  };

  /**
   * wrapper method to add the timestamp as well
   * @param {string} resolverName name of the resolver
   * @param {*} payload what is sending 
   * @param {object} extra additonal property we want to merge into the deliverable
   * @return {object} delierable
   */
  function createDeliverable$1(resolverName, payload, extra) {
    var obj;

    if ( extra === void 0 ) extra = {};
    return Object.assign(( obj = {}, obj[resolverName] = payload, obj[TIMESTAMP_PARAM_NAME$1] = [ timestamp$1() ], obj ), extra)
  }

  /**
   * @param {string} resolverName name of function
   * @param {array} [args=[]] from the ...args
   * @param {boolean} [jsonp = false] add v1.3.0 to koa
   * @return {object} formatted argument
   */
  function createQuery$1(resolverName, args, jsonp) {
    if ( args === void 0 ) args = [];
    if ( jsonp === void 0 ) jsonp = false;

    if (isString$3(resolverName) && isArray$1(args)) {
      var payload = formatPayload$1(args);
      if (jsonp === true) {
        return payload
      }
      return createDeliverable$1(resolverName, payload)
    }
    throw new JsonqlValidationError$1('utils:params-api:createQuery', { 
      message: "expect resolverName to be string and args to be array!",
      resolverName: resolverName, 
      args: args 
    })
  }

  /**
   * string version of the createQuery
   * @return {string}
   */
  function createQueryStr$1(resolverName, args, jsonp) {
    if ( args === void 0 ) args = [];
    if ( jsonp === void 0 ) jsonp = false;

    return JSON.stringify(createQuery$1(resolverName, args, jsonp))
  }

  // There are the socket related methods ported back from 

  var PAYLOAD_NOT_DECODED_ERR$1 = 'payload can not decoded';
  var WS_KEYS$1 = [
    WS_REPLY_TYPE$1,
    WS_EVT_NAME$1,
    WS_DATA_NAME$1
  ];

  /**
   * @param {string|object} payload should be string when reply but could be transformed
   * @return {boolean} true is OK
   */
  var isWsReply$1 = function (payload) {
    var json = isString$3(payload) ? toJson$1(payload) : payload;
    var data = json.data;
    if (data) {
      var result = WS_KEYS$1.filter(function (key) { return isObjectHasKey$2(data, key); });
      return (result.length === WS_KEYS$1.length) ? data : false
    }
    return false
  };

  /**
   * @param {string|object} data received data
   * @param {function} [cb=nil] this is for extracting the TS field or when it's error
   * @return {object} false on failed
   */
  var extractWsPayload$1 = function (payload, cb) {
    if ( cb === void 0 ) cb = nil$1;

    try {
      var json = toJson$1(payload);
      // now handle the data
      var _data;
      if ((_data = isWsReply$1(json)) !== false) {
        // note the ts property is on its own 
        cb('_data', _data);
        
        return {
          data: toJson$1(_data[WS_DATA_NAME$1]),
          resolverName: _data[WS_EVT_NAME$1],
          type: _data[WS_REPLY_TYPE$1]
        }
      }
      throw new JsonqlError$2(PAYLOAD_NOT_DECODED_ERR$1, payload)
    } catch(e) {
      return cb(ERROR_KEY$1, e)
    }
  };

  /**
   * This will get re-use when we start using the disconnect event method
   * @param {*} ws 
   * @return {void}
   */
  function disconnect(ws) {
    if (ws.terminate && isFunc$1(ws.terminate)) {
      ws.terminate();
    } else if (ws.close && isFunc$1(ws.close)) {
      ws.close();
    }
  }

  // pass the different type of ws to generate the client

  /**
   * 
   * @param {*} WebSocketClass 
   * @param {*} url 
   * @param {*} type 
   * @param {*} options 
   */
  function createWs(WebSocketClass, type, url, options) {
    if (type === 'browser') {
      var headers = options.headers; 
      if (headers) {
        for (var key in headers) {
          if (!js_cookie.get(key)) {
            js_cookie.set(key, headers[key]);
          }
        }
      }
    }
    var wsUrl = fixWss(url);
    return type === 'node' ? new WebSocketClass(wsUrl, options) : new WebSocketClass(wsUrl) 
  }

  /**
   * Group the ping and get respond create new client in one
   * @param {object} ws 
   * @param {object} WebSocket 
   * @param {string} url
   * @param {function} resolver 
   * @param {function} rejecter 
   * @param {boolean} auth client or not
   * @return {promise} resolve the confirm client
   */
  function initPingAction(ws, WebSocketClass, type, url, wsOptions, resolver, rejecter) {

    // @TODO how to we id this client can issue a CSRF
    // by origin? 
    ws.onopen = function onOpenCallback() {
      ws.send(createInitPing());
    };

    ws.onmessage = function onMessageCallback(payload) {
      try {
        var header = extractPingResult(payload.data);
        // delay or not show no different but just on the safe side
        setTimeout(function () { 
          disconnect(ws);
        }, 50);
        var newWs = createWs(WebSocketClass, type, url, Object.assign(wsOptions, header)); 
        
        resolver(newWs);  
        
      } catch(e) {
        rejecter(e);
      }
    };

    ws.onerror = function onErrorCallback(err) {
      rejecter(err);
    };
  }

  /**
   * less duplicated code the better 
   * @param {object} WebSocket 
   * @param {string} type we need to change the way how it deliver header for different platform
   * @param {string} url formatted url
   * @param {object} options or not
   * @return {promise} resolve the actual verified client
   */
  function asyncConnect(WebSocketClass, type, url, options) {
    
    return new Promise(function (resolver, rejecter) {  
      var unconfirmClient = createWs(WebSocketClass, type, url, options);
                                
      return initPingAction(unconfirmClient, WebSocketClass, type, url, options, resolver, rejecter)
    })
  }

  /**
   * The bug was in the wsOptions where ws don't need it but socket.io do
   * therefore the object was pass as second parameter!
   * @NOTE here we only return a method to create the client, it might not get call 
   * @param {object} WebSocket the client or node version of ws
   * @param {string} [type = 'browser'] we need to tell if this is browser or node  
   * @param {boolean} [auth = false] if it's auth then 3 param or just one
   * @return {function} the client method to connect to the ws socket server
   */
  function setupWebsocketClientFn(WebSocketClass, type, auth) {
    if ( type === void 0 ) type = 'browser';
    if ( auth === void 0 ) auth = false;

    if (auth === false) {
      /**
       * Create a non-protected client
       * @param {string} uri already constructed url  
       * @param {object} config from the ws-client-core this will be wsOptions taken out from opts 
       * @return {promise} resolve to the confirmed client
       */
      return function createWsClient(uri, config) {
        // const { log } = config
        var ref = prepareConnectConfig(uri, config, false);
        var url = ref.url;
        var opts = ref.opts;

        return asyncConnect(WebSocketClass, type, url, opts)
      }
    }

    /**
     * Create a client with auth token
     * @param {string} uri start with ws:// @TODO check this?
     * @param {object} config this is the full configuration because we need something from it
     * @param {string} token the jwt token
     * @return {object} ws instance
     */
    return function createWsAuthClient(uri, config, token) {
      // const { log } = config
      var ref = prepareConnectConfig(uri, config, token);
      var url = ref.url;
      var opts = ref.opts;

      return asyncConnect(WebSocketClass, type, url, opts)
    }
  }

  // @BUG when call disconnected

  /**
   * when we received a login event 
   * from the http-client or the standalone login call 
   * we received a token here --> update the opts then trigger 
   * the CONNECT_EVENT_NAME again
   * @param {object} opts configurations
   * @param {object} nspMap contain all the required info
   * @param {object} ee event emitter
   * @return {void}
   */
  function loginEventListener(opts, nspMap, ee) {
    var log = opts.log;
    var namespaces = nspMap.namespaces;

    log("[4] loginEventHandler");

    ee.$only(LOGIN_EVENT_NAME$1, function loginEventHandlerCallback(tokenFromLoginAction) {

      log('createClient LOGIN_EVENT_NAME $only handler');
      // @NOTE this is wrong because whenever start it will connect the to the public 
      // but when login it should just start binding the private event
      clearMainEmitEvt(ee, namespaces);
      // reload the nsp and rebind all the events
      opts.token = tokenFromLoginAction; 
      ee.$trigger(CONNECT_EVENT_NAME$1, [opts, ee]); // don't need to pass the nspMap 
    });
  }

  // actually binding the event client to the socket client

  /**
   * Because the nsps can be throw away so it doesn't matter the scope
   * this will get reuse again
   * @NOTE when we enable the standalone method this sequence will not change 
   * only call and reload
   * @param {object} opts configuration
   * @param {object} nspMap from contract
   * @param {string|null} token whether we have the token at run time
   * @return {promise} resolve the nsps namespace with namespace as key
   */
  var createNsp = function(opts, nspMap, token) {
    if ( token === void 0 ) token = null;

    // we leave the token param out because it could get call by another method
    token = token || opts.token; 
    var publicNamespace = nspMap.publicNamespace;
    var namespaces = nspMap.namespaces;
    var log = opts.log; 
    log("createNspAction", 'publicNamespace', publicNamespace, 'namespaces', namespaces);
    
    // reverse the namespaces because it got stuck for some reason
    // const reverseNamespaces = namespaces.reverse()
    if (opts.enableAuth) {
      return chainPromises(
        namespaces.map(function (namespace, i) {
          if (i === 0) {
            if (token) {
              opts.token = token;
              log('create createNspAuthClient at run time');
              return createNspAuthClient(namespace, opts)
            }
            return Promise.resolve(false)
          }
          return createNspClient(namespace, opts)
        })
      )
      .then(function (results) { return results.map(function (result, i) {
            var obj;

            return (( obj = {}, obj[namespaces[i]] = result, obj ));
          })
            .reduce(function (a, b) { return Object.assign(a, b); }, {}); }
      )
    }
    
    return createNspClient(false, opts)
      .then(function (nsp) {
        var obj;

        return (( obj = {}, obj[publicNamespace] = nsp, obj ));
    })
  };

  // taken out from the bind-socket-event-handler 

  /**
   * This is the actual logout (terminate socket connection) handler 
   * There is another one that is handle what should do when this happen 
   * @param {object} ee eventEmitter
   * @param {object} ws the WebSocket instance
   * @return {void}
   */
  function disconnectEventListener(ee, ws) {
    // listen to the LOGOUT_EVENT_NAME when this is a private nsp
    ee.$on(DISCONNECT_EVENT_NAME$1, function closeEvtHandler() {
      try {
        // @TODO we need find a way to get the userdata
        ws.send(createIntercomPayload(LOGOUT_EVENT_NAME));
        log('terminate ws connection');
        ws.terminate();
      } catch(e) {
        console.error('ws.terminate error', e);
      }
    });
  }

  // the WebSocket main handler

  /**
   * in some edge case we might not even have a resolverName, then
   * we issue a global error for the developer to catch it
   * @param {object} ee event emitter
   * @param {string} namespace nsp
   * @param {string} resolverName resolver
   * @param {object} json decoded payload or error object
   * @return {undefined} nothing return
   */
  var errorTypeHandler = function (ee, namespace, resolverName, json) {
    var evt = [namespace];
    if (resolverName) {
      evt.push(resolverName);
    }
    evt.push(ON_ERROR_FN_NAME$1);
    var evtName = Reflect.apply(createEvt$1, null, evt);
    // test if there is a data field
    var payload = json.data || json;
    ee.$trigger(evtName, [payload]);
  };

  /**
   * Binding the event to socket normally
   * @param {string} namespace
   * @param {object} ws the nsp
   * @param {object} ee EventEmitter
   * @param {boolean} isPrivate to id if this namespace is private or not
   * @param {object} opts configuration
   * @param {number} ctnLeft in the namespaceEventListener count down how many namespace left to call 
   * @return {object} promise resolve after the onopen event
   */
  function bindSocketEventHandler(namespace, ws, ee, isPrivate, opts, ctnLeft) {
    var log = opts.log;
    // setup the logut event listener 
    // this will hear the event and actually call the ws.terminate
    if (isPrivate) {
      log('Private namespace', namespace, ' binding to the DISCONNECT ws.terminate');
      disconnectEventListener(ee, ws);
    }
    // log(`log test, isPrivate:`, isPrivate)
    // connection open
    ws.onopen = function onOpenCallback() {

      log('client.ws.onopen listened -->', namespace);
      /*
      This is the change from before about the hooks 
      now only the public namespace will get trigger the onReady 
      and the private one will get the onLogin 
      this way, we won't get into the problem of multiple event 
      get trigger, and for developer they just have to place 
      the callback inside the right hook
      */
      if (isPrivate) {
        log(("isPrivate and fire the " + ON_LOGIN_FN_NAME$1));
        ee.$call(ON_LOGIN_FN_NAME$1)(namespace);
      } else {
        ee.$call(ON_READY_FN_NAME$1)(namespace, ctnLeft);
        // The namespaceEventListener will count this for here
        // and this count will only count the public namespace 
        // it will always be 1 for now
        log("isPublic onReady hook executed", ctnLeft);
        if (ctnLeft === 0) {
          ee.$off(ON_READY_FN_NAME$1);
        }
      }
      // add listener only after the open is called
      ee.$only(
        createEvt$1(namespace, EMIT_REPLY_TYPE$1),
        /**
         * actually send the payload to server
         * @param {string} resolverName
         * @param {array} args NEED TO CHECK HOW WE PASS THIS!
         */
        function wsMainOnEvtHandler(resolverName, args) {
          var payload = createQueryStr$1(resolverName, args);
          log('ws.onopen.send', resolverName, args, payload);

          ws.send(payload);
        }
      );
    };

    // reply
    // If we change it to the event callback style
    // then the payload will just be the payload and fucks up the extractWsPayload call @TODO
    ws.onmessage = function onMessageCallback(payload) {

      log("client.ws.onmessage raw payload", payload.data);
      
      // console.log(`on.message`, typeof payload, payload)
      try {
        // log(`ws.onmessage raw payload`, payload)
        // @TODO the payload actually contain quite a few things - is that changed?
        // type: message, data: data_send_from_server
        var json = extractWsPayload$1(payload.data);
        var resolverName = json.resolverName;
        var type = json.type;

        log('Respond from server', type, json);

        switch (type) {
          case EMIT_REPLY_TYPE$1:
            var e1 = createEvt$1(namespace, resolverName, ON_MESSAGE_FN_NAME$1);
            var r = ee.$call(e1)(json);
            
            log("EMIT_REPLY_TYPE", e1, r);
            break
          case ACKNOWLEDGE_REPLY_TYPE:
            var e2 = createEvt$1(namespace, resolverName, ON_RESULT_FN_NAME$1);
            var x2 = ee.$call(e2)(json);

            log("ACKNOWLEDGE_REPLY_TYPE", e2, x2);
            break
          case ERROR_KEY$1:
            // this is handled error and we won't throw it
            // we need to extract the error from json
            log("ERROR_KEY");
            errorTypeHandler(ee, namespace, resolverName, json);
            break
          // @TODO there should be an error type instead of roll into the other two types? TBC
          default:
            // if this happen then we should throw it and halt the operation all together
            log('Unhandled event!', json);
            errorTypeHandler(ee, namespace, resolverName, json);
            // let error = {error: {'message': 'Unhandled event!', type}};
            // ee.$trigger(createEvt(namespace, resolverName, ON_RESULT_FN_NAME), [error])
        }
      } catch(e) {
        log("client.ws.onmessage error", e);
        errorTypeHandler(ee, namespace, false, e);
      }
    };
    // when the server close the connection
    ws.onclose = function onCloseCallback() {
      log('client.ws.onclose callback');
      // @TODO what to do with this
      // ee.$trigger(LOGOUT_EVENT_NAME, [namespace])
    };
    // add a onerror event handler here
    ws.onerror = function onErrorCallback(err) {
      // trigger a global error event
      log("client.ws.onerror", err);
      handleNamespaceOnError(ee, namespace, err);
    };
    
    // we don't bind the logut here and just return the ws 
    return ws 
  }

  // take out from the bind-framework-to-jsonql 

  /**
   * This is the hard of establishing the connection and binding to the jsonql events 
   * @param {*} nspMap 
   * @param {*} ee event emitter
   * @param {function} log function to show internal 
   * @return {void}
   */
  function connectEventListener(nspMap, ee, log) {
    log("[2] setup the CONNECT_EVENT_NAME");
    // this is a automatic trigger from within the framework
    ee.$only(CONNECT_EVENT_NAME$1, function connectEventNameHandler($config, $ee) {
      log("[3] CONNECT_EVENT_NAME", $config);

      return createNsp($config, nspMap)
              .then(function (nsps) { return namespaceEventListener(bindSocketEventHandler, nsps); })
              .then(function (listenerFn) { return listenerFn($config, nspMap, $ee); })
    });
  }

  // share method to create the wsClientResolver

  /**
   * Create the framework <---> jsonql client binding
   * @param {object} WebSocketClass the different WebSocket module
   * @param {string} [type=browser] we need different setup for browser or node
   * @return {function} the wsClientResolver
   */
  function setupConnectClient(WebSocketClass, type) {
    if ( type === void 0 ) type = 'browser';

    /**
     * wsClientResolver
     * @param {object} opts configuration
     * @param {object} nspMap from the contract
     * @param {object} ee instance of the eventEmitter
     * @return {object} passing the same 3 input out with additional in the opts
     */
    return function createClientBindingAction(opts, nspMap, ee) {
      var log = opts.log;

      log("There is problem here with passing the opts", opts);
      // this will put two callable methods into the opts 
      opts[NSP_CLIENT] = setupWebsocketClientFn(WebSocketClass, type);
      // we don't need this one unless enableAuth === true 
      if (opts[ENABLE_AUTH_PROP_KEY$1] === true) {
        opts[NSP_AUTH_CLIENT] = setupWebsocketClientFn(WebSocketClass, type, true);
      }    
      // debug 
      log("[1] bindWebsocketToJsonql", ee.$name, nspMap);
      // @2020-03-20 @NOTE 
      
      connectEventListener(nspMap, ee, log);
    
      // next we need to setup the login event handler
      // But the same design (see above) when we received a login event 
      // from the http-client or the standalone login call 
      // we received a token here --> update the opts then trigger 
      // the CONNECT_EVENT_NAME again
      loginEventListener(opts, nspMap, ee);

      log("just before returing the values for the next operation from createClientBindingAction");

      // we just return what comes in
      return { opts: opts, nspMap: nspMap, ee: ee }
    }
  }

  // this will be the news style interface that will pass to the jsonql-ws-client

  var setupSocketClientListener = setupConnectClient(ws$1);

  // this is the module entry point for ES6 for client

  // export back the function and that's it
  function wsBrowserClient(config, constProps) {
    if ( config === void 0 ) config = {};
    if ( constProps === void 0 ) constProps = {};

    return wsClientCore(
      setupSocketClientListener, 
      wsClientCheckMap, 
      Object.assign({}, wsClientConstProps, constProps)
    )(config)
  }

  // just export interface for browser

  return wsBrowserClient;

})));
//# sourceMappingURL=jsonql-ws-client.umd.js.map
