

/**
 * @TODO need to decided if we actually need this or not
 * Disconnect from the server
 * @param {object} ee event emitter 
 * @param {object} nsps namespace as key
 * @param {string} type of server
 */
function disconnect(ee, nsps) {
  try {
    // @TODO need to figure out a better way here?
    const method = 'terminate'
    for (let namespace in nsps) {
      let nsp = nsps[namespace]
      // we send the disconnect call to the server 

      // then issue a disconnect event

      if (nsp && nsp[method]) {
        Reflect.apply(nsp[method], null, [])
      }
    }
  } catch(e) {
    // socket.io throw a this.destroy of undefined?
    console.error('Disconnect call failed', e)
  }
}

export { disconnect }