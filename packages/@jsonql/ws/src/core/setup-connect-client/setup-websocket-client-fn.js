// pass the different type of ws to generate the client
// this is where the framework specific code get injected
import cookies from 'js-cookie'
import { disconnect } from '../disconnect'
import {
  createInitPing, 
  extractPingResult,
  prepareConnectConfig,
  fixWss
} from '../modules'

/**
 * 
 * @param {*} WebSocketClass 
 * @param {*} url 
 * @param {*} type 
 * @param {*} options 
 */
function createWs(WebSocketClass, type, url, options) {
  if (type === 'browser') {
    const { headers } = options 
    if (headers) {
      for (let key in headers) {
        if (!cookies.get(key)) {
          cookies.set(key, headers[key])
        }
      }
    }
  }
  let wsUrl = fixWss(url)
  return type === 'node' ? new WebSocketClass(wsUrl, options) : new WebSocketClass(wsUrl) 
}

/**
 * Group the ping and get respond create new client in one
 * @param {object} ws 
 * @param {object} WebSocket 
 * @param {string} url
 * @param {function} resolver 
 * @param {function} rejecter 
 * @param {boolean} auth client or not
 * @return {promise} resolve the confirm client
 */
function initPingAction(ws, WebSocketClass, type, url, wsOptions, resolver, rejecter) {

  // @TODO how to we id this client can issue a CSRF
  // by origin? 
  ws.onopen = function onOpenCallback() {
    ws.send(createInitPing())
  }

  ws.onmessage = function onMessageCallback(payload) {
    try {
      const header = extractPingResult(payload.data)
      // delay or not show no different but just on the safe side
      setTimeout(() => { 
        disconnect(ws)
      }, 50)
      const newWs = createWs(WebSocketClass, type, url, Object.assign(wsOptions, header)) 
      
      resolver(newWs)  
      
    } catch(e) {
      rejecter(e)
    }
  }

  ws.onerror = function onErrorCallback(err) {
    rejecter(err)
  }
}

/**
 * less duplicated code the better 
 * @param {object} WebSocket 
 * @param {string} type we need to change the way how it deliver header for different platform
 * @param {string} url formatted url
 * @param {object} options or not
 * @return {promise} resolve the actual verified client
 */
function asyncConnect(WebSocketClass, type, url, options) {
  
  return new Promise((resolver, rejecter) => {  
    const unconfirmClient = createWs(WebSocketClass, type, url, options)
                              
    return initPingAction(unconfirmClient, WebSocketClass, type, url, options, resolver, rejecter)
  })
}

/**
 * The bug was in the wsOptions where ws don't need it but socket.io do
 * therefore the object was pass as second parameter!
 * @NOTE here we only return a method to create the client, it might not get call 
 * @param {object} WebSocket the client or node version of ws
 * @param {string} [type = 'browser'] we need to tell if this is browser or node  
 * @param {boolean} [auth = false] if it's auth then 3 param or just one
 * @return {function} the client method to connect to the ws socket server
 */
function setupWebsocketClientFn(WebSocketClass, type = 'browser', auth = false) {
  if (auth === false) {
    /**
     * Create a non-protected client
     * @param {string} uri already constructed url  
     * @param {object} config from the ws-client-core this will be wsOptions taken out from opts 
     * @return {promise} resolve to the confirmed client
     */
    return function createWsClient(uri, config) {
      // const { log } = config
      const { url, opts } = prepareConnectConfig(uri, config, false)

      return asyncConnect(WebSocketClass, type, url, opts)
    }
  }

  /**
   * Create a client with auth token
   * @param {string} uri start with ws:// @TODO check this?
   * @param {object} config this is the full configuration because we need something from it
   * @param {string} token the jwt token
   * @return {object} ws instance
   */
  return function createWsAuthClient(uri, config, token) {
    // const { log } = config
    const { url, opts } = prepareConnectConfig(uri, config, token)

    return asyncConnect(WebSocketClass, type, url, opts)
  }
}

export { setupWebsocketClientFn }