// share method to create the wsClientResolver
import {  
  NSP_CLIENT, 
  NSP_AUTH_CLIENT,
  ENABLE_AUTH_PROP_KEY
} from 'jsonql-constants'
import { 
  setupWebsocketClientFn 
} from './setup-websocket-client-fn'
import { 
  loginEventListener, 
  connectEventListener 
} from '../setup-socket-listeners'

/**
 * Create the framework <---> jsonql client binding
 * @param {object} WebSocketClass the different WebSocket module
 * @param {string} [type=browser] we need different setup for browser or node
 * @return {function} the wsClientResolver
 */
function setupConnectClient(WebSocketClass, type = 'browser') {
  /**
   * wsClientResolver
   * @param {object} opts configuration
   * @param {object} nspMap from the contract
   * @param {object} ee instance of the eventEmitter
   * @return {object} passing the same 3 input out with additional in the opts
   */
  return function createClientBindingAction(opts, nspMap, ee) {
    const { log } = opts

    log(`There is problem here with passing the opts`, opts)
    // this will put two callable methods into the opts 
    opts[NSP_CLIENT] = setupWebsocketClientFn(WebSocketClass, type)
    // we don't need this one unless enableAuth === true 
    if (opts[ENABLE_AUTH_PROP_KEY] === true) {
      opts[NSP_AUTH_CLIENT] = setupWebsocketClientFn(WebSocketClass, type, true)
    }    
    // debug 
    log(`[1] bindWebsocketToJsonql`, ee.$name, nspMap)
    // @2020-03-20 @NOTE 
    
    connectEventListener(nspMap, ee, log)
  
    // next we need to setup the login event handler
    // But the same design (see above) when we received a login event 
    // from the http-client or the standalone login call 
    // we received a token here --> update the opts then trigger 
    // the CONNECT_EVENT_NAME again
    loginEventListener(opts, nspMap, ee)

    log(`just before returing the values for the next operation from createClientBindingAction`)

    // we just return what comes in
    return { opts, nspMap, ee }
  }
}

export { setupConnectClient }