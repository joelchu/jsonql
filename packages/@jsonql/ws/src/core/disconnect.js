import { isFunc } from 'jsonql-utils/module'

/**
 * This will get re-use when we start using the disconnect event method
 * @param {*} ws 
 * @return {void}
 */
export function disconnect(ws) {
  if (ws.terminate && isFunc(ws.terminate)) {
    ws.terminate()
  } else if (ws.close && isFunc(ws.close)) {
    ws.close()
  }
}