/**
 * Rollup config for building the slim version
 */
import { join } from 'path'
import buble from 'rollup-plugin-buble'
import { terser } from "rollup-plugin-terser"
import replace from 'rollup-plugin-replace'
import commonjs from 'rollup-plugin-commonjs'
import nodeResolve from 'rollup-plugin-node-resolve'
import nodeGlobals from 'rollup-plugin-node-globals'
import builtins from 'rollup-plugin-node-builtins'
import size from 'rollup-plugin-bundle-size'
import async from 'rollup-plugin-async'

import pkg from './package.json'

const env = process.env.NODE_ENV;
const target = process.env.TARGET; // 1.4.0 add new prop to control the build

let plugins = [
  buble({
    objectAssign: 'Object.assign'
  }),
  nodeResolve({
    preferBuiltins: true,
    mainFields: ['module', 'browser']
  }),
  commonjs({
    include: 'node_modules/**'
  }),
  nodeGlobals(),
  builtins(),
  async(),
  replace({
    'process.env.NODE_ENV': JSON.stringify('production'),
    '__VERSION__': pkg.version
  })
]

let globals = {
  'debug': 'debug',
  'promise-polyfill': 'Promise',
  'flyio': 'Fly'
}
let external =  [
  'flyio',
  'debug',
  'fetch',
  'Promise',
  'promise-polyfill',
  'superagent',
  'handlebars',
  'tty'
]
let moduleName = 'jsonqlClient'
let sourceFile = 'index.js'
let distFile = 'core.js'
const prod = env === 'production'
switch (target) {
  case 'BROWSER':
    sourceFile = 'full.js'
    distFile = join('dist', 'jsonql-client.umd.js')
  break;
  case 'STATIC':
    moduleName = 'jsonqlClientStatic'
    sourceFile = join('src', 'static.js')
    distFile = 'static.js'
  break;
  case 'FULL':
    moduleName = 'jsonqlClientStatic'
    sourceFile = join('src', 'static-full.js')
    distFile = join('dist', 'jsonql-client.static.js')
  break;
  case 'VUE':
    moduleName = 'jsonqlClientVue'
    sourceFile = join('vue', 'index.js')
    distFile = join('dist', `jsonql-client-vue${prod ? '' : '.debug'}.js`)
  break;
  case 'PREACT':
    moduleName = 'jsonqlClientPreact'
    sourceFile = join('preact', 'ws.js')
    distFile = join('dist', `jsonql-client-preact${prod ? '' : '.debug'}.js`)
  break;
  case 'RIOT':
    moduleName = 'jsonqlClientRiot'
    sourceFile = join('riot', 'index.js')
    distFile = join('dist', `jsonql-client-riot${prod ? '' : '.debug'}.js`)
  break;
  case 'IO':
    moduleName = 'jsonqlClientIo'
    sourceFile = 'io.js'
    distFile = join('dist', `jsonql-client-io${prod ? '' : '.debug'}.js`)
  break;
  case 'WS':
    moduleName = 'jsonqlClientWs'
    sourceFile = 'ws.js'
    distFile = prod ? join('dist', `jsonql-client-ws.js`) : join('tests', 'dist', 'jsonql-client-ws.debug.js')
  break;
  default:
    sourceFile = 'index.js'
}
if (env === 'production') {
  plugins.push(terser())
}
plugins.push(size())

let config = {
  input: join(__dirname, sourceFile),
  output: {
    name: moduleName,
    file: join(__dirname, distFile),
    format: 'umd',
    sourcemap: true,
    globals
  },
  plugins,
  external
}

export default config
