# This is jsonql client for Vue

To include the standard http module use

```js
import JsonqlVue from '@jsonql/client/vue'
```

For http with WebSocket client

```js
import JsonqlVueWs from '@jsonql/client/vue/ws'
```

For http with Socket.io client

```js
import JsonqlVueIo from '@jsonql/client/vue/io'
```

More to come
