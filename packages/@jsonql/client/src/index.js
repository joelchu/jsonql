// Combine interface to also init the socket client if it's required
import { SOCKET_NAME, CHECKED_KEY } from 'jsonql-constants'
import { JsonqlError } from 'jsonql-errors'
import { isContract } from 'jsonql-utils/src/contract'
import { isObjectHasKey } from 'jsonql-utils/src/generic'
import { objHasProp } from 'jsonql-utils'
import {
  JsonqlBaseEngine,
  getEventEmitter,
  generator,
  getContractFromConfig,
  getPreConfigCheck
} from 'jsonql-client/module'

/**
 * Create the custom check options method
 * @param {object} extraDefaultOptions for valdiation
 * @param {object} extraConstProps for merge after
 * @return {function} resolve the clean configuration
 */
const getCheckConfigFn = function(extraDefaultOptions, extraConstProps) {
  const checkAction = getPreConfigCheck(extraDefaultOptions, extraConstProps)
  return (config = {}) => Promise.resolve(checkAction(config))
}

/**
 * Check if the contract has socket field and the socket client is suplied
 * @param {*} [socketClient=null] from the original config
 * @return {function} takes in the extra params then return the client
 */
function initSocketClient(socketClient = null) {
  /**
   * @param {object} client the http client
   * @param {object} contract the json
   * @param {object} config the checked configuration
   */
  return (client, contract, config) => {
    if (isObjectHasKey(contract, SOCKET_NAME)) {
      console.log('contract has socket_name', config)

      const prop = objHasProp(config, CHECKED_KEY)

      console.error('CHECKED_KEY', prop)

      if (socketClient) {
        console.log('has socketClient')
        const constProps = {
          contract,
          log: client.getLogger(`jsonql-client:${config.serverType}`),
          eventEmitter: client.eventEmitter
        }
        return socketClient(config, constProps)
          .then(sc => {
            client[SOCKET_NAME] = sc
            return client
          })
      } else {
        throw new JsonqlError(`initSocketClient`, `socketClient is missing!`)
      }
    }
    // just return it if there is none
    return client
  }
}

/**
 * Main interface for construct the client and return extra options for continue
 * with socket client if any
 * @param {object} Fly the http engine
 * @param {object} [config={}] configuration
 * @return {function} return promise resolve with opts, contract, client
 */
function getJsonqlClient(fly, extraDefaultOptions = {}, extraConstProps = {}) {
  const checkConfigFn = getCheckConfigFn(extraDefaultOptions, extraConstProps)
  // resolve opts, contract, client
  return (config = {}) => {

    return checkConfigFn(config)
      .then(opts => (
        {
          opts,
          baseClient: new JsonqlBaseEngine(fly, opts)
        }
      ))
      // make sure the contract is presented
      .then(({opts, baseClient}) => {
        const ee = getEventEmitter(opts.debugOn)
        return getContractFromConfig(baseClient, opts.contract)
          .then(contract => (
            {
              opts,
              contract,
              client: generator(baseClient, opts, contract, ee)
            }
          )
        )
      })
      // @NOTE we only return the opts, contract, client here
      // and allow the client to chain into this to coninue
      // finally generate the websocket client if any
  }
}
// export it
export { getJsonqlClient, initSocketClient }
