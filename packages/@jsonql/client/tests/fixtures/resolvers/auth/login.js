const { JsonqlAuthorisationError } = require('jsonql-errors')
/**
 * login method which check the name
 * @param {string} username
 * @return {object}
 */
module.exports = function login(username) {
  if (username === 'joel') {
    return {name: username}
  }
  throw new JsonqlAuthorisationError(`${username} not recognized`)
}
