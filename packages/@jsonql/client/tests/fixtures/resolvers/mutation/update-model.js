const db = require('../db')

/**
 * add or update the model
 * @param {*} data anything to add to the model
 * @param {number} id if we want to remove something
 * @return {array} return the model once it's done
 */
module.exports = function updateModel(data, id = false) {

  return db(data, id)

}
