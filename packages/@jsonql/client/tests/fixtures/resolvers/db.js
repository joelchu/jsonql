let model = []

module.exports = function update(data, id = false) {
  if (id === false) {
    model.push(data)
  } else if (model[id]) {
    model.splice(id, 1)
  }
  return model;
}
