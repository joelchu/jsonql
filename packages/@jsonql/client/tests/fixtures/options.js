const { join } = require('path')

module.exports = {
  port: 7890,
  contractDir: join(__dirname, 'contract'),
  resolverDir: join(__dirname, 'resolvers'),
  keysDir: join(__dirname, 'keys')
}
