// run a simple test to verify the options are correct or not
const test = require('ava')
const Fly = require("flyio/src/node")
const { join } = require('path')
const fsx = require('fs-extra')
const debug = require('debug')('jsonql-client:test:vuex')
const contract = fsx.readJsonSync(join(__dirname, 'fixtures', 'contract', 'public-contract.json'))
const config = {
  contract,
  hostname: 'http://localhost:3456' // just a dummy one
}
const { getJsonqlVuexModule, getActions } = require('../vue/vuex')

test(`It should able to generate a Vuex store with the flat clients`, t => {

  const store = getActions({'query': {}, 'mutation': {}, 'auth': {}}, config)

  debug(store)

  t.truthy(store)
})
