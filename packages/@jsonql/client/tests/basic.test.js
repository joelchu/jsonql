// basic test to check if the socket client get included
const test = require('ava')
const jsonqlWsClient = require('../ws')
const debug = require('debug')('jsonql-client:test:basic')
const { CHECKED_KEY } = require('jsonql-constants')
const { objHasProp } = require('jsonql-utils')
const fly = require('flyio/src/node')
// debug(jsonqlWsClient)

test(`Just checking the export options`, t => {

  const result = jsonqlWsClient.default(fly, {
    serverType: 'ws',
  })

  debug(result)

  t.pass()

  // t.truthy(objHasProp(result, CHECKED_KEY))

})

test.todo(`The client should also have a socket client included`)
