// jsonql base class

/**
 * Base class to inherit from
 * @TODO we should move all the main back here
 * because the main interface should be a generator based on the contract
 */

import { merge } from 'lodash-es';
// let this.opts = {};
import defaultOptions from './options';

export default class JsonqlBaseClientClass {

  constructor(config) {
    this.opts = merge({}, defaultOptions, config);
  }

  /**
   * computed property
   * @return {object} for the accepted header
   */
  get baseHeader() {

    const { contentType } = this.opts;
    return {
      'Accept': contentType,
      'Content-Type': contentType
    };
  }

  /**
   * setter for the issuer method to use
   * @param {string} token to store
   * @return {string} on success
   */
  set token(token) {
    return this.__storeAuthToken(token);
  }

  /**
   * @return {object} cache burster
   * @private
   */
  __cacheBurst() {
    return {_cb: (new Date()).getTime() };
  }

  /**
   * @param {object} header to set
   * @return {object} combined header
   * @private
   */
  __createHeaders(header = {}) {
    const authHeader = this.__getAuthToken();
    return merge({}, this.baseHeader, header, authHeader);
  }

  /**
   * Store the contract in the local storage
   * @param {object} contract json
   * @return {object} contract
   */
  __storeContract(contract) {
    if (this.opts.useLocalstorage === true) {
      if (window && window.localStorage) {
        window.localStorage.setItem(this.opts.storageKey, JSON.stringify(contract));
      }
    }
    return contract;
  }

  /**
   * Return the stored contract if any
   * @TODO need an option to compare the time and check with the server
   * @return {mixed} false on not found
   */
  __readContract() {
    const contract = this.opts.contract;
    // first check if we have a contract already in memory
    if (contract && typeof contract === 'object') {
      return contract;
    }
    if (this.opts.useLocalstorage === true) {
      if (window && window.localStorage) {
        const _contract = window.localStorage.getItem(this.opts.storageKey);
        return _contract ? JSON.parse(_contract) : false;
      }
    }
    return false;
  }

  /**
   * Store the auth token into session storage
   * @return {string} token
   */
  __storeAuthToken(token) {
    if (window && window.sessionStorage) {
      window.sessionStorage.setItem(this.opts.authKey, token);
      return token;
    }
    throw new Error('There is no sessionStorage support in your environment!');
  }

  __getRawAuthToken() {
    if (window && window.sessionStorage) {
      return window.sessionStorage.getItem(this.opts.authKey);
    }
    throw new Error('No window.sessionStorage support');
  }


  /**
   * Get the stored auth token from session storage
   * @return {string} auth token
   */
  __getAuthToken() {
    if (window && window.sessionStorage) {
      const token = this.__getRawAuthToken();
      // @2019-03-21 We shouldn't throw here because we don't necessary to store the auth
      /* if (!token) {
        throw new Error('No token stored!');
      } */
      const bearer = this.opts.authPrefix;
      return token ? {Authorization: `${bearer} ${token}`} : {};
    }
    throw new Error('No window.sessionStorage support');
  }

  /**
   * When we have the contractKey setup then we need to include this into heading
   * @return {object} combine object
   */
  __getAuth() {
    const extraParams = this.__cacheBurst();
    if (this.opts.contractKey === false) {
      return extraParams;
    }
    const contractAuth = {[this.opts.contractKeyName]: this.opts.contractKey};
    // debug('let see the contract key value pair', contractAuth);
    return merge({}, contractAuth, extraParams);
  }
}
