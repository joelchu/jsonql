// this is port back from the client to share across all projects
import { merge } from 'lodash-es';
import { runValidation, config2argsAction } from './utils';

/**
 * @param {object} config user provide configuration option
 * @param {object} appProps mutation configuration options
 * @param {object} constProps the immutable configuration options
 * @param {function} cb the validateSync method
 * @return {object} Promise resolve merge config object
 */
export default function(config = {}, appProps, constProps, cb) {
  return merge(
    runValidation(
      config2argsAction(config, appProps),
      cb
    ),
    constProps
  );
}
