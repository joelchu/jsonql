/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

/**
 * generate a 32bit hash based on the function.toString()
 * _from http://stackoverflow.com/questions/7616461/generate-a-hash-_from-string-in-javascript-jquery
 * @param {function} fn the converted to string function
 * @return {string} the hashed function string
 */
function hashCode(fn) {
    var s = fn.toString();
    return s.split("").reduce(function (a, b) { a = ((a << 5) - a) + b.charCodeAt(0); return a & a; }, 0) + "";
}

// the suspend ported from nb-event-service
// This is different from the nb-event-service
// instead of using object.defineProperty we just use the native ES6 setter
// with Typescript private
var JsonqlSuspend = /** @class */ (function () {
    function JsonqlSuspend(logger) {
        this.lastSuspendState = null;
        this.suspend = null;
        this.queue = new Set();
    }
    Object.defineProperty(JsonqlSuspend.prototype, "$suspend", {
        /**
         * getter to get back the current suspend state
         * @return {boolean} the suspend state
         */
        get: function () {
            return this.suspend;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(JsonqlSuspend.prototype, "$queues", {
        /**
         * getter return the queue as array
         * @return {array} queue to array
         */
        get: function () {
            return this.arrayFrom(this.queue);
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {any} mapObj the Map object
     * @return {array} transform to array to work with
     */
    JsonqlSuspend.prototype.arrayFrom = function (mapObj) {
        return Array.from(mapObj);
    };
    return JsonqlSuspend;
}());

var JsonqlStoreService = /** @class */ (function (_super) {
    __extends(JsonqlStoreService, _super);
    function JsonqlStoreService(logger) {
        var _this = _super.call(this, logger) || this;
        // hack
        _this.logger = typeof logger === 'function' ? logger : function () { };
        // should this be WeakMap or should they be Map?
        _this.normalStore = new Map();
        _this.lazyStore = new Map();
        // don't keep
        _this.keep = false;
        // place holder, should this be a WeakSet?
        _this.result = [];
        return _this;
    }
    /**
     * return all the listener from the event
     * @param {string} evtName event name
     * @param {boolean} [full=false] if true then return the entire content
     * @return {array|boolean} listerner(s) or false when not found
     */
    JsonqlStoreService.prototype.$get = function (evt, full) {
        if (full === void 0) { full = false; }
        this.validateEvt(evt);
        var store = this.normalStore;
        this.logger('$get', full, this.normalStore);
        if (store.has(evt)) {
            return this
                .arrayFrom(store.get(evt))
                .map(function (l) {
                if (full) {
                    return l;
                }
                var key = l[0], callback = l[1];
                return callback;
            });
        }
        return false;
    };
    Object.defineProperty(JsonqlStoreService.prototype, "$done", {
        /**
         * @TODO is there any real use with the keep prop?
         * getter for $done
         * @return {*} whatever last store result
         */
        get: function () {
            if (this.keep) {
                this.logger(this.result);
                return this.result[this.result.length - 1];
            }
            return this.result;
        },
        ////////////////////////////////
        //       SETTER / GETTER      //
        ////////////////////////////////
        /**
         * store the return result from the run
         * @param {*} value whatever return from callback
         */
        set: function (value) {
            this.logger('set $done', value);
            if (this.keep) {
                this.result.push(value);
            }
            else {
                this.result = value;
            }
        },
        enumerable: true,
        configurable: true
    });
    /////////////////////////////////
    // protected / PROTECTED METHODS //
    /////////////////////////////////
    /**
     * make sure we store the argument correctly
     * @param {*} arg could be array
     * @return {array} make sured
     */
    JsonqlStoreService.prototype.toArray = function (arg) {
        return Array.isArray(arg) ? arg : [arg];
    };
    /**
     * Run the callback
     * @param {function} callback function to execute
     * @param {array} payload for callback
     * @param {object} ctx context or null
     * @return {void} the result store in $done
     */
    JsonqlStoreService.prototype.run = function (callback, payload, ctx) {
        this.logger('run', callback, payload, ctx);
        this.$done = Reflect.apply(callback, ctx, this.toArray(payload));
    };
    /**
     * Take the content out and remove it from store id by the name
     * @param {string} evt event name
     * @return {object|boolean} content or false on not found
     */
    JsonqlStoreService.prototype.takeFromStore = function (evt) {
        var store = this.lazyStore;
        if (store.has(evt)) {
            var content = store.get(evt);
            this.logger('takeFromStore', content);
            store.delete(evt);
            return content;
        }
        this.logger("lazyStore doesn't have " + evt);
        return false;
    };
    /**
     * The add to store step is similar so make it generic for resuse
     * @param {object} store which store to use
     * @param {string} evt event name
     * @param {spread} args because the lazy store and normal store store different things
     * @return {array} store and the size of the store (cheated @TODO)
     */
    JsonqlStoreService.prototype.addToStore = function (store, evt) {
        var args = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            args[_i - 2] = arguments[_i];
        }
        var fnSet;
        if (store.has(evt)) {
            this.logger('addToStore', evt + " existed");
            fnSet = store.get(evt);
        }
        else {
            this.logger('addToStore', "create new Set for \"" + evt + "\"");
            // this is new
            fnSet = new Set();
        }
        // lazy only store 2 items - this is not the case in V1.6.0 anymore
        // we need to check the first parameter is string or not
        if (args.length > 2) {
            if (Array.isArray(args[0])) { // lazy store
                // check if this type of this event already register in the lazy store
                var t = args[2];
                if (!this.checkTypeInLazyStore(evt, t)) {
                    fnSet.add(args);
                }
            }
            else {
                if (!this.checkContentExist(args, fnSet)) {
                    this.logger('addToStore', "insert new", args);
                    fnSet.add(args);
                }
            }
        }
        else { // add straight to lazy store
            fnSet.add(args);
        }
        store.set(evt, fnSet);
        // this.logger('fnSet', fnSet, store)
        return [store, fnSet.size];
    };
    /**
     * @param {array} args for compare
     * @param {object} fnSet A Set to search from
     * @return {boolean} true on exist
     */
    JsonqlStoreService.prototype.checkContentExist = function (args, fnSet) {
        var list = this.arrayFrom(fnSet);
        return !!list
            .filter(function (value, index, array) {
            var hash = value[0];
            return hash === args[0];
        }).length;
    };
    /**
     * get the existing type to make sure no mix type add to the same store
     * @param {string} evtName event name
     * @param {string} type the type to check
     * @return {boolean} true you can add, false then you can't add this type
     */
    JsonqlStoreService.prototype.checkTypeInStore = function (evtName, type) {
        this.validateEvt(evtName, type);
        var all = this.$get(evtName, true);
        if (all === false) {
            // pristine it means you can add
            return true;
        }
        // it should only have ONE type in ONE event store
        return !all
            .filter(function (value, index, array) {
            var t = value[3];
            return type !== t;
        }).length;
    };
    /**
     * This is checking just the lazy store because the structure is different
     * therefore we need to use a new method to check it
     * @param {string} evtName event name
     * @param {string} type the types of allow event
     * @return {boolean} true OK
     */
    JsonqlStoreService.prototype.checkTypeInLazyStore = function (evtName, type) {
        this.validateEvt(evtName, type);
        var store = this.lazyStore.get(evtName);
        if (store) {
            this.logger('checkTypeInLazyStore', store);
            return !!this.arrayFrom(store)
                .filter(function (value, index, array) {
                var t = value[2];
                return t !== type;
            }).length;
        }
        this.logger("Store " + evtName + " is empty");
        return false;
    };
    /**
     * wrapper to re-use the addToStore,
     * V1.3.0 add extra check to see if this type can add to this evt
     * @param {string} evt event name
     * @param {string} type on or once
     * @param {function} callback function
     * @param {object} context the context the function execute in or null
     * @return {number|boolean} size of the store, or false when the check type failed
     */
    JsonqlStoreService.prototype.addToNormalStore = function (evt, type, callback, context) {
        if (context === void 0) { context = null; }
        // @TODO we need to check the existing store for the type first!
        if (this.checkTypeInStore(evt, type)) {
            this.logger("addToNormalStore: " + type + " can add to \"" + evt + "\" store");
            var key = hashCode(callback);
            var args = [this.normalStore, evt, key, callback, context, type];
            var _a = Reflect.apply(this.addToStore, this, args), _store = _a[0], size = _a[1];
            // @BUG? is this the problem because it was a setter before
            this.normalStore = _store;
            // this.logger('after add to store', this.normalStore)
            return size;
        }
        this.logger('addToNormalStore:', evt, type, 'NOT add to normal store', callback.toString());
        return false;
    };
    /**
     * Add to lazy store this get calls when the callback is not register yet
     * so we only get a payload object or even nothing
     * @param {string} evt event name
     * @param {array} payload of arguments or empty if there is none
     * @param {object} [ctx=null] the context the callback execute in
     * @param {string|boolean} [type=false] register a type so no other type can add to this evt
     * @return {number} size of the store
     */
    JsonqlStoreService.prototype.addToLazyStore = function (evt, payload, ctx, type) {
        if (payload === void 0) { payload = []; }
        if (ctx === void 0) { ctx = null; }
        // this is add in V1.6.0
        // when there is type then we will need to check if this already added in lazy store
        // and no other type can add to this lazy store
        var args = [];
        args.push(this.lazyStore, evt, this.toArray(payload), ctx);
        if (type) {
            args.push(type);
        }
        var _a = Reflect.apply(this.addToStore, this, args), _store = _a[0], size = _a[1];
        // @BUG ? this was a setter
        this.lazyStore = _store;
        return size;
    };
    // VALIDATORS //
    // This is another JOKE about Typescript, after you spend all this time on
    // fixing the non-error (aka typing) it doesn't do anything during run time
    // so what the fuck?
    /**
     * validate the event name
     * @param {string} evt event name
     * @return {boolean} true when OK
     */
    JsonqlStoreService.prototype.validateEvt = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        args.forEach(function (arg) {
            if (typeof arg !== 'string') {
                throw new Error("event name must be string type! " + arg);
            }
        });
        return true;
    };
    /**
     * Simple quick check on the two main parameters
     * @param {function} callback function to call
     * @param {string} evt event name
     * @return {boolean} true when OK
     */
    JsonqlStoreService.prototype.validate = function (callback) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        if (typeof callback === 'function') {
            return Reflect.apply(this.validateEvt, this, args);
        }
        throw new Error("callback required to be function type! " + callback);
    };
    /**
     * Check if this type is correct or not added in V1.5.0
     * @param {string} type for checking
     * @return {boolean} true on OK
     */
    JsonqlStoreService.prototype.validateType = function (type) {
        var types = ['on', 'only', 'once', 'onlyOnce'];
        return !!types
            .filter(function (t) { return type === t; }).length;
    };
    return JsonqlStoreService;
}(JsonqlSuspend));

// main
var JsonqlEvent = /** @class */ (function (_super) {
    __extends(JsonqlEvent, _super);
    /**
     * Create EventService instance
     * @param {configObj} config configuration object
     * @return {void} nothing - don't do :void this fuck typescript up what a lot of shit
     */
    function JsonqlEvent(logger) {
        return _super.call(this, logger) || this;
    }
    /**
     * Register your evt handler, note we don't check the type here,
     * we expect you to be sensible and know what you are doing.
     * @param {string} evt name of event
     * @param {function} callback bind method --> if it's array or not
     * @param {object} [context=null] to execute this call in
     * @return {number} the size of the store
     */
    JsonqlEvent.prototype.$on = function (evt, callback, context) {
        var _this = this;
        if (context === void 0) { context = null; }
        this.validate(callback, evt);
        var type = 'on';
        // first need to check if this evt is in lazy store
        var lazyStoreContent = this.takeFromStore(evt);
        // this is normal register first then call later
        if (lazyStoreContent === false) {
            this.logger('$on', evt + " callback is not in lazy store");
            // @TODO we need to check if there was other listener to this
            // event and are they the same type then we could solve that
            // register the different type to the same event name
            return this.addToNormalStore(evt, type, callback, context);
        }
        this.logger('$on', evt + " found in lazy store");
        // this is when they call $trigger before register this callback
        var size = 0;
        lazyStoreContent.forEach(function (content) {
            var payload = content[0], ctx = content[1], t = content[2];
            if (t && t !== type) {
                throw new Error("You are trying to register an event already been taken by other type: " + t);
            }
            _this.run(callback, payload, context || ctx);
            var result = _this.addToNormalStore(evt, type, callback, context || ctx);
            if (typeof result !== 'boolean') {
                size += result;
            }
        });
        return size;
    };
    /**
     * once only registered it once, there is no overwrite option here
     * @NOTE change in v1.3.0 $once can add multiple listeners
     *       but once the event fired, it will remove this event (see $only)
     * @param {string} evt name
     * @param {function} callback to execute
     * @param {object} [context=null] the handler execute in
     * @return {number|boolean} result
     */
    JsonqlEvent.prototype.$once = function (evt, callback, context) {
        if (context === void 0) { context = null; }
        this.validate(callback, evt);
        var type = 'once';
        var lazyStoreContent = this.takeFromStore(evt);
        // this is normal register before call $trigger
        var nStore = this.normalStore;
        if (lazyStoreContent === false) {
            this.logger('$once', evt + " not in the lazy store");
            // v1.3.0 $once now allow to add multiple listeners
            return this.addToNormalStore(evt, type, callback, context);
        }
        else {
            // now this is the tricky bit
            // there is a potential bug here that cause by the developer
            // if they call $trigger first, the lazy won't know it's a once call
            // so if in the middle they register any call with the same evt name
            // then this $once call will be fucked - add this to the documentation
            this.logger('$once', lazyStoreContent);
            var list = this.arrayFrom(lazyStoreContent);
            // should never have more than 1
            var _a = list[0], payload = _a[0], ctx = _a[1], t = _a[2];
            if (t && t !== type) {
                throw new Error("You are trying to register an event already been taken by other type: " + t);
            }
            this.run(callback, payload, context || ctx);
            // remove this evt from store
            this.$off(evt);
        }
        return false;
    };
    /**
     * This one event can only bind one callbackback
     * @param {string} evt event name
     * @param {function} callback event handler
     * @param {object} [context=null] the context the event handler execute in
     * @return {boolean} true bind for first time, false already existed
     */
    JsonqlEvent.prototype.$only = function (evt, callback, context) {
        var _this = this;
        if (context === void 0) { context = null; }
        this.validate(callback, evt);
        var type = 'only';
        // keep getting TS232, this is why typescript is a joke
        // it will just ended up with everything is any type and back to square one
        var added = false;
        var lazyStoreContent = this.takeFromStore(evt);
        // this is normal register before call $trigger
        var nStore = this.normalStore;
        if (!nStore.has(evt)) {
            this.logger("$only", evt + " add to store");
            added = this.addToNormalStore(evt, type, callback, context);
        }
        if (lazyStoreContent !== false) {
            // there are data store in lazy store
            this.logger('$only', evt + " found data in lazy store to execute");
            var list = this.arrayFrom(lazyStoreContent);
            // $only allow to trigger this multiple time on the single handler
            list.forEach(function (l) {
                var payload = l[0], ctx = l[1], t = l[2];
                if (t && t !== type) {
                    throw new Error("You are trying to register an event already been taken by other type: " + t);
                }
                _this.run(callback, payload, context || ctx);
            });
        }
        return added;
    };
    /**
     * $only + $once this is because I found a very subtile bug when we pass a
     * resolver, rejecter - and it never fire because that's OLD adeed in v1.4.0
     * @param {string} evt event name
     * @param {function} callback to call later
     * @param {object} [context=null] exeucte context
     * @return {boolean} same as above
     */
    JsonqlEvent.prototype.$onlyOnce = function (evt, callback, context) {
        if (context === void 0) { context = null; }
        this.validate(callback, evt);
        var type = 'onlyOnce';
        var added = false;
        var lazyStoreContent = this.takeFromStore(evt);
        // this is normal register before call $trigger
        var nStore = this.normalStore;
        if (!nStore.has(evt)) {
            this.logger("$onlyOnce", evt + " add to store");
            added = this.addToNormalStore(evt, type, callback, context);
        }
        if (lazyStoreContent !== false) {
            // there are data store in lazy store
            this.logger('$onlyOnce', lazyStoreContent);
            var list = this.arrayFrom(lazyStoreContent);
            // should never have more than 1
            var _a = list[0], payload = _a[0], ctx = _a[1], t = _a[2];
            if (t && t !== 'onlyOnce') {
                throw new Error("You are trying to register an event already been taken by other type: " + t);
            }
            this.run(callback, payload, context || ctx);
            // remove this evt from store
            this.$off(evt);
        }
        return added;
    };
    /**
     * This is a shorthand of $off + $on added in V1.5.0
     * @param {string} evt event name
     * @param {function} callback to exeucte
     * @param {object} [context = null] or pass a string as type
     * @param {string} [type=on] what type of method to replace
     * @return {any} whatever the call method return
     */
    JsonqlEvent.prototype.$replace = function (evt, callback, context, type) {
        if (context === void 0) { context = null; }
        if (type === void 0) { type = 'on'; }
        this.$off(evt);
        // this will become a problem
        var method = this.getMethodBy(type);
        return Reflect.apply(method, this, [evt, callback, context]);
    };
    /**
     * trigger the event
     * @param {string} evt name NOT allow array anymore!
     * @param {mixed} [payload = []] pass to fn
     * @param {object|string} [context = null] overwrite what stored
     * @param {string} [type=false] if pass this then we need to add type to store too
     * @return {number} if it has been execute how many times
     */
    JsonqlEvent.prototype.$trigger = function (evt, payload, context, type) {
        if (payload === void 0) { payload = []; }
        if (context === void 0) { context = null; }
        if (type === void 0) { type = false; }
        this.validateEvt(evt);
        var found = 0;
        // first check the normal store
        var nStore = this.normalStore;
        this.logger('$trigger, from normalStore: ', nStore);
        if (nStore.has(evt)) {
            var added = this.$queue(evt, payload, context, type);
            this.logger('$trigger', evt, 'found; add to queue: ', added);
            if (added === true) {
                return found; // just exit
            }
            var nSet = this.arrayFrom(nStore.get(evt));
            var ctn = nSet.length;
            var hasOnce = false;
            // let hasOnly = false;
            for (var i = 0; i < ctn; ++i) {
                ++found;
                // this.logger('found', found)
                var _a = nSet[i], _ = _a[0], callback = _a[1], ctx = _a[2], type_1 = _a[3];
                this.run(callback, payload, context || ctx);
                if (type_1 === 'once' || type_1 === 'onlyOnce') {
                    hasOnce = true;
                }
            }
            if (hasOnce) {
                nStore.delete(evt);
            }
            return found;
        }
        // now this is not register yet
        this.addToLazyStore(evt, payload, context, type);
        return found;
    };
    /**
     * this is an alias to the $trigger
     * @NOTE breaking change in V1.6.0 we swap the parameter around
     * @param {string} evt event name
     * @param {*} params pass to the callback
     * @param {string} type of call
     * @param {object} context what context callback execute in
     * @return {*} from $trigger
     */
    JsonqlEvent.prototype.$call = function (evt, params, type, context) {
        if (type === void 0) { type = false; }
        if (context === void 0) { context = null; }
        this.validateEvt(evt);
        var args = [evt, params];
        args.push(context, type);
        return Reflect.apply(this.$trigger, this, args);
    };
    /**
     * remove the evt from all the stores
     * @param {string} evt name
     * @return {boolean} true actually delete something
     */
    JsonqlEvent.prototype.$off = function (evt) {
        this.validateEvt(evt);
        var stores = [this.lazyStore, this.normalStore];
        var found = false;
        stores.forEach(function (store) {
            if (store.has(evt)) {
                found = true;
                store.delete(evt);
            }
        });
        return found;
    };
    /**
     * When using ES6 you just need this[$+type] but typescript no such luck
     * @param {string} type the 4 types
     * @return {*} the method
     */
    JsonqlEvent.prototype.getMethodBy = function (type) {
        switch (type) {
            case 'on':
                return this.$on;
            case 'only':
                return this.$only;
            case 'once':
                return this.$once;
            case 'onlyOnce':
                return this.$onlyOnce;
            default:
                throw new Error(type + " is not supported!");
        }
    };
    /**
     * Add $trigger args to the queue during suspend state
     * @param {array} args from the $trigger call parameters
     * @return {boolean} true when added
     */
    JsonqlEvent.prototype.$queue = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        if (this.suspend) {
            this.queue.add(args);
        }
        return this.suspend;
    };
    /**
     * whenever the suspend change from true to false
     * then we check the queue to see if there is anything to do
     * @return {boolean} true if there is queue
     */
    JsonqlEvent.prototype.releaseQueue = function () {
        var _this = this;
        var size = this.queue.size;
        if (size > 0) {
            var queue = this.arrayFrom(this.queue);
            this.queue.clear();
            queue.forEach(function (args) {
                Reflect.apply(_this.$trigger, _this, args);
            });
        }
        return !!size;
    };
    Object.defineProperty(JsonqlEvent.prototype, "$suspend", {
        /**
         * This is the main interface to see if anything happens
         * @param {boolean} state to change the suspend state
         * @return {boolean} what comes in what goes out
         */
        set: function (state) {
            var _this = this;
            if (typeof state !== 'boolean') {
                throw new Error("Typescript is a fucking joke! BTW we want a Boolean which fucking Typescript can't figure it out!!!! " + typeof state);
            }
            var lastSuspendState = this.suspend;
            this.suspend = state;
            if (lastSuspendState === true && state === false) {
                setTimeout(function () {
                    _this.releaseQueue();
                }, 1);
            }
        },
        enumerable: true,
        configurable: true
    });
    return JsonqlEvent;
}(JsonqlStoreService));

// The top level for @jsonql/event

export default JsonqlEvent;
