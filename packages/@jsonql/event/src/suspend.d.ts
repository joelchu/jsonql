export default class JsonqlSuspend {
    protected queue: Set<any[]>;
    protected suspend?: any;
    protected lastSuspendState?: any;
    constructor(logger: any);
    /**
     * getter to get back the current suspend state
     * @return {boolean} the suspend state
     */
    readonly $suspend: boolean;
    /**
     * @param {any} mapObj the Map object
     * @return {array} transform to array to work with
     */
    protected arrayFrom(mapObj: any): any[];
}
