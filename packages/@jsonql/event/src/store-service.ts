// break up the store related operation here
// because the class is getting way too big now
import hashFnToKey from './hash-code'
import JsonqlSuspend from './suspend'

export default class JsonqlStoreService extends JsonqlSuspend {
  // public props
  keep: boolean;
  // protected props
  protected normalStore: Map<string,any>;
  protected lazyStore: Map<string,any>;
  protected result: any[];
  protected logger: any;

  constructor(logger: any) {
    super(logger)
    // hack
    this.logger = typeof logger === 'function' ? logger : () => {};
    // should this be WeakMap or should they be Map?
    this.normalStore = new Map()
    this.lazyStore = new Map()
    // don't keep
    this.keep = false;
    // place holder, should this be a WeakSet?
    this.result = [];
  }


    /**
     * return all the listener from the event
     * @param {string} evtName event name
     * @param {boolean} [full=false] if true then return the entire content
     * @return {array|boolean} listerner(s) or false when not found
     */
    public $get(evt: string, full: boolean = false): any {
      this.validateEvt(evt)
      let store = this.normalStore;
      this.logger('$get', full, this.normalStore)
      if (store.has(evt)) {
        return this
          .arrayFrom(store.get(evt))
          .map( l => {
            if (full) {
              return l;
            }
            let [key, callback, ] = l;
            return callback;
          })
      }
      return false;
    }


    ////////////////////////////////
    //       SETTER / GETTER      //
    ////////////////////////////////

    /**
     * store the return result from the run
     * @param {*} value whatever return from callback
     */
    set $done(value: any) {
      this.logger('set $done', value)
      if (this.keep) {
        this.result.push(value)
      } else {
        this.result = value;
      }
    }

    /**
     * @TODO is there any real use with the keep prop?
     * getter for $done
     * @return {*} whatever last store result
     */
    get $done(): any {
      if (this.keep) {
        this.logger(this.result)
        return this.result[this.result.length - 1]
      }
      return this.result;
    }

  /////////////////////////////////
  // protected / PROTECTED METHODS //
  /////////////////////////////////

  /**
   * make sure we store the argument correctly
   * @param {*} arg could be array
   * @return {array} make sured
   */
  protected toArray(arg: any) {
    return Array.isArray(arg) ? arg : [arg];
  }

  /**
   * Run the callback
   * @param {function} callback function to execute
   * @param {array} payload for callback
   * @param {object} ctx context or null
   * @return {void} the result store in $done
   */
  protected run(callback: myCallbackType, payload: any[], ctx: any): void {
    this.logger('run', callback, payload, ctx)
    this.$done = Reflect.apply(callback, ctx, this.toArray(payload))
  }

  /**
   * Take the content out and remove it from store id by the name
   * @param {string} evt event name
   * @return {object|boolean} content or false on not found
   */
  protected takeFromStore(evt: string) {
    const store = this.lazyStore;
    if (store.has(evt)) {
      let content = store.get(evt)
      this.logger('takeFromStore', content)
      store.delete(evt)
      return content;
    }
    this.logger(`lazyStore doesn't have ${evt}`)
    return false;
  }

  /**
   * The add to store step is similar so make it generic for resuse
   * @param {object} store which store to use
   * @param {string} evt event name
   * @param {spread} args because the lazy store and normal store store different things
   * @return {array} store and the size of the store (cheated @TODO)
   */
  protected addToStore(store: any, evt: string, ...args: any[]): any[] {
    let fnSet;
    if (store.has(evt)) {
      this.logger('addToStore', `${evt} existed`)
      fnSet = store.get(evt)
    } else {
      this.logger('addToStore', `create new Set for "${evt}"`)
      // this is new
      fnSet = new Set()
    }
    // lazy only store 2 items - this is not the case in V1.6.0 anymore
    // we need to check the first parameter is string or not
    if (args.length > 2) {
      if (Array.isArray(args[0])) { // lazy store
        // check if this type of this event already register in the lazy store
        let [,,t] = args;
        if (!this.checkTypeInLazyStore(evt, t)) {
          fnSet.add(args)
        }
      } else {
        if (!this.checkContentExist(args, fnSet)) {
          this.logger('addToStore', `insert new`, args)
          fnSet.add(args)
        }
      }
    } else { // add straight to lazy store
      fnSet.add(args)
    }
    store.set(evt, fnSet)
    // this.logger('fnSet', fnSet, store)
    return [store, fnSet.size]
  }

  /**
   * @param {array} args for compare
   * @param {object} fnSet A Set to search from
   * @return {boolean} true on exist
   */
  protected checkContentExist(args: any[], fnSet: Map<string,any>): boolean {
    let list = this.arrayFrom(fnSet)
    return !!list
      .filter((value: any, index: number, array: any[]): boolean => {
        let [hash,] = value;
        return hash === args[0];
      }).length;
  }

  /**
   * get the existing type to make sure no mix type add to the same store
   * @param {string} evtName event name
   * @param {string} type the type to check
   * @return {boolean} true you can add, false then you can't add this type
   */
  protected checkTypeInStore(evtName: string, type: string): boolean {
    this.validateEvt(evtName, type)
    let all = this.$get(evtName, true)
    if (all === false) {
       // pristine it means you can add
      return true;
    }
    // it should only have ONE type in ONE event store
    return !all
      .filter((value: any, index: number, array: any[]): boolean => {
        let [ ,,,t ] = value;
        return type !== t;
      }).length;
  }

  /**
   * This is checking just the lazy store because the structure is different
   * therefore we need to use a new method to check it
   * @param {string} evtName event name
   * @param {string} type the types of allow event
   * @return {boolean} true OK
   */
  protected checkTypeInLazyStore(evtName: string, type: string): boolean {
    this.validateEvt(evtName, type)
    let store = this.lazyStore.get(evtName)
    if (store) {
      this.logger('checkTypeInLazyStore', store)
      return !!this.arrayFrom(store)
        .filter((value: any, index: number, array: any[]): boolean => {
          let [,,t] = value;
          return t !== type;
        }).length
    }
    this.logger(`Store ${evtName} is empty`)
    return false;
  }

  /**
   * wrapper to re-use the addToStore,
   * V1.3.0 add extra check to see if this type can add to this evt
   * @param {string} evt event name
   * @param {string} type on or once
   * @param {function} callback function
   * @param {object} context the context the function execute in or null
   * @return {number|boolean} size of the store, or false when the check type failed
   */
  protected addToNormalStore(evt: string, type: any, callback: myCallbackType, context: any = null): number | boolean {
    // @TODO we need to check the existing store for the type first!
    if (this.checkTypeInStore(evt, type)) {
      this.logger(`addToNormalStore: ${type} can add to "${evt}" store`)
      let key = hashFnToKey(callback)
      let args = [this.normalStore, evt, key, callback, context, type]
      let [_store, size] = Reflect.apply(this.addToStore, this, args)
      // @BUG? is this the problem because it was a setter before
      this.normalStore = _store;
      // this.logger('after add to store', this.normalStore)
      return size;
    }
    this.logger('addToNormalStore:', evt, type, 'NOT add to normal store', callback.toString())
    return false;
  }

  /**
   * Add to lazy store this get calls when the callback is not register yet
   * so we only get a payload object or even nothing
   * @param {string} evt event name
   * @param {array} payload of arguments or empty if there is none
   * @param {object} [ctx=null] the context the callback execute in
   * @param {string|boolean} [type=false] register a type so no other type can add to this evt
   * @return {number} size of the store
   */
  protected addToLazyStore(evt: string, payload: any[] = [], ctx: any = null, type: string|boolean): number {
    // this is add in V1.6.0
    // when there is type then we will need to check if this already added in lazy store
    // and no other type can add to this lazy store
    let args = [];
    args.push(this.lazyStore, evt, this.toArray(payload), ctx)
    if (type) {
      args.push(type)
    }
    let [_store, size] = Reflect.apply(this.addToStore, this, args)
    // @BUG ? this was a setter
    this.lazyStore = _store;
    return size;
  }

  // VALIDATORS //
  // This is another JOKE about Typescript, after you spend all this time on
  // fixing the non-error (aka typing) it doesn't do anything during run time
  // so what the fuck?

  /**
   * validate the event name
   * @param {string} evt event name
   * @return {boolean} true when OK
   */
  protected validateEvt(...args: string[]): boolean {
    args.forEach((arg: string) => {
      if (typeof arg !== 'string') {
        throw new Error(`event name must be string type! ${arg}`)
      }
    })
    return true;
  }

  /**
   * Simple quick check on the two main parameters
   * @param {function} callback function to call
   * @param {string} evt event name
   * @return {boolean} true when OK
   */
  protected validate(callback: myCallbackType, ...args: string[]): boolean {
    if (typeof callback === 'function') {
      return Reflect.apply(this.validateEvt, this, args)
    }
    throw new Error(`callback required to be function type! ${callback}`)
  }

  /**
   * Check if this type is correct or not added in V1.5.0
   * @param {string} type for checking
   * @return {boolean} true on OK
   */
  protected validateType(type: string): boolean {
    const types = ['on', 'only', 'once', 'onlyOnce']
    return !!types
      .filter(
        (t: any): boolean => type === t
      ).length;
  }


}
