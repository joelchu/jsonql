// the suspend ported from nb-event-service
// This is different from the nb-event-service
// instead of using object.defineProperty we just use the native ES6 setter
// with Typescript private

export default class JsonqlSuspend {
  protected queue: Set<any[]>;
  protected suspend?: any; // Typescript is a fucking joke
  protected lastSuspendState?: any; // Typescript is a fucking joke

  constructor(logger: any) {
    this.lastSuspendState = null;
    this.suspend = null;
    this.queue = new Set()
  }

  /**
   * getter to get back the current suspend state
   * @return {boolean} the suspend state
   */
  public get $suspend(): boolean {
    return this.suspend;
  }

  /**
   * getter return the queue as array
   * @return {array} queue to array
   */
  public get $queues() {
    return this.arrayFrom(this.queue)
  }

  /**
   * @param {any} mapObj the Map object
   * @return {array} transform to array to work with
   */
  protected arrayFrom(mapObj: any): any[] {
    return Array.from(mapObj)
  }


}
