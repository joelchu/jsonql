// The top level for @jsonql/event

// The class file has no default export therefore here you will ended up with named export
import JsonqlEvent from './src/event-service';
// You have to do this in 2 steps to get rip of the .default from cjs!
export default JsonqlEvent;
