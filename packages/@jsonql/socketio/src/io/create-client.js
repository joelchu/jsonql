// this will create the socket.io client
import { chainPromises } from 'jsonql-jwt'
import { getNameFromPayload, isString } from 'jsonql-params-validator'
import { LOGIN_EVENT_NAME, LOGOUT_EVENT_NAME } from 'jsonql-constants'

import { nspClient, nspAuthClient } from '../utils/create-nsp-client'
import clientEventHandler from '../client-event-handler'
import ioMainHandler from './io-main-handler'

import { getDebug, clearMainEmitEvt, getNamespaceInOrder, disconnect } from '../utils'
const debugFn = getDebug('io-create-client')

// just to make it less ugly
const mapNsps = (nsps, namespaces) => nsps
  .map((nsp, i) => ({[namespaces[i]]: nsp}))
  .reduce((last, next) => Object.assign(last,next), {})

/**
 * This one will run the create nsps in sequence and make sure
 * the auth one connect before we call the others
 * @param {object} opts configuration
 * @param {object} nspMap contract map
 * @param {string} token validation
 * @return {object} promise resolve with namespaces, nsps in same order array
 */
const createAuthNsps = function(opts, nspMap, token, namespaces) {
  let { publicNamespace } = nspMap;
  opts.token = token;
  let p1 = () => nspAuthClient(namespaces[0], opts)
  let p2 = () => nspClient(namespaces[1], opts)
  return chainPromises([p1(), p2()])
    .then(nsps => ({
      nsps: mapNsps(nsps, namespaces),
      namespaces,
      login: false
    }))
}

/**
 * Because the nsps can be throw away so it doesn't matter the scope
 * this will get reuse again
 * @param {object} opts configuration
 * @param {object} nspMap from contract
 * @param {string|null} token whether we have the token at run time
 * @return {object} nsps namespace with namespace as key
 */
const createNsps = function(opts, nspMap, token) {
  let { nspSet, publicNamespace } = nspMap;
  let login = false;
  let nsps = {}
  // first we need to binding all the events handler
  if (opts.enableAuth && opts.useJwt) {
    let namespaces = getNamespaceInOrder(nspSet, publicNamespace)
    debugFn('namespaces', namespaces)
    login = opts.useJwt; // just saying we need to listen to login event
    if (token) {
      debugFn('call createAuthNsps')
      return createAuthNsps(opts, nspMap, token, namespaces)
    }
    debugFn('init with a placeholder')
    return nspClient(publicNamespace, opts)
      .then(nsp => ({
        nsps: {
          [ publicNamespace ]: nsp,
          [ namespaces[0] ]: false
        },
        namespaces,
        login
      }))
  }
  // standard without login
  // the stock version should not have a namespace
  return nspClient(false, opts)
    .then(nsp => ({
      nsps: {[publicNamespace]: nsp},
      namespaces: [publicNamespace],
      login
    }))
}

/**
 * This is just copy of the ws version we need to figure
 * out how to deal with the roundtrip login later
 * @param {object} opts configuration
 * @param {object} nspMap namespace with resolvers
 * @param {object} ee EventEmitter to pass through
 * @return {object} what comes in what goes out
 */
export default function createClient(opts, nspMap, ee) {
  // arguments don't change
  let args = [opts, nspMap, ee, ioMainHandler]
  return createNsps(opts, nspMap, opts.token)
    .then( ({ nsps, namespaces, login }) => {
      // binding the listeners - and it will listen to LOGOUT event
      // to unbind itself, and the above call will bind it again
      Reflect.apply(clientEventHandler, null, args.concat([namespaces, nsps]))
      if (login) {
        ee.$only(LOGIN_EVENT_NAME, function(token) {
          // here we should disconnect all the previous nsps
          disconnect(nsps)
          // first trigger a LOGOUT event to unbind ee to ws
          // ee.$trigger(LOGOUT_EVENT_NAME) // <-- this seems to cause a lot of problems
          clearMainEmitEvt(ee, namespaces)
          debugFn('LOGIN_EVENT_NAME')
          createNsps(opts, nspMap, token)
            .then(newNsps => {
              // rebind it
              Reflect.apply(
                clientEventHandler,
                null,
                args.concat([newNsps.namespaces, newNsps.nsps])
              )
            })
        })
      }
      // return this will also works because the outter call are in promise chain
      return { opts, nspMap, ee }
    })
}
