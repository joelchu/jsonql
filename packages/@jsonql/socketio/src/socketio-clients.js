// take the export from the browser client then we wrap the code in
// a node version implmentation that save some of the parameters
let socketIoClientModule;
try {
  socketIoClientModule = require('socket.io-client')
} catch(e) {
  console.error(`You need to install socket.io-client manually! If you are not using it then just ignore this message`)
}
const {
  socketIoHandshakeLogin,
  socketIoRoundtripLogin,
  socketIoClient,
  socketIoClientAsync,
  socketIoChainConnect
} = require('jsonql-jwt')

const { IO_ROUNDTRIP_LOGIN, IO_HANDSHAKE_LOGIN }  = require('jsonql-constants')

/**
 * please note we only support NSP connection!
 * @param {string} url the end point to conect to
 * @param {string} token to use for auth
 * @param {object} [options={}] optional configuration for init the socket.io-client
 * @return {object} promise resolve the socket.io-client instance
 */
function socketIoNodeHandshakeLogin(url, token, options = {}) {
  return Reflect.apply(socketIoHandshakeLogin, null, [socketIoClientModule, url, token, options])
}

/**
 * This one is a bit tricky because it has to get call after the connection is inited
 * @param {string} url the nsp endpoint
 * @param {string} token for auth
 * @param {object} [options={}] optional
 * @return {object} a promise resolve the onAuthenitcated and catch on onUnauthorized
 */
function socketIoNodeRoundtripLogin(url, token, options = {}) {
  return Reflect.apply(socketIoRoundtripLogin, null, [socketIoClientModule, url, token, options])
}

/**
 * @param {string} url end point
 * @param {object} [options = {}] configuration
 * @return {object} io instance
 */
function socketIoNodeClient(url, options = {}) {
  return Reflect.apply(socketIoClient, null, [socketIoClientModule, url, options])
}

/**
 * Async version of the above api
 * @param {string} url end point
 * @param {object} [options = {}] configuration
 * @return {object} io instance
 */
function socketIoNodeClientAsync(...args) {
  return Reflect.apply(socketIoClientAsync, null, [socketIoClientModule, ...args])
}

/**
 * create the chain connect auth client
 * @param {string} baseUrl to connect
 * @param {array} namespaces to append to baseUrl
 * @param {string} token for validation
 * @param {array} options passing to the clients
 * @return {object} promise resolved n*nsps in order
 */
function socketIoChainHSConnect(baseUrl, namespaces, token, options) {
  let args = [baseUrl, namespaces, token, IO_HANDSHAKE_LOGIN, options]
  return Reflect.apply(socketIoChainConnect, null, [socketIoClientModule, ...args])
}

// the round trip version
function socketIoChainRTConnect(baseUrl, namespaces, token, options) {
  let args = [baseUrl, namespaces, token, IO_ROUNDTRIP_LOGIN, options]
  return Reflect.apply(socketIoChainConnect, null, [socketIoClientModule, ...args])
}

// export
module.exports = {
  socketIoNodeHandshakeLogin,
  socketIoNodeRoundtripLogin,
  socketIoNodeClient,
  socketIoNodeClientAsync,
  socketIoChainHSConnect,
  socketIoChainRTConnect
}
