// import { isString } from 'jsonql-params-validator';
import { JsonqlValidationError } from 'jsonql-errors';
import socketIoClient from './socket-io-client'
/**
 * The core method of the socketJwt client side
 * @param {object} socket the socket.io connected instance
 * @param {string} token for validation
 * @param {function} onAuthenitcated callback when authenticated
 * @param {function} onUnauthorized callback when authorized
 * @return {void}
 */
const socketIoLoginAction = (socket, token, onAuthenticated, onUnauthorized) => {
  socket
    .emit('authenticate', { token })
    .on('authenticated', onAuthenticated)
    .on('unauthorized', onUnauthorized)
}

/**
 * completely rethink about how the browser version should be!
 *
 */
export default function socketIoRoundtripLogin(io, url, token, options = {}) {
  const socket = socketIoClient(io, url)
  return new Promise((resolver, rejecter) => {
    socketIoLoginAction(socket, token, () => resolver(socket) , rejecter);
  })
}
