
/**
 * create a normalize method to grab the decoded token data from socket
 * @param {object} socket io
 * @return {object|boolean} false on failed
 */
module.exports = function getUserdata(socket) {
  if (socket) {
    if (socket.decoded_token) {
      return socket.decoded_token;
    }
    if (socket.handshake && socket.handshake.decoded_token) {
      return socket.handshake.decoded_token
    }
  }
  return false;
}
