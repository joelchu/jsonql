// Testing with jwt method
const debug = require('debug')('jsonql-ws-server:test:socket.io')
const { join } = require('path')
const fsx = require('fs-extra')
const test = require('ava')
const socketIoClient = require('socket.io-client')
const { JSONQL_PATH, RSA_ALGO } = require('jsonql-constants')
const socketIo = require('socket.io')
const { socketIoNodeRoundtripLogin, socketIoNodeClient } = require('jsonql-jwt')
const createToken = require('./fixtures/token')
const secret = '12345678';

const contractDir = join(__dirname, 'fixtures', 'contract', 'auth')
const contractFile = join(contractDir, 'contract.json')
const contract = fsx.readJsonSync(contractFile)
const keysDir = join(__dirname, 'fixtures', 'keys')
const publicKey = fsx.readFileSync(join(keysDir, 'publicKey.pem'))
const setup = require('./fixtures/full-setup')

const port = 8888;
const payload = {name: 'Joel'};
const msg = 'Hello there!';

test.before(async t => {
  const { app, io } = await setup({
    contractDir,
    contract,
    useJwt: secret,
    privateMethodDir: 'private',
    enableAuth: true
  })
  t.context.io = io;

  t.context.baseUrl = `ws://localhost:${port}/${JSONQL_PATH}/`;

  t.context.server = app.listen(port)

  t.context.token = createToken(payload, secret)

});

test.after(t => {
  t.context.server.close()
})

// private
test.cb("It should able to connect to socket.io private namespace with a token", t => {
  t.plan(2);

  t.truthy(t.context.io[[JSONQL_PATH, 'private'].join('/')])

  debug('token:', t.context.token)

  socketIoNodeRoundtripLogin(t.context.baseUrl + 'private', t.context.token)
    .then(client => {
      client.emit('secretChatroom', {args: ['back', 'stuff']} , reply => {
        debug(reply)
        t.truthy(reply)
        t.end()
      })
    })
    .catch(err => {
      debug('died!', err)
      t.fail()
    })
})

// public
test.cb('It should able to connect to a socket.io public namespace', t => {

  t.plan(2)

  let client_public = socketIoNodeClient(t.context.baseUrl + 'public')

  t.truthy(t.context.io[ [JSONQL_PATH, 'public'].join('/') ])

  client_public.on('connect', () => {
    client_public.emit('availableToEveryone', {args: []} , reply => {
      debug('reply', reply)
      t.truthy(reply.data)
      t.end()
    })
  })
})
