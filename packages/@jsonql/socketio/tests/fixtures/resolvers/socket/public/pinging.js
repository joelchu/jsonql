// this is a public method always avaialble
let ctn = 0;
/**
 * @param {string} msg message
 * @return {string} reply message based on your message
 */
module.exports = function pinging(msg) {
  if (ctn > 0) {
    switch (msg) {
      case 'ping':
        pinging.send = 'pong';
      case 'pong':
        pinging.send = 'ping';
      default:
        return;
        //pinging.send = 'You lose!';
    }
    return;
  }
  ++ctn;
  return 'connection established';
}
