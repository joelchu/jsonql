
const { join } = require('path');

module.exports = {
  contractDir: join(__dirname, 'contract', 'auth'),
  resolverDir: join(__dirname, 'resolvers'),
  public: true,
  enableAuth: true,
  useJwt: true
}
