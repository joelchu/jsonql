const http = require('http')
const fsx = require('fs-extra')
const { join } = require('path')
const debug = require('debug')('jsonql-ws-client:fixtures:server')
const { JSONQL_PATH } = require('jsonql-constants')

const resolverDir = join(__dirname, 'resolvers')
const contractDir = join(__dirname, 'contract')
// require('../../../ws-server')
const wsServer = require('jsonql-ws-server')

// start
const server = http.createServer(function(req, res) {
  res.writeHead(200, { 'Content-Type': 'text/plain' })
  res.write('request successfully proxied!' + '\n' + JSON.stringify(req.headers, true, 2))
  res.end()
})

module.exports = function(extra = {}) {
  // extra.contract = extra.contract || contract;
  return new Promise(resolver => {
    wsServer(
      Object.assign({
        resolverDir,
        contractDir,
        serverType: 'ws'
      }, extra),
      server
    )
    .then(io => {
      resolver({
        io,
        app: server
      });
    })
  });
}
