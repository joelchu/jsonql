// testing the socket.io auth with client and server
const test = require('ava')
const http = require('http')
const socketIo = require('socket.io')
// const socketIoClient = require('socket.io-client')
const debug = require('debug')('jsonql-jwt:test:socketio-auth')
// import server
const { socketIoJwtAuth, socketIoHandshakeAuth, socketIoNodeRoundtripLogin } = require('../main')
// import data
const { secret, token, namespace, msg } = require('./fixtures/options')

const namespace1 = '/' + namespace + '/a';
const namespace2 = '/' + namespace + '/b';
const port = 3002;

const url = `http://localhost:${port}`;

test.before( t => {
  // setup server
  const server = http.createServer((req, res) => {
    res.writeHead(200, {'Content-Type': 'text/plain'})
    res.write('Hello world!')
    res.end()
  });
  t.context.server = server;
  // setup socket.io
  const io = socketIo(server)
  const nsp1 = io.of(namespace1)

  t.context.nsp1 = socketIoJwtAuth(nsp1, { secret: 'wrong-secret' })

  t.context.server.listen(port)
});

test.after( t => {
  t.context.server.close()
});

test.cb('It should failed when we pass a wrong secret', t => {
  t.plan(1)
  t.context.nsp1.then( socket => {
    setTimeout(() => {
      socket.emit('hello', msg)
    }, 500)
  })

  socketIoNodeRoundtripLogin(`${url}${namespace1}`, token)
    .then(client => {
      client.on('hello', message => {
        t.is(message, msg)
        t.end()
      })
    })
    .catch(err => {
      debug('did get the catch error!', err)
      t.pass()
      t.end()
    })

})
