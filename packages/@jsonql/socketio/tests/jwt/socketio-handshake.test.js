const test = require('ava')
const http = require('http')
const socketIo = require('socket.io')
const socketIoClient = require('socket.io-client')
const debug = require('debug')('jsonql-jwt:test:socketio-auth')
const { join } = require('path')
const fsx = require('fs-extra')
const { BASE64_FORMAT, RSA_ALGO } = require('jsonql-constants')
// import server
const {
  socketIoHandshakeAuth,
  jwtRsaToken,
  socketIoGetUserdata,
  socketIoNodeClientAsync,
  socketIoNodeHandshakeLogin,
  chainPromises

} = require('../main')
// import data
const { secret, token, namespace, msg } = require('./fixtures/options')
const baseDir = join(__dirname, 'fixtures', 'keys')
const namespace1 = '/' + namespace + '/a';
const namespace2 = '/' + namespace + '/b';
const port = 3005;
const payload = {name: 'Joel'};
const url = `http://localhost:${port}`;

test.before( t => {
  t.context.publicKey = fsx.readFileSync(join(baseDir, 'publicKey.pem'))
  t.context.privateKey = fsx.readFileSync(join(baseDir, 'privateKey.pem'))

  t.context.token = jwtRsaToken(payload, t.context.privateKey);

  // setup server
  const server = http.createServer((req, res) => {
    res.writeHead(200, {'Content-Type': 'text/plain'})
    res.write('Hello world!')
    res.end()
  })

  t.context.server = server;
  // setup socket.io
  const io = socketIo(server)

  const nsp1 = io.of(namespace1)
  const nsp2 = io.of(namespace2)

  t.context.nsp1 = socketIoHandshakeAuth(nsp1, {
    secret: Buffer.from(t.context.publicKey, BASE64_FORMAT),
    algorithms: RSA_ALGO
  })

  t.context.nsp2 = nsp2;

  t.context.server.listen(port)
});

test.after( t => {
  t.context.server.close()
});

test.serial.cb('It should able to connect to nsp during handshake', t => {
  t.plan(4)
  let nsp1 = t.context.nsp1;
  let nsp2 = t.context.nsp2;
  nsp1.on('connection', (sock) => {
    let userdata = socketIoGetUserdata(sock)
    debug('nsp1 connection established', userdata)
    setTimeout(() => {
      sock.emit('hello2', msg)
    }, 500)
    sock.on('hello1', m => {
      // debug('hello1', m);
      t.truthy(m) // 1
    })
  })
  nsp2.on('connection', sock2 => {
    sock2.on('hello', m => {
      debug('hello', m)
      t.is(m, 'world') // 2
    })
  })
  socketIoNodeHandshakeLogin(`${url}${namespace1}`, t.context.token)
    .then(client1 => {
      client1.emit('hello1', 'WTF?')
      debug('connect to nsp1 established')
      client1.on('hello2', message => {
        t.is(message, msg) // 3
        client1.disconnect()
        t.end()
      })
      // try to connect to the public nsp here
      socketIoNodeClientAsync(`${url}${namespace2}`)
        .then(client2 => {
          debug('connect to nsp2 established')
          t.truthy(client2) // 4
          client2.emit('hello', 'world')
          setTimeout(() => {
            client2.disconnect()
          }, 500)
        })

    })
    .catch(() => {
      debug('login failed!')
    })
})

test.serial.cb('Try to use the chain promise method with manually insert connector', t => {
  t.plan(3)
  let nsp1 = t.context.nsp1;
  let nsp2 = t.context.nsp2;
  nsp1.on('connection', sock1 => {
    let userdata = socketIoGetUserdata(sock1)
    t.is(userdata.name, payload.name)
  })

  nsp2.on('connection', sock2 => {
    sock2.on('hello', msg => {
      t.is(msg, 'world')
      t.end()
    })
  })

  let p1 = () => socketIoNodeHandshakeLogin(`${url}${namespace1}`, t.context.token)
  let p2 = () => socketIoNodeClientAsync(`${url}${namespace2}`)

  chainPromises([p1(), p2()])
    .then(nsps => {
      t.is(nsps.length,2)
      nsps[1].emit('hello', 'world')
    })
})
