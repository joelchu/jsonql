/**
 * Rollup config for just building out the decode token to share between cjs and browser
 */
import { join } from 'path'
import buble from 'rollup-plugin-buble'

// import { terser } from "rollup-plugin-terser";
import replace from 'rollup-plugin-replace'
import commonjs from 'rollup-plugin-commonjs'

import json from 'rollup-plugin-json'

import nodeResolve from 'rollup-plugin-node-resolve'
import nodeGlobals from 'rollup-plugin-node-globals'
import builtins from 'rollup-plugin-node-builtins'
import size from 'rollup-plugin-bundle-size'
// support async functions
import async from 'rollup-plugin-async'
// get the version info
import { version } from './package.json'

const env = process.env.NODE_ENV;

let plugins = [
  json({
    preferConst: true
  }),
  buble({
    objectAssign: 'Object.assign'
  }),
  nodeResolve({ preferBuiltins: true }),
  commonjs({
    include: 'node_modules/**'
  }),
  nodeGlobals(),
  builtins(),
  async(),
  replace({
    'process.env.NODE_ENV': JSON.stringify('production'),
    '__PLACEHOLDER__': `version: ${version} module: ${env==='prod' ? 'cjs' : 'umd'}`
  }),
  size()
];

let config = {
  input: join(__dirname, 'src', 'client', 'socketio', 'index.js'),
  output: {
    name: 'jsonqlJwtSocketIoClient',
    file: join(__dirname, 'src', 'server', 'socketio' , 'clients.js'),
    format: 'cjs',
    sourceMap: false,
    globals: {
      'jwt-decode': 'jwt_decode',
      'jsonql-params-validator': 'jsonqlParamsValidator',
      'jsonql-errors': 'jsonqlErrors',
      'jsonwebtoken': 'jwt',
      'socket.io-client': 'io',
      'promise-polyfill': 'Promise',
      'debug': 'debug'
    }
  },
  external: [
    'jwt-decode',
    'jsonql-params-validator',
    'jsonql-errors',
    'jsonwebtoken',
    'jwt',
    'WebSocket',
    'socket.io-client',
    'io',
    'debug',
    'Promise',
    'promise-polyfill'
  ],
  plugins: plugins
};

export default config;
