// test the basic connection and such
const test = require('ava')
// const WebSocket = require('ws')
const { join } = require('path')
const fsx = require('fs-extra')

const { wsNodeClient } = require('jsonql-jwt')

const wsServer = require('./fixtures/server')
const { JSONQL_PATH, ERROR_TYPE } = require('jsonql-constants')
const debug = require('debug')('jsonql-ws-server:test:ws')

const contractDir = join(__dirname, 'fixtures', 'contract', 'es6')
const resolverDir = join(__dirname, 'fixtures', 'es6resolvers')
const contractFile = join(contractDir, 'contract.json')
const contract = fsx.readJsonSync(contractFile)

const { extractWsPayload } = require('../lib/share/helpers')
const createPayload = require('./fixtures/create-payload')
const port = 8898;
const msg = 'Something';
let ctn = 0;

test.before(async t => {
  const { app, io } = await wsServer({
    contractDir,
    resolverDir,
    contract
  })
  t.context.io = io;
  t.context.server = app;
  t.context.server.listen(port)

  t.context.client = wsNodeClient(`ws://localhost:${port}/${JSONQL_PATH}`)
});

test.after(t => {
  t.context.server.close()
});

test.cb('Grouping all test together as one because the way ws reponse to on.message', t => {
  t.plan(6);
  // connect to the server
  let client = t.context.client;

  t.is(true , t.context.io[JSONQL_PATH] !== undefined)

  client.on('open', () => {

    client.send( createPayload('wsHandler', msg + ' 1st', Date.now()) )

    client.send( createPayload('chatroom', msg + ' 2nd', Date.now()) )

    setTimeout(() => {
      client.send( createPayload('causeError', msg + ' 3nd') )
    }, 500)

  })

  // wait for reply
  client.on('message', (data) => {
    let json;
    try {
      json = extractWsPayload(data)
      debug('on message received: ', json)
      switch (json.resolverName) {
        case 'wsHandler':
          t.truthy(json.data, 'wsHandler should reply with a message')
          break;
        case 'chatroom':
          t.truthy(json.data, 'chatroom should reply with a message')
          break;
        default:
          debug('catch error here %O', json)
          let { data } = json;

          t.truthy(data.error, 'causeError should have error field')
          t.is(json.type, ERROR_TYPE)
          t.true(data.error.className === 'JsonqlResolverAppError' && data.error.message === 'causeError')

          t.end()
      }
    } catch(e) {

      debug('error', e)
      /*
      let { data } = error;
      t.true(data.error.className === 'JsonqlResolverAppError' && data.error.message === 'causeError')
      t.is(data.error.type, ERROR_TYPE)
      t.truthy(data.error, 'causeError should have error field')
      */

      t.end()
    }
  });
});
