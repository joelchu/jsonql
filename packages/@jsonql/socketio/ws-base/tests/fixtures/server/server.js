const http = require('http')
const fsx = require('fs-extra')
const debug = require('debug')('jsonql-ws-server:fixtures:server')
const fs = require('fs')
const { join } = require('path')
const { JSONQL_PATH } = require('jsonql-constants')

const resolverDir = join(__dirname, 'resolvers')
const contractDir = join(__dirname, 'contract')

const wsServer = require('../../index')

// start
const server = http.createServer(function(req, res) {
  res.writeHead(200, { 'Content-Type': 'text/plain' })
  res.write('request successfully proxied!' + '\n' + JSON.stringify(req.headers, true, 2))
  res.end()
})

module.exports = function(extra = {}) {
  // extra.contract = extra.contract || contract;
  return new Promise(resolver => {
    wsServer(
      Object.assign({
        resolverDir,
        contractDir,
        serverType: 'ws'
      }, extra),
      server
    )
    .then(io => {
      resolver({
        io,
        app: server
      })
    })
  })
}
