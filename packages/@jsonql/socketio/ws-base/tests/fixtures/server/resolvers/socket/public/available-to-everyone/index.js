// This method for testing the public call

/**
 * There is no parameter require for this call
 * @return {string} a message
 */
module.exports = function availableToEveryone() {
  return 'You get a public message';
}
