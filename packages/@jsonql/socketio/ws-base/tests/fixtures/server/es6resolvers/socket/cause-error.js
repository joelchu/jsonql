// this method will throw an error

/**
 * @param {string} msg a message
 * @return {string} a message but here we throw an error
 */
export default function causeError(msg) {
  throw new Error(msg)
}
