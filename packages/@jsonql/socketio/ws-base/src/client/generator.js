// DEPRECATED for reference ONLY

// generator resolvers
// this will be a mini client server architect
// The reason is when the enableAuth setup - the private route
// might not be validated, but we need the callable point is ready
// therefore this part will always take the contract and generate
// callable api for the developer to setup their front end
// the only thing is - when they call they might get an error or
// NOT_LOGIN_IN and they can react to this error accordingly
import {
  JsonqlResolverNotFoundError,
  JsonqlValidationError,
  JsonqlError,
  finalCatch
} from 'jsonql-errors'
import {
  validateAsync,
  validateSync,
  isObjectHasKey,
  isString
} from 'jsonql-params-validator'
import {
  ERROR_TYPE,
  DATA_KEY,
  ERROR_KEY,
  ON_ERROR_PROP_NAME,
  ON_MESSAGE_PROP_NAME,
  ON_RESULT_PROP_NAME,
  SEND_MSG_PROP_NAME,
  LOGIN_EVENT_NAME,
  ON_READY_PROP_NAME,
  LOGOUT_EVENT_NAME
} from 'jsonql-constants'
const { injectToFn, objDefineProps } from 'jsonql-utils'

import { getDebug, constants, createEvt, toArray } from './utils'
const { EMIT_EVT, NOT_ALLOW_OP, UNKNOWN_RESULT, MY_NAMESPACE } = constants;
const debugFn = getDebug('generator')

/**
 * prepare the methods
 * @param {object} opts configuration
 * @param {object} nspMap resolvers index by their namespace
 * @param {object} ee EventEmitter
 * @return {object} of resolvers
 * @public
 */
export default function generator(opts, nspMap, ee) {
  const obj = {};
  const { nspSet } = nspMap;
  for (let namespace in nspSet) {
    let list = nspSet[namespace]
    for (let resolverName in list) {
      let params = list[resolverName]
      let fn = createResolver(ee, namespace, resolverName, params)
      obj[resolverName] = setupResolver(namespace, resolverName, params, fn, ee)
    }
  }
  // add error handler
  createNamespaceErrorHandler(obj, ee, nspSet)
  // add onReady handler
  createOnReadyhandler(obj, ee, nspSet)
  // Auth related methods
  createAuthMethods(obj, ee, opts)
  // this is a helper method for the developer to know the namespace inside
  obj.getNsp = () => {
    return Object.keys(nspSet)
  }
  // output
  return obj;
}

/**
 * create the actual function to send message to server
 * @param {object} ee EventEmitter instance
 * @param {string} namespace this resolver end point
 * @param {string} resolverName name of resolver as event name
 * @param {object} params from contract
 * @return {function} resolver
 */
function createResolver(ee, namespace, resolverName, params) {
  // note we pass the new withResult=true option
  return function(...args) {
    return validateAsync(args, params.params, true)
      .then( _args => actionCall(ee, namespace, resolverName, _args) )
      .catch(finalCatch)
  }
}

/**
 * just wrapper
 * @param {object} ee EventEmitter
 * @param {string} namespace where this belongs
 * @param {string} resolverName resolver
 * @param {array} args arguments
 * @return {void} nothing
 */
function actionCall(ee, namespace, resolverName, args = []) {
  debugFn(`actionCall: ${namespace} ${resolverName}`, args)
  ee.$trigger(createEvt(namespace, EMIT_EVT), [
    resolverName,
    toArray(args)
  ])
}

/**
 * break out to use in different places to handle the return from server
 * @param {object} data from server
 * @param {function} resolver from promise
 * @param {function} rejecter from promise
 * @return {void} nothing
 */
function respondHandler(data, resolver, rejecter) {
  if (isObjectHasKey(data, ERROR_KEY)) {
    debugFn('rejecter called', data[ERROR_KEY])
    rejecter(data[ERROR_KEY])
  } else if (isObjectHasKey(data, DATA_KEY)) {
    debugFn('resolver called', data[DATA_KEY])
    resolver(data[DATA_KEY])
  } else {
    debugFn('UNKNOWN_RESULT', data)
    rejecter({message: UNKNOWN_RESULT, error: data})
  }
}

/**
 * Add extra property to the resolver
 * @param {string} namespace where this belongs
 * @param {string} resolverName name as event name
 * @param {object} params from contract
 * @param {function} fn resolver function
 * @param {object} ee EventEmitter
 * @return {function} resolver
 */
const setupResolver = (namespace, resolverName, params, fn, ee) => {
  // also need to setup a getter to get back the namespace of this resolver
  let _fn = injectToFn(fn, MY_NAMESPACE, namespace)
  // onResult handler
  _fn = objDefineProps(_fn, ON_RESULT_PROP_NAME, function(resultCallback) {
      if (typeof resultCallback === 'function') {
        ee.$only(
          createEvt(namespace, resolverName, ON_RESULT_PROP_NAME),
          function resultHandler(result) {
            respondHandler(result, resultCallback, (error) => {
              ee.$trigger(createEvt(namespace, resolverName, ON_ERROR_PROP_NAME), error)
            })
          }
        )
      }
    })
  // we do need to add the send prop back because it's the only way to deal with
  // bi-directional data stream
  _fn = objDefineProps(_fn, ON_MESSAGE_PROP_NAME, function(messageCallback) {
    // we expect this to be a function
    if (typeof messageCallback === 'function') {
      // did that add to the callback
      let onMessageCallback = (args) => {
        respondHandler(args, messageCallback, (error) => {
          ee.$trigger(createEvt(namespace, resolverName, ON_ERROR_PROP_NAME), error)
        })
      }
      // register the handler for this message event
      ee.$only(createEvt(namespace, resolverName, ON_MESSAGE_PROP_NAME), onMessageCallback)
    }
  })
  // add an ON_ERROR_PROP_NAME handler
  _fn = objDefineProps(_fn, ON_ERROR_PROP_NAME, function(resolverErrorHandler) {
    if (typeof resolverErrorHandler === 'function') {
      // please note ON_ERROR_PROP_NAME can add multiple listners
      ee.$only(createEvt(namespace, resolverName, ON_ERROR_PROP_NAME), resolverErrorHandler)
    }
  })
  // pairing with the server vesrion SEND_MSG_PROP_NAME
  _fn = objDefineProps(_fn, SEND_MSG_PROP_NAME, function(messagePayload) {
    const result = validateSync(toArray(messagePayload), params.params, true)
    // here is the different we don't throw erro instead we trigger an
    // onError
    if (result[ERROR_KEY] && result[ERROR_KEY].length) {
      ee.$call(
        createEvt(namespace, resolverName, ON_ERROR_PROP_NAME),
        [JsonqlValidationError(resolverName, result[ERROR_KEY])]
      )
    } else {
      // there is no return only an action call
      actionCall(ee, namespace, resolverName, result[DATA_KEY])
    }
  })
  return _fn;
}

/**
 * The problem is the namespace can have more than one
 * and we only have on onError message
 * @param {object} obj the client itself
 * @param {object} ee Event Emitter
 * @param {object} nspSet namespace keys
 * @return {void}
 */
const createNamespaceErrorHandler = (obj, ee, nspSet) => {
  // using the onError as name
  // @TODO we should follow the convention earlier
  // make this a setter for the obj itself
  objDefineProps(obj, ON_ERROR_PROP_NAME, function(namespaceErrorHandler) {
    if (typeof namespaceErrorHandler === 'function') {
      // please note ON_ERROR_PROP_NAME can add multiple listners
      for (let namespace in nspSet) {
        // this one is very tricky, we need to make sure the trigger is calling
        // with the namespace as well as the error
        ee.$on(createEvt(namespace, ON_ERROR_PROP_NAME), namespaceErrorHandler)
      }
    }
  })
}

/**
 * This event will fire when the socket.io.on('connection') and ws.onopen
 * @param {object} obj the client itself
 * @param {object} ee Event Emitter
 * @param {object} nspSet namespace keys
 * @return {void}
 */
const createOnReadyhandler = (obj, ee, nspSet) => {
  objDefineProps(obj, ON_READY_PROP_NAME, function(onReadyCallback) {
    if (typeof onReadyCallback === 'function') {
      // reduce it down to just one flat level
      let result = ee.$on(ON_READY_PROP_NAME, onReadyCallback)
    }
  })
}

/**
 * Create auth related methods
 * @param {object} obj the client itself
 * @param {object} ee Event Emitter
 * @param {object} opts configuration
 * @return {void}
 */
const createAuthMethods = (obj, ee, opts) => {
  if (opts.enableAuth) {
    // create an additonal login handler
    // we require the token
    obj[opts.loginHandlerName] = (token) => {
      debugFn(opts.loginHandlerName, token)
      if (token && isString(token)) {
        return ee.$trigger(LOGIN_EVENT_NAME, [token])
      }
      throw new JsonqlValidationError(opts.loginHandlerName)
    }
    // logout event handler
    obj[opts.logoutHandlerName] = (...args) => {
      ee.$trigger(LOGOUT_EVENT_NAME, args)
    }
  }
}
