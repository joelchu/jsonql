// @TODO port what is in the ws-main-handler
// because all the client side call are via the ee
// and that makes it re-usable between different client setup
import {
  ON_MESSAGE_PROP_NAME,
  ON_RESULT_PROP_NAME,
  EMIT_EVT,
  SOCKET_IO,
  WS
} from './utils/constants'
import {
  LOGIN_EVENT_NAME,
  LOGOUT_EVENT_NAME,
  NOT_LOGIN_ERR_MSG,
  ON_ERROR_PROP_NAME
} from 'jsonql-constants'

import { getDebug, createEvt, clearMainEmitEvt } from './utils'
import triggerNamespacesOnError from './utils/trigger-namespaces-on-error'
const debugFn = getDebug('client-event-handler')

/**
 * A fake ee handler
 * @param {string} namespace nsp
 * @param {object} ee EventEmitter
 * @return {void}
 */
const notLoginWsHandler = (namespace, ee) => {
  ee.$only(
    createEvt(namespace, EMIT_EVT),
    function(resolverName, args) {
      debugFn('noLoginHandler hijack the ws call', namespace, resolverName, args)
      let error = {
        message: NOT_LOGIN_ERR_MSG
      }
      // It should just throw error here and should not call the result
      // because that's channel for handling normal event not the fake one
      ee.$call(createEvt(namespace, resolverName, ON_ERROR_PROP_NAME), [error])
      // also trigger the result handler, but wrap inside the error key
      ee.$call(createEvt(namespace, resolverName, ON_RESULT_PROP_NAME), [{ error }])
    }
  )
}

/**
 * centralize all the comm in one place
 * @param {object} opts configuration
 * @param {array} namespaces namespace(s)
 * @param {object} ee Event Emitter instance
 * @param {function} bindWsHandler binding the ee to ws
 * @param {array} namespaces array of namespace available
 * @param {object} nsps namespaced nsp
 * @return {void} nothing
 */
export default function clientEventHandler(opts, nspMap, ee, bindWsHandler, namespaces, nsps) {
  // loop
  // @BUG for io this has to be in order the one with auth need to get call first
  // The order of login is very import we need to run a waterfall here to make sure
  // one is execute then the other
  namespaces.forEach(namespace => {
    if (nsps[namespace]) {
      debugFn('call bindWsHandler', namespace)
      let args = [namespace, nsps[namespace], ee]
      if (opts.serverType === SOCKET_IO) {
        let { nspSet } = nspMap;
        args.push(nspSet[namespace])
        args.push(opts)
      }
      Reflect.apply(bindWsHandler, null, args)
    } else {
      // a dummy placeholder
      notLoginWsHandler(namespace, ee)
    }
  })
  // this will be available regardless enableAuth
  // because the server can log the client out
  ee.$on(LOGOUT_EVENT_NAME, function() {
    debugFn('LOGOUT_EVENT_NAME')
    // disconnect(nsps, opts.serverType)
    // we need to issue error to all the namespace onError handler
    triggerNamespacesOnError(ee, namespaces, LOGOUT_EVENT_NAME)
    // rebind all of the handler to the fake one
    namespaces.forEach( namespace => {
      clearMainEmitEvt(ee, namespace)
      // clear out the nsp
      nsps[namespace] = false;
      // add a NOT LOGIN error if call
      notLoginWsHandler(namespace, ee)
    })
  })
}
