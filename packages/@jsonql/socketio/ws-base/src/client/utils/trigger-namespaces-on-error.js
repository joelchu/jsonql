
import { ON_ERROR_PROP_NAME } from 'jsonql-constants'
import { createEvt } from './index'
/**
 * trigger errors on all the namespace onError handler
 * @param {object} ee Event Emitter
 * @param {array} namespaces nsps string
 * @param {string} message optional
 * @return {void}
 */
export default function triggerNamespacesOnError(ee, namespaces, message) {
  namespaces.forEach( namespace => {
    ee.$call(createEvt(namespace, ON_ERROR_PROP_NAME), [{ message, namespace }])
  })
}
