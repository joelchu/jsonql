// generate the web socket connect client for browser
/*
all these has moved to other standalone modules
import {
  socketIoRoundtripLogin,
  socketIoClientAsync,
  socketIoHandshakeLogin,
  wsAuthClient,
  wsClient
} from 'jsonql-jwt'
*/
import { isString } from 'jsonql-params-validator'
import { JsonqlError } from 'jsonql-errors'
import { SOCKET_IO, WS } from './constants'
// import { IO_ROUNDTRIP_LOGIN, IO_HANDSHAKE_LOGIN } from 'jsonql-constants'
import getDebug from './get-debug'
const debug = getDebug('client-generator')

/**
 * create the web socket client
 * @param {object} payload passing
 * @return {object} just mutate it then pass it on
 */
export default function clientGenerator({opts, nspMap, ee}) {
  switch (opts.serverType) {
    case SOCKET_IO:
        opts.nspClient = (...args) => (
          Reflect.apply(socketIoClientAsync, null, [io, ...args])
        )
        if (isString(opts.useJwt)) {
          opts.nspAuthClient = (...args) => (
            Reflect.apply(socketIoRoundtripLogin, null, [io, ...args])
          )
        } else {
          opts.nspAuthClient = (...args) => (
            Reflect.apply(socketIoHandshakeLogin, null, [io, ...args])
          )
        }
      break;
    case WS:
        opts.nspClient = wsClient;
        opts.nspAuthClient = wsAuthClient;
      break;
    default:
      throw new JsonqlError(`Unknown serverType: ${opts.serverType}`)
  }
  return {opts, nspMap, ee}
}
