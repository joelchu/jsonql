// client generator for node.js
const { chainPromises } = require('jsonql-jwt')
const { JsonqlError } = require('jsonql-errors')
const { JS_WS_SOCKET_IO_NAME, JS_WS_NAME } = require('jsonql-constants')
const { isString } = require('jsonql-params-validator')
const debug = require('debug')('jsonql-ws-client:client-generator:cjs')

/**
 * @TODO we have taken out all the socket client to their respective package
 * now we need to figure out how to inject them back into this client generator
 * websocket client generator
 * @param {object} payload with opts, nspMap, ee
 * @return {object} same just mutate it
 */
const clientGenerator = ({ opts, nspMap, ee }) => {
  // debug(nspMap)
  switch (opts.serverType) {
    case JS_WS_SOCKET_IO_NAME:
        // the socket.io normal client is not Promise so we make them all the same
        opts.nspClient = socketIoNodeClientAsync;
        // (...args) => Promise.resolve(Reflect.apply(socketIoNodeClient, null, args))
        // we also need to determine the type of socket.io login here
        opts.nspAuthClient = isString(opts.useJwt) ? socketIoNodeRoundtripLogin : socketIoNodeHandshakeLogin;
        // debug(opts.nspAuthClient)
      break;
    case JS_WS_NAME:
        opts.nspClient = wsNodeClient;
        opts.nspAuthClient = wsNodeAuthClient;
      break;
    default:
      throw new JsonqlError(`Unknown serverType: ${opts.serverType}`)
  }
  return { opts, nspMap, ee }
}

// export it
module.exports = clientGenerator;
