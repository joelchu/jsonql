'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var debug$2 = _interopDefault(require('debug'));

/**
 * This is a custom error to throw when server throw a 406
 * This help us to capture the right error, due to the call happens in sequence
 * @param {string} message to tell what happen
 * @param {mixed} extra things we want to add, 500?
 */
var Jsonql406Error = /*@__PURE__*/(function (Error) {
  function Jsonql406Error() {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

    Error.apply(this, args);
    this.message = args[0];
    this.detail = args[1];
    // We can't access the static name from an instance
    // but we can do it like this
    this.className = Jsonql406Error.name;

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, Jsonql406Error);
    }
  }

  if ( Error ) Jsonql406Error.__proto__ = Error;
  Jsonql406Error.prototype = Object.create( Error && Error.prototype );
  Jsonql406Error.prototype.constructor = Jsonql406Error;

  var staticAccessors = { statusCode: { configurable: true },name: { configurable: true } };

  staticAccessors.statusCode.get = function () {
    return 406;
  };

  staticAccessors.name.get = function () {
    return 'Jsonql406Error';
  };

  Object.defineProperties( Jsonql406Error, staticAccessors );

  return Jsonql406Error;
}(Error));

/**
 * This is a custom error to throw when server throw a 500
 * This help us to capture the right error, due to the call happens in sequence
 * @param {string} message to tell what happen
 * @param {mixed} extra things we want to add, 500?
 */
var Jsonql500Error = /*@__PURE__*/(function (Error) {
  function Jsonql500Error() {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

    Error.apply(this, args);

    this.message = args[0];
    this.detail = args[1];

    this.className = Jsonql500Error.name;

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, Jsonql500Error);
    }
  }

  if ( Error ) Jsonql500Error.__proto__ = Error;
  Jsonql500Error.prototype = Object.create( Error && Error.prototype );
  Jsonql500Error.prototype.constructor = Jsonql500Error;

  var staticAccessors = { statusCode: { configurable: true },name: { configurable: true } };

  staticAccessors.statusCode.get = function () {
    return 500;
  };

  staticAccessors.name.get = function () {
    return 'Jsonql500Error';
  };

  Object.defineProperties( Jsonql500Error, staticAccessors );

  return Jsonql500Error;
}(Error));

/**
 * This is a custom error to throw when pass credential but fail
 * This help us to capture the right error, due to the call happens in sequence
 * @param {string} message to tell what happen
 * @param {mixed} extra things we want to add, 500?
 */
var JsonqlAuthorisationError = /*@__PURE__*/(function (Error) {
  function JsonqlAuthorisationError() {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

    Error.apply(this, args);
    this.message = args[0];
    this.detail = args[1];

    this.className = JsonqlAuthorisationError.name;

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, JsonqlAuthorisationError);
    }
  }

  if ( Error ) JsonqlAuthorisationError.__proto__ = Error;
  JsonqlAuthorisationError.prototype = Object.create( Error && Error.prototype );
  JsonqlAuthorisationError.prototype.constructor = JsonqlAuthorisationError;

  var staticAccessors = { statusCode: { configurable: true },name: { configurable: true } };

  staticAccessors.statusCode.get = function () {
    return 401;
  };

  staticAccessors.name.get = function () {
    return 'JsonqlAuthorisationError';
  };

  Object.defineProperties( JsonqlAuthorisationError, staticAccessors );

  return JsonqlAuthorisationError;
}(Error));

/**
 * This is a custom error when not supply the credential and try to get contract
 * This help us to capture the right error, due to the call happens in sequence
 * @param {string} message to tell what happen
 * @param {mixed} extra things we want to add, 500?
 */
var JsonqlContractAuthError = /*@__PURE__*/(function (Error) {
  function JsonqlContractAuthError() {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

    Error.apply(this, args);
    this.message = args[0];
    this.detail = args[1];

    this.className = JsonqlContractAuthError.name;

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, JsonqlContractAuthError);
    }
  }

  if ( Error ) JsonqlContractAuthError.__proto__ = Error;
  JsonqlContractAuthError.prototype = Object.create( Error && Error.prototype );
  JsonqlContractAuthError.prototype.constructor = JsonqlContractAuthError;

  var staticAccessors = { statusCode: { configurable: true },name: { configurable: true } };

  staticAccessors.statusCode.get = function () {
    return 401;
  };

  staticAccessors.name.get = function () {
    return 'JsonqlContractAuthError';
  };

  Object.defineProperties( JsonqlContractAuthError, staticAccessors );

  return JsonqlContractAuthError;
}(Error));

/**
 * This is a custom error to throw when the resolver throw error and capture inside the middleware
 * This help us to capture the right error, due to the call happens in sequence
 * @param {string} message to tell what happen
 * @param {mixed} extra things we want to add, 500?
 */
var JsonqlResolverAppError = /*@__PURE__*/(function (Error) {
  function JsonqlResolverAppError() {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

    Error.apply(this, args);

    this.message = args[0];
    this.detail = args[1];

    this.className = JsonqlResolverAppError.name;

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, JsonqlResolverAppError);
    }
  }

  if ( Error ) JsonqlResolverAppError.__proto__ = Error;
  JsonqlResolverAppError.prototype = Object.create( Error && Error.prototype );
  JsonqlResolverAppError.prototype.constructor = JsonqlResolverAppError;

  var staticAccessors = { statusCode: { configurable: true },name: { configurable: true } };

  staticAccessors.statusCode.get = function () {
    return 500;
  };

  staticAccessors.name.get = function () {
    return 'JsonqlResolverAppError';
  };

  Object.defineProperties( JsonqlResolverAppError, staticAccessors );

  return JsonqlResolverAppError;
}(Error));

/**
 * This is a custom error to throw when could not find the resolver
 * This help us to capture the right error, due to the call happens in sequence
 * @param {string} message to tell what happen
 * @param {mixed} extra things we want to add, 500?
 */
var JsonqlResolverNotFoundError = /*@__PURE__*/(function (Error) {
  function JsonqlResolverNotFoundError() {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

    Error.apply(this, args);

    this.message = args[0];
    this.detail = args[1];

    this.className = JsonqlResolverNotFoundError.name;

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, JsonqlResolverNotFoundError);
    }
  }

  if ( Error ) JsonqlResolverNotFoundError.__proto__ = Error;
  JsonqlResolverNotFoundError.prototype = Object.create( Error && Error.prototype );
  JsonqlResolverNotFoundError.prototype.constructor = JsonqlResolverNotFoundError;

  var staticAccessors = { statusCode: { configurable: true },name: { configurable: true } };

  staticAccessors.statusCode.get = function () {
    return 404;
  };

  staticAccessors.name.get = function () {
    return 'JsonqlResolverNotFoundError';
  };

  Object.defineProperties( JsonqlResolverNotFoundError, staticAccessors );

  return JsonqlResolverNotFoundError;
}(Error));

// this get throw from within the checkOptions when run through the enum failed
var JsonqlEnumError = /*@__PURE__*/(function (Error) {
  function JsonqlEnumError() {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

    Error.apply(this, args);
    
    this.message = args[0];
    this.detail = args[1];

    this.className = JsonqlEnumError.name;

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, JsonqlEnumError);
    }
  }

  if ( Error ) JsonqlEnumError.__proto__ = Error;
  JsonqlEnumError.prototype = Object.create( Error && Error.prototype );
  JsonqlEnumError.prototype.constructor = JsonqlEnumError;

  var staticAccessors = { name: { configurable: true } };

  staticAccessors.name.get = function () {
    return 'JsonqlEnumError';
  };

  Object.defineProperties( JsonqlEnumError, staticAccessors );

  return JsonqlEnumError;
}(Error));

// this will throw from inside the checkOptions
var JsonqlTypeError = /*@__PURE__*/(function (Error) {
  function JsonqlTypeError() {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

    Error.apply(this, args);

    this.message = args[0];
    this.detail = args[1];

    this.className = JsonqlTypeError.name;

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, JsonqlTypeError);
    }
  }

  if ( Error ) JsonqlTypeError.__proto__ = Error;
  JsonqlTypeError.prototype = Object.create( Error && Error.prototype );
  JsonqlTypeError.prototype.constructor = JsonqlTypeError;

  var staticAccessors = { name: { configurable: true } };

  staticAccessors.name.get = function () {
    return 'JsonqlTypeError';
  };

  Object.defineProperties( JsonqlTypeError, staticAccessors );

  return JsonqlTypeError;
}(Error));

// allow supply a custom checker function
// if that failed then we throw this error
var JsonqlCheckerError = /*@__PURE__*/(function (Error) {
  function JsonqlCheckerError() {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

    Error.apply(this, args);
    this.message = args[0];
    this.detail = args[1];

    this.className = JsonqlCheckerError.name;

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, JsonqlCheckerError);
    }
  }

  if ( Error ) JsonqlCheckerError.__proto__ = Error;
  JsonqlCheckerError.prototype = Object.create( Error && Error.prototype );
  JsonqlCheckerError.prototype.constructor = JsonqlCheckerError;

  var staticAccessors = { name: { configurable: true } };

  staticAccessors.name.get = function () {
    return 'JsonqlCheckerError';
  };

  Object.defineProperties( JsonqlCheckerError, staticAccessors );

  return JsonqlCheckerError;
}(Error));

// custom validation error class
// when validaton failed
var JsonqlValidationError = /*@__PURE__*/(function (Error) {
  function JsonqlValidationError() {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

    Error.apply(this, args);

    this.message = args[0];
    this.detail = args[1];

    this.className = JsonqlValidationError.name;

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, JsonqlValidationError);
    }
  }

  if ( Error ) JsonqlValidationError.__proto__ = Error;
  JsonqlValidationError.prototype = Object.create( Error && Error.prototype );
  JsonqlValidationError.prototype.constructor = JsonqlValidationError;

  var staticAccessors = { name: { configurable: true } };

  staticAccessors.name.get = function () {
    return 'JsonqlValidationError';
  };

  Object.defineProperties( JsonqlValidationError, staticAccessors );

  return JsonqlValidationError;
}(Error));

// the core stuff to id if it's calling with jsonql
var DATA_KEY = 'data';
var ERROR_KEY = 'error';

var JSONQL_PATH = 'jsonql';
var DEFAULT_TYPE = 'any';

// @TODO remove this is not in use
// export const CLIENT_CONFIG_FILE = '.clients.json';
// export const CONTRACT_CONFIG_FILE = 'jsonql-contract-config.js';
// type of resolvers
var QUERY_NAME = 'query';
var MUTATION_NAME = 'mutation';
var SOCKET_NAME = 'socket';
var QUERY_ARG_NAME = 'args';
// for  contract-cli
var KEY_WORD = 'continue';

var TYPE_KEY = 'type';
var OPTIONAL_KEY = 'optional';
var ENUM_KEY = 'enumv';  // need to change this because enum is a reserved word
var ARGS_KEY = 'args';
var CHECKER_KEY = 'checker';
var ALIAS_KEY = 'alias';
var LOGIN_NAME = 'login';
var ISSUER_NAME = LOGIN_NAME; // legacy issue need to replace them later
var LOGOUT_NAME = 'logout';

var OR_SEPERATOR = '|';

var STRING_TYPE = 'string';
var BOOLEAN_TYPE = 'boolean';
var ARRAY_TYPE = 'array';
var OBJECT_TYPE = 'object';

var NUMBER_TYPE = 'number';
var ARRAY_TYPE_LFT = 'array.<';
var ARRAY_TYPE_RGT = '>';

var NO_ERROR_MSG = 'No message';
var NO_STATUS_CODE = -1;
var LOGIN_EVENT_NAME = '__login__';
var LOGOUT_EVENT_NAME = '__logout__';

// for ws servers
var WS_REPLY_TYPE = '__reply__';
var WS_EVT_NAME = '__event__';
var WS_DATA_NAME = '__data__';
var EMIT_REPLY_TYPE = 'emit';
var ACKNOWLEDGE_REPLY_TYPE = 'acknowledge';
var ERROR_TYPE = 'error';

var JS_WS_SOCKET_IO_NAME = 'socket.io';
var JS_WS_NAME = 'ws';

// for ws client
var ON_MESSAGE_PROP_NAME = 'onMessage';
var ON_RESULT_PROP_NAME = 'onResult';
var ON_ERROR_PROP_NAME = 'onError';
var ON_READY_PROP_NAME = 'onReady';
var SEND_MSG_PROP_NAME = 'send';
var NOT_LOGIN_ERR_MSG = 'NOT LOGIN';
var HSA_ALGO = 'HS256';

/**
 * This is a custom error to throw whenever a error happen inside the jsonql
 * This help us to capture the right error, due to the call happens in sequence
 * @param {string} message to tell what happen
 * @param {mixed} extra things we want to add, 500?
 */
var JsonqlError = /*@__PURE__*/(function (Error) {
  function JsonqlError() {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

    Error.apply(this, args);

    this.message = args[0];
    this.detail = args[1];

    this.className = JsonqlError.name;

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, JsonqlError);
    }
  }

  if ( Error ) JsonqlError.__proto__ = Error;
  JsonqlError.prototype = Object.create( Error && Error.prototype );
  JsonqlError.prototype.constructor = JsonqlError;

  var staticAccessors = { name: { configurable: true },statusCode: { configurable: true } };

  staticAccessors.name.get = function () {
    return 'JsonqlError';
  };

  staticAccessors.statusCode.get = function () {
    return NO_STATUS_CODE;
  };

  Object.defineProperties( JsonqlError, staticAccessors );

  return JsonqlError;
}(Error));

// this is from an example from Koa team to use for internal middleware ctx.throw
// but after the test the res.body part is unable to extract the required data
// I keep this one here for future reference

var JsonqlServerError = /*@__PURE__*/(function (Error) {
  function JsonqlServerError(statusCode, message) {
    Error.call(this, message);
    this.statusCode = statusCode;
    this.className = JsonqlServerError.name;
  }

  if ( Error ) JsonqlServerError.__proto__ = Error;
  JsonqlServerError.prototype = Object.create( Error && Error.prototype );
  JsonqlServerError.prototype.constructor = JsonqlServerError;

  var staticAccessors = { name: { configurable: true } };

  staticAccessors.name.get = function () {
    return 'JsonqlServerError';
  };

  Object.defineProperties( JsonqlServerError, staticAccessors );

  return JsonqlServerError;
}(Error));

/**
 * this will put into generator call at the very end and catch
 * the error throw from inside then throw again
 * this is necessary because we split calls inside and the throw
 * will not reach the actual client unless we do it this way
 * @param {object} e Error
 * @return {void} just throw
 */
function finalCatch(e) {
  // this is a hack to get around the validateAsync not actually throw error
  // instead it just rejected it with the array of failed parameters
  if (Array.isArray(e)) {
    // if we want the message then I will have to create yet another function
    // to wrap this function to provide the name prop
    throw new JsonqlValidationError('', e);
  }
  var msg = e.message || NO_ERROR_MSG;
  var detail = e.detail || e;
  switch (true) {
    case e instanceof Jsonql406Error:
      throw new Jsonql406Error(msg, detail);
    case e instanceof Jsonql500Error:
      throw new Jsonql500Error(msg, detail);
    case e instanceof JsonqlAuthorisationError:
      throw new JsonqlAuthorisationError(msg, detail);
    case e instanceof JsonqlContractAuthError:
      throw new JsonqlContractAuthError(msg, detail);
    case e instanceof JsonqlResolverAppError:
      throw new JsonqlResolverAppError(msg, detail);
    case e instanceof JsonqlResolverNotFoundError:
      throw new JsonqlResolverNotFoundError(msg, detail);
    case e instanceof JsonqlEnumError:
      throw new JsonqlEnumError(msg, detail);
    case e instanceof JsonqlTypeError:
      throw new JsonqlTypeError(msg, detail);
    case e instanceof JsonqlCheckerError:
      throw new JsonqlCheckerError(msg, detail);
    case e instanceof JsonqlValidationError:
      throw new JsonqlValidationError(msg, detail);
    case e instanceof JsonqlServerError:
      throw new JsonqlServerError(msg, detail);
    default:
      throw new JsonqlError(msg, detail);
  }
}

var global$1 = (typeof global !== "undefined" ? global :
            typeof self !== "undefined" ? self :
            typeof window !== "undefined" ? window : {});

/** Detect free variable `global` from Node.js. */
var freeGlobal = typeof global$1 == 'object' && global$1 && global$1.Object === Object && global$1;

/** Detect free variable `self`. */
var freeSelf = typeof self == 'object' && self && self.Object === Object && self;

/** Used as a reference to the global object. */
var root = freeGlobal || freeSelf || Function('return this')();

/** Built-in value references. */
var Symbol = root.Symbol;

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var nativeObjectToString = objectProto.toString;

/** Built-in value references. */
var symToStringTag = Symbol ? Symbol.toStringTag : undefined;

/**
 * A specialized version of `baseGetTag` which ignores `Symbol.toStringTag` values.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the raw `toStringTag`.
 */
function getRawTag(value) {
  var isOwn = hasOwnProperty.call(value, symToStringTag),
      tag = value[symToStringTag];

  try {
    value[symToStringTag] = undefined;
    var unmasked = true;
  } catch (e) {}

  var result = nativeObjectToString.call(value);
  if (unmasked) {
    if (isOwn) {
      value[symToStringTag] = tag;
    } else {
      delete value[symToStringTag];
    }
  }
  return result;
}

/** Used for built-in method references. */
var objectProto$1 = Object.prototype;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var nativeObjectToString$1 = objectProto$1.toString;

/**
 * Converts `value` to a string using `Object.prototype.toString`.
 *
 * @private
 * @param {*} value The value to convert.
 * @returns {string} Returns the converted string.
 */
function objectToString(value) {
  return nativeObjectToString$1.call(value);
}

/** `Object#toString` result references. */
var nullTag = '[object Null]',
    undefinedTag = '[object Undefined]';

/** Built-in value references. */
var symToStringTag$1 = Symbol ? Symbol.toStringTag : undefined;

/**
 * The base implementation of `getTag` without fallbacks for buggy environments.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the `toStringTag`.
 */
function baseGetTag(value) {
  if (value == null) {
    return value === undefined ? undefinedTag : nullTag;
  }
  return (symToStringTag$1 && symToStringTag$1 in Object(value))
    ? getRawTag(value)
    : objectToString(value);
}

/**
 * Checks if `value` is object-like. A value is object-like if it's not `null`
 * and has a `typeof` result of "object".
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
 * @example
 *
 * _.isObjectLike({});
 * // => true
 *
 * _.isObjectLike([1, 2, 3]);
 * // => true
 *
 * _.isObjectLike(_.noop);
 * // => false
 *
 * _.isObjectLike(null);
 * // => false
 */
function isObjectLike(value) {
  return value != null && typeof value == 'object';
}

/** `Object#toString` result references. */
var symbolTag = '[object Symbol]';

/**
 * Checks if `value` is classified as a `Symbol` primitive or object.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a symbol, else `false`.
 * @example
 *
 * _.isSymbol(Symbol.iterator);
 * // => true
 *
 * _.isSymbol('abc');
 * // => false
 */
function isSymbol(value) {
  return typeof value == 'symbol' ||
    (isObjectLike(value) && baseGetTag(value) == symbolTag);
}

/**
 * A specialized version of `_.map` for arrays without support for iteratee
 * shorthands.
 *
 * @private
 * @param {Array} [array] The array to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array} Returns the new mapped array.
 */
function arrayMap(array, iteratee) {
  var index = -1,
      length = array == null ? 0 : array.length,
      result = Array(length);

  while (++index < length) {
    result[index] = iteratee(array[index], index, array);
  }
  return result;
}

/**
 * Checks if `value` is classified as an `Array` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an array, else `false`.
 * @example
 *
 * _.isArray([1, 2, 3]);
 * // => true
 *
 * _.isArray(document.body.children);
 * // => false
 *
 * _.isArray('abc');
 * // => false
 *
 * _.isArray(_.noop);
 * // => false
 */
var isArray = Array.isArray;

/** Used as references for various `Number` constants. */
var INFINITY = 1 / 0;

/** Used to convert symbols to primitives and strings. */
var symbolProto = Symbol ? Symbol.prototype : undefined,
    symbolToString = symbolProto ? symbolProto.toString : undefined;

/**
 * The base implementation of `_.toString` which doesn't convert nullish
 * values to empty strings.
 *
 * @private
 * @param {*} value The value to process.
 * @returns {string} Returns the string.
 */
function baseToString(value) {
  // Exit early for strings to avoid a performance hit in some environments.
  if (typeof value == 'string') {
    return value;
  }
  if (isArray(value)) {
    // Recursively convert values (susceptible to call stack limits).
    return arrayMap(value, baseToString) + '';
  }
  if (isSymbol(value)) {
    return symbolToString ? symbolToString.call(value) : '';
  }
  var result = (value + '');
  return (result == '0' && (1 / value) == -INFINITY) ? '-0' : result;
}

/**
 * Checks if `value` is the
 * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
 * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
 * @example
 *
 * _.isObject({});
 * // => true
 *
 * _.isObject([1, 2, 3]);
 * // => true
 *
 * _.isObject(_.noop);
 * // => true
 *
 * _.isObject(null);
 * // => false
 */
function isObject(value) {
  var type = typeof value;
  return value != null && (type == 'object' || type == 'function');
}

/**
 * This method returns the first argument it receives.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Util
 * @param {*} value Any value.
 * @returns {*} Returns `value`.
 * @example
 *
 * var object = { 'a': 1 };
 *
 * console.log(_.identity(object) === object);
 * // => true
 */
function identity(value) {
  return value;
}

/** `Object#toString` result references. */
var asyncTag = '[object AsyncFunction]',
    funcTag = '[object Function]',
    genTag = '[object GeneratorFunction]',
    proxyTag = '[object Proxy]';

/**
 * Checks if `value` is classified as a `Function` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a function, else `false`.
 * @example
 *
 * _.isFunction(_);
 * // => true
 *
 * _.isFunction(/abc/);
 * // => false
 */
function isFunction(value) {
  if (!isObject(value)) {
    return false;
  }
  // The use of `Object#toString` avoids issues with the `typeof` operator
  // in Safari 9 which returns 'object' for typed arrays and other constructors.
  var tag = baseGetTag(value);
  return tag == funcTag || tag == genTag || tag == asyncTag || tag == proxyTag;
}

/** Used to detect overreaching core-js shims. */
var coreJsData = root['__core-js_shared__'];

/** Used to detect methods masquerading as native. */
var maskSrcKey = (function() {
  var uid = /[^.]+$/.exec(coreJsData && coreJsData.keys && coreJsData.keys.IE_PROTO || '');
  return uid ? ('Symbol(src)_1.' + uid) : '';
}());

/**
 * Checks if `func` has its source masked.
 *
 * @private
 * @param {Function} func The function to check.
 * @returns {boolean} Returns `true` if `func` is masked, else `false`.
 */
function isMasked(func) {
  return !!maskSrcKey && (maskSrcKey in func);
}

/** Used for built-in method references. */
var funcProto = Function.prototype;

/** Used to resolve the decompiled source of functions. */
var funcToString = funcProto.toString;

/**
 * Converts `func` to its source code.
 *
 * @private
 * @param {Function} func The function to convert.
 * @returns {string} Returns the source code.
 */
function toSource(func) {
  if (func != null) {
    try {
      return funcToString.call(func);
    } catch (e) {}
    try {
      return (func + '');
    } catch (e) {}
  }
  return '';
}

/**
 * Used to match `RegExp`
 * [syntax characters](http://ecma-international.org/ecma-262/7.0/#sec-patterns).
 */
var reRegExpChar = /[\\^$.*+?()[\]{}|]/g;

/** Used to detect host constructors (Safari). */
var reIsHostCtor = /^\[object .+?Constructor\]$/;

/** Used for built-in method references. */
var funcProto$1 = Function.prototype,
    objectProto$2 = Object.prototype;

/** Used to resolve the decompiled source of functions. */
var funcToString$1 = funcProto$1.toString;

/** Used to check objects for own properties. */
var hasOwnProperty$1 = objectProto$2.hasOwnProperty;

/** Used to detect if a method is native. */
var reIsNative = RegExp('^' +
  funcToString$1.call(hasOwnProperty$1).replace(reRegExpChar, '\\$&')
  .replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, '$1.*?') + '$'
);

/**
 * The base implementation of `_.isNative` without bad shim checks.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a native function,
 *  else `false`.
 */
function baseIsNative(value) {
  if (!isObject(value) || isMasked(value)) {
    return false;
  }
  var pattern = isFunction(value) ? reIsNative : reIsHostCtor;
  return pattern.test(toSource(value));
}

/**
 * Gets the value at `key` of `object`.
 *
 * @private
 * @param {Object} [object] The object to query.
 * @param {string} key The key of the property to get.
 * @returns {*} Returns the property value.
 */
function getValue(object, key) {
  return object == null ? undefined : object[key];
}

/**
 * Gets the native function at `key` of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {string} key The key of the method to get.
 * @returns {*} Returns the function if it's native, else `undefined`.
 */
function getNative(object, key) {
  var value = getValue(object, key);
  return baseIsNative(value) ? value : undefined;
}

/* Built-in method references that are verified to be native. */
var WeakMap$1 = getNative(root, 'WeakMap');

/** Built-in value references. */
var objectCreate = Object.create;

/**
 * The base implementation of `_.create` without support for assigning
 * properties to the created object.
 *
 * @private
 * @param {Object} proto The object to inherit from.
 * @returns {Object} Returns the new object.
 */
var baseCreate = (function() {
  function object() {}
  return function(proto) {
    if (!isObject(proto)) {
      return {};
    }
    if (objectCreate) {
      return objectCreate(proto);
    }
    object.prototype = proto;
    var result = new object;
    object.prototype = undefined;
    return result;
  };
}());

/**
 * A faster alternative to `Function#apply`, this function invokes `func`
 * with the `this` binding of `thisArg` and the arguments of `args`.
 *
 * @private
 * @param {Function} func The function to invoke.
 * @param {*} thisArg The `this` binding of `func`.
 * @param {Array} args The arguments to invoke `func` with.
 * @returns {*} Returns the result of `func`.
 */
function apply(func, thisArg, args) {
  switch (args.length) {
    case 0: return func.call(thisArg);
    case 1: return func.call(thisArg, args[0]);
    case 2: return func.call(thisArg, args[0], args[1]);
    case 3: return func.call(thisArg, args[0], args[1], args[2]);
  }
  return func.apply(thisArg, args);
}

/**
 * Copies the values of `source` to `array`.
 *
 * @private
 * @param {Array} source The array to copy values from.
 * @param {Array} [array=[]] The array to copy values to.
 * @returns {Array} Returns `array`.
 */
function copyArray(source, array) {
  var index = -1,
      length = source.length;

  array || (array = Array(length));
  while (++index < length) {
    array[index] = source[index];
  }
  return array;
}

/** Used to detect hot functions by number of calls within a span of milliseconds. */
var HOT_COUNT = 800,
    HOT_SPAN = 16;

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeNow = Date.now;

/**
 * Creates a function that'll short out and invoke `identity` instead
 * of `func` when it's called `HOT_COUNT` or more times in `HOT_SPAN`
 * milliseconds.
 *
 * @private
 * @param {Function} func The function to restrict.
 * @returns {Function} Returns the new shortable function.
 */
function shortOut(func) {
  var count = 0,
      lastCalled = 0;

  return function() {
    var stamp = nativeNow(),
        remaining = HOT_SPAN - (stamp - lastCalled);

    lastCalled = stamp;
    if (remaining > 0) {
      if (++count >= HOT_COUNT) {
        return arguments[0];
      }
    } else {
      count = 0;
    }
    return func.apply(undefined, arguments);
  };
}

/**
 * Creates a function that returns `value`.
 *
 * @static
 * @memberOf _
 * @since 2.4.0
 * @category Util
 * @param {*} value The value to return from the new function.
 * @returns {Function} Returns the new constant function.
 * @example
 *
 * var objects = _.times(2, _.constant({ 'a': 1 }));
 *
 * console.log(objects);
 * // => [{ 'a': 1 }, { 'a': 1 }]
 *
 * console.log(objects[0] === objects[1]);
 * // => true
 */
function constant(value) {
  return function() {
    return value;
  };
}

var defineProperty = (function() {
  try {
    var func = getNative(Object, 'defineProperty');
    func({}, '', {});
    return func;
  } catch (e) {}
}());

/**
 * The base implementation of `setToString` without support for hot loop shorting.
 *
 * @private
 * @param {Function} func The function to modify.
 * @param {Function} string The `toString` result.
 * @returns {Function} Returns `func`.
 */
var baseSetToString = !defineProperty ? identity : function(func, string) {
  return defineProperty(func, 'toString', {
    'configurable': true,
    'enumerable': false,
    'value': constant(string),
    'writable': true
  });
};

/**
 * Sets the `toString` method of `func` to return `string`.
 *
 * @private
 * @param {Function} func The function to modify.
 * @param {Function} string The `toString` result.
 * @returns {Function} Returns `func`.
 */
var setToString = shortOut(baseSetToString);

/**
 * The base implementation of `_.findIndex` and `_.findLastIndex` without
 * support for iteratee shorthands.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {Function} predicate The function invoked per iteration.
 * @param {number} fromIndex The index to search from.
 * @param {boolean} [fromRight] Specify iterating from right to left.
 * @returns {number} Returns the index of the matched value, else `-1`.
 */
function baseFindIndex(array, predicate, fromIndex, fromRight) {
  var length = array.length,
      index = fromIndex + (fromRight ? 1 : -1);

  while ((fromRight ? index-- : ++index < length)) {
    if (predicate(array[index], index, array)) {
      return index;
    }
  }
  return -1;
}

/**
 * The base implementation of `_.isNaN` without support for number objects.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is `NaN`, else `false`.
 */
function baseIsNaN(value) {
  return value !== value;
}

/**
 * A specialized version of `_.indexOf` which performs strict equality
 * comparisons of values, i.e. `===`.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {*} value The value to search for.
 * @param {number} fromIndex The index to search from.
 * @returns {number} Returns the index of the matched value, else `-1`.
 */
function strictIndexOf(array, value, fromIndex) {
  var index = fromIndex - 1,
      length = array.length;

  while (++index < length) {
    if (array[index] === value) {
      return index;
    }
  }
  return -1;
}

/**
 * The base implementation of `_.indexOf` without `fromIndex` bounds checks.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {*} value The value to search for.
 * @param {number} fromIndex The index to search from.
 * @returns {number} Returns the index of the matched value, else `-1`.
 */
function baseIndexOf(array, value, fromIndex) {
  return value === value
    ? strictIndexOf(array, value, fromIndex)
    : baseFindIndex(array, baseIsNaN, fromIndex);
}

/** Used as references for various `Number` constants. */
var MAX_SAFE_INTEGER = 9007199254740991;

/** Used to detect unsigned integer values. */
var reIsUint = /^(?:0|[1-9]\d*)$/;

/**
 * Checks if `value` is a valid array-like index.
 *
 * @private
 * @param {*} value The value to check.
 * @param {number} [length=MAX_SAFE_INTEGER] The upper bounds of a valid index.
 * @returns {boolean} Returns `true` if `value` is a valid index, else `false`.
 */
function isIndex(value, length) {
  var type = typeof value;
  length = length == null ? MAX_SAFE_INTEGER : length;

  return !!length &&
    (type == 'number' ||
      (type != 'symbol' && reIsUint.test(value))) &&
        (value > -1 && value % 1 == 0 && value < length);
}

/**
 * The base implementation of `assignValue` and `assignMergeValue` without
 * value checks.
 *
 * @private
 * @param {Object} object The object to modify.
 * @param {string} key The key of the property to assign.
 * @param {*} value The value to assign.
 */
function baseAssignValue(object, key, value) {
  if (key == '__proto__' && defineProperty) {
    defineProperty(object, key, {
      'configurable': true,
      'enumerable': true,
      'value': value,
      'writable': true
    });
  } else {
    object[key] = value;
  }
}

/**
 * Performs a
 * [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
 * comparison between two values to determine if they are equivalent.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to compare.
 * @param {*} other The other value to compare.
 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
 * @example
 *
 * var object = { 'a': 1 };
 * var other = { 'a': 1 };
 *
 * _.eq(object, object);
 * // => true
 *
 * _.eq(object, other);
 * // => false
 *
 * _.eq('a', 'a');
 * // => true
 *
 * _.eq('a', Object('a'));
 * // => false
 *
 * _.eq(NaN, NaN);
 * // => true
 */
function eq(value, other) {
  return value === other || (value !== value && other !== other);
}

/** Used for built-in method references. */
var objectProto$3 = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty$2 = objectProto$3.hasOwnProperty;

/**
 * Assigns `value` to `key` of `object` if the existing value is not equivalent
 * using [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
 * for equality comparisons.
 *
 * @private
 * @param {Object} object The object to modify.
 * @param {string} key The key of the property to assign.
 * @param {*} value The value to assign.
 */
function assignValue(object, key, value) {
  var objValue = object[key];
  if (!(hasOwnProperty$2.call(object, key) && eq(objValue, value)) ||
      (value === undefined && !(key in object))) {
    baseAssignValue(object, key, value);
  }
}

/**
 * Copies properties of `source` to `object`.
 *
 * @private
 * @param {Object} source The object to copy properties from.
 * @param {Array} props The property identifiers to copy.
 * @param {Object} [object={}] The object to copy properties to.
 * @param {Function} [customizer] The function to customize copied values.
 * @returns {Object} Returns `object`.
 */
function copyObject(source, props, object, customizer) {
  var isNew = !object;
  object || (object = {});

  var index = -1,
      length = props.length;

  while (++index < length) {
    var key = props[index];

    var newValue = customizer
      ? customizer(object[key], source[key], key, object, source)
      : undefined;

    if (newValue === undefined) {
      newValue = source[key];
    }
    if (isNew) {
      baseAssignValue(object, key, newValue);
    } else {
      assignValue(object, key, newValue);
    }
  }
  return object;
}

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeMax = Math.max;

/**
 * A specialized version of `baseRest` which transforms the rest array.
 *
 * @private
 * @param {Function} func The function to apply a rest parameter to.
 * @param {number} [start=func.length-1] The start position of the rest parameter.
 * @param {Function} transform The rest array transform.
 * @returns {Function} Returns the new function.
 */
function overRest(func, start, transform) {
  start = nativeMax(start === undefined ? (func.length - 1) : start, 0);
  return function() {
    var args = arguments,
        index = -1,
        length = nativeMax(args.length - start, 0),
        array = Array(length);

    while (++index < length) {
      array[index] = args[start + index];
    }
    index = -1;
    var otherArgs = Array(start + 1);
    while (++index < start) {
      otherArgs[index] = args[index];
    }
    otherArgs[start] = transform(array);
    return apply(func, this, otherArgs);
  };
}

/**
 * The base implementation of `_.rest` which doesn't validate or coerce arguments.
 *
 * @private
 * @param {Function} func The function to apply a rest parameter to.
 * @param {number} [start=func.length-1] The start position of the rest parameter.
 * @returns {Function} Returns the new function.
 */
function baseRest(func, start) {
  return setToString(overRest(func, start, identity), func + '');
}

/** Used as references for various `Number` constants. */
var MAX_SAFE_INTEGER$1 = 9007199254740991;

/**
 * Checks if `value` is a valid array-like length.
 *
 * **Note:** This method is loosely based on
 * [`ToLength`](http://ecma-international.org/ecma-262/7.0/#sec-tolength).
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a valid length, else `false`.
 * @example
 *
 * _.isLength(3);
 * // => true
 *
 * _.isLength(Number.MIN_VALUE);
 * // => false
 *
 * _.isLength(Infinity);
 * // => false
 *
 * _.isLength('3');
 * // => false
 */
function isLength(value) {
  return typeof value == 'number' &&
    value > -1 && value % 1 == 0 && value <= MAX_SAFE_INTEGER$1;
}

/**
 * Checks if `value` is array-like. A value is considered array-like if it's
 * not a function and has a `value.length` that's an integer greater than or
 * equal to `0` and less than or equal to `Number.MAX_SAFE_INTEGER`.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is array-like, else `false`.
 * @example
 *
 * _.isArrayLike([1, 2, 3]);
 * // => true
 *
 * _.isArrayLike(document.body.children);
 * // => true
 *
 * _.isArrayLike('abc');
 * // => true
 *
 * _.isArrayLike(_.noop);
 * // => false
 */
function isArrayLike(value) {
  return value != null && isLength(value.length) && !isFunction(value);
}

/**
 * Checks if the given arguments are from an iteratee call.
 *
 * @private
 * @param {*} value The potential iteratee value argument.
 * @param {*} index The potential iteratee index or key argument.
 * @param {*} object The potential iteratee object argument.
 * @returns {boolean} Returns `true` if the arguments are from an iteratee call,
 *  else `false`.
 */
function isIterateeCall(value, index, object) {
  if (!isObject(object)) {
    return false;
  }
  var type = typeof index;
  if (type == 'number'
        ? (isArrayLike(object) && isIndex(index, object.length))
        : (type == 'string' && index in object)
      ) {
    return eq(object[index], value);
  }
  return false;
}

/**
 * Creates a function like `_.assign`.
 *
 * @private
 * @param {Function} assigner The function to assign values.
 * @returns {Function} Returns the new assigner function.
 */
function createAssigner(assigner) {
  return baseRest(function(object, sources) {
    var index = -1,
        length = sources.length,
        customizer = length > 1 ? sources[length - 1] : undefined,
        guard = length > 2 ? sources[2] : undefined;

    customizer = (assigner.length > 3 && typeof customizer == 'function')
      ? (length--, customizer)
      : undefined;

    if (guard && isIterateeCall(sources[0], sources[1], guard)) {
      customizer = length < 3 ? undefined : customizer;
      length = 1;
    }
    object = Object(object);
    while (++index < length) {
      var source = sources[index];
      if (source) {
        assigner(object, source, index, customizer);
      }
    }
    return object;
  });
}

/** Used for built-in method references. */
var objectProto$4 = Object.prototype;

/**
 * Checks if `value` is likely a prototype object.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a prototype, else `false`.
 */
function isPrototype(value) {
  var Ctor = value && value.constructor,
      proto = (typeof Ctor == 'function' && Ctor.prototype) || objectProto$4;

  return value === proto;
}

/**
 * The base implementation of `_.times` without support for iteratee shorthands
 * or max array length checks.
 *
 * @private
 * @param {number} n The number of times to invoke `iteratee`.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array} Returns the array of results.
 */
function baseTimes(n, iteratee) {
  var index = -1,
      result = Array(n);

  while (++index < n) {
    result[index] = iteratee(index);
  }
  return result;
}

/** `Object#toString` result references. */
var argsTag = '[object Arguments]';

/**
 * The base implementation of `_.isArguments`.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an `arguments` object,
 */
function baseIsArguments(value) {
  return isObjectLike(value) && baseGetTag(value) == argsTag;
}

/** Used for built-in method references. */
var objectProto$5 = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty$3 = objectProto$5.hasOwnProperty;

/** Built-in value references. */
var propertyIsEnumerable = objectProto$5.propertyIsEnumerable;

/**
 * Checks if `value` is likely an `arguments` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an `arguments` object,
 *  else `false`.
 * @example
 *
 * _.isArguments(function() { return arguments; }());
 * // => true
 *
 * _.isArguments([1, 2, 3]);
 * // => false
 */
var isArguments = baseIsArguments(function() { return arguments; }()) ? baseIsArguments : function(value) {
  return isObjectLike(value) && hasOwnProperty$3.call(value, 'callee') &&
    !propertyIsEnumerable.call(value, 'callee');
};

/**
 * This method returns `false`.
 *
 * @static
 * @memberOf _
 * @since 4.13.0
 * @category Util
 * @returns {boolean} Returns `false`.
 * @example
 *
 * _.times(2, _.stubFalse);
 * // => [false, false]
 */
function stubFalse() {
  return false;
}

/** Detect free variable `exports`. */
var freeExports = typeof exports == 'object' && exports && !exports.nodeType && exports;

/** Detect free variable `module`. */
var freeModule = freeExports && typeof module == 'object' && module && !module.nodeType && module;

/** Detect the popular CommonJS extension `module.exports`. */
var moduleExports = freeModule && freeModule.exports === freeExports;

/** Built-in value references. */
var Buffer = moduleExports ? root.Buffer : undefined;

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeIsBuffer = Buffer ? Buffer.isBuffer : undefined;

/**
 * Checks if `value` is a buffer.
 *
 * @static
 * @memberOf _
 * @since 4.3.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a buffer, else `false`.
 * @example
 *
 * _.isBuffer(new Buffer(2));
 * // => true
 *
 * _.isBuffer(new Uint8Array(2));
 * // => false
 */
var isBuffer = nativeIsBuffer || stubFalse;

/** `Object#toString` result references. */
var argsTag$1 = '[object Arguments]',
    arrayTag = '[object Array]',
    boolTag = '[object Boolean]',
    dateTag = '[object Date]',
    errorTag = '[object Error]',
    funcTag$1 = '[object Function]',
    mapTag = '[object Map]',
    numberTag = '[object Number]',
    objectTag = '[object Object]',
    regexpTag = '[object RegExp]',
    setTag = '[object Set]',
    stringTag = '[object String]',
    weakMapTag = '[object WeakMap]';

var arrayBufferTag = '[object ArrayBuffer]',
    dataViewTag = '[object DataView]',
    float32Tag = '[object Float32Array]',
    float64Tag = '[object Float64Array]',
    int8Tag = '[object Int8Array]',
    int16Tag = '[object Int16Array]',
    int32Tag = '[object Int32Array]',
    uint8Tag = '[object Uint8Array]',
    uint8ClampedTag = '[object Uint8ClampedArray]',
    uint16Tag = '[object Uint16Array]',
    uint32Tag = '[object Uint32Array]';

/** Used to identify `toStringTag` values of typed arrays. */
var typedArrayTags = {};
typedArrayTags[float32Tag] = typedArrayTags[float64Tag] =
typedArrayTags[int8Tag] = typedArrayTags[int16Tag] =
typedArrayTags[int32Tag] = typedArrayTags[uint8Tag] =
typedArrayTags[uint8ClampedTag] = typedArrayTags[uint16Tag] =
typedArrayTags[uint32Tag] = true;
typedArrayTags[argsTag$1] = typedArrayTags[arrayTag] =
typedArrayTags[arrayBufferTag] = typedArrayTags[boolTag] =
typedArrayTags[dataViewTag] = typedArrayTags[dateTag] =
typedArrayTags[errorTag] = typedArrayTags[funcTag$1] =
typedArrayTags[mapTag] = typedArrayTags[numberTag] =
typedArrayTags[objectTag] = typedArrayTags[regexpTag] =
typedArrayTags[setTag] = typedArrayTags[stringTag] =
typedArrayTags[weakMapTag] = false;

/**
 * The base implementation of `_.isTypedArray` without Node.js optimizations.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
 */
function baseIsTypedArray(value) {
  return isObjectLike(value) &&
    isLength(value.length) && !!typedArrayTags[baseGetTag(value)];
}

/**
 * The base implementation of `_.unary` without support for storing metadata.
 *
 * @private
 * @param {Function} func The function to cap arguments for.
 * @returns {Function} Returns the new capped function.
 */
function baseUnary(func) {
  return function(value) {
    return func(value);
  };
}

/** Detect free variable `exports`. */
var freeExports$1 = typeof exports == 'object' && exports && !exports.nodeType && exports;

/** Detect free variable `module`. */
var freeModule$1 = freeExports$1 && typeof module == 'object' && module && !module.nodeType && module;

/** Detect the popular CommonJS extension `module.exports`. */
var moduleExports$1 = freeModule$1 && freeModule$1.exports === freeExports$1;

/** Detect free variable `process` from Node.js. */
var freeProcess = moduleExports$1 && freeGlobal.process;

/** Used to access faster Node.js helpers. */
var nodeUtil = (function() {
  try {
    // Use `util.types` for Node.js 10+.
    var types = freeModule$1 && freeModule$1.require && freeModule$1.require('util').types;

    if (types) {
      return types;
    }

    // Legacy `process.binding('util')` for Node.js < 10.
    return freeProcess && freeProcess.binding && freeProcess.binding('util');
  } catch (e) {}
}());

/* Node.js helper references. */
var nodeIsTypedArray = nodeUtil && nodeUtil.isTypedArray;

/**
 * Checks if `value` is classified as a typed array.
 *
 * @static
 * @memberOf _
 * @since 3.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
 * @example
 *
 * _.isTypedArray(new Uint8Array);
 * // => true
 *
 * _.isTypedArray([]);
 * // => false
 */
var isTypedArray = nodeIsTypedArray ? baseUnary(nodeIsTypedArray) : baseIsTypedArray;

/** Used for built-in method references. */
var objectProto$6 = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty$4 = objectProto$6.hasOwnProperty;

/**
 * Creates an array of the enumerable property names of the array-like `value`.
 *
 * @private
 * @param {*} value The value to query.
 * @param {boolean} inherited Specify returning inherited property names.
 * @returns {Array} Returns the array of property names.
 */
function arrayLikeKeys(value, inherited) {
  var isArr = isArray(value),
      isArg = !isArr && isArguments(value),
      isBuff = !isArr && !isArg && isBuffer(value),
      isType = !isArr && !isArg && !isBuff && isTypedArray(value),
      skipIndexes = isArr || isArg || isBuff || isType,
      result = skipIndexes ? baseTimes(value.length, String) : [],
      length = result.length;

  for (var key in value) {
    if ((inherited || hasOwnProperty$4.call(value, key)) &&
        !(skipIndexes && (
           // Safari 9 has enumerable `arguments.length` in strict mode.
           key == 'length' ||
           // Node.js 0.10 has enumerable non-index properties on buffers.
           (isBuff && (key == 'offset' || key == 'parent')) ||
           // PhantomJS 2 has enumerable non-index properties on typed arrays.
           (isType && (key == 'buffer' || key == 'byteLength' || key == 'byteOffset')) ||
           // Skip index properties.
           isIndex(key, length)
        ))) {
      result.push(key);
    }
  }
  return result;
}

/**
 * Creates a unary function that invokes `func` with its argument transformed.
 *
 * @private
 * @param {Function} func The function to wrap.
 * @param {Function} transform The argument transform.
 * @returns {Function} Returns the new function.
 */
function overArg(func, transform) {
  return function(arg) {
    return func(transform(arg));
  };
}

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeKeys = overArg(Object.keys, Object);

/** Used for built-in method references. */
var objectProto$7 = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty$5 = objectProto$7.hasOwnProperty;

/**
 * The base implementation of `_.keys` which doesn't treat sparse arrays as dense.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 */
function baseKeys(object) {
  if (!isPrototype(object)) {
    return nativeKeys(object);
  }
  var result = [];
  for (var key in Object(object)) {
    if (hasOwnProperty$5.call(object, key) && key != 'constructor') {
      result.push(key);
    }
  }
  return result;
}

/**
 * Creates an array of the own enumerable property names of `object`.
 *
 * **Note:** Non-object values are coerced to objects. See the
 * [ES spec](http://ecma-international.org/ecma-262/7.0/#sec-object.keys)
 * for more details.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Object
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.keys(new Foo);
 * // => ['a', 'b'] (iteration order is not guaranteed)
 *
 * _.keys('hi');
 * // => ['0', '1']
 */
function keys(object) {
  return isArrayLike(object) ? arrayLikeKeys(object) : baseKeys(object);
}

/**
 * This function is like
 * [`Object.keys`](http://ecma-international.org/ecma-262/7.0/#sec-object.keys)
 * except that it includes inherited enumerable properties.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 */
function nativeKeysIn(object) {
  var result = [];
  if (object != null) {
    for (var key in Object(object)) {
      result.push(key);
    }
  }
  return result;
}

/** Used for built-in method references. */
var objectProto$8 = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty$6 = objectProto$8.hasOwnProperty;

/**
 * The base implementation of `_.keysIn` which doesn't treat sparse arrays as dense.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 */
function baseKeysIn(object) {
  if (!isObject(object)) {
    return nativeKeysIn(object);
  }
  var isProto = isPrototype(object),
      result = [];

  for (var key in object) {
    if (!(key == 'constructor' && (isProto || !hasOwnProperty$6.call(object, key)))) {
      result.push(key);
    }
  }
  return result;
}

/**
 * Creates an array of the own and inherited enumerable property names of `object`.
 *
 * **Note:** Non-object values are coerced to objects.
 *
 * @static
 * @memberOf _
 * @since 3.0.0
 * @category Object
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.keysIn(new Foo);
 * // => ['a', 'b', 'c'] (iteration order is not guaranteed)
 */
function keysIn(object) {
  return isArrayLike(object) ? arrayLikeKeys(object, true) : baseKeysIn(object);
}

/** Used to match property names within property paths. */
var reIsDeepProp = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/,
    reIsPlainProp = /^\w*$/;

/**
 * Checks if `value` is a property name and not a property path.
 *
 * @private
 * @param {*} value The value to check.
 * @param {Object} [object] The object to query keys on.
 * @returns {boolean} Returns `true` if `value` is a property name, else `false`.
 */
function isKey(value, object) {
  if (isArray(value)) {
    return false;
  }
  var type = typeof value;
  if (type == 'number' || type == 'symbol' || type == 'boolean' ||
      value == null || isSymbol(value)) {
    return true;
  }
  return reIsPlainProp.test(value) || !reIsDeepProp.test(value) ||
    (object != null && value in Object(object));
}

/* Built-in method references that are verified to be native. */
var nativeCreate = getNative(Object, 'create');

/**
 * Removes all key-value entries from the hash.
 *
 * @private
 * @name clear
 * @memberOf Hash
 */
function hashClear() {
  this.__data__ = nativeCreate ? nativeCreate(null) : {};
  this.size = 0;
}

/**
 * Removes `key` and its value from the hash.
 *
 * @private
 * @name delete
 * @memberOf Hash
 * @param {Object} hash The hash to modify.
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function hashDelete(key) {
  var result = this.has(key) && delete this.__data__[key];
  this.size -= result ? 1 : 0;
  return result;
}

/** Used to stand-in for `undefined` hash values. */
var HASH_UNDEFINED = '__lodash_hash_undefined__';

/** Used for built-in method references. */
var objectProto$9 = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty$7 = objectProto$9.hasOwnProperty;

/**
 * Gets the hash value for `key`.
 *
 * @private
 * @name get
 * @memberOf Hash
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */
function hashGet(key) {
  var data = this.__data__;
  if (nativeCreate) {
    var result = data[key];
    return result === HASH_UNDEFINED ? undefined : result;
  }
  return hasOwnProperty$7.call(data, key) ? data[key] : undefined;
}

/** Used for built-in method references. */
var objectProto$a = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty$8 = objectProto$a.hasOwnProperty;

/**
 * Checks if a hash value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf Hash
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function hashHas(key) {
  var data = this.__data__;
  return nativeCreate ? (data[key] !== undefined) : hasOwnProperty$8.call(data, key);
}

/** Used to stand-in for `undefined` hash values. */
var HASH_UNDEFINED$1 = '__lodash_hash_undefined__';

/**
 * Sets the hash `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf Hash
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the hash instance.
 */
function hashSet(key, value) {
  var data = this.__data__;
  this.size += this.has(key) ? 0 : 1;
  data[key] = (nativeCreate && value === undefined) ? HASH_UNDEFINED$1 : value;
  return this;
}

/**
 * Creates a hash object.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */
function Hash(entries) {
  var index = -1,
      length = entries == null ? 0 : entries.length;

  this.clear();
  while (++index < length) {
    var entry = entries[index];
    this.set(entry[0], entry[1]);
  }
}

// Add methods to `Hash`.
Hash.prototype.clear = hashClear;
Hash.prototype['delete'] = hashDelete;
Hash.prototype.get = hashGet;
Hash.prototype.has = hashHas;
Hash.prototype.set = hashSet;

/**
 * Removes all key-value entries from the list cache.
 *
 * @private
 * @name clear
 * @memberOf ListCache
 */
function listCacheClear() {
  this.__data__ = [];
  this.size = 0;
}

/**
 * Gets the index at which the `key` is found in `array` of key-value pairs.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {*} key The key to search for.
 * @returns {number} Returns the index of the matched value, else `-1`.
 */
function assocIndexOf(array, key) {
  var length = array.length;
  while (length--) {
    if (eq(array[length][0], key)) {
      return length;
    }
  }
  return -1;
}

/** Used for built-in method references. */
var arrayProto = Array.prototype;

/** Built-in value references. */
var splice = arrayProto.splice;

/**
 * Removes `key` and its value from the list cache.
 *
 * @private
 * @name delete
 * @memberOf ListCache
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function listCacheDelete(key) {
  var data = this.__data__,
      index = assocIndexOf(data, key);

  if (index < 0) {
    return false;
  }
  var lastIndex = data.length - 1;
  if (index == lastIndex) {
    data.pop();
  } else {
    splice.call(data, index, 1);
  }
  --this.size;
  return true;
}

/**
 * Gets the list cache value for `key`.
 *
 * @private
 * @name get
 * @memberOf ListCache
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */
function listCacheGet(key) {
  var data = this.__data__,
      index = assocIndexOf(data, key);

  return index < 0 ? undefined : data[index][1];
}

/**
 * Checks if a list cache value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf ListCache
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function listCacheHas(key) {
  return assocIndexOf(this.__data__, key) > -1;
}

/**
 * Sets the list cache `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf ListCache
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the list cache instance.
 */
function listCacheSet(key, value) {
  var data = this.__data__,
      index = assocIndexOf(data, key);

  if (index < 0) {
    ++this.size;
    data.push([key, value]);
  } else {
    data[index][1] = value;
  }
  return this;
}

/**
 * Creates an list cache object.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */
function ListCache(entries) {
  var index = -1,
      length = entries == null ? 0 : entries.length;

  this.clear();
  while (++index < length) {
    var entry = entries[index];
    this.set(entry[0], entry[1]);
  }
}

// Add methods to `ListCache`.
ListCache.prototype.clear = listCacheClear;
ListCache.prototype['delete'] = listCacheDelete;
ListCache.prototype.get = listCacheGet;
ListCache.prototype.has = listCacheHas;
ListCache.prototype.set = listCacheSet;

/* Built-in method references that are verified to be native. */
var Map$1 = getNative(root, 'Map');

/**
 * Removes all key-value entries from the map.
 *
 * @private
 * @name clear
 * @memberOf MapCache
 */
function mapCacheClear() {
  this.size = 0;
  this.__data__ = {
    'hash': new Hash,
    'map': new (Map$1 || ListCache),
    'string': new Hash
  };
}

/**
 * Checks if `value` is suitable for use as unique object key.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is suitable, else `false`.
 */
function isKeyable(value) {
  var type = typeof value;
  return (type == 'string' || type == 'number' || type == 'symbol' || type == 'boolean')
    ? (value !== '__proto__')
    : (value === null);
}

/**
 * Gets the data for `map`.
 *
 * @private
 * @param {Object} map The map to query.
 * @param {string} key The reference key.
 * @returns {*} Returns the map data.
 */
function getMapData(map, key) {
  var data = map.__data__;
  return isKeyable(key)
    ? data[typeof key == 'string' ? 'string' : 'hash']
    : data.map;
}

/**
 * Removes `key` and its value from the map.
 *
 * @private
 * @name delete
 * @memberOf MapCache
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function mapCacheDelete(key) {
  var result = getMapData(this, key)['delete'](key);
  this.size -= result ? 1 : 0;
  return result;
}

/**
 * Gets the map value for `key`.
 *
 * @private
 * @name get
 * @memberOf MapCache
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */
function mapCacheGet(key) {
  return getMapData(this, key).get(key);
}

/**
 * Checks if a map value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf MapCache
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function mapCacheHas(key) {
  return getMapData(this, key).has(key);
}

/**
 * Sets the map `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf MapCache
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the map cache instance.
 */
function mapCacheSet(key, value) {
  var data = getMapData(this, key),
      size = data.size;

  data.set(key, value);
  this.size += data.size == size ? 0 : 1;
  return this;
}

/**
 * Creates a map cache object to store key-value pairs.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */
function MapCache(entries) {
  var index = -1,
      length = entries == null ? 0 : entries.length;

  this.clear();
  while (++index < length) {
    var entry = entries[index];
    this.set(entry[0], entry[1]);
  }
}

// Add methods to `MapCache`.
MapCache.prototype.clear = mapCacheClear;
MapCache.prototype['delete'] = mapCacheDelete;
MapCache.prototype.get = mapCacheGet;
MapCache.prototype.has = mapCacheHas;
MapCache.prototype.set = mapCacheSet;

/** Error message constants. */
var FUNC_ERROR_TEXT = 'Expected a function';

/**
 * Creates a function that memoizes the result of `func`. If `resolver` is
 * provided, it determines the cache key for storing the result based on the
 * arguments provided to the memoized function. By default, the first argument
 * provided to the memoized function is used as the map cache key. The `func`
 * is invoked with the `this` binding of the memoized function.
 *
 * **Note:** The cache is exposed as the `cache` property on the memoized
 * function. Its creation may be customized by replacing the `_.memoize.Cache`
 * constructor with one whose instances implement the
 * [`Map`](http://ecma-international.org/ecma-262/7.0/#sec-properties-of-the-map-prototype-object)
 * method interface of `clear`, `delete`, `get`, `has`, and `set`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Function
 * @param {Function} func The function to have its output memoized.
 * @param {Function} [resolver] The function to resolve the cache key.
 * @returns {Function} Returns the new memoized function.
 * @example
 *
 * var object = { 'a': 1, 'b': 2 };
 * var other = { 'c': 3, 'd': 4 };
 *
 * var values = _.memoize(_.values);
 * values(object);
 * // => [1, 2]
 *
 * values(other);
 * // => [3, 4]
 *
 * object.a = 2;
 * values(object);
 * // => [1, 2]
 *
 * // Modify the result cache.
 * values.cache.set(object, ['a', 'b']);
 * values(object);
 * // => ['a', 'b']
 *
 * // Replace `_.memoize.Cache`.
 * _.memoize.Cache = WeakMap;
 */
function memoize(func, resolver) {
  if (typeof func != 'function' || (resolver != null && typeof resolver != 'function')) {
    throw new TypeError(FUNC_ERROR_TEXT);
  }
  var memoized = function() {
    var args = arguments,
        key = resolver ? resolver.apply(this, args) : args[0],
        cache = memoized.cache;

    if (cache.has(key)) {
      return cache.get(key);
    }
    var result = func.apply(this, args);
    memoized.cache = cache.set(key, result) || cache;
    return result;
  };
  memoized.cache = new (memoize.Cache || MapCache);
  return memoized;
}

// Expose `MapCache`.
memoize.Cache = MapCache;

/** Used as the maximum memoize cache size. */
var MAX_MEMOIZE_SIZE = 500;

/**
 * A specialized version of `_.memoize` which clears the memoized function's
 * cache when it exceeds `MAX_MEMOIZE_SIZE`.
 *
 * @private
 * @param {Function} func The function to have its output memoized.
 * @returns {Function} Returns the new memoized function.
 */
function memoizeCapped(func) {
  var result = memoize(func, function(key) {
    if (cache.size === MAX_MEMOIZE_SIZE) {
      cache.clear();
    }
    return key;
  });

  var cache = result.cache;
  return result;
}

/** Used to match property names within property paths. */
var rePropName = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|$))/g;

/** Used to match backslashes in property paths. */
var reEscapeChar = /\\(\\)?/g;

/**
 * Converts `string` to a property path array.
 *
 * @private
 * @param {string} string The string to convert.
 * @returns {Array} Returns the property path array.
 */
var stringToPath = memoizeCapped(function(string) {
  var result = [];
  if (string.charCodeAt(0) === 46 /* . */) {
    result.push('');
  }
  string.replace(rePropName, function(match, number, quote, subString) {
    result.push(quote ? subString.replace(reEscapeChar, '$1') : (number || match));
  });
  return result;
});

/**
 * Converts `value` to a string. An empty string is returned for `null`
 * and `undefined` values. The sign of `-0` is preserved.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to convert.
 * @returns {string} Returns the converted string.
 * @example
 *
 * _.toString(null);
 * // => ''
 *
 * _.toString(-0);
 * // => '-0'
 *
 * _.toString([1, 2, 3]);
 * // => '1,2,3'
 */
function toString(value) {
  return value == null ? '' : baseToString(value);
}

/**
 * Casts `value` to a path array if it's not one.
 *
 * @private
 * @param {*} value The value to inspect.
 * @param {Object} [object] The object to query keys on.
 * @returns {Array} Returns the cast property path array.
 */
function castPath(value, object) {
  if (isArray(value)) {
    return value;
  }
  return isKey(value, object) ? [value] : stringToPath(toString(value));
}

/** Used as references for various `Number` constants. */
var INFINITY$1 = 1 / 0;

/**
 * Converts `value` to a string key if it's not a string or symbol.
 *
 * @private
 * @param {*} value The value to inspect.
 * @returns {string|symbol} Returns the key.
 */
function toKey(value) {
  if (typeof value == 'string' || isSymbol(value)) {
    return value;
  }
  var result = (value + '');
  return (result == '0' && (1 / value) == -INFINITY$1) ? '-0' : result;
}

/**
 * The base implementation of `_.get` without support for default values.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {Array|string} path The path of the property to get.
 * @returns {*} Returns the resolved value.
 */
function baseGet(object, path) {
  path = castPath(path, object);

  var index = 0,
      length = path.length;

  while (object != null && index < length) {
    object = object[toKey(path[index++])];
  }
  return (index && index == length) ? object : undefined;
}

/**
 * Gets the value at `path` of `object`. If the resolved value is
 * `undefined`, the `defaultValue` is returned in its place.
 *
 * @static
 * @memberOf _
 * @since 3.7.0
 * @category Object
 * @param {Object} object The object to query.
 * @param {Array|string} path The path of the property to get.
 * @param {*} [defaultValue] The value returned for `undefined` resolved values.
 * @returns {*} Returns the resolved value.
 * @example
 *
 * var object = { 'a': [{ 'b': { 'c': 3 } }] };
 *
 * _.get(object, 'a[0].b.c');
 * // => 3
 *
 * _.get(object, ['a', '0', 'b', 'c']);
 * // => 3
 *
 * _.get(object, 'a.b.c', 'default');
 * // => 'default'
 */
function get(object, path, defaultValue) {
  var result = object == null ? undefined : baseGet(object, path);
  return result === undefined ? defaultValue : result;
}

/**
 * Appends the elements of `values` to `array`.
 *
 * @private
 * @param {Array} array The array to modify.
 * @param {Array} values The values to append.
 * @returns {Array} Returns `array`.
 */
function arrayPush(array, values) {
  var index = -1,
      length = values.length,
      offset = array.length;

  while (++index < length) {
    array[offset + index] = values[index];
  }
  return array;
}

/** Built-in value references. */
var getPrototype = overArg(Object.getPrototypeOf, Object);

/** `Object#toString` result references. */
var objectTag$1 = '[object Object]';

/** Used for built-in method references. */
var funcProto$2 = Function.prototype,
    objectProto$b = Object.prototype;

/** Used to resolve the decompiled source of functions. */
var funcToString$2 = funcProto$2.toString;

/** Used to check objects for own properties. */
var hasOwnProperty$9 = objectProto$b.hasOwnProperty;

/** Used to infer the `Object` constructor. */
var objectCtorString = funcToString$2.call(Object);

/**
 * Checks if `value` is a plain object, that is, an object created by the
 * `Object` constructor or one with a `[[Prototype]]` of `null`.
 *
 * @static
 * @memberOf _
 * @since 0.8.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a plain object, else `false`.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 * }
 *
 * _.isPlainObject(new Foo);
 * // => false
 *
 * _.isPlainObject([1, 2, 3]);
 * // => false
 *
 * _.isPlainObject({ 'x': 0, 'y': 0 });
 * // => true
 *
 * _.isPlainObject(Object.create(null));
 * // => true
 */
function isPlainObject(value) {
  if (!isObjectLike(value) || baseGetTag(value) != objectTag$1) {
    return false;
  }
  var proto = getPrototype(value);
  if (proto === null) {
    return true;
  }
  var Ctor = hasOwnProperty$9.call(proto, 'constructor') && proto.constructor;
  return typeof Ctor == 'function' && Ctor instanceof Ctor &&
    funcToString$2.call(Ctor) == objectCtorString;
}

/**
 * The base implementation of `_.slice` without an iteratee call guard.
 *
 * @private
 * @param {Array} array The array to slice.
 * @param {number} [start=0] The start position.
 * @param {number} [end=array.length] The end position.
 * @returns {Array} Returns the slice of `array`.
 */
function baseSlice(array, start, end) {
  var index = -1,
      length = array.length;

  if (start < 0) {
    start = -start > length ? 0 : (length + start);
  }
  end = end > length ? length : end;
  if (end < 0) {
    end += length;
  }
  length = start > end ? 0 : ((end - start) >>> 0);
  start >>>= 0;

  var result = Array(length);
  while (++index < length) {
    result[index] = array[index + start];
  }
  return result;
}

/**
 * Casts `array` to a slice if it's needed.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {number} start The start position.
 * @param {number} [end=array.length] The end position.
 * @returns {Array} Returns the cast slice.
 */
function castSlice(array, start, end) {
  var length = array.length;
  end = end === undefined ? length : end;
  return (!start && end >= length) ? array : baseSlice(array, start, end);
}

/** Used to compose unicode character classes. */
var rsAstralRange = '\\ud800-\\udfff',
    rsComboMarksRange = '\\u0300-\\u036f',
    reComboHalfMarksRange = '\\ufe20-\\ufe2f',
    rsComboSymbolsRange = '\\u20d0-\\u20ff',
    rsComboRange = rsComboMarksRange + reComboHalfMarksRange + rsComboSymbolsRange,
    rsVarRange = '\\ufe0e\\ufe0f';

/** Used to compose unicode capture groups. */
var rsZWJ = '\\u200d';

/** Used to detect strings with [zero-width joiners or code points from the astral planes](http://eev.ee/blog/2015/09/12/dark-corners-of-unicode/). */
var reHasUnicode = RegExp('[' + rsZWJ + rsAstralRange  + rsComboRange + rsVarRange + ']');

/**
 * Checks if `string` contains Unicode symbols.
 *
 * @private
 * @param {string} string The string to inspect.
 * @returns {boolean} Returns `true` if a symbol is found, else `false`.
 */
function hasUnicode(string) {
  return reHasUnicode.test(string);
}

/**
 * Converts an ASCII `string` to an array.
 *
 * @private
 * @param {string} string The string to convert.
 * @returns {Array} Returns the converted array.
 */
function asciiToArray(string) {
  return string.split('');
}

/** Used to compose unicode character classes. */
var rsAstralRange$1 = '\\ud800-\\udfff',
    rsComboMarksRange$1 = '\\u0300-\\u036f',
    reComboHalfMarksRange$1 = '\\ufe20-\\ufe2f',
    rsComboSymbolsRange$1 = '\\u20d0-\\u20ff',
    rsComboRange$1 = rsComboMarksRange$1 + reComboHalfMarksRange$1 + rsComboSymbolsRange$1,
    rsVarRange$1 = '\\ufe0e\\ufe0f';

/** Used to compose unicode capture groups. */
var rsAstral = '[' + rsAstralRange$1 + ']',
    rsCombo = '[' + rsComboRange$1 + ']',
    rsFitz = '\\ud83c[\\udffb-\\udfff]',
    rsModifier = '(?:' + rsCombo + '|' + rsFitz + ')',
    rsNonAstral = '[^' + rsAstralRange$1 + ']',
    rsRegional = '(?:\\ud83c[\\udde6-\\uddff]){2}',
    rsSurrPair = '[\\ud800-\\udbff][\\udc00-\\udfff]',
    rsZWJ$1 = '\\u200d';

/** Used to compose unicode regexes. */
var reOptMod = rsModifier + '?',
    rsOptVar = '[' + rsVarRange$1 + ']?',
    rsOptJoin = '(?:' + rsZWJ$1 + '(?:' + [rsNonAstral, rsRegional, rsSurrPair].join('|') + ')' + rsOptVar + reOptMod + ')*',
    rsSeq = rsOptVar + reOptMod + rsOptJoin,
    rsSymbol = '(?:' + [rsNonAstral + rsCombo + '?', rsCombo, rsRegional, rsSurrPair, rsAstral].join('|') + ')';

/** Used to match [string symbols](https://mathiasbynens.be/notes/javascript-unicode). */
var reUnicode = RegExp(rsFitz + '(?=' + rsFitz + ')|' + rsSymbol + rsSeq, 'g');

/**
 * Converts a Unicode `string` to an array.
 *
 * @private
 * @param {string} string The string to convert.
 * @returns {Array} Returns the converted array.
 */
function unicodeToArray(string) {
  return string.match(reUnicode) || [];
}

/**
 * Converts `string` to an array.
 *
 * @private
 * @param {string} string The string to convert.
 * @returns {Array} Returns the converted array.
 */
function stringToArray(string) {
  return hasUnicode(string)
    ? unicodeToArray(string)
    : asciiToArray(string);
}

/**
 * Removes all key-value entries from the stack.
 *
 * @private
 * @name clear
 * @memberOf Stack
 */
function stackClear() {
  this.__data__ = new ListCache;
  this.size = 0;
}

/**
 * Removes `key` and its value from the stack.
 *
 * @private
 * @name delete
 * @memberOf Stack
 * @param {string} key The key of the value to remove.
 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
 */
function stackDelete(key) {
  var data = this.__data__,
      result = data['delete'](key);

  this.size = data.size;
  return result;
}

/**
 * Gets the stack value for `key`.
 *
 * @private
 * @name get
 * @memberOf Stack
 * @param {string} key The key of the value to get.
 * @returns {*} Returns the entry value.
 */
function stackGet(key) {
  return this.__data__.get(key);
}

/**
 * Checks if a stack value for `key` exists.
 *
 * @private
 * @name has
 * @memberOf Stack
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function stackHas(key) {
  return this.__data__.has(key);
}

/** Used as the size to enable large array optimizations. */
var LARGE_ARRAY_SIZE = 200;

/**
 * Sets the stack `key` to `value`.
 *
 * @private
 * @name set
 * @memberOf Stack
 * @param {string} key The key of the value to set.
 * @param {*} value The value to set.
 * @returns {Object} Returns the stack cache instance.
 */
function stackSet(key, value) {
  var data = this.__data__;
  if (data instanceof ListCache) {
    var pairs = data.__data__;
    if (!Map$1 || (pairs.length < LARGE_ARRAY_SIZE - 1)) {
      pairs.push([key, value]);
      this.size = ++data.size;
      return this;
    }
    data = this.__data__ = new MapCache(pairs);
  }
  data.set(key, value);
  this.size = data.size;
  return this;
}

/**
 * Creates a stack cache object to store key-value pairs.
 *
 * @private
 * @constructor
 * @param {Array} [entries] The key-value pairs to cache.
 */
function Stack(entries) {
  var data = this.__data__ = new ListCache(entries);
  this.size = data.size;
}

// Add methods to `Stack`.
Stack.prototype.clear = stackClear;
Stack.prototype['delete'] = stackDelete;
Stack.prototype.get = stackGet;
Stack.prototype.has = stackHas;
Stack.prototype.set = stackSet;

/** Detect free variable `exports`. */
var freeExports$2 = typeof exports == 'object' && exports && !exports.nodeType && exports;

/** Detect free variable `module`. */
var freeModule$2 = freeExports$2 && typeof module == 'object' && module && !module.nodeType && module;

/** Detect the popular CommonJS extension `module.exports`. */
var moduleExports$2 = freeModule$2 && freeModule$2.exports === freeExports$2;

/** Built-in value references. */
var Buffer$1 = moduleExports$2 ? root.Buffer : undefined,
    allocUnsafe = Buffer$1 ? Buffer$1.allocUnsafe : undefined;

/**
 * Creates a clone of  `buffer`.
 *
 * @private
 * @param {Buffer} buffer The buffer to clone.
 * @param {boolean} [isDeep] Specify a deep clone.
 * @returns {Buffer} Returns the cloned buffer.
 */
function cloneBuffer(buffer, isDeep) {
  if (isDeep) {
    return buffer.slice();
  }
  var length = buffer.length,
      result = allocUnsafe ? allocUnsafe(length) : new buffer.constructor(length);

  buffer.copy(result);
  return result;
}

/**
 * A specialized version of `_.filter` for arrays without support for
 * iteratee shorthands.
 *
 * @private
 * @param {Array} [array] The array to iterate over.
 * @param {Function} predicate The function invoked per iteration.
 * @returns {Array} Returns the new filtered array.
 */
function arrayFilter(array, predicate) {
  var index = -1,
      length = array == null ? 0 : array.length,
      resIndex = 0,
      result = [];

  while (++index < length) {
    var value = array[index];
    if (predicate(value, index, array)) {
      result[resIndex++] = value;
    }
  }
  return result;
}

/**
 * This method returns a new empty array.
 *
 * @static
 * @memberOf _
 * @since 4.13.0
 * @category Util
 * @returns {Array} Returns the new empty array.
 * @example
 *
 * var arrays = _.times(2, _.stubArray);
 *
 * console.log(arrays);
 * // => [[], []]
 *
 * console.log(arrays[0] === arrays[1]);
 * // => false
 */
function stubArray() {
  return [];
}

/** Used for built-in method references. */
var objectProto$c = Object.prototype;

/** Built-in value references. */
var propertyIsEnumerable$1 = objectProto$c.propertyIsEnumerable;

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeGetSymbols = Object.getOwnPropertySymbols;

/**
 * Creates an array of the own enumerable symbols of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of symbols.
 */
var getSymbols = !nativeGetSymbols ? stubArray : function(object) {
  if (object == null) {
    return [];
  }
  object = Object(object);
  return arrayFilter(nativeGetSymbols(object), function(symbol) {
    return propertyIsEnumerable$1.call(object, symbol);
  });
};

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeGetSymbols$1 = Object.getOwnPropertySymbols;

/**
 * Creates an array of the own and inherited enumerable symbols of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of symbols.
 */
var getSymbolsIn = !nativeGetSymbols$1 ? stubArray : function(object) {
  var result = [];
  while (object) {
    arrayPush(result, getSymbols(object));
    object = getPrototype(object);
  }
  return result;
};

/**
 * The base implementation of `getAllKeys` and `getAllKeysIn` which uses
 * `keysFunc` and `symbolsFunc` to get the enumerable property names and
 * symbols of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {Function} keysFunc The function to get the keys of `object`.
 * @param {Function} symbolsFunc The function to get the symbols of `object`.
 * @returns {Array} Returns the array of property names and symbols.
 */
function baseGetAllKeys(object, keysFunc, symbolsFunc) {
  var result = keysFunc(object);
  return isArray(object) ? result : arrayPush(result, symbolsFunc(object));
}

/**
 * Creates an array of own enumerable property names and symbols of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names and symbols.
 */
function getAllKeys(object) {
  return baseGetAllKeys(object, keys, getSymbols);
}

/**
 * Creates an array of own and inherited enumerable property names and
 * symbols of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names and symbols.
 */
function getAllKeysIn(object) {
  return baseGetAllKeys(object, keysIn, getSymbolsIn);
}

/* Built-in method references that are verified to be native. */
var DataView = getNative(root, 'DataView');

/* Built-in method references that are verified to be native. */
var Promise$1 = getNative(root, 'Promise');

/* Built-in method references that are verified to be native. */
var Set$1 = getNative(root, 'Set');

/** `Object#toString` result references. */
var mapTag$1 = '[object Map]',
    objectTag$2 = '[object Object]',
    promiseTag = '[object Promise]',
    setTag$1 = '[object Set]',
    weakMapTag$1 = '[object WeakMap]';

var dataViewTag$1 = '[object DataView]';

/** Used to detect maps, sets, and weakmaps. */
var dataViewCtorString = toSource(DataView),
    mapCtorString = toSource(Map$1),
    promiseCtorString = toSource(Promise$1),
    setCtorString = toSource(Set$1),
    weakMapCtorString = toSource(WeakMap$1);

/**
 * Gets the `toStringTag` of `value`.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the `toStringTag`.
 */
var getTag = baseGetTag;

// Fallback for data views, maps, sets, and weak maps in IE 11 and promises in Node.js < 6.
if ((DataView && getTag(new DataView(new ArrayBuffer(1))) != dataViewTag$1) ||
    (Map$1 && getTag(new Map$1) != mapTag$1) ||
    (Promise$1 && getTag(Promise$1.resolve()) != promiseTag) ||
    (Set$1 && getTag(new Set$1) != setTag$1) ||
    (WeakMap$1 && getTag(new WeakMap$1) != weakMapTag$1)) {
  getTag = function(value) {
    var result = baseGetTag(value),
        Ctor = result == objectTag$2 ? value.constructor : undefined,
        ctorString = Ctor ? toSource(Ctor) : '';

    if (ctorString) {
      switch (ctorString) {
        case dataViewCtorString: return dataViewTag$1;
        case mapCtorString: return mapTag$1;
        case promiseCtorString: return promiseTag;
        case setCtorString: return setTag$1;
        case weakMapCtorString: return weakMapTag$1;
      }
    }
    return result;
  };
}

var getTag$1 = getTag;

/** Built-in value references. */
var Uint8Array = root.Uint8Array;

/**
 * Creates a clone of `arrayBuffer`.
 *
 * @private
 * @param {ArrayBuffer} arrayBuffer The array buffer to clone.
 * @returns {ArrayBuffer} Returns the cloned array buffer.
 */
function cloneArrayBuffer(arrayBuffer) {
  var result = new arrayBuffer.constructor(arrayBuffer.byteLength);
  new Uint8Array(result).set(new Uint8Array(arrayBuffer));
  return result;
}

/**
 * Creates a clone of `typedArray`.
 *
 * @private
 * @param {Object} typedArray The typed array to clone.
 * @param {boolean} [isDeep] Specify a deep clone.
 * @returns {Object} Returns the cloned typed array.
 */
function cloneTypedArray(typedArray, isDeep) {
  var buffer = isDeep ? cloneArrayBuffer(typedArray.buffer) : typedArray.buffer;
  return new typedArray.constructor(buffer, typedArray.byteOffset, typedArray.length);
}

/**
 * Initializes an object clone.
 *
 * @private
 * @param {Object} object The object to clone.
 * @returns {Object} Returns the initialized clone.
 */
function initCloneObject(object) {
  return (typeof object.constructor == 'function' && !isPrototype(object))
    ? baseCreate(getPrototype(object))
    : {};
}

/** Used to stand-in for `undefined` hash values. */
var HASH_UNDEFINED$2 = '__lodash_hash_undefined__';

/**
 * Adds `value` to the array cache.
 *
 * @private
 * @name add
 * @memberOf SetCache
 * @alias push
 * @param {*} value The value to cache.
 * @returns {Object} Returns the cache instance.
 */
function setCacheAdd(value) {
  this.__data__.set(value, HASH_UNDEFINED$2);
  return this;
}

/**
 * Checks if `value` is in the array cache.
 *
 * @private
 * @name has
 * @memberOf SetCache
 * @param {*} value The value to search for.
 * @returns {number} Returns `true` if `value` is found, else `false`.
 */
function setCacheHas(value) {
  return this.__data__.has(value);
}

/**
 *
 * Creates an array cache object to store unique values.
 *
 * @private
 * @constructor
 * @param {Array} [values] The values to cache.
 */
function SetCache(values) {
  var index = -1,
      length = values == null ? 0 : values.length;

  this.__data__ = new MapCache;
  while (++index < length) {
    this.add(values[index]);
  }
}

// Add methods to `SetCache`.
SetCache.prototype.add = SetCache.prototype.push = setCacheAdd;
SetCache.prototype.has = setCacheHas;

/**
 * A specialized version of `_.some` for arrays without support for iteratee
 * shorthands.
 *
 * @private
 * @param {Array} [array] The array to iterate over.
 * @param {Function} predicate The function invoked per iteration.
 * @returns {boolean} Returns `true` if any element passes the predicate check,
 *  else `false`.
 */
function arraySome(array, predicate) {
  var index = -1,
      length = array == null ? 0 : array.length;

  while (++index < length) {
    if (predicate(array[index], index, array)) {
      return true;
    }
  }
  return false;
}

/**
 * Checks if a `cache` value for `key` exists.
 *
 * @private
 * @param {Object} cache The cache to query.
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function cacheHas(cache, key) {
  return cache.has(key);
}

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG = 1,
    COMPARE_UNORDERED_FLAG = 2;

/**
 * A specialized version of `baseIsEqualDeep` for arrays with support for
 * partial deep comparisons.
 *
 * @private
 * @param {Array} array The array to compare.
 * @param {Array} other The other array to compare.
 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
 * @param {Function} customizer The function to customize comparisons.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Object} stack Tracks traversed `array` and `other` objects.
 * @returns {boolean} Returns `true` if the arrays are equivalent, else `false`.
 */
function equalArrays(array, other, bitmask, customizer, equalFunc, stack) {
  var isPartial = bitmask & COMPARE_PARTIAL_FLAG,
      arrLength = array.length,
      othLength = other.length;

  if (arrLength != othLength && !(isPartial && othLength > arrLength)) {
    return false;
  }
  // Assume cyclic values are equal.
  var stacked = stack.get(array);
  if (stacked && stack.get(other)) {
    return stacked == other;
  }
  var index = -1,
      result = true,
      seen = (bitmask & COMPARE_UNORDERED_FLAG) ? new SetCache : undefined;

  stack.set(array, other);
  stack.set(other, array);

  // Ignore non-index properties.
  while (++index < arrLength) {
    var arrValue = array[index],
        othValue = other[index];

    if (customizer) {
      var compared = isPartial
        ? customizer(othValue, arrValue, index, other, array, stack)
        : customizer(arrValue, othValue, index, array, other, stack);
    }
    if (compared !== undefined) {
      if (compared) {
        continue;
      }
      result = false;
      break;
    }
    // Recursively compare arrays (susceptible to call stack limits).
    if (seen) {
      if (!arraySome(other, function(othValue, othIndex) {
            if (!cacheHas(seen, othIndex) &&
                (arrValue === othValue || equalFunc(arrValue, othValue, bitmask, customizer, stack))) {
              return seen.push(othIndex);
            }
          })) {
        result = false;
        break;
      }
    } else if (!(
          arrValue === othValue ||
            equalFunc(arrValue, othValue, bitmask, customizer, stack)
        )) {
      result = false;
      break;
    }
  }
  stack['delete'](array);
  stack['delete'](other);
  return result;
}

/**
 * Converts `map` to its key-value pairs.
 *
 * @private
 * @param {Object} map The map to convert.
 * @returns {Array} Returns the key-value pairs.
 */
function mapToArray(map) {
  var index = -1,
      result = Array(map.size);

  map.forEach(function(value, key) {
    result[++index] = [key, value];
  });
  return result;
}

/**
 * Converts `set` to an array of its values.
 *
 * @private
 * @param {Object} set The set to convert.
 * @returns {Array} Returns the values.
 */
function setToArray(set) {
  var index = -1,
      result = Array(set.size);

  set.forEach(function(value) {
    result[++index] = value;
  });
  return result;
}

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG$1 = 1,
    COMPARE_UNORDERED_FLAG$1 = 2;

/** `Object#toString` result references. */
var boolTag$1 = '[object Boolean]',
    dateTag$1 = '[object Date]',
    errorTag$1 = '[object Error]',
    mapTag$2 = '[object Map]',
    numberTag$1 = '[object Number]',
    regexpTag$1 = '[object RegExp]',
    setTag$2 = '[object Set]',
    stringTag$1 = '[object String]',
    symbolTag$1 = '[object Symbol]';

var arrayBufferTag$1 = '[object ArrayBuffer]',
    dataViewTag$2 = '[object DataView]';

/** Used to convert symbols to primitives and strings. */
var symbolProto$1 = Symbol ? Symbol.prototype : undefined,
    symbolValueOf = symbolProto$1 ? symbolProto$1.valueOf : undefined;

/**
 * A specialized version of `baseIsEqualDeep` for comparing objects of
 * the same `toStringTag`.
 *
 * **Note:** This function only supports comparing values with tags of
 * `Boolean`, `Date`, `Error`, `Number`, `RegExp`, or `String`.
 *
 * @private
 * @param {Object} object The object to compare.
 * @param {Object} other The other object to compare.
 * @param {string} tag The `toStringTag` of the objects to compare.
 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
 * @param {Function} customizer The function to customize comparisons.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Object} stack Tracks traversed `object` and `other` objects.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */
function equalByTag(object, other, tag, bitmask, customizer, equalFunc, stack) {
  switch (tag) {
    case dataViewTag$2:
      if ((object.byteLength != other.byteLength) ||
          (object.byteOffset != other.byteOffset)) {
        return false;
      }
      object = object.buffer;
      other = other.buffer;

    case arrayBufferTag$1:
      if ((object.byteLength != other.byteLength) ||
          !equalFunc(new Uint8Array(object), new Uint8Array(other))) {
        return false;
      }
      return true;

    case boolTag$1:
    case dateTag$1:
    case numberTag$1:
      // Coerce booleans to `1` or `0` and dates to milliseconds.
      // Invalid dates are coerced to `NaN`.
      return eq(+object, +other);

    case errorTag$1:
      return object.name == other.name && object.message == other.message;

    case regexpTag$1:
    case stringTag$1:
      // Coerce regexes to strings and treat strings, primitives and objects,
      // as equal. See http://www.ecma-international.org/ecma-262/7.0/#sec-regexp.prototype.tostring
      // for more details.
      return object == (other + '');

    case mapTag$2:
      var convert = mapToArray;

    case setTag$2:
      var isPartial = bitmask & COMPARE_PARTIAL_FLAG$1;
      convert || (convert = setToArray);

      if (object.size != other.size && !isPartial) {
        return false;
      }
      // Assume cyclic values are equal.
      var stacked = stack.get(object);
      if (stacked) {
        return stacked == other;
      }
      bitmask |= COMPARE_UNORDERED_FLAG$1;

      // Recursively compare objects (susceptible to call stack limits).
      stack.set(object, other);
      var result = equalArrays(convert(object), convert(other), bitmask, customizer, equalFunc, stack);
      stack['delete'](object);
      return result;

    case symbolTag$1:
      if (symbolValueOf) {
        return symbolValueOf.call(object) == symbolValueOf.call(other);
      }
  }
  return false;
}

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG$2 = 1;

/** Used for built-in method references. */
var objectProto$d = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty$a = objectProto$d.hasOwnProperty;

/**
 * A specialized version of `baseIsEqualDeep` for objects with support for
 * partial deep comparisons.
 *
 * @private
 * @param {Object} object The object to compare.
 * @param {Object} other The other object to compare.
 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
 * @param {Function} customizer The function to customize comparisons.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Object} stack Tracks traversed `object` and `other` objects.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */
function equalObjects(object, other, bitmask, customizer, equalFunc, stack) {
  var isPartial = bitmask & COMPARE_PARTIAL_FLAG$2,
      objProps = getAllKeys(object),
      objLength = objProps.length,
      othProps = getAllKeys(other),
      othLength = othProps.length;

  if (objLength != othLength && !isPartial) {
    return false;
  }
  var index = objLength;
  while (index--) {
    var key = objProps[index];
    if (!(isPartial ? key in other : hasOwnProperty$a.call(other, key))) {
      return false;
    }
  }
  // Assume cyclic values are equal.
  var stacked = stack.get(object);
  if (stacked && stack.get(other)) {
    return stacked == other;
  }
  var result = true;
  stack.set(object, other);
  stack.set(other, object);

  var skipCtor = isPartial;
  while (++index < objLength) {
    key = objProps[index];
    var objValue = object[key],
        othValue = other[key];

    if (customizer) {
      var compared = isPartial
        ? customizer(othValue, objValue, key, other, object, stack)
        : customizer(objValue, othValue, key, object, other, stack);
    }
    // Recursively compare objects (susceptible to call stack limits).
    if (!(compared === undefined
          ? (objValue === othValue || equalFunc(objValue, othValue, bitmask, customizer, stack))
          : compared
        )) {
      result = false;
      break;
    }
    skipCtor || (skipCtor = key == 'constructor');
  }
  if (result && !skipCtor) {
    var objCtor = object.constructor,
        othCtor = other.constructor;

    // Non `Object` object instances with different constructors are not equal.
    if (objCtor != othCtor &&
        ('constructor' in object && 'constructor' in other) &&
        !(typeof objCtor == 'function' && objCtor instanceof objCtor &&
          typeof othCtor == 'function' && othCtor instanceof othCtor)) {
      result = false;
    }
  }
  stack['delete'](object);
  stack['delete'](other);
  return result;
}

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG$3 = 1;

/** `Object#toString` result references. */
var argsTag$2 = '[object Arguments]',
    arrayTag$1 = '[object Array]',
    objectTag$3 = '[object Object]';

/** Used for built-in method references. */
var objectProto$e = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty$b = objectProto$e.hasOwnProperty;

/**
 * A specialized version of `baseIsEqual` for arrays and objects which performs
 * deep comparisons and tracks traversed objects enabling objects with circular
 * references to be compared.
 *
 * @private
 * @param {Object} object The object to compare.
 * @param {Object} other The other object to compare.
 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
 * @param {Function} customizer The function to customize comparisons.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Object} [stack] Tracks traversed `object` and `other` objects.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */
function baseIsEqualDeep(object, other, bitmask, customizer, equalFunc, stack) {
  var objIsArr = isArray(object),
      othIsArr = isArray(other),
      objTag = objIsArr ? arrayTag$1 : getTag$1(object),
      othTag = othIsArr ? arrayTag$1 : getTag$1(other);

  objTag = objTag == argsTag$2 ? objectTag$3 : objTag;
  othTag = othTag == argsTag$2 ? objectTag$3 : othTag;

  var objIsObj = objTag == objectTag$3,
      othIsObj = othTag == objectTag$3,
      isSameTag = objTag == othTag;

  if (isSameTag && isBuffer(object)) {
    if (!isBuffer(other)) {
      return false;
    }
    objIsArr = true;
    objIsObj = false;
  }
  if (isSameTag && !objIsObj) {
    stack || (stack = new Stack);
    return (objIsArr || isTypedArray(object))
      ? equalArrays(object, other, bitmask, customizer, equalFunc, stack)
      : equalByTag(object, other, objTag, bitmask, customizer, equalFunc, stack);
  }
  if (!(bitmask & COMPARE_PARTIAL_FLAG$3)) {
    var objIsWrapped = objIsObj && hasOwnProperty$b.call(object, '__wrapped__'),
        othIsWrapped = othIsObj && hasOwnProperty$b.call(other, '__wrapped__');

    if (objIsWrapped || othIsWrapped) {
      var objUnwrapped = objIsWrapped ? object.value() : object,
          othUnwrapped = othIsWrapped ? other.value() : other;

      stack || (stack = new Stack);
      return equalFunc(objUnwrapped, othUnwrapped, bitmask, customizer, stack);
    }
  }
  if (!isSameTag) {
    return false;
  }
  stack || (stack = new Stack);
  return equalObjects(object, other, bitmask, customizer, equalFunc, stack);
}

/**
 * The base implementation of `_.isEqual` which supports partial comparisons
 * and tracks traversed objects.
 *
 * @private
 * @param {*} value The value to compare.
 * @param {*} other The other value to compare.
 * @param {boolean} bitmask The bitmask flags.
 *  1 - Unordered comparison
 *  2 - Partial comparison
 * @param {Function} [customizer] The function to customize comparisons.
 * @param {Object} [stack] Tracks traversed `value` and `other` objects.
 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
 */
function baseIsEqual(value, other, bitmask, customizer, stack) {
  if (value === other) {
    return true;
  }
  if (value == null || other == null || (!isObjectLike(value) && !isObjectLike(other))) {
    return value !== value && other !== other;
  }
  return baseIsEqualDeep(value, other, bitmask, customizer, baseIsEqual, stack);
}

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG$4 = 1,
    COMPARE_UNORDERED_FLAG$2 = 2;

/**
 * The base implementation of `_.isMatch` without support for iteratee shorthands.
 *
 * @private
 * @param {Object} object The object to inspect.
 * @param {Object} source The object of property values to match.
 * @param {Array} matchData The property names, values, and compare flags to match.
 * @param {Function} [customizer] The function to customize comparisons.
 * @returns {boolean} Returns `true` if `object` is a match, else `false`.
 */
function baseIsMatch(object, source, matchData, customizer) {
  var index = matchData.length,
      length = index,
      noCustomizer = !customizer;

  if (object == null) {
    return !length;
  }
  object = Object(object);
  while (index--) {
    var data = matchData[index];
    if ((noCustomizer && data[2])
          ? data[1] !== object[data[0]]
          : !(data[0] in object)
        ) {
      return false;
    }
  }
  while (++index < length) {
    data = matchData[index];
    var key = data[0],
        objValue = object[key],
        srcValue = data[1];

    if (noCustomizer && data[2]) {
      if (objValue === undefined && !(key in object)) {
        return false;
      }
    } else {
      var stack = new Stack;
      if (customizer) {
        var result = customizer(objValue, srcValue, key, object, source, stack);
      }
      if (!(result === undefined
            ? baseIsEqual(srcValue, objValue, COMPARE_PARTIAL_FLAG$4 | COMPARE_UNORDERED_FLAG$2, customizer, stack)
            : result
          )) {
        return false;
      }
    }
  }
  return true;
}

/**
 * Checks if `value` is suitable for strict equality comparisons, i.e. `===`.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` if suitable for strict
 *  equality comparisons, else `false`.
 */
function isStrictComparable(value) {
  return value === value && !isObject(value);
}

/**
 * Gets the property names, values, and compare flags of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the match data of `object`.
 */
function getMatchData(object) {
  var result = keys(object),
      length = result.length;

  while (length--) {
    var key = result[length],
        value = object[key];

    result[length] = [key, value, isStrictComparable(value)];
  }
  return result;
}

/**
 * A specialized version of `matchesProperty` for source values suitable
 * for strict equality comparisons, i.e. `===`.
 *
 * @private
 * @param {string} key The key of the property to get.
 * @param {*} srcValue The value to match.
 * @returns {Function} Returns the new spec function.
 */
function matchesStrictComparable(key, srcValue) {
  return function(object) {
    if (object == null) {
      return false;
    }
    return object[key] === srcValue &&
      (srcValue !== undefined || (key in Object(object)));
  };
}

/**
 * The base implementation of `_.matches` which doesn't clone `source`.
 *
 * @private
 * @param {Object} source The object of property values to match.
 * @returns {Function} Returns the new spec function.
 */
function baseMatches(source) {
  var matchData = getMatchData(source);
  if (matchData.length == 1 && matchData[0][2]) {
    return matchesStrictComparable(matchData[0][0], matchData[0][1]);
  }
  return function(object) {
    return object === source || baseIsMatch(object, source, matchData);
  };
}

/**
 * The base implementation of `_.hasIn` without support for deep paths.
 *
 * @private
 * @param {Object} [object] The object to query.
 * @param {Array|string} key The key to check.
 * @returns {boolean} Returns `true` if `key` exists, else `false`.
 */
function baseHasIn(object, key) {
  return object != null && key in Object(object);
}

/**
 * Checks if `path` exists on `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {Array|string} path The path to check.
 * @param {Function} hasFunc The function to check properties.
 * @returns {boolean} Returns `true` if `path` exists, else `false`.
 */
function hasPath(object, path, hasFunc) {
  path = castPath(path, object);

  var index = -1,
      length = path.length,
      result = false;

  while (++index < length) {
    var key = toKey(path[index]);
    if (!(result = object != null && hasFunc(object, key))) {
      break;
    }
    object = object[key];
  }
  if (result || ++index != length) {
    return result;
  }
  length = object == null ? 0 : object.length;
  return !!length && isLength(length) && isIndex(key, length) &&
    (isArray(object) || isArguments(object));
}

/**
 * Checks if `path` is a direct or inherited property of `object`.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Object
 * @param {Object} object The object to query.
 * @param {Array|string} path The path to check.
 * @returns {boolean} Returns `true` if `path` exists, else `false`.
 * @example
 *
 * var object = _.create({ 'a': _.create({ 'b': 2 }) });
 *
 * _.hasIn(object, 'a');
 * // => true
 *
 * _.hasIn(object, 'a.b');
 * // => true
 *
 * _.hasIn(object, ['a', 'b']);
 * // => true
 *
 * _.hasIn(object, 'b');
 * // => false
 */
function hasIn(object, path) {
  return object != null && hasPath(object, path, baseHasIn);
}

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG$5 = 1,
    COMPARE_UNORDERED_FLAG$3 = 2;

/**
 * The base implementation of `_.matchesProperty` which doesn't clone `srcValue`.
 *
 * @private
 * @param {string} path The path of the property to get.
 * @param {*} srcValue The value to match.
 * @returns {Function} Returns the new spec function.
 */
function baseMatchesProperty(path, srcValue) {
  if (isKey(path) && isStrictComparable(srcValue)) {
    return matchesStrictComparable(toKey(path), srcValue);
  }
  return function(object) {
    var objValue = get(object, path);
    return (objValue === undefined && objValue === srcValue)
      ? hasIn(object, path)
      : baseIsEqual(srcValue, objValue, COMPARE_PARTIAL_FLAG$5 | COMPARE_UNORDERED_FLAG$3);
  };
}

/**
 * The base implementation of `_.property` without support for deep paths.
 *
 * @private
 * @param {string} key The key of the property to get.
 * @returns {Function} Returns the new accessor function.
 */
function baseProperty(key) {
  return function(object) {
    return object == null ? undefined : object[key];
  };
}

/**
 * A specialized version of `baseProperty` which supports deep paths.
 *
 * @private
 * @param {Array|string} path The path of the property to get.
 * @returns {Function} Returns the new accessor function.
 */
function basePropertyDeep(path) {
  return function(object) {
    return baseGet(object, path);
  };
}

/**
 * Creates a function that returns the value at `path` of a given object.
 *
 * @static
 * @memberOf _
 * @since 2.4.0
 * @category Util
 * @param {Array|string} path The path of the property to get.
 * @returns {Function} Returns the new accessor function.
 * @example
 *
 * var objects = [
 *   { 'a': { 'b': 2 } },
 *   { 'a': { 'b': 1 } }
 * ];
 *
 * _.map(objects, _.property('a.b'));
 * // => [2, 1]
 *
 * _.map(_.sortBy(objects, _.property(['a', 'b'])), 'a.b');
 * // => [1, 2]
 */
function property(path) {
  return isKey(path) ? baseProperty(toKey(path)) : basePropertyDeep(path);
}

/**
 * The base implementation of `_.iteratee`.
 *
 * @private
 * @param {*} [value=_.identity] The value to convert to an iteratee.
 * @returns {Function} Returns the iteratee.
 */
function baseIteratee(value) {
  // Don't store the `typeof` result in a variable to avoid a JIT bug in Safari 9.
  // See https://bugs.webkit.org/show_bug.cgi?id=156034 for more details.
  if (typeof value == 'function') {
    return value;
  }
  if (value == null) {
    return identity;
  }
  if (typeof value == 'object') {
    return isArray(value)
      ? baseMatchesProperty(value[0], value[1])
      : baseMatches(value);
  }
  return property(value);
}

/**
 * Creates a base function for methods like `_.forIn` and `_.forOwn`.
 *
 * @private
 * @param {boolean} [fromRight] Specify iterating from right to left.
 * @returns {Function} Returns the new base function.
 */
function createBaseFor(fromRight) {
  return function(object, iteratee, keysFunc) {
    var index = -1,
        iterable = Object(object),
        props = keysFunc(object),
        length = props.length;

    while (length--) {
      var key = props[fromRight ? length : ++index];
      if (iteratee(iterable[key], key, iterable) === false) {
        break;
      }
    }
    return object;
  };
}

/**
 * The base implementation of `baseForOwn` which iterates over `object`
 * properties returned by `keysFunc` and invokes `iteratee` for each property.
 * Iteratee functions may exit iteration early by explicitly returning `false`.
 *
 * @private
 * @param {Object} object The object to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @param {Function} keysFunc The function to get the keys of `object`.
 * @returns {Object} Returns `object`.
 */
var baseFor = createBaseFor();

/**
 * The base implementation of `_.forOwn` without support for iteratee shorthands.
 *
 * @private
 * @param {Object} object The object to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Object} Returns `object`.
 */
function baseForOwn(object, iteratee) {
  return object && baseFor(object, iteratee, keys);
}

/**
 * This function is like `assignValue` except that it doesn't assign
 * `undefined` values.
 *
 * @private
 * @param {Object} object The object to modify.
 * @param {string} key The key of the property to assign.
 * @param {*} value The value to assign.
 */
function assignMergeValue(object, key, value) {
  if ((value !== undefined && !eq(object[key], value)) ||
      (value === undefined && !(key in object))) {
    baseAssignValue(object, key, value);
  }
}

/**
 * This method is like `_.isArrayLike` except that it also checks if `value`
 * is an object.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an array-like object,
 *  else `false`.
 * @example
 *
 * _.isArrayLikeObject([1, 2, 3]);
 * // => true
 *
 * _.isArrayLikeObject(document.body.children);
 * // => true
 *
 * _.isArrayLikeObject('abc');
 * // => false
 *
 * _.isArrayLikeObject(_.noop);
 * // => false
 */
function isArrayLikeObject(value) {
  return isObjectLike(value) && isArrayLike(value);
}

/**
 * Gets the value at `key`, unless `key` is "__proto__" or "constructor".
 *
 * @private
 * @param {Object} object The object to query.
 * @param {string} key The key of the property to get.
 * @returns {*} Returns the property value.
 */
function safeGet(object, key) {
  if (key === 'constructor' && typeof object[key] === 'function') {
    return;
  }

  if (key == '__proto__') {
    return;
  }

  return object[key];
}

/**
 * Converts `value` to a plain object flattening inherited enumerable string
 * keyed properties of `value` to own properties of the plain object.
 *
 * @static
 * @memberOf _
 * @since 3.0.0
 * @category Lang
 * @param {*} value The value to convert.
 * @returns {Object} Returns the converted plain object.
 * @example
 *
 * function Foo() {
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.assign({ 'a': 1 }, new Foo);
 * // => { 'a': 1, 'b': 2 }
 *
 * _.assign({ 'a': 1 }, _.toPlainObject(new Foo));
 * // => { 'a': 1, 'b': 2, 'c': 3 }
 */
function toPlainObject(value) {
  return copyObject(value, keysIn(value));
}

/**
 * A specialized version of `baseMerge` for arrays and objects which performs
 * deep merges and tracks traversed objects enabling objects with circular
 * references to be merged.
 *
 * @private
 * @param {Object} object The destination object.
 * @param {Object} source The source object.
 * @param {string} key The key of the value to merge.
 * @param {number} srcIndex The index of `source`.
 * @param {Function} mergeFunc The function to merge values.
 * @param {Function} [customizer] The function to customize assigned values.
 * @param {Object} [stack] Tracks traversed source values and their merged
 *  counterparts.
 */
function baseMergeDeep(object, source, key, srcIndex, mergeFunc, customizer, stack) {
  var objValue = safeGet(object, key),
      srcValue = safeGet(source, key),
      stacked = stack.get(srcValue);

  if (stacked) {
    assignMergeValue(object, key, stacked);
    return;
  }
  var newValue = customizer
    ? customizer(objValue, srcValue, (key + ''), object, source, stack)
    : undefined;

  var isCommon = newValue === undefined;

  if (isCommon) {
    var isArr = isArray(srcValue),
        isBuff = !isArr && isBuffer(srcValue),
        isTyped = !isArr && !isBuff && isTypedArray(srcValue);

    newValue = srcValue;
    if (isArr || isBuff || isTyped) {
      if (isArray(objValue)) {
        newValue = objValue;
      }
      else if (isArrayLikeObject(objValue)) {
        newValue = copyArray(objValue);
      }
      else if (isBuff) {
        isCommon = false;
        newValue = cloneBuffer(srcValue, true);
      }
      else if (isTyped) {
        isCommon = false;
        newValue = cloneTypedArray(srcValue, true);
      }
      else {
        newValue = [];
      }
    }
    else if (isPlainObject(srcValue) || isArguments(srcValue)) {
      newValue = objValue;
      if (isArguments(objValue)) {
        newValue = toPlainObject(objValue);
      }
      else if (!isObject(objValue) || isFunction(objValue)) {
        newValue = initCloneObject(srcValue);
      }
    }
    else {
      isCommon = false;
    }
  }
  if (isCommon) {
    // Recursively merge objects and arrays (susceptible to call stack limits).
    stack.set(srcValue, newValue);
    mergeFunc(newValue, srcValue, srcIndex, customizer, stack);
    stack['delete'](srcValue);
  }
  assignMergeValue(object, key, newValue);
}

/**
 * The base implementation of `_.merge` without support for multiple sources.
 *
 * @private
 * @param {Object} object The destination object.
 * @param {Object} source The source object.
 * @param {number} srcIndex The index of `source`.
 * @param {Function} [customizer] The function to customize merged values.
 * @param {Object} [stack] Tracks traversed source values and their merged
 *  counterparts.
 */
function baseMerge(object, source, srcIndex, customizer, stack) {
  if (object === source) {
    return;
  }
  baseFor(source, function(srcValue, key) {
    stack || (stack = new Stack);
    if (isObject(srcValue)) {
      baseMergeDeep(object, source, key, srcIndex, baseMerge, customizer, stack);
    }
    else {
      var newValue = customizer
        ? customizer(safeGet(object, key), srcValue, (key + ''), object, source, stack)
        : undefined;

      if (newValue === undefined) {
        newValue = srcValue;
      }
      assignMergeValue(object, key, newValue);
    }
  }, keysIn);
}

/**
 * The base implementation of methods like `_.findKey` and `_.findLastKey`,
 * without support for iteratee shorthands, which iterates over `collection`
 * using `eachFunc`.
 *
 * @private
 * @param {Array|Object} collection The collection to inspect.
 * @param {Function} predicate The function invoked per iteration.
 * @param {Function} eachFunc The function to iterate over `collection`.
 * @returns {*} Returns the found element or its key, else `undefined`.
 */
function baseFindKey(collection, predicate, eachFunc) {
  var result;
  eachFunc(collection, function(value, key, collection) {
    if (predicate(value, key, collection)) {
      result = key;
      return false;
    }
  });
  return result;
}

/**
 * This method is like `_.find` except that it returns the key of the first
 * element `predicate` returns truthy for instead of the element itself.
 *
 * @static
 * @memberOf _
 * @since 1.1.0
 * @category Object
 * @param {Object} object The object to inspect.
 * @param {Function} [predicate=_.identity] The function invoked per iteration.
 * @returns {string|undefined} Returns the key of the matched element,
 *  else `undefined`.
 * @example
 *
 * var users = {
 *   'barney':  { 'age': 36, 'active': true },
 *   'fred':    { 'age': 40, 'active': false },
 *   'pebbles': { 'age': 1,  'active': true }
 * };
 *
 * _.findKey(users, function(o) { return o.age < 40; });
 * // => 'barney' (iteration order is not guaranteed)
 *
 * // The `_.matches` iteratee shorthand.
 * _.findKey(users, { 'age': 1, 'active': true });
 * // => 'pebbles'
 *
 * // The `_.matchesProperty` iteratee shorthand.
 * _.findKey(users, ['active', false]);
 * // => 'fred'
 *
 * // The `_.property` iteratee shorthand.
 * _.findKey(users, 'active');
 * // => 'barney'
 */
function findKey(object, predicate) {
  return baseFindKey(object, baseIteratee(predicate), baseForOwn);
}

/** `Object#toString` result references. */
var stringTag$2 = '[object String]';

/**
 * Checks if `value` is classified as a `String` primitive or object.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a string, else `false`.
 * @example
 *
 * _.isString('abc');
 * // => true
 *
 * _.isString(1);
 * // => false
 */
function isString(value) {
  return typeof value == 'string' ||
    (!isArray(value) && isObjectLike(value) && baseGetTag(value) == stringTag$2);
}

/** `Object#toString` result references. */
var boolTag$2 = '[object Boolean]';

/**
 * Checks if `value` is classified as a boolean primitive or object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a boolean, else `false`.
 * @example
 *
 * _.isBoolean(false);
 * // => true
 *
 * _.isBoolean(null);
 * // => false
 */
function isBoolean(value) {
  return value === true || value === false ||
    (isObjectLike(value) && baseGetTag(value) == boolTag$2);
}

/**
 * Performs a deep comparison between two values to determine if they are
 * equivalent.
 *
 * **Note:** This method supports comparing arrays, array buffers, booleans,
 * date objects, error objects, maps, numbers, `Object` objects, regexes,
 * sets, strings, symbols, and typed arrays. `Object` objects are compared
 * by their own, not inherited, enumerable properties. Functions and DOM
 * nodes are compared by strict equality, i.e. `===`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to compare.
 * @param {*} other The other value to compare.
 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
 * @example
 *
 * var object = { 'a': 1 };
 * var other = { 'a': 1 };
 *
 * _.isEqual(object, other);
 * // => true
 *
 * object === other;
 * // => false
 */
function isEqual(value, other) {
  return baseIsEqual(value, other);
}

/** `Object#toString` result references. */
var numberTag$2 = '[object Number]';

/**
 * Checks if `value` is classified as a `Number` primitive or object.
 *
 * **Note:** To exclude `Infinity`, `-Infinity`, and `NaN`, which are
 * classified as numbers, use the `_.isFinite` method.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a number, else `false`.
 * @example
 *
 * _.isNumber(3);
 * // => true
 *
 * _.isNumber(Number.MIN_VALUE);
 * // => true
 *
 * _.isNumber(Infinity);
 * // => true
 *
 * _.isNumber('3');
 * // => false
 */
function isNumber(value) {
  return typeof value == 'number' ||
    (isObjectLike(value) && baseGetTag(value) == numberTag$2);
}

/**
 * Checks if `value` is `NaN`.
 *
 * **Note:** This method is based on
 * [`Number.isNaN`](https://mdn.io/Number/isNaN) and is not the same as
 * global [`isNaN`](https://mdn.io/isNaN) which returns `true` for
 * `undefined` and other non-number values.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is `NaN`, else `false`.
 * @example
 *
 * _.isNaN(NaN);
 * // => true
 *
 * _.isNaN(new Number(NaN));
 * // => true
 *
 * isNaN(undefined);
 * // => true
 *
 * _.isNaN(undefined);
 * // => false
 */
function isNaN(value) {
  // An `NaN` primitive is the only value that is not equal to itself.
  // Perform the `toStringTag` check first to avoid errors with some
  // ActiveX objects in IE.
  return isNumber(value) && value != +value;
}

/**
 * Checks if `value` is `null`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is `null`, else `false`.
 * @example
 *
 * _.isNull(null);
 * // => true
 *
 * _.isNull(void 0);
 * // => false
 */
function isNull(value) {
  return value === null;
}

/**
 * Checks if `value` is `undefined`.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is `undefined`, else `false`.
 * @example
 *
 * _.isUndefined(void 0);
 * // => true
 *
 * _.isUndefined(null);
 * // => false
 */
function isUndefined(value) {
  return value === undefined;
}

/**
 * The opposite of `_.mapValues`; this method creates an object with the
 * same values as `object` and keys generated by running each own enumerable
 * string keyed property of `object` thru `iteratee`. The iteratee is invoked
 * with three arguments: (value, key, object).
 *
 * @static
 * @memberOf _
 * @since 3.8.0
 * @category Object
 * @param {Object} object The object to iterate over.
 * @param {Function} [iteratee=_.identity] The function invoked per iteration.
 * @returns {Object} Returns the new mapped object.
 * @see _.mapValues
 * @example
 *
 * _.mapKeys({ 'a': 1, 'b': 2 }, function(value, key) {
 *   return key + value;
 * });
 * // => { 'a1': 1, 'b2': 2 }
 */
function mapKeys(object, iteratee) {
  var result = {};
  iteratee = baseIteratee(iteratee);

  baseForOwn(object, function(value, key, object) {
    baseAssignValue(result, iteratee(value, key, object), value);
  });
  return result;
}

/**
 * Creates an object with the same keys as `object` and values generated
 * by running each own enumerable string keyed property of `object` thru
 * `iteratee`. The iteratee is invoked with three arguments:
 * (value, key, object).
 *
 * @static
 * @memberOf _
 * @since 2.4.0
 * @category Object
 * @param {Object} object The object to iterate over.
 * @param {Function} [iteratee=_.identity] The function invoked per iteration.
 * @returns {Object} Returns the new mapped object.
 * @see _.mapKeys
 * @example
 *
 * var users = {
 *   'fred':    { 'user': 'fred',    'age': 40 },
 *   'pebbles': { 'user': 'pebbles', 'age': 1 }
 * };
 *
 * _.mapValues(users, function(o) { return o.age; });
 * // => { 'fred': 40, 'pebbles': 1 } (iteration order is not guaranteed)
 *
 * // The `_.property` iteratee shorthand.
 * _.mapValues(users, 'age');
 * // => { 'fred': 40, 'pebbles': 1 } (iteration order is not guaranteed)
 */
function mapValues(object, iteratee) {
  var result = {};
  iteratee = baseIteratee(iteratee);

  baseForOwn(object, function(value, key, object) {
    baseAssignValue(result, key, iteratee(value, key, object));
  });
  return result;
}

/**
 * This method is like `_.assign` except that it recursively merges own and
 * inherited enumerable string keyed properties of source objects into the
 * destination object. Source properties that resolve to `undefined` are
 * skipped if a destination value exists. Array and plain object properties
 * are merged recursively. Other objects and value types are overridden by
 * assignment. Source objects are applied from left to right. Subsequent
 * sources overwrite property assignments of previous sources.
 *
 * **Note:** This method mutates `object`.
 *
 * @static
 * @memberOf _
 * @since 0.5.0
 * @category Object
 * @param {Object} object The destination object.
 * @param {...Object} [sources] The source objects.
 * @returns {Object} Returns `object`.
 * @example
 *
 * var object = {
 *   'a': [{ 'b': 2 }, { 'd': 4 }]
 * };
 *
 * var other = {
 *   'a': [{ 'c': 3 }, { 'e': 5 }]
 * };
 *
 * _.merge(object, other);
 * // => { 'a': [{ 'b': 2, 'c': 3 }, { 'd': 4, 'e': 5 }] }
 */
var merge = createAssigner(function(object, source, srcIndex) {
  baseMerge(object, source, srcIndex);
});

/** Error message constants. */
var FUNC_ERROR_TEXT$1 = 'Expected a function';

/**
 * Creates a function that negates the result of the predicate `func`. The
 * `func` predicate is invoked with the `this` binding and arguments of the
 * created function.
 *
 * @static
 * @memberOf _
 * @since 3.0.0
 * @category Function
 * @param {Function} predicate The predicate to negate.
 * @returns {Function} Returns the new negated function.
 * @example
 *
 * function isEven(n) {
 *   return n % 2 == 0;
 * }
 *
 * _.filter([1, 2, 3, 4, 5, 6], _.negate(isEven));
 * // => [1, 3, 5]
 */
function negate(predicate) {
  if (typeof predicate != 'function') {
    throw new TypeError(FUNC_ERROR_TEXT$1);
  }
  return function() {
    var args = arguments;
    switch (args.length) {
      case 0: return !predicate.call(this);
      case 1: return !predicate.call(this, args[0]);
      case 2: return !predicate.call(this, args[0], args[1]);
      case 3: return !predicate.call(this, args[0], args[1], args[2]);
    }
    return !predicate.apply(this, args);
  };
}

/**
 * The base implementation of `_.set`.
 *
 * @private
 * @param {Object} object The object to modify.
 * @param {Array|string} path The path of the property to set.
 * @param {*} value The value to set.
 * @param {Function} [customizer] The function to customize path creation.
 * @returns {Object} Returns `object`.
 */
function baseSet(object, path, value, customizer) {
  if (!isObject(object)) {
    return object;
  }
  path = castPath(path, object);

  var index = -1,
      length = path.length,
      lastIndex = length - 1,
      nested = object;

  while (nested != null && ++index < length) {
    var key = toKey(path[index]),
        newValue = value;

    if (index != lastIndex) {
      var objValue = nested[key];
      newValue = customizer ? customizer(objValue, key, nested) : undefined;
      if (newValue === undefined) {
        newValue = isObject(objValue)
          ? objValue
          : (isIndex(path[index + 1]) ? [] : {});
      }
    }
    assignValue(nested, key, newValue);
    nested = nested[key];
  }
  return object;
}

/**
 * The base implementation of  `_.pickBy` without support for iteratee shorthands.
 *
 * @private
 * @param {Object} object The source object.
 * @param {string[]} paths The property paths to pick.
 * @param {Function} predicate The function invoked per property.
 * @returns {Object} Returns the new object.
 */
function basePickBy(object, paths, predicate) {
  var index = -1,
      length = paths.length,
      result = {};

  while (++index < length) {
    var path = paths[index],
        value = baseGet(object, path);

    if (predicate(value, path)) {
      baseSet(result, castPath(path, object), value);
    }
  }
  return result;
}

/**
 * Creates an object composed of the `object` properties `predicate` returns
 * truthy for. The predicate is invoked with two arguments: (value, key).
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Object
 * @param {Object} object The source object.
 * @param {Function} [predicate=_.identity] The function invoked per property.
 * @returns {Object} Returns the new object.
 * @example
 *
 * var object = { 'a': 1, 'b': '2', 'c': 3 };
 *
 * _.pickBy(object, _.isNumber);
 * // => { 'a': 1, 'c': 3 }
 */
function pickBy(object, predicate) {
  if (object == null) {
    return {};
  }
  var props = arrayMap(getAllKeysIn(object), function(prop) {
    return [prop];
  });
  predicate = baseIteratee(predicate);
  return basePickBy(object, props, function(value, path) {
    return predicate(value, path[0]);
  });
}

/**
 * The opposite of `_.pickBy`; this method creates an object composed of
 * the own and inherited enumerable string keyed properties of `object` that
 * `predicate` doesn't return truthy for. The predicate is invoked with two
 * arguments: (value, key).
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Object
 * @param {Object} object The source object.
 * @param {Function} [predicate=_.identity] The function invoked per property.
 * @returns {Object} Returns the new object.
 * @example
 *
 * var object = { 'a': 1, 'b': '2', 'c': 3 };
 *
 * _.omitBy(object, _.isNumber);
 * // => { 'b': '2' }
 */
function omitBy(object, predicate) {
  return pickBy(object, negate(baseIteratee(predicate)));
}

/**
 * Used by `_.trim` and `_.trimEnd` to get the index of the last string symbol
 * that is not found in the character symbols.
 *
 * @private
 * @param {Array} strSymbols The string symbols to inspect.
 * @param {Array} chrSymbols The character symbols to find.
 * @returns {number} Returns the index of the last unmatched string symbol.
 */
function charsEndIndex(strSymbols, chrSymbols) {
  var index = strSymbols.length;

  while (index-- && baseIndexOf(chrSymbols, strSymbols[index], 0) > -1) {}
  return index;
}

/**
 * Used by `_.trim` and `_.trimStart` to get the index of the first string symbol
 * that is not found in the character symbols.
 *
 * @private
 * @param {Array} strSymbols The string symbols to inspect.
 * @param {Array} chrSymbols The character symbols to find.
 * @returns {number} Returns the index of the first unmatched string symbol.
 */
function charsStartIndex(strSymbols, chrSymbols) {
  var index = -1,
      length = strSymbols.length;

  while (++index < length && baseIndexOf(chrSymbols, strSymbols[index], 0) > -1) {}
  return index;
}

/** Used to match leading and trailing whitespace. */
var reTrim = /^\s+|\s+$/g;

/**
 * Removes leading and trailing whitespace or specified characters from `string`.
 *
 * @static
 * @memberOf _
 * @since 3.0.0
 * @category String
 * @param {string} [string=''] The string to trim.
 * @param {string} [chars=whitespace] The characters to trim.
 * @param- {Object} [guard] Enables use as an iteratee for methods like `_.map`.
 * @returns {string} Returns the trimmed string.
 * @example
 *
 * _.trim('  abc  ');
 * // => 'abc'
 *
 * _.trim('-_-abc-_-', '_-');
 * // => 'abc'
 *
 * _.map(['  foo  ', '  bar  '], _.trim);
 * // => ['foo', 'bar']
 */
function trim(string, chars, guard) {
  string = toString(string);
  if (string && (guard || chars === undefined)) {
    return string.replace(reTrim, '');
  }
  if (!string || !(chars = baseToString(chars))) {
    return string;
  }
  var strSymbols = stringToArray(string),
      chrSymbols = stringToArray(chars),
      start = charsStartIndex(strSymbols, chrSymbols),
      end = charsEndIndex(strSymbols, chrSymbols) + 1;

  return castSlice(strSymbols, start, end).join('');
}

/**
 * Check several parameter that there is something in the param
 * @param {*} param input
 * @return {boolean}
 */

function notEmpty (a) {
  if (isArray(a)) {
    return true;
  }
  return a !== undefined && a !== null && trim(a) !== '';
}

// validator numbers
/**
 * @2015-05-04 found a problem if the value is a number like string
 * it will pass, so add a check if it's string before we pass to next
 * @param {number} value expected value
 * @return {boolean} true if OK
 */
var checkIsNumber = function(value) {
  return isString(value) ? false : !isNaN( parseFloat(value) )
};

// validate string type
/**
 * @param {string} value expected value
 * @return {boolean} true if OK
 */
var checkIsString = function(value) {
  return (trim(value) !== '') ? isString(value) : false;
};

// check for boolean
/**
 * @param {boolean} value expected
 * @return {boolean} true if OK
 */
var checkIsBoolean = function(value) {
  return isBoolean(value);
};

// validate any thing only check if there is something
/**
 * @param {*} value the value
 * @param {boolean} [checkNull=true] strict check if there is null value
 * @return {boolean} true is OK
 */
var checkIsAny = function(value, checkNull) {
  if ( checkNull === void 0 ) checkNull = true;

  if (!isUndefined(value) && value !== '' && trim(value) !== '') {
    if (checkNull === false || (checkNull === true && !isNull(value))) {
      return true;
    }
  }
  return false;
};

// Good practice rule - No magic number

var ARGS_NOT_ARRAY_ERR = "args is not an array! You might want to do: ES6 Array.from(arguments) or ES5 Array.prototype.slice.call(arguments)";
var PARAMS_NOT_ARRAY_ERR = "params is not an array! Did something gone wrong when you generate the contract.json?";
var EXCEPTION_CASE_ERR = 'Could not understand your arguments and parameter structure!';
// @TODO the jsdoc return array.<type> and we should also allow array<type> syntax
var DEFAULT_TYPE$1 = DEFAULT_TYPE;
var ARRAY_TYPE_LFT$1 = ARRAY_TYPE_LFT;
var ARRAY_TYPE_RGT$1 = ARRAY_TYPE_RGT;

var TYPE_KEY$1 = TYPE_KEY;
var OPTIONAL_KEY$1 = OPTIONAL_KEY;
var ENUM_KEY$1 = ENUM_KEY;
var ARGS_KEY$1 = ARGS_KEY;
var CHECKER_KEY$1 = CHECKER_KEY;
var ALIAS_KEY$1 = ALIAS_KEY;

var ARRAY_TYPE$1 = ARRAY_TYPE;
var OBJECT_TYPE$1 = OBJECT_TYPE;
var STRING_TYPE$1 = STRING_TYPE;
var BOOLEAN_TYPE$1 = BOOLEAN_TYPE;
var NUMBER_TYPE$1 = NUMBER_TYPE;
var KEY_WORD$1 = KEY_WORD;
var OR_SEPERATOR$1 = OR_SEPERATOR;

// not actually in use
// export const NUMBER_TYPES = JSONQL_CONSTANTS.NUMBER_TYPES;

// primitive types

/**
 * this is a wrapper method to call different one based on their type
 * @param {string} type to check
 * @return {function} a function to handle the type
 */
var combineFn = function(type) {
  switch (type) {
    case NUMBER_TYPE$1:
      return checkIsNumber;
    case STRING_TYPE$1:
      return checkIsString;
    case BOOLEAN_TYPE$1:
      return checkIsBoolean;
    default:
      return checkIsAny;
  }
};

// validate array type

/**
 * @param {array} value expected
 * @param {string} [type=''] pass the type if we encounter array.<T> then we need to check the value as well
 * @return {boolean} true if OK
 */
var checkIsArray = function(value, type) {
  if ( type === void 0 ) type='';

  if (isArray(value)) {
    if (type === '' || trim(type)==='') {
      return true;
    }
    // we test it in reverse
    // @TODO if the type is an array (OR) then what?
    // we need to take into account this could be an array
    var c = value.filter(function (v) { return !combineFn(type)(v); });
    return !(c.length > 0)
  }
  return false;
};

/**
 * check if it matches the array.<T> pattern
 * @param {string} type
 * @return {boolean|array} false means NO, always return array
 */
var isArrayLike$1 = function(type) {
  // @TODO could that have something like array<> instead of array.<>? missing the dot?
  // because type script is Array<T> without the dot
  if (type.indexOf(ARRAY_TYPE_LFT$1) > -1 && type.indexOf(ARRAY_TYPE_RGT$1) > -1) {
    var _type = type.replace(ARRAY_TYPE_LFT$1, '').replace(ARRAY_TYPE_RGT$1, '');
    if (_type.indexOf(OR_SEPERATOR$1)) {
      return _type.split(OR_SEPERATOR$1)
    }
    return [_type]
  }
  return false;
};

/**
 * we might encounter something like array.<T> then we need to take it apart
 * @param {object} p the prepared object for processing
 * @param {string|array} type the type came from <T>
 * @return {boolean} for the filter to operate on
 */
var arrayTypeHandler = function(p, type) {
  var arg = p.arg;
  // need a special case to handle the OR type
  // we need to test the args instead of the type(s)
  if (type.length > 1) {
    return !arg.filter(function (v) { return (
      !(type.length > type.filter(function (t) { return !combineFn(t)(v); }).length)
    ); }).length;
  }
  // type is array so this will be or!
  return type.length > type.filter(function (t) { return !checkIsArray(arg, t); }).length;
};

// validate object type
/**
 * @TODO if provide with the keys then we need to check if the key:value type as well
 * @param {object} value expected
 * @param {array} [keys=null] if it has the keys array to compare as well
 * @return {boolean} true if OK
 */
var checkIsObject = function(value, keys) {
  if ( keys === void 0 ) keys=null;

  if (isPlainObject(value)) {
    if (!keys) {
      return true;
    }
    if (checkIsArray(keys)) {
      // please note we DON'T care if some is optional
      // plese refer to the contract.json for the keys
      return !keys.filter(function (key) {
        var _value = value[key.name];
        return !(key.type.length > key.type.filter(function (type) {
          var tmp;
          if (!isUndefined(_value)) {
            if ((tmp = isArrayLike$1(type)) !== false) {
              return !arrayTypeHandler({arg: _value}, tmp)
              // return tmp.filter(t => !checkIsArray(_value, t)).length;
              // @TODO there might be an object within an object with keys as well :S
            }
            return !combineFn(type)(_value)
          }
          return true;
        }).length)
      }).length;
    }
  }
  return false;
};

/**
 * fold this into it's own function to handler different object type
 * @param {object} p the prepared object for process
 * @return {boolean}
 */
var objectTypeHandler = function(p) {
  var arg = p.arg;
  var param = p.param;
  var _args = [arg];
  if (Array.isArray(param.keys) && param.keys.length) {
    _args.push(param.keys);
  }
  // just simple check
  return checkIsObject.apply(null, _args)
};

// move the index.js code here that make more sense to find where things are

// import debug from 'debug'
// const debugFn = debug('jsonql-params-validator:validator')
// also export this for use in other places

/**
 * We need to handle those optional parameter without a default value
 * @param {object} params from contract.json
 * @return {boolean} for filter operation false is actually OK
 */
var optionalHandler = function( params ) {
  var arg = params.arg;
  var param = params.param;
  if (notEmpty(arg)) {
    // debug('call optional handler', arg, params);
    // loop through the type in param
    return !(param.type.length > param.type.filter(function (type) { return validateHandler(type, params); }
    ).length)
  }
  return false;
};

/**
 * actually picking the validator
 * @param {*} type for checking
 * @param {*} value for checking
 * @return {boolean} true on OK
 */
var validateHandler = function(type, value) {
  var tmp;
  switch (true) {
    case type === OBJECT_TYPE$1:
      // debugFn('call OBJECT_TYPE')
      return !objectTypeHandler(value)
    case type === ARRAY_TYPE$1:
      // debugFn('call ARRAY_TYPE')
      return !checkIsArray(value.arg)
    // @TODO when the type is not present, it always fall through here
    // so we need to find a way to actually pre-check the type first
    // AKA check the contract.json map before running here
    case (tmp = isArrayLike$1(type)) !== false:
      // debugFn('call ARRAY_LIKE: %O', value)
      return !arrayTypeHandler(value, tmp)
    default:
      return !combineFn(type)(value.arg)
  }
};

/**
 * it get too longer to fit in one line so break it out from the fn below
 * @param {*} arg value
 * @param {object} param config
 * @return {*} value or apply default value
 */
var getOptionalValue = function(arg, param) {
  if (!isUndefined(arg)) {
    return arg;
  }
  return (param.optional === true && !isUndefined(param.defaultvalue) ? param.defaultvalue : null)
};

/**
 * padding the arguments with defaultValue if the arguments did not provide the value
 * this will be the name export
 * @param {array} args normalized arguments
 * @param {array} params from contract.json
 * @return {array} merge the two together
 */
var normalizeArgs = function(args, params) {
  // first we should check if this call require a validation at all
  // there will be situation where the function doesn't need args and params
  if (!checkIsArray(params)) {
    // debugFn('params value', params)
    throw new JsonqlError(PARAMS_NOT_ARRAY_ERR)
  }
  if (params.length === 0) {
    return [];
  }
  if (!checkIsArray(args)) {
    throw new JsonqlError(ARGS_NOT_ARRAY_ERR)
  }
  // debugFn(args, params);
  // fall through switch
  switch(true) {
    case args.length == params.length: // standard
      return args.map(function (arg, i) { return (
        {
          arg: arg,
          index: i,
          param: params[i]
        }
      ); });
    case params[0].variable === true: // using spread syntax
      var type = params[0].type;
      return args.map(function (arg, i) { return (
        {
          arg: arg,
          index: i, // keep the index for reference
          param: params[i] || { type: type, name: '_' }
        }
      ); });
    // with optional defaultValue parameters
    case args.length < params.length:
      return params.map(function (param, i) { return (
        {
          param: param,
          index: i,
          arg: getOptionalValue(args[i], param),
          optional: param.optional || false
        }
      ); });
    // this one pass more than it should have anything after the args.length will be cast as any type
    case args.length > params.length && params.length === 1:
      // this happens when we have those array.<number> type
      var tmp, _type = [ DEFAULT_TYPE$1 ];
      // we only looking at the first one, this might be a @BUG!
      if ((tmp = isArrayLike$1(params[0].type[0])) !== false) {
        _type = tmp;
      }
      // if not then we fall back to the following
      return args.map(function (arg, i) { return (
        {
          arg: arg,
          index: i,
          param: params[i] || { type: _type, name: '_' }
        }
      ); });
    // @TODO find out if there is more cases not cover
    default: // this should never happen
      // debugFn('args', args)
      // debugFn('params', params)
      // this is unknown therefore we just throw it!
      throw new JsonqlError(EXCEPTION_CASE_ERR, { args: args, params: params })
  }
};

// what we want is after the validaton we also get the normalized result
// which is with the optional property if the argument didn't provide it
/**
 * process the array of params back to their arguments
 * @param {array} result the params result
 * @return {array} arguments
 */
var processReturn = function (result) { return result.map(function (r) { return r.arg; }); };

/**
 * validator main interface
 * @param {array} args the arguments pass to the method call
 * @param {array} params from the contract for that method
 * @param {boolean} [withResul=false] if true then this will return the normalize result as well
 * @return {array} empty array on success, or failed parameter and reasons
 */
var validateSync = function(args, params, withResult) {
  var obj;

  if ( withResult === void 0 ) withResult = false;
  var cleanArgs = normalizeArgs(args, params);
  var checkResult = cleanArgs.filter(function (p) {
    if (p.param.optional === true) {
      return optionalHandler(p)
    }
    // because array of types means OR so if one pass means pass
    return !(p.param.type.length > p.param.type.filter(
      function (type) { return validateHandler(type, p); }
    ).length)
  });
  // using the same convention we been using all this time
  return !withResult ? checkResult : ( obj = {}, obj[ERROR_KEY] = checkResult, obj[DATA_KEY] = processReturn(cleanArgs), obj )
};

/**
 * A wrapper method that return promise
 * @param {array} args arguments
 * @param {array} params from contract.json
 * @param {boolean} [withResul=false] if true then this will return the normalize result as well
 * @return {object} promise.then or catch
 */
var validateAsync = function(args, params, withResult) {
  if ( withResult === void 0 ) withResult = false;

  return new Promise(function (resolver, rejecter) {
    var result = validateSync(args, params, withResult);
    if (withResult) {
      return result[ERROR_KEY].length ? rejecter(result[ERROR_KEY])
                                      : resolver(result[DATA_KEY])
    }
    // the different is just in the then or catch phrase
    return result.length ? rejecter(result) : resolver([])
  })
};

/**
 * @param {array} arr Array for check
 * @param {*} value target
 * @return {boolean} true on successs
 */
var isInArray = function(arr, value) {
  return !!arr.filter(function (a) { return a === value; }).length;
};

/**
 * @param {object} obj for search
 * @param {string} key target
 * @return {boolean} true on success
 */
var checkKeyInObject = function(obj, key) {
  var keys = Object.keys(obj);
  return isInArray(keys, key)
};

// import debug from 'debug';
// const debugFn = debug('jsonql-params-validator:options:prepare')

// just not to make my head hurt
var isEmpty = function (value) { return !notEmpty(value); };

/**
 * Map the alias to their key then grab their value over
 * @param {object} config the user supplied config
 * @param {object} appProps the default option map
 * @return {object} the config keys replaced with the appProps key by the ALIAS
 */
function mapAliasConfigKeys(config, appProps) {
  // need to do two steps
  // 1. take key with alias key
  var aliasMap = omitBy(appProps, function (value, k) { return !value[ALIAS_KEY$1]; } );
  if (isEqual(aliasMap, {})) {
    return config;
  }
  return mapKeys(config, function (v, key) { return findKey(aliasMap, function (o) { return o.alias === key; }) || key; })
}

/**
 * We only want to run the valdiation against the config (user supplied) value
 * but keep the defaultOptions untouch
 * @param {object} config configuraton supplied by user
 * @param {object} appProps the default options map
 * @return {object} the pristine values that will add back to the final output
 */
function preservePristineValues(config, appProps) {
  // @BUG this will filter out those that is alias key
  // we need to first map the alias keys back to their full key
  var _config = mapAliasConfigKeys(config, appProps);
  // take the default value out
  var pristineValues = mapValues(
    omitBy(appProps, function (value, key) { return checkKeyInObject(_config, key); }),
    function (value) { return value.args; }
  );
  // for testing the value
  var checkAgainstAppProps = omitBy(appProps, function (value, key) { return !checkKeyInObject(_config, key); });
  // output
  return {
    pristineValues: pristineValues,
    checkAgainstAppProps: checkAgainstAppProps,
    config: _config // passing this correct values back
  }
}

/**
 * This will take the value that is ONLY need to check
 * @param {object} config that one
 * @param {object} props map for creating checking
 * @return {object} put that arg into the args
 */
function processConfigAction(config, props) {
  // debugFn('processConfigAction', props)
  // v.1.2.0 add checking if its mark optional and the value is empty then pass
  return mapValues(props, function (value, key) {
    var obj, obj$1;

    return (
    isUndefined(config[key]) || (value[OPTIONAL_KEY$1] === true && isEmpty(config[key]))
      ? merge({}, value, ( obj = {}, obj[KEY_WORD$1] = true, obj ))
      : ( obj$1 = {}, obj$1[ARGS_KEY$1] = config[key], obj$1[TYPE_KEY$1] = value[TYPE_KEY$1], obj$1[OPTIONAL_KEY$1] = value[OPTIONAL_KEY$1] || false, obj$1[ENUM_KEY$1] = value[ENUM_KEY$1] || false, obj$1[CHECKER_KEY$1] = value[CHECKER_KEY$1] || false, obj$1 )
    );
  }
  )
}

/**
 * Quick transform
 * @TODO we should only validate those that is pass from the config
 * and pass through those values that is from the defaultOptions
 * @param {object} opts that one
 * @param {object} appProps mutation configuration options
 * @return {object} put that arg into the args
 */
function prepareArgsForValidation(opts, appProps) {
  var ref = preservePristineValues(opts, appProps);
  var config = ref.config;
  var pristineValues = ref.pristineValues;
  var checkAgainstAppProps = ref.checkAgainstAppProps;
  // output
  return [
    processConfigAction(config, checkAgainstAppProps),
    pristineValues
  ]
}

// breaking the whole thing up to see what cause the multiple calls issue

// import debug from 'debug';
// const debugFn = debug('jsonql-params-validator:options:validation')

/**
 * just make sure it returns an array to use
 * @param {*} arg input
 * @return {array} output
 */
var toArray = function (arg) { return checkIsArray(arg) ? arg : [arg]; };

/**
 * DIY in array
 * @param {array} arr to check against
 * @param {*} value to check
 * @return {boolean} true on OK
 */
var inArray = function (arr, value) { return (
  !!arr.filter(function (v) { return v === value; }).length
); };

/**
 * break out to make the code easier to read
 * @param {object} value to process
 * @param {function} cb the validateSync
 * @return {array} empty on success
 */
function validateHandler$1(value, cb) {
  var obj;

  // cb is the validateSync methods
  var args = [
    [ value[ARGS_KEY$1] ],
    [( obj = {}, obj[TYPE_KEY$1] = toArray(value[TYPE_KEY$1]), obj[OPTIONAL_KEY$1] = value[OPTIONAL_KEY$1], obj )]
  ];
  // debugFn('validateHandler', args)
  return Reflect.apply(cb, null, args)
}

/**
 * Check against the enum value if it's provided
 * @param {*} value to check
 * @param {*} enumv to check against if it's not false
 * @return {boolean} true on OK
 */
var enumHandler = function (value, enumv) {
  if (checkIsArray(enumv)) {
    return inArray(enumv, value)
  }
  return true;
};

/**
 * Allow passing a function to check the value
 * There might be a problem here if the function is incorrect
 * and that will makes it hard to debug what is going on inside
 * @TODO there could be a few feature add to this one under different circumstance
 * @param {*} value to check
 * @param {function} checker for checking
 */
var checkerHandler = function (value, checker) {
  try {
    return isFunction(checker) ? checker.apply(null, [value]) : false;
  } catch (e) {
    return false;
  }
};

/**
 * Taken out from the runValidaton this only validate the required values
 * @param {array} args from the config2argsAction
 * @param {function} cb validateSync
 * @return {array} of configuration values
 */
function runValidationAction(cb) {
  return function (value, key) {
    // debugFn('runValidationAction', key, value)
    if (value[KEY_WORD$1]) {
      return value[ARGS_KEY$1]
    }
    var check = validateHandler$1(value, cb);
    if (check.length) {
      // debugFn('runValidationAction', key, value)
      throw new JsonqlTypeError(key, check)
    }
    if (value[ENUM_KEY$1] !== false && !enumHandler(value[ARGS_KEY$1], value[ENUM_KEY$1])) {
      throw new JsonqlEnumError(key)
    }
    if (value[CHECKER_KEY$1] !== false && !checkerHandler(value[ARGS_KEY$1], value[CHECKER_KEY$1])) {
      throw new JsonqlCheckerError(key)
    }
    return value[ARGS_KEY$1]
  }
}

/**
 * @param {object} args from the config2argsAction
 * @param {function} cb validateSync
 * @return {object} of configuration values
 */
function runValidation(args, cb) {
  var argsForValidate = args[0];
  var pristineValues = args[1];
  // turn the thing into an array and see what happen here
  // debugFn('_args', argsForValidate)
  var result = mapValues(argsForValidate, runValidationAction(cb));
  return merge(result, pristineValues)
}

// this is port back from the client to share across all projects

// import debug from 'debug'
// const debugFn = debug('jsonql-params-validator:check-options-async')

/**
 * Quick transform
 * @param {object} config that one
 * @param {object} appProps mutation configuration options
 * @return {object} put that arg into the args
 */
var configToArgs = function (config, appProps) {
  return Promise.resolve(
    prepareArgsForValidation(config, appProps)
  )
};

/**
 * @param {object} config user provide configuration option
 * @param {object} appProps mutation configuration options
 * @param {object} constProps the immutable configuration options
 * @param {function} cb the validateSync method
 * @return {object} Promise resolve merge config object
 */
function checkOptionsAsync(config, appProps, constProps, cb) {
  if ( config === void 0 ) config = {};

  return configToArgs(config, appProps)
    .then(function (args1) {
      // debugFn('args', args1)
      return runValidation(args1, cb)
    })
    // next if every thing good then pass to final merging
    .then(function (args2) { return merge({}, args2, constProps); })
}

// create function to construct the config entry so we don't need to keep building object
// import debug from 'debug';
// const debugFn = debug('jsonql-params-validator:construct-config');
/**
 * @param {*} args value
 * @param {string} type for value
 * @param {boolean} [optional=false]
 * @param {boolean|array} [enumv=false]
 * @param {boolean|function} [checker=false]
 * @return {object} config entry
 */
function constructConfigFn(args, type, optional, enumv, checker, alias) {
  if ( optional === void 0 ) optional=false;
  if ( enumv === void 0 ) enumv=false;
  if ( checker === void 0 ) checker=false;
  if ( alias === void 0 ) alias=false;

  var base = {};
  base[ARGS_KEY] = args;
  base[TYPE_KEY] = type;
  if (optional === true) {
    base[OPTIONAL_KEY] = true;
  }
  if (checkIsArray(enumv)) {
    base[ENUM_KEY] = enumv;
  }
  if (isFunction(checker)) {
    base[CHECKER_KEY] = checker;
  }
  if (isString(alias)) {
    base[ALIAS_KEY] = alias;
  }
  return base;
}

// export also create wrapper methods

// import debug from 'debug';
// const debugFn = debug('jsonql-params-validator:options:index');

/**
 * This has a different interface
 * @param {*} value to supply
 * @param {string|array} type for checking
 * @param {object} params to map against the config check
 * @param {array} params.enumv NOT enum
 * @param {boolean} params.optional false then nothing
 * @param {function} params.checker need more work on this one later
 * @param {string} params.alias mostly for cmd
 */
var createConfig = function (value, type, params) {
  if ( params === void 0 ) params = {};

  // Note the enumv not ENUM
  // const { enumv, optional, checker, alias } = params;
  // let args = [value, type, optional, enumv, checker, alias];
  var o = params[OPTIONAL_KEY];
  var e = params[ENUM_KEY];
  var c = params[CHECKER_KEY];
  var a = params[ALIAS_KEY];
  return constructConfigFn.apply(null,  [value, type, o, e, c, a])
};

/**
 * We recreate the method here to avoid the circlar import
 * @param {object} config user supply configuration
 * @param {object} appProps mutation options
 * @param {object} [constantProps={}] optional: immutation options
 * @return {object} all checked configuration
 */
var checkConfigAsync = function(validateSync) {
  return function(config, appProps, constantProps) {
    if ( constantProps === void 0 ) constantProps= {};

    return checkOptionsAsync(config, appProps, constantProps, validateSync)
  }
};

// since this need to use everywhere might as well include in the validator

function checkIsContract(contract) {
  return checkIsObject(contract)
  && (
    checkKeyInObject(contract, QUERY_NAME)
 || checkKeyInObject(contract, MUTATION_NAME)
 || checkKeyInObject(contract, SOCKET_NAME)
  )
}

// craete several helper function to construct / extract the payload

/**
 * Get name from the payload (ported back from jsonql-koa)
 * @param {*} payload to extract from
 * @return {string} name
 */
function getNameFromPayload(payload) {
  return Object.keys(payload)[0]
}

/**
 * @param {string} resolverName name of function
 * @param {array} [args=[]] from the ...args
 * @param {boolean} [jsonp = false] add v1.3.0 to koa
 * @return {object} formatted argument
 */
function createQuery(resolverName, args, jsonp) {
  var obj;

  if ( args === void 0 ) args = [];
  if ( jsonp === void 0 ) jsonp = false;
  if (checkIsString(resolverName) && checkIsArray(args)) {
    var payload =  {};
    payload[QUERY_ARG_NAME] = args;
    if (jsonp === true) {
      return payload;
    }
    return ( obj = {}, obj[resolverName] = payload, obj )
  }
  throw new JsonqlValidationError("[createQuery] expect resolverName to be string and args to be array!", { resolverName: resolverName, args: args })
}

// string version of the above
function createQueryStr(resolverName, args, jsonp) {
  if ( args === void 0 ) args = [];
  if ( jsonp === void 0 ) jsonp = false;

  return JSON.stringify(createQuery(resolverName, args, jsonp))
}

// export
var isString$1 = checkIsString;
var isArray$1 = checkIsArray;
var validateSync$1 = validateSync;
var validateAsync$1 = validateAsync;

var createConfig$1 = createConfig;

var checkConfigAsync$1 = checkConfigAsync(validateSync);

var isObjectHasKey = checkKeyInObject;

var isContract = checkIsContract;
var createQueryStr$1 = createQueryStr;
var getNameFromPayload$1 = getNameFromPayload;

/**
 * Try to normalize it to use between browser and node
 * @param {string} name for the debug output
 * @return {function} debug
 */
var getDebug = function (name) {
  if (debug$2) {
    return debug$2('jsonql-ws-client').extend(name)
  }
  return function () {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

    console.info.apply(null, [name].concat(args));
  }
};
try {
  if (window && window.localStorage) {
    localStorage.setItem('DEBUG', 'jsonql-ws-client*');
  }
} catch(e) {}

// since both the ws and io version are
var debugFn = getDebug('create-nsp-client');
/**
 * wrapper method to create a nsp without login
 * @param {string|boolean} namespace namespace url could be false
 * @param {object} opts configuration
 * @return {object} ws client instance
 */
var nspClient = function (namespace, opts) {
  var wssPath = opts.wssPath;
  var wsOptions = opts.wsOptions;
  var hostname = opts.hostname;
  var url = namespace ? [hostname, namespace].join('/') : wssPath;
  return opts.nspClient(url, wsOptions)
};

/**
 * wrapper method to create a nsp with token auth
 * @param {string} namespace namespace url
 * @param {object} opts configuration
 * @return {object} ws client instance
 */
var nspAuthClient = function (namespace, opts) {
  var wssPath = opts.wssPath;
  var token = opts.token;
  var wsOptions = opts.wsOptions;
  var hostname = opts.hostname;
  var url = namespace ? [hostname, namespace].join('/') : wssPath;
  return opts.nspAuthClient(url, token, wsOptions)
};

// constants

var SOCKET_IO = JS_WS_SOCKET_IO_NAME;
var WS = JS_WS_NAME;

var AVAILABLE_SERVERS = [SOCKET_IO, WS];

var SOCKET_NOT_DEFINE_ERR = 'socket is not define in the contract file!';

var MISSING_PROP_ERR = 'Missing property in contract!';

var EMIT_EVT = EMIT_REPLY_TYPE;

var UNKNOWN_RESULT = 'UKNNOWN RESULT!';

var MY_NAMESPACE = 'myNamespace';

/**
 * Got to make sure the connection order otherwise
 * it will hang
 * @param {object} nspSet contract
 * @param {string} publicNamespace like the name said
 * @return {array} namespaces in order
 */
function getNamespaceInOrder(nspSet, publicNamespace) {
  var names = []; // need to make sure the order!
  for (var namespace in nspSet) {
    if (namespace === publicNamespace) {
      names[1] = namespace;
    } else {
      names[0] = namespace;
    }
  }
  return names;
}

var obj, obj$1;
var debug = getDebug('check-options');

var fixWss = function (url, serverType) {
  // ws only allow ws:// path
  if (serverType===WS) {
    return url.replace('http://', 'ws://')
  }
  return url;
};

var getHostName = function () { return (
  [window.location.protocol, window.location.host].join('//')
); };

var constProps = {
  // this will be the switcher!
  nspClient: null,
  nspAuthClient: null,
  // contructed path
  wssPath: ''
};

var defaultOptions = {
  loginHandlerName: createConfig$1(ISSUER_NAME, [STRING_TYPE]),
  logoutHandlerName: createConfig$1(LOGOUT_NAME, [STRING_TYPE]),
  // we will use this for determine the socket.io client type as well
  useJwt: createConfig$1(false, [BOOLEAN_TYPE, STRING_TYPE]),
  hostname: createConfig$1(false, [STRING_TYPE]),
  namespace: createConfig$1(JSONQL_PATH, [STRING_TYPE]),
  wsOptions: createConfig$1({transports: ['websocket'], 'force new connection' : true}, [OBJECT_TYPE]),
  serverType: createConfig$1(SOCKET_IO, [STRING_TYPE], ( obj = {}, obj[ENUM_KEY] = AVAILABLE_SERVERS, obj )),
  // we require the contract already generated and pass here
  contract: createConfig$1({}, [OBJECT_TYPE], ( obj$1 = {}, obj$1[CHECKER_KEY] = isContract, obj$1 )),
  enableAuth: createConfig$1(false, [BOOLEAN_TYPE]),
  token: createConfig$1(false, [STRING_TYPE])
};
// export
function checkOptions(config) {
  return checkConfigAsync$1(config, defaultOptions, constProps)
    .then(function (opts) {
      if (!opts.hostname) {
        opts.hostname = getHostName();
      }
      // @TODO the contract now will supply the namespace information
      // and we need to use that to group the namespace call
      opts.wssPath = fixWss([opts.hostname, opts.namespace].join('/'), opts.serverType);

      debug('opts', opts);
      return opts;
    })
}

var NB_EVENT_SERVICE_PRIVATE_STORE = new WeakMap();
var NB_EVENT_SERVICE_PRIVATE_LAZY = new WeakMap();

/**
 * generate a 32bit hash based on the function.toString()
 * _from http://stackoverflow.com/questions/7616461/generate-a-hash-_from-string-in-javascript-jquery
 * @param {string} s the converted to string function
 * @return {string} the hashed function string
 */
function hashCode(s) {
	return s.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0)
}

// making all the functionality on it's own
// import { WatchClass } from './watch'

var SuspendClass = function SuspendClass() {
  // suspend, release and queue
  this.__suspend__ = null;
  this.queueStore = new Set();
  /*
  this.watch('suspend', function(value, prop, oldValue) {
    this.logger(`${prop} set from ${oldValue} to ${value}`)
    // it means it set the suspend = true then release it
    if (oldValue === true && value === false) {
      // we want this happen after the return happens
      setTimeout(() => {
        this.release()
      }, 1)
    }
    return value; // we need to return the value to store it
  })
  */
};

var prototypeAccessors = { $suspend: { configurable: true },$queues: { configurable: true } };

/**
 * setter to set the suspend and check if it's boolean value
 * @param {boolean} value to trigger
 */
prototypeAccessors.$suspend.set = function (value) {
    var this$1 = this;

  if (typeof value === 'boolean') {
    var lastValue = this.__suspend__;
    this.__suspend__ = value;
    this.logger('($suspend)', ("Change from " + lastValue + " --> " + value));
    if (lastValue === true && value === false) {
      setTimeout(function () {
        this$1.release();
      }, 1);
    }
  } else {
    throw new Error("$suspend only accept Boolean value!")
  }
};

/**
 * queuing call up when it's in suspend mode
 * @param {any} value
 * @return {Boolean} true when added or false when it's not
 */
SuspendClass.prototype.$queue = function $queue () {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

  if (this.__suspend__ === true) {
    this.logger('($queue)', 'added to $queue', args);
    // there shouldn't be any duplicate ...
    this.queueStore.add(args);
  }
  return this.__suspend__;
};

/**
 * a getter to get all the store queue
 * @return {array} Set turn into Array before return
 */
prototypeAccessors.$queues.get = function () {
  var size = this.queueStore.size;
  this.logger('($queues)', ("size: " + size));
  if (size > 0) {
    return Array.from(this.queueStore)
  }
  return []
};

/**
 * Release the queue
 * @return {int} size if any
 */
SuspendClass.prototype.release = function release () {
    var this$1 = this;

  var size = this.queueStore.size;
  this.logger('(release)', ("Release was called " + size));
  if (size > 0) {
    var queue = Array.from(this.queueStore);
    this.queueStore.clear();
    this.logger('queue', queue);
    queue.forEach(function (args) {
      this$1.logger(args);
      Reflect.apply(this$1.$trigger, this$1, args);
    });
    this.logger(("Release size " + (this.queueStore.size)));
  }
};

Object.defineProperties( SuspendClass.prototype, prototypeAccessors );

// break up the main file because its getting way too long

var NbEventServiceBase = /*@__PURE__*/(function (SuspendClass) {
  function NbEventServiceBase(config) {
    if ( config === void 0 ) config = {};

    SuspendClass.call(this);
    if (config.logger && typeof config.logger === 'function') {
      this.logger = config.logger;
    }
    this.keep = config.keep;
    // for the $done setter
    this.result = config.keep ? [] : null;
    // we need to init the store first otherwise it could be a lot of checking later
    this.normalStore = new Map();
    this.lazyStore = new Map();
  }

  if ( SuspendClass ) NbEventServiceBase.__proto__ = SuspendClass;
  NbEventServiceBase.prototype = Object.create( SuspendClass && SuspendClass.prototype );
  NbEventServiceBase.prototype.constructor = NbEventServiceBase;

  var prototypeAccessors = { normalStore: { configurable: true },lazyStore: { configurable: true } };

  /**
   * validate the event name(s)
   * @param {string[]} evt event name
   * @return {boolean} true when OK
   */
  NbEventServiceBase.prototype.validateEvt = function validateEvt () {
    var this$1 = this;
    var evt = [], len = arguments.length;
    while ( len-- ) evt[ len ] = arguments[ len ];

    evt.forEach(function (e) {
      if (typeof e !== 'string') {
        this$1.logger('(validateEvt)', e);
        throw new Error("event name must be string type!")
      }
    });
    return true;
  };

  /**
   * Simple quick check on the two main parameters
   * @param {string} evt event name
   * @param {function} callback function to call
   * @return {boolean} true when OK
   */
  NbEventServiceBase.prototype.validate = function validate (evt, callback) {
    if (this.validateEvt(evt)) {
      if (typeof callback === 'function') {
        return true;
      }
    }
    throw new Error("callback required to be function type!")
  };

  /**
   * Check if this type is correct or not added in V1.5.0
   * @param {string} type for checking
   * @return {boolean} true on OK
   */
  NbEventServiceBase.prototype.validateType = function validateType (type) {
    var types = ['on', 'only', 'once', 'onlyOnce'];
    return !!types.filter(function (t) { return type === t; }).length;
  };

  /**
   * Run the callback
   * @param {function} callback function to execute
   * @param {array} payload for callback
   * @param {object} ctx context or null
   * @return {void} the result store in $done
   */
  NbEventServiceBase.prototype.run = function run (callback, payload, ctx) {
    this.logger('(run)', callback, payload, ctx);
    this.$done = Reflect.apply(callback, ctx, this.toArray(payload));
  };

  /**
   * Take the content out and remove it from store id by the name
   * @param {string} evt event name
   * @param {string} [storeName = lazyStore] name of store
   * @return {object|boolean} content or false on not found
   */
  NbEventServiceBase.prototype.takeFromStore = function takeFromStore (evt, storeName) {
    if ( storeName === void 0 ) storeName = 'lazyStore';

    var store = this[storeName]; // it could be empty at this point
    if (store) {
      this.logger('(takeFromStore)', storeName, store);
      if (store.has(evt)) {
        var content = store.get(evt);
        this.logger('(takeFromStore)', ("has " + evt), content);
        store.delete(evt);
        return content;
      }
      return false;
    }
    throw new Error((storeName + " is not supported!"))
  };

  /**
   * The add to store step is similar so make it generic for resuse
   * @param {object} store which store to use
   * @param {string} evt event name
   * @param {spread} args because the lazy store and normal store store different things
   * @return {array} store and the size of the store
   */
  NbEventServiceBase.prototype.addToStore = function addToStore (store, evt) {
    var args = [], len = arguments.length - 2;
    while ( len-- > 0 ) args[ len ] = arguments[ len + 2 ];

    var fnSet;
    if (store.has(evt)) {
      this.logger('(addToStore)', (evt + " existed"));
      fnSet = store.get(evt);
    } else {
      this.logger('(addToStore)', ("create new Set for " + evt));
      // this is new
      fnSet = new Set();
    }
    // lazy only store 2 items - this is not the case in V1.6.0 anymore
    // we need to check the first parameter is string or not
    if (args.length > 2) {
      if (Array.isArray(args[0])) { // lazy store
        // check if this type of this event already register in the lazy store
        var t = args[2];
        if (!this.checkTypeInLazyStore(evt, t)) {
          fnSet.add(args);
        }
      } else {
        if (!this.checkContentExist(args, fnSet)) {
          this.logger('(addToStore)', "insert new", args);
          fnSet.add(args);
        }
      }
    } else { // add straight to lazy store
      fnSet.add(args);
    }
    store.set(evt, fnSet);
    return [store, fnSet.size]
  };

  /**
   * @param {array} args for compare
   * @param {object} fnSet A Set to search from
   * @return {boolean} true on exist
   */
  NbEventServiceBase.prototype.checkContentExist = function checkContentExist (args, fnSet) {
    var list = Array.from(fnSet);
    return !!list.filter(function (l) {
      var hash = l[0];
      if (hash === args[0]) {
        return true;
      }
      return false;
    }).length;
  };

  /**
   * get the existing type to make sure no mix type add to the same store
   * @param {string} evtName event name
   * @param {string} type the type to check
   * @return {boolean} true you can add, false then you can't add this type
   */
  NbEventServiceBase.prototype.checkTypeInStore = function checkTypeInStore (evtName, type) {
    this.validateEvt(evtName, type);
    var all = this.$get(evtName, true);
    if (all === false) {
       // pristine it means you can add
      return true;
    }
    // it should only have ONE type in ONE event store
    return !all.filter(function (list) {
      var t = list[3];
      return type !== t;
    }).length;
  };

  /**
   * This is checking just the lazy store because the structure is different
   * therefore we need to use a new method to check it
   */
  NbEventServiceBase.prototype.checkTypeInLazyStore = function checkTypeInLazyStore (evtName, type) {
    this.validateEvt(evtName, type);
    var store = this.lazyStore.get(evtName);
    this.logger('(checkTypeInLazyStore)', store);
    if (store) {
      return !!Array
        .from(store)
        .filter(function (l) {
          var t = l[2];
          return t !== type;
        }).length
    }
    return false;
  };

  /**
   * wrapper to re-use the addToStore,
   * V1.3.0 add extra check to see if this type can add to this evt
   * @param {string} evt event name
   * @param {string} type on or once
   * @param {function} callback function
   * @param {object} context the context the function execute in or null
   * @return {number} size of the store
   */
  NbEventServiceBase.prototype.addToNormalStore = function addToNormalStore (evt, type, callback, context) {
    if ( context === void 0 ) context = null;

    this.logger('(addToNormalStore)', evt, type, 'try to add to normal store');
    // @TODO we need to check the existing store for the type first!
    if (this.checkTypeInStore(evt, type)) {
      this.logger('(addToNormalStore)', (type + " can add to " + evt + " normal store"));
      var key = this.hashFnToKey(callback);
      var args = [this.normalStore, evt, key, callback, context, type];
      var ref = Reflect.apply(this.addToStore, this, args);
      var _store = ref[0];
      var size = ref[1];
      this.normalStore = _store;
      return size;
    }
    return false;
  };

  /**
   * Add to lazy store this get calls when the callback is not register yet
   * so we only get a payload object or even nothing
   * @param {string} evt event name
   * @param {array} payload of arguments or empty if there is none
   * @param {object} [context=null] the context the callback execute in
   * @param {string} [type=false] register a type so no other type can add to this evt
   * @return {number} size of the store
   */
  NbEventServiceBase.prototype.addToLazyStore = function addToLazyStore (evt, payload, context, type) {
    if ( payload === void 0 ) payload = [];
    if ( context === void 0 ) context = null;
    if ( type === void 0 ) type = false;

    // this is add in V1.6.0
    // when there is type then we will need to check if this already added in lazy store
    // and no other type can add to this lazy store
    var args = [this.lazyStore, evt, this.toArray(payload), context];
    if (type) {
      args.push(type);
    }
    var ref = Reflect.apply(this.addToStore, this, args);
    var _store = ref[0];
    var size = ref[1];
    this.lazyStore = _store;
    return size;
  };

  /**
   * make sure we store the argument correctly
   * @param {*} arg could be array
   * @return {array} make sured
   */
  NbEventServiceBase.prototype.toArray = function toArray (arg) {
    return Array.isArray(arg) ? arg : [arg];
  };

  /**
   * setter to store the Set in private
   * @param {object} obj a Set
   */
  prototypeAccessors.normalStore.set = function (obj) {
    NB_EVENT_SERVICE_PRIVATE_STORE.set(this, obj);
  };

  /**
   * @return {object} Set object
   */
  prototypeAccessors.normalStore.get = function () {
    return NB_EVENT_SERVICE_PRIVATE_STORE.get(this)
  };

  /**
   * setter to store the Set in lazy store
   * @param {object} obj a Set
   */
  prototypeAccessors.lazyStore.set = function (obj) {
    NB_EVENT_SERVICE_PRIVATE_LAZY.set(this , obj);
  };

  /**
   * @return {object} the lazy store Set
   */
  prototypeAccessors.lazyStore.get = function () {
    return NB_EVENT_SERVICE_PRIVATE_LAZY.get(this)
  };

  /**
   * generate a hashKey to identify the function call
   * The build-in store some how could store the same values!
   * @param {function} fn the converted to string function
   * @return {string} hashKey
   */
  NbEventServiceBase.prototype.hashFnToKey = function hashFnToKey (fn) {
    return hashCode(fn.toString()) + '';
  };

  Object.defineProperties( NbEventServiceBase.prototype, prototypeAccessors );

  return NbEventServiceBase;
}(SuspendClass));

// The top level
// export
var EventService = /*@__PURE__*/(function (NbStoreService) {
  function EventService(config) {
    if ( config === void 0 ) config = {};

    NbStoreService.call(this, config);
  }

  if ( NbStoreService ) EventService.__proto__ = NbStoreService;
  EventService.prototype = Object.create( NbStoreService && NbStoreService.prototype );
  EventService.prototype.constructor = EventService;

  var prototypeAccessors = { $done: { configurable: true } };

  /**
   * logger function for overwrite
   */
  EventService.prototype.logger = function logger () {};

  //////////////////////////
  //    PUBLIC METHODS    //
  //////////////////////////

  /**
   * Register your evt handler, note we don't check the type here,
   * we expect you to be sensible and know what you are doing.
   * @param {string} evt name of event
   * @param {function} callback bind method --> if it's array or not
   * @param {object} [context=null] to execute this call in
   * @return {number} the size of the store
   */
  EventService.prototype.$on = function $on (evt , callback , context) {
    var this$1 = this;
    if ( context === void 0 ) context = null;

    var type = 'on';
    this.validate(evt, callback);
    // first need to check if this evt is in lazy store
    var lazyStoreContent = this.takeFromStore(evt);
    // this is normal register first then call later
    if (lazyStoreContent === false) {
      this.logger('($on)', (evt + " callback is not in lazy store"));
      // @TODO we need to check if there was other listener to this
      // event and are they the same type then we could solve that
      // register the different type to the same event name

      return this.addToNormalStore(evt, type, callback, context)
    }
    this.logger('($on)', (evt + " found in lazy store"));
    // this is when they call $trigger before register this callback
    var size = 0;
    lazyStoreContent.forEach(function (content) {
      var payload = content[0];
      var ctx = content[1];
      var t = content[2];
      if (t && t !== type) {
        throw new Error(("You are trying to register an event already been taken by other type: " + t))
      }
      this$1.run(callback, payload, context || ctx);
      size += this$1.addToNormalStore(evt, type, callback, context || ctx);
    });
    return size;
  };

  /**
   * once only registered it once, there is no overwrite option here
   * @NOTE change in v1.3.0 $once can add multiple listeners
   *       but once the event fired, it will remove this event (see $only)
   * @param {string} evt name
   * @param {function} callback to execute
   * @param {object} [context=null] the handler execute in
   * @return {boolean} result
   */
  EventService.prototype.$once = function $once (evt , callback , context) {
    if ( context === void 0 ) context = null;

    this.validate(evt, callback);
    var type = 'once';
    var lazyStoreContent = this.takeFromStore(evt);
    // this is normal register before call $trigger
    var nStore = this.normalStore;
    if (lazyStoreContent === false) {
      this.logger('($once)', (evt + " not in the lazy store"));
      // v1.3.0 $once now allow to add multiple listeners
      return this.addToNormalStore(evt, type, callback, context)
    } else {
      // now this is the tricky bit
      // there is a potential bug here that cause by the developer
      // if they call $trigger first, the lazy won't know it's a once call
      // so if in the middle they register any call with the same evt name
      // then this $once call will be fucked - add this to the documentation
      this.logger('($once)', lazyStoreContent);
      var list = Array.from(lazyStoreContent);
      // should never have more than 1
      var ref = list[0];
      var payload = ref[0];
      var ctx = ref[1];
      var t = ref[2];
      if (t && t !== type) {
        throw new Error(("You are trying to register an event already been taken by other type: " + t))
      }
      this.run(callback, payload, context || ctx);
      // remove this evt from store
      this.$off(evt);
    }
  };

  /**
   * This one event can only bind one callbackback
   * @param {string} evt event name
   * @param {function} callback event handler
   * @param {object} [context=null] the context the event handler execute in
   * @return {boolean} true bind for first time, false already existed
   */
  EventService.prototype.$only = function $only (evt, callback, context) {
    var this$1 = this;
    if ( context === void 0 ) context = null;

    this.validate(evt, callback);
    var type = 'only';
    var added = false;
    var lazyStoreContent = this.takeFromStore(evt);
    // this is normal register before call $trigger
    var nStore = this.normalStore;
    if (!nStore.has(evt)) {
      this.logger("($only)", (evt + " add to store"));
      added = this.addToNormalStore(evt, type, callback, context);
    }
    if (lazyStoreContent !== false) {
      // there are data store in lazy store
      this.logger('($only)', (evt + " found data in lazy store to execute"));
      var list = Array.from(lazyStoreContent);
      // $only allow to trigger this multiple time on the single handler
      list.forEach( function (l) {
        var payload = l[0];
        var ctx = l[1];
        var t = l[2];
        if (t && t !== type) {
          throw new Error(("You are trying to register an event already been taken by other type: " + t))
        }
        this$1.run(callback, payload, context || ctx);
      });
    }
    return added;
  };

  /**
   * $only + $once this is because I found a very subtile bug when we pass a
   * resolver, rejecter - and it never fire because that's OLD adeed in v1.4.0
   * @param {string} evt event name
   * @param {function} callback to call later
   * @param {object} [context=null] exeucte context
   * @return {void}
   */
  EventService.prototype.$onlyOnce = function $onlyOnce (evt, callback, context) {
    if ( context === void 0 ) context = null;

    this.validate(evt, callback);
    var type = 'onlyOnce';
    var added = false;
    var lazyStoreContent = this.takeFromStore(evt);
    // this is normal register before call $trigger
    var nStore = this.normalStore;
    if (!nStore.has(evt)) {
      this.logger("($onlyOnce)", (evt + " add to store"));
      added = this.addToNormalStore(evt, type, callback, context);
    }
    if (lazyStoreContent !== false) {
      // there are data store in lazy store
      this.logger('($onlyOnce)', lazyStoreContent);
      var list = Array.from(lazyStoreContent);
      // should never have more than 1
      var ref = list[0];
      var payload = ref[0];
      var ctx = ref[1];
      var t = ref[2];
      if (t && t !== 'onlyOnce') {
        throw new Error(("You are trying to register an event already been taken by other type: " + t))
      }
      this.run(callback, payload, context || ctx);
      // remove this evt from store
      this.$off(evt);
    }
    return added;
  };

  /**
   * This is a shorthand of $off + $on added in V1.5.0
   * @param {string} evt event name
   * @param {function} callback to exeucte
   * @param {object} [context = null] or pass a string as type
   * @param {string} [type=on] what type of method to replace
   * @return {}
   */
  EventService.prototype.$replace = function $replace (evt, callback, context, type) {
    if ( context === void 0 ) context = null;
    if ( type === void 0 ) type = 'on';

    if (this.validateType(type)) {
      this.$off(evt);
      var method = this['$' + type];
      return Reflect.apply(method, this, [evt, callback, context])
    }
    throw new Error((type + " is not supported!"))
  };

  /**
   * trigger the event
   * @param {string} evt name NOT allow array anymore!
   * @param {mixed} [payload = []] pass to fn
   * @param {object|string} [context = null] overwrite what stored
   * @param {string} [type=false] if pass this then we need to add type to store too
   * @return {number} if it has been execute how many times
   */
  EventService.prototype.$trigger = function $trigger (evt , payload , context, type) {
    if ( payload === void 0 ) payload = [];
    if ( context === void 0 ) context = null;
    if ( type === void 0 ) type = false;

    this.validateEvt(evt);
    var found = 0;
    // first check the normal store
    var nStore = this.normalStore;
    this.logger('($trigger)', 'normalStore', nStore);
    if (nStore.has(evt)) {
      // @1.8.0 to add the suspend queue
      var added = this.$queue(evt, payload, context, type);
      this.logger('($trigger)', evt, 'found; add to queue: ', added);
      if (added === true) {
        return false; // not executed
      }
      var nSet = Array.from(nStore.get(evt));
      var ctn = nSet.length;
      var hasOnce = false;
      for (var i=0; i < ctn; ++i) {
        ++found;
        // this.logger('found', found)
        var ref = nSet[i];
        var _ = ref[0];
        var callback = ref[1];
        var ctx = ref[2];
        var type$1 = ref[3];
        this.run(callback, payload, context || ctx);
        if (type$1 === 'once' || type$1 === 'onlyOnce') {
          hasOnce = true;
        }
      }
      if (hasOnce) {
        nStore.delete(evt);
      }
      return found;
    }
    // now this is not register yet
    this.addToLazyStore(evt, payload, context, type);
    return found;
  };

  /**
   * this is an alias to the $trigger
   * @NOTE breaking change in V1.6.0 we swap the parameter around
   * @param {string} evt event name
   * @param {*} params pass to the callback
   * @param {string} type of call
   * @param {object} context what context callback execute in
   * @return {*} from $trigger
   */
  EventService.prototype.$call = function $call (evt, params, type, context) {
    if ( type === void 0 ) type = false;
    if ( context === void 0 ) context = null;

    var args = [evt, params];
    args.push(context, type);
    return Reflect.apply(this.$trigger, this, args)
  };

  /**
   * remove the evt from all the stores
   * @param {string} evt name
   * @return {boolean} true actually delete something
   */
  EventService.prototype.$off = function $off (evt) {
    this.validateEvt(evt);
    var stores = [ this.lazyStore, this.normalStore ];
    var found = false;
    stores.forEach(function (store) {
      if (store.has(evt)) {
        found = true;
        store.delete(evt);
      }
    });
    return found;
  };

  /**
   * return all the listener from the event
   * @param {string} evtName event name
   * @param {boolean} [full=false] if true then return the entire content
   * @return {array|boolean} listerner(s) or false when not found
   */
  EventService.prototype.$get = function $get (evt, full) {
    if ( full === void 0 ) full = false;

    this.validateEvt(evt);
    var store = this.normalStore;
    if (store.has(evt)) {
      return Array
        .from(store.get(evt))
        .map( function (l) {
          if (full) {
            return l;
          }
          var key = l[0];
          var callback = l[1];
          return callback;
        })
    }
    return false;
  };

  /**
   * store the return result from the run
   * @param {*} value whatever return from callback
   */
  prototypeAccessors.$done.set = function (value) {
    this.logger('($done)', 'value: ', value);
    if (this.keep) {
      this.result.push(value);
    } else {
      this.result = value;
    }
  };

  /**
   * @TODO is there any real use with the keep prop?
   * getter for $done
   * @return {*} whatever last store result
   */
  prototypeAccessors.$done.get = function () {
    if (this.keep) {
      this.logger('(get $done)', this.result);
      return this.result[this.result.length - 1]
    }
    return this.result;
  };

  Object.defineProperties( EventService.prototype, prototypeAccessors );

  return EventService;
}(NbEventServiceBase));

// default

// create a clone version so we know which one we actually is using
var JsonqlWsEvt = /*@__PURE__*/(function (NBEventService) {
  function JsonqlWsEvt() {
    NBEventService.call(this, {logger: getDebug('nb-event-service')});
  }

  if ( NBEventService ) JsonqlWsEvt.__proto__ = NBEventService;
  JsonqlWsEvt.prototype = Object.create( NBEventService && NBEventService.prototype );
  JsonqlWsEvt.prototype.constructor = JsonqlWsEvt;

  var prototypeAccessors = { name: { configurable: true } };

  prototypeAccessors.name.get = function () {
    return 'jsonql-ws-client'
  };

  Object.defineProperties( JsonqlWsEvt.prototype, prototypeAccessors );

  return JsonqlWsEvt;
}(EventService));

// This is ported back from ws-server and it will get use in the server / client side

function extractSocketPart(contract) {
  if (isObjectHasKey(contract, 'socket')) {
    return contract.socket;
  }
  return contract;
}

/**
 * @BUG we should check the socket part instead of expect the downstream to read the menu!
 * We only need this when the enableAuth is true otherwise there is only one namespace
 * @param {object} contract the socket part of the contract file
 * @return {object} 1. remap the contract using the namespace --> resolvers
 * 2. the size of the object (1 all private, 2 mixed public with private)
 * 3. which namespace is public
 */
function groupByNamespace(contract) {
  var socket = extractSocketPart(contract);

  var nspSet = {};
  var size = 0;
  var publicNamespace;
  for (var resolverName in socket) {
    var params = socket[resolverName];
    var namespace = params.namespace;
    if (namespace) {
      if (!nspSet[namespace]) {
        ++size;
        nspSet[namespace] = {};
      }
      nspSet[namespace][resolverName] = params;
      if (!publicNamespace) {
        if (params.public) {
          publicNamespace = namespace;
        }
      }
    }
  }
  return { size: size, nspSet: nspSet, publicNamespace: publicNamespace }
}

// This is ported back from ws-client
// the idea if from https://decembersoft.com/posts/promises-in-serial-with-array-reduce/
/**
 * previously we already make sure the order of the namespaces
 * and attach the auth client to it
 * @param {array} promises array of unresolved promises
 * @return {object} promise resolved with the array of promises resolved results
 */
function chainPromises(promises) {
  return promises.reduce(function (promiseChain, currentTask) { return (
    promiseChain.then(function (chainResults) { return (
      currentTask.then(function (currentResult) { return (
        chainResults.concat( [currentResult])
      ); })
    ); })
  ); }, Promise.resolve([]))
}

/**
 * The code was extracted from:
 * https://github.com/davidchambers/Base64.js
 */

var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

function InvalidCharacterError(message) {
  this.message = message;
}

InvalidCharacterError.prototype = new Error();
InvalidCharacterError.prototype.name = 'InvalidCharacterError';

function polyfill (input) {
  var str = String(input).replace(/=+$/, '');
  if (str.length % 4 == 1) {
    throw new InvalidCharacterError("'atob' failed: The string to be decoded is not correctly encoded.");
  }
  for (
    // initialize result and counters
    var bc = 0, bs, buffer, idx = 0, output = '';
    // get next character
    buffer = str.charAt(idx++);
    // character found in table? initialize bit storage and add its ascii value;
    ~buffer && (bs = bc % 4 ? bs * 64 + buffer : buffer,
      // and if not first of each 4 characters,
      // convert the first 8 bits to one ascii character
      bc++ % 4) ? output += String.fromCharCode(255 & bs >> (-2 * bc & 6)) : 0
  ) {
    // try to find character in table (0-63, not found => -1)
    buffer = chars.indexOf(buffer);
  }
  return output;
}


var atob = typeof window !== 'undefined' && window.atob && window.atob.bind(window) || polyfill;

function InvalidTokenError(message) {
  this.message = message;
}

InvalidTokenError.prototype = new Error();
InvalidTokenError.prototype.name = 'InvalidTokenError';

var obj$2, obj$1$1, obj$2$1, obj$3, obj$4, obj$5, obj$6, obj$7, obj$8;

var appProps = {
  algorithm: createConfig$1(HSA_ALGO, [STRING_TYPE]),
  expiresIn: createConfig$1(false, [BOOLEAN_TYPE, NUMBER_TYPE, STRING_TYPE], ( obj$2 = {}, obj$2[ALIAS_KEY] = 'exp', obj$2[OPTIONAL_KEY] = true, obj$2 )),
  notBefore: createConfig$1(false, [BOOLEAN_TYPE, NUMBER_TYPE, STRING_TYPE], ( obj$1$1 = {}, obj$1$1[ALIAS_KEY] = 'nbf', obj$1$1[OPTIONAL_KEY] = true, obj$1$1 )),
  audience: createConfig$1(false, [BOOLEAN_TYPE, STRING_TYPE], ( obj$2$1 = {}, obj$2$1[ALIAS_KEY] = 'iss', obj$2$1[OPTIONAL_KEY] = true, obj$2$1 )),
  subject: createConfig$1(false, [BOOLEAN_TYPE, STRING_TYPE], ( obj$3 = {}, obj$3[ALIAS_KEY] = 'sub', obj$3[OPTIONAL_KEY] = true, obj$3 )),
  issuer: createConfig$1(false, [BOOLEAN_TYPE, STRING_TYPE], ( obj$4 = {}, obj$4[ALIAS_KEY] = 'iss', obj$4[OPTIONAL_KEY] = true, obj$4 )),
  noTimestamp: createConfig$1(false, [BOOLEAN_TYPE], ( obj$5 = {}, obj$5[OPTIONAL_KEY] = true, obj$5 )),
  header: createConfig$1(false, [BOOLEAN_TYPE, STRING_TYPE], ( obj$6 = {}, obj$6[OPTIONAL_KEY] = true, obj$6 )),
  keyid: createConfig$1(false, [BOOLEAN_TYPE, STRING_TYPE], ( obj$7 = {}, obj$7[OPTIONAL_KEY] = true, obj$7 )),
  mutatePayload: createConfig$1(false, [BOOLEAN_TYPE], ( obj$8 = {}, obj$8[OPTIONAL_KEY] = true, obj$8 ))
};

// ws client using native WebSocket

function getWS() {
  switch(true) {
    case (typeof WebSocket !== 'undefined'):
      return WebSocket;
    case (typeof MozWebSocket !== 'undefined'):
      return MozWebSocket;
    // case (typeof global !== 'undefined'):
    //  return global.WebSocket || global.MozWebSocket;
    case (typeof window !== 'undefined'):
      return window.WebSocket || window.MozWebSocket;
    // case (typeof self !== 'undefined'):
    //   return self.WebSocket || self.MozWebSocket;
    default:
      throw new JsonqlValidationError('WebSocket is NOT SUPPORTED!')
  }
}

var WS$1 = getWS();

// mapping the resolver to their respective nsp
var debug$1 = getDebug('process-contract');

/**
 * Just make sure the object contain what we are looking for
 * @param {object} opts configuration from checkOptions
 * @return {object} the target content
 */
var getResolverList = function (contract) {
  if (contract) {
    var socket = contract.socket;
    if (socket) {
      return socket;
    }
  }
  throw new JsonqlResolverNotFoundError(MISSING_PROP_ERR)
};

/**
 * process the contract first
 * @param {object} opts configuration
 * @return {object} sorted list
 */
function processContract(opts) {
  var obj;

  var contract = opts.contract;
  var enableAuth = opts.enableAuth;
  if (enableAuth) {
    return groupByNamespace(contract)
  }
  return {
    nspSet: ( obj = {}, obj[JSONQL_PATH] = getResolverList(contract), obj ),
    publicNamespace: JSONQL_PATH,
    size: 1 // this prop is pretty meaningless now
  }
}

// export the util methods

var toArray$1 = function (arg) { return isArray$1(arg) ? arg : [arg]; };

/**
 * very simple tool to create the event name
 * @param {string} [...args] spread
 * @return {string} join by _
 */
var createEvt = function () {
  var args = [], len = arguments.length;
  while ( len-- ) args[ len ] = arguments[ len ];

  return args.join('_');
};

/**
 * Unbind the event
 * @param {object} ee EventEmitter
 * @param {string} namespace
 * @return {void}
 */
var clearMainEmitEvt = function (ee, namespace) {
  var nsps = isArray$1(namespace) ? namespace : [namespace];
  nsps.forEach(function (n) {
    ee.$off(createEvt(n, EMIT_EVT));
  });
};

/**
 * @param {*} args arguments to send
 *@return {object} formatted payload
 */
var formatPayload = function (args) {
  var obj;

  return (
  ( obj = {}, obj[QUERY_ARG_NAME] = args, obj )
);
};

/**
 * @param {object} nsps namespace as key
 * @param {string} type of server
 */
var disconnect = function (nsps, type) {
  if ( type === void 0 ) type = JS_WS_SOCKET_IO_NAME;

  try {
    var method = type === JS_WS_SOCKET_IO_NAME ? 'disconnect' : 'terminate';
    for (var namespace in nsps) {
      var nsp = nsps[namespace];
      if (nsp && nsp[method]) {
        Reflect.apply(nsp[method], null, []);
      }
    }
  } catch(e) {
    // socket.io throw a this.destroy of undefined?
    console.error('disconnect', e);
  }
};

/**
 * trigger errors on all the namespace onError handler
 * @param {object} ee Event Emitter
 * @param {array} namespaces nsps string
 * @param {string} message optional
 * @return {void}
 */
function triggerNamespacesOnError(ee, namespaces, message) {
  namespaces.forEach( function (namespace) {
    ee.$call(createEvt(namespace, ON_ERROR_PROP_NAME), [{ message: message, namespace: namespace }]);
  });
}

// @TODO port what is in the ws-main-handler
var debugFn$1 = getDebug('client-event-handler');

/**
 * A fake ee handler
 * @param {string} namespace nsp
 * @param {object} ee EventEmitter
 * @return {void}
 */
var notLoginWsHandler = function (namespace, ee) {
  ee.$only(
    createEvt(namespace, EMIT_EVT),
    function(resolverName, args) {
      debugFn$1('noLoginHandler hijack the ws call', namespace, resolverName, args);
      var error = {
        message: NOT_LOGIN_ERR_MSG
      };
      // It should just throw error here and should not call the result
      // because that's channel for handling normal event not the fake one
      ee.$call(createEvt(namespace, resolverName, ON_ERROR_PROP_NAME), [error]);
      // also trigger the result handler, but wrap inside the error key
      ee.$call(createEvt(namespace, resolverName, ON_RESULT_PROP_NAME), [{ error: error }]);
    }
  );
};

/**
 * centralize all the comm in one place
 * @param {object} opts configuration
 * @param {array} namespaces namespace(s)
 * @param {object} ee Event Emitter instance
 * @param {function} bindWsHandler binding the ee to ws
 * @param {array} namespaces array of namespace available
 * @param {object} nsps namespaced nsp
 * @return {void} nothing
 */
function clientEventHandler(opts, nspMap, ee, bindWsHandler, namespaces, nsps) {
  // loop
  // @BUG for io this has to be in order the one with auth need to get call first
  // The order of login is very import we need to run a waterfall here to make sure
  // one is execute then the other
  namespaces.forEach(function (namespace) {
    if (nsps[namespace]) {
      debugFn$1('call bindWsHandler', namespace);
      var args = [namespace, nsps[namespace], ee];
      if (opts.serverType === SOCKET_IO) {
        var nspSet = nspMap.nspSet;
        args.push(nspSet[namespace]);
        args.push(opts);
      }
      Reflect.apply(bindWsHandler, null, args);
    } else {
      // a dummy placeholder
      notLoginWsHandler(namespace, ee);
    }
  });
  // this will be available regardless enableAuth
  // because the server can log the client out
  ee.$on(LOGOUT_EVENT_NAME, function() {
    debugFn$1('LOGOUT_EVENT_NAME');
    // disconnect(nsps, opts.serverType)
    // we need to issue error to all the namespace onError handler
    triggerNamespacesOnError(ee, namespaces, LOGOUT_EVENT_NAME);
    // rebind all of the handler to the fake one
    namespaces.forEach( function (namespace) {
      clearMainEmitEvt(ee, namespace);
      // clear out the nsp
      nsps[namespace] = false;
      // add a NOT LOGIN error if call
      notLoginWsHandler(namespace, ee);
    });
  });
}

// take the ws reply data for use
var debugFn$2 = getDebug('extract-ws-payload');

var keys$1 = [ WS_REPLY_TYPE, WS_EVT_NAME, WS_DATA_NAME ];

/**
 * @param {object} payload should be string when reply but could be transformed
 * @return {boolean} true is OK
 */
var isWsReply = function (payload) {
  var data = payload.data;
  if (data) {
    var result = keys$1.filter(function (key) { return isObjectHasKey(data, key); });
    return (result.length === keys$1.length) ? data : false;
  }
  return false;
};

/**
 * @param {object} payload This is the entire ws Event Object
 * @return {object} false on failed
 */
var extractWsPayload = function (payload) {
  var data = payload.data;
  var json = isString$1(data) ? JSON.parse(data) : data;
  // debugFn('extractWsPayload', json)
  var fdata;
  if ((fdata = isWsReply(json)) !== false) {
    return {
      resolverName: fdata[WS_EVT_NAME],
      data: fdata[WS_DATA_NAME],
      type: fdata[WS_REPLY_TYPE]
    };
  }
  throw new JsonqlError('payload can not be decoded', payload)
};

// the WebSocket main handler
var debugFn$3 = getDebug('ws-main-handler');

/**
 * under extremely circumstances we might not even have a resolverName, then
 * we issue a global error for the developer to catch it
 * @param {object} ee event emitter
 * @param {string} namespace nsp
 * @param {string} resolverName resolver
 * @param {object} json decoded payload or error object
 */
var errorTypeHandler = function (ee, namespace, resolverName, json) {
  var evt = [namespace];
  if (resolverName) {
    debugFn$3(("a global error on " + namespace));
    evt.push(resolverName);
  }
  evt.push(ON_ERROR_PROP_NAME);
  var evtName = Reflect.apply(createEvt, null, evt);
  // test if there is a data field
  var payload = json.data || json;
  ee.$trigger(evtName, [payload]);
};

/**
 * Binding the even to socket normally
 * @param {string} namespace
 * @param {object} ws the nsp
 * @param {object} ee EventEmitter
 * @return {object} promise resolve after the onopen event
 */
function wsMainHandlerAction(namespace, ws, ee) {
  // send
  ws.onopen = function() {
    // we just call the onReady
    ee.$call(ON_READY_PROP_NAME, namespace);
    // add listener
    ee.$only(
      createEvt(namespace, EMIT_EVT),
      function(resolverName, args) {
        debugFn$3('calling server', resolverName, args);
        ws.send(
          createQueryStr$1(resolverName, args)
        );
      }
    );
  };

  // reply
  ws.onmessage = function(payload) {
    try {
      var json = extractWsPayload(payload);
      debugFn$3('Hear from server', json);
      var resolverName = json.resolverName;
      var type = json.type;
      switch (type) {
        case EMIT_REPLY_TYPE:
          var r = ee.$trigger(createEvt(namespace, resolverName, ON_MESSAGE_PROP_NAME), [json]);
          debugFn$3(ON_MESSAGE_PROP_NAME, r);
          break;
        case ACKNOWLEDGE_REPLY_TYPE:
          debugFn$3(ON_RESULT_PROP_NAME, json);
          var x = ee.$trigger(createEvt(namespace, resolverName, ON_RESULT_PROP_NAME), [json]);
          debugFn$3('onResult add to event?', x);
          break;
        case ERROR_TYPE:
          // this is handled error and we won't throw it
          // we need to extract the error from json
          errorTypeHandler(ee, namespace, resolverName, json);
          break;
        // @TODO there should be an error type instead of roll into the other two types? TBC
        default:
          // if this happen then we should throw it and halt the operation all together
          debugFn$3('Unhandled event!', json);
          errorTypeHandler(ee, namespace, resolverName, json);
          // let error = {error: {'message': 'Unhandled event!', type}};
          // ee.$trigger(createEvt(namespace, resolverName, ON_RESULT_PROP_NAME), [error])
      }
    } catch(e) {
      errorTypeHandler(ee, namespace, false, e);
    }
  };
  // when the server close the connection
  ws.onclose = function() {
    debugFn$3('ws.onclose');
    // @TODO what to do with this
    // ee.$trigger(LOGOUT_EVENT_NAME, [namespace])
  };
  // listen to the LOGOUT_EVENT_NAME
  ee.$on(LOGOUT_EVENT_NAME, function close() {
    try {
      debugFn$3('terminate ws connection');
      ws.terminate();
    } catch(e) {
      debugFn$3('terminate ws error', e);
    }
  });
}

// make this another standalone module
var debugFn$4 = getDebug('ws-create-client');

/**
 * Because the nsps can be throw away so it doesn't matter the scope
 * this will get reuse again
 * @param {object} opts configuration
 * @param {object} nspMap from contract
 * @param {string|null} token whether we have the token at run time
 * @return {object} nsps namespace with namespace as key
 */
var createNsps = function(opts, nspMap, token) {
  var nspSet = nspMap.nspSet;
  var publicNamespace = nspMap.publicNamespace;
  var login = false;
  var namespaces = [];
  var nsps = {};
  // first we need to binding all the events handler
  if (opts.enableAuth && opts.useJwt) {
    login = true; // just saying we need to listen to login event
    namespaces = getNamespaceInOrder(nspSet, publicNamespace);
    nsps = namespaces.map(function (namespace, i) {
      var obj, obj$1, obj$2;

      if (i === 0) {
        if (token) {
          opts.token = token;
          return ( obj = {}, obj[namespace] = nspAuthClient(namespace, opts), obj )
        }
        return ( obj$1 = {}, obj$1[namespace] = false, obj$1 )
      }
      return ( obj$2 = {}, obj$2[namespace] = nspClient(namespace, opts), obj$2 )
    }).reduce(function (first, next) { return Object.assign(first, next); }, {});
  } else {
    var namespace = getNameFromPayload$1(nspSet);
    namespaces.push(namespace);
    // standard without login
    // the stock version should not have a namespace
    nsps[namespace] = nspClient(false, opts);
  }
  // return
  return { nsps: nsps, namespaces: namespaces, login: login }
};

/**
 * create a ws client
 * @param {object} opts configuration
 * @param {object} nspMap namespace with resolvers
 * @param {object} ee EventEmitter to pass through
 * @return {object} what comes in what goes out
 */
function createClient(opts, nspMap, ee) {
  // arguments that don't change
  var args = [opts, nspMap, ee, wsMainHandlerAction];
  // now create the nsps
  var ref = createNsps(opts, nspMap, opts.token);
  var nsps = ref.nsps;
  var namespaces = ref.namespaces;
  var login = ref.login;
  // binding the listeners - and it will listen to LOGOUT event
  // to unbind itself, and the above call will bind it again
  Reflect.apply(clientEventHandler, null, args.concat([namespaces, nsps]));
  // setup listener
  if (login) {
    ee.$only(LOGIN_EVENT_NAME, function(token) {
      disconnect(nsps, JS_WS_NAME);
      // @TODO should we trigger error on this one?
      // triggerNamespacesOnError(ee, namespaces, LOGIN_EVENT_NAME)
      clearMainEmitEvt(ee, namespaces);
      debugFn$4('LOGIN_EVENT_NAME', token);
      var newNsps = createNsps(opts, nspMap, token);
      // rebind it
      Reflect.apply(
        clientEventHandler,
        null,
        args.concat([newNsps.namespaces, newNsps.nsps])
      );
    });
  }
  // return what input
  return { opts: opts, nspMap: nspMap, ee: ee }
}

// we only need to export one interface from now on

var debugFn$5 = getDebug('io-main-handler');

/**
 * @param {object} ee Event Emitter
 * @param {string} namespace namespace of this nsp
 * @param {string} resolverName resolver to handle this call
 * @return {function} capture the result
 */
var resultHandler = function (ee, namespace, resolverName, evt) {
  if ( evt === void 0 ) evt = ON_RESULT_PROP_NAME;

  return function (result) {
    ee.$trigger(createEvt(namespace, resolverName, evt), [result]);
  }
};

/**
 * @param {object} nspSet resolver list
 * @param {object} nsp nsp instance
 * @param {object} ee Event Emitter
 * @param {string} namespace name of this nsp
 * @return {void}
 */
var createResolverListener = function (nspSet, nsp, ee, namespace) {
  for (var resolverName in nspSet) {
    nsp.on(
      resolverName,
      resultHandler(ee, namespace, resolverName, ON_MESSAGE_PROP_NAME)
    );
  }
};

/**
 * @param {object} nsp instance
 * @param {object} ee Event Emitter
 * @param {string} namespace name of this nsp
 * @return {void}
 */
var mainEventHandler = function (nsp, ee, namespace) {
  ee.$only(
    createEvt(namespace, EMIT_EVT),
    function resolverEmitHandler(resolverName, args) {
      debugFn$5('mainEventHandler', resolverName, args);
      nsp.emit(
        resolverName,
        formatPayload(args),
        resultHandler(ee, namespace, resolverName)
      );
    }
  );
};

/**
 * it makes no different at this point if we know its connection establish or not
 * We should actually know this before hand before we call here
 * @param {string} namespace of this client
 * @param {object} socket this is the resolved nsp connection object
 * @param {object} ee Event Emitter
 * @param {object} nspSet the list of resolvers
 * @param {object} opts configuration
 */
function ioMainHandler(namespace, socket, ee, nspSet, opts) {
  // the main listener for all the client resolvers
  mainEventHandler(socket, ee, namespace);
  // it doesn't make much different between inside the connect or not
  // loop through to create the listeners
  createResolverListener(nspSet, socket, ee, namespace);
  //@TODO next we need to add a ERROR handler
  // The server side is not implementing a global ERROR call yet
  // and the result or message error will be handle individually by their callback
  // listen to the server close event
  socket.on('disconnect', function disconnect() {
    debugFn$5('io.disconnect');
    // TBC what to do with this
    // ee.$trigger(LOGOUT_EVENT_NAME, [namespace])
  });
  // listen to the logout event
  ee.$on(LOGOUT_EVENT_NAME, function logoutHandler() {
    try {
      debugFn$5('terminate ws connection');
      socket.close();
    } catch(e) {
      debugFn$5('terminate ws error', e);
    }
  });
  // the last one to fire
  ee.$trigger(ON_READY_PROP_NAME, namespace);
}

// this will create the socket.io client
var debugFn$6 = getDebug('io-create-client');

// just to make it less ugly
var mapNsps = function (nsps, namespaces) { return nsps
  .map(function (nsp, i) {
    var obj;

    return (( obj = {}, obj[namespaces[i]] = nsp, obj ));
  })
  .reduce(function (last, next) { return Object.assign(last,next); }, {}); };

/**
 * This one will run the create nsps in sequence and make sure
 * the auth one connect before we call the others
 * @param {object} opts configuration
 * @param {object} nspMap contract map
 * @param {string} token validation
 * @return {object} promise resolve with namespaces, nsps in same order array
 */
var createAuthNsps = function(opts, nspMap, token, namespaces) {
  var publicNamespace = nspMap.publicNamespace;
  opts.token = token;
  var p1 = function () { return nspAuthClient(namespaces[0], opts); };
  var p2 = function () { return nspClient(namespaces[1], opts); };
  return chainPromises([p1(), p2()])
    .then(function (nsps) { return ({
      nsps: mapNsps(nsps, namespaces),
      namespaces: namespaces,
      login: false
    }); })
};

/**
 * Because the nsps can be throw away so it doesn't matter the scope
 * this will get reuse again
 * @param {object} opts configuration
 * @param {object} nspMap from contract
 * @param {string|null} token whether we have the token at run time
 * @return {object} nsps namespace with namespace as key
 */
var createNsps$1 = function(opts, nspMap, token) {
  var nspSet = nspMap.nspSet;
  var publicNamespace = nspMap.publicNamespace;
  var login = false;
  // first we need to binding all the events handler
  if (opts.enableAuth && opts.useJwt) {
    var namespaces = getNamespaceInOrder(nspSet, publicNamespace);
    debugFn$6('namespaces', namespaces);
    login = opts.useJwt; // just saying we need to listen to login event
    if (token) {
      debugFn$6('call createAuthNsps');
      return createAuthNsps(opts, nspMap, token, namespaces)
    }
    debugFn$6('init with a placeholder');
    return nspClient(publicNamespace, opts)
      .then(function (nsp) {
        var obj;

        return ({
        nsps: ( obj = {}, obj[ publicNamespace ] = nsp, obj[ namespaces[0] ] = false, obj ),
        namespaces: namespaces,
        login: login
      });
    })
  }
  // standard without login
  // the stock version should not have a namespace
  return nspClient(false, opts)
    .then(function (nsp) {
      var obj;

      return ({
      nsps: ( obj = {}, obj[publicNamespace] = nsp, obj ),
      namespaces: [publicNamespace],
      login: login
    });
  })
};



/**
 * This is just copy of the ws version we need to figure
 * out how to deal with the roundtrip login later
 * @param {object} opts configuration
 * @param {object} nspMap namespace with resolvers
 * @param {object} ee EventEmitter to pass through
 * @return {object} what comes in what goes out
 */
function createClient$1(opts, nspMap, ee) {
  // arguments don't change
  var args = [opts, nspMap, ee, ioMainHandler];
  return createNsps$1(opts, nspMap, opts.token)
    .then( function (ref) {
      var nsps = ref.nsps;
      var namespaces = ref.namespaces;
      var login = ref.login;

      // binding the listeners - and it will listen to LOGOUT event
      // to unbind itself, and the above call will bind it again
      Reflect.apply(clientEventHandler, null, args.concat([namespaces, nsps]));
      if (login) {
        ee.$only(LOGIN_EVENT_NAME, function(token) {
          // here we should disconnect all the previous nsps
          disconnect(nsps);
          // first trigger a LOGOUT event to unbind ee to ws
          // ee.$trigger(LOGOUT_EVENT_NAME) // <-- this seems to cause a lot of problems
          clearMainEmitEvt(ee, namespaces);
          debugFn$6('LOGIN_EVENT_NAME');
          createNsps$1(opts, nspMap, token)
            .then(function (newNsps) {
              // rebind it
              Reflect.apply(
                clientEventHandler,
                null,
                args.concat([newNsps.namespaces, newNsps.nsps])
              );
            });
        });
      }
      // return this will also works because the outter call are in promise chain
      return { opts: opts, nspMap: nspMap, ee: ee }
    })
}

/**
 * get the create client instance function
 * @param {string} type of client
 * @return {function} the actual methods
 * @public
 */
function createSocketClient(opts, nspMap, ee) {
  switch (opts.serverType) {
    case SOCKET_IO:
      return createClient$1(opts, nspMap, ee)
    case WS:
      return createClient(opts, nspMap, ee)
    default:
      throw new JsonqlError(SOCKET_NOT_DEFINE_ERR)
  }
}

// generator resolvers
var EMIT_EVT$1 = EMIT_EVT;
var UNKNOWN_RESULT$1 = UNKNOWN_RESULT;
var MY_NAMESPACE$1 = MY_NAMESPACE;
var debugFn$7 = getDebug('generator');

/**
 * prepare the methods
 * @param {object} opts configuration
 * @param {object} nspMap resolvers index by their namespace
 * @param {object} ee EventEmitter
 * @return {object} of resolvers
 * @public
 */
function generator(opts, nspMap, ee) {
  var obj = {};
  var nspSet = nspMap.nspSet;
  for (var namespace in nspSet) {
    var list = nspSet[namespace];
    for (var resolverName in list) {
      var params = list[resolverName];
      var fn = createResolver(ee, namespace, resolverName, params);
      obj[resolverName] = setupResolver(namespace, resolverName, params, fn, ee);
    }
  }
  // add error handler
  createNamespaceErrorHandler(obj, ee, nspSet);
  // add onReady handler
  createOnReadyhandler(obj, ee);
  // Auth related methods
  createAuthMethods(obj, ee, opts);
  // this is a helper method for the developer to know the namespace inside
  obj.getNsp = function () {
    return Object.keys(nspSet)
  };
  // output
  return obj;
}

/**
 * create the actual function to send message to server
 * @param {object} ee EventEmitter instance
 * @param {string} namespace this resolver end point
 * @param {string} resolverName name of resolver as event name
 * @param {object} params from contract
 * @return {function} resolver
 */
function createResolver(ee, namespace, resolverName, params) {
  // note we pass the new withResult=true option
  return function() {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

    return validateAsync$1(args, params.params, true)
      .then( function (_args) { return actionCall(ee, namespace, resolverName, _args); } )
      .catch(finalCatch)
  }
}

/**
 * just wrapper
 * @param {object} ee EventEmitter
 * @param {string} namespace where this belongs
 * @param {string} resolverName resolver
 * @param {array} args arguments
 * @return {void} nothing
 */
function actionCall(ee, namespace, resolverName, args) {
  if ( args === void 0 ) args = [];

  debugFn$7(("actionCall: " + namespace + " " + resolverName), args);
  ee.$trigger(createEvt(namespace, EMIT_EVT$1), [
    resolverName,
    toArray$1(args)
  ]);
}

/**
 * break out to use in different places to handle the return from server
 * @param {object} data from server
 * @param {function} resolver from promise
 * @param {function} rejecter from promise
 * @return {void} nothing
 */
function respondHandler(data, resolver, rejecter) {
  if (isObjectHasKey(data, 'error')) {
    debugFn$7('rejecter called', data.error);
    rejecter(data.error);
  } else if (isObjectHasKey(data, 'data')) {
    debugFn$7('resolver called', data.data);
    resolver(data.data);
  } else {
    debugFn$7('UNKNOWN_RESULT', data);
    rejecter({message: UNKNOWN_RESULT$1, error: data});
  }
}

/**
 * Add extra property to the resolver
 * @param {string} namespace where this belongs
 * @param {string} resolverName name as event name
 * @param {object} params from contract
 * @param {function} fn resolver function
 * @param {object} ee EventEmitter
 * @return {function} resolver
 */
var setupResolver = function (namespace, resolverName, params, fn, ee) {
  // also need to setup a getter to get back the namespace of this resolver
  if (Object.getOwnPropertyDescriptor(fn, MY_NAMESPACE$1) === undefined) {
    Object.defineProperty(fn, MY_NAMESPACE$1, {
      value: namespace,
      writeable: false
    });
  }
  // onResult handler
  if (Object.getOwnPropertyDescriptor(fn, ON_RESULT_PROP_NAME) === undefined) {
    Object.defineProperty(fn, ON_RESULT_PROP_NAME, {
      set: function(resultCallback) {
        if (typeof resultCallback === 'function') {
          ee.$only(
            createEvt(namespace, resolverName, ON_RESULT_PROP_NAME),
            function resultHandler(result) {
              respondHandler(result, resultCallback, function (error) {
                ee.$trigger(createEvt(namespace, resolverName, ON_ERROR_PROP_NAME), error);
              });
            }
          );
        }
      },
      get: function() {
        return null;
      }
    });
  }
  // we do need to add the send prop back because it's the only way to deal with
  // bi-directional data stream
  if (Object.getOwnPropertyDescriptor(fn, ON_MESSAGE_PROP_NAME) === undefined) {
    Object.defineProperty(fn, ON_MESSAGE_PROP_NAME, {
      set: function(messageCallback) {
        // we expect this to be a function
        if (typeof messageCallback === 'function') {
          // did that add to the callback
          var onMessageCallback = function (args) {
            respondHandler(args, messageCallback, function (error) {
              ee.$trigger(createEvt(namespace, resolverName, ON_ERROR_PROP_NAME), error);
            });
          };
          // register the handler for this message event
          ee.$only(createEvt(namespace, resolverName, ON_MESSAGE_PROP_NAME), onMessageCallback);
        }
      },
      get: function() {
        return null; // just return nothing
      }
    });
  }
  // add an ON_ERROR_PROP_NAME handler
  if (Object.getOwnPropertyDescriptor(fn, ON_ERROR_PROP_NAME) === undefined) {
    Object.defineProperty(fn, ON_ERROR_PROP_NAME, {
      set: function(resolverErrorHandler) {
        if (typeof resolverErrorHandler === 'function') {
          // please note ON_ERROR_PROP_NAME can add multiple listners
          ee.$only(createEvt(namespace, resolverName, ON_ERROR_PROP_NAME), resolverErrorHandler);
        }
      },
      get: function() {
        return null;
      }
    });
  }
  // pairing with the server vesrion SEND_MSG_PROP_NAME
  if (Object.getOwnPropertyDescriptor(fn, SEND_MSG_PROP_NAME) === undefined) {
    Object.defineProperty(fn, SEND_MSG_PROP_NAME, {
      set: function(messagePayload) {
        var result = validateSync$1(toArray$1(messagePayload), params.params, true);
        // here is the different we don't throw erro instead we trigger an
        // onError
        if (result[ERROR_KEY] && result[ERROR_KEY].length) {
          ee.$call(
            createEvt(namespace, resolverName, ON_ERROR_PROP_NAME),
            [JsonqlValidationError(resolverName, result[ERROR_KEY])]
          );
        } else {
          // there is no return only an action call
          actionCall(ee, namespace, resolverName, result[DATA_KEY]);
        }
      },
      get: function() {
        return null; // just return nothing
      }
    });
  }
  return fn;
};

/**
 * The problem is the namespace can have more than one
 * and we only have on onError message
 * @param {object} obj the client itself
 * @param {object} ee Event Emitter
 * @param {object} nspSet namespace keys
 * @return {void}
 */
var createNamespaceErrorHandler = function (obj, ee, nspSet) {
  // using the onError as name
  // @TODO we should follow the convention earlier
  // make this a setter for the obj itself
  if (Object.getOwnPropertyDescriptor(obj, ON_ERROR_PROP_NAME) === undefined) {
    Object.defineProperty(obj, ON_ERROR_PROP_NAME, {
      set: function(namespaceErrorHandler) {
        if (typeof namespaceErrorHandler === 'function') {
          // please note ON_ERROR_PROP_NAME can add multiple listners
          for (var namespace in nspSet) {
            // this one is very tricky, we need to make sure the trigger is calling
            // with the namespace as well as the error
            ee.$on(createEvt(namespace, ON_ERROR_PROP_NAME), namespaceErrorHandler);
          }
        }
      },
      get: function() {
        return null;
      }
    });
  }
};

/**
 * This event will fire when the socket.io.on('connection') and ws.onopen
 * @param {object} obj the client itself
 * @param {object} ee Event Emitter
 * @param {object} nspSet namespace keys
 * @return {void}
 */
var createOnReadyhandler = function (obj, ee, nspSet) {
  if (Object.getOwnPropertyDescriptor(obj, ON_READY_PROP_NAME) === undefined) {
    Object.defineProperty(obj, ON_READY_PROP_NAME, {
      set: function(onReadyCallback) {
        if (typeof onReadyCallback === 'function') {
          // reduce it down to just one flat level
          var result = ee.$on(ON_READY_PROP_NAME, onReadyCallback);
        }
      },
      get: function() {
        return null;
      }
    });
  }
};

/**
 * Create auth related methods
 * @param {object} obj the client itself
 * @param {object} ee Event Emitter
 * @param {object} opts configuration
 * @return {void}
 */
var createAuthMethods = function (obj, ee, opts) {
  if (opts.enableAuth) {
    // create an additonal login handler
    // we require the token
    obj[opts.loginHandlerName] = function (token) {
      debugFn$7(opts.loginHandlerName, token);
      if (token && isString$1(token)) {
        return ee.$trigger(LOGIN_EVENT_NAME, [token])
      }
      throw new JsonqlValidationError(opts.loginHandlerName)
    };
    // logout event handler
    obj[opts.logoutHandlerName] = function () {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      ee.$trigger(LOGOUT_EVENT_NAME, args);
    };
  }
};

// main api to get the ws-client

/**
 * The main interface to create the wsClient for use
 * @param {function} clientGenerator this is an internal way to generate node or browser client
 * @return {function} wsClient
 * @public
 */
function main(clientGenerator) {
  /**
   * @param {object} config configuration
   * @param {object} [eventEmitter=false] this will be the bridge between clients
   * @return {object} wsClient
   */
  var wsClient = function (config, eventEmitter) {
    if ( eventEmitter === void 0 ) eventEmitter = false;

    return checkOptions(config)
      .then(function (opts) { return ({
          opts: opts,
          nspMap: processContract(opts),
          ee: eventEmitter || new JsonqlWsEvt()
        }); }
      )
      .then(clientGenerator)
      .then(
        function (ref) {
          var opts = ref.opts;
          var nspMap = ref.nspMap;
          var ee = ref.ee;

          return createSocketClient(opts, nspMap, ee);
    }
      )
      .then(
        function (ref) {
          var opts = ref.opts;
          var nspMap = ref.nspMap;
          var ee = ref.ee;

          return generator(opts, nspMap, ee);
    }
      )
      .catch(function (err) {
        console.error('jsonql-ws-client init error', err);
      })
  };
  // use the Object.addProperty trick
  Object.defineProperty(wsClient, 'CLIENT_TYPE_INFO', {
    value: 'version: 1.0.0-beta.2 module: cjs',
    writable: false
  });
  return wsClient;
}

module.exports = main;
