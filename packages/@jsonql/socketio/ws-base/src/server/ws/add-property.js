
// const debug = require('debug')('jsonql-ws-server:ws-add-property');
const createWsReply = require('./create-ws-reply')
const { EMIT_REPLY_TYPE, SEND_MSG_PROP_NAME, ON_MESSAGE_PROP_NAME } = require('jsonql-constants')
// const { JsonqlError } = require('jsonql-errors')
const addHandlerProperty = require('../share/add-handler-property')
const { nil } = require('../share/helpers')

/**
 * @param {function} fn the actual resolver function
 * @param {string} resolverName name of resolver
 * @param {object} ws the io instance
 * @return {function} fn with additional property
 */
const addProperty = (fn, resolverName, ws) => {
  let resolver;
  resolver = addHandlerProperty(fn, SEND_MSG_PROP_NAME, function(prop) {
    ws.send(createWsReply(EMIT_REPLY_TYPE, resolverName, prop))
  }, nil)
  /*
  @TODO is this necessary?
  resolver = addHandlerProperty(resolver, ON_MESSAGE_PROP_NAME, function(handler) {
    if (handler && typeof handler === 'function') {

    }
    throw new JsonqlError(resolverName, {message: `Require ${ON_MESSAGE_PROP_NAME} to be a function!`})
  }, nil)
  */
  return resolver;
}

module.exports = addProperty
