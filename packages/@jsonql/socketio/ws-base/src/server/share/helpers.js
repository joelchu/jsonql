// util methods
const { trim } = require('lodash')
const debug = require('debug')
// jsonql libraries
const {
  JSONQL_PATH,
  WS_REPLY_TYPE,
  WS_EVT_NAME,
  WS_DATA_NAME
} = require('jsonql-constants')
const {
  isString,
  isObjectHasKey
} = require('jsonql-params-validator')
const {
  JsonqlError,
  clientErrorsHandler
} = require('jsonql-errors')

const { MODULE_NAME } = require('./constants')

// create debug
const getDebug = name => debug(MODULE_NAME).extend(name)

const _debug = getDebug('helpers')

// import { getDebug } from '../get-debug';
const keys = [ WS_REPLY_TYPE, WS_EVT_NAME, WS_DATA_NAME ]
// const debug = getDebug('is-ws-reply');
/**
 * @param {string|object} payload should be string when reply but could be transformed
 * @return {boolean} true is OK
 */
const isWsReply = payload => {
  const json = isString(payload) ? JSON.parse(payload) : payload;
  const { data } = json;
  if (data) {
    let result = keys.filter(key => isObjectHasKey(data, key))
    return (result.length === keys.length) ? data : false;
  }
  return false;
}

/**
 * @param {string|object} data received data
 * @return {object} false on failed
 */
const extractWsPayload = payload => {
  const json = isString(payload) ? JSON.parse(payload) : payload;
  // if there is error then this will throw
  clientErrorsHandler(json);
  // now handle the data
  let _data;
  if ((_data = isWsReply(json)) !== false) {
    return {
      data: _data[WS_DATA_NAME],
      resolverName: _data[WS_EVT_NAME],
      type: _data[WS_REPLY_TYPE]
    }
  }
  throw new JsonqlError('payload can not decoded', payload)
}

/**
 * We are going to completely change this
 * 1. there will only be max two namespace
 * 2. when it's normal we will have the stock path as namespace
 * 3. when enableAuth then we will have two one is jsonql/public + private
 * @param {object} config options
 * @return {array} of namespace(s)
 */
const getNamespace = function(config) {
  const base = JSONQL_PATH;
  if (config.enableAuth) {
    // the public come first
    return [
      [ base , config.publicNamespace].join('/'),
      [ base , config.privateNamespace].join('/')
    ]
  }
  return [ base ]
}

/**
 * From underscore.string library
 * @BUG there is a bug here with the non-standard name
 * @param {string} str string
 * @return {string} dasherize string
 */
const dasherize = str => trim(str)
    .replace(/([A-Z])/g, '-$1')
    .replace(/[-_\s]+/g, '-')
    .toLowerCase()

/**
 * create the error message
 * @param {object} e error
 * @param {boolean} s true toString
 * @return {object} formatted reply
 */
const packError = (e, s = false) => {
  const payload = {
    error: {
      className: e.className || 'JsonqlServerError',
      message: e.message || 'NO MESSAGE',
      detail: e.detail || e
    }
  }
  return s ? JSON.stringify(payload) : payload;
}

// just an empty method for addProperty getter
const nil = function() {
  return false;
}

// export
module.exports = {
  getDebug,
  getNamespace,
  dasherize,
  packError,
  extractWsPayload,
  nil
}
