
/**
 * this is a generic method that add the socket property
 * to the resolver function for other use
 * @param {function} fn the resolver
 * @param {object} socket the socket.io socket or nsp
 * @return {function} fn the resolver + new property from ctx
 */
const addWsProperty = (fn , socket) => {
  if (Object.getOwnPropertyDescriptor(fn, 'socket') === undefined) {
    Object.defineProperty(fn, 'socket', {
      value: socket,
      writeable: false
    })
  }
  return fn;
}
// export
module.exports = addWsProperty;
