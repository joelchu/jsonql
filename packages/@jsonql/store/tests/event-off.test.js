const test = require('ava')
const { 
  JsonqlStore
} = require('../dist/jsonql-store.cjs')
const logger = require('debug')('jsonql-store:test:event-off')


test(`Testing the event off`, t => {

  const store = new JsonqlStore({ eventOff: true, logger })

  const evt = store.on('some-event', () => {})

  t.is(evt, undefined)

})