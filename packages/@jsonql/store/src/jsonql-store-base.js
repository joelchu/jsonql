/**
 * How do we create a plugin to allow a third party storage 
 */
import { isFunc } from 'jsonql-utils/src/generic'
import { chainFns } from 'jsonql-utils/src/chain-fns'
import EventClass from '@to1source/event'
import { DATA_KEY, TIME_KEY } from './constants'

export class JsonqlStoreBaseClass extends EventClass {

  constructor(opts) {
    super(opts)

    this.$store = new Map()
    // this is just placeholder for now
    if (opts.plugin && isFunc(opts.plugin)) {
      this.$__plugin = opts.plugin 
    }
  }

  /**
   * Wrap all event emit here and can be turn off via the eventOff = true
   * @param {*} evtName 
   * @param {array} payload expect an array but you need to diy it! 
   * @return {*} undefined means did nothing 
   */
  $__emit(evtName, payload) {
    if (this.__eventOff === false) {
      return this.$trigger(evtName, payload)
    }
  }

  /**
   * This is the plugin placeholder
   * @param {*} methodName 
   * @param  {...any} args 
   */
  $__plugin(methodName, ...args) {

  }

  /**
   * the core method to run everything
   * @param {*} method 
   * @param  {...any} args 
   */
  $__run(method, ...args) {
    const action = chainFns(method, this.$__plugin)
    return Reflect.apply(action, this, args)
  }

  /**
   * a wraper method to create the stored items 
   * @TODO add timestamp using a Set etc
   * @param {*} value whatever you want to store 
   * @return {*} TBC  
   */
  $__wrap(value) {
    //return value 
    return {
      [DATA_KEY]: value,
      [TIME_KEY]: Date.now()
    }
  }

  /**
   * Unwrap our internal store and return the value
   * @param {*} value
   * @return {*} TBC  
   */
  $__unwrap(value) {
    // return value 
    // @TODO what to do with the expired time 
    return value[DATA_KEY]
  }

  /**
   * Instead of calling the Map methods directly we overload it without own methods
   * @param {*} key
   * @return {boolean}
   */
  $__has(key) {
    return this.$store.has(key) || false 
  }

  /**
   * setter method to store data 
   * @param {*} key 
   * @param {*} value 
   */
  $__set(key, value) {
    return this.$store.set(key, this.$__wrap(value))
  }

  /**
   * Get data @TODO should check the ts method if any 
   * then expired the data
   * @param {*} key 
   * @return {*}
   */
  $__get(key) {
    return this.$__unwrap(this.$store.get(key)) 
  }

  /**
   * Delete data 
   * @param {*} key 
   * @return {*}
   */
  $__delete(key) {
    return this.$store.delete(key)
  }

  /**
   * this will always return as an array of all data 
   * because we need to run it through the $__unwarp method 
   * @return {array}
   */
  $__entries() {
    return Array
      .from(this.$store.entries())
      .map(e => this.$__unwrap(e))
  }

  /**
   * Get the size of the store
   * @return {*}
   */
  $__size() {
    return this.$store.size
  }

  /**
   * Clear the store
   * @return {*}
   */
  $__clear() {
    return this.$store.clear()
  }
}

