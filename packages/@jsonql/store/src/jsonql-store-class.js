// just fucking around with the cache object 
// NodeCache is a piece of shit 
import { JsonqlStoreBaseClass } from './jsonql-store-base'
import {
  HAS_EVT,
  SET_EVT,
  GET_EVT,
  DELETE_EVT,
  TAKE_EVT,
  ALL_EVT,
  SIZE_EVT,
  CLEAR_EVT,
  AVAILABLE_EVTS
} from './constants'


export class JsonqlStore extends JsonqlStoreBaseClass {

  /**
   * constructor init the instance
   * @param {*} opts 
   */
  constructor(opts = {}) {
    super(opts)
    // @0.9.1 add a switch to turn off the event
    this.__eventOff = !!opts.eventOff    
    
  }

  //////////////////////////////////
  //        PUBLIC METHODS        //
  //////////////////////////////////

  /**
   * Check key exist 
   * @param {*} key 
   * @return {*}
   */
  has(key) {
    const result = this.$__has(key)
    this.$__emit(HAS_EVT, [key, result])

    return result
  }

  /**
   * note this actually return the value itself 
   * @param {*} key 
   * @param {*} value 
   * @return {*}
   */
  set(key, value) {
    this.$__set(key, value)
    this.$__emit(SET_EVT, [key, value])

    return this.has(key)
  }

  /**
   * get it and make sure it return boolen false when nothing
   * @param {*} key
   * @return {*} 
   */
  get(key) {
    const result = this.$__get(key)
    this.$__emit(GET_EVT, [key, result])

    return result
  }

  /**
   * Remove from store 
   * @param {*} key
   * @return {*} 
   */
  delete(key) {
    const result = this.$__delete(key)
    this.$__emit(DELETE_EVT, [key, result])
    
    return result
  }

  /**
   * return the key value then remove it
   * @param {*} key
   * @return {*} 
   */
  take(key) {
    if (this.has(key)) {
      const value = this.$__get(key)
      this.$__emit(TAKE_EVT, [key, value])
      // we call the native method because we don't want to trigger the delete event again
      this.$__delete(key)
      return value
    }
    this.$__emit(TAKE_EVT, [key])
    return false
  }

  /**
   * alias to entries return interable object
   * @return {array} this is the default now
   */
  all() {
    const result = this.$__entries()
    this.$__emit(ALL_EVT, [result])

    return result
  }


  /**
   * Get the size (length) of the store
   * @return {number} 
   */
  size() {
    const result = this.$__size()
    this.$__emit(SIZE_EVT, [result])

    return result
  }

  /**
   * Alias to size as a getter
   * @return {number} 
   */
  get length() {
    return this.size()
  }

  /**
   * Clear out everything from store
   * @return {void}
   */
  clear() {
    this.$__clear()
    this.$__emit(CLEAR_EVT)
    // @TODO should we clear out all the event listeners as well? 
  }

  /** 
   * event hook 
   * @param {string} evtName
   * @param {function} callback
   * @param {*} context
   * @return {*} return undefined means it did nothing  
   */
  on(evtName, callback, context = null) {
    if (this.__eventOff === false) {
      const ok = !!AVAILABLE_EVTS.filter(e => e === evtName).length
      if (ok) {
        return this.$on(evtName, callback, context)
      } 
      throw new Error(`${evtName} is not one of ours!`)
    }
  }
}


