
// create constants for all the event names 
// 'has', 'set', 'get', 'remove', 'take', 'all', 'size'
export const HAS_EVT = '__has__'
export const SET_EVT = '__set__'
export const GET_EVT = '__get__'
export const DELETE_EVT = '__delete__'
export const TAKE_EVT = '__take__'
export const ALL_EVT = '__all__'
export const SIZE_EVT = '__size__'
export const CLEAR_EVT = '__clear__'
export const DATA_KEY = '__data__'
export const TIME_KEY = '__ts__'

export const AVAILABLE_EVTS = [
  HAS_EVT,
  SET_EVT,
  GET_EVT,
  DELETE_EVT,
  TAKE_EVT,
  ALL_EVT,
  SIZE_EVT,
  CLEAR_EVT,
  DATA_KEY,
  TIME_KEY
]