// just wrap this in one method
const { JS_WS_NAME, JS_WS_SOCKET_IO_NAME, JS_PRIMUS_NAME } = require('jsonql-constants')

/**
 * @param {object} config configuration
 * @return {*} false when not found or throw Error when module not found
 */
function getSocketServer(config, server) {
  let wsServer
  if (config.serverType) {
    switch (config.serverType) {
      case JS_WS_NAME:
          // @NOTE replace the module name later jsonql-ws-server
          const { jsonqlWsServer } = require('jsonql-ws-server')
          wsServer = jsonqlWsServer
        break;
      case JS_WS_SOCKET_IO_NAME:
      case JS_PRIMUS_NAME:
      default:
        console.error(`Not support ${config.serverType} at the moment!`)
        return false
    }
    // there is a problem with the socket-server that need to fix first

    return wsServer(config, server)
  }
  return false
}

module.exports = { getSocketServer }
