// main export for set up the server
const http = require('http')
const Koa = require('koa')
const bodyparser = require('koa-bodyparser')
const cors = require('koa-cors')
// const { composeJsonqlKoa } = require('../../../koa/main')
const { composeJsonqlKoa } = require('jsonql-koa')
// const { chainFns } = require('jsonql-utils')
const { getSocketServer } = require('./get-socket-server')
const debug = require('debug')('jsonql-koa:init-server')

/**
 * Creating all the required servers
 * @param {object} config configuration
 * @param {array} middlewares what it said
 * @return {object} see below for detail
 */
function initServer(config, middlewares) {

  const app = new Koa()
  // apply default middlewares
  app.use(bodyparser())
  if (config.cors === true) { // default is true
    app.use(cors())
  }
  // init jsonqlKoa
  app.use(composeJsonqlKoa(config))
  // if any
  middlewares.forEach(middleware => {
    app.use(middleware)
  })

  const server = http.createServer(app.callback())

  server.on('error', err => {
    debug('server.error', err)
  })
  
  const ws = getSocketServer(config, server)
  // return it
  return { server, app, ws }
}

module.exports = { initServer }
