const { createConfig } = require('jsonql-params-validator')
const {
  BOOLEAN_TYPE,
  NUMBER_TYPE,
  STRING_TYPE,
  ENUM_KEY,
  ALIAS_KEY,
  DEFAULT_PORT_NUM,
  JS_WS_NAME,
  JS_WS_SOCKET_IO_NAME,
  JS_PRIMUS_NAME
} = require('jsonql-constants')

const options = {
  cors: createConfig(true, [BOOLEAN_TYPE]),
  autoStart: createConfig(true, [BOOLEAN_TYPE]),
  port: createConfig(DEFAULT_PORT_NUM, [NUMBER_TYPE]),
  // new prop for socket client
  serverType: createConfig(null, [STRING_TYPE], {
    [ENUM_KEY]: [JS_WS_NAME, JS_WS_SOCKET_IO_NAME, JS_PRIMUS_NAME],
    [ALIAS_KEY]: 'socketServerType'
  })
}

module.exports = {
  jsonqlKoaOptions: options
}
