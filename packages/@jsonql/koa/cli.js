#!/usr/bin/node
// new cli interface for jsonql Koa server
// this interface only accept on param --config
// and it search for a js or json file to import config
// if it's not found then use all the default options
// const { version } = require('./package.json')
const jsonqlKoaServer = require('./index')
const { extname } = require('path')
const fsx = require('fs-extra')
// const debug = require('debug')('jsonql-koa:cli')
const argv = require('yargs').argv
const args = argv._
let config = {}

if (args.length) {
  const configFile = args[0]
  if (fsx.existsSync(configFile)) {
    // if it's json
    const ext = extname(configFile).toLowerCase()
    switch(ext) {
      case '.json':
        config = fsx.readJsonSync(configFile)
        break;
      case '.js':
        config = require(configFile)
        break;
      default:
        console.error(`Can not use the supplied config file, only support js or json, but got: ${ext}`)
    }
  } else {
    console.error(`config file can not be found in: ${configFile}`)
  }
} else {
  console.info(`Using default option to start server`)
}
// @TODO we should look at the config files to see if there is two parts as well

jsonqlKoaServer(config)

/*
require('yargs')
  .option('config', {
    alias: 'c',
    describe: 'Path to configuration file'
  })
  .argv
*/
