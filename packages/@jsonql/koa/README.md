# jsonql Koa server

This is an all in one setup to have your jsonql API server setup in no time.

It includes:

1. Koa
2. bodyparser, cors middleware
3. [jsonql-koa](https://npmjs.com/pacakge/jsonql-koa) middleware
4. OptionalDependencies [jsonql-ws-server](https://npmjs.com/package/jsonql-ws-server)
5. OptionalDependencies jsonql-socketio-server (coming soon)

## Quick start

```js
const jsonqlKoaServer = require('@jsonql/koa')

jsonqlKoaServer({
  autoStart: true,
  port: 3000
})
```

That's it.

## CLI 

You can also use the command line `jsonql-koa-cli`, it's **better** install this globally.

```sh
$ jsonql-koa-cli /path/to/config.json
```

If you don't supply the config file, then it will just all the default options to start the server. 


## Configuration

TBC

---

MIT (c) 2020 [NEWBRAN LTD](https://newbran.ch) / [TO1SOURCE CN](https://to1source.cn)
