// main
const JsonqlKoaServer = require('./src/jsonql-koa-server')
/**
 * @param {object} [config={}]
 * @param {array} [middlewares=[]]
 * @return {object} the jsonqlKoaServer instance see the class for more info
 * @public
 */
module.exports = function createJsonqlKoaServer(config = {}, middlewares = []) {
  return new JsonqlKoaServer(config, middlewares)
}
