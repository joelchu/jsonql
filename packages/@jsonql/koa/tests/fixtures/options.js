const { join } = require('path')
const resolverDir = join(__dirname, 'resolvers')
const contractBaseDir = join(__dirname, 'contract')
const keysDir = join(__dirname, 'keys')
const AUTH_DIR = 'auth'
const BASIC_DIR = 'basic'
const SOCKET_DIR = 'socket'
const MAIN_DIR = 'main'
const MS_A_DIR = 'ms-a'
const MS_B_DIR = 'ms-b'
const MS_C_DIR = 'ms-c'
const PORT_A = 8084
const PORT_B = 8085
const PORT_C = 8086


module.exports = {
  resolverDir,
  contractBaseDir,
  keysDir,
  AUTH_DIR,
  BASIC_DIR,
  SOCKET_DIR,
  MS_A_DIR,
  MS_B_DIR,
  MS_C_DIR,
  PORT_A,
  PORT_B,
  PORT_C,
  MAIN_DIR
}
