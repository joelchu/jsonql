// const debug = require('debug')('jsonql-koa:test:get-something')
/**
 * just a pass over method to get something from server b
 * @return {object} an object contains two different time stamps
 */
module.exports = async function getSomething() {
  const client = getSomething.client('client0')
  // @NOTE when there are more than one client
  // we must pass the name prop otherwise it return false
  // debug(client)

  // just pass it straight through
  const msg = await client.sendingOutSomething()
  return `${msg} via getSomething()`
}
