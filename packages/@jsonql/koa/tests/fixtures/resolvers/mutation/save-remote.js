const debug = require('debug')('jsonql-koa:tests:mutation:save-remote')
/**
 * This will call the remote ms to save the result then modified it
 * @param {object} payload something to save
 * @param {number} condition
 * @return {object} with array of timestamp
 */
module.exports = async function saveRemote(payload, condition) {

  // @NOTE without the await and see if it works, and it should?
  const client = saveRemote.client('client2c')

  const result = await client.saveSomething(payload, condition)

  if (result.timestamp && Array.isArray(result.timestamp)) {
    result.timestamp.push(Date.now())
    return result
  }
  debug(result)
  throw new Error(`The result is not expected!`)
}
