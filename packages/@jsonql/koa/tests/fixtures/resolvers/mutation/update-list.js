const debug = require('debug')('jsonql-koa:resolver:mutation:updateList');
/**
 * @param {object} payload
 * @param {number} payload.user
 * @param {object} condition
 * @return {object} with user as key
 */
module.exports = function(payload, condition) {
  debug('calling updateList with', payload, condition)
  let p = payload.user + 1
  return {user: p}
};
