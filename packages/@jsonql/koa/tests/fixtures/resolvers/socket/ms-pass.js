const debug = require('debug')('jsonql-koa:test:basic')
/**
 * just a pass over method to call server B
 * @param {string} msg an incoming message
 * @return {object} contain everything and send it back
 */
module.exports = function msPass(msg) {
  try {
    // then we call the client and get something from it
    const client = msPass.client()

    return client.socket.giveNumber()
      .then(result => {
        return `${msg} received on ${result}`
      })
  } catch(e) {
    debug(e)
    throw new Error(e)
  }
}
