// multiple clients to multiple server test
const test = require('ava')
const fsx = require('fs-extra')
const { join } = require('path')
const jsonqlNodeClient = require('jsonql-node-client')
const debug = require('debug')('jsonql-koa:test:msm')
const { JS_WS_NAME, HELLO } = require('jsonql-constants')

const { contractBaseDir, MS_A_DIR, MS_B_DIR, MS_C_DIR, PORT_A, PORT_B, PORT_C } = require('./fixtures/options')
const contractDirA = join(contractBaseDir, MS_A_DIR)
const contractDirB = join(contractBaseDir, MS_B_DIR)
const contractDirC = join(contractBaseDir, MS_C_DIR)

const createMsTestServer = require('./fixtures/ms-test-servers')
const clientConfig = {
  hostname: `http://localhost:${PORT_C}`,
  name: 'client2c',
  contractDir: join(contractBaseDir, 'client2c')
}

const getClient = async (name = 'A') => (
  await jsonqlNodeClient(
    name === 'A' ? {
      hostname: `http://localhost:${PORT_A + 5}`,
      contractDir: join(contractBaseDir, 'client1'),
      serverType: 'ws'
    } : clientConfig
  )
)

test.before(t => {

  t.context.serverA = createMsTestServer('A1', clientConfig)
  t.context.serverB = createMsTestServer('B1')
  t.context.serverC = createMsTestServer('C')
})

test.after(t => {

  t.context.serverA.stop()
  t.context.serverB.stop()
  t.context.serverC.stop()

  fsx.removeSync(join(contractBaseDir, 'client2c'))

})

test(`It should able to connect to service C directly`, async t => {
  const client = await getClient('C')

  const res = await client.helloWorld()

  t.is(res, HELLO)
})

test(`It should able to connect to another service via the internal nodeClient using query`, async t => {
  const client1 = await getClient()

  const msg = await client1.getSomething()

  debug(msg)

  t.truthy(msg)
})


test(`It should able to connect to another service via the internal nodeClient using mutation`, async t => {
  const client1 = await getClient()
  const payload = { name: 'John Doe' }
  const result = await client1.saveRemote(payload, 1)

  debug(result)

  t.true(Array.isArray(result.timestamp))
})
