// ms test
const test = require('ava')
const fsx = require('fs-extra')
const { join } = require('path')
const jsonqlNodeClient = require('jsonql-node-client')
const debug = require('debug')('jsonql-koa:test:basic')
const { HELLO } = require('jsonql-constants')

const { contractBaseDir, MS_A_DIR, MS_B_DIR, PORT_A, PORT_B } = require('./fixtures/options')
const contractDirA = join(contractBaseDir, MS_A_DIR)
const contractDirB = join(contractBaseDir, MS_B_DIR)

const createMsTestServer = require('./fixtures/ms-test-servers')

const getClientA = async () => (
  await jsonqlNodeClient({
    hostname: `http://localhost:${PORT_A}`,
    contractDir: join(contractBaseDir, 'client1'),
    serverType: 'ws'
  })
)

test.before(t => {
  t.context.serverA = createMsTestServer('A')
  t.context.serverB = createMsTestServer('B')
})

test.after(t => {

  t.context.serverA.stop()
  t.context.serverB.stop()

  // fsx.removeSync(contractDirA)
  // fsx.removeSync(contractDirB)
  // fsx.removeSync(join(contractBaseDir, 'client1'))
  // fsx.removeSync(join(contractBaseDir, 'client2'))
})

test(`It should able to connect to service A directly`, async t => {
  const client1 = await getClientA()

  const res1 = await client1.helloWorld()

  t.is(res1, HELLO)
})

test(`It should able to connect to server B directly`, async t => {
  const client2 = await jsonqlNodeClient({
    hostname: `http://localhost:${PORT_B}`,
    contractDir: join(contractBaseDir, 'client2')
  })
  const res2 = await client2.helloWorld()
  t.is(res2, HELLO)
})


test(`It should able to connect to another service via the internal nodeClient`, async t => {
  const client1 = await getClientA()

  const msg = await client1.getSomething()

  debug(msg)

  t.truthy(msg)
})

/*
The socket is broken some how ...
test(`just test if there is any socket functions`, async t => {
  const client1 = await getClientA()

  const result = await client1.socket.msPass(`Something`)
  debug(result)
  t.truthy(result)
})
*/

test.cb.skip(`It should able to connect to the another client via socket connection`, t => {
  t.plan(2)
  getClientA()
    .then(client => {
      debug(`pass the then`)
      t.pass() // should get a client here

      client.socket.msPass(`Something`)
        .then(result => {
          debug(result)
          t.truthy(result)
          t.end()
        })
    })
    .catch(err => {
      debug('test error!', err)
    })

})
