// test the koa server with appDir options

const test = require('ava')
const fsx = require('fs-extra')
const { join } = require('path')
const jsonqlNodeClient = require('jsonql-node-client')
// const debug = require('debug')('jsonql-koa:test:basic')
const { HELLO } = require('jsonql-constants')

const basedir = join(__dirname, 'fixtures')
const contractDir = join(basedir, 'contract')
const createJsonqlKoaServer = require('../index')
const port = 10086

test.before(t => {
  t.context.server = createJsonqlKoaServer({
    appDir: join('tests', 'fixtures'),
    autoStart: true,
    port 
  })
})

test.after(t => {
  t.context.server.stop()
  fsx.removeSync(join(contractDir, 'contract.json'))
  fsx.removeSync(join(contractDir, 'public-contract.json'))
})

test(`It should able to use the appDir prop`, async t => {

  const client = await jsonqlNodeClient({
    hostname: `http://localhost:${port}`,
    contractDir
  })

  const result = await client.helloWorld()

  t.is(result, HELLO)
})

