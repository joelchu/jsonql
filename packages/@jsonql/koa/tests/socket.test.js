const test = require('ava')
const fsx = require('fs-extra')
const { join } = require('path')
const jsonqlNodeClient = require('jsonql-node-client')
const debug = require('debug')('jsonql-koa:test:socket')
const { JS_WS_NAME, HELLO } = require('jsonql-constants')
const jsonqlKoaServer = require('./fixtures/test-server')
const { contractBaseDir, SOCKET_DIR } = require('./fixtures/options')
const contractDir = join(contractBaseDir, SOCKET_DIR)
const keysDir = join(contractBaseDir, [SOCKET_DIR, 'keys'].join('-'))
const port = 8083

test.before(t => {
  const jsonqlKoaInt = jsonqlKoaServer({
    port,
    keysDir,
    contractDir,
    enableAuth: true,
    serverType: JS_WS_NAME
  })

  t.context.stop = () => jsonqlKoaInt.stop()

  jsonqlKoaInt.start()

})

test.after( t => {
  t.context.stop()
  fsx.removeSync(contractDir)
  fsx.removeSync(keysDir)
})

test.beforeEach(async t => {
  t.context.client = await jsonqlNodeClient({
    hostname: `http://localhost:${port}`,
    socketClientType: JS_WS_NAME,
    enableAuth: true,
    contractDir
  })
})

test.serial.cb(`It should able to connect to the public socket`, t => {
  t.plan(1)
  const client = t.context.client

  client.socket.availableToEveryone.onResult = (msg) => {
    t.truthy(msg.indexOf('public message'))
    t.end()
  }

  client.socket.availableToEveryone()
})
// @BUG this one was pending
test.cb.skip(`It should able to login to the socket and connect to a private socket channel`, t => {

  t.plan(2)

  const client = t.context.client
  // @BUG The problem happens in the ws-client when trigger the login it might be looking for the custom
  // login method which is shouldn't and needn't
  client.login('Jack')
    .then(result => {
      // how to test this token as valid?
      // debug(result)
      t.truthy(result)
    })
  // @TODO should make this onLogin available as a global method
  client.socket.onLogin = function() {

    const user = client.userdata()

    t.is(user.name, 'Jack')
    t.end()
  }
})
