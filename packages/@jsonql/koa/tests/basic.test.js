// basic test
const test = require('ava')
const fsx = require('fs-extra')
const { join } = require('path')
const jsonqlNodeClient = require('jsonql-node-client')
// const debug = require('debug')('jsonql-koa:test:basic')
const { HELLO } = require('jsonql-constants')

const { contractBaseDir, BASIC_DIR } = require('./fixtures/options')
const contractDir = join(contractBaseDir, BASIC_DIR)

const jsonqlKoaServer = require('./fixtures/test-server')

test.before(t => {
  const jsonqlKoaInt = jsonqlKoaServer({
    port: 8001,
    autoStart: true
  })

  // debug(jsonqlKoaInt)

  t.context.stop = () => jsonqlKoaInt.stop()
})

test.after(t => {
  t.context.stop()
  fsx.removeSync(contractDir)
  // debug(contractDir)
})

test(`Basic hello world test`, async t => {
  const client = await jsonqlNodeClient({
    hostname: 'http://localhost:8001',
    contractDir
  })

  const result = await client.helloWorld()

  t.is(result, HELLO)

})
