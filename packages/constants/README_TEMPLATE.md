[![NPM](https://nodei.co/npm/jsonql-constants.png?compact=true)](https://npmjs.org/package/jsonql-constants)

# jsonql-constants

This is a module export all the share constant use across all the
[jsonql](https://jsonql.org) modules.

Use it only when you want to develop your jsonql compatible module. 
If you are not using javascript to develop your module. 
You can also use the included `constants.json`.

As of Version 2, we split up the constants into sections:

- base
- prop 
- socket 
- validation 

Please consult the detail break down below.

## constants

[REPLACE]

---

MIT

to1source / newbran ltd (c) 2019
