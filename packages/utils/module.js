// exportfor ES modules
// ported from jsonql-params-validator
import { chainFns } from './src/chain-fns'
import {
  chainPromises,
  chainProcessPromises
} from './src/chain-promises'
import {
  injectToFn,
  objDefineProps,
  objHasProp
} from './src/obj-define-props'
import {
  isContract,
  extractSocketPart,
  extractArgsFromPayload,
  extractParamsFromContract
} from './src/contract'
import { timestamp } from './src/timestamp'
import { dasherize } from './src/dasherize'
import {
  urlParams,
  cacheBurst,
  cacheBurstUrl
} from './src/urls'
import {
  trim,
  inArray,
  toArray,
  isObjectHasKey,
  createEvt,
  getConfigValue,
  isNotEmpty,
  toJson,
  isFunc,
  isString,
  isArray,
  isNumber,
  parseJson,
  nil
} from './src/generic'
import {
  toPayload,
  formatPayload,
  createQuery,
  createQueryStr,
  createMutation,
  createMutationStr,
  getQueryFromArgs,
  getQueryFromPayload,
  getMutationFromArgs,
  getMutationFromPayload,
  getResolverFromPayload
} from './src/params-api'
import {
  getCallMethod,
  packResult,
  packError,
  resultHandler,
  isJsonqlErrorObj
} from './src/results'
import {
  logger,
  getLogger
} from './src/logger'
const VERSION = '__VERSION__'
// 1.0.1
import {
  groupByNamespace,
  getNamespaceInOrder,
  getNamespace,
  getNspInfoByConfig,
  getPrivateNamespace
} from './src/namespace'
import {
  isRegExp,
  getRegex
} from './src/regex'
// @1.1.0
import {
  createWsReply,
  createReplyMsg,
  createAcknowledgeMsg,
  isWsReply,
  extractWsPayload,
  createSendPayload
} from './src/socket'
// cookies 
import {
  cookieSet,
  cookieGet,
  cookieUpdate,
  cookieDel
} from './src/cookies'
// deep-merge 
import {
  deepmerge,
  merge
} from './src/deep-merge'
import {
  objIsEqual,
  isPlainObject
} from './src/object'
import { 
  mapValues, 
  mapKeys, 
  omitBy, 
  pickBy,
  findKey 
} from './src/lo'

// exports
export {
  getCallMethod,
  packResult,
  packError,
  resultHandler,
  isJsonqlErrorObj,
  // chain-fns
  chainFns,
  chainPromises,
  chainProcessPromises,
  // contract
  extractArgsFromPayload,
  extractParamsFromContract,
  extractSocketPart,
  isContract,
  // generic
  trim,
  inArray,
  toArray,
  isObjectHasKey,
  dasherize,
  createEvt,
  timestamp,
  urlParams,
  cacheBurst,
  cacheBurstUrl,
  getConfigValue,
  isNotEmpty,
  toJson,
  isFunc,
  isString,
  isArray,
  isNumber,
  parseJson,
  nil,
  // params-api
  toPayload,
  formatPayload,
  createQuery,
  createQueryStr,
  createMutation,
  createMutationStr,
  getQueryFromArgs,
  getQueryFromPayload,
  getMutationFromArgs,
  getMutationFromPayload,
  
  injectToFn,
  objDefineProps,
  objHasProp,

  VERSION,

  logger,
  getLogger,

  groupByNamespace,
  getNamespaceInOrder,
  getNamespace,
  getNspInfoByConfig,
  getResolverFromPayload,
  getPrivateNamespace,
  // 1.0.6
  isRegExp,
  getRegex,
  // 1.1.0 
  createWsReply,
  createReplyMsg,
  createAcknowledgeMsg,
  isWsReply,
  extractWsPayload,
  createSendPayload,
  // cookies 
  cookieSet,
  cookieGet,
  cookieUpdate,
  cookieDel,
  // deep merge 
  deepmerge,
  merge,
  // object 
  objIsEqual,
  isPlainObject,

  mapValues, 
  mapKeys, 
  omitBy, 
  pickBy,
  findKey
}
