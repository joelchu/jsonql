// testing the ported over methods 

const test = require('ava')
const { 
  mapValues, 
  mapKeys, 
  omitBy, 
  pickBy,
  findKey,
  isNumber,
  isString,
  objIsEqual
} = require('../main')

const obj = {id: 'a', name: 'b', age: 100, height: '2m', weight: '300lbs'}

test(`Test the two map object methods`, t => {

  const result1 = mapValues(obj, v => v+v)
  
  const result2 = mapKeys(obj, (v, k) => k+'x')

  t.is(result1.id, 'aa')
  t.is(result1.name, 'bb')

  t.is(result2.namex, 'b')
})

test(`Testing the omit and find from object method`, t => {

  const result3 = omitBy(obj, value => isString(value))

  const result4 = findKey(obj, value => isNumber(value))

  const result5 = pickBy(obj, value => isString(value))

  t.true(objIsEqual(result3, {age: 100}))

  t.is(result4, 'age')

  t.falsy(result5.age)
})