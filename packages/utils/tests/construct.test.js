// this is ported back from jsonql-params-validator
const test = require('ava')
const { isContract, isObjectHasKey } = require('../main')
const debug = require('debug')('jsonql-utils:construct')

test("It should able to check if an object is contract or not", t => {

    t.false(isContract('contract'))
    t.false(isContract([]))
    t.false(isContract({}))

    const contract = {query: {getSomething: {}}}

    t.deepEqual(isContract(contract), contract)
})

test("Test isObjectHasKey is exported or not", t => {
  const client = {query: {}, mutation: false, socket: null};

  t.true(isObjectHasKey(client, 'mutation'))
  t.true(isObjectHasKey(client, 'socket'))

  t.false(isObjectHasKey(client, 'auth'))

})
