// testing the client utils methods
const test = require('ava')
const { 
  VERSION,
  chainPromises, 
  objIsEqual,
  isPlainObject,
  merge,
  isNumber
} = require('../main')
const { join } = require('path')
const fsx = require('fs-extra')
const pkg = fsx.readJsonSync(join(__dirname, '..', 'package.json'))

const debug = require('debug')('jsonql-utils:client')

test(`It should have the version field`, t => {
  t.is(VERSION, pkg.version)
})


test.cb('The promises should be resolve one after the other even the early one resolve in a timer', t => {
  t.plan(3)
  const results = ['first', 'second', 'third']

  const p1 = () => new Promise(resolver => {
    setTimeout(() => {
      resolver(results[0])
    }, 1000)
  })
  const p2 = () => new Promise(resolver => {
    setTimeout(() => {
      resolver(results[1])
    }, 300)
  })
  const p3 = () => new Promise(resolver => {
    resolver(results[2])
  })

  chainPromises([p1(), p2(), p3()])
    .then(res => {
      res.forEach((r, i) => {
        t.is(r, results[i])
      })
      t.end()
    })
})


test(`Testing the new lodash-ish methods`, t => {

  const obj1 = {id: 'x'}
  const obj2 = {id: 'y'}
  const obj3 = {id: 'x'}

  const fn = function() {}

  t.true(objIsEqual(obj1, obj3))
  t.false(objIsEqual(obj1, obj2))

  t.true(isPlainObject(obj1))
  t.false(isPlainObject(fn))


  let f = {id: 1, name: {firstname: 'John', lastname: 'Doe'}, other: [1]}

  let n = {name: {lastname: 'Not Doe'}}
  let n1 = {other: [2,3]}

  let result = merge(f, n, n1)

  debug(result)

  t.is(result.name.lastname, 'Not Doe')

  t.is(3, result.other.length)
})

test(`just test the isNumber method`, t => {

  const num1 = 123
  const num11 = 123.4
  const num2 = '123'

  const txt = 'abc'

  t.true(isNumber(num1))
  t.true(isNumber(num11))


  t.false(isNumber(num2))

  t.false(isNumber(txt))

})