/*
Cookie utils 
*/
import { trim } from './generic'
const EXPIRE_KEY = 'expires'

/**
 * cookie 存贮
 * @param {String} key  属性
 * @param {*} value  值
 * @param String expire  过期时间,单位天
 * @return {void}
 */
export const cookieSet = (key, value, expire = false) => {
  let d 
  if (expire) {
    d = new Date()
    d.setDate(d.getDate() + expire)
  }
  let content = `${key}=${value};`
  document.cookie = content + (d ? `${EXPIRE_KEY}=${d.toGMTString()}` : '')
}

/**
 * cookie 获取
 * @param {String} key  属性
 * @return {*}
 */
export const cookieGet = key => {
  const cookieStr = unescape(document.cookie)
  const arr = cookieStr.split(';')
  let cookieValue = ''
  const ctn = arr.length
  for (var i = 0; i < ctn; ++i) {
    const temp = arr[i].split('=')
    if (temp[0] === key) {
      cookieValue = trim(temp[1])
      break
    }
  }
  return cookieValue
}

/**
 * Update cookie 
 * @param {*} key
 * @param {*} value 
 * @return {void}
 */
export const cookieUpdate = (key, value) => {
  const cookieStr = unescape(document.cookie)
  const arr = cookieStr.split(';')

  document.cookie = arr.map(a => {
    const pair = a.split('=')
    if (pair[0] === key) {
      return `${key}=${value}`
    }
    return a
  }).join(';')
}


/**
 * cookie 删除
 * @param {String} key  属性
 * @return {void}
 */
export const cookieDel = key => {
  document.cookie = `${encodeURIComponent(key)}=;${EXPIRE_KEY}=${new Date()}`
}