/*
DeepMerge recommended by https://davidwalsh.name/javascript-deep-merge
*/

/**
 * 
 * @param {*} val 
 */
function isMergeableObject(val) {
  var nonNullObject = val && typeof val === 'object'

  return nonNullObject
      && Object.prototype.toString.call(val) !== '[object RegExp]'
      && Object.prototype.toString.call(val) !== '[object Date]'
}

/**
 * 
 * @param {*} val 
 */
function emptyTarget(val) {
  return Array.isArray(val) ? [] : {}
}

/**
 * 
 * @param {*} value 
 * @param {*} optionsArgument 
 */
function cloneIfNecessary(value, optionsArgument) {
  var clone = optionsArgument && optionsArgument.clone === true
  return (clone && isMergeableObject(value)) ? deepmerge(emptyTarget(value), value, optionsArgument) : value
}

/**
 * 
 * @param {*} target 
 * @param {*} source 
 * @param {*} optionsArgument 
 */
function defaultArrayMerge(target, source, optionsArgument) {
  var destination = target.slice()
  source.forEach(function(e, i) {
      if (typeof destination[i] === 'undefined') {
          destination[i] = cloneIfNecessary(e, optionsArgument)
      } else if (isMergeableObject(e)) {
          destination[i] = deepmerge(target[i], e, optionsArgument)
      } else if (target.indexOf(e) === -1) {
          destination.push(cloneIfNecessary(e, optionsArgument))
      }
  })
  return destination
}

/**
 * 
 * @param {*} target 
 * @param {*} source 
 * @param {*} optionsArgument 
 */
function mergeObject(target, source, optionsArgument) {
  var destination = {}
  if (isMergeableObject(target)) {
      Object.keys(target).forEach(function (key) {
          destination[key] = cloneIfNecessary(target[key], optionsArgument)
      })
  }
  Object.keys(source).forEach(function (key) {
      if (!isMergeableObject(source[key]) || !target[key]) {
          destination[key] = cloneIfNecessary(source[key], optionsArgument)
      } else {
          destination[key] = deepmerge(target[key], source[key], optionsArgument)
      }
  })
  return destination
}

/**
 * The main interface to perform the merge of two object 
 * @param {*} target 
 * @param {*} source 
 * @param {*} optionsArgument
 * @return {object}  
 */
function deepmerge(target, source, optionsArgument) {
  var array = Array.isArray(source);
  var options = optionsArgument || { arrayMerge: defaultArrayMerge }
  var arrayMerge = options.arrayMerge || defaultArrayMerge

  if (array) {
      return Array.isArray(target) ? arrayMerge(target, source, optionsArgument) : cloneIfNecessary(source, optionsArgument)
  } else {
      return mergeObject(target, source, optionsArgument)
  }
}

/**
 * This is the merge multiple 
 * @param {array} array 
 * @param {object} optionsArgument
 * @return {object} 
 */
deepmerge.all = function deepmergeAll(array, optionsArgument) {
  if (!Array.isArray(array) || array.length < 2) {
      throw new Error('first argument should be an array with at least two elements')
  }

  // we are sure there are at least 2 values, so it is safe to have no initial value
  return array.reduce(function(prev, next) {
      return deepmerge(prev, next, optionsArgument)
  })
}

/**
 * pretend to be the lodash version
 * @param  {...any} args 
 * @return {object}
 */
function merge(...args) {
  return deepmerge.all(args, {})
}

export { deepmerge, merge }