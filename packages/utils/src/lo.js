// mocking the lodash-es methods 

/**
 * mapValues / mapKeys internal method 
 * @param {*} obj 
 * @param {*} fn 
 * @param {*} type 
 * @return {object}
 */
function mapFn(obj, fn, type = 'key') {
  let newObj = {}
  for (let key in obj) {
    let value = obj[key]
    let k = type === 'key' ? fn(value, key) : key 
    let v = type === 'key' ? value : fn(value, key)
    newObj[k] = v 
  }
  return newObj
}


/**
 * map values to an object
 * @param {object} obj 
 * @param {function} fn 
 * @return {object}
 */
export function mapValues(obj, fn) {
  return mapFn(obj, fn, 'value')
}

/**
 * map the new keys to an object
 * @param {object} obj 
 * @param {function} fn 
 * @return {object} 
 */
export function mapKeys(obj, fn) {
  return mapFn(obj, fn, 'key')
}

/**
 * Core method for the omitBy and pickBy 
 * @param {*} obj 
 * @param {*} fn 
 * @param {*} result 
 * @return {object}
 */
function filterFn(obj, fn, result = true) {
  let newObj = {}
  for (let key in obj) {
    let value = obj[key]
    let r = fn(value, key)
    if (result) {
      if (r) {
        newObj[key] = value 
      }
    } else {
      if (!r) {
        newObj[key] = value
      }
    }
  }
  return newObj
}

/**
 * Run filter by a callback on an object 
 * @param {object} obj 
 * @param {function} fn callback to filter out things
 * @return {object}
 */
export function omitBy(obj, fn) {
  return filterFn(obj, fn, false)
}

/**
 * Opposite of OmitBy 
 * @param {*} obj 
 * @param {*} fn 
 * @return {object}
 */
export function pickBy(obj, fn) {
  return filterFn(obj, fn, true)
}

/**
 * Find a key from an object by the callback 
 * @param {object} obj 
 * @param {function} fn 
 * @return {string}
 */
export function findKey(obj, fn) {
  for (let key in obj) {
    let value = obj[key]
    if (fn(value, key)) {
      return key
    }
  }
  return null
}