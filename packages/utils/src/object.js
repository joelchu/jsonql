// we are going to replace the dependencey of lodash-es 
/**
 * 判断两个对象是否相等,目前只支持对象值为简单数据类型的判断
 * @param {Object} oneObj  对象
 * @param {Object} twoObj 对象
 */
export const objIsEqual = (oneObj, twoObj) => {
  const aProps = Object.getOwnPropertyNames(oneObj)
  const bProps = Object.getOwnPropertyNames(twoObj)

  if (aProps.length != bProps.length) {
    return false
  }

  for (let i = 0; i < aProps.length; i++) {
    let propName = aProps[i]
    let propA = oneObj[propName]
    let propB = twoObj[propName]
    if ( propA !== propB) {
      return false
    }
  }
  return true
}

// ensure this method is available in the Object
Object.getPrototypeOf || (Object.getPrototypeOf=function(obj){
  return obj.__proto__ || obj.prototype || (obj.constructor&&obj.constructor.prototype) || Object.prototype
})

/**
 * Test if a prop is plain object (not null function etc)
 * @param {*} obj
 * @return {boolean}  
 */
export const isPlainObject = obj => {
  if (obj!=null && typeof(obj)=="object" && Object.getPrototypeOf(obj)==Object.prototype) {
    return true 
  }
  return false
}

