The documentation has moved to https://github.com/joel-chu/jsonql-org

And its now published in [jsonql.js.org](https://jsonql.js.org)
