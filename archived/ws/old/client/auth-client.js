import getWS from './ws'
import { TOKEN_PARAM_NAME } from 'jsonql-constants'
const WS = getWS()
/**
 * Create a client with auth token
 * @param {string} url start with ws:// @TODO check this?
 * @param {string} token the jwt token
 * @return {object} ws instance
 */
export function wsAuthClient(url, token) {
  return wsClient(`${url}?${TOKEN_PARAM_NAME}=${token}`)
}

/**
 * Also create a normal client this is for down stream to able to use in node and browser
 * @NOTE CAN NOT pass an option if I do then the brower throw error
 * @param {string} url end point
 * @return {object} ws instance
 */
export function wsClient(url) {
  return new WS(url)
}
