// ws node clients
let WebSocket;
try {
  WebSocket = require('ws')
} catch(e) {
  console.error(`You need to install ws module manually as a peer dependencies! If you are not using it then ignore this message.`)
}

const { TOKEN_PARAM_NAME } = require('jsonql-constants')

/**
 * normal client
 * @param {object} WebSocket the ws object
 * @param {string} url end point
 * @param {object} [options={}] configuration
 * @return {object} ws instance
 */
function wsNodeClient(url, options = {}) {
  return new WebSocket(url, options)
}

/**
 * Create a client with auth token
 * @param {string} url start with ws:// @TODO check this?
 * @param {string} token the jwt token
 * @param {object} [options={}] if there is any configuration option
 * @return {object} ws instance
 */
function wsNodeAuthClient(url, token, options = {}) {
  return wsNodeClient(`${url}?${TOKEN_PARAM_NAME}=${token}`, options)
}

module.exports = {
  wsNodeClient,
  wsNodeAuthClient
}
