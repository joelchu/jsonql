// this will generate a event emitter and will be use everywhere
import NBEventService from 'nb-event-service'
// output
export default function(debugOn) {
  let logger = debugOn ? (...args) => {
    args.unshift('[NBS]')
    console.log.apply(null, args)
  }: undefined;
  return new NBEventService({ logger })
}
