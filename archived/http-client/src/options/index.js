// export interface
import checkOptionsAsync from './check-options-async'
import checkOptions from './check-options'

export {
  checkOptionsAsync,
  checkOptions
}
