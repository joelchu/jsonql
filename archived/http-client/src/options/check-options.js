// This is for the sync version therefore we don't need to care about the contract options
import { appProps, constProps } from './base-options'
import { checkConfig } from 'jsonql-params-validator'

export default function checkOptions(config) {
  return checkConfig(config, appProps, constProps)
}
