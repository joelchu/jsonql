// Generate the resolver for developer to use

// @TODO when enableAuth we need to add one extra check
// before the resolver call make it to the core
// which is checking the login state, if the developer
// is calling a private method without logging in
// then we should throw the JsonqlForbiddenError at this point
// instead of making a round trip to the server
import { LOGOUT_NAME, ISSUER_NAME, KEY_WORD } from 'jsonql-constants'
import { validateAsync } from 'jsonql-params-validator'
import {
  JsonqlValidationError,
  JsonqlError,
  clientErrorsHandler,
  finalCatch
} from 'jsonql-errors'

import methodsGenerator from './methods-generator'

/**
 * @param {object} jsonqlInstance jsonql class instance
 * @param {object} config options
 * @param {object} contract the contract
 * @param {object} ee eventEmitter
 * @return {object} constructed functions call
 */
const generator = (jsonqlInstance, config, contract, ee) => {
  // V1.3.0 - now everything wrap inside this method
  let obj = methodsGenerator(jsonqlInstance, ee, config, contract)
  // create the rest of the methods
  if (config.enableAuth) {
    /**
     * new method to allow retrieve the current login user data
     * @return {*} userdata
     */
    obj.userdata = () => jsonqlInstance.userdata;
  }
  // allow getting the token for valdiate agains the socket
  obj.getToken = () => jsonqlInstance.rawAuthToken;
  // this will pass to the ws-client if needed
  // obj.eventEmitter = ee;
  // this will require a param
  if (config.exposeContract) {
    obj.getContract = () => jsonqlInstance.get()
  }
  // this is for the ws to use later
  obj.eventEmitter = ee;
  obj.version = '__VERSION__';
  // output
  return obj;
};

export default generator;
