// this is the new Event base interface
// the export will be different and purposely design for framework that
// is very hard to use Promise such as Vue
import jsonqlStaticGenerator from './core/jsonql-static-generator'
import JsonqlBaseClient from './base'
import { checkOptions } from './options'
import { getContractFromConfig } from './utils'
import getEventEmitter from './ee'
/**
 * this is the slim client without Fly, you pick the version of Fly to use
 * This is a breaking change because it swap the input positions
 * @param {object} Fly fly.js
 * @param {object} config configuration
 * @return {object} the jsonql client instance
 */
export default function jsonqlStaticClient(Fly, config = {}) {
  const { contract } = config;
  const opts = checkOptions(config)
  const jsonqlBase = new JsonqlBaseClient(opts, Fly)
  const contractPromise = getContractFromConfig(jsonqlBase, contract)
  const ee = getEventEmitter(opts.debugOn)
  // finally
  let methods = jsonqlStaticGenerator(jsonqlBase, opts, contractPromise, ee)
  methods.eventEmitter = ee;
  return methods;
}
