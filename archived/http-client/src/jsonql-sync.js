// this will be the sync version
import JsonqlBaseClient from './base'
import generator from './core/jsonql-api-generator'
import { checkOptions } from './options'
import ee from './ee'

/**
 * when the client contains a valid contract
 * @param {object} config
 * @param {object} Fly the fly client
 * @return {object} the client
 */
export default function jsonqlSync(ee, config, Fly) {
  const { contract } = config;
  const opts = checkOptions(config)

  const jsonqlBase = new JsonqlBaseClient(opts, Fly)

  return generator(jsonqlBase, opts, contract, ee)
}
