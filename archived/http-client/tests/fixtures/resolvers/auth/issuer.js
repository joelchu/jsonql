const debug = require('debug')('jsonql-client:auth:issuer');

/**
 * @param {string} key for append
 * @return {string} concat string
 */
module.exports = function(key) {
  debug('got a key', key);
  return 123456 + key;
}
