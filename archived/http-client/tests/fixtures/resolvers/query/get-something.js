
/**
 * This is a new query that require parameters for testing
 * @param {string} key key
 * @param {string} [value='world'] optional value
 * @return {object} put key / value together into an object
 */
module.exports = function(key, value = 'world') {
  return {
    [key]: value
  }
}
