/**
 * Rollup config for TESTING
 * DO NOT USE THIS DIRECTLY
 */
import { join } from 'path'
import buble from 'rollup-plugin-buble'
import replace from 'rollup-plugin-replace'
import commonjs from 'rollup-plugin-commonjs'
import size from 'rollup-plugin-bundle-size'
import nodeResolve from 'rollup-plugin-node-resolve'
import nodeGlobals from 'rollup-plugin-node-globals'
import builtins from 'rollup-plugin-node-builtins';
import async from 'rollup-plugin-async'

const env = process.env.NODE_ENV;

let plugins = [
  buble({
    objectAssign: 'Object.assign'
  }),
  nodeResolve({ preferBuiltins: false }),
  commonjs({
    include: 'node_modules/**'
  }),
  nodeGlobals(),
  builtins(),
  async(),
  replace({ 'process.env.NODE_ENV': JSON.stringify('production') }),
  size()
]

const dist = join(__dirname, 'dist')
let in_file = join(__dirname, 'test.js')
let out_file = join(dist, 'jsonql-client.test.js')
let name = 'JsonqlClientOOP';
// swith the files using the env
switch (env) {
  case 'stores':
      in_file = join(src, 'lib', 'stores', 'index.js')
      out_file = join(dist, 'jsonql-stores.js')
      name = 'JsonqlStores';
    break;
  default:
    // nothing
}

let config = {
  input: in_file,
  output: {
    name: name,
    file: out_file,
    format: 'umd',
    sourcemap: true,
    globals: {
      debug: 'debug',
      'promise-polyfill': 'Promise'
    }
  },
  external: [
    'debug',
    'fetch',
    'Promise',
    'promise-polyfill',
    'superagent',
    'handlebars',
    'tty'
  ],
  plugins: plugins
}

export default config
