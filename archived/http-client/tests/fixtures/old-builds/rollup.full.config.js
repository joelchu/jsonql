/**
 * Rollup config to create a full distribution with fly.js for browser
 */
import { join } from 'path'
import buble from 'rollup-plugin-buble'
import { terser } from "rollup-plugin-terser"
import replace from 'rollup-plugin-replace'
import commonjs from 'rollup-plugin-commonjs'
import nodeResolve from 'rollup-plugin-node-resolve'
import nodeGlobals from 'rollup-plugin-node-globals'
// import builtins from 'rollup-plugin-node-builtins'
import size from 'rollup-plugin-bundle-size'
import async from 'rollup-plugin-async'

import pkg from './package.json'
/*
 need to include more different one
 ws:
 ap:
 node: flyio/src/node
 weex:
*/
const getReplace = () => {
  switch (process.env.TARGET) {
    case 'wx':
      return 'flyio/dist/npm/wx'
    case 'ap':
      return 'flyio/dist/npm/ap'
    case 'weex':
      return 'flyio/dist/npm/weex'
    default: // browser
      return 'flyio/dist/npm/fly'
  }
}

const getOutputFile = () => {
  let fileName = env === 'production' ? 'jsonql-client.umd.js' : 'jsonql-static-client.umd.js'
  switch (process.env.TARGET) {
    case 'wx':
      return join(__dirname, 'wx.js')
    case 'ap':
      return join(__dirname, 'ap.js')
    case 'weex':
      return join(__dirname, 'weex.js')
    default:
      return join(__dirname, 'dist', fileName)
  }
}

const env = process.env.NODE_ENV;

let plugins = [
  buble({
    objectAssign: 'Object.assign'
  }),
  nodeResolve({
    preferBuiltins: true
  }),
  commonjs({
    include: 'node_modules/**'
  }),
  nodeGlobals(),
  // builtins(),
  async(),
  replace({
    'process.env.NODE_ENV': JSON.stringify('production'),
    '__VERSION__': pkg.version,
    '__TO_REPLACE_IMPORT__': getReplace()
  })
]

// plugins.push(terser())
plugins.push(size())

let globals = {
  debug: 'debug',
  'promise-polyfill': 'Promise',
  'fs': 'fs',
  'util': 'util',
  'path': 'path'
}
let external =  [
  'debug',
  'fetch',
  'Promise',
  'promise-polyfill',
  'superagent',
  'handlebars',
  'tty',
  'fs',
  'util'
]

let sourceFile = env === 'production' ? 'full.js' : join('src', 'static-full.js')
let moduleName = env === 'production' ? 'jsonqlClient' : 'jsonqlClientStatic'
let config = {
  input: join(__dirname, sourceFile),
  output: [{
    name: moduleName,
    file: getOutputFile(),
    format: 'umd',
    sourcemap: true,
    globals
  }],
  plugins,
  external
}

export default config
