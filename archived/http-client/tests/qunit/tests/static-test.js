

QUnit.test('jsonqlClientStatic should able to connect to server', function(assert) {
  var done1 = assert.async()
  var done2 = assert.async()
  // init client
  var client = jsonqlClientStatic({
    hostname: 'http://localhost:8081',
    showContractDesc: true,
    keepContract: false,
    debugOn: true
  })

  client.query('getSomethingNonExisted', 'whatever')
    .then(function(result) {
      console.log('[Qunit]', 'getSomething result', result)
    })

  client.query('getSomething', 'whatever', 'shit')
    .then(function(result) {
      console.log('[Qunit]', 'getSomething result', result)
      assert.equal(true, typeof result.whatever === 'string', 'just passing this one')
      done2()
    })
    .catch(function(error) {
      console.error('[Qunit]', 'some error happend?', error)
      done2()
    })

  client.query('helloWorld')
    .then(function(result) {
      console.info(result)
      assert.equal('Hello world!', result, "Hello world test done")
      done1()
    })
    .catch(function(error) {
      console.error('[Qunit]', 'catch', error)
      done1()
    })


})
