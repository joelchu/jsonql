// this one will bring the fly.js in
// also the built jsonql-client.umd.js together
// init it @TODO placeholder this Fly import and switch using the NODE_ENV
// because we are going to create one for wechat and one for node

import Fly from 'flyio/dist/npm/fly'
import jsonqlClient from './index'

export default function(config = {}) {
  return jsonqlClient(Fly, config)
}
