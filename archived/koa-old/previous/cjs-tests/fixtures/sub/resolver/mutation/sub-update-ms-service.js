/**
 * create a mutation to test why the call said the payload already declared
 * @param {string} payload incoming
 * @return {string} output
 */
module.exports = function subUpdateMsService(payload) {
  return payload + ' updated';
}
