// testing just one function chainFns
const test = require('ava')
const { chainFns } = require('../src/lib')


test('It should able to accept more than one functions after the first one', t => {

  const baseFn = (num) => num * 10;
  const add1 = (num) => num + 1;
  const add2 = (num) => num + 2;

  const fn = chainFns(baseFn, add1, add2)

  const result = fn(10)

  t.is(103, result)

})
