// The core of the jsonql middleware
const { QUERY_NAME, MUTATION_NAME } = require('jsonql-constants')
const { getDebug, resolveMethod } = require('./lib')
const debug = getDebug('core')

/**
 * Top level entry point for jsonql Koa middleware
 * @param {object} config options
 * @return {mixed} depends whether if we catch the call
 */
module.exports = function(opts) {
  // ouput the middleware
  return async function(ctx, next) {
    if (ctx.state.jsonql.isReq) {
      const { contract, resolverType } = ctx.state.jsonql;
      debug('isJsonqlRequest', resolverType)
      if (resolverType === QUERY_NAME || resolverType === MUTATION_NAME) {
        // debug(`Is jsonql query`, contract, opts)
        // The problem is - the config is correct
        // but the contract return the previous one
        return resolveMethod(ctx, resolverType, opts, contract)
      }
    } else {
      await next()
    }
  }
}
