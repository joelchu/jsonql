// This will be handling the json:ql console
const { isJsonqlConsoleUrl } = require('./lib')
const webConsole = require('jsonql-web-console')

// export
module.exports = function(opts) {
  return webConsole(opts, isJsonqlConsoleUrl)
}
