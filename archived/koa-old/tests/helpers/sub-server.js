const { join } = require('path')
const fixturesDir = join(__dirname, '..', 'fixtures')

const serverIoCore = require('server-io-core')
const { jsonqlKoa } = require('../../main')

function startSubServer(msPort) {
  return serverIoCore({
    webroot: join(fixturesDir, 'html'),
    socket:false,
    open:false,
    debugger: false,
    port: msPort,
    middlewares: [
      jsonqlKoa({
        name: `server${msPort}`,
        contractDir: join(fixturesDir, 'tmp', `sub-server-${msPort}`),
        resolverDir: join(fixturesDir, 'sub', 'resolver')
      })
    ]
  })
}

module.exports = startSubServer
