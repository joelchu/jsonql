// this will export the server for other test to use
// const test = require('ava');
const Koa = require('koa')
const { join } = require('path')
const bodyparser = require('koa-bodyparser')
const { jsonqlKoa } = require('../../main')
console.info(jsonqlKoa)
const { type, headers, dirs } = require('../fixtures/options')
const fsx = require('fs-extra')
const myKey = '4670994sdfkl';
// add a dir to seperate the contract files
module.exports = (config={}, dir = '') => {
  const app = new Koa()
  app.use(bodyparser())
  app.use(jsonqlKoa(
    Object.assign({},{
      resolverDir: dirs.resolverDir,
      contractDir: join(dirs.contractDir, dir)
    }, config)
  ))
  return app;
}
