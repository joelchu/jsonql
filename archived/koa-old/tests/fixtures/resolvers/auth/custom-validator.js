// this will use for testing the chaining validator methods
const debug = require('debug')('jsonql-koa:custom-validator')
const { dummy } = require('../../options')

/**
 * @param {*} userdata pass by the jsonql-jwt method
 * @return {*} just pass it back
 */
module.exports = function(userdata) {
  debug('I am being call by the chain')
  userdata.dummy = dummy;
  return userdata;
}
