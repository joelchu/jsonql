// main export interface
import authMiddleware from './auth-middleware'
import coreMiddleware from './core-middleware'
import contractMiddleware from './contract-middleware'
import helloMiddleware from './hello-middleware'


import consoleMiddleware from './console-middleware'
import publicMethodMiddleware from './public-method-middleware'
// import errorsHandlerMiddleware from './errors-handler-middleware'
import initMiddleware from './init-middleware'

// export
export {
  // configCheck,

  consoleMiddleware,
  authMiddleware,
  coreMiddleware,
  contractMiddleware,
  helloMiddleware,

  publicMethodMiddleware,
  // errorsHandlerMiddleware,
  initMiddleware
}
